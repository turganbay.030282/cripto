<?php


namespace App\Api;

use GuzzleHttp\Client;
use Psr\Http\Message\StreamInterface;
use Throwable;

abstract class ApiProvider
{
    /**
     * @var Client Guzzle client
     */
    protected $client;

    /**
     * VendorProvider constructor.
     */
    public function __construct()
    {
        $this->client = static::createClient();
    }

    /**
     * Query request by GET method.
     *
     * @param string $method
     * @param string $requestUri
     * @param array $options
     * @return StreamInterface
     * @throws Throwable
     */
    public function getSingleQueryResponseContent(string $method, string $requestUri, array $options = [])
    {
        try {
            return $this->client->request($method, $requestUri, $options)->getBody();
        } catch (Throwable $throwable) {
            \Log::channel('api')->info($throwable->getMessage() . PHP_EOL . json_encode($throwable->getTrace()));
            throw $throwable;
        }
    }

    /**
     * Create Guzzle client.
     *
     * @return Client
     */
    protected function createClient()
    {
        return new Client([
            'base_uri' => $this->getBaseUri(),
            'connect_timeout' => config('api.api_timeout'),
        ]);
    }

    /**
     * @return string
     */
    abstract protected function getBaseUri();
}
