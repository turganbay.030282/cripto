<?php


namespace App\Api\Cripto;


use App\Api\ApiProvider;
use Exception;
use Throwable;

class CoinmarketcapApiProvider extends ApiProvider
{
    /**
     * @param string $uri
     * @param array $queryParameters
     * @return mixed
     * @throws Throwable
     */
    protected function request(string $uri, array $queryParameters = [])
    {
        $options = [
            'query' => $queryParameters,
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'X-CMC_PRO_API_KEY' => config('api.coin_market_cap_api.key')
            ],
        ];

        $content = $this->getSingleQueryResponseContent('GET', $uri, $options);

        $decodedContent = json_decode($content);

        if (json_last_error() == JSON_ERROR_NONE) {
            return $decodedContent;
        } else {
            \Log::channel('coin')->info('Not valid JSON received from Coin api: ' . $uri);
            throw new Exception('Not valid JSON received from Game api: ' . $uri);
        }
    }

    /**
     * @return string
     */
    protected function getBaseUri()
    {
        return config('api.coin_market_cap_api.base_url') . config('api.coin_market_cap_api.version') . '/';
    }
}
