<?php

namespace App\Api\Cripto;

use Throwable;

class CoinmarketcapDataProvider extends CoinmarketcapApiProvider
{
    const COIN_MAP = 'cryptocurrency/map';
    const COIN_METADATA = 'cryptocurrency/info';
    const COIN_LATEST_LISTINGS = 'cryptocurrency/listings/latest';
    const COIN_HISTORICAL_LISTINGS = 'cryptocurrency/listings/historical';
    const COIN_LATEST_QUOTES = 'cryptocurrency/quotes/latest';
    const COIN_HISTORICAL_QUOTES = 'cryptocurrency/quotes/historical';
    const COIN_LATEST_MARKET_PAIRS = 'cryptocurrency/market-pairs/latest';
    const COIN_LATEST_OHLCV = 'cryptocurrency/ohlcv/latest';
    const COIN_HISTORICAL_OHLCV = 'cryptocurrency/ohlcv/historical';
    const COIN_PRICE_PERFORMANCE_STATS = 'cryptocurrency/price-performance-stats/latest';

    public function map(array $data = [])
    {
        return $this->request(self::COIN_MAP, $data);
    }

    public function info(array $data = [])
    {
        return $this->request(self::COIN_METADATA, $data);
    }

    public function listingsLatest(array $data = [])
    {
        return $this->request(self::COIN_LATEST_LISTINGS, $data);
    }

    public function listingsHistory(array $data = [])
    {
        return $this->request(self::COIN_HISTORICAL_LISTINGS, $data);
    }

    public function quotesLatest(array $data = [])
    {
        return $this->request(self::COIN_LATEST_QUOTES, $data);
    }

    public function marketPairs(array $data = [])
    {
        return $this->request(self::COIN_LATEST_MARKET_PAIRS, $data);
    }

    public function historicalOhlcv(array $data = [])
    {
        return $this->request(self::COIN_HISTORICAL_OHLCV, $data);
    }
}
