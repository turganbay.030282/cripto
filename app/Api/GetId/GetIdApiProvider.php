<?php

namespace App\Api\GetId;

use App\Api\ApiProvider;
use Exception;
use Throwable;

class GetIdApiProvider extends ApiProvider
{
    /**
     * @param string $uri
     * @param array $queryParameters
     * @return mixed
     * @throws Throwable
     */
    public function request(array $queryParameters = [])
    {
        $options = [
            'json' => [
                'metadata' => [
                    'externalId' => (string)$queryParameters['user_id'],
                    'platform' => 'web'
                ],
                'application' => [
                    'fields' => [
                        [
                            'contentType' => 'string',
                            'content' => $queryParameters['user_first_name'],
                            'category' => 'First name'
                        ],
                        [
                            'contentType' => 'string',
                            'content' => $queryParameters['user_last_name'],
                            'category' => ' Last name'
                        ],
                        [
                            'contentType' => 'string',
                            'content' => $queryParameters['user_birthday'],
                            'category' => 'Date of Birth'
                        ]
                    ],
                    'documents' => [
                        [
                            'documentType' => $queryParameters['document_type'],
                            'files' => $queryParameters['files']
                        ]
                    ]
                ],
                'verificationTypes' => [
                    'data-extraction',
                    'watchlists',
                    'cross-checking'
                ]
            ],
            'headers' => [
                'Content-Type' => 'application/json',
                'X-API-Key' => config('api.get_id_api.key')
            ]
        ];

        $content = $this->getSingleQueryResponseContent('POST', 'application', $options);

        $decodedContent = json_decode($content);

        if (json_last_error() == JSON_ERROR_NONE) {
            return $decodedContent;
        } else {
            \Log::channel('verify')->info('Not valid JSON received from GetId POST api');
            throw new Exception('Not valid JSON received from Game api: application');
        }
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Throwable
     */
    public function applicationStatus(array $data)
    {
        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
                'X-API-Key' => config('api.get_id_api.key')
            ],
        ];

        $content = $this->getSingleQueryResponseContent('GET', 'application/'.$data['request_id'], $options);

        $decodedContent = json_decode($content);

        if (json_last_error() == JSON_ERROR_NONE) {
            return $decodedContent;
        } else {
            \Log::channel('verify')->info('Not valid JSON received from GetId GET api');
            throw new Exception('Not valid JSON received from Game api: application');
        }
    }


    /**
     * @return string
     */
    protected function getBaseUri()
    {
        return config('api.get_id_api.base_url') . '/' . config('api.get_id_api.version') . '/';
    }
}
