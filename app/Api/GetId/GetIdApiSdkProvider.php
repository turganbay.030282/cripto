<?php

namespace App\Api\GetId;

use App\Api\ApiProvider;
use Exception;
use Throwable;

class GetIdApiSdkProvider extends ApiProvider
{
    public function getToken($customerId)
    {
        $options = [
            'json' => [
                'customerId' => $customerId
            ],
            'headers' => [
                'Content-Type' => 'application/json',
                'apikey' => config('api.get_id_api.key-sdk')
            ]
        ];

        $content = $this->getSingleQueryResponseContent('POST', 'token', $options);

        $decodedContent = json_decode($content);

        if (json_last_error() == JSON_ERROR_NONE) {
            return $decodedContent;
        } else {
            \Log::channel('verify')->info('Not valid JSON received from GetId POST api');
            throw new Exception('Not valid JSON received from Game api: application');
        }
    }


    /**
     * @return string
     */
    protected function getBaseUri()
    {
        return config('api.get_id_api.base_url_sdk') . '/sdk/' . config('api.get_id_api.version') . '/';
    }
}
