<?php

namespace App\Api\GetId;

use Throwable;

class GetIdDataProvider extends GetIdApiProvider
{
    const URL_APPLICATION = 'application';

    public function application(array $data = [])
    {
        return $this->request(self::URL_APPLICATION, $data);
    }
}
