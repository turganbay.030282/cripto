<?php

namespace App\Api\GetId;

use App\Entity\UserVerification;

class GetIdResponse
{
    private $response;

    public function __construct($response)
    {
        if (is_array($response))
            $response = json_decode(json_encode($response));

        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getStatus()
    {
        return $this->response->overallResult->status;
    }

    public function getId()
    {
        return $this->response->id;
    }

    public function getClientId()
    {
        return $this->response->metadata->externalId;
    }

    public function getAppFields()
    {
        return $this->response->application->fields;
    }

    public function getAppDocuments()
    {
        return $this->response->application->documents;
    }

    public function getAppSelfie()
    {
        return $this->response->application->selfie;
    }

    public function isApprove()
    {
        return $this->getStatus() == UserVerification::STATUS_APPROVED;
    }

    public function isError()
    {
        return $this->getStatus() == UserVerification::STATUS_ERROR;
    }

    public function isDecline()
    {
        return $this->getStatus() == UserVerification::STATUS_REJECT;
    }
}
