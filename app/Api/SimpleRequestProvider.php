<?php


namespace App\Api;


use Throwable;

class SimpleRequestProvider extends ApiProvider
{
    /**
     * @param string $method
     * @param string $url
     * @param array $formParameters
     * @return mixed
     * @throws Throwable
     */
    public function formDataRequest(string $method, string $url, array $formParameters = [])
    {
        $options = [
            'form_params' => $formParameters,
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
        ];

        return $this->getSingleQueryResponseContent($method, $url, $options)->getContents();
    }

    /**
     * @return string
     */
    protected function getBaseUri()
    {
        return '';
    }
}
