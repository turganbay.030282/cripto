<?php

namespace App\AppServices\Sms;

class ArraySender implements SmsSender
{
    private $messages = [];

    public function send($number, $text): void
    {
        $this->messages[] = [
            'to' => '+' . trim($number, '+'),
            'text' => $text
        ];
        \Log::info('ArraySender', [$this->messages]);
    }

    public function getMessages(): array
    {
        return $this->messages;
    }
}
