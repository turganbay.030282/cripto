<?php

namespace App\AppServices\Sms;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class Esteria implements SmsSender
{
    private $appKey;
    private $url;
    private $client;

    public function __construct($appKey, $url = 'https://api2.esteria.lv/')
    {
        if (empty($appKey)) {
            throw new \InvalidArgumentException('Sms appId must be set.');
        }

        $this->appKey = $appKey;
        $this->url = $url;
        $this->client = new Client();
    }

    public function send($number, $text): void
    {
        $result = $this->client->post($this->url . 'send', [
            'form_params' => [
                'sender' => 'AssetsCore',
                'api-key' => $this->appKey,
                'number' => '+' . str_replace(['(', ')', '-'], '', trim($number, '+')),
                'text' => $text
            ],
        ]);
        Log::debug('sms', ['number' => $number, 'result' => $result->getBody(), 'text' => $text]);
    }
}
