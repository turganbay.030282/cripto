<?php

namespace App\AppServices\Sms;

interface SmsSender
{
    public function send($number, $text): void;
}
