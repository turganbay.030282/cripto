<?php

namespace App\Console\Commands;

use App\Api\Cripto\CoinmarketcapDataProvider;
use App\Entity\Coinmarketcap;
use App\Entity\Contract\Contract;
use App\Entity\Setting;
use App\Entity\User;
use App\Services\Contract\ContractService;
use App\Services\Contract\Repositories\EloquentContractRepository;
use Illuminate\Console\Command;

class CoinmarketcapCommand extends Command
{
    protected $signature = 'api:coinmarketcap';

    protected $description = 'Listings Latest';

    private $coinmarketcapDataProvider;
    private $contractRepository;
    private $contractService;

    public function __construct(
        CoinmarketcapDataProvider $coinmarketcapDataProvider,
        EloquentContractRepository $contractRepository,
        ContractService $contractService
    )
    {
        parent::__construct();
        $this->coinmarketcapDataProvider = $coinmarketcapDataProvider;
        $this->contractRepository = $contractRepository;
        $this->contractService = $contractService;
    }

    public function handle()
    {
        try {
            $response = $this->coinmarketcapDataProvider->listingsLatest([
                'limit' => 5000,
                'convert' => 'EUR',
                'cryptocurrency_type' => 'all'
            ]);
            if ($response->status->error_code == 0) {
                $this->updateData($response);

                $total = $response->status->total_count;
                $page = intval($total / 1000);
                if ($page > 1) {
                    for ($i = 2; $i <= $page; $i++) {
                        $response = $this->coinmarketcapDataProvider->listingsLatest([
                            'start' => $i,
                            'convert' => 'EUR',
                            'limit' => 5000,
                            'cryptocurrency_type' => 'all'//"all""coins""tokens"
                        ]);
                        if ($response->status->error_code == 0) {
                            $this->updateData($response);
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
            \Log::channel('coin')->error('Coinmarketcap', [$exception]);
        }

        try {
            User::whereHas('contracts', function ($query) {
                $query->where('type', Contract::TYPE_CREDIT);
            })->chunk(500, function ($users) {
                foreach ($users as $user) {
                    $this->contractService->addPortfolioGraph(
                        $user,
                        $this->contractRepository->portfolioValue($user)
                    );
                }
            });
        } catch (\Exception $exception) {
            \Log::channel('coin')->error('Coinmarketcap addPortfolioGraph', [$exception]);
        }
    }

    private function updateData($response)
    {
        foreach ($response->data as $data) {
            $coint = Coinmarketcap::where('symbol', $data->symbol)->first();

            if (!$coint) {
                Coinmarketcap::create([
                    'name' => $data->name,
                    'symbol' => $data->symbol,
                    'price' => $data->quote->EUR->price,
                    'volume_24h' => $data->quote->EUR->volume_24h,
                    'percent_change_1h' => $data->quote->EUR->percent_change_1h,
                    'percent_change_24h' => $data->quote->EUR->percent_change_24h,
                    'percent_change_7d' => $data->quote->EUR->percent_change_7d
                ]);
            } else {
                $coint->update([
                    'price' => $data->quote->EUR->price,
                    'volume_24h' => $data->quote->EUR->volume_24h,
                    'percent_change_1h' => $data->quote->EUR->percent_change_1h,
                    'percent_change_24h' => $data->quote->EUR->percent_change_24h,
                    'percent_change_7d' => $data->quote->EUR->percent_change_7d
                ]);
            }

            if ($data->symbol == 'DAI') {
                Setting::first()->update([
                    'course_investor' => format_number($coint->price)
                ]);
            }

            if ($coint && !$coint->logo) {
                $result = $this->coinmarketcapDataProvider->info([
                    'symbol' => $coint->symbol
                ]);

                foreach ($result->data as $info) {
                    $coint->update(['logo' => $info->logo]);
                }
            }
        }
    }
}
