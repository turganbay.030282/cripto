<?php

namespace App\Console\Commands;

use App\Api\Cripto\CoinmarketcapDataProvider;
use App\Entity\Coinmarketcap;
use App\Entity\CoinmarketcapOhlcv;
use App\Entity\Handbook\Asset;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CoinmarketcapOhlcvCommand extends Command
{
    protected $signature = 'api:coinmarketcap-ohlcv';

    protected $description = 'Listings Latest';

    private $coinmarketcapDataProvider;

    public function __construct(CoinmarketcapDataProvider $coinmarketcapDataProvider)
    {
        parent::__construct();
        $this->coinmarketcapDataProvider = $coinmarketcapDataProvider;
    }

    public function handle()
    {
        Coinmarketcap::whereIn('id', Asset::pluck('coinmarketcap_id')->all())->chunk(1000, function ($symbols) {
            foreach ($symbols as $symbol) {
                try {
                    $response = $this->coinmarketcapDataProvider->historicalOhlcv([
                        'symbol' => $symbol->symbol,
                        'convert' => 'EUR',
                        'count' => 100,
                        'time_period' => 'hourly',//daily hourly
                        'interval' => 'hourly',//"hourly""daily""weekly""monthly""yearly""1h""2h""3h""4h""6h""12h""1d""2d""3d""7d""14d""15d""30d""60d""90d""365d"
                    ]);
                    if ($response->status->error_code == 0) {
                        $this->updateData($response, $symbol);
                    }
                } catch (\Exception $exception) {
                    \Log::channel('coin')->error('Coinmarketcap', [$exception]);
                }
                sleep(2);
            }
        });
    }

    private function updateData($response, Coinmarketcap $symbol)
    {
        foreach ($response->data->quotes as $quote) {
            try {
                $quoteEUR = $quote->quote->EUR;
                $timestamp = Carbon::parse($quoteEUR->timestamp);
                $cointOhlcv = CoinmarketcapOhlcv::where('coinmarketcap_id', $symbol->id)->where('timestamp', $timestamp)->first();
                if (!$cointOhlcv) {
                    CoinmarketcapOhlcv::create([
                        'coinmarketcap_id' => $symbol->id,
                        'time_open' => Carbon::parse($quote->time_open),
                        'time_close' => Carbon::parse($quote->time_close),
                        'time_high' => Carbon::parse($quote->time_high),
                        'time_low' => Carbon::parse($quote->time_low),
                        'open' => $quoteEUR->open,
                        'high' => $quoteEUR->high,
                        'low' => $quoteEUR->low,
                        'close' => $quoteEUR->close,
                        'volume' => $quoteEUR->volume,
                        'market_cap' => $quoteEUR->market_cap,
                        'timestamp' => $timestamp,
                        'data' => (array)$quote
                    ]);
                }
            } catch (\Exception $exception) {
                \Log::channel('coin')->error('Coinmarketcap', [$exception]);
            }
        }

        $ohlcv_x = [];
        $ohlcv_y = [];

        $ohlcvs = $symbol->ohlcv()->orderBy('timestamp', 'desc')->take(7 * 24)->get();
        if ($ohlcvs) {
            $ohlcvs = $ohlcvs->sortBy('timestamp');
            foreach ($ohlcvs as $ohlcv) {
                $ohlcv_x[] = $ohlcv->time_open->format('d.m');
                $ohlcv_y[] = $ohlcv->high;
            }
        }

        $symbol->update([
            'ohlcv_x' => $ohlcv_x,
            'ohlcv_y' => $ohlcv_y
        ]);
    }
}
