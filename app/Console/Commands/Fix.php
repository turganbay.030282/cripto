<?php

namespace App\Console\Commands;

use App\Entity\Contract\Contract;
use Illuminate\Console\Command;

class Fix extends Command
{
    protected $signature = 'test:fix';

    protected $description = 'test';

    public function handle()
    {
        Contract::deposit()->chunk(200, function ($contracts){
            foreach ($contracts as $contract){
                $index = 1;
                for ($i = $contract->period; $i > 0; $i--) {
                    $contract->investProfits()->create([
                        'percent' => format_number($contract->calcInvestorAmount()),
                        'amount' => format_number($contract->amount + $index * $contract->calcInvestorAmount()),
                        'date' => $contract->created_at->addMonths($index)->format("d.m.Y")
                    ]);
                    $index++;
                }
            }
        });
        die;
        Contract::credit()->chunk(200, function ($contracts){
            foreach ($contracts as $contract){
                $amount = $contract->amount;
                foreach ($contract->payments as $payment){
                    $payment->update(['balance_owed' => format_number($amount - $payment->main_amount)]);
                    $amount -= $payment->main_amount;
                }
            }
        });
    }
}
