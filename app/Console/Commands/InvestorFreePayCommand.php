<?php

namespace App\Console\Commands;

use App\Entity\Contract\Contract;
use App\Entity\Setting;
use App\Helpers\StatusHelper;
use App\Services\User\UserService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InvestorFreePayCommand extends Command
{
    protected $signature = 'investor:free-pay';

    protected $description = 'Investor Free Pay';

    protected $userService;

    public function __construct(
        UserService $userService
    )
    {
        parent::__construct();
        $this->userService = $userService;
    }

    public function handle()
    {
        try {
            Contract::active()
                ->with('investProfits')
                ->whereHas('investProfits', function ($q) {
                    $q->where('is_pay', false);
                    $q->where('date', '>=', Carbon::now()->startOfDay());
                    $q->where('date', '<=', Carbon::now()->endOfDay());
                })
                ->chunk(500, function ($contracts) {
                    foreach ($contracts as $contract) {
                        $investProfit = $contract->investProfits()
                            ->where('is_pay', false)
                            ->where('date', '>=', Carbon::now()->startOfDay())
                            ->where('date', '<=', Carbon::now()->endOfDay())
                            ->first();
                        $client = $contract->client;

                        if ($investProfit) {
                            \DB::transaction(function () use ($investProfit, $client, $contract) {
                                $client->update(['free_active' => ($client->free_active + $investProfit->percent_dai)]);
                                $investProfit->update(['is_pay' => 1]);

                                if (!$contract->investProfits()
                                    ->where('is_pay', false)
                                    ->exists()) {

                                    $balance = $client->balances->where('asset_id', $contract->type_asset_id)->first();

                                    $contract->update(['status' => StatusHelper::STATUS_CLOSE]);

                                    $balance->update([
                                        'amount' => numeric_fmt(
                                            $balance->amount + $contract->getAmountPercentOnCurrentDate() / $contract->typeAsset->coinValue->price
                                        )
                                    ]);
                                }
                            });
                        }
                    }
                });
        } catch (\Exception $exception) {
            \Log::channel('invest_profits')->error('handle', [$exception]);
        }
    }

}
