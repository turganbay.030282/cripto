<?php

namespace App\Console\Commands;

use App\Entity\Contract\Contract;
use App\Entity\Setting;
use App\Entity\Transfer\Transaction;
use App\Entity\User;
use App\Helpers\StatusHelper;
use App\Helpers\TypeUserHelper;
use App\Services\Contract\ContractService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Mail\Mailer;

class PenaltyPaymentCommand extends Command
{
    protected $signature = 'trader:penalty-payment';

    protected $description = 'Trader penalty payment';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $setting = Setting::first();

        Contract::credit()
            ->with('client')
            ->where('status', StatusHelper::STATUS_APPROVE)
            ->whereHas('client', function ($q) {
                $q->active();
            })
            ->whereHas('payments', function ($q) {
                $q->where('status', StatusHelper::STATUS_NEW);
                $q->where('date', '<', Carbon::now()->subDays(10)->startOfDay());
            })
            ->with(['payments' => function ($q) {
                $q->where('status', StatusHelper::STATUS_NEW);
                $q->where('date', '<', Carbon::now()->subDays(10)->startOfDay());
            }])
            ->chunk(200, function ($contracts) use ($setting) {
                foreach ($contracts as $contract) {

                    $client = $contract->client;
                    $payment = $contract->payments->first();

                    $days = $payment->date->diffInDays(Carbon::now()) - 10;

                    if ($days >= 30) {
                        $client->update(['status' => User::STATUS_BLOCK]);
                        if ($days >= 31) {
                            $client = $contract->client;
                            $setting = Setting::firstOrFail();

                            $contract->update(['status' => StatusHelper::STATUS_WAIT_CLOSE]);
                            Transaction::create([
                                'is_sell' => true,
                                'user_id' => $client->id,
                                'credit_payment_id' => $contract->getPayment()->id,
                                'amount' => format_number($contract->getCurrentAmountAssetCost()) -
                                    format_number(
                                        $contract->getCurrentAmountAssetCost() * $setting->commission_trader / 100
                                    ) - $contract->getCurrentContractPaymentAmount(),
                                'type_user' => TypeUserHelper::TYPE_USER_TRADER,
                                'type_transaction' => Transaction::TYPE_TRADER_CONTRACT_DELAYS_SELL,
                                'type' => Transaction::TYPE_REQUEST,
                                'status' => StatusHelper::STATUS_NEW,
                                'coinmarketcap_id' => $contract->typeAsset->coinmarketcap_id,
                            ]);
                        }
                    }

                    $payment->update([
                        'penalty' => format_number($days * $contract->amount * $setting->penalty)
                    ]);
                }
            });
    }
}
