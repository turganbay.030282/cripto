<?php

namespace App\Console\Commands;

use App\Entity\Contract\Contract;
use App\Entity\Tpl\NotifyText;
use App\Helpers\StatusHelper;
use App\Mail\TplNotify;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Mail\Mailer;

class PlanePaymentCommand extends Command
{
    protected $signature = 'trader:plane-payment';

    protected $description = 'Trader plane payment';

    private $mailer;

    public function __construct(Mailer $mailer)
    {
        parent::__construct();

        $this->mailer = $mailer;
    }

    public function handle()
    {
        Contract::credit()->whereHas('payments', function ($q) {
            $q->where('status', StatusHelper::STATUS_NEW);
            $q->whereDate('date', Carbon::now()->subDays(3)->format('Y-m-d'));
        })->chunk(200, function ($contracts) {
            foreach ($contracts as $contract) {
                $notify = NotifyText::find(25);
                $client = $contract->client;

                $this->mailer->to($contract->client)->send(new TplNotify(
                    $notify->{'subject:'.$client->lang},
                    $notify->{'body:' . $client->lang}
                ));
            }
        });
    }

}
