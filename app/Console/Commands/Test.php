<?php

namespace App\Console\Commands;

use App\Entity\Contract\Contract;
use App\Entity\Contract\ContractInvestProfit;
use App\Entity\Setting;
use App\Entity\User;
use App\Services\Statistic\StatisticUserService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class Test extends Command
{
    protected $signature = 'test:test';

    protected $description = 'test';

    private $service;

    public function __construct(StatisticUserService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    public function handle()
    {
        $setting = Setting::first();

        Contract::credit()->active()->chunk(100, function ($contracts) use ($setting) {
            foreach ($contracts as $contract) {
                $index = 1;
                foreach ($contract->payments as $payment) {
                    $payment->update(['balance_owed' => format_number($contract->amount - $index * $contract->amount / $contract->period)]);
                    $index++;
                }
            }
        });

//        ContractInvestProfit::chunk(100, function ($profits) use($setting) {
//            foreach ($profits as $profit){
//                $profit->update(['percent_dai' => format_number($profit->percent / $setting->course_investor)]);
//            }
//        });


//        User::user()->chunk(100, function ($users){
//            foreach ($users as $user){
//                $this->service->register(
//                    $user->created_at->month,
//                    $user->created_at->year,
//                    0
//                );
//                if($user->is_verification){
//                    $this->service->verify(
//                        $user->updated_at->month,
//                        $user->updated_at->year
//                    );
//                }
//            }
//        });
    }
}
