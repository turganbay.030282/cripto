<?php

namespace App\Console\Commands;

use App\Entity\UserVerification;
use App\Services\User\UserService;
use Illuminate\Console\Command;

class UserVerificationCommand extends Command
{
    protected $signature = 'api:user-verification';

    protected $description = 'User Verification';

    private $userService;

    public function __construct(
        UserService $userService
    )
    {
        parent::__construct();
        $this->userService = $userService;
    }

    public function handle()
    {
        try {
            UserVerification::where('status', UserVerification::STATUS_WAIT)
            ->whereNotNull('external_id')
            ->chunk(500, function ($verifications) {
                foreach ($verifications as $verification) {
                    $this->userService->verifyAuto($verification);
                }
            });
        } catch (\Exception $exception) {
            \Log::channel('verify')->error('UserVerificationCommand', [$exception]);
        }
    }

}
