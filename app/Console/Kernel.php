<?php

namespace App\Console;

use App\Console\Commands\InvestorFreePayCommand;
use App\Console\Commands\PenaltyPaymentCommand;
use App\Console\Commands\PlanePaymentCommand;
use App\Console\Commands\Test;
use App\Console\Commands\UserVerificationCommand;
use App\Console\Translation\ExportCommand;
use App\Console\Translation\ImportCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Test::class,
        ImportCommand::class,
        ExportCommand::class,
        UserVerificationCommand::class,
        InvestorFreePayCommand::class,
        PlanePaymentCommand::class,
        PenaltyPaymentCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if (env('APP_ENV') != 'prod') {
            $schedule->command('api:coinmarketcap')->everyFourHours();
            $schedule->command('api:coinmarketcap-ohlcv')->everyFourHours();
        } else {
            $schedule->command('api:coinmarketcap')->hourly();
            $schedule->command('api:coinmarketcap-ohlcv')->hourly();
        }

        $schedule->command('investor:free-pay')->hourly();
        $schedule->command('api:user-verification')->everyMinute();
        $schedule->command('trader:plane-payment')->dailyAt('06:00');
        $schedule->command('trader:penalty-payment')->dailyAt('06:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
