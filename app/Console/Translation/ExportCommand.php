<?php

namespace App\Console\Translation;

use App\Entity\Translation;
use App\Services\Translation\TranslationService;
use Illuminate\Console\Command;

class ExportCommand extends Command
{
    protected $signature = 'translations:from-db';

    protected $description = 'translations export from database';

    private $service;

    public function __construct(TranslationService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    public function handle()
    {
        $this->service->fromDb(Translation::group_front);
    }
}
