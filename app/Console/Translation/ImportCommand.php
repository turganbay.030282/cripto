<?php

namespace App\Console\Translation;

use App\Entity\Translation;
use App\Services\Translation\TranslationService;
use Illuminate\Console\Command;

class ImportCommand extends Command
{
    protected $signature = 'translations:to-db';

    protected $description = 'translations import to database';

    private $service;

    public function __construct(TranslationService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    public function handle()
    {
        $this->service->toDb(Translation::group_front);
    }
}
