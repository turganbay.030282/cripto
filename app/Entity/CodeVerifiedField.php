<?php

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\CodeVerifiedField
 *
 * @property int $id
 * @property int $user_id
 * @property string $verify_token
 * @property string $type
 * @property Carbon $verify_token_expire
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User $client
 * @method static Builder|CodeVerifiedField investorContract()
 * @method static Builder|CodeVerifiedField investorDepositPay()
 * @method static Builder|CodeVerifiedField newModelQuery()
 * @method static Builder|CodeVerifiedField newQuery()
 * @method static Builder|CodeVerifiedField query()
 * @method static Builder|CodeVerifiedField traderContract()
 * @method static Builder|CodeVerifiedField verifyPhone()
 * @method static Builder|CodeVerifiedField whereCreatedAt($value)
 * @method static Builder|CodeVerifiedField whereId($value)
 * @method static Builder|CodeVerifiedField whereType($value)
 * @method static Builder|CodeVerifiedField whereUpdatedAt($value)
 * @method static Builder|CodeVerifiedField whereUserId($value)
 * @method static Builder|CodeVerifiedField whereVerifyToken($value)
 * @method static Builder|CodeVerifiedField whereVerifyTokenExpire($value)
 * @mixin \Eloquent
 * @method static Builder|CodeVerifiedField investorDepositPayOutDai()
 */

class CodeVerifiedField extends Model
{
    CONST TYPE_VERIFY = 'verify';
    CONST TYPE_TRADER_CONTRACT = 'trader_contract';
    CONST TYPE_INVESTOR_CONTRACT = 'investor_contract';
    CONST TYPE_PAYOUT = 'payout';
    CONST TYPE_STAKING_EURO = 'staking_euro';
    CONST TYPE_INVESTOR_DEPOSIT_PAY = 'investor_deposit_pay';
    CONST TYPE_INVESTOR_DEPOSIT_PAY_OUT_DAI = 'investor_deposit_pay_out_dai';
    const TYPE_TRANSFER = 'transfer';//перевод


    protected $table = 'code_verified_fields';

    protected $guarded = ['id'];

    protected $casts = [
        'verify_token_expire' => 'datetime'
    ];

    public function client()
    {
        return $this->belongsTo(User::class,'user_id', 'id');
    }

    public function scopeVerifyPhone(Builder $query)
    {
        return $query->where('type', self::TYPE_VERIFY);
    }

    public function scopeTraderContract(Builder $query)
    {
        return $query->where('type', self::TYPE_TRADER_CONTRACT);
    }

    public function scopeInvestorContract(Builder $query)
    {
        return $query->where('type', self::TYPE_INVESTOR_CONTRACT);
    }

    public function scopePayout(Builder $query)
    {
        return $query->where('type', self::TYPE_PAYOUT);
    }

    public function scopeInvestorDepositPay(Builder $query)
    {
        return $query->where('type', self::TYPE_INVESTOR_DEPOSIT_PAY);
    }

    public function scopeInvestorDepositPayOutDai(Builder $query)
    {
        return $query->where('type', self::TYPE_INVESTOR_DEPOSIT_PAY_OUT_DAI);
    }
}
