<?php

namespace App\Entity;

use App\Entity\Handbook\Asset;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Coinmarketcap
 *
 * @property int $id
 * @property string $name
 * @property string $logo
 * @property string $symbol
 * @property double $price
 * @property double $volume_24h
 * @property double $percent_change_1h
 * @property double $percent_change_24h
 * @property double $percent_change_7d
 * @property string $ohlcv_x
 * @property string $ohlcv_y
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CoinmarketcapOhlcv[] $ohlcv
 * @property Asset[] $assets
 * @property-read int|null $assets_count
 * @property-read int|null $ohlcv_count
 * @method static Builder|Coinmarketcap newModelQuery()
 * @method static Builder|Coinmarketcap newQuery()
 * @method static Builder|Coinmarketcap query()
 * @method static Builder|Coinmarketcap sortable($defaultParameters = null)
 * @method static Builder|Coinmarketcap whereCreatedAt($value)
 * @method static Builder|Coinmarketcap whereId($value)
 * @method static Builder|Coinmarketcap whereName($value)
 * @method static Builder|Coinmarketcap whereOhlcvX($value)
 * @method static Builder|Coinmarketcap whereOhlcvY($value)
 * @method static Builder|Coinmarketcap wherePercentChange1h($value)
 * @method static Builder|Coinmarketcap wherePercentChange24h($value)
 * @method static Builder|Coinmarketcap wherePercentChange7d($value)
 * @method static Builder|Coinmarketcap wherePrice($value)
 * @method static Builder|Coinmarketcap whereSymbol($value)
 * @method static Builder|Coinmarketcap whereUpdatedAt($value)
 * @method static Builder|Coinmarketcap whereVolume24h($value)
 * @mixin \Eloquent
 * @method static Builder|Coinmarketcap whereLogo($value)
 */

class Coinmarketcap extends Model
{
    use Sortable;

    const TYPE_CREDIT = 'credit';
    const TYPE_DEPOSIT = 'deposit';

    protected $table = 'coinmarketcaps';

    protected $guarded = ['id'];

    public function ohlcv()
    {
        return $this->hasMany(CoinmarketcapOhlcv::class, 'coinmarketcap_id', 'id');
    }

    public function assets()
    {
        return $this->hasMany(Asset::class, 'coinmarketcap_id', 'id');
    }
}
