<?php

namespace App\Entity;

use App\Entity\Handbook\Asset;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\CoinmarketcapOhlcv
 *
 * @property int $id
 * @property int $coinmarketcap_id
 * @property double $open
 * @property double $high
 * @property double $low
 * @property double $close
 * @property double $volume
 * @property double $market_cap
 * @property array $data
 * @property Carbon $time_open
 * @property Carbon $time_close
 * @property Carbon $time_high
 * @property Carbon $time_low
 * @property Carbon $timestamp
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @method static Builder|CoinmarketcapOhlcv newModelQuery()
 * @method static Builder|CoinmarketcapOhlcv newQuery()
 * @method static Builder|CoinmarketcapOhlcv query()
 * @method static Builder|CoinmarketcapOhlcv whereClose($value)
 * @method static Builder|CoinmarketcapOhlcv whereCoinmarketcapId($value)
 * @method static Builder|CoinmarketcapOhlcv whereCreatedAt($value)
 * @method static Builder|CoinmarketcapOhlcv whereData($value)
 * @method static Builder|CoinmarketcapOhlcv whereHigh($value)
 * @method static Builder|CoinmarketcapOhlcv whereId($value)
 * @method static Builder|CoinmarketcapOhlcv whereLow($value)
 * @method static Builder|CoinmarketcapOhlcv whereMarketCap($value)
 * @method static Builder|CoinmarketcapOhlcv whereOpen($value)
 * @method static Builder|CoinmarketcapOhlcv whereTimeClose($value)
 * @method static Builder|CoinmarketcapOhlcv whereTimeHigh($value)
 * @method static Builder|CoinmarketcapOhlcv whereTimeLow($value)
 * @method static Builder|CoinmarketcapOhlcv whereTimeOpen($value)
 * @method static Builder|CoinmarketcapOhlcv whereTimestamp($value)
 * @method static Builder|CoinmarketcapOhlcv whereUpdatedAt($value)
 * @method static Builder|CoinmarketcapOhlcv whereVolume($value)
 * @mixin \Eloquent
 */

class CoinmarketcapOhlcv extends Model
{
    protected $table = 'coinmarketcap_ohlcvs';

    protected $guarded = ['id'];

    protected $casts = [
        'data' => 'array',
        'time_open' => 'datetime',
        'time_close' => 'datetime',
        'time_high' => 'datetime',
        'time_low' => 'datetime',
        'timestamp' => 'datetime',
    ];
}
