<?php

namespace App\Entity\Content;

use App\Traits\CropImageTrait;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\BlockPackage
 *
 * @property int $id
 * @property string $photo
 * @property int $type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Entity\Content\BlockPackageTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Content\BlockPackageTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|BlockPackage onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage query()
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage translated()
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackage withTranslation()
 * @method static \Illuminate\Database\Query\Builder|BlockPackage withTrashed()
 * @method static \Illuminate\Database\Query\Builder|BlockPackage withoutTrashed()
 * @mixin \Eloquent
 */
class BlockPackage extends Model
{
    use CropImageTrait, Sortable, SoftDeletes, Translatable;

    protected $table = 'block_packages';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'type',
        'created_at',
        'updated_at',
    ];

    public $translatedAttributes = [
        'name',
        'desc',
        'price',
    ];
}
