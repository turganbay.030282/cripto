<?php

namespace App\Entity\Content;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\BlockPackageTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $name
 * @property string $desc
 * @property string $price
 * @property int $block_package_id
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackageTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackageTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackageTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackageTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackageTranslation whereBlockPackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackageTranslation whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackageTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackageTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackageTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlockPackageTranslation wherePrice($value)
 * @mixin \Eloquent
 */
class BlockPackageTranslation extends Model
{
    use Status;

    protected $table = 'block_package_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'name',
        'desc',
        'price',
    ];
}
