<?php

namespace App\Entity\Content\Blog;

use App\Entity\Content\Blog\BlogItem;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\Blog\BlogCategory
 *
 * @property int $id
 * @property string $slug
 * @property BlogItem[] $items
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read int|null $items_count
 * @property-read \App\Entity\Content\Blog\BlogCategoryTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Content\Blog\BlogCategoryTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|BlogCategory onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory translated()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory withTranslation()
 * @method static \Illuminate\Database\Query\Builder|BlogCategory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|BlogCategory withoutTrashed()
 * @mixin \Eloquent
 */
class BlogCategory extends Model
{
    use Sortable, SoftDeletes, Translatable;

    protected $table = 'blog_categories';

    protected $guarded = ['id'];

    public $translatedAttributes = [
        'title',
    ];

    public function items()
    {
        return $this->belongsToMany(BlogItem::class, 'blog_categories_items', 'blog_category_id', 'blog_item_id');
    }
}
