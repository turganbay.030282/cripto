<?php

namespace App\Entity\Content\Blog;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Content\Blog\BlogCategoryTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $title
 * @property int $blog_category_id
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategoryTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategoryTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategoryTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategoryTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategoryTranslation whereBlogCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategoryTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategoryTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategoryTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class BlogCategoryTranslation extends Model
{
    use Status;

    protected $table = 'blog_category_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'title',
    ];
}
