<?php

namespace App\Entity\Content\Blog;

use App\Entity\Content\Blog\BlogCategory;
use App\Entity\User;
use App\Traits\CropImageTrait;
use App\Traits\Seo;
use App\Traits\Status;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\Blog\BlogItem
 *
 * @property int $id
 * @property int $publish
 * @property string $slug
 * @property string $photo
 * @property int $count_view
 * @property int $count_like
 * @property int $author_id
 * @property User $author
 * @property BlogCategory[] $categories
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read int|null $categories_count
 * @property-read mixed $status_name
 * @property-read \App\Entity\Content\Blog\BlogItemTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Content\Blog\BlogItemTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|BlogItem onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem publish()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem translated()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem whereCountLike($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem whereCountView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem wherePublish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItem withTranslation()
 * @method static \Illuminate\Database\Query\Builder|BlogItem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|BlogItem withoutTrashed()
 * @mixin \Eloquent
 */
class BlogItem extends Model
{
    use CropImageTrait, Status, Seo, Sortable, SoftDeletes, Translatable;

    protected $table = 'blog_items';

    protected $guarded = ['id'];

    public $translatedAttributes = [
        'title',
        'description',
        'content',
        'meta_title',
        'meta_desc',
    ];

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    public function categories()
    {
        return $this->belongsToMany(BlogCategory::class, 'blog_categories_items', 'blog_item_id', 'blog_category_id');
    }
}
