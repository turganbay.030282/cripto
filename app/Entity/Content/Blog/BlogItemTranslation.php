<?php

namespace App\Entity\Content\Blog;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\Blog\BlogItemTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $meta_title
 * @property string $meta_desc
 * @property int $blog_item_id
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItemTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItemTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItemTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItemTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItemTranslation whereBlogItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItemTranslation whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItemTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItemTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItemTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItemTranslation whereMetaDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItemTranslation whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogItemTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class BlogItemTranslation extends Model
{
    use Status;

    protected $table = 'blog_item_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'title',
        'description',
        'content',
        'meta_title',
        'meta_desc',
    ];
}
