<?php

namespace App\Entity\Content;

use App\Traits\CropImageTrait;
use App\Traits\FileTrait;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\Certificate
 *
 * @property int $id
 * @property string $photo
 * @property string $file
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Entity\Content\CertificateTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Content\CertificateTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|Certificate onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate query()
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate translated()
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate withTranslation()
 * @method static \Illuminate\Database\Query\Builder|Certificate withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Certificate withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|Certificate whereFile($value)
 */
class Certificate extends Model
{
    use CropImageTrait, FileTrait, Sortable, SoftDeletes, Translatable;

    protected $table = 'certificates';

    protected $guarded = ['id'];

    public $translatedAttributes = [
        'title',
        'description',
    ];
}
