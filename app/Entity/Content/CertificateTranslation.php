<?php

namespace App\Entity\Content;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\CertificateTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $title
 * @property string $description
 * @property int $certificate_id
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateTranslation whereCertificateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class CertificateTranslation extends Model
{
    use Status;

    protected $table = 'certificate_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'title',
        'description',
    ];
}
