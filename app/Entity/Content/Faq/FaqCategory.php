<?php

namespace App\Entity\Content\Faq;

use App\Entity\Content\Faq\FaqItem;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\Faq\FaqCategory
 *
 * @property int $id
 * @property FaqItem[] $items
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read int|null $items_count
 * @property-read \App\Entity\Content\Faq\FaqCategoryTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Content\Faq\FaqCategoryTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|FaqCategory onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory translated()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory withTranslation()
 * @method static \Illuminate\Database\Query\Builder|FaqCategory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|FaqCategory withoutTrashed()
 * @mixin \Eloquent
 * @property bool $is_convert
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategory whereIsConvert($value)
 */
class FaqCategory extends Model
{
    use Sortable, SoftDeletes, Translatable;

    protected $table = 'faq_categories';

    protected $guarded = ['id'];

    public $translatedAttributes = [
        'title',
        'description',
    ];

    protected $casts = [
        'is_convert' => 'boolean'
    ];

    public function items()
    {
        return $this->hasMany(FaqItem::class, 'faq_category_id', 'id');
    }
}
