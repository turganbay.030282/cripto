<?php

namespace App\Entity\Content\Faq;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Content\Faq\FaqCategoryTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $title
 * @property string $description
 * @property int $faq_category_id
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategoryTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategoryTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategoryTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategoryTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategoryTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategoryTranslation whereFaqCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategoryTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategoryTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqCategoryTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class FaqCategoryTranslation extends Model
{
    use Status;

    protected $table = 'faq_category_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'title',
        'description',
    ];
}
