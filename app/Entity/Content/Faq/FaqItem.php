<?php

namespace App\Entity\Content\Faq;

use App\Entity\Content\Faq\FaqCategory;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\Faq\FaqItem
 *
 * @property int $id
 * @property int $faq_category_id
 * @property FaqCategory $category
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Entity\Content\Faq\FaqItemTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Content\Faq\FaqItemTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|FaqItem onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem translated()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereFaqCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem withTranslation()
 * @method static \Illuminate\Database\Query\Builder|FaqItem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|FaqItem withoutTrashed()
 * @mixin \Eloquent
 */
class FaqItem extends Model
{
    use Sortable, SoftDeletes, Translatable;

    protected $table = 'faq_items';

    protected $guarded = ['id'];

    public $translatedAttributes = [
        'title',
        'description',
    ];

    public function category()
    {
        return $this->belongsTo(FaqCategory::class, 'faq_category_id', 'id');
    }
}
