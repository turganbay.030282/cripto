<?php

namespace App\Entity\Content\Faq;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\Faq\FaqItemTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $title
 * @property string $description
 * @property int $faq_item_id
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItemTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItemTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItemTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItemTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItemTranslation sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItemTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItemTranslation whereFaqItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItemTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItemTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItemTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class FaqItemTranslation extends Model
{
    use Sortable, Status;

    protected $table = 'faq_item_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'title',
        'description',
    ];

    public $sortable = [
        'locale',
        'title',
        'description',
    ];
}
