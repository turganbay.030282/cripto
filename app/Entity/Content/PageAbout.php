<?php

namespace App\Entity\Content;

use App\Traits\CropImageTrait;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Content\PageAbout
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string|null $benefit_b_icon
 * @property string|null $benefit_b_icon2
 * @property string|null $benefit_b_icon3
 * @property string|null $benefit_b_icon4
 * @property string|null $benefit_b_icon5
 * @property string|null $benefit_b_icon6
 * @property-read \App\Entity\Content\PageAboutTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Content\PageAboutTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout query()
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout translated()
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout whereBenefitBIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout whereBenefitBIcon2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout whereBenefitBIcon3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout whereBenefitBIcon4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout whereBenefitBIcon5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout whereBenefitBIcon6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAbout withTranslation()
 * @mixin \Eloquent
 */
class PageAbout extends Model
{
    use CropImageTrait, Translatable;

    protected $table = 'page_about';

    protected $guarded = ['id'];

    public $translatedAttributes = [
        'title',
        'desc',
        'btn_text',
        'benefit_title',
        'benefit_sub_title',
        'benefit_b_title',
        'benefit_b_desc',
        'benefit_b_title2',
        'benefit_b_desc2',
        'benefit_b_title3',
        'benefit_b_desc3',
        'benefit_b_title4',
        'benefit_b_desc4',
        'benefit_b_title5',
        'benefit_b_desc5',
        'benefit_b_title6',
        'benefit_b_desc6',
        'priority_title',
        'priority_sub_title',
        'priority_b_title',
        'priority_b_desc',
        'priority_b_title2',
        'priority_b_desc2',
        'priority_b_title3',
        'priority_b_desc3',
        'priority_b_title4',
        'priority_b_desc4',
    ];
}
