<?php

namespace App\Entity\Content;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\PageAboutTranslation
 *
 * @property int $id
 * @property string $title
 * @property string $desc
 * @property string $btn_text
 * @property string $benefit_title
 * @property string $benefit_sub_title
 * @property string $benefit_b_icon
 * @property string $benefit_b_title
 * @property string $benefit_b_desc
 * @property string $benefit_b_icon2
 * @property string $benefit_b_title2
 * @property string $benefit_b_desc2
 * @property string $benefit_b_icon3
 * @property string $benefit_b_title3
 * @property string $benefit_b_desc3
 * @property string $benefit_b_icon4
 * @property string $benefit_b_title4
 * @property string $benefit_b_desc4
 * @property string $benefit_b_icon5
 * @property string $benefit_b_title5
 * @property string $benefit_b_desc5
 * @property string $benefit_b_icon6
 * @property string $benefit_b_title6
 * @property string $benefit_b_desc6
 * @property string $priority_title
 * @property string $priority_sub_title
 * @property string $priority_b_title
 * @property string $priority_b_desc
 * @property string $priority_b_title2
 * @property string $priority_b_desc2
 * @property string $priority_b_title3
 * @property string $priority_b_desc3
 * @property string $priority_b_title4
 * @property string $priority_b_desc4
 * @property int|null $page_about_id
 * @property string $locale
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBDesc2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBDesc3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBDesc4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBDesc5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBDesc6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBIcon2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBIcon3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBIcon4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBIcon5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBIcon6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBTitle2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBTitle3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBTitle4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBTitle5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitBTitle6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitSubTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBenefitTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereBtnText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation wherePageAboutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation wherePriorityBDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation wherePriorityBDesc2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation wherePriorityBDesc3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation wherePriorityBDesc4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation wherePriorityBTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation wherePriorityBTitle2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation wherePriorityBTitle3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation wherePriorityBTitle4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation wherePrioritySubTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation wherePriorityTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageAboutTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class PageAboutTranslation extends Model
{
    use Status;

    protected $table = 'page_about_translations';

    public $timestamps = false;

    public $guarded = ['id'];
}
