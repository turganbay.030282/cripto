<?php

namespace App\Entity\Content;

use App\Traits\CropImageTrait;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Content\PageBonus
 *
 * @property int $id
 * @property string|null $photo1
 * @property string|null $photo2
 * @property string|null $photo3
 * @property string|null $photo4
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entity\Content\PageBonusTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Content\PageBonusTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus query()
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus translated()
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus wherePhoto1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus wherePhoto2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus wherePhoto3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus wherePhoto4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonus withTranslation()
 * @mixin \Eloquent
 */
class PageBonus extends Model implements TranslatableContract
{
    use CropImageTrait, Translatable;

    protected $table = 'page_bonus';

    protected $fillable = [
        'photo1'
    ];

    public $translatedAttributes = [
        'title',
        'subtitle',
        'title1',
        'content1',
        'title2',
        'content2',
        'title3',
        'content3',
    ];
}
