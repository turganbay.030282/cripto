<?php

namespace App\Entity\Content;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Content\PageBonusTranslation
 *
 * @property int $id
 * @property int|null $page_bonus_id
 * @property string $locale
 * @property string|null $title
 * @property string|null $subtitle
 * @property string|null $title1
 * @property string|null $content1
 * @property string|null $title2
 * @property string|null $content2
 * @property string|null $title3
 * @property string|null $content3
 * @property string|null $meta_title
 * @property string|null $meta_desc
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation whereContent1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation whereContent2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation whereContent3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation whereMetaDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation wherePageBonusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation whereTitle1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation whereTitle2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageBonusTranslation whereTitle3($value)
 * @mixin \Eloquent
 */
class PageBonusTranslation extends Model
{
    protected $table = 'page_bonus_translations';

    public $timestamps = false;

    public $guarded = ['id'];
}
