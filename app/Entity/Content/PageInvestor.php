<?php

namespace App\Entity\Content;

use App\Traits\CropImageTrait;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Content\PageInvestor
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string|null $about_b_icon
 * @property string|null $about_b_icon2
 * @property string|null $about_b_icon3
 * @property-read \App\Entity\Content\PageInvestorTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Content\PageInvestorTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor query()
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor translated()
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor whereAboutBIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor whereAboutBIcon2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor whereAboutBIcon3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestor withTranslation()
 * @mixin \Eloquent
 */
class PageInvestor extends Model
{
    use CropImageTrait, Translatable;

    protected $table = 'page_investor';

    protected $guarded = ['id'];

    public $translatedAttributes = [
        'title',
        'desc',
        'btn_text',
        'about_title',
        'about_sub_title',
        'about_b_title',
        'about_b_desc',
        'about_b_title2',
        'about_b_desc2',
        'about_b_title3',
        'about_b_desc3',
        'benefit_title',
        'benefit_b_title',
        'benefit_b_desc',
        'benefit_b_title2',
        'benefit_b_desc2',
        'benefit_b_title3',
        'benefit_b_desc3',
        'benefit_b_title4',
        'benefit_b_desc4',
        'benefit_b_title5',
        'benefit_b_desc5',
        'benefit_b_title6',
        'benefit_b_desc6',
    ];
}
