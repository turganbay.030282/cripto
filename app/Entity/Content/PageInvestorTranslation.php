<?php

namespace App\Entity\Content;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\PageInvestorTranslation
 *
 * @property int $id
 * @property string $title
 * @property string $desc
 * @property string $btn_text
 * @property string $about_title
 * @property string $about_sub_title
 * @property string $about_b_icon
 * @property string $about_b_title
 * @property string $about_b_desc
 * @property string $about_b_icon2
 * @property string $about_b_title2
 * @property string $about_b_desc2
 * @property string $about_b_icon3
 * @property string $about_b_title3
 * @property string $about_b_desc3
 * @property string $benefit_title
 * @property string $benefit_b_title
 * @property string $benefit_b_desc
 * @property string $benefit_b_title2
 * @property string $benefit_b_desc2
 * @property string $benefit_b_title3
 * @property string $benefit_b_desc3
 * @property string $benefit_b_title4
 * @property string $benefit_b_desc4
 * @property string $benefit_b_title5
 * @property string $benefit_b_desc5
 * @property string $benefit_b_title6
 * @property string $benefit_b_desc6
 * @property int|null $page_investor_id
 * @property string $locale
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereAboutBDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereAboutBDesc2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereAboutBDesc3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereAboutBTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereAboutBTitle2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereAboutBTitle3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereAboutSubTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereAboutTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBenefitBDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBenefitBDesc2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBenefitBDesc3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBenefitBDesc4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBenefitBDesc5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBenefitBDesc6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBenefitBTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBenefitBTitle2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBenefitBTitle3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBenefitBTitle4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBenefitBTitle5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBenefitBTitle6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBenefitTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereBtnText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation wherePageInvestorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageInvestorTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class PageInvestorTranslation extends Model
{
    use Status;

    protected $table = 'page_investor_translations';

    public $timestamps = false;

    public $fillable = [
        'title',
        'desc',
        'btn_text',
        'about_title',
        'about_sub_title',
        'about_b_title',
        'about_b_desc',
        'about_b_title2',
        'about_b_desc2',
        'about_b_title3',
        'about_b_desc3',
        'benefit_title',
        'benefit_b_title',
        'benefit_b_desc',
        'benefit_b_title2',
        'benefit_b_desc2',
        'benefit_b_title3',
        'benefit_b_desc3',
        'benefit_b_title4',
        'benefit_b_desc4',
        'benefit_b_title5',
        'benefit_b_desc5',
        'benefit_b_title6',
        'benefit_b_desc6',
    ];
}
