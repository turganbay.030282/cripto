<?php

namespace App\Entity\Content;

use App\Traits\CropImageTrait;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Content\PageMain
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read \App\Entity\Content\PageMainTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Content\PageMainTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain query()
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain translated()
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMain withTranslation()
 * @mixin \Eloquent
 */
class PageMain extends Model
{
    use CropImageTrait, Translatable;

    protected $table = 'page_main';

    protected $guarded = ['id'];

    public $translatedAttributes = [
        'title',
        'sub_title',
        'title_inv',
        'sub_title_inv',
        'title_offer_tr',
        'title_offer_inv',
        'desc_offer_tr',
        'desc_offer_inv',
        'offer_tr_title',
        'offer_tr_sub_title',
        'offer_tr_desc',
        'offer_inv_title',
        'offer_inv_sub_title',
        'offer_inv_desc',
        'offer_tr_btn',
        'offer_inv_btn',
    ];
}
