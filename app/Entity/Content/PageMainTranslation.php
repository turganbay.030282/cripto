<?php

namespace App\Entity\Content;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\PageMainTranslation
 *
 * @property int $id
 * @property string $title
 * @property string $sub_title
 * @property string $title_inv
 * @property string $sub_title_inv
 * @property string $title_offer_tr
 * @property string $title_offer_inv
 * @property string $desc_offer_tr
 * @property string $desc_offer_inv
 * @property string $offer_tr_title
 * @property string $offer_tr_sub_title
 * @property string $offer_tr_desc
 * @property string $offer_inv_title
 * @property string $offer_inv_sub_title
 * @property string $offer_inv_desc
 * @property string $offer_tr_btn
 * @property string $offer_inv_btn
 * @property int|null $page_main_id
 * @property string $locale
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereDescOfferInv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereDescOfferTr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereOfferInvBtn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereOfferInvDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereOfferInvSubTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereOfferInvTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereOfferTrBtn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereOfferTrDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereOfferTrSubTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereOfferTrTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation wherePageMainId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereSubTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereSubTitleInv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereTitleInv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereTitleOfferInv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageMainTranslation whereTitleOfferTr($value)
 * @mixin \Eloquent
 */
class PageMainTranslation extends Model
{
    use Status;

    protected $table = 'page_main_translations';

    public $timestamps = false;

    public $guarded = ['id'];
}
