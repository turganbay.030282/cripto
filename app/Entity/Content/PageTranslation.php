<?php

namespace App\Entity\Content;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Content\PageTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $title
 * @property string $content
 * @property string $meta_title
 * @property string $meta_desc
 * @property int|null $page_id
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|PageTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|PageTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|PageTranslation whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageTranslation whereMetaDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageTranslation whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageTranslation wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class PageTranslation extends Model
{
    use Status;

    protected $table = 'page_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'title',
        'content',
        'meta_title',
        'meta_desc',
    ];
}
