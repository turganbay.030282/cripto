<?php

namespace App\Entity\Contract;

use App\Entity\Handbook\Asset;
use App\Entity\Setting;
use App\Entity\Transfer\Transaction;
use App\Entity\User;
use App\Helpers\NumberHelper;
use App\Helpers\StatusHelper;
use App\Traits\CropImageTrait;
use App\Traits\StatusTransaction;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Contract\Contract
 *
 * @property int $id
 * @property int $user_id
 * @property int $type_asset_id
 * @property string $type
 * @property string $number
 * @property double $amount
 * @property double $amount_asset
 * @property int $period
 * @property int $rate
 * @property string $document
 * @property string $pay_ticket
 * @property string $pay_ticket_out
 * @property string $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User $client
 * @property Asset $typeAsset
 * @property ContractPayment[] $payments
 * @property ContractPayment $firstPayment
 * @property ContractInvestProfit[] $investProfits
 * @property-read User $contractFreeAsset
 * @property-read mixed $status_name
 * @property-read mixed $status_name_html
 * @property-read int|null $invest_profits_count
 * @property-read int|null $payments_count
 * @method static Builder|Contract active()
 * @method static Builder|Contract approve()
 * @method static Builder|Contract close()
 * @method static Builder|Contract credit()
 * @method static Builder|Contract deposit()
 * @method static Builder|Contract new()
 * @method static Builder|Contract newModelQuery()
 * @method static Builder|Contract newQuery()
 * @method static Builder|Contract notApprove()
 * @method static Builder|Contract query()
 * @method static Builder|Contract sortable($defaultParameters = null)
 * @method static Builder|Contract waitClose()
 * @method static Builder|Contract whereAmount($value)
 * @method static Builder|Contract whereAmountAsset($value)
 * @method static Builder|Contract whereCreatedAt($value)
 * @method static Builder|Contract whereDocument($value)
 * @method static Builder|Contract whereId($value)
 * @method static Builder|Contract whereNumber($value)
 * @method static Builder|Contract wherePayTicket($value)
 * @method static Builder|Contract wherePayTicketOut($value)
 * @method static Builder|Contract wherePeriod($value)
 * @method static Builder|Contract whereRate($value)
 * @method static Builder|Contract whereStatus($value)
 * @method static Builder|Contract whereType($value)
 * @method static Builder|Contract whereTypeAssetId($value)
 * @method static Builder|Contract whereUpdatedAt($value)
 * @method static Builder|Contract whereUserId($value)
 * @mixin \Eloquent
 * @property-read Transaction $depositTransaction
 * @property-read mixed $bonus_amount
 * @property-read mixed $bonus_amount_partner
 * @property-read mixed $bonus_type
 * @property-read mixed $credit_bonus_amount
 * @property-read mixed $deposit_bonus_amount
 * @property string|null $course
 * @method static Builder|Contract whereCourse($value)
 */
class Contract extends Model
{
    use Sortable, CropImageTrait, StatusTransaction;

    const TYPE_CREDIT = 'credit';
    const TYPE_DEPOSIT = 'deposit';

    protected $table = 'contracts';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'type',
        'user_id',
        'type_asset_id',
        'number',
        'amount',
        'period',
        'rate',
        'course',
        'amount_asset',
        'status',
        'created_at',
        'updated_at',
    ];

    public function isContractTransactionApprove()
    {
        if ($this->isTypeDeposit() && $this->isApprove()) {
            return true;
        }

        if ($this->isTypeCredit()) {
            return $this->payments()->where(
                'status', StatusHelper::STATUS_APPROVE
            )->exists();
        }

        return false;
    }

    public function countLastPayment()
    {
        return $this->payments()->where('status', '<>',
            StatusHelper::STATUS_APPROVE)->count();
    }

    public function getBonusAmountPartnerAttribute()
    {
        $setting = Setting::first();

        if ($this->isTypeCredit()) {
            return format_number(($this->amount * $setting->percent_ref_credit) / 100);
        }
        return format_number(($this->amount * $setting->percent_ref_deposit) / 100);
    }

    public function getBonusAmountAttribute()
    {
        return $this->type == self::TYPE_CREDIT ? $this->credit_bonus_amount : $this->deposit_bonus_amount;
    }

    public function getBonusTypeAttribute()
    {
        return $this->type == self::TYPE_CREDIT ? 'скидка' : 'бонус';
    }

    public function getCreditBonusAmountAttribute()
    {
        $percent = format_number(format_number($this->calcTraderAmount()) - format_number($this->amount / $this->period));
        return format_number($percent * $this->period / $this->rate);
    }

    public function getDepositBonusAmountAttribute()
    {
        return format_number($this->calcInvestorAmount() * $this->period / $this->rate);
    }

    public function getCurrentAmountAssetCost()
    {
        return format_number($this->amount_asset * $this->typeAsset->coinValue->price);
    }

    public function getCurrentContractPaymentAmount()
    {
        $currentContractPayment = $this->getLastContractPayment();
        if ($currentContractPayment) {
            return $currentContractPayment->balance_owed;
        }
        return $this->amount;
    }

    public function getLastContractPayment(): ?ContractPayment
    {
        return $this->payments->where('status', StatusHelper::STATUS_APPROVE)->first();
    }

    public function getCurrentContractPayment(): ?ContractPayment
    {
        return $this->payments->where('status', StatusHelper::STATUS_NEW)->first();
    }

    public function calcTraderAmount()
    {
        return NumberHelper::calcTraderAnnuityFirstPayment($this->amount, $this->period, $this->rate);
    }

    public function calcInvestorAmount()
    {
        return NumberHelper::calcInvestorFirstPayment($this->amount, $this->rate);
    }

    public function getNextDatePayment()
    {
        $payment = $this->payments->where('status', StatusHelper::STATUS_NEW)->first();
        if ($payment) {
            return $payment->date->format('d.m.Y');
        }
        return '';
    }

    public function getNextDatePaymentCarbon()
    {
        $payment = $this->payments->where('status', StatusHelper::STATUS_NEW)->first();
        if ($payment) {
            return $payment->date->endOfDay();
        }
        return '';
    }

    public function getPayment()
    {
        $payment = $this->payments->where('status', StatusHelper::STATUS_NEW)->first();
        if ($payment) {
            return $payment;
        }
        return null;
    }

    public function getAmountPercentOnCurrentDate()
    {
        $investProfits = $this->investProfits;
        if ($investProfits->isNotEmpty()) {
            foreach ($investProfits as $investProfit) {
                if (Carbon::now() >= $investProfit->date && Carbon::now() <= $investProfit->date->copy()->addMonths(1)) {
                    return $investProfit->amount;
                }
            }
        }
        return $this->amount;
    }

    public function getAmountPercentOnCurrentDateDai()
    {
        $amount = $this->amount;
        $investProfits = $this->investProfits;

        if ($investProfits->isNotEmpty()) {
            foreach ($investProfits as $investProfit) {
                if (Carbon::now() >= $investProfit->date && Carbon::now() <= $investProfit->date->copy()->addMonths(1)) {
                    $amount = $investProfit->amount;
                }
            }
        }

        return $this->amount_asset > 0 ? numeric_fmt($amount / numeric_fmt($this->amount / $this->amount_asset)) : 0;
    }

    public function isTypeDeposit()
    {
        return $this->type == self::TYPE_DEPOSIT;
    }

    public function isTypeCredit()
    {
        return $this->type == self::TYPE_CREDIT;
    }

    public function client()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(ContractPayment::class, 'contract_id', 'id')->orderBy('id', 'asc');
    }

    public function firstPayment()
    {
        return $this->hasOne(ContractPayment::class, 'contract_id', 'id')->oldest();
    }

    public function investProfits()
    {
        return $this->hasMany(ContractInvestProfit::class, 'contract_id', 'id')->orderBy('id', 'asc');
    }

    public function typeAsset()
    {
        return $this->belongsTo(Asset::class, 'type_asset_id', 'id');
    }

    public function contractFreeAsset()
    {
        return $this->belongsTo(User::class, 'id', 'contract_id');
    }

    public function depositTransaction()
    {
        return $this->belongsTo(Transaction::class, 'id', 'deposit_payment_id');
    }

    public function scopeDeposit(Builder $query)
    {
        return $query->where('type', self::TYPE_DEPOSIT);
    }

    public function scopeCredit(Builder $query)
    {
        return $query->where('type', self::TYPE_CREDIT);
    }
}
