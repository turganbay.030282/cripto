<?php

namespace App\Entity\Contract;

use App\Entity\Coinmarketcap;
use App\Entity\Handbook\Asset;
use App\Entity\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Contract\ContractFreeAsset
 *
 * @property int $id
 * @property int $type_asset_id
 * @property int $user_id
 * @property int $contract_id
 * @property double $amount_asset
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read \App\Entity\Contract\Contract|null $contract
 * @property-read Asset|null $typeAsset
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|ContractFreeAsset newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractFreeAsset newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractFreeAsset query()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractFreeAsset sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractFreeAsset whereAmountAsset($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractFreeAsset whereContractId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractFreeAsset whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractFreeAsset whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractFreeAsset whereTypeAssetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractFreeAsset whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractFreeAsset whereUserId($value)
 * @mixin \Eloquent
 * @property-read Coinmarketcap|null $coinmarketcap
 */
class ContractFreeAsset extends Model
{
    use Sortable;

    protected $table = 'contract_free_assets';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'type_asset_id',
        'user_id',
        'contract_id',
        'amount_asset',
        'created_at',
        'updated_at',
    ];

    public function contract()
    {
        return $this->belongsTo(Contract::class, 'contract_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function typeAsset()
    {
        return $this->belongsTo(Asset::class, 'type_asset_id', 'id');
    }

    public function coinmarketcap()
    {
        return $this->hasOneThrough(
            Coinmarketcap::class,
            Asset::class,
            'id',
            'id',
            'type_asset_id',
            'coinmarketcap_id'
        );
    }

}
