<?php

namespace App\Entity\Contract;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Contract\ContractInvestProfit
 *
 * @property int $id
 * @property int $contract_id
 * @property double $percent
 * @property double $amount
 * @property double $amount_dai
 * @property Carbon $date
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property float $percent_dai
 * @property bool $is_pay
 * @method static \Illuminate\Database\Eloquent\Builder|ContractInvestProfit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractInvestProfit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractInvestProfit query()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractInvestProfit sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractInvestProfit whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractInvestProfit whereContractId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractInvestProfit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractInvestProfit whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractInvestProfit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractInvestProfit whereIsPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractInvestProfit wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractInvestProfit wherePercentDai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractInvestProfit whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContractInvestProfit extends Model
{
    use Sortable;

    protected $table = 'contract_invest_profits';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'contract_id',
        'percent',
        'amount',
        'date',
        'is_pay',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'date' => 'date',
        'is_pay' => 'boolean',
    ];
}
