<?php

namespace App\Entity\Contract;

use App\Traits\CropImageTrait;
use App\Traits\StatusTransaction;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Contract\ContractPayment
 *
 * @property int $id
 * @property int $contract_id
 * @property double $main_amount
 * @property double $balance_owed
 * @property double $percent
 * @property double $amount
 * @property string $status
 * @property string $pay_ticket
 * @property Carbon $date
 * @property Carbon $created_at
 * @property Carbon
 * @property Contract $contract
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $status_name
 * @property-read mixed $status_name_html
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment active()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment approve()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment close()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment new()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment notApprove()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment waitClose()
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment whereBalanceOwed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment whereContractId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment whereMainAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment wherePayTicket($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property float $penalty
 * @method static \Illuminate\Database\Eloquent\Builder|ContractPayment wherePenalty($value)
 */
class ContractPayment extends Model
{
    use Sortable, CropImageTrait, StatusTransaction;

    protected $table = 'contract_payments';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'contract_id',
        'main_amount',
        'balance_owed',
        'percent',
        'amount',
        'date',
        'status',
        'penalty',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'date' => 'date'
    ];

    public function contract()
    {
        return $this->belongsTo(Contract::class, 'contract_id', 'id');
    }
}
