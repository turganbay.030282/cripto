<?php

namespace App\Entity;

use App\Helpers\ArrayHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Form
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $phone
 * @property string $company
 * @property string $message
 * @property string $reason
 * @property string $file
 * @property int $status
 * @property int $type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User $client
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read mixed $type_name
 * @method static \Illuminate\Database\Eloquent\Builder|Form newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Form newQuery()
 * @method static \Illuminate\Database\Query\Builder|Form onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Form query()
 * @method static \Illuminate\Database\Eloquent\Builder|Form sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|Form withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Form withoutTrashed()
 * @mixin \Eloquent
 */

class Form extends Model
{
    use Sortable, SoftDeletes;

    const type_contact = 1;

    const status_new = 1;
    const status_close = 2;

    protected $table = 'forms';

    protected $fillable = [
        'user_id',
        'name',
        'surname',
        'email',
        'phone',
        'company',
        'message',
        'reason',
        'file',
        'status',
        'type',
    ];

    public $sortable = [
        'id',
        'user_id',
        'name',
        'surname',
        'email',
        'phone',
        'company',
        'message',
        'reason',
        'file',
        'status',
        'type',
        'created_at',
        'updated_at',
    ];

    public static function statusList()
    {
        return [
            self::status_new => 'Новая',
            self::status_close => 'Обработана',
        ];
    }

    public static function typeList()
    {
        return [
            self::type_contact => 'Контактная форма',
        ];
    }

    public function getTypeNameAttribute()
    {
        return ArrayHelper::getValue(self::typeList(), $this->type, '');
    }

    public function client()
    {
        return $this->belongsTo(User::class,'user_id', 'id');
    }

}
