<?php

namespace App\Entity\Handbook;

use App\Entity\Coinmarketcap;
use App\Entity\Contract\Contract;
use App\Helpers\ArrayHelper;
use App\Traits\CropImageTrait;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Handbook\Asset
 *
 * @property int $id
 * @property int $coinmarketcap_id
 * @property string $type
 * @property bool $for_graph
 * @property bool $for_trader
 * @property array $calc
 * @property Contract[] $contracts
 * @property Coinmarketcap $coinValue
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read int|null $contracts_count
 * @property-read mixed $type_name
 * @property-read \App\Entity\Handbook\AssetTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Handbook\AssetTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static Builder|Asset forGraph()
 * @method static Builder|Asset listsTranslations($translationField)
 * @method static Builder|Asset newModelQuery()
 * @method static Builder|Asset newQuery()
 * @method static Builder|Asset notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|Asset onlyTrashed()
 * @method static Builder|Asset orWhereTranslation($translationField, $value, $locale = null)
 * @method static Builder|Asset orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static Builder|Asset orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static Builder|Asset query()
 * @method static Builder|Asset sortable($defaultParameters = null)
 * @method static Builder|Asset translated()
 * @method static Builder|Asset translatedIn($locale = null)
 * @method static Builder|Asset typeFirst()
 * @method static Builder|Asset typeSecond()
 * @method static Builder|Asset whereCoinmarketcapId($value)
 * @method static Builder|Asset whereCreatedAt($value)
 * @method static Builder|Asset whereDeletedAt($value)
 * @method static Builder|Asset whereForGraph($value)
 * @method static Builder|Asset whereId($value)
 * @method static Builder|Asset whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static Builder|Asset whereTranslationLike($translationField, $value, $locale = null)
 * @method static Builder|Asset whereType($value)
 * @method static Builder|Asset whereUpdatedAt($value)
 * @method static Builder|Asset withTranslation()
 * @method static \Illuminate\Database\Query\Builder|Asset withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Asset withoutTrashed()
 * @mixin \Eloquent
 * @method static Builder|Asset forTrader()
 * @method static Builder|Asset whereForTrader($value)
 * @property bool $for_investor
 * @property string|null $wallet_number
 * @property string|null $wallet_img
 * @method static Builder|Asset forInvestor()
 * @method static Builder|Asset whereCalc($value)
 * @method static Builder|Asset whereForInvestor($value)
 * @method static Builder|Asset whereWalletImg($value)
 * @method static Builder|Asset whereWalletNumber($value)
 */
class Asset extends Model
{
    use Sortable, SoftDeletes, Translatable, CropImageTrait;

    const TYPE_NATURAL = 'natural';
    const TYPE_VIRTUAL = 'virtual';

    protected $table = 'assets';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'type',
        'created_at',
        'updated_at',
    ];

    public $translatedAttributes = [
        'name',
    ];

    public $casts = [
        'for_graph' => 'boolean',
        'for_trader' => 'boolean',
        'for_investor' => 'boolean',
        'calc' => 'array'
    ];

    /**
     * @param $assetId
     * @param $amount
     * @param $period
     * @return int|mixed
     */
    public static function getMonthPercent($assetId, $amount, $period)
    {
        $asset = self::find($assetId);
        if (!$asset) {
            return 0;
        }

        $calc = $asset->calc;
        if (!$calc) {
            return 0;
        }

        $row = 1;

        //3-6-9-12-24-36-48-60
        if ($period <= 3) {
            return $calc["td{$row}1"] ?? 0;
        } elseif ($period <= 6) {
            return $calc["td{$row}2"] ?? 0;
        } elseif ($period <= 9) {
            return $calc["td{$row}3"] ?? 0;
        } elseif ($period <= 12) {
            return $calc["td{$row}4"] ?? 0;
        } elseif ($period <= 24) {
            return $calc["td{$row}5"] ?? 0;
        } elseif ($period <= 36) {
            return $calc["td{$row}6"] ?? 0;
        } elseif ($period <= 48) {
            return $calc["td{$row}7"] ?? 0;
        } else {
            return $calc["td{$row}8"] ?? 0;
        }
    }

    public function isNatural()
    {
        return $this->type == self::TYPE_NATURAL;
    }

    public function isVirtual()
    {
        return $this->type == self::TYPE_VIRTUAL;
    }

    public static function listType()
    {
        return [
            self::TYPE_NATURAL => trans('site.active.type-1'),
            self::TYPE_VIRTUAL => trans('site.active.type-2'),
        ];
    }

    public function getTypeNameAttribute()
    {
        return ArrayHelper::getValue(self::listType(), $this->type);
    }

    public function contracts()
    {
        return $this->hasMany(Contract::class, 'type_asset_id', 'id');
    }

    public function coinValue()
    {
        return $this->hasOne(Coinmarketcap::class, 'id', 'coinmarketcap_id');
    }

    public function scopeTypeFirst(Builder $query)
    {
        return $query->where('type', self::TYPE_NATURAL);
    }

    public function scopeTypeSecond(Builder $query)
    {
        return $query->where('type', self::TYPE_VIRTUAL);
    }

    public function scopeForGraph(Builder $query)
    {
        return $query->where('for_graph', true);
    }

    public function scopeForTrader(Builder $query)
    {
        return $query->where('for_trader', true);
    }

    public function scopeForInvestor(Builder $query)
    {
        return $query->where('for_investor', true);
    }
}
