<?php

namespace App\Entity\Handbook;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Handbook\AssetTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $name
 * @property int $asset_id
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|AssetTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AssetTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AssetTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|AssetTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|AssetTranslation whereAssetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssetTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssetTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssetTranslation whereName($value)
 * @mixin \Eloquent
 */
class AssetTranslation extends Model
{
    use Status;

    protected $table = 'asset_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'name',
    ];
}
