<?php

namespace App\Entity\Handbook;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Handbook\CountryTranslation
 *
 * @property int $id
 * @property int $country_id
 * @property string $locale
 * @property string $name
 * @property string|null $title
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|CountryTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CountryTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CountryTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|CountryTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|CountryTranslation whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountryTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountryTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CountryTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class CountryTranslation extends Model
{
    use Status;

    protected $table = 'country_translations';

    public $timestamps = false;

    public $fillable = [
        'country_id',
        'locale',
        'title',
    ];
}
