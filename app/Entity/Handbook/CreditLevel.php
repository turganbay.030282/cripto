<?php

namespace App\Entity\Handbook;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Handbook\CreditLevel
 *
 * @property int $id
 * @property int $range_from
 * @property int $range_to
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read mixed $range
 * @property-read \App\Entity\Handbook\CreditLevelTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Handbook\CreditLevelTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|CreditLevel onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel query()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel translated()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel whereRangeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel whereRangeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevel withTranslation()
 * @method static \Illuminate\Database\Query\Builder|CreditLevel withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CreditLevel withoutTrashed()
 * @mixin \Eloquent
 */
class CreditLevel extends Model
{
    use Sortable, SoftDeletes, Translatable;

    protected $table = 'credit_levels';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'range_from',
        'range_to',
        'created_at',
        'updated_at',
    ];

    public $translatedAttributes = [
        'name',
    ];

    public function getRangeAttribute()
    {
        return $this->attributes['range_from'] . '-' . $this->attributes['range_to'];
    }
}
