<?php

namespace App\Entity\Handbook;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Handbook\CreditLevelTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $name
 * @property int $credit_level_id
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelTranslation whereCreditLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelTranslation whereName($value)
 * @mixin \Eloquent
 */
class CreditLevelTranslation extends Model
{
    use Status;

    protected $table = 'credit_level_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'name',
    ];
}
