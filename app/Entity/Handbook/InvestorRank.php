<?php

namespace App\Entity\Handbook;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Handbook\InvestorRank
 *
 * @property int $id
 * @property int $range_from
 * @property int $range_to
 * @property int $min
 * @property int $max
 * @property int $level
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read mixed $range
 * @property-read \App\Entity\Handbook\InvestorRankTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Handbook\InvestorRankTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|InvestorRank onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank query()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank translated()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank whereRangeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank whereRangeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRank withTranslation()
 * @method static \Illuminate\Database\Query\Builder|InvestorRank withTrashed()
 * @method static \Illuminate\Database\Query\Builder|InvestorRank withoutTrashed()
 * @mixin \Eloquent
 */
class InvestorRank extends Model
{
    use Sortable, SoftDeletes, Translatable;

    protected $table = 'investor_ranks';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'name',
        'range_from',
        'range_to',
        'created_at',
        'updated_at',
    ];

    public $translatedAttributes = [
        'name',
    ];

    public function getRangeAttribute()
    {
        return $this->attributes['range_from'] . '-' . $this->attributes['range_to'];
    }

}
