<?php

namespace App\Entity\Handbook;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Handbook\InvestorRankTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $name
 * @property int $investor_rank_id
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankTranslation whereInvestorRankId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankTranslation whereName($value)
 * @mixin \Eloquent
 */
class InvestorRankTranslation extends Model
{
    use Status;

    protected $table = 'investor_rank_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'name',
    ];
}
