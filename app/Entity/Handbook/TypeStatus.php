<?php

namespace App\Entity\Handbook;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Handbook\TypeStatus
 *
 * @method static \Illuminate\Database\Eloquent\Builder|TypeStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeStatus sortable($defaultParameters = null)
 * @mixin \Eloquent
 * @property int $id
 * @property string $key
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TypeStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeStatus whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeStatus whereUpdatedAt($value)
 */
class TypeStatus extends Model
{
    use Sortable;

    protected $table = 'type_statuses';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'key',
        'name',
        'created_at',
        'updated_at',
    ];

}
