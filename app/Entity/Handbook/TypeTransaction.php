<?php

namespace App\Entity\Handbook;

use App\Entity\Setting;
use App\Traits\TypeUser;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Handbook\TypeTransaction
 *
 * @property int $id
 * @property string $type
 * @property double $value
 * @property int $period
 * @property int $min
 * @property int $max
 * @property int $level
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read mixed $type_name
 * @property-read \App\Entity\Handbook\TypeTransactionTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Handbook\TypeTransactionTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction investor()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|TypeTransaction onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction trader()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction translated()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction whereMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction whereMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction wherePeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransaction withTranslation()
 * @method static \Illuminate\Database\Query\Builder|TypeTransaction withTrashed()
 * @method static \Illuminate\Database\Query\Builder|TypeTransaction withoutTrashed()
 * @mixin \Eloquent
 */
class TypeTransaction extends Model
{
    use Sortable, SoftDeletes, TypeUser, Translatable;

    protected $table = 'type_transactions';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'type',
        'value',
        'period',
        'min',
        'max',
        'level',
        'created_at',
        'updated_at',
    ];

    public $translatedAttributes = [
        'name',
    ];

    public function getValueBonus($user, $isContract = true)
    {
        if (!$user) {
            return $this->value;
        }

        $setting = Setting::first();

        if ($isContract && !$user->hasContract()) {
            return $this->value - $setting->bonus_ref_credit;
        }

        if (!$isContract && !$user->hasDeposit()) {
            return $this->value + $setting->bonus_ref_deposit;
        }

        return $this->value;
    }

}
