<?php

namespace App\Entity\Handbook;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Handbook\TypeTransactionTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $name
 * @property int $type_transaction_id
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransactionTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransactionTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransactionTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransactionTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransactionTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransactionTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransactionTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTransactionTranslation whereTypeTransactionId($value)
 * @mixin \Eloquent
 */
class TypeTransactionTranslation extends Model
{
    use Status;

    protected $table = 'type_transaction_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'name',
    ];
}
