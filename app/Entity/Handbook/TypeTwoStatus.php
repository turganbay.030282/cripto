<?php

namespace App\Entity\Handbook;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Handbook\TypeStatusTwo
 *
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTwoStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTwoStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTwoStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTwoStatus sortable($defaultParameters = null)
 * @mixin \Eloquent
 * @property int $id
 * @property string $key
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTwoStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTwoStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTwoStatus whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTwoStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeTwoStatus whereUpdatedAt($value)
 */
class TypeTwoStatus extends Model
{
    use Sortable;

    protected $table = 'type_two_statuses';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'key',
        'name',
        'created_at',
        'updated_at',
    ];

}
