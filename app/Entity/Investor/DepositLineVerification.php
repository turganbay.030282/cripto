<?php

namespace App\Entity\Investor;

use App\Entity\User;
use App\Traits\CropImageTrait;
use App\Traits\StatusVerification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Investor\DepositLineVerification
 *
 * @property int $id
 * @property int $user_id
 * @property int $amount
 * @property int $manager_id
 * @property string $status
 * @property string $comment
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User $user
 * @property User $manager
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification history()
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification query()
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification reject()
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification verify()
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification wait()
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification whereManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DepositLineVerification whereUserId($value)
 * @mixin \Eloquent
 */
class DepositLineVerification extends Model
{
    use CropImageTrait, Sortable, StatusVerification;

    public const STATUS_WAIT = 'wait';
    public const STATUS_REJECT = 'declined';
    public const STATUS_APPROVED = 'approved';

    protected $table = 'deposit_line_verifications';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'amount',
        'status',
        'comment',
        'created_at',
        'updated_at',
    ];

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
