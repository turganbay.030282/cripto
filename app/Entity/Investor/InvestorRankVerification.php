<?php

namespace App\Entity\Investor;

use App\Entity\User;
use App\Traits\CropImageTrait;
use App\Traits\StatusVerification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Investor\InvestorRankVerification
 *
 * @property int $id
 * @property int $user_id
 * @property int $amount
 * @property int $manager_id
 * @property string $status
 * @property string $comment
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $company_name
 * @property string $company_reg_number
 * @property Carbon $company_date_reg
 * @property string $company_country_reg
 * @property string $company_address
 * @property string $company_ur_address
 * @property string $company_field_activity
 * @property string $company_phone
 * @property string $company_email
 * @property string $full_name
 * @property Carbon $birthday
 * @property string $sex
 * @property string $bin
 * @property string $place_birth
 * @property string $country
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $professional_activities
 * @property bool $volume_1
 * @property bool $volume_2
 * @property bool $volume_3
 * @property bool $volume_4
 * @property bool $volume_5
 * @property string $position
 * @property string $doc_name
 * @property bool $source_business_activities
 * @property bool $source_dividend
 * @property bool $source_loans
 * @property bool $source_income_assets
 * @property bool $source_contributions
 * @property bool $source_prize
 * @property bool $source_other
 * @property string $source_other_text
 * @property string $source_country
 * @property bool $i_beneficiary
 * @property string $beneficiary_name
 * @property bool $not_pep
 * @property string $politics
 * @property User $user
 * @property User $manager
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification history()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification query()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification reject()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification verify()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification wait()
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereBeneficiaryName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereBin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereCompanyAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereCompanyCountryReg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereCompanyDateReg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereCompanyEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereCompanyFieldActivity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereCompanyPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereCompanyRegNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereCompanyUrAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereDocName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereIBeneficiary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereNotPep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification wherePlaceBirth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification wherePolitics($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereProfessionalActivities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereSourceBusinessActivities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereSourceContributions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereSourceCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereSourceDividend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereSourceIncomeAssets($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereSourceLoans($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereSourceOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereSourceOtherText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereSourcePrize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereVolume1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereVolume2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereVolume3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereVolume4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvestorRankVerification whereVolume5($value)
 * @mixin \Eloquent
 */
class InvestorRankVerification extends Model
{
    use CropImageTrait, Sortable, StatusVerification;

    public const STATUS_WAIT = 'wait';
    public const STATUS_REJECT = 'declined';
    public const STATUS_APPROVED = 'approved';

    protected $table = 'investor_rank_verifications';

    protected $guarded = ['id'];
    protected $casts = [
        'company_date_reg' => 'date',
        'birthday' => 'date',

        'volume_1' => 'boolean',
        'volume_2' => 'boolean',
        'volume_3' => 'boolean',
        'volume_4' => 'boolean',
        'volume_5' => 'boolean',
        'source_business_activities' => 'boolean',
        'source_dividend' => 'boolean',
        'source_income_assets' => 'boolean',
        'source_contributions' => 'boolean',
        'source_loans' => 'boolean',
        'source_prize' => 'boolean',
        'source_other' => 'boolean',
        'i_beneficiary' => 'boolean',
        'not_pep' => 'boolean',
    ];

    public $sortable = [
        'id',
        'amount',
        'status',
        'comment',
        'created_at',
        'updated_at',
    ];

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
