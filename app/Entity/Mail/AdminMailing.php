<?php

namespace App\Entity\Mail;

use App\Entity\User;
use App\Helpers\ArrayHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Mail\AdminMailing
 *
 * @property int $id
 * @property int $admin_id
 * @property string $list_user
 * @property string $subject
 * @property string $body
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User $admin
 * @property User[] $adminMailingUsers
 * @property-read int|null $admin_mailing_users_count
 * @property-read mixed $name_list_user
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMailing newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMailing newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMailing query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMailing sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMailing whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMailing whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMailing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMailing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMailing whereListUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMailing whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMailing whereUpdatedAt($value)
 * @mixin \Eloquent
 */

class AdminMailing extends Model
{
    use Sortable;

    protected $table = 'admin_mailings';

    CONST LIST_ALL = 'all';
    CONST LIST_TRADER = 'trader';
    CONST LIST_INVESTOR = 'investor';
    CONST LIST_ONLY_SELECT = 'only_select';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'admin_id',
        'list_user',
        'subject',
        'body',
        'created_at',
        'updated_at',
    ];

    public static function listListTo()
    {
        return [
            self::LIST_ALL => 'Все',
            self::LIST_TRADER => 'Трейдеры',
            self::LIST_INVESTOR => 'Инвесторы',
            self::LIST_ONLY_SELECT => 'Только выбранные',
        ];
    }

    public function getNameListUserAttribute()
    {
        return ArrayHelper::getValue(self::listListTo(),$this->list_user,'-');
    }

    public function isAll()
    {
        return $this->list_user == self::LIST_ALL;
    }

    public function isTrader()
    {
        return $this->list_user == self::LIST_TRADER;
    }

    public function isInvestor()
    {
        return $this->list_user == self::LIST_INVESTOR;
    }

    public function isOnlySelect()
    {
        return $this->list_user == self::LIST_ONLY_SELECT;
    }

    public function admin()
    {
        return $this->belongsTo(User::class,'admin_id', 'id');
    }

    public function adminMailingUsers()
    {
        return $this->belongsToMany(User::class,'admin_mailing_users', 'admin_mailing_id', 'user_id');
    }
}
