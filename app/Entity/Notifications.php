<?php

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Notifications
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $body
 * @property bool $is_read
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User $user
 * @method static Builder|Notifications new()
 * @method static Builder|Notifications newModelQuery()
 * @method static Builder|Notifications newQuery()
 * @method static Builder|Notifications query()
 * @method static Builder|Notifications sortable($defaultParameters = null)
 * @method static Builder|Notifications whereBody($value)
 * @method static Builder|Notifications whereCreatedAt($value)
 * @method static Builder|Notifications whereId($value)
 * @method static Builder|Notifications whereIsRead($value)
 * @method static Builder|Notifications whereTitle($value)
 * @method static Builder|Notifications whereUpdatedAt($value)
 * @method static Builder|Notifications whereUserId($value)
 * @mixin \Eloquent
 */
class Notifications extends Model
{
    use Sortable;

    protected $table = 'notification_users';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'user_id',
        'title',
        'body',
        'is_read',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'is_read' => 'boolean'
    ];

    public function read()
    {
        if (!$this->is_read)
            $this->update(['is_read' => true]);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function scopeNew(Builder $query)
    {
        return $query->where('is_read', false);
    }
}
