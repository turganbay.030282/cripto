<?php

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\RegisterVerifiedCode
 *
 * @property int $id
 * @property string $phone
 * @property string $verify_token
 * @property Carbon $verify_token_expire
 * @property string $ip
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|RegisterVerifiedCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RegisterVerifiedCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RegisterVerifiedCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|RegisterVerifiedCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RegisterVerifiedCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RegisterVerifiedCode whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RegisterVerifiedCode wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RegisterVerifiedCode whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RegisterVerifiedCode whereVerifyToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RegisterVerifiedCode whereVerifyTokenExpire($value)
 * @mixin \Eloquent
 */

class RegisterVerifiedCode extends Model
{
    protected $table = 'register_verify_codes';

    protected $guarded = ['id'];

    protected $casts = [
        'verify_token_expire' => 'datetime'
    ];
}
