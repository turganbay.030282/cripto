<?php

namespace App\Entity;

use App\Traits\CropImageTrait;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Setting
 *
 * @property int $id
 * @property int $crop_blog_width
 * @property int $crop_blog_height
 * @property int $crop_ava_width
 * @property int $crop_ava_height
 * @property int $crop_cert_width
 * @property int $crop_cert_height
 * @property string $contact_phone
 * @property string $contact_email
 * @property string $soc_fb
 * @property string $soc_tw
 * @property string $soc_in
 * @property string $wallet_number
 * @property string $wallet_img
 * @property boolean $is_manual_verify
 * @property int $commission_trader
 * @property int $commission_investor
 * @property int $commission_sell_trader
 * @property double $course_investor
 * @property double $conversion_fee
 * @property double $penalty
 * @property-read \App\Entity\SettingTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\SettingTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|Setting listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting translated()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCommissionInvestor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCommissionSellTrader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCommissionTrader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereContactEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereContactPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCourseInvestor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCropAvaHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCropAvaWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCropBlogHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCropBlogWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCropCertHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCropCertWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereIsManualVerify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereSocFb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereSocIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereSocTw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting withTranslation()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereWalletImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereWalletNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereConversionFee($value)
 * @property string $bonus_ref_credit
 * @property string $bonus_ref_deposit
 * @property string $percent_ref_credit
 * @property string $percent_ref_deposit
 * @property string|null $soc_yt
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereBonusRefCredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereBonusRefDeposit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting wherePenalty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting wherePercentRefCredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting wherePercentRefDeposit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereSocYt($value)
 */
class Setting extends Model
{
    use Translatable, CropImageTrait;

    protected $table = 'settings';

    public $timestamps = false;

    protected $guarded = ['id'];

    public function getCommissionTraderAttribute($value)
    {
        return $value / 100;
    }

    public function setCommissionTraderAttribute($value)
    {
        $this->attributes['commission_trader'] = $value * 100;
    }

    public function getCommissionSellTraderAttribute($value)
    {
        return $value / 100;
    }

    public function setCommissionSellTraderAttribute($value)
    {
        $this->attributes['commission_sell_trader'] = $value * 100;
    }

    public function getCommissionInvestorAttribute($value)
    {
        return $value / 100;
    }

    public function setCommissionInvestorAttribute($value)
    {
        $this->attributes['commission_investor'] = $value * 100;
    }

    public $translatedAttributes = [
        'contact_address',
        'req_iban',
        'req_bank_address',
        'req_sia',
        'req_reg',
        'req_seb',
        'req_swift',
        'footer_text_1',
        'footer_text_2',
        'file_politic',
    ];

    protected $casts = [
        'is_manual_verify' => 'boolean'
    ];
}
