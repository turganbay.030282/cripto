<?php

namespace App\Entity;

use App\Traits\FileTrait;
use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\SettingTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $contact_address
 * @property string $req_sia
 * @property string $req_reg
 * @property string $req_seb
 * @property string $req_swift
 * @property string $footer_text_1
 * @property string $footer_text_2
 * @property int $setting_id
 * @property string|null $req_iban
 * @property string|null $req_bank_address
 * @property string|null $file_politic
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation whereContactAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation whereFooterText1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation whereFooterText2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation whereReqBankAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation whereReqIban($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation whereReqReg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation whereReqSeb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation whereReqSia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation whereReqSwift($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation whereSettingId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|SettingTranslation whereFilePolitic($value)
 */
class SettingTranslation extends Model
{
    use Sortable, Status, FileTrait;

    protected $table = 'setting_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'contact_address',
        'req_iban',
        'req_bank_address',
        'req_sia',
        'req_reg',
        'req_seb',
        'req_swift',
        'footer_text_1',
        'footer_text_2',
        'file_politic',
    ];

    public $sortable = [
        'locale',
        'contact_address',
        'req_iban',
        'req_bank_address',
        'req_sia',
        'req_reg',
        'req_seb',
        'req_swift',
        'footer_text_1',
        'footer_text_2',
        'file_politic',
    ];

}
