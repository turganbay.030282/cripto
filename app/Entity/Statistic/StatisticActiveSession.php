<?php

namespace App\Entity\Statistic;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Statistic\StatisticActiveSession
 *
 * @property int $id
 * @property int $investor
 * @property int $trader
 * @property Carbon $date_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticActiveSession newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticActiveSession newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticActiveSession query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticActiveSession whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticActiveSession whereDateAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticActiveSession whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticActiveSession whereInvestor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticActiveSession whereTrader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticActiveSession whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StatisticActiveSession extends Model
{
    protected $table = 'statistic_active_sessions';

    protected $guarded = ['id'];

    protected $casts = [
        'date_at' => 'date'
    ];
}
