<?php

namespace App\Entity\Statistic;

use App\Entity\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Statistic\StatisticPromoCode
 *
 * @property int $id
 * @property int $user_id
 * @property string $email
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User $user
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticPromoCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticPromoCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticPromoCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticPromoCode sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticPromoCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticPromoCode whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticPromoCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticPromoCode whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticPromoCode whereUserId($value)
 * @mixin \Eloquent
 * @property int|null $reg_user_id
 * @property-read User|null $registerUser
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticPromoCode whereRegUserId($value)
 */
class StatisticPromoCode extends Model
{
    use Sortable;

    protected $table = 'statistic_promo_codes';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'email',
        'created_at',
        'updated_at',
    ];

    public function registerUser()
    {
        return $this->belongsTo(User::class, 'reg_user_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
