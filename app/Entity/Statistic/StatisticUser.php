<?php

namespace App\Entity\Statistic;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Statistic\StatisticUser
 *
 * @property int $id
 * @property int $month
 * @property int $year
 * @property int $register
 * @property int $trader
 * @property int $investor
 * @property int $verify
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticUser whereInvestor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticUser whereMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticUser whereRegister($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticUser whereTrader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticUser whereVerify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatisticUser whereYear($value)
 * @mixin \Eloquent
 */
class StatisticUser extends Model
{
    protected $table = 'statistic_register_users';

    protected $guarded = ['id'];
}
