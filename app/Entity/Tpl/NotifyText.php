<?php

namespace App\Entity\Tpl;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Tpl\NotifyText
 *
 * @property int $id
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entity\Tpl\NotifyTextTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Tpl\NotifyTextTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText query()
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText translated()
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyText withTranslation()
 * @mixin \Eloquent
 */
class NotifyText extends Model
{
    use Translatable, Sortable;

    const CONVERT_SEND = 40;
    const CONVERT_CLOSED = 41;
    const CONVERT_CANCELED = 42;
    const USER_DELETE = 43;
    protected $table = 'notify_texts';

    protected $guarded = ['id'];

    public $translatedAttributes = [
        'subject',
        'body'
    ];
}
