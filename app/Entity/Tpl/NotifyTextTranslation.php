<?php

namespace App\Entity\Tpl;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Tpl\NotifyTextTranslation
 *
 * @property int $id
 * @property int $notify_text_id
 * @property string $locale
 * @property string $subject
 * @property string $body
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyTextTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyTextTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyTextTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyTextTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyTextTranslation sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyTextTranslation whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyTextTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyTextTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyTextTranslation whereNotifyTextId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NotifyTextTranslation whereSubject($value)
 * @mixin \Eloquent
 */
class NotifyTextTranslation extends Model
{
    use Sortable, Status;

    protected $table = 'notify_text_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'subject',
        'body'
    ];

    public $sortable = [
        'locale',
        'subject',
        'body'
    ];
}
