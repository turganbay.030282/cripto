<?php

namespace App\Entity\Tpl;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Tpl\TplDocument
 *
 * @property int $id
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entity\Tpl\TplDocumentTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Tpl\TplDocumentTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument translated()
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocument withTranslation()
 * @mixin \Eloquent
 */
class TplDocument extends Model
{
    use Translatable, Sortable;

    protected $table = 'tpl_documents';

    protected $guarded = ['id'];

    public $translatedAttributes = [
        'content',
    ];
}
