<?php

namespace App\Entity\Tpl;

use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Tpl\TplDocumentTranslation
 *
 * @property int $id
 * @property string $locale
 * @property string $content
 * @property int $tpl_document_id
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocumentTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocumentTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocumentTranslation publish()
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocumentTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocumentTranslation sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocumentTranslation whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocumentTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocumentTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TplDocumentTranslation whereTplDocumentId($value)
 * @mixin \Eloquent
 */
class TplDocumentTranslation extends Model
{
    use Sortable, Status;

    protected $table = 'tpl_document_translations';

    public $timestamps = false;

    public $fillable = [
        'locale',
        'content',
    ];

    public $sortable = [
        'locale',
        'content',
    ];
}
