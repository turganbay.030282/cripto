<?php

namespace App\Entity\Trader;

use App\Entity\User;
use App\Traits\CropImageTrait;
use App\Traits\FileTrait;
use App\Traits\StatusVerification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Trader\CreditLevelUrVerification
 *
 * @property int $id
 * @property int $user_id
 * @property int $amount
 * @property int $manager_id
 * @property string $status
 * @property string $comment
 * @property Carbon $register_date
 * @property Carbon $credit_date_to
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $file_statement
 * @property string $file_income_work
 * @property string $file_utility_bill
 * @property User $user
 * @property User $manager
 * @property CreditLevelUrVerificationPerson[] $persons
 * @property int $is_ur
 * @property string|null $company_name
 * @property string|null $reg_number
 * @property string|null $ur_address
 * @property string|null $address
 * @property string|null $contact_phone
 * @property string|null $email
 * @property string|null $primary_occupation
 * @property string|null $capital_amount
 * @property string|null $person_name
 * @property string|null $person_surname
 * @property string|null $person_personal_code
 * @property string|null $person_turnover_prev
 * @property string|null $person_profit_prev
 * @property string|null $person_banks
 * @property string|null $creditor
 * @property string|null $start_amount
 * @property string|null $balance_obligations
 * @property string|null $month_payments
 * @property string|null $credit_target
 * @property string|null $credit_source
 * @property-read mixed $status_name
 * @property-read int|null $persons_count
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification history()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification query()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification reject()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification verify()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification wait()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereBalanceObligations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereCapitalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereContactPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereCreditDateTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereCreditSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereCreditTarget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereCreditor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereFileIncomeWork($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereFileStatement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereFileUtilityBill($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereIsUr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereMonthPayments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification wherePersonBanks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification wherePersonName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification wherePersonPersonalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification wherePersonProfitPrev($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification wherePersonSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification wherePersonTurnoverPrev($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification wherePrimaryOccupation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereRegNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereRegisterDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereStartAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereUrAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerification whereUserId($value)
 * @mixin \Eloquent
 */
class CreditLevelUrVerification extends Model
{
    use CropImageTrait, Sortable, StatusVerification, FileTrait;

    public const STATUS_WAIT = 'wait';
    public const STATUS_REJECT = 'declined';
    public const STATUS_APPROVED = 'approved';

    protected $table = 'credit_level_ur_verifications';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'amount',
        'status',
        'comment',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'register_date' => 'date',
        'credit_date_to' => 'date',
    ];

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function persons()
    {
        return $this->hasMany(CreditLevelUrVerificationPerson::class, 'credit_level_id', 'id');
    }
}
