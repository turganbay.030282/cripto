<?php

namespace App\Entity\Trader;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Trader\CreditLevelUrVerificationPerson
 *
 * @property int $id
 * @property int $credit_level_id
 * @property string $name
 * @property string $surname
 * @property string $personal_code
 * @property string|null $capital_amount
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerificationPerson newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerificationPerson newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerificationPerson query()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerificationPerson whereCapitalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerificationPerson whereCreditLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerificationPerson whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerificationPerson whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerificationPerson wherePersonalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelUrVerificationPerson whereSurname($value)
 * @mixin \Eloquent
 */
class CreditLevelUrVerificationPerson extends Model
{
    protected $table = 'credit_level_owners';

    protected $guarded = ['id'];

    public $timestamps = false;
}
