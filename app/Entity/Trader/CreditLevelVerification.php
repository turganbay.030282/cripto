<?php

namespace App\Entity\Trader;

use App\Entity\User;
use App\Traits\CropImageTrait;
use App\Traits\FileTrait;
use App\Traits\StatusVerification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Trader\CreditLevelVerification
 *
 * @property int $id
 * @property int $user_id
 * @property int $amount
 * @property int $manager_id
 * @property string $status
 * @property string $comment
 * @property string $first_name
 * @property string $last_name
 * @property string $sex
 * @property string $personal_code
 * @property string $family_status
 * @property string $political_functions
 * @property string $phone
 * @property string $email
 * @property string $dependents
 * @property string $type_accommodation
 * @property string $education
 * @property string $citizenship
 * @property string $residence_county
 * @property string $residence_city
 * @property string $residence_street
 * @property string $residence_postcode
 * @property string $income_source
 * @property int $income_net
 * @property int $income_additional
 * @property string $income_place
 * @property string $income_position
 * @property int $income_work_experience
 * @property string $credit_liabilities
 * @property int $credit_liabilities_month
 * @property int $credit_type
 * @property Carbon $credit_date_from
 * @property Carbon $credit_date_to
 * @property string $file_statement
 * @property string $file_income_work
 * @property string $file_utility_bill
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User $user
 * @property User $manager
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification history()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification query()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification reject()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification verify()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification wait()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereCitizenship($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereCreditDateFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereCreditDateTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereCreditLiabilities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereCreditLiabilitiesMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereCreditType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereDependents($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereEducation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereFamilyStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereFileIncomeWork($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereFileStatement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereFileUtilityBill($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereIncomeAdditional($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereIncomeNet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereIncomePlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereIncomePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereIncomeSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereIncomeWorkExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification wherePersonalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification wherePoliticalFunctions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereResidenceCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereResidenceCounty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereResidencePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereResidenceStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereTypeAccommodation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLevelVerification whereUserId($value)
 * @mixin \Eloquent
 * @property-read mixed $address
 */
class CreditLevelVerification extends Model
{
    use CropImageTrait, Sortable, StatusVerification, FileTrait;

    public const STATUS_WAIT = 'wait';
    public const STATUS_REJECT = 'declined';
    public const STATUS_APPROVED = 'approved';

    protected $table = 'credit_level_verifications';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'amount',
        'status',
        'comment',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'credit_date_from' => 'date',
        'credit_date_to' => 'date',
    ];

    public function getAddressAttribute()
    {
        return "{$this->residence_county}, {$this->residence_city}, {$this->residence_street}";
    }

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
