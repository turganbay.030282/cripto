<?php

namespace App\Entity\Trader;

use App\Entity\User;
use App\Traits\CropImageTrait;
use App\Traits\StatusVerification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Trader\CreditLineVerification
 *
 * @property int $id
 * @property int $user_id
 * @property int $amount
 * @property int $manager_id
 * @property string $status
 * @property string $comment
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User $user
 * @property User $manager
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification history()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification query()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification reject()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification verify()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification wait()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification whereManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditLineVerification whereUserId($value)
 * @mixin \Eloquent
 */
class CreditLineVerification extends Model
{
    use CropImageTrait, Sortable, StatusVerification;

    public const STATUS_WAIT = 'wait';
    public const STATUS_REJECT = 'declined';
    public const STATUS_APPROVED = 'approved';

    protected $table = 'credit_line_verifications';

    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'amount',
        'status',
        'comment',
        'created_at',
        'updated_at',
    ];

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
