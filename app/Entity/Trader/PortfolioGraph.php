<?php

namespace App\Entity\Trader;

use App\Entity\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Trader\PortfolioGraph
 *
 * @property int $id
 * @property int $user_id
 * @property double $amount
 * @property integer $tm
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User $user
 * @method static \Illuminate\Database\Eloquent\Builder|PortfolioGraph newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PortfolioGraph newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PortfolioGraph query()
 * @method static \Illuminate\Database\Eloquent\Builder|PortfolioGraph whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortfolioGraph whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortfolioGraph whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortfolioGraph whereTm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortfolioGraph whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortfolioGraph whereUserId($value)
 * @mixin \Eloquent
 */
class PortfolioGraph extends Model
{
    protected $table = 'trader_portfolio_graphs';

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
