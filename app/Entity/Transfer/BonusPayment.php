<?php

namespace App\Entity\Transfer;

use App\Entity\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Transfer\BonusPayment
 *
 * @property int $id
 * @property int $user_id
 * @property float $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read User $client
 * @method static \Illuminate\Database\Eloquent\Builder|BonusPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BonusPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BonusPayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|BonusPayment whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BonusPayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BonusPayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BonusPayment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BonusPayment whereUserId($value)
 * @mixin \Eloquent
 */
class BonusPayment extends Model
{
    protected $table = 'bonus_payments';

    protected $fillable = ['user_id', 'amount'];

    public function client()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
