<?php

namespace App\Entity\Transfer;

use App\Entity\Handbook\Asset;
use App\Entity\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\Transfer\Convert
 *
 * @property int $id
 * @property string $type
 * @property string $amount
 * @property float $amount_asset
 * @property int|null $type_asset_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read User $client
 * @property-read Asset|null $typeAsset
 * @method static \Illuminate\Database\Eloquent\Builder|Convert newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Convert newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Convert query()
 * @method static \Illuminate\Database\Eloquent\Builder|Convert sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Convert whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Convert whereAmountAsset($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Convert whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Convert whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Convert whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Convert whereTypeAssetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Convert whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Convert whereUserId($value)
 * @mixin \Eloquent
 * @property string $status
 * @method static Builder|Convert new()
 * @method static Builder|Convert whereStatus($value)
 */
class Convert extends Model
{
    use Sortable;

    protected $table = 'converts';

    CONST TYPE_BUY = 'buy';
    CONST TYPE_SELL = 'sell';
    CONST STATUS_NEW = 'new';
    CONST STATUS_CLOSE = 'close';
    CONST STATUS_CANCELED = 'canceled';

    protected $fillable = [
        'type',  'amount', 'amount_asset', 'type_asset_id', 'user_id', 'status'
    ];

    public $sortable = [
        'id',
        'type',
        'amount',
        'amount_asset',
        'type_asset_id',
        'status',
        'created_at',
        'updated_at',
    ];

    public function isClose()
    {
        return $this->type == self::STATUS_CLOSE;
    }

    public function isNew()
    {
        return $this->status == self::STATUS_NEW;
    }

    public function isTypeBuy()
    {
        return $this->type == self::TYPE_BUY;
    }

    public function isTypeSell()
    {
        return $this->type == self::TYPE_SELL;
    }

    public function scopeNew(Builder $query)
    {
        return $query->where('status', self::STATUS_NEW);
    }

    public function client()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function typeAsset()
    {
        return $this->belongsTo(Asset::class, 'type_asset_id', 'id');
    }
}
