<?php

namespace App\Entity\Transfer;

use App\Entity\Coinmarketcap;
use App\Entity\Contract\Contract;
use App\Entity\Handbook\TypeStatus;
use App\Entity\Handbook\TypeTwoStatus;
use App\Entity\User;
use App\Helpers\StatusHelper;
use App\Traits\CropImageTrait;
use App\Traits\StatusTransaction;
use App\Traits\TypeUserTransaction;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use App\Entity\Contract\ContractPayment;

/**
 * App\Entity\Transfer\Transaction
 *
 * @property int $id
 * @property int $user_id
 * @property float $amount
 * @property string $type_user
 * @property string $type_transaction
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $status
 * @property int|null $deposit_payment_id
 * @property int|null $credit_payment_id
 * @property string|null $number
 * @property string $type
 * @property bool $is_redeem
 * @property bool $is_sell
 * @property string|null $pay_ticket
 * @property float $amount_dai
 * @property string|null $wallet
 * @property string|null $wallet_to
 * @property int|null $coinmarketcap_id
 * @property-read User $client
 * @property-read ContractPayment|null $creditPayment
 * @property-read Contract|null $depositPayment
 * @property-read mixed $payment
 * @property-read mixed $status_name
 * @property-read mixed $status_name_html
 * @property-read mixed $type_name
 * @property-read mixed $type_name_html
 * @property-read mixed $type_transaction_name
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction active()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction approve()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction close()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction investor()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction new()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction notApprove()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction trader()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction waitClose()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereAmountDai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereCoinmarketcapId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereCreditPaymentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereDepositPaymentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereIsRedeem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereIsSell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction wherePayTicket($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereTypeTransaction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereTypeUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereWallet($value)
 * @mixin \Eloquent
 * @property-read Coinmarketcap|null $coinmarketcap
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereWalletTo($value)
 */
class Transaction extends Model
{
    use Sortable, TypeUserTransaction, StatusTransaction, CropImageTrait;

    const TYPE_CREDIT = 'credit';
    const TYPE_DEPOSIT = 'deposit';
    const TYPE_DEPOSIT_DAI = 'deposit_dai';
    const TYPE_PURCHASE = 'purchase';//покупка активов
    const TYPE_PAYOUT = 'payout';//вывод прибыли
    const TYPE_CONTRIBUTION = 'contribution';//Взнос
    const TYPE_STAKING_EURO = 'staking_euro';//Стейкинг в EURO
    const TYPE_STAKING_DAI = 'staking_dai';//Стейкинг в DAI
    const TYPE_TRANSFER_ASSET = 'transfer_asset';//Цифра в кошелек
    const TYPE_TRADER_ACTIVE_EURO = 'trader_active_euro';//Актив в Евро
    const TYPE_TRADER_PAY_IN = 'trader_pay_in';//Трейдер покупка
    const TYPE_INVESTOR_PAY_IN_DAI = 'inv_pay_in_dai';//DAI в кошелек
    const TYPE_TRADER_CONTRACT_SELL = 'trader_contract_sell';//Досрочная продажа
    const TYPE_TRADER_CONTRACT_DELAYS_SELL = 'trader_contract_delays_sell';//Продажа просрочки
    const TYPE_BONUS = 'bonus';//Вывести бонус

    const NOTY_TRADER_CONTRACT_START = 11; //Договор холдера начат
    const NOTY_TRADER_WITHDRAWAL_EURO = 12; // в Евро
    const NOTY_TRADER_PREMATURE_SALE_APPROVAL = 16; //Досрочная продажа одобрена
    const NOTY_STAKING_CONTRACT_START = 21; //Договор холдера начат
    const NOTY_STAKING_EURO = 26; //Стейкинг в EURO
    const NOTY_STAKING_DAI = 28; //Стейкинг в EURO
    const NOTY_TRADER_ACTIVE_EURO = 29; //Актив в Евро
    const NOTY_TRANSFER_ASSET = 37; //Цифра в кошелек
    const NOTY_TRADER_CONTRACT_FINISHED = 38; //Договор холдера завершен
    const NOTY_TRADER_CONTRACT_DELAYS_SELL = 39; //Досрочная продажа одобрена

    const TYPE_IN = 'in';//входящие
    const TYPE_OUT = 'out';//исходящие
    const TYPE_REQUEST = 'request';//запрос
    const TYPE_CONTR = 'contr';//Взнос
    const TYPE_TRANSFER = 'transfer';//перевод

    protected $table = 'transactions';

    protected $guarded = ['id'];

    protected $casts = [
        'is_redeem' => 'boolean',
        'is_sell' => 'boolean',
    ];

    public $sortable = [
        'id',
        'user_id',
        'number',
        'deposit_payment_id',
        'credit_payment_id',
        'amount',
        'type_user',
        'type_transaction',
        'status',
        'is_redeem',
        'is_sell',
        'wallet',
        'wallet_to',
        'created_at',
        'updated_at',
    ];

    public static function typeList()
    {
        return [
            self::TYPE_CREDIT => 'Кредит',
            self::TYPE_DEPOSIT => 'Депозит',
            self::TYPE_DEPOSIT_DAI => 'Депозит в DAI',
            self::TYPE_PURCHASE => 'Покупка',
            self::TYPE_PAYOUT => 'Трейдер вывод',
            self::TYPE_TRANSFER_ASSET => 'Цифра в кошелек',
            self::TYPE_CONTRIBUTION => 'Взнос',
            self::TYPE_STAKING_EURO => 'Свободные в Евро',
            self::TYPE_STAKING_DAI => 'Стейкинг в DAI',
            self::TYPE_TRADER_ACTIVE_EURO => 'Актив в Евро',
            self::TYPE_TRADER_PAY_IN => 'Трейдер покупка',
            self::TYPE_INVESTOR_PAY_IN_DAI => 'DAI в кошелек',
            self::TYPE_TRADER_CONTRACT_SELL => 'Досрочная продажа',
            self::TYPE_TRADER_CONTRACT_DELAYS_SELL => 'Продажа просрочки',
        ];
    }

    public function getTypeNameHtmlAttribute()
    {
        $type = TypeTwoStatus::where('key', $this->type)->first();
        if (!$type) {
            return '';
        }

        if ($this->isTypeRequest()) {
            return '<span class="label label-primary">' . $type->name . '</span>';
        }
        if ($this->isTypeIn()) {
            return '<span class="label label-info">' . $type->name . '</span>';
        }
        if ($this->isTypeOut()) {
            return '<span class="label label-danger">' . $type->name . '</span>';
        }
        if ($this->isContr()) {
            return '<span class="label label-warning">' . $type->name . '</span>';
        }
        return '<span class="label label-default">' . $type->name . '</span>';
    }

    public function getSumFreeActive()
    {
        return $this->client->contractFreeAssets()
            ->whereHas('typeAsset', function ($q) {
                $q->where('coinmarketcap_id', $this->coinmarketcap_id);
            })
            ->sum('amount_asset');
    }

    public function isTypeIn()
    {
        return $this->type == self::TYPE_IN;
    }

    public function isTypeOut()
    {
        return $this->type == self::TYPE_OUT;
    }

    public function isContr()
    {
        return $this->type == self::TYPE_CONTR;
    }

    public function isTypeRequest()
    {
        return $this->type == self::TYPE_REQUEST;
    }

    public function isDeposit()
    {
        return $this->type_transaction == self::TYPE_DEPOSIT;
    }

    public function isDepositDai()
    {
        return $this->type_transaction == self::TYPE_DEPOSIT_DAI;
    }

    public function isCredit()
    {
        return $this->type_transaction == self::TYPE_CREDIT;
    }

    public function isTraderContractSell()
    {
        return $this->type_transaction == self::TYPE_TRADER_CONTRACT_SELL;
    }

    public function isTraderContractDalaysSell()
    {
        return $this->type_transaction == self::TYPE_TRADER_CONTRACT_DELAYS_SELL;
    }

    public function isPayout()
    {
        return $this->type_transaction == self::TYPE_PAYOUT;
    }

    public function isContribution()
    {
        return $this->type_transaction == self::TYPE_CONTRIBUTION;
    }

    public function isStakingEuro()
    {
        return $this->type_transaction == self::TYPE_STAKING_EURO;
    }

    public function isStakingDai()
    {
        return $this->type_transaction == self::TYPE_STAKING_DAI;
    }

    public function isBonus()
    {
        return $this->type_transaction == self::TYPE_BONUS;
    }

    public function isTransferAsset()
    {
        return $this->type_transaction == self::TYPE_TRANSFER_ASSET;
    }

    public function isTraderActiveEuro()
    {
        return $this->type_transaction == self::TYPE_TRADER_ACTIVE_EURO;
    }

    public function isTraderPayIn()
    {
        return $this->type_transaction == self::TYPE_TRADER_PAY_IN;
    }

    public function isInvestorPayInDai()
    {
        return $this->type_transaction == self::TYPE_INVESTOR_PAY_IN_DAI;
    }

    public function getTypeTransactionNameAttribute()
    {
        $lastType = TypeStatus::query()->where('key', self::TYPE_TRADER_CONTRACT_DELAYS_SELL)->first();
        if (!$lastType){
            $name = self::typeList()[self::TYPE_TRADER_CONTRACT_DELAYS_SELL];
            TypeStatus::query()->create(['key' => self::TYPE_TRADER_CONTRACT_DELAYS_SELL, 'name' => $name]);
        }
        $type = TypeStatus::where('key', $this->type_transaction)->first();
        if (!$type) {
            return '';
        }
        return $type->name;
    }

    public function getPaymentAttribute()
    {
        if ($this->type_transaction == self::TYPE_CREDIT) {
            $creditPayment = $this->creditPayment;
            if ($this->isTypeOut() && $creditPayment) {
                $contract = $creditPayment->contract;
                return $contract && $contract->pay_ticket_out ? $contract->getPhotoUrl('pay_ticket_out') : null;
            }
            return $creditPayment && $creditPayment->pay_ticket ? $creditPayment->getPhotoUrl('pay_ticket') : null;
        }
        if ($this->type_transaction == self::TYPE_DEPOSIT) {
            $depositPayment = $this->depositPayment;
            if ($this->isTypeOut()) {
                return $depositPayment && $depositPayment->pay_ticket_out ? $depositPayment->getPhotoUrl('pay_ticket_out') : null;
            } else {
                return $depositPayment && $depositPayment->pay_ticket ? $depositPayment->getPhotoUrl('pay_ticket') : null;
            }
        }
        return null;
    }

    public function client()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function depositPayment()
    {
        return $this->belongsTo(Contract::class, 'deposit_payment_id', 'id');
    }

    public function creditPayment()
    {
        return $this->belongsTo(ContractPayment::class, 'credit_payment_id', 'id');
    }

    public function coinmarketcap()
    {
        return $this->belongsTo(Coinmarketcap::class, 'coinmarketcap_id', 'id');
    }

    public function coin()
    {
        $coinValue = '';
        $coin = $this->coinmarketcap;
        if ($coin){
            $coinValue = $coin->symbol;
        }
        return $coinValue;
    }

    public function type()
    {
        return __('site.'.$this->type_transaction);
    }

    public function status()
    {
        return __('site.'.$this->status);
    }

    public function canUserReject()
    {
        return $this->type_transaction != Transaction::TYPE_TRADER_CONTRACT_DELAYS_SELL
            && $this->status == StatusHelper::STATUS_NEW;
    }
}
