<?php

namespace App\Entity;

use Carbon\Carbon;
use Kyslik\ColumnSortable\Sortable;
use Spatie\TranslationLoader\LanguageLine;

/**
 * App\Entity\Translation
 *
 * @property int $id
 * @property string $group
 * @property string $key
 * @property array $text
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Translation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Translation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Translation query()
 * @method static \Illuminate\Database\Eloquent\Builder|Translation sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Translation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Translation whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Translation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Translation whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Translation whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Translation whereUpdatedAt($value)
 * @mixin \Eloquent
 */

class Translation extends LanguageLine
{
    use Sortable;

    const group_admin = 'admin';
    const group_front = 'site';

    protected $table = 'language_lines';

    protected $sortable = ['id', 'group', 'key', 'text'];
}
