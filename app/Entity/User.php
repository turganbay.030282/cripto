<?php

namespace App\Entity;

use App\Entity\Contract\Contract;
use App\Entity\Contract\ContractFreeAsset;
use App\Entity\Handbook\Asset;
use App\Entity\Handbook\CreditLevel;
use App\Entity\Handbook\InvestorRank;
use App\Entity\Investor\DepositLineVerification;
use App\Entity\Investor\InvestorRankVerification;
use App\Entity\Statistic\StatisticPromoCode;
use App\Entity\Trader\CreditLevelUrVerification;
use App\Entity\Trader\CreditLevelVerification;
use App\Entity\Trader\CreditLineVerification;
use App\Entity\Trader\PortfolioGraph;
use App\Entity\Transfer\BonusPayment;
use App\Entity\Transfer\Transaction;
use App\Helpers\StatusHelper;
use App\Traits\CropImageTrait;
use App\Traits\Role;
use App\Traits\StatusUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\User
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $password
 * @property string $role
 * @property string $status
 * @property string $photo
 * @property string $position
 * @property string $email_verify_token
 * @property double $balance_trader
 * @property double $balance_investor
 * @property Carbon $email_verified_at
 * @property bool $is_investor
 * @property Carbon $last_login_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $credit_level_id
 * @property int $investor_rank_id
 * @property bool $notify_news
 * @property bool $notify_promo
 * @property bool $notify_by_email
 * @property bool $notify_by_sms
 * @property bool $notify_by_push
 * @property string $promo_code
 * @property int $referer_id
 * @property bool $phone_verified
 * @property bool $google2fa_enable
 * @property string $google2fa_secret
 * @property int $investor_rank
 * @property bool $is_ur
 * @property string $lang
 * @property double $payout_balance
 * @property string $contract_number
 * @property string $contract_payment_number
 * @property string $contract_payment_numbers
 * @property UserVerification[] $userVerifications
 * @property UserVerification $userVerification
 * @property UserChange[] $userChanges
 * @property UserChange $userChange
 * @property CreditLevel $creditLevel
 * @property InvestorRank $investorRank
 * @property CreditLevelVerification[] $creditLevelVerifications
 * @property CreditLevelVerification $creditLevelVerification
 * @property CreditLevelUrVerification[] $creditLevelUrVerifications
 * @property CreditLevelUrVerification $creditLevelUrVerification
 * @property InvestorRankVerification[] $investorRankVerifications
 * @property InvestorRankVerification $investorRankVerification
 * @property CreditLineVerification[] $creditLineVerifications
 * @property CreditLineVerification $creditLineVerification
 * @property DepositLineVerification[] $depositLineVerifications
 * @property DepositLineVerification $depositLineVerification
 * @property Contract[] $contracts
 * @property User $referer
 * @property Notifications[] $notifications
 * @property PortfolioGraph[] $portfolioGraphs
 * @property CodeVerifiedField[] $codeVerifies
 * @property Transaction[] $transactions
 * @property ContractFreeAsset[] $contractFreeAssets
 * @property string|null $remember_token
 * @property bool $is_verification
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property float $free_active
 * @property-read int|null $code_verifies_count
 * @property-read int|null $contract_free_assets_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Contract[] $contractInvestors
 * @property-read int|null $contract_investors_count
 * @property-read int|null $contracts_count
 * @property-read int|null $credit_level_ur_verifications_count
 * @property-read int|null $credit_level_verifications_count
 * @property-read int|null $credit_line_verifications_count
 * @property-read int|null $deposit_line_verifications_count
 * @property-read mixed $credit_line
 * @property-read mixed $deposit_euro_number
 * @property-read mixed $deposit_line
 * @property-read mixed $full_name
 * @property-read mixed $role_name
 * @property-read mixed $status_name
 * @property-read int|null $investor_rank_verifications_count
 * @property-read Transaction|null $lastTransaction
 * @property-read int|null $notifications_count
 * @property-read int|null $portfolio_graphs_count
 * @property-read int|null $transactions_count
 * @property-read int|null $user_changes_count
 * @property-read int|null $user_verifications_count
 * @property-read string|null $bank_number
 * @method static Builder|User active()
 * @method static Builder|User admin()
 * @method static Builder|User block()
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static Builder|User query()
 * @method static Builder|User sortable($defaultParameters = null)
 * @method static Builder|User user()
 * @method static Builder|User wait()
 * @method static Builder|User whereBalanceInvestor($value)
 * @method static Builder|User whereBalanceTrader($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereCreditLevelId($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereEmailVerifyToken($value)
 * @method static Builder|User whereFirstName($value)
 * @method static Builder|User whereFreeActive($value)
 * @method static Builder|User whereGoogle2faEnable($value)
 * @method static Builder|User whereGoogle2faSecret($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereInvestorRank($value)
 * @method static Builder|User whereInvestorRankId($value)
 * @method static Builder|User whereIsInvestor($value)
 * @method static Builder|User whereIsUr($value)
 * @method static Builder|User whereIsVerification($value)
 * @method static Builder|User whereLang($value)
 * @method static Builder|User whereLastLoginAt($value)
 * @method static Builder|User whereLastName($value)
 * @method static Builder|User whereNotifyByEmail($value)
 * @method static Builder|User whereNotifyByPush($value)
 * @method static Builder|User whereNotifyBySms($value)
 * @method static Builder|User whereNotifyNews($value)
 * @method static Builder|User whereNotifyPromo($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User wherePayoutBalance($value)
 * @method static Builder|User wherePhone($value)
 * @method static Builder|User wherePhoneVerified($value)
 * @method static Builder|User wherePhoto($value)
 * @method static Builder|User wherePosition($value)
 * @method static Builder|User wherePromoCode($value)
 * @method static Builder|User whereRefererId($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereRole($value)
 * @method static Builder|User whereStatus($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|BonusPayment[] $bonusPayments
 * @property-read int|null $bonus_payments_count
 * @property-read mixed $amount_bonus
 * @property-read mixed $bonus_payment_amount
 * @property-read mixed $bonus_total
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $referrals
 * @property-read int|null $referrals_count
 * @property-read \Illuminate\Database\Eloquent\Collection|StatisticPromoCode[] $statisticPromoCodes
 * @property-read int|null $statistic_promo_codes_count
 * @method static Builder|User whereBankNumber($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\UserBalance[] $balances
 * @property-read int|null $balances_count
 */
class User extends Authenticatable
{
    use Notifiable, StatusUser, Role, Sortable, SoftDeletes, CropImageTrait;

    public const STATUS_BLOCK = 'block';
    public const STATUS_WAIT = 'wait';
    public const STATUS_ACTIVE = 'active';

    public const ROLE_ADMIN = 'admin';
    public const ROLE_USER = 'user';

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
        'role',
        'status',
        'photo',
        'position',
        'balance_trader',
        'balance_investor',
        'free_active',
        'email_verify_token',
        'last_login_at',
        'balance',
        'is_investor',
        'is_verification',
        'credit_level_id',
        'investor_rank_id',
        'investor_rank',
        'notify_news',
        'notify_promo',
        'notify_by_email',
        'notify_by_sms',
        'notify_by_push',
        'promo_code',
        'referer_id',
        'phone_verified',
        'google2fa_enable',
        'google2fa_secret',
        'is_ur',
        'lang',
        'bank_number',
        'payout_balance'
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'google2fa_secret',
    ];

    protected $visible = ['full_name'];

    public $sortable = [
        'id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
        'role',
        'status',
        'balance_trader',
        'balance_investor',
        'last_login_at',
        'bank_number',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_login_at' => 'datetime',
        'is_investor' => 'boolean',
        'is_verification' => 'boolean',
        'notify_news' => 'boolean',
        'notify_promo' => 'boolean',
        'notify_by_email' => 'boolean',
        'notify_by_sms' => 'boolean',
        'notify_by_push' => 'boolean',
        'phone_verified' => 'boolean',
        'is_ur' => 'boolean',
    ];

    public function getWalletsNameAttribute($coinMarketCapId)
    {
        $names = $this->wallets->pluck('wallet')->all();
        if (!$names) {
            return "";
        }

        return implode(', ', $names);
    }

    public function getBalanceByCoin($coinMarketCapId)
    {
        $asset = Asset::where('coinmarketcap_id', $coinMarketCapId)->first();
        if (!$asset) {
            return 0;
        }

        $balance = $this->balances()->where('asset_id', $asset->id)->first();
        if (!$balance) {
            return 0;
        }

        return $balance->amount;
    }

    public function moneyToBalance($coinId, $value)
    {
        $asset = Asset::forInvestor()->where(
            'coinmarketcap_id', $coinId
        )->firstOrFail();

        /** @var UserBalance $balance */
        $balance = $this->balances()->where('asset_id', $asset->id)->first();

        if (!$balance) {
            $this->balances()->create([
                'asset_id' => $asset->id,
                'amount' => $value
            ]);
        } else {
            $balance->update([
                'amount' => format_number($balance->amount + $value)
            ]);
        }
    }

    public function hasFirstPayIn()
    {
        return $this->transactions()
            ->approve()
            ->where('type_transaction', Transaction::TYPE_CONTRIBUTION)
            ->exists();
    }

    public function getBalanceInvestorActivities()
    {
        return $this->contracts()->deposit()->active()
            ->whereHas('depositTransaction', function ($q) {
                $q->approve();
            })
            ->sum('amount_asset');
    }

    public function changeLocale($locale): void
    {
        $this->update(['lang' => $locale]);
    }

    public function hasContract(): bool
    {
        return $this->contracts()->credit()->exists();
    }

    public function hasDeposit(): bool
    {
        return $this->contracts()->deposit()->exists();
    }

    public function isReferral(): bool
    {
        return $this->referer_id ? true : false;
    }

    public function hasExpiredContractPayment(): bool
    {
        return $this->contracts()->credit()->approve()
            ->whereHas('payments', function ($q) {
                $q->where('status', StatusHelper::STATUS_NEW);
                $q->where('date', '<=', \Carbon\Carbon::now()->endOfDay());
            })->exists();
    }

    public function getAmountBonusAttribute()//amount_bonus
    {
        $setting = Setting::first();

        return format_number(
            Contract::whereHas('client', function ($query) {
                $query->where('referer_id', $this->id);
            })->where('type', Contract::TYPE_DEPOSIT)->sum('amount') * $setting->percent_ref_deposit / 100 +
            Contract::whereHas('client', function ($query) {
                $query->where('referer_id', $this->id);
            })->where('type', Contract::TYPE_CREDIT)->sum('amount') * $setting->percent_ref_credit / 100 -
            $this->bonusPayments()->sum('amount')
        );
    }

    public function getContractNumberAttribute()//contract_number
    {
        $i = 1;
        $number = $this->id . ($this->contracts->count() + $i);

        while (Contract::where('number', $number)->exists()) {
            $i++;
            $number = $this->id . ($this->contracts->count() + $i);
        }

        return $number;
    }

    public function getContractPaymentNumberAttribute()//contract_payment_number
    {
        $i = 1;
        $number = $this->id . $this->contract_number . ($this->transactions->count() + $i);

        while (Transaction::where('number', $number)->exists()) {
            $i++;
            $number = $this->id . $this->contract_number . ($this->transactions->count() + $i);
        }

        return $number;
    }

    public function getContractPaymentNumbersAttribute()//contract_payment_numbers
    {
        $i = 1;
        $number = $this->id . $this->contract_number . ($this->transactions->count() + $i);

        while (Transaction::where('number', $number)->exists()) {
            $i++;
            $number = $this->id . $this->contract_number . ($this->transactions->count() + $i);
        }
        $numbers = [];
        $credits = $this->contracts()->credit()->approve()->get();
        foreach ($credits as $credit){
            $numbers[] = $number;
            $number++;
        }
        return implode(', ', $numbers);
    }

    public function getDepositEuroNumberAttribute()//deposit_euro_number
    {
        $count = Transaction::where('type_transaction', Transaction::TYPE_CONTRIBUTION)->count() + 1;
        if ($count < 10) {
            return '000' . $count;
        }
        if ($count < 100) {
            return '00' . $count;
        }
        if ($count < 1000) {
            return '0' . $count;
        }
        return $count;
    }

    public function setGoogle2faSecretAttribute($value)
    {
        $this->attributes['google2fa_secret'] = encrypt($value);
    }

    public function getGoogle2faSecretAttribute($value)
    {
        return decrypt($value);
    }

    public function getFullNameAttribute()
    {
        return $this->last_name . ' ' . $this->first_name;
    }

    public function getBonusTotalAttribute()
    {
        $setting = Setting::first();

        return format_number(
            Contract::whereHas('client', function ($query) {
                $query->where('referer_id', $this->id);
            })->where('type', Contract::TYPE_DEPOSIT)->sum('amount') * $setting->percent_ref_deposit / 100 +
            Contract::whereHas('client', function ($query) {
                $query->where('referer_id', $this->id);
            })->where('type', Contract::TYPE_CREDIT)->sum('amount') * $setting->percent_ref_credit / 100
        );
    }

    public function getBonusPaymentAmountAttribute()
    {
        return $this->bonusPayments()->sum('amount');
    }

    public function getCreditLineAttribute() //credit_line
    {
        return $this->creditLineVerifications()->verify()->sum('amount');
    }

    public function getDepositLineAttribute() //deposit_line
    {
        return $this->depositLineVerifications()->verify()->sum('amount');
    }

    public function getEmailForVerification()
    {
        return $this->email;
    }

//    public function sendPasswordResetNotification($token)
//    {
//        $this->notify(new ResetPassword($token));
//    }

    public function userVerifications()
    {
        return $this->hasMany(UserVerification::class, 'user_id', 'id');
    }

    public function bonusPayments()
    {
        return $this->hasMany(BonusPayment::class, 'user_id', 'id');
    }

    public function userVerification()
    {
        return $this->hasOne(UserVerification::class, 'user_id', 'id')->latest();
    }

    public function userChanges()
    {
        return $this->hasMany(UserChange::class, 'user_id', 'id');
    }

    public function creditLevelVerifications()
    {
        return $this->hasMany(CreditLevelVerification::class, 'user_id', 'id');
    }

    public function creditLevelVerification()
    {
        return $this->hasOne(CreditLevelVerification::class, 'user_id', 'id')->latest();
    }

    public function creditLevelUrVerifications()
    {
        return $this->hasMany(CreditLevelUrVerification::class, 'user_id', 'id');
    }

    public function creditLevelUrVerification()
    {
        return $this->hasOne(CreditLevelUrVerification::class, 'user_id', 'id')->latest();
    }

    public function investorRankVerifications()
    {
        return $this->hasMany(InvestorRankVerification::class, 'user_id', 'id');
    }

    public function investorRankVerification()
    {
        return $this->hasOne(InvestorRankVerification::class, 'user_id', 'id')->latest();
    }

    public function creditLevel()
    {
        return $this->hasOne(CreditLevel::class, 'id', 'credit_level_id')->latest();
    }

    public function investorRank()
    {
        return $this->hasOne(InvestorRank::class, 'id', 'investor_rank_id')->latest();
    }

    public function creditLineVerifications()
    {
        return $this->hasMany(CreditLineVerification::class, 'user_id', 'id');
    }

    public function creditLineVerification()
    {
        return $this->hasOne(CreditLineVerification::class, 'user_id', 'id')->latest();
    }

    public function depositLineVerifications()
    {
        return $this->hasMany(DepositLineVerification::class, 'user_id', 'id');
    }

    public function depositLineVerification()
    {
        return $this->hasOne(DepositLineVerification::class, 'user_id', 'id')->latest();
    }

    public function contracts()
    {
        return $this->hasMany(Contract::class, 'user_id', 'id');
    }

    public function contractInvestors()
    {
        return $this->hasMany(Contract::class, 'user_id', 'id')
            ->deposit();
    }

    public function referer()
    {
        return $this->belongsTo(User::class, 'referer_id', 'id');
    }

    public function referrals()
    {
        return $this->hasMany(User::class, 'referer_id', 'id');
    }

    public function statisticPromoCodes()
    {
        return $this->hasMany(StatisticPromoCode::class, 'user_id', 'id');
    }

    public function notifications()
    {
        return $this->hasMany(Notifications::class, 'user_id', 'id');
    }

    public function portfolioGraphs()
    {
        return $this->hasMany(PortfolioGraph::class, 'user_id', 'id');
    }

    public function codeVerifies()
    {
        return $this->hasMany(CodeVerifiedField::class, 'user_id', 'id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'user_id', 'id');
    }

    public function lastTransaction()
    {
        return $this->hasOne(Transaction::class, 'user_id', 'id')->latest();
    }

    public function contractFreeAssets()
    {
        return $this->hasMany(ContractFreeAsset::class, 'user_id', 'id');
    }

    public function balances()
    {
        return $this->hasMany(UserBalance::class, 'user_id', 'id');
    }

    public function wallets()
    {
        return $this->hasMany(UserWallet::class, 'user_id', 'id');
    }
}
