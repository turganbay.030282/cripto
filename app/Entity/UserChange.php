<?php

namespace App\Entity;

use App\Helpers\ArrayHelper;
use App\Traits\CropImageTrait;
use App\Traits\FileTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\UserChange
 *
 * @property int $id
 * @property int $user_id
 * @property int $manager_id
 * @property string $first_name
 * @property string $last_name
 * @property string $birthday
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $card_id
 * @property string $status
 * @property string $comment
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User $user
 * @property User $manager
 * @property-read mixed $full_name
 * @property-read mixed $status_name
 * @method static Builder|UserChange history()
 * @method static Builder|UserChange newModelQuery()
 * @method static Builder|UserChange newQuery()
 * @method static Builder|UserChange query()
 * @method static Builder|UserChange reject()
 * @method static Builder|UserChange sortable($defaultParameters = null)
 * @method static Builder|UserChange verify()
 * @method static Builder|UserChange wait()
 * @method static Builder|UserChange whereAddress($value)
 * @method static Builder|UserChange whereBirthday($value)
 * @method static Builder|UserChange whereCardId($value)
 * @method static Builder|UserChange whereComment($value)
 * @method static Builder|UserChange whereCreatedAt($value)
 * @method static Builder|UserChange whereEmail($value)
 * @method static Builder|UserChange whereFirstName($value)
 * @method static Builder|UserChange whereId($value)
 * @method static Builder|UserChange whereLastName($value)
 * @method static Builder|UserChange whereManagerId($value)
 * @method static Builder|UserChange wherePhone($value)
 * @method static Builder|UserChange whereStatus($value)
 * @method static Builder|UserChange whereUpdatedAt($value)
 * @method static Builder|UserChange whereUserId($value)
 * @mixin \Eloquent
 */
class UserChange extends Model
{
    use CropImageTrait, Sortable;

    public const STATUS_WAIT = 'wait';
    public const STATUS_REJECT = 'declined';
    public const STATUS_APPROVED = 'approved';

    protected $table = 'user_changes';

    protected $guarded = ['id'];

    protected $casts = [
        'birthday' => 'date'
    ];

    public static function listStatus()
    {
        return [
            self::STATUS_WAIT => 'На проверке',
            self::STATUS_APPROVED => 'Подтвержден',
            self::STATUS_REJECT => 'Отклонен',
        ];
    }

    public function isWait()
    {
        return $this->status == self::STATUS_WAIT;
    }

    public function isReject()
    {
        return $this->status == self::STATUS_REJECT;
    }

    public function getFullNameAttribute()
    {
        return $this->last_name . ' ' . $this->first_name;
    }

    public function getStatusNameAttribute()
    {
        return ArrayHelper::getValue(self::listStatus(), $this->status);
    }

    public function getBirthdayAttribute()
    {
        return $this->attributes['birthday'] ? Carbon::parse($this->attributes['birthday'])->format('d.m.Y') : null;
    }

    public function setBirthdayAttribute($value)
    {
        $this->attributes['birthday'] = $value ? Carbon::parse($value)->format('Y-m-d') : null;
    }

    public function scopeReject(Builder $query)
    {
        return $query->where('status', self::STATUS_REJECT);
    }

    public function scopeVerify(Builder $query)
    {
        return $query->where('status', self::STATUS_APPROVED);
    }

    public function scopeWait(Builder $query)
    {
        return $query->where('status', self::STATUS_WAIT);
    }

    public function scopeHistory(Builder $query)
    {
        return $query->where('status', '<>', self::STATUS_WAIT);
    }

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
