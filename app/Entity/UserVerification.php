<?php

namespace App\Entity;

use App\Traits\CropImageTrait;
use App\Traits\StatusVerification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Entity\UserVerification
 *
 * @property int $id
 * @property int $user_id
 * @property int $manager_id
 * @property string $last_name
 * @property string $citizen
 * @property string $name
 * @property string $birthday
 * @property string $address
 * @property string $phone
 * @property string $type_doc
 * @property string $card_id
 * @property string $card_id_back
 * @property string $status
 * @property string $comment
 * @property string $external_id
 * @property array $data_api
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User $user
 * @property User $manager
 * @property-read mixed $full_name
 * @property-read mixed $status_name
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification history()
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification reject()
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification sortable($defaultParameters = null)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification verify()
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification wait()
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereCardIdBack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereCitizen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereDataApi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereTypeDoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserVerification whereUserId($value)
 * @mixin \Eloquent
 */
class UserVerification extends Model
{
    use CropImageTrait, Sortable, StatusVerification;

    public const STATUS_WAIT = 'wait';
    public const STATUS_ERROR = 'error';
    public const STATUS_NEEDS_REVIEW = 'needs-review';
    public const STATUS_REJECT = 'declined';
    public const STATUS_APPROVED = 'approved';

    const DOCUMENT_TYPE_PASSPORT = "passport";
    const DOCUMENT_TYPE_ID_CARD = "id-card";
    const DOCUMENT_TYPE_DRIVING_LICENCE = "driving-licence";
    const DOCUMENT_TYPE_RESIDENCE_PERMIT = "residence-permit";

    const DOCUMENT_KIND_FRONT = "front";
    const DOCUMENT_KIND_BACK = "back";
    const DOCUMENT_KIND_SINGLE_PAGE = "single-page";

    public static function lisTypeDocuments()
    {
        return [
            self::DOCUMENT_TYPE_PASSPORT => trans('site.doc.type-passport'),
            self::DOCUMENT_TYPE_ID_CARD => trans('site.doc.type-id-card'),
            self::DOCUMENT_TYPE_RESIDENCE_PERMIT => trans('site.doc.type-residence-permit'),
        ];
    }

    protected $table = 'user_verifications';

    protected $guarded = ['id'];

    protected $casts = [
        'birthday' => 'date',
        'data_api' => 'array',
    ];

    public function getFullNameAttribute()
    {
        return $this->last_name . ' ' . $this->name;
    }

    public function getBirthdayAttribute()
    {
        return $this->attributes['birthday'] ? Carbon::parse($this->attributes['birthday'])->format('d.m.Y') : null;
    }

    public function setBirthdayAttribute($value)
    {
        $this->attributes['birthday'] = $value ? Carbon::parse($value)->format('Y-m-d') : null;
    }

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
