<?php

namespace App\Events\Admin;

use App\Entity\Mail\AdminMailing;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AdminMailingEvent
{
    use Dispatchable, SerializesModels;

    public $adminMailing;

    public function __construct(AdminMailing $adminMailing)
    {
        $this->adminMailing = $adminMailing;
    }

}
