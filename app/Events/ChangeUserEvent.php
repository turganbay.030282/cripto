<?php

namespace App\Events;

use App\Entity\User;
use App\Entity\UserChange;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ChangeUserEvent
{
    use Dispatchable, SerializesModels;

    public $user;
    public $userChange;

    public function __construct(UserChange $userChange, User $user)
    {
        $this->user = $user;
        $this->userChange = $userChange;
    }
}
