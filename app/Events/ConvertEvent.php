<?php

namespace App\Events;

use App\Entity\Transfer\Convert;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ConvertEvent
{
    use Dispatchable, SerializesModels;

    public $convert;

    public function __construct(Convert $convert)
    {
        $this->convert = $convert;
    }
}
