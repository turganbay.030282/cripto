<?php

namespace App\Events;

use App\Entity\Trader\CreditLevelVerification;
use App\Entity\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class CreditLevelEvent
{
    use Dispatchable, SerializesModels;

    public $user;
    public $creditLevelVerification;

    public function __construct(CreditLevelVerification $creditLevelVerification, User $user)
    {
        $this->user = $user;
        $this->creditLevelVerification = $creditLevelVerification;
    }
}
