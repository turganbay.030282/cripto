<?php

namespace App\Events;

use App\Entity\Trader\CreditLevelUrVerification;
use App\Entity\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class CreditLevelUrEvent
{
    use Dispatchable, SerializesModels;

    public $user;
    public $creditLevelUrVerification;

    public function __construct(CreditLevelUrVerification $creditLevelUrVerification, User $user)
    {
        $this->user = $user;
        $this->creditLevelUrVerification = $creditLevelUrVerification;
    }
}
