<?php

namespace App\Events;

use App\Entity\Form;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class FormUserEvent
{
    use Dispatchable, SerializesModels;

    public $form;

    public function __construct(Form $form)
    {
        $this->form = $form;
    }
}
