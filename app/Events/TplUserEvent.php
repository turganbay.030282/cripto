<?php

namespace App\Events;

use App\Entity\Tpl\NotifyText;
use App\Entity\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class TplUserEvent
{

    use Dispatchable, SerializesModels;

    public $user;
    public $notifyText;
    public $text;
    public $attachments;

    public function __construct(User $user, NotifyText $notifyText, array $attachments = [], string $text = '')
    {
        $this->user = $user;
        $this->notifyText = $notifyText;
        $this->text = $text;
        $this->attachments = $attachments;
    }

}
