<?php

namespace App\Events\User;

use App\Entity\Mail\AdminMailing;
use App\Entity\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PromoEvent
{
    use Dispatchable, SerializesModels;

    public $email;
    public $user;

    public function __construct(User $user, string $email)
    {
        $this->email = $email;
        $this->user = $user;
    }
}
