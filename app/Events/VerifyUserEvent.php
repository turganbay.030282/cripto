<?php

namespace App\Events;

use App\Entity\User;
use App\Entity\UserVerification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class VerifyUserEvent
{
    use Dispatchable, SerializesModels;

    public $user;
    public $verification;

    public function __construct(UserVerification $verification, User $user)
    {
        $this->user = $user;
        $this->verification = $verification;
    }
}
