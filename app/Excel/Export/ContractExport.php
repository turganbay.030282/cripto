<?php

namespace App\Excel\Export;

use App\Entity\ClientService;
use App\Entity\Contract\Contract;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ContractExport implements FromView, ShouldAutoSize
{
    public $contract;

    public function __construct(Contract $contract)
    {
        $this->contract = $contract;
    }

    public function view(): View
    {
        return view('admin.contracts._pdf', [
            'contract' => $this->contract
        ]);
    }

}
