<?php

namespace App\Excel\Export;

use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;

class RefPartnersExport implements FromView, ShouldAutoSize
{
    public $items;

    public function __construct($items)
    {
        $this->items = $items;
    }

    public function view(): View
    {
        return view('admin.statistics._excel_partners', [
            'items' => $this->items
        ]);
    }

}
