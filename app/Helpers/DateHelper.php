<?php

namespace App\Helpers;

use Carbon\Carbon;

class DateHelper
{
    const day_week1 = 1;
    const day_week2 = 2;
    const day_week3 = 3;
    const day_week4 = 4;
    const day_week5 = 5;
    const day_week6 = 6;
    const day_week7 = 0;

    public static function listDayWeek()
    {
        return [
            self::day_week1 => 'Понедельник',
            self::day_week2 => 'Вторник',
            self::day_week3 => 'Среда',
            self::day_week4 => 'Четверг',
            self::day_week5 => 'Пятница',
            self::day_week6 => 'Суббота',
            self::day_week7 => 'Воскресенье',
        ];
    }

    public static function listDayWeekShort()
    {
        return [
            self::day_week1 => 'ПН',
            self::day_week2 => 'ВТ',
            self::day_week3 => 'СР',
            self::day_week4 => 'ЧТ',
            self::day_week5 => 'ПТ',
            self::day_week6 => 'СБ',
            self::day_week7 => 'ВС',
        ];
    }

    public static function activeSessionDates(array $days)
    {
        $array = [];
        foreach ($days as $day) {
            $array[] = self::listDayWeekShort()[$day->dayOfWeek];
        }
        return $array;
    }

    public static function listMonth()
    {
        return [
            1 => 'янв.',
            2 => 'фев.',
            3 => 'мар',
            4 => 'апр.',
            5 => 'май',
            6 => 'июн',
            7 => 'июл',
            8 => 'авг',
            9 => 'сен',
            10 => 'окт',
            11 => 'ноя',
            12 => 'дек',
        ];
    }

    public static function arrayMonthToString($arrayMonths)
    {
        $array = [];

        return $array;
    }


}
