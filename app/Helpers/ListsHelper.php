<?php

namespace App\Helpers;

class ListsHelper
{
    public static function sex()
    {
        return [
            trans('site.cabinet.credit-level-male') => trans('site.cabinet.credit-level-male'),
            trans('site.cabinet.credit-level-female') => trans('site.cabinet.credit-level-female'),
            trans('site.cabinet.credit-level-sex-other') => trans('site.cabinet.credit-level-sex-other')
        ];
    }

    public static function typeAccommodations()
    {
        return [
            trans('site.cabinet.credit-level-type-own') => trans('site.cabinet.credit-level-type-own'),
            trans('site.cabinet.credit-level-type-rent') => trans('site.cabinet.credit-level-type-rent'),
            trans('site.cabinet.credit-level-type-with-parents') => trans('site.cabinet.credit-level-type-with-parents')
        ];
    }

    public static function educations()
    {
        return [
            trans('site.cabinet.credit-level-education-initial') => trans('site.cabinet.credit-level-education-initial'),
            trans('site.cabinet.credit-level-education-middle') => trans('site.cabinet.credit-level-education-middle'),
            trans('site.cabinet.credit-level-education-high') => trans('site.cabinet.credit-level-education-high'),
        ];
    }

    public static function bool()
    {
        return [
            trans('site.cabinet.credit-level-yes') => trans('site.cabinet.credit-level-yes'),
            trans('site.cabinet.credit-level-no') => trans('site.cabinet.credit-level-no'),
        ];
    }

    public static function incomeSource()
    {
        return [
            trans('site.cabinet.credit-income-salary') => trans('site.cabinet.credit-income-salary'),
            trans('site.cabinet.credit-income-pension') => trans('site.cabinet.credit-income-pension'),
            trans('site.cabinet.credit-income-allowance') => trans('site.cabinet.credit-income-allowance'),
            trans('site.cabinet.credit-income-from') => trans('site.cabinet.credit-income-from'),
        ];
    }

    public static function creditType()
    {
        return [
            trans('site.cabinet.credit-credit-consumer') => trans('site.cabinet.credit-credit-consumer'),
            trans('site.cabinet.credit-credit-consolidation') => trans('site.cabinet.credit-credit-consolidation'),
            trans('site.cabinet.credit-credit-auto') => trans('site.cabinet.credit-credit-auto'),
            trans('site.cabinet.credit-credit-secured-loan') => trans('site.cabinet.credit-credit-secured-loan'),
            trans('site.cabinet.credit-credit-business-loan') => trans('site.cabinet.credit-credit-business-loan'),
            trans('site.cabinet.credit-credit-loan') => trans('site.cabinet.credit-credit-loan'),
            trans('site.cabinet.credit-credit-land-loan') => trans('site.cabinet.credit-credit-land-loan'),
            trans('site.cabinet.credit-credit-student-loan') => trans('site.cabinet.credit-credit-student-loan'),
            trans('site.cabinet.credit-credit-other') => trans('site.cabinet.credit-credit-other'),
        ];
    }
}
