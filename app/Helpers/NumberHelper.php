<?php

namespace App\Helpers;

class NumberHelper
{
    public static function format_number($number)
    {
        if ($number == 0)
            return 0;

        return number_format($number, 2, '.', '');
    }

    public static function format_number_round($number, $decimals)
    {
        return number_format($number, $decimals, '.', '');
    }

    public static function format_number_cripto($number)
    {
        return number_format($number, 8, '.', '');
    }

    public static function format_number_with_thousand($number)
    {
        return number_format($number, 2, '.', ' ');
    }

    public static function calcTraderAnnuityFirstPayment($amount, $period, $rate)
    {
        if (!$period || !$rate) {
            return 0;
        }

        $monthPercent = self::format_number_round(floatval($rate / (100 * 12)), 4);
        $coefficient = self::format_number_round(floatval($monthPercent * ((pow(1 + $monthPercent, $period)) / (pow(1 + $monthPercent, $period) - 1))), 4);
        return self::format_number($amount * $coefficient);
    }

    public static function calcTraderAnnuityFirstPaymentRevert($amount, $period, $rate)
    {
        $monthPercent = self::format_number_round(floatval($rate / (100 * 12)), 4);
        $coefficient = self::format_number_round(floatval($monthPercent * ((pow(1 + $monthPercent, $period)) / (pow(1 + $monthPercent, $period) - 1))), 4);
        return self::format_number($amount / $coefficient);
    }

    public static function calcTraderFirstPayment($amount, $rate, $period)
    {
        return ($amount * (($rate / 100) * $period / 12) + $amount) / $period;
    }

    public static function calcInvestorFirstPayment($amount, $rate)
    {
        return $amount * (($rate / 100) / 12);
    }
}
