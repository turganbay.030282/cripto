<?php

namespace App\Helpers;

class PublishHelper
{

    const publish_off = 1;
    const publish_on = 2;

    public static function statusList()
    {
        return [
            self::publish_off => 'Нет',
            self::publish_on => 'Да',
        ];
    }

}
