<?php

namespace App\Helpers;

class StatusHelper
{
    CONST STATUS_NEW = 'new';
    CONST STATUS_APPROVE = 'approve';
    CONST STATUS_WAIT_CLOSE = 'wait_close';
    CONST STATUS_CLOSE = 'close';
    CONST STATUS_REJECT = 'reject';
}
