<?php

namespace App\Helpers;

class StringHelper
{

    public static function phone($phone)
    {
        return str_replace([' ', '\s', '(', '}', '-'],'',$phone);
    }
}
