<?php

namespace App\Helpers;

class TypeUserHelper
{
    CONST TYPE_USER_TRADER = 'trader';
    CONST TYPE_USER_INVESTOR = 'investor';

    public static function typeUserList()
    {
        return [
            self::TYPE_USER_TRADER => 'Трейдер',
            self::TYPE_USER_INVESTOR => 'Инвестор',
        ];
    }
}
