<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Mail\AdminMailing;
use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Services\AdminMailing\AdminMailingService;
use App\Services\AdminMailing\Repositories\EloquentAdminMailingRepository;
use App\Services\AdminMailing\Requests\AdminMailingRequest;
use App\Services\AdminMailing\Requests\FilterRequest;

class AdminMailingsController extends Controller
{
    private $service;
    private $repository;

    public function __construct(AdminMailingService $service, EloquentAdminMailingRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $items = $this->repository->search($request)->paginate(50);
        $listListUser = AdminMailing::listListTo();

        return view('admin.admin-mailings.index', compact('items', 'listListUser'));
    }

    public function create()
    {
        $listListUser = AdminMailing::listListTo();
        $users = User::user()->get()->pluck('full_name', 'id')->all();
        return view('admin.admin-mailings.create', compact('listListUser', 'users'));
    }

    public function store(AdminMailingRequest $request)
    {
        try {
            $adminMailing = $this->service->new($request);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.admin-mailings.create')->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->route('admin.admin-mailings.show', $adminMailing)->with('success', 'Запись создана');
    }

    public function show(AdminMailing $adminMailing)
    {
        $adminMailingUsers = $adminMailing->adminMailingUsers;
        return view('admin.admin-mailings.show', compact('adminMailing', 'adminMailingUsers'));
    }

}
