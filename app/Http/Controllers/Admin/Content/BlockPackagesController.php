<?php

namespace App\Http\Controllers\Admin\Content;

use App\Entity\Content\BlockPackage;
use App\Http\Controllers\Controller;
use App\Services\BlockPackage\BlockPackageService;
use App\Services\BlockPackage\Repositories\EloquentBlockPackageRepository;
use App\Services\BlockPackage\Requests\CreateRequest;
use App\Services\BlockPackage\Requests\FilterRequest;
use App\Services\BlockPackage\Requests\UpdateRequest;
use Illuminate\Http\Request;

class BlockPackagesController extends Controller
{
    private $service;
    private $repository;

    public function __construct(BlockPackageService $service, EloquentBlockPackageRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $items = $this->repository->search($request)->paginate(20);
        $isArchive = false;
        return view('admin.content.block-packages.index', compact('items', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $items = $this->repository->search($request)->onlyTrashed()->paginate(20);
        $isArchive = true;
        return view('admin.content.block-packages.index', compact('items', 'isArchive'));
    }

    public function restore(Request $request)
    {
        $this->service->restore($request->get('id'));
        return redirect()->back();
    }

    public function create()
    {
        return view('admin.content.block-packages.create');
    }

    public function store(CreateRequest $request)
    {
        try {
            $blockPackage = $this->service->new($request);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.block-packages.create')->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->route('admin.content.block-packages.show', $blockPackage)->with('success', 'Запись создана');
    }

    public function show(BlockPackage $blockPackage)
    {
        return view('admin.content.block-packages.show', compact('blockPackage'));
    }

    public function edit(BlockPackage $blockPackage)
    {
        return view('admin.content.block-packages.edit', compact('blockPackage'));
    }

    public function update(UpdateRequest $request, BlockPackage $blockPackage)
    {
        try {
            $this->service->edit($request, $blockPackage);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.block-packages.edit', $blockPackage)->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->route('admin.content.block-packages.show', $blockPackage)->with('success', 'Запись изменена');
    }

    public function destroy(BlockPackage $blockPackage)
    {
        $blockPackage->delete();
        return redirect()->route('admin.content.block-packages.index')->with('success', 'Запись удалена');
    }

}
