<?php

namespace App\Http\Controllers\Admin\Content;

use App\Entity\Content\Blog\BlogCategory;
use App\Http\Controllers\Controller;
use App\Services\Blog\BlogService;
use App\Services\Blog\Repositories\EloquentBlogRepository;
use App\Services\Blog\Requests\CreateCategoryRequest;
use App\Services\Blog\Requests\FilterRequest;
use App\Services\Blog\Requests\UpdateCategoryRequest;
use Illuminate\Http\Request;

class BlogCategoriesController extends Controller
{
    private $service;
    private $repository;

    public function __construct(BlogService $service, EloquentBlogRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $items = $this->repository->searchCategory($request)->paginate(20);
        $isArchive = false;
        return view('admin.content.blog-categories.index', compact('items', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $items = $items = $this->repository->searchCategory($request)->onlyTrashed()->paginate(20);
        $isArchive = true;

        return view('admin.content.blog-categories.index', compact('items', 'isArchive'));
    }

    public function restore (Request $request)
    {
        BlogCategory::onlyTrashed()
            ->where('id', $request->get('id'))
            ->restore();
        return redirect()->back();
    }

    public function create()
    {
        return view('admin.content.blog-categories.create');
    }

    public function store(CreateCategoryRequest $request)
    {
        try{
            $blogCategory = $this->service->newCategory($request);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.blog-categories.create')->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.content.blog-categories.show', $blogCategory)->with('success', 'Запись создана');
    }

    public function show(BlogCategory $blogCategory)
    {
        return view('admin.content.blog-categories.show', compact('blogCategory'));
    }

    public function edit(BlogCategory $blogCategory)
    {
        return view('admin.content.blog-categories.edit', compact('blogCategory'));
    }

    public function update(UpdateCategoryRequest $request, BlogCategory $blogCategory)
    {
        try{
            $this->service->editCategory($request, $blogCategory);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.blog-categories.edit', $blogCategory)->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.content.blog-categories.show', $blogCategory)->with('success', 'Запись изменена');
    }

    public function destroy(BlogCategory $blogCategory)
    {
        $blogCategory->delete();
        return redirect()->route('admin.content.blog-categories.index')->with('success', 'Запись удалена');
    }

}
