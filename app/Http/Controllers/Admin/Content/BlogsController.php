<?php

namespace App\Http\Controllers\Admin\Content;

use App\Entity\Content\Blog\BlogItem;
use App\Entity\Setting;
use App\Helpers\PublishHelper;
use App\Http\Controllers\Controller;
use App\Services\Blog\BlogService;
use App\Services\Blog\Repositories\EloquentBlogRepository;
use App\Services\Blog\Requests\CreateRequest;
use App\Services\Blog\Requests\FilterRequest;
use App\Services\Blog\Requests\UpdateRequest;
use Illuminate\Http\Request;

class BlogsController extends Controller
{
    private $service;
    private $repository;

    public function __construct(BlogService $service, EloquentBlogRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $items = $this->repository->search($request)->paginate(20);
        $statuses = PublishHelper::statusList();
        $isArchive = false;
        return view('admin.content.blogs.index', compact('items', 'statuses', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $items = $items = $this->repository->search($request)->onlyTrashed()->paginate(20);
        $statuses = PublishHelper::statusList();
        $isArchive = true;

        return view('admin.content.blogs.index', compact('items', 'statuses', 'isArchive'));
    }

    public function restore (Request $request)
    {
        BlogItem::onlyTrashed()
            ->where('id', $request->get('id'))
            ->restore();
        return redirect()->back();
    }

    public function create()
    {
        $setting = Setting::first();
        $statuses = PublishHelper::statusList();
        $categories = $this->repository->getCategory()->pluck('title', 'id')->all();
        return view('admin.content.blogs.create', compact('statuses', 'categories', 'setting'));
    }

    public function store(CreateRequest $request)
    {
        try{
            $blog = $this->service->new($request);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.blogs.create')->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.content.blogs.show', $blog)->with('success', 'Запись создана');
    }

    public function show(BlogItem $blog)
    {
        return view('admin.content.blogs.show', compact('blog', 'statuses'));
    }

    public function edit(BlogItem $blog)
    {
        $setting = Setting::first();
        $statuses = PublishHelper::statusList();
        $categories = $this->repository->getCategory()->pluck('title', 'id')->all();
        return view('admin.content.blogs.edit', compact('blog', 'statuses', 'categories', 'setting'));
    }

    public function update(UpdateRequest $request, BlogItem $blog)
    {
        try{
            $this->service->edit($request, $blog);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.blogs.edit', $blog)->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.content.blogs.show', $blog)->with('success', 'Запись изменена');
    }

    public function destroy(BlogItem $blog)
    {
        $blog->delete();
        return redirect()->route('admin.content.blogs.index')->with('success', 'Запись удалена');
    }

}
