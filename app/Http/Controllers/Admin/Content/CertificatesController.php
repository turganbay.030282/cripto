<?php

namespace App\Http\Controllers\Admin\Content;

use App\Entity\Content\Certificate;
use App\Entity\Setting;
use App\Http\Controllers\Controller;
use App\Services\Certificate\CertificateService;
use App\Services\Certificate\Repositories\EloquentCertificateRepository;
use App\Services\Certificate\Requests\CreateRequest;
use App\Services\Certificate\Requests\FilterRequest;
use App\Services\Certificate\Requests\UpdateRequest;
use Illuminate\Http\Request;

class CertificatesController extends Controller
{
    private $service;
    private $repository;

    public function __construct(CertificateService $service, EloquentCertificateRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $items = $this->repository->search($request)->paginate(20);
        $isArchive = false;
        return view('admin.content.certificates.index', compact('items', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $items = $this->repository->search($request)->onlyTrashed()->paginate(20);
        $isArchive = true;
        return view('admin.content.certificates.index', compact('items', 'isArchive'));
    }

    public function restore(Request $request)
    {
        $this->service->restore($request->get('id'));
        return redirect()->back();
    }

    public function create()
    {
        $setting = Setting::first();
        return view('admin.content.certificates.create', compact('setting'));
    }

    public function store(CreateRequest $request)
    {
        try {
            $certificate = $this->service->new($request);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.certificates.create')->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->route('admin.content.certificates.show', $certificate)->with('success', 'Запись создана');
    }

    public function show(Certificate $certificate)
    {
        return view('admin.content.certificates.show', compact('certificate'));
    }

    public function edit(Certificate $certificate)
    {
        $setting = Setting::first();
        return view('admin.content.certificates.edit', compact('certificate', 'setting'));
    }

    public function update(UpdateRequest $request, Certificate $certificate)
    {
        try {
            $this->service->edit($request, $certificate);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.certificates.edit', $certificate)->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->route('admin.content.certificates.show', $certificate)->with('success', 'Запись изменена');
    }

    public function destroy(Certificate $certificate)
    {
        $certificate->delete();
        return redirect()->route('admin.content.certificates.index')->with('success', 'Запись удалена');
    }

}
