<?php

namespace App\Http\Controllers\Admin\Content;

use App\Entity\Content\Faq\FaqCategory;
use App\Http\Controllers\Controller;
use App\Services\Faq\FaqService;
use App\Services\Faq\Repositories\EloquentFaqRepository;
use App\Services\Faq\Requests\CreateCategoryRequest;
use App\Services\Faq\Requests\FilterRequest;
use App\Services\Faq\Requests\UpdateCategoryRequest;
use Illuminate\Http\Request;

class FaqCategoriesController extends Controller
{
    private $service;
    private $repository;

    public function __construct(FaqService $service, EloquentFaqRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $items = $this->repository->searchCategory($request)->paginate(20);
        $isArchive = false;
        return view('admin.content.faq-categories.index', compact('items', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $items = $items = $this->repository->searchCategory($request)->onlyTrashed()->paginate(20);
        $isArchive = true;

        return view('admin.content.faq-categories.index', compact('items', 'isArchive'));
    }

    public function restore (Request $request)
    {
        FaqCategory::onlyTrashed()
            ->where('id', $request->get('id'))
            ->restore();
        return redirect()->back();
    }

    public function create()
    {
        return view('admin.content.faq-categories.create');
    }

    public function store(CreateCategoryRequest $request)
    {
        try{
            $faqCategory = $this->service->newCategory($request);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.faq-categories.create')->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.content.faq-categories.show', $faqCategory)->with('success', 'Запись создана');
    }

    public function show(FaqCategory $faqCategory)
    {
        return view('admin.content.faq-categories.show', compact('faqCategory'));
    }

    public function edit(FaqCategory $faqCategory)
    {
        return view('admin.content.faq-categories.edit', compact('faqCategory'));
    }

    public function update(UpdateCategoryRequest $request, FaqCategory $faqCategory)
    {
        try{
            $this->service->editCategory($request, $faqCategory);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.faq-categories.edit', $faqCategory)->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.content.faq-categories.show', $faqCategory)->with('success', 'Запись изменена');
    }

    public function destroy(FaqCategory $faqCategory)
    {
        $faqCategory->delete();
        return redirect()->route('admin.content.faq-categories.index')->with('success', 'Запись удалена');
    }

}
