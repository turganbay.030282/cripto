<?php

namespace App\Http\Controllers\Admin\Content;

use App\Entity\Content\Faq\FaqItem;
use App\Http\Controllers\Controller;
use App\Services\Faq\FaqService;
use App\Services\Faq\Repositories\EloquentFaqRepository;
use App\Services\Faq\Requests\CreateRequest;
use App\Services\Faq\Requests\FilterRequest;
use App\Services\Faq\Requests\UpdateRequest;
use Illuminate\Http\Request;

class FaqsController extends Controller
{
    private $service;
    private $repository;

    public function __construct(FaqService $service, EloquentFaqRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $items = $this->repository->search($request)->paginate(20);
        $categories = $this->repository->getCategory()->pluck('title', 'id')->all();
        $isArchive = false;
        return view('admin.content.faqs.index', compact('items', 'categories', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $items = $items = $this->repository->search($request)->onlyTrashed()->paginate(20);
        $categories = $this->repository->getCategory()->pluck('title', 'id')->all();
        $isArchive = true;

        return view('admin.content.faqs.index', compact('items', 'categories', 'isArchive'));
    }

    public function restore (Request $request)
    {
        FaqItem::onlyTrashed()
            ->where('id', $request->get('id'))
            ->restore();
        return redirect()->back();
    }

    public function create()
    {
        $categories = $this->repository->getCategory()->pluck('title', 'id')->all();
        return view('admin.content.faqs.create', compact('categories'));
    }

    public function store(CreateRequest $request)
    {
        try{
            $faq = $this->service->new($request);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.faqs.create')->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.content.faqs.show', $faq)->with('success', 'Запись создана');
    }

    public function show(FaqItem $faq)
    {
        return view('admin.content.faqs.show', compact('faq'));
    }

    public function edit(FaqItem $faq)
    {
        $categories = $this->repository->getCategory()->pluck('title', 'id')->all();
        return view('admin.content.faqs.edit', compact('faq', 'categories'));
    }

    public function update(UpdateRequest $request, FaqItem $faq)
    {
        try{
            $this->service->edit($request, $faq);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.faqs.edit', $faq)->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.content.faqs.show', $faq)->with('success', 'Запись изменена');
    }

    public function destroy(FaqItem $faq)
    {
        $faq->delete();
        return redirect()->route('admin.content.faqs.index')->with('success', 'Запись удалена');
    }

}
