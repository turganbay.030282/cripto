<?php

namespace App\Http\Controllers\Admin\Content;

use App\Entity\Content\PageAbout;
use App\Http\Controllers\Controller;
use App\Services\Page\PageService;
use App\Services\Page\Requests\PageAboutRequest;

class PageAboutController extends Controller
{
    private $service;

    public function __construct(PageService $service)
    {
        $this->service = $service;
    }

    public function edit()
    {
        $pageAbout = PageAbout::firstOrFail();
        return view('admin.content.page-about.edit', compact('pageAbout'));
    }

    public function update(PageAboutRequest $request, PageAbout $pageAbout)
    {
        try {
            $this->service->editPageAbout($request, $pageAbout);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Запись изменена');
    }

}
