<?php

namespace App\Http\Controllers\Admin\Content;

use App\Entity\Content\PageBonus;
use App\Http\Controllers\Controller;
use App\Services\Page\PageService;
use App\Services\Page\Requests\PageBonusRequest;

class PageBonusController extends Controller
{
    private $service;

    public function __construct(PageService $service)
    {
        $this->service = $service;
    }

    public function edit()
    {
        $pageBonus = PageBonus::firstOrFail();
        return view('admin.content.page-bonus.edit', compact('pageBonus'));
    }

    public function update(PageBonusRequest $request, PageBonus $pageBonu)
    {
        try {
            $this->service->editPageBonus($request, $pageBonu);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Запись изменена');
    }

}
