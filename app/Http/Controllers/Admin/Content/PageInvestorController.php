<?php

namespace App\Http\Controllers\Admin\Content;

use App\Entity\Content\PageInvestor;
use App\Http\Controllers\Controller;
use App\Services\Page\PageService;
use App\Services\Page\Requests\PageInvestorRequest;

class PageInvestorController extends Controller
{
    private $service;

    public function __construct(PageService $service)
    {
        $this->service = $service;
    }

    public function edit()
    {
        $pageInvestor = PageInvestor::firstOrFail();
        return view('admin.content.page-investor.edit', compact('pageInvestor'));
    }

    public function update(PageInvestorRequest $request, PageInvestor $pageInvestor)
    {
        try {
            $this->service->editPageInvestor($request, $pageInvestor);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Запись изменена');
    }
}
