<?php

namespace App\Http\Controllers\Admin\Content;

use App\Entity\Content\PageMain;
use App\Http\Controllers\Controller;
use App\Services\Page\PageService;
use App\Services\Page\Requests\PageMainRequest;

class PageMainController extends Controller
{
    private $service;

    public function __construct(PageService $service)
    {
        $this->service = $service;
    }

    public function edit()
    {
        $pageMain = PageMain::firstOrFail();
        return view('admin.content.page-main.edit', compact('pageMain'));
    }

    public function update(PageMainRequest $request, PageMain $pageMain)
    {
        try {
            $this->service->editPageMain($request, $pageMain);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Запись изменена');
    }

}
