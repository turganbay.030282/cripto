<?php

namespace App\Http\Controllers\Admin\Content;

use App\Entity\Content\Page;
use App\Helpers\PublishHelper;
use App\Http\Controllers\Controller;
use App\Services\Page\PageService;
use App\Services\Page\Repositories\EloquentPageRepository;
use App\Services\Page\Requests\CreateRequest;
use App\Services\Page\Requests\FilterRequest;
use App\Services\Page\Requests\UpdateRequest;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    private $service;
    private $repository;

    public function __construct(PageService $service, EloquentPageRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $items = $this->repository->search($request)->paginate(20);
        $statuses = PublishHelper::statusList();
        $isArchive = false;
        return view('admin.content.pages.index', compact('items', 'statuses', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $items = $this->repository->search($request)->onlyTrashed()->paginate(20);
        $statuses = PublishHelper::statusList();
        $isArchive = true;
        return view('admin.content.pages.index', compact('items', 'statuses', 'isArchive'));
    }

    public function restore(Request $request)
    {
        $this->service->restore($request->get('id'));
        return redirect()->back();
    }

    public function create()
    {
        return view('admin.content.pages.create');
    }

    public function store(CreateRequest $request)
    {
        try {
            $page = $this->service->new($request);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.pages.create')->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->route('admin.content.pages.show', $page)->with('success', 'Запись создана');
    }

    public function show(Page $page)
    {
        return view('admin.content.pages.show', compact('page'));
    }

    public function edit(Page $page)
    {
        return view('admin.content.pages.edit', compact('page'));
    }

    public function update(UpdateRequest $request, Page $page)
    {
        try {
            $this->service->edit($request, $page);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.content.pages.edit', $page)->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->route('admin.content.pages.show', $page)->with('success', 'Запись изменена');
    }

    public function destroy(Page $page)
    {
        $page->delete();

        return redirect()->route('admin.content.pages.index')->with('success', 'Запись удалена');
    }

}
