<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Contract\Contract;
use App\Entity\Handbook\Asset;
use App\Entity\Setting;
use App\Excel\Export\ContractExport;
use App\Http\Controllers\Controller;
use App\Services\Contract\Repositories\EloquentContractRepository;
use App\Services\Contract\Requests\FilterRequest;
use App\Services\Tpl\PdfService;
use Maatwebsite\Excel\Facades\Excel;

class ContractsController extends Controller
{
    /**
     * @var EloquentContractRepository
     */
    private $repository;
    private $pdfService;

    public function __construct(EloquentContractRepository $repository, PdfService $pdfService)
    {
        $this->repository = $repository;
        $this->pdfService = $pdfService;
    }

    public function index(FilterRequest $request)
    {
        $assets = Asset::get()->pluck('name', 'id')->all();
        $items = $this->repository->search($request)->paginate(50);

        return view('admin.contracts.index', compact('items', 'assets'));
    }

    public function pdf(Contract $contract)
    {
        $filename = 'ccontract-' . $contract->number . '.pdf';
        $this->pdfService->generateFromHtml(
            $contract->document, $filename, storage_path('app/public/temp/')
        );

        return response()->file(storage_path('app/public/temp/') . $filename);
    }

    public function show(Contract $contract)
    {
        $setting = Setting::first();

        return view('admin.contracts.show', compact('contract', 'setting'));
    }

    public function destroy(Contract $contract)
    {
        $contract->delete();

        return redirect()->route('admin.contracts.index');
    }
}
