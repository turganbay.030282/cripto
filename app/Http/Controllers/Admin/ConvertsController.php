<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Transfer\Convert;
use App\Http\Controllers\Controller;
use App\Services\Convert\ConvertService;
use App\Services\Convert\Repositories\ConvertRepository;
use App\Services\Convert\Requests\FilterRequest;

class ConvertsController extends Controller
{
    /**
     * @var ConvertRepository
     */
    protected $repository;

    /**
     * @var ConvertService
     */
    protected $service;

    public function __construct(
        ConvertRepository $repository,
        ConvertService $service
    )
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * @param FilterRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(FilterRequest $request)
    {
        $items = $this->repository->search($request)->paginate(30);

        return view('admin.converts.index', compact('items'));
    }

    /**
     * @param Convert $convert
     * @return \Illuminate\Http\RedirectResponse
     */
    public function close(Convert $convert)
    {
        $this->service->close($convert);

        return redirect()->back()->with('success', 'Обмен закрыт');
    }

    /**
     * @param Convert $convert
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancel(Convert $convert)
    {
        $this->service->cancel($convert);

        return redirect()->back()->with('success', 'Обмен отменен');
    }

}
