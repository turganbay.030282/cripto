<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Form;
use App\Http\Controllers\Controller;
use App\Services\Form\Repositories\EloquentFormRepository;
use App\Services\Form\Requests\FilterRequest;
use Illuminate\Http\Request;

class FormsController extends Controller
{
    /**
     * @var EloquentFormRepository
     */
    private $repository;

    public function __construct(EloquentFormRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $forms = $this->repository->search($request)->paginate(50);
        $isArchive = false;

        return view('admin.forms.index', compact('forms', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $forms = $this->repository->search($request)->onlyTrashed()->paginate(50);
        $isArchive = true;

        return view('admin.forms.index', compact('forms', 'isArchive'));
    }

    public function restore(Request $request)
    {
        Form::onlyTrashed()
            ->where('id', $request->get('id'))
            ->restore();
        return redirect()->back();
    }

    public function destroy(Form $form)
    {
        $form->delete();
        return redirect()->route('admin.forms.index');
    }

}
