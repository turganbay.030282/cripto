<?php

namespace App\Http\Controllers\Admin\Handbook;

use App\Entity\Coinmarketcap;
use \App\Entity\Handbook\Asset;
use App\Http\Controllers\Controller;
use App\Services\Handbook\AssetService;
use App\Services\Handbook\Repositories\EloquentHandbookRepository;
use App\Services\Handbook\Requests\FilterRequest;
use App\Services\Handbook\Requests\AssetsCreateRequest;
use App\Services\Handbook\Requests\AssetsUpdateRequest;
use Illuminate\Http\Request;

class AssetsController extends Controller
{
    private $service;
    private $repository;

    public function __construct(AssetService $service, EloquentHandbookRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $items = $this->repository->searchasset($request)->paginate(20);
        $types = Asset::listType();
        $isArchive = false;
        return view('admin.handbook.assets.index', compact('items', 'types', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $items = $items = $this->repository->searchasset($request)->onlyTrashed()->paginate(20);
        $types = Asset::listType();
        $isArchive = true;

        return view('admin.handbook.assets.index', compact('items', 'types', 'isArchive'));
    }

    public function restore(Request $request)
    {
        Asset::onlyTrashed()
            ->where('id', $request->get('id'))
            ->restore();
        return redirect()->back();
    }

    public function create()
    {
        $types = Asset::listType();
        $coins = [];
        return view('admin.handbook.assets.create', compact('types', 'coins'));
    }

    public function store(AssetsCreateRequest $request)
    {
        try {
            $asset = $this->service->new($request);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.handbook.assets.create')->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.handbook.assets.show', $asset)->with('success', 'Запись создана');
    }

    public function show(Asset $asset)
    {
        return view('admin.handbook.assets.show', compact('asset'));
    }

    public function edit(Asset $asset)
    {
        $types = Asset::listType();
        $coins = $asset->coinmarketcap_id ? [$asset->coinmarketcap_id => $asset->coinValue->symbol] : [];
        return view('admin.handbook.assets.edit', compact('asset', 'types', 'coins'));
    }

    public function update(AssetsUpdateRequest $request, Asset $asset)
    {
        try {
            $this->service->edit($request, $asset);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.handbook.assets.edit', $asset)->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.handbook.assets.show', $asset)->with('success', 'Запись изменена');
    }

    public function destroy(Asset $asset)
    {
        $asset->delete();
        return redirect()->route('admin.handbook.assets.index')->with('success', 'Запись удалена');
    }

    public function coins(Request $request)
    {
        $out = ['results' => ['id' => '', 'text' => '']];
        $q = $request->get("q", null);

        if ($q) {
            $arr = [];
            $items = Coinmarketcap::where(function ($query) use ($q) {
                $query->where('symbol', 'like', '%' . strtoupper($q) . '%');
            })->limit(10)->get();

            if (!$items) return json_encode($out);

            foreach ($items as $key => $item) {
                $arr[$key]["id"] = $item->id;
                $arr[$key]["text"] = $item->symbol . ': ' . $item->name;
            }
            $out['results'] = $arr;
        }
        return json_encode($out);
    }
}
