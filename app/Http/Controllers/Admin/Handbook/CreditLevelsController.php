<?php

namespace App\Http\Controllers\Admin\Handbook;

use \App\Entity\Handbook\CreditLevel;
use App\Http\Controllers\Controller;
use App\Services\Handbook\CreditLevelService;
use App\Services\Handbook\Repositories\EloquentHandbookRepository;
use App\Services\Handbook\Requests\FilterRequest;
use App\Services\Handbook\Requests\CreateRequest;
use App\Services\Handbook\Requests\UpdateRequest;
use Illuminate\Http\Request;

class CreditLevelsController extends Controller
{
    private $service;
    private $repository;

    public function __construct(CreditLevelService $service, EloquentHandbookRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $items = $this->repository->searchCreditLevel($request)->paginate(20);
        $isArchive = false;
        return view('admin.handbook.credit-levels.index', compact('items', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $items = $items = $this->repository->searchCreditLevel($request)->onlyTrashed()->paginate(20);
        $isArchive = true;

        return view('admin.handbook.credit-levels.index', compact('items', 'isArchive'));
    }

    public function restore (Request $request)
    {
        creditLevel::onlyTrashed()
            ->where('id', $request->get('id'))
            ->restore();
        return redirect()->back();
    }

    public function create()
    {
        return view('admin.handbook.credit-levels.create');
    }

    public function store(CreateRequest $request)
    {
        try{
            $creditLevel = $this->service->new($request);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.handbook.credit-levels.create')->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.handbook.credit-levels.show', $creditLevel)->with('success', 'Запись создана');
    }

    public function show(CreditLevel $creditLevel)
    {
        return view('admin.handbook.credit-levels.show', compact('creditLevel'));
    }

    public function edit(CreditLevel $creditLevel)
    {
        return view('admin.handbook.credit-levels.edit', compact('creditLevel'));
    }

    public function update(UpdateRequest $request, CreditLevel $creditLevel)
    {
        try{
            $this->service->edit($request, $creditLevel);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.handbook.credit-levels.edit', $creditLevel)->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.handbook.credit-levels.show', $creditLevel)->with('success', 'Запись изменена');
    }

    public function destroy(CreditLevel $creditLevel)
    {
        $creditLevel->delete();
        return redirect()->route('admin.handbook.credit-levels.index')->with('success', 'Запись удалена');
    }

}
