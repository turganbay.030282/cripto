<?php

namespace App\Http\Controllers\Admin\Handbook;

use App\Entity\Handbook\InvestorRank;
use App\Http\Controllers\Controller;
use App\Services\Handbook\InvestorRankService;
use App\Services\Handbook\Requests\FilterRequest;
use App\Services\Handbook\Requests\CreateRequest;
use App\Services\Handbook\Requests\UpdateRequest;
use App\Services\Handbook\Repositories\EloquentHandbookRepository;
use Illuminate\Http\Request;

class InvestorRanksController extends Controller
{
    private $service;
    private $repository;

    public function __construct(InvestorRankService $service, EloquentHandbookRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $items = $this->repository->searchInvestorRank($request)->paginate(20);
        $isArchive = false;
        return view('admin.handbook.investor-ranks.index', compact('items', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $items = $items = $this->repository->searchInvestorRank($request)->onlyTrashed()->paginate(20);
        $isArchive = true;

        return view('admin.handbook.investor-ranks.index', compact('items', 'isArchive'));
    }

    public function restore (Request $request)
    {
        investorRank::onlyTrashed()
            ->where('id', $request->get('id'))
            ->restore();
        return redirect()->back();
    }

    public function create()
    {
        return view('admin.handbook.investor-ranks.create');
    }

    public function store(CreateRequest $request)
    {
        try{
            $investorRank = $this->service->new($request);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.handbook.investor-ranks.create')->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.handbook.investor-ranks.show', $investorRank)->with('success', 'Запись создана');
    }

    public function show(InvestorRank $investorRank)
    {
        return view('admin.handbook.investor-ranks.show', compact('investorRank'));
    }

    public function edit(InvestorRank $investorRank)
    {
        return view('admin.handbook.investor-ranks.edit', compact('investorRank'));
    }

    public function update(UpdateRequest $request, InvestorRank $investorRank)
    {
        try{
            $this->service->edit($request, $investorRank);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.handbook.investor-ranks.edit', $investorRank)->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.handbook.investor-ranks.show', $investorRank)->with('success', 'Запись изменена');
    }

    public function destroy(InvestorRank $investorRank)
    {
        $investorRank->delete();
        return redirect()->route('admin.handbook.investor-ranks.index')->with('success', 'Запись удалена');
    }

}
