<?php

namespace App\Http\Controllers\Admin\Handbook;

use App\Entity\Handbook\TypeStatus;
use App\Http\Controllers\Controller;
use App\Services\Handbook\TypeTransactionService;
use App\Services\Handbook\Repositories\EloquentHandbookRepository;
use App\Services\Handbook\Requests\FilterTypeTransactionRequest;
use App\Services\Handbook\Requests\CreateTypeRequest;
use Illuminate\Http\Request;

class TypeStatusesController extends Controller
{
    private $service;
    private $repository;

    public function __construct(TypeTransactionService $service, EloquentHandbookRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $items = $this->repository->searchTypeStatus()->paginate(20);
        $isArchive = false;

        return view('admin.handbook.type-statuses.index', compact('items', 'isArchive'));
    }

    public function create()
    {
        return view('admin.handbook.type-statuses.create');
    }

    public function store(CreateTypeRequest $request)
    {
        try {
            $typeStatus = $this->service->newTypeStatus($request);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.handbook.type-statuses.create')->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->route('admin.handbook.type-statuses.index')->with('success', 'Запись создана');
    }

    public function edit(TypeStatus $typeStatus)
    {
        return view('admin.handbook.type-statuses.edit', compact('typeStatus'));
    }

    public function update(CreateTypeRequest $request, TypeStatus $typeStatus)
    {
        try {
            $this->service->editTypeStatus($request, $typeStatus);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.handbook.type-statuses.edit', $typeStatus)->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->route('admin.handbook.type-statuses.index')->with('success', 'Запись изменена');
    }

    public function destroy(TypeStatus $typeStatus)
    {
        $typeStatus->delete();

        return redirect()->route('admin.handbook.type-statuses.index')->with('success', 'Запись удалена');
    }

}
