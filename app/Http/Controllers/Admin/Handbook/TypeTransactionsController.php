<?php

namespace App\Http\Controllers\Admin\Handbook;

use \App\Entity\Handbook\TypeTransaction;
use App\Http\Controllers\Controller;
use App\Services\Handbook\TypeTransactionService;
use App\Services\Handbook\Repositories\EloquentHandbookRepository;
use App\Services\Handbook\Requests\FilterTypeTransactionRequest;
use App\Services\Handbook\Requests\TypeTransactionCreateRequest;
use App\Services\Handbook\Requests\TypeTransactionUpdateRequest;
use Illuminate\Http\Request;

class TypeTransactionsController extends Controller
{
    private $service;
    private $repository;

    public function __construct(TypeTransactionService $service, EloquentHandbookRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterTypeTransactionRequest $request)
    {
        $items = $this->repository->searchTypeTransaction($request)->paginate(20);
        $isArchive = false;
        return view('admin.handbook.type-transactions.index', compact('items', 'isArchive'));
    }

    public function archive(FilterTypeTransactionRequest $request)
    {
        $items = $items = $this->repository->searchTypeTransaction($request)->onlyTrashed()->paginate(20);
        $isArchive = true;

        return view('admin.handbook.type-transactions.index', compact('items', 'isArchive'));
    }

    public function restore (Request $request)
    {
        TypeTransaction::onlyTrashed()
            ->where('id', $request->get('id'))
            ->restore();
        return redirect()->back();
    }

    public function create()
    {
        $types = TypeTransaction::typeUserList();
        return view('admin.handbook.type-transactions.create', compact('types'));
    }

    public function store(TypeTransactionCreateRequest $request)
    {
        try{
            $typeTransaction = $this->service->new($request);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.handbook.type-transactions.create')->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.handbook.type-transactions.show', $typeTransaction)->with('success', 'Запись создана');
    }

    public function show(TypeTransaction $typeTransaction)
    {
        return view('admin.handbook.type-transactions.show', compact('typeTransaction'));
    }

    public function edit(TypeTransaction $typeTransaction)
    {
        $types = TypeTransaction::typeUserList();
        return view('admin.handbook.type-transactions.edit', compact('typeTransaction', 'types'));
    }

    public function update(TypeTransactionUpdateRequest $request, TypeTransaction $typeTransaction)
    {
        try{
            $this->service->edit($request, $typeTransaction);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.handbook.type-transactions.edit', $typeTransaction)->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.handbook.type-transactions.show', $typeTransaction)->with('success', 'Запись изменена');
    }

    public function destroy(TypeTransaction $typeTransaction)
    {
        $typeTransaction->delete();
        return redirect()->route('admin.handbook.type-transactions.index')->with('success', 'Запись удалена');
    }

}
