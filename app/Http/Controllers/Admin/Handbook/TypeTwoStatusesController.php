<?php

namespace App\Http\Controllers\Admin\Handbook;

use App\Entity\Handbook\TypeTwoStatus;
use App\Http\Controllers\Controller;
use App\Services\Handbook\TypeTransactionService;
use App\Services\Handbook\Repositories\EloquentHandbookRepository;
use App\Services\Handbook\Requests\CreateTypeRequest;
use Illuminate\Http\Request;

class TypeTwoStatusesController extends Controller
{
    private $service;
    private $repository;

    public function __construct(TypeTransactionService $service, EloquentHandbookRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $items = $this->repository->searchTypeTwoStatus()->paginate(20);
        $isArchive = false;

        return view('admin.handbook.type-two-statuses.index', compact('items', 'isArchive'));
    }

    public function create()
    {
        return view('admin.handbook.type-two-statuses.create');
    }

    public function store(CreateTypeRequest $request)
    {
        try {
            $typeTwoStatus = $this->service->newTypeTwoStatus($request);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.handbook.type-two-statuses.create')->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->route('admin.handbook.type-two-statuses.index')->with('success', 'Запись создана');
    }

    public function edit(TypeTwoStatus $typeTwoStatus)
    {
        return view('admin.handbook.type-two-statuses.edit', compact('typeTwoStatus'));
    }

    public function update(CreateTypeRequest $request, TypeTwoStatus $typeTwoStatus)
    {
        try {
            $this->service->editTypeTwoStatus($request, $typeTwoStatus);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.handbook.type-two-statuses.edit', $typeTwoStatus)->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->route('admin.handbook.type-two-statuses.index')->with('success', 'Запись изменена');
    }

    public function destroy(TypeTwoStatus $typeTwoStatus)
    {
        $typeTwoStatus->delete();

        return redirect()->route('admin.handbook.type-two-statuses.index')->with('success', 'Запись удалена');
    }

}
