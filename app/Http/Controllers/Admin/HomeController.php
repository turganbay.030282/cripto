<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Statistic\StatisticActiveSession;
use App\Entity\Statistic\StatisticUser;
use App\Entity\User;
use App\Helpers\ArrayHelper;
use App\Helpers\DateHelper;
use App\Http\Controllers\Controller;
use App\Services\Contract\Repositories\EloquentContractRepository;

class HomeController extends Controller
{
    /**
     * @var EloquentContractRepository
     */
    private $contractRepository;

    public function __construct(EloquentContractRepository $contractRepository)
    {
        $this->contractRepository = $contractRepository;
    }

    public function index()
    {
        $statistic = StatisticUser::orderBy('id', 'asc')->get();
        $month = ArrayHelper::arrayToJs($statistic->pluck('month')->all());
        $register = ArrayHelper::arrayToJs($statistic->pluck('register')->all());
        $verify = ArrayHelper::arrayToJs($statistic->pluck('verify')->all());
        $registerTrader = $statistic->sum('trader');
        $registerInvestor = $statistic->sum('investor');

        $balanceTrader = intval(User::user()->sum('balance_trader'));
        $creditContractValue = intval($this->contractRepository->creditContractValueUsers());

        $statisticActiveSessions = StatisticActiveSession::orderBy('id', 'desc')->take(7)->get()->sortBy('id');
        $statisticActiveSessionTrader = ArrayHelper::arrayToJs($statisticActiveSessions->pluck('trader')->all());
        $statisticActiveSessionInvestor = ArrayHelper::arrayToJs($statisticActiveSessions->pluck('investor')->all());
        $statisticActiveSessionDates = implode('|', DateHelper::activeSessionDates($statisticActiveSessions->pluck('date_at')->all()));

        return view('admin.home', compact(
            'statistic',
            'month',
            'register',
            'verify',
            'registerTrader',
            'registerInvestor',
            'balanceTrader',
            'creditContractValue',
            'statisticActiveSessionTrader',
            'statisticActiveSessionInvestor',
            'statisticActiveSessionDates'
        ));
    }
}
