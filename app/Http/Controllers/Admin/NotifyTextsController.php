<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Tpl\NotifyText;
use App\Entity\Tpl\NotifyTextTranslation;
use App\Http\Controllers\Controller;
use App\Services\Tpl\NotifyTextService;
use App\Services\Tpl\Repositories\NotifyTextRepository;
use App\Services\Tpl\Requests\CreateNotifyTextRequest;
use App\Services\Tpl\Requests\UpdateNotifyTextRequest;
use App\Services\Tpl\Requests\FilterNotifyTextRequest;
use Illuminate\Http\Request;

class NotifyTextsController extends Controller
{
    private $service;
    private $repository;

    public function __construct(NotifyTextService $service, NotifyTextRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterNotifyTextRequest $request)
    {
        $items = $this->repository->search($request)->paginate(20);
        $isArchive = false;

        return view('admin.notify-texts.index', compact('items', 'isArchive'));
    }

    public function create()
    {
        return view('admin.notify-texts.create');
    }

    public function store(CreateNotifyTextRequest $request)
    {
        try {
            $blog = $this->service->new($request);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.notify-texts.create')->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->route('admin.notify-texts.show', $blog)->with('success', 'Запись создана');
    }

    public function show(NotifyText $notifyText)
    {
        return view('admin.notify-texts.show', compact('notifyText'));
    }

    public function edit(NotifyText $notifyText)
    {
        return view('admin.notify-texts.edit', compact('notifyText'));
    }

    public function update(UpdateNotifyTextRequest $request, NotifyText $notifyText)
    {
        try {
            $this->service->edit($request, $notifyText);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.notify-texts.edit', $notifyText)->withInput($request->all())->with('danger', $e->getMessage());
        }

        return redirect()->route('admin.notify-texts.show', $notifyText)->with('success', 'Запись изменена');
    }

    public function destroy(NotifyText $notifyText)
    {
        if ($notifyText->active) {
            return redirect()->back()->with('danger', 'Запись не может быть удалена');
        }
        try {
            NotifyTextTranslation::query()->where('notify_text_id', $notifyText->id)->delete();
            $notifyText->delete();
            return redirect()->back()->with('success', 'Запись удалена');
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }
}
