<?php

namespace App\Http\Controllers\Admin\Referrals;

use App\Entity\Statistic\StatisticPromoCode;
use App\Http\Controllers\Controller;
use function redirect;
use function view;

class StatisticPromoCodesController extends Controller
{
    public function index()
    {
        $items = StatisticPromoCode::sortable(['created_at' => 'desc'])->paginate(50);
        return view('admin.statistic-promo-codes.index', compact('items'));
    }

    public function destroy(StatisticPromoCode $statisticPromoCode)
    {
        $statisticPromoCode->delete();
        return redirect()->route('admin.statistic-promo-codes.index');
    }
}
