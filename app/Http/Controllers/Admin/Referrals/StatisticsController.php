<?php

namespace App\Http\Controllers\Admin\Referrals;

use App\Entity\Contract\Contract;
use App\Entity\User;
use App\Excel\Export\RefPartnersExport;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class StatisticsController extends Controller
{
    public function invites()
    {
        $items = Contract::with('client')
            ->whereHas('client', function ($query) {
                $query->whereNotNull('referer_id');
            })->sortable(['created_at' => 'desc'])
            ->paginate(20);

        return view('admin.statistics.invites', compact('items'));
    }

    public function partners(Request $request)
    {
        $query = Contract::with('client.referer')
            ->has('client.referer')
            ->orderBy('created_at', 'desc');

        if ($value = $request->get('id')) {
            $query->where('id', $value);
        }
        if ($value = $request->get('name')) {
            $query->whereHas('client', function ($query) use ($value) {
                $query->where(function ($q) use ($value) {
                    $q->where('first_name', 'like', '%' . $value . '%');
                    $q->orWhere('last_name', 'like', '%' . $value . '%');
                });
                $query->whereNotNull('referer_id');
            });
        }
        if ($value = $request->get('from')) {
            $query->where('created_at', '>=', Carbon::parse($value)->startOfDay());
        }
        if ($value = $request->get('to')) {
            $query->where('created_at', '<=', Carbon::parse($value)->endOfDay());
        }

        if ($request->has('excel')) {
            return Excel::download(
                new RefPartnersExport($query->get()), 'partners' . date('Ymd') . '.xlsx'
            );
        }

        $items = $query->paginate(20);

        return view('admin.statistics.partners', compact('items'));
    }

    public function analytics()
    {
        $items = User::has('referrals')
            ->withCount('statisticPromoCodes')
            ->withCount('referrals')
            ->sortable(['created_at' => 'desc'])
            ->paginate(50);

        return view('admin.statistics.analytics', compact('items'));
    }
}
