<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Setting;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SettingsRequest;

class SettingsController extends Controller
{
    public function index()
    {
        $setting = Setting::first();

        return view('admin.settings.edit', compact('setting'));
    }

    public function update(SettingsRequest $request, Setting $setting)
    {
        $setting->update($request->except('wallet_img'));

        $wallet_img = $request['wallet_img'];
        if ($wallet_img) {
            $setting->editImage($wallet_img, null, 'wallet_img');
        }

        foreach (config('translatable.locales') as $locale) {
            $filePolitic = isset($request[$locale]['file_politic']) ? $request[$locale]['file_politic'] : null;
            if ($filePolitic) {
                $translate = $setting->translations->where('locale', $locale)->first();
                $translate->editFile($filePolitic, 'file_politic', 'settings');
            }
        }

        return redirect()->route('admin.settings.index')->with('success', trans('admin.messages.edit'));
    }
}
