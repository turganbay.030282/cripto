<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Subscribe;
use App\Http\Controllers\Controller;
use App\Services\Form\Repositories\EloquentFormRepository;
use App\Services\Form\Requests\FilterRequest;
use Illuminate\Http\Request;

class SubscribesController extends Controller
{
    /**
     * @var EloquentFormRepository
     */
    private $repository;

    public function __construct(EloquentFormRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $forms = $this->repository->searchSubscribes($request)->paginate(50);
        $isArchive = false;

        return view('admin.subscribes.index', compact('forms', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $forms = $this->repository->searchSubscribes($request)->onlyTrashed()->paginate(50);
        $isArchive = true;

        return view('admin.subscribes.index', compact('forms', 'isArchive'));
    }

    public function restore(Request $request)
    {
        Subscribe::onlyTrashed()
            ->where('id', $request->get('id'))
            ->restore();
        return redirect()->back();
    }

    public function destroy(Subscribe $subscribe)
    {
        $subscribe->delete();
        return redirect()->route('admin.subscribes.index');
    }

}
