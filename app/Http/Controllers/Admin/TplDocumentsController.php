<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Tpl\TplDocument;
use App\Http\Controllers\Controller;
use App\Services\Tpl\TplService;
use App\Services\Tpl\Repositories\TplDocumentRepository;
use App\Services\Tpl\Requests\CreateRequest;
use App\Services\Tpl\Requests\FilterRequest;
use App\Services\Tpl\Requests\UpdateRequest;
use Illuminate\Http\Request;

class TplDocumentsController extends Controller
{
    private $service;
    private $repository;

    public function __construct(TplService $service, TplDocumentRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $items = $this->repository->search($request)->paginate(20);
        $isArchive = false;
        return view('admin.tpl-documents.index', compact('items', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $items = $items = $this->repository->search($request)->onlyTrashed()->paginate(20);
        $isArchive = true;

        return view('admin.tpl-documents.index', compact('items',  'isArchive'));
    }

    public function restore (Request $request)
    {
        TplDocument::onlyTrashed()
            ->where('id', $request->get('id'))
            ->restore();
        return redirect()->back();
    }

    public function create()
    {
        return view('admin.tpl-documents.create');
    }

    public function store(CreateRequest $request)
    {
        try{
            $blog = $this->service->new($request);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.tpl-documents.create')->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.tpl-documents.show', $blog)->with('success', 'Запись создана');
    }

    public function show(TplDocument $tplDocument)
    {
        return view('admin.tpl-documents.show', compact('tplDocument'));
    }

    public function edit(TplDocument $tplDocument)
    {
        return view('admin.tpl-documents.edit', compact('tplDocument'));
    }

    public function update(UpdateRequest $request, TplDocument $tplDocument)
    {
        try{
            $this->service->edit($request, $tplDocument);
        }catch (\Exception $e){
            \Log::error('Exception', [$e]);
            return redirect()->route('admin.tpl-documents.edit', $tplDocument)->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.tpl-documents.show', $tplDocument)->with('success', 'Запись изменена');
    }

    public function destroy(TplDocument $tplDocument)
    {
        $tplDocument->delete();
        return redirect()->route('admin.tpl-documents.index')->with('success', 'Запись удалена');
    }

}
