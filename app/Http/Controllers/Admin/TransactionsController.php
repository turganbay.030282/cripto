<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Setting;
use App\Entity\Tpl\NotifyText;
use App\Entity\Transfer\Transaction;
use App\Events\TplUserEvent;
use App\Events\TransactionApprove;
use App\Helpers\StatusHelper;
use App\Http\Controllers\Controller;
use App\Services\Contract\Requests\ContractPaymentRequest;
use App\Services\Transaction\BonusPaymentService;
use App\Services\Transaction\Repositories\EloquentTransactionRepository;
use App\Services\Transaction\Requests\FilterRequest;
use App\Services\Transaction\TransactionAdminService;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    /**
     * @var EloquentTransactionRepository
     */
    private $repository;
    /**
     * @var TransactionAdminService
     */
    private $service;
    /**
     * @var BonusPaymentService
     */
    private $bonusPaymentService;

    public function __construct(
        TransactionAdminService $service,
        BonusPaymentService $bonusPaymentService,
        EloquentTransactionRepository $repository
    )
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->bonusPaymentService = $bonusPaymentService;
    }

    public function index(FilterRequest $request)
    {
        $items = $this->repository->search($request)->paginate(30);
        $setting = Setting::firstOrFail();
        return view('admin.transactions.index', compact('items', 'setting'));
    }

    public function approve(Request $request, Transaction $transaction)
    {
        try {
            $this->service->approveTransaction($transaction);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->back()->with('success', 'Платеж подтвержден');
    }

    public function reject(Transaction $transaction)
    {
        try {
            $transaction->update(['status' => StatusHelper::STATUS_REJECT]);

            switch ($transaction->type_transaction){
                case Transaction::TYPE_TRADER_ACTIVE_EURO:
                    $noty = NotifyText::find(33);
                    break;
                case Transaction::TYPE_TRANSFER_ASSET:
                    $noty = NotifyText::find(34);
                    break;
                case Transaction::TYPE_STAKING_EURO:
                    $noty = NotifyText::find(35);
                    break;
                case Transaction::TYPE_STAKING_DAI:
                    $noty = NotifyText::find(36);
                    break;
                default:
                    $noty = null;
            }

            $client = $transaction->client;
            if ($noty){
                event(new TplUserEvent($client, $noty));
            }
            event(new TransactionApprove($client));

        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->back()->with('success', 'Платеж отклонен');
    }

    public function approveBonus(Request $request, Transaction $transaction)
    {
        try {
            $this->bonusPaymentService->payoutApprove($transaction);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->back()->with('success', 'Платеж подтвержден');
    }

    public function approveTraderOutEdit(Transaction $transaction)
    {
        return view('admin.transactions.approve-trader-out', compact('transaction'));
    }

    public function approveTraderOut(ContractPaymentRequest $request, Transaction $transaction)
    {
        try {
            $this->service->approveTraderOutTransaction($request, $transaction);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.transactions.index')->with('success', 'Платеж подтвержден');
    }

    public function approveTraderIsSell(Request $request, Transaction $transaction)
    {
        try {
            $this->service->approveTraderSellTransaction($transaction);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.transactions.index')->with('success', 'Платеж подтвержден');
    }

    public function approveTraderTransaction(Request $request, Transaction $transaction)
    {
        try {
            $this->service->approveTraderTransaction($transaction);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.transactions.index')->with('success', 'Платеж подтвержден');
    }

    public function approveInvOutEdit(Transaction $transaction)
    {
        return view('admin.transactions.approve-inv-out', compact('transaction'));
    }

    public function approveInvOut(ContractPaymentRequest $request, Transaction $transaction)
    {
        try {
            $this->service->approveInvOutTransaction($request, $transaction);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.transactions.index')->with('success', 'Платеж подтвержден');
    }

    public function approveDepositOut(Request $request, Transaction $transaction)
    {
        try {
            $this->service->approveInvDepositTransaction($transaction);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.transactions.index')->with('success', 'Платеж подтвержден');
    }

    public function approveStaking(Request $request, Transaction $transaction)
    {
        try {
            $this->service->approveStakingTransaction($transaction);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.transactions.index')->with('success', 'Платеж подтвержден');
    }

    public function approveInvOutPay(Request $request, Transaction $transaction)
    {
        try {

            if($transaction->isTypeOut()){
                $this->service->approveInvTerminateContractTransaction($transaction);
            }else{
                $this->service->approveInvOutPayTransaction($transaction);
            }
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.transactions.index')->with('success', 'Платеж подтвержден');
    }

    public function approveInvPayInDai(Request $request, Transaction $transaction)
    {
        try {
            $this->service->approveInvPayInDaiTransaction($transaction);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.transactions.index')->with('success', 'Платеж подтвержден');
    }

}
