<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Translation;
use App\Http\Controllers\Controller;
use App\Services\Translation\Repositories\TranslationRepository;
use App\Services\Translation\Requests\CreateRequest;
use App\Services\Translation\Requests\Filter\FilterRequest;
use App\Services\Translation\Requests\UpdateRequest;
use App\Services\Translation\TranslationService;

class TranslationsController extends Controller
{
    private $service;
    private $repository;

    public function __construct(TranslationService $service, TranslationRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(FilterRequest $request)
    {
        $listLocale = config('translatable.locales');
        $translations = $this->repository->searchSiteTranslate($request)->paginate(100);
        return view('admin.translations.index', compact('translations', 'listGroup', 'listLocale'));
    }

    public function create()
    {
        $langs = config('translatable.locales');
        return view('admin.translations.create', compact('langs'));
    }

    public function store(CreateRequest $request)
    {
        try{
            $this->service->create($request);
        }catch (\Exception $exception){
            return redirect()->back()->with('danger', $exception->getMessage());
        }
        return redirect()->route('admin.translations.index')->with('success', trans('admin.messages.save'));
    }

    public function edit(Translation $translation)
    {
        $langs = config('translatable.locales');
        return view('admin.translations.edit', compact('translation', 'langs'));
    }

    public function update(UpdateRequest $request, Translation $translation)
    {
        try{
            $this->service->update($request, $translation);
        }catch (\Exception $exception){
            return redirect()->back()->with('danger', $exception->getMessage());
        }
        return redirect()->route('admin.translations.index')->with('success', trans('admin.messages.edit'));
    }
}
