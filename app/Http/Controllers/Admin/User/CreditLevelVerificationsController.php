<?php

namespace App\Http\Controllers\Admin\User;

use App\Entity\Handbook\CreditLevel;
use App\Entity\Trader\CreditLevelUrVerification;
use App\Entity\Trader\CreditLevelVerification;
use App\Http\Controllers\Controller;
use App\Services\User\Repositories\EloquentUserRepository;
use App\Services\User\Requests\Filter\FilterCreditLevelVerificationsRequest;
use App\Services\User\Requests\Verify\CreditLevel\VerifyCreditLevelManagerRequest;
use App\Services\User\CreditLevelVerifyService;

class CreditLevelVerificationsController extends Controller
{

    private $repository;
    private $service;

    public function __construct(EloquentUserRepository $repository, CreditLevelVerifyService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index(FilterCreditLevelVerificationsRequest $request)
    {
        $statuses = CreditLevelVerification::listStatus();
        $users = $this->repository->searchCreditLevelVerification($request)->wait()->get();
        $urUsers = $this->repository->searchCreditLevelUrVerification($request)->wait()->get();
        $users = $urUsers->merge($users);
        $isArchive = false;
        return view('admin.user.credit-level-verifications.index', compact('users', 'urUsers', 'statuses', 'isArchive'));
    }

    public function archive(FilterCreditLevelVerificationsRequest $request)
    {
        $statuses = CreditLevelVerification::listStatus();
        $users = $this->repository->searchCreditLevelVerification($request)->with('user.creditLevel')->history()->paginate(40);
        $isArchive = true;
        return view('admin.user.credit-level-verifications.index', compact('users', 'statuses', 'isArchive'));
    }

    public function archiveUr(FilterCreditLevelVerificationsRequest $request)
    {
        $statuses = CreditLevelVerification::listStatus();
        $users = $this->repository->searchCreditLevelUrVerification($request)->with('user.creditLevel')->history()->paginate(40);
        $isArchive = true;
        return view('admin.user.credit-level-verifications.index', compact('users', 'statuses', 'isArchive'));
    }

    public function show(CreditLevelVerification $creditLevelVerification)
    {
        return view('admin.user.credit-level-verifications.show', compact('creditLevelVerification'));
    }

    public function showUr(CreditLevelUrVerification $creditLevelVerification)
    {
        return view('admin.user.credit-level-verifications.show-ur', compact('creditLevelVerification'));
    }

    public function edit(CreditLevelVerification $creditLevelVerification)
    {
        $statuses = CreditLevelVerification::listStatus();
        unset($statuses[CreditLevelVerification::STATUS_WAIT]);
        $levels = CreditLevel::get()->pluck('name', 'id')->all();
        return view('admin.user.credit-level-verifications.edit', compact('statuses', 'creditLevelVerification', 'levels'));
    }

    public function update(VerifyCreditLevelManagerRequest $request, CreditLevelVerification $creditLevelVerification)
    {
        try {
            $this->service->creditLevelVerify($request, $creditLevelVerification);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.user.credit-level-verifications.index')->with('success', 'Запись изменена');
    }

    public function editUr(CreditLevelUrVerification $creditLevelUrVerification)
    {
        $statuses = CreditLevelVerification::listStatus();
        unset($statuses[CreditLevelVerification::STATUS_WAIT]);
        $levels = CreditLevel::get()->pluck('name', 'id')->all();
        return view('admin.user.credit-level-verifications.edit_ur', compact('statuses', 'creditLevelUrVerification', 'levels'));
    }

    public function updateUr(VerifyCreditLevelManagerRequest $request, CreditLevelUrVerification $creditLevelUrVerification)
    {
        try {
            $this->service->creditLevelUrVerify($request, $creditLevelUrVerification);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.user.credit-level-verifications.index')->with('success', 'Запись изменена');
    }

}
