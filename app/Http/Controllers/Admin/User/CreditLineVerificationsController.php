<?php

namespace App\Http\Controllers\Admin\User;

use App\Entity\Handbook\CreditLevel;
use App\Entity\Trader\CreditLineVerification;
use App\Http\Controllers\Controller;
use App\Services\User\Repositories\EloquentUserRepository;
use App\Services\User\Requests\Filter\FilterCreditLineVerificationsRequest;
use App\Services\User\Requests\Verify\VerifyCreditLineManagerRequest;
use App\Services\User\UserService;

class CreditLineVerificationsController extends Controller
{

    private $repository;
    private $service;

    public function __construct(EloquentUserRepository $repository, UserService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index(FilterCreditLineVerificationsRequest $request)
    {
        $statuses = CreditLineVerification::listStatus();
        $users = $this->repository->searchCreditLineVerification($request)->wait()->paginate(40);
        $isArchive = false;
        return view('admin.user.credit-line-verifications.index', compact('users', 'statuses', 'isArchive'));
    }

    public function archive(FilterCreditLineVerificationsRequest $request)
    {
        $statuses = CreditLineVerification::listStatus();
        $users = $this->repository->searchCreditLineVerification($request)->history()->paginate(40);
        $isArchive = true;
        return view('admin.user.credit-line-verifications.index', compact('users', 'statuses', 'isArchive'));
    }

    public function edit(CreditLineVerification $creditLineVerification)
    {
        $statuses = CreditLineVerification::listStatus();
        unset($statuses[CreditLineVerification::STATUS_WAIT]);
        $levels = CreditLevel::get()->pluck('name', 'id')->all();
        return view('admin.user.credit-line-verifications.edit', compact('statuses', 'creditLineVerification', 'levels'));
    }

    public function update(VerifyCreditLineManagerRequest $request, CreditLineVerification $creditLineVerification)
    {
        try {
            $this->service->creditLineVerify($request, $creditLineVerification);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.user.credit-line-verifications.index')->with('success', 'Запись изменена');
    }

}
