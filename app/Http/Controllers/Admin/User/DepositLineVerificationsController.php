<?php

namespace App\Http\Controllers\Admin\User;

use App\Entity\Investor\DepositLineVerification;
use App\Http\Controllers\Controller;
use App\Services\User\Repositories\EloquentUserRepository;
use App\Services\User\Requests\Filter\FilterDepositLineVerificationsRequest;
use App\Services\User\Requests\Verify\VerifyDepositLineManagerRequest;
use App\Services\User\UserService;

class DepositLineVerificationsController extends Controller
{
    private $repository;
    private $service;

    public function __construct(EloquentUserRepository $repository, UserService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index(FilterDepositLineVerificationsRequest $request)
    {
        $statuses = DepositLineVerification::listStatus();
        $users = $this->repository->searchDepositLineVerification($request)->wait()->paginate(40);
        $isArchive = false;
        return view('admin.user.deposit-line-verifications.index', compact('users', 'statuses', 'isArchive'));
    }

    public function archive(FilterDepositLineVerificationsRequest $request)
    {
        $statuses = DepositLineVerification::listStatus();
        $users = $this->repository->searchDepositLineVerification($request)->history()->paginate(40);
        $isArchive = true;
        return view('admin.user.deposit-line-verifications.index', compact('users', 'statuses', 'isArchive'));
    }

    public function edit(DepositLineVerification $depositLineVerification)
    {
        $statuses = DepositLineVerification::listStatus();
        unset($statuses[DepositLineVerification::STATUS_WAIT]);
        return view('admin.user.deposit-line-verifications.edit', compact('statuses', 'depositLineVerification'));
    }

    public function update(VerifyDepositLineManagerRequest $request, DepositLineVerification $depositLineVerification)
    {
        try {
            $this->service->depositLineVerify($request, $depositLineVerification);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.user.deposit-line-verifications.index')->with('success', 'Запись изменена');
    }

}
