<?php

namespace App\Http\Controllers\Admin\User;

use App\Entity\Handbook\InvestorRank;
use App\Entity\Handbook\TypeTransaction;
use App\Entity\Investor\InvestorRankVerification;
use App\Entity\Trader\CreditLevelVerification;
use App\Http\Controllers\Controller;
use App\Services\User\Repositories\EloquentUserRepository;
use App\Services\User\Requests\Filter\FilterInvestorRankVerificationsRequest;
use App\Services\User\Requests\Verify\InvestorRank\VerifyInvestRankManagerRequest;
use App\Services\User\InvestorRankService;

class InvestRankVerificationsController extends Controller
{

    private $repository;
    private $service;

    public function __construct(EloquentUserRepository $repository, InvestorRankService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index(FilterInvestorRankVerificationsRequest $request)
    {
        $statuses = InvestorRankVerification::listStatus();
        $users = $this->repository->searchInvestorRankVerification($request)->wait()->paginate(40);
        $isArchive = false;
        return view('admin.user.invest-rank-verifications.index', compact('users', 'statuses', 'isArchive'));
    }

    public function archive(FilterInvestorRankVerificationsRequest $request)
    {
        $statuses = InvestorRankVerification::listStatus();
        $users = $this->repository->searchInvestorRankVerification($request)->with('user.investorRank')->history()->paginate(40);
        $isArchive = true;
        return view('admin.user.invest-rank-verifications.index', compact('users', 'statuses', 'isArchive'));
    }

    public function show(InvestorRankVerification $investRankVerification)
    {
        $user = $investRankVerification->user;
        return view('admin.user.invest-rank-verifications.show', compact('investRankVerification', 'user'));
    }

    public function edit(InvestorRankVerification $investRankVerification)
    {
        $statuses = CreditLevelVerification::listStatus();
        unset($statuses[CreditLevelVerification::STATUS_WAIT]);
        $ranks = TypeTransaction::investor()->get()->pluck('level', 'id')->all();
        return view('admin.user.invest-rank-verifications.edit', compact('statuses', 'investRankVerification', 'ranks'));
    }

    public function update(VerifyInvestRankManagerRequest $request, InvestorRankVerification $investRankVerification)
    {
        try {
            $this->service->investRankVerify($request, $investRankVerification);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.user.invest-rank-verifications.index')->with('success', 'Запись изменена');
    }

}
