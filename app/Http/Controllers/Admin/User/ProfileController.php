<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Services\User\UserService;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    private $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function changePassword()
    {
        return view('admin.user.profile.password');
    }

    public function setPassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|string|min:6|confirmed',
        ]);
        $this->service->changePassword($request->get('password'), auth()->user());
        return redirect()->route('admin.user.profile.changePassword')->with('success', 'Пароль изменен');
    }
}
