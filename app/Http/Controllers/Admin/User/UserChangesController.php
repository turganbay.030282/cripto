<?php

namespace App\Http\Controllers\Admin\User;

use App\Entity\UserChange;
use App\Http\Controllers\Controller;
use App\Services\User\Repositories\EloquentUserRepository;
use App\Services\User\Requests\Verify\ChangeManagerRequest;
use App\Services\User\Requests\Filter\FilterChangeRequest;
use App\Services\User\UserService;

class UserChangesController extends Controller
{

    private $repository;
    private $service;

    public function __construct(EloquentUserRepository $repository, UserService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index(FilterChangeRequest $request)
    {
        $statuses = UserChange::listStatus();
        $users = $this->repository->searchChange($request)->wait()->paginate(40);
        $isArchive = false;
        return view('admin.user.user-changes.index', compact('users', 'statuses', 'isArchive'));
    }

    public function archive(FilterChangeRequest $request)
    {
        $statuses = UserChange::listStatus();
        $users = $this->repository->searchChange($request)->history()->paginate(40);
        $isArchive = true;
        return view('admin.user.user-changes.index', compact('users', 'statuses', 'isArchive'));
    }

    public function edit(UserChange $userChange)
    {
        $statuses = UserChange::listStatus();
        unset($statuses[UserChange::STATUS_WAIT]);
        return view('admin.user.user-changes.edit', compact('statuses', 'userChange'));
    }

    public function update(ChangeManagerRequest $request, UserChange $userChange)
    {
        try {
            $this->service->changeApprove($request, $userChange);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.user.user-changes.index')->with('success', 'Запись изменена');
    }

}
