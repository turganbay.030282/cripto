<?php

namespace App\Http\Controllers\Admin\User;

use App\Entity\User;
use App\Entity\UserVerification;
use App\Http\Controllers\Controller;
use App\Services\User\Repositories\EloquentUserRepository;
use App\Services\User\Requests\Filter\FilterVerificationRequest;
use App\Services\User\Requests\Verify\VerifyManagerRequest;
use App\Services\User\UserService;

class UserVerificationsController extends Controller
{

    private $repository;
    private $service;

    public function __construct(EloquentUserRepository $repository, UserService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index(FilterVerificationRequest $request)
    {
        $statuses = UserVerification::listStatus();
        $users = $this->repository->searchVerification($request)->wait()->paginate(40);
        $isArchive = false;
        return view('admin.user.user-verifications.index', compact('users', 'statuses', 'isArchive'));
    }

    public function archive(FilterVerificationRequest $request)
    {
        $statuses = UserVerification::listStatus();
        $users = $this->repository->searchVerification($request)->history()->paginate(40);
        $isArchive = true;
        return view('admin.user.user-verifications.index', compact('users', 'statuses', 'isArchive'));
    }

    public function show(UserVerification $userVerification)
    {
        return view('admin.user.user-verifications.show', compact('userVerification'));
    }

    public function edit(UserVerification $userVerification)
    {
        $statuses = UserVerification::listStatus();
        unset($statuses[UserVerification::STATUS_WAIT]);
        return view('admin.user.user-verifications.edit', compact('statuses', 'userVerification'));
    }

    public function update(VerifyManagerRequest $request, UserVerification $userVerification)
    {
        try {
            $this->service->verify($request, $userVerification);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.user.user-verifications.index')->with('success', 'Запись изменена');
    }

}
