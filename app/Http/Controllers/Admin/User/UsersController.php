<?php

namespace App\Http\Controllers\Admin\User;

use App\Entity\Setting;
use App\Entity\User;
use App\Entity\UserWallet;
use App\Http\Controllers\Controller;
use App\Services\User\Requests\CreateRequest;
use App\Services\User\Requests\UpdateRequest;
use App\Services\User\Repositories\EloquentUserRepository;
use App\Services\User\Requests\Filter\FilterRequest;
use App\Services\User\UserService;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    private $repository;
    private $service;

    public function __construct(EloquentUserRepository $repository, UserService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index(FilterRequest $request)
    {
        $statuses = User::listStatus();
        $roles = User::listRoleUser();
        $users = $this->repository->search($request)->paginate(40);
        $isArchive = false;
        return view('admin.user.users.index', compact('users', 'statuses', 'roles', 'isArchive'));
    }

    public function archive(FilterRequest $request)
    {
        $statuses = User::listStatus();
        $roles = User::listRoleUser();
        $users = $this->repository->search($request)->onlyTrashed()->paginate(40);
        $isArchive = true;
        return view('admin.user.users.index', compact('users', 'statuses', 'roles', 'isArchive'));
    }

    public function restore(Request $request)
    {
        User::onlyTrashed()
            ->where('id', $request->get('id'))
            ->restore();
        return redirect()->back();
    }

    public function show(User $user)
    {
        return view('admin.user.users.show', compact('user'));
    }

    public function create()
    {
        $setting = Setting::first();
        $statuses = User::listStatus();
        $roles = User::listRoleUser();
        return view('admin.user.users.create', compact('roles', 'statuses', 'setting'));
    }

    public function store(CreateRequest $request)
    {
        try {
            $user = $this->service->create($request);
        } catch (\Exception $e) {
            \Log::error('UsersController', [$e]);
            return redirect()->back()->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.user.users.show', $user)->with('success', 'Пользователь создан');
    }

    public function edit(User $user)
    {
        $setting = Setting::first();
        $statuses = User::listStatus();
        $roles = User::listRoleUser();
        return view('admin.user.users.edit', compact('roles', 'statuses', 'user', 'setting'));
    }

    public function update(UpdateRequest $request, User $user)
    {
        try {
            $this->service->edit($request, $user);
        } catch (\Exception $e) {
            \Log::error('UsersController', [$e]);
            return redirect()->back()->withInput($request->all())->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.user.users.show', $user)->with('success', 'Пользователь изменен');
    }

    public function destroy(User $user)
    {
        try {
            $this->service->remove($user);
        } catch (\Exception $e) {
            \Log::error('UsersController', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->route('admin.user.users.index')->with('success', 'Пользователь удален');
    }

    public function delete(Request $request)
    {
        try {
            $this->service->delete($request->get('id'));
        } catch (\Exception $e) {
            \Log::error('UsersController', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->back()->with('success', 'Пользователь удален');
    }

    public function addWallet(Request $request)
    {
        $key = $request->get('key', 0) + 1;

        return view('admin.user.users._row_wallet', ['key' => $key, 'wallet' => null])->render();
    }

    public function removeWallet(Request $request, UserWallet $wallet)
    {
        $wallet->delete();

        if ($request->ajax())
            return response()->json([]);

        return redirect()->back()->with('success', 'Запись удалена');
    }
}
