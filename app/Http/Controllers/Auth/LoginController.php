<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use App\Services\Statistic\StatisticUserService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    /**
     * @var StatisticUserService
     */
    private $statisticUserService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(StatisticUserService $statisticUserService)
    {
        $this->middleware('guest')->except('logout');
        $this->statisticUserService = $statisticUserService;
    }

    public function login(LoginRequest $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $authenticate = Auth::attempt([
            'email' => strtolower($request->get('email')),
            'password' => $request->get('password'),
        ]);
        if ($authenticate) {
            $request->session()->regenerate();
            $this->clearLoginAttempts($request);
            $user = Auth::user();

//            if ($user->isBlock()) {
//                Auth::logout();
//                return back()->with('error', 'You are blocked.');
//            }

            if ($user->isWait()) {
                Auth::logout();
                return back()->with('error', trans('site.auth.login.email-not-verify'));
            }

            if ($user->isAdmin()) {
                return redirect()->route('admin.home');
            } else {
                $this->statisticUserService->login($user);
                if($user->is_investor){
                    return redirect()->route('front.cabinet.investor.home');
                }
                return redirect()->route('front.cabinet.trader.home');
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
}
