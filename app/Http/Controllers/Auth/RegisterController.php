<?php

namespace App\Http\Controllers\Auth;

use App\Entity\Setting;
use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Services\User\PhoneService;
use App\Services\User\RegisterService;
use App\Services\User\Requests\RegisterPhoneRequest;
use App\Services\User\Requests\RegisterRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class RegisterController extends Controller
{
    protected $redirectTo = RouteServiceProvider::HOME;

    private $service;
    private $phoneService;

    public function __construct(RegisterService $service, PhoneService $phoneService)
    {
        $this->middleware('guest');
        $this->service = $service;
        $this->phoneService = $phoneService;
    }

    public function showRegistrationForm(Request $request)
    {
        if ($request->get('ref')) {
            $referrer = User::where('promo_code', $request->get('ref'))->exists();
            if ($referrer) {
                Cookie::queue('ref', $request->get('ref'));
            }
        }

        $filePolitic = null;
        $setting = Setting::first();
        $translation = $setting->translations->where('locale', \App::getLocale())->first();
        if ($translation && $translation->file_politic) {
            $filePolitic = $translation->getFileUrl('file_politic', 'settings');
        }

        return view('auth.register', compact('filePolitic'));
    }

    public function register(RegisterRequest $request)
    {
        try {
            $user = $this->service->register($request);
        } catch (\DomainException $e) {
            return redirect()->back()->withInput($request->all())->with('error', $e->getMessage());
        }
        return redirect()->route('register.showResendForm', $user->email)->with('success', trans('site.page.register-message-register'));
    }

    public function sendVerifyCode(RegisterPhoneRequest $request)
    {
        try {
            $this->phoneService->registerPhoneRequest($request->get('phone'));
            return response()->json([]);
        } catch (\DomainException $e) {
            return response()->json(['error' => $e->getMessage()]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function verify($token)
    {
        if (!$user = User::where('email_verify_token', $token)->first()) {
            return redirect()->route('login')->with('error', trans('site.page.register-message-verify-error'));
        }

        try {
            $this->service->verify($user->id);
            return redirect()->route('login')->with('success', trans('site.page.register-message-verify-success'));
        } catch (\DomainException $e) {
            return redirect()->route('login')->with('error', $e->getMessage());
        }
    }

    public function verifyEmail(Request $request, $email)
    {
        if (!$request->ajax()) {
            return redirect()->route('login');
        }

        if (!$user = User::where('email', $email)->first()) {
            return response()->json(['location' => route('register')]);
        }

        if ($user->isActive()) {
            return response()->json(['location' => route('login')]);
        }

        return response()->json([]);
    }

    public function showResendForm($email)
    {
        return view('auth.resend', compact('email'));
    }

    public function resend(Request $request)
    {
        try {
            $this->service->resend($request->get('email'));
            return redirect()->route('register.showResendForm', $request->get('email'))->with('success', trans('site.page.register-message-resend'));
        } catch (\DomainException $e) {
            return redirect()->route('register.showResendForm', $request->get('email'))->with('error', $e->getMessage());
        }
    }

}
