<?php

namespace App\Http\Controllers\Front\Cabinet\Admin;

use App\Http\Controllers\Controller;

class ConfigsController extends Controller
{

    public function index()
    {
        return view('front.cabinet.admin.configs.index');
    }

}
