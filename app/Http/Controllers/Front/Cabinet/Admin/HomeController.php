<?php

namespace App\Http\Controllers\Front\Cabinet\Admin;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function show()
    {
        return view('front.cabinet.admin.home');
    }

}
