<?php

namespace App\Http\Controllers\Front\Cabinet\Admin;

use App\Http\Controllers\Controller;

class UsersController extends Controller
{

    public function index()
    {
        return view('front.cabinet.admin.users.index');
    }

}
