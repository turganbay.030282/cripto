<?php

namespace App\Http\Controllers\Front\Cabinet;

use App\Entity\Coinmarketcap;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AjaxController extends Controller
{

    public function coinPrice(Request $request)
    {
        $coin = Coinmarketcap::find($request->get('coin'));
        if (!$coin) {
            return response()->json(['error' => 'Coin not found']);
        }
        return response()->json(['price' => format_number($coin->price)]);
    }

}
