<?php

namespace App\Http\Controllers\Front\Cabinet;

use App\Entity\Contract\Contract;
use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Services\Transaction\BonusPaymentService;
use App\Services\Transaction\Requests\BonusPayout\BonusPayoutDaiRequest;
use App\Services\Transaction\Requests\BonusPayout\BonusPayoutRequest;
use Illuminate\Http\Request;

class BonusesController extends Controller
{
    public $service;

    public function __construct(BonusPaymentService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $user = auth()->user();

        $bonuses = Contract::with('client.referer')
            ->whereHas('client', function ($query) use ($user) {
                $query->where('referer_id', $user->id);
            })->sortable(['created_at' => 'desc'])->paginate(20);

        return view('front.cabinet.bonuses.index', compact('user', 'bonuses'));
    }

    /**
     * @param BonusPayoutRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function payout(BonusPayoutRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();

        try {
            $this->service->payout($request, $user);
        } catch (\Exception $e) {
            return redirect()->back()->with('danger', $e->getMessage());
        }

        return redirect()->back();
    }

    /**
     * @param BonusPayoutDaiRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function payoutDai(BonusPayoutDaiRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();

        try {
            $this->service->payoutDai($request, $user);
        } catch (\Exception $e) {
            return redirect()->back()->with('danger', $e->getMessage());
        }

        return redirect()->back();
    }

}
