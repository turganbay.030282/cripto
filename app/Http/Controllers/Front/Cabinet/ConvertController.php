<?php

namespace App\Http\Controllers\Front\Cabinet;

use App\Entity\Content\Faq\FaqCategory;
use App\Entity\Transfer\Convert;
use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Services\Convert\ConvertService;
use App\Services\Convert\Requests\ConvertRequest;
use App\Services\Handbook\Repositories\EloquentHandbookRepository;
use Illuminate\Http\Request;

class ConvertController extends Controller
{
    protected $handbookRepository;
    protected $service;

    public function __construct(
        EloquentHandbookRepository $handbookRepository,
        ConvertService $service
    )
    {
        $this->handbookRepository = $handbookRepository;
        $this->service = $service;
    }

    public function request(ConvertRequest $request)
    {
        try {
            /** @var User $user */
            $user = auth()->user();

            $this->service->request($request, $user);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', trans('site.cabinet.form-error'));
        }

        return redirect()->back()->with('success', trans('site.page.cabinet.convert-success'));
    }


    public function conversions(Request $request)
    {
        $userConverts = Convert::query()->where('user_id', auth()->id())
            ->orderBy('created_at', 'desc')
            ->paginate(30);
        return view('front.cabinet.conversions.index', compact('userConverts'));
    }

    public function cancel(Convert $convert)
    {
        $this->service->cancel($convert);

        return redirect()->back()->with('success', 'Обмен отменен');
    }
}
