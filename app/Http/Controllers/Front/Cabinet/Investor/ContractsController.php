<?php

namespace App\Http\Controllers\Front\Cabinet\Investor;

use App\Entity\Contract\Contract;
use App\Entity\Setting;
use App\Helpers\StatusHelper;
use App\Http\Controllers\Controller;
use App\Services\Contract\ContractService;
use App\Services\Contract\Repositories\EloquentContractRepository;
use App\Services\Tpl\PdfService;

class ContractsController extends Controller
{
    protected $repository;
    protected $pdfService;
    protected $service;

    /**
     * ContractsController constructor.
     * @param EloquentContractRepository $repository
     * @param ContractService $service
     * @param PdfService $pdfService
     */
    public function __construct(
        EloquentContractRepository $repository, ContractService $service, PdfService $pdfService
    )
    {
        $this->repository = $repository;
        $this->pdfService = $pdfService;
        $this->service = $service;
    }

    public function index()
    {
        $user = auth()->user();
        $assetContracts = Contract::deposit()
            ->approve()
            ->where('user_id', $user->id)
            ->orderBy('id', 'desc')
            ->get();
        $setting = Setting::first();
        $assetContractsAmount = $assetContracts->where('status', '<>', StatusHelper::STATUS_CLOSE)->sum('amount');

        return view('front.cabinet.investor.contracts.index', compact('user', 'assetContracts', 'assetContractsAmount', 'setting'));
    }

    public function archive()
    {
        $user = auth()->user();
        $assetContracts = Contract::deposit()
            ->close()
            ->where('user_id', $user->id)
            ->orderBy('id', 'desc')
            ->get();
        $assetContractsAmount = $assetContracts->where('status', '<>', StatusHelper::STATUS_CLOSE)->sum('amount');
        $setting = Setting::first();

        return view('front.cabinet.investor.contracts.archive', compact('user', 'assetContracts', 'assetContractsAmount', 'setting'));
    }

    public function download(Contract $contract)
    {
        return $this->pdfService->downloadFromHtml($contract->document, 'deposit-' . $contract->number . '.pdf');
    }

    public function print(Contract $contract)
    {
        $filename = 'deposit-' . $contract->number . '.pdf';
        $this->pdfService->generateFromHtml($contract->document, $filename, storage_path('app/public/temp/'));
        return url('storage/temp/' . $filename);
    }

    public function terminate(Contract $contract)
    {
        try {
            $this->service->terminate($contract);
        } catch (\Exception $e) {
            \Log::error('ContractsController terminate', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->back()->with('success', trans('site.page.cabinet.contract-inv-terminate-message-success'));
    }

}
