<?php

namespace App\Http\Controllers\Front\Cabinet\Investor;

use App\Http\Controllers\Controller;
use App\Services\Transaction\Requests\PayDepositRequest;
use App\Services\Transaction\Requests\PayInDaiRequest;
use App\Services\Transaction\Requests\PayoutDaiRequest;
use App\Services\Transaction\Requests\PayoutRequest;
use App\Services\Transaction\TransactionService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DepositController extends Controller
{

    private $service;

    public function __construct(TransactionService $service)
    {
        $this->service = $service;
    }

    public function payIn(PayDepositRequest $request)
    {
        if($request->get('amount') <= 0){
            return redirect()->back()->with('danger', trans('site.cabinet.form-un-correct-amount'));
        }

        try {
            $this->service->createDeposit($request, auth()->user());
            return redirect()->back()->with('success', trans('site.cabinet.investor-deposit-pay-send'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('danger', $exception->getMessage());
        }
    }

    public function payOutEuro(PayoutRequest $request)
    {
        if($request->get('amount') <= 0){
            return redirect()->back()->with('danger', trans('site.cabinet.form-un-correct-amount'));
        }

        try {
            $this->service->createPayOutEuro($request, auth()->user());
            return redirect()->back()->with('success', trans('site.cabinet.investor-deposit-pay-out-send'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('danger', $exception->getMessage());
        }
    }

    public function payOutDai(PayoutDaiRequest $request)
    {
        if($request->get('amount') <= 0){
            return redirect()->back()->with('danger', trans('site.cabinet.form-un-correct-amount'));
        }

        try {
            $this->service->createPayOutDai($request, auth()->user());
            return redirect()->back()->with('success', trans('site.cabinet.investor-deposit-pay-out-send'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('danger', $exception->getMessage());
        }
    }

    public function payInDai(PayInDaiRequest $request)
    {
        if($request->get('amount') <= 0){
            return redirect()->back()->with('danger', trans('site.cabinet.form-un-correct-amount'));
        }

        try {
            $this->service->createPayInDai($request, auth()->user());
            return redirect()->back()->with('success', trans('site.cabinet.investor-deposit-pay-in-send'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('danger', $exception->getMessage());
        }
    }

    public function requestPhoneDeposit(Request $request)
    {
        try {
            $this->service->requestPhoneDeposit(auth()->user(), Carbon::now());
            return response()->json([]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()]);
        }
    }

    public function requestPhonePayoutDai(Request $request)
    {
        try {
            $this->service->requestPhonePayoutDai(auth()->user(), Carbon::now());
            return response()->json([]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()]);
        }
    }
}
