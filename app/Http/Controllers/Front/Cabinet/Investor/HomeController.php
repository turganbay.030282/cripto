<?php

namespace App\Http\Controllers\Front\Cabinet\Investor;

use App\Entity\Handbook\Asset;
use App\Entity\User;
use App\Entity\UserVerification;
use App\Http\Controllers\Controller;
use App\Services\Contract\ContractService;
use App\Services\Contract\Requests\ContractDepositCreateRequest;
use App\Services\Tpl\PdfService;
use App\Services\Tpl\TplService;
use App\Services\User\Requests\Verify\VerifyDepositLineRequest;
use App\Services\User\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    private $service;
    private $contractService;
    private $tplService;
    private $pdfService;

    public function __construct(UserService $service, ContractService $contractService, TplService $tplService, PdfService $pdfService)
    {
        $this->service = $service;
        $this->contractService = $contractService;
        $this->tplService = $tplService;
        $this->pdfService = $pdfService;
    }

    public function show()
    {
        /** @var User $user */
        $user = auth()->user();
        $userVerification = $user->userVerification;
        $investorRankVerification = $user->investorRankVerification;
        $lisTypeDocuments = UserVerification::lisTypeDocuments();
        $hasFirstPayIn = $user->hasFirstPayIn();
        $actives = Asset::forInvestor()->with('coinValue')->get();
        $activeFirst = $actives->first();
        $activeFirsts = Asset::forInvestor()->typeFirst()->with('coinValue')->get();
        $activeSeconds = Asset::forInvestor()->typeSecond()->with('coinValue')->get();

        return view('front.cabinet.investor.home', compact('user',
            'userVerification', 'investorRankVerification', 'lisTypeDocuments',
            'hasFirstPayIn', 'actives', 'activeFirst', 'activeFirsts', 'activeSeconds'
        ));
    }

    public function depositLine(VerifyDepositLineRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $this->service->depositLineVerifySend($request, $user);
        return redirect()->back()->with('success', trans('site.cabinet.form-requested-send'));
    }

    public function contract(ContractDepositCreateRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        try {
            $this->contractService->addContractDeposit($request, $user);
        } catch (\DomainException $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', trans('site.cabinet.form-error'));
        }
        return redirect()->back()->with('success', 'Договор создан');
    }

    public function contractDai(ContractDepositCreateRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        try {
            $this->contractService->addContractDepositDai($request, $user);
        } catch (\DomainException $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', trans('site.cabinet.form-error'));
        }
        return redirect()->back()->with('success', 'Договор создан');
    }

    public function investorDocument(Request $request)
    {
        return response()->json(['text' => $this->tplDocument($request)]);
    }

    public function investorDocumentDai(Request $request)
    {
        return response()->json(['text' => $this->tplDocumentDai($request)]);
    }

    public function investorDocumentPdf(Request $request)
    {
        $filename = 'deposit-' . auth()->id() . '.pdf';
        if (is_file(storage_path('app/public/temp/' . $filename))) {
            unlink(storage_path('app/public/temp/' . $filename));
        }
        $this->pdfService->generateFromHtml($request->get('text'), $filename, storage_path('app/public/temp/'));
        return url('storage/temp/' . $filename);
    }

    private function tplDocument(Request $request)
    {
        $period = $request->get('period');
        $amount = $request->get('amount');
        $rate = $request->get('rate');
        $assetId = $request->get('asset');

        return $this->tplService->docForInvestor(
            auth()->user(),
            $assetId,
            $period,
            $amount,
            $rate
        );
    }

    private function tplDocumentDai(Request $request)
    {
        $period = $request->get('period');
        $amount = $request->get('amount');
        $rate = $request->get('rate');
        $assetId = $request->get('asset');

        return $this->tplService->docForInvestorDai(
            auth()->user(),
            $assetId,
            $period,
            $amount,
            $rate
        );
    }

    public function requestPhoneVerification(Request $request)
    {
        try {
            $this->contractService->requestPhoneVerificationInvestor(auth()->user(), Carbon::now());
            return response()->json([]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()]);
        }
    }

    public function getMonthPercent(Request $request)
    {
        try {
            return response()->json(['percent' => Asset::getMonthPercent(
                $request->get('asset'),
                $request->get('amount'),
                $request->get('period')
            )]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()]);
        }
    }

    public function graphInvestor(Request $request)
    {
        $activeFirsts = Asset::find($request->get('asset'));

        return response()->json([
            'id' => uniqid(),
            'color' => $request->get('color'),
            'symbol' => $activeFirsts->coinValue->symbol,
            'ohlcvY' => json_decode($activeFirsts->coinValue->ohlcv_y, true),
            'ohlcvX' => json_decode($activeFirsts->coinValue->ohlcv_x, true),
        ]);
    }
}
