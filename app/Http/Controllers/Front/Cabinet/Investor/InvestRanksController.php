<?php

namespace App\Http\Controllers\Front\Cabinet\Investor;

use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Services\User\InvestorRankService;
use App\Services\User\Requests\Verify\InvestorRank\InvestRankFirstRequest;
use App\Services\User\Requests\Verify\InvestorRank\InvestRankSecondRequest;
use App\Services\User\Requests\Verify\InvestorRank\InvestRankThirdRequest;
use App\Services\User\Requests\Verify\InvestorRank\InvestRankUrFirstRequest;
use App\Services\User\Requests\Verify\InvestorRank\InvestRankUrFourRequest;
use App\Services\User\Requests\Verify\InvestorRank\InvestRankUrSecondRequest;
use App\Services\User\Requests\Verify\InvestorRank\InvestRankUrThirdRequest;

class InvestRanksController extends Controller
{
    private $service;

    public function __construct(InvestorRankService $service)
    {
        $this->service = $service;
    }

    public function investorRankFirst(InvestRankFirstRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        session()->put('investorRank', $request->all());
        return redirect()->route('front.cabinet.investor.home', ['q2' => 1]);
    }

    public function investorRankSecond(InvestRankSecondRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $level1 = session()->has('investorRank') ? session()->get('investorRank') : [];
        session()->put('investorRank', array_merge($level1, $request->all()));
        return redirect()->route('front.cabinet.investor.home', ['q3' => 1])->withInput($request->all());
    }

    public function investorRankFinish(InvestRankThirdRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        try {
            $this->service->investorRankFinish($request, $user);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('front.cabinet.investor.home', ['q3' => 1])->withInput()->with('danger', trans('site.cabinet.form-error'));
        }
        return redirect()->route('front.cabinet.investor.home', ['q4' => 1]);
    }


    public function investorRankFirstUr(InvestRankUrFirstRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        session()->put('investorRankUr', $request->all());
        return redirect()->route('front.cabinet.investor.home', ['q2' => 1]);
    }

    public function investorRankSecondUr(InvestRankUrSecondRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $level1 = session()->has('investorRankUr') ? session()->get('investorRankUr') : [];
        session()->put('investorRankUr', array_merge($level1, $request->all()));
        return redirect()->route('front.cabinet.investor.home', ['q3' => 1])->withInput($request->all());
    }

    public function investorRankThirdUr(InvestRankUrThirdRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $level2 = session()->has('investorRankUr') ? session()->get('investorRankUr') : [];
        session()->put('investorRankUr', array_merge($level2, $request->all()));
        return redirect()->route('front.cabinet.investor.home', ['q4' => 1])->withInput($request->all());
    }

    public function investorRankFinishUr(InvestRankUrFourRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        try {
            $this->service->investorRankFinishUr($request, $user);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('front.cabinet.investor.home', ['q4' => 1])->withInput()->with('danger', trans('site.cabinet.form-error'));
        }
        return redirect()->route('front.cabinet.investor.home', ['q5' => 1]);
    }
}
