<?php

namespace App\Http\Controllers\Front\Cabinet;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JournalController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();
        $userNotifications = $user->notifications()->orderBy('created_at', 'desc')->paginate(10);
        foreach ($userNotifications as $userNotification){
            $userNotification->read();
        }

        return view('front.cabinet.journal.index', compact('user', 'userNotifications'));
    }

    public function read(Request $request)
    {
        auth()->user()->notifications()->update(['is_read' => true]);
        if ($request->ajax()){
            return response()->json([]);
        }
        return redirect()->back();
    }
}
