<?php

namespace App\Http\Controllers\Front\Cabinet;

use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Services\Form\FormService;
use App\Services\Form\Requests\CreateRequest;
use App\Services\User\G2faService;
use App\Services\User\PhoneService;
use App\Services\User\Requests\ChangeNotifyRequest;
use App\Services\User\Requests\G2faDisabledRequest;
use App\Services\User\Requests\G2faEnableRequest;
use App\Services\User\Requests\PromoRequest;
use App\Services\User\Requests\Verify\ChangeRequest;
use App\Services\User\Requests\ChangeTypeRequest;
use App\Services\User\Requests\PasswordRequest;
use App\Services\User\Requests\Verify\VerifyRequest;
use App\Services\User\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var FormService
     */
    private $formService;
    /**
     * @var PhoneService
     */
    private $phoneService;
    /**
     * @var G2faService
     */
    private $g2faService;

    public function __construct(
        UserService $userService,
        PhoneService $phoneService,
        FormService $formService,
        G2faService $g2faService
    )
    {
        $this->userService = $userService;
        $this->formService = $formService;
        $this->phoneService = $phoneService;
        $this->g2faService = $g2faService;
    }

    public function show(Request $request)
    {
        $user = $request->user();
        $userVerification = $user->userVerification;
        $codeVerified = null;
        if (!$user->phone_verified) {
            $codeVerified = $user->codeVerifies()->verifyPhone()->orderBy('id', 'desc')->first();
        }

        return view('front.cabinet.profile.show', compact('user', 'userVerification', 'codeVerified'));
    }

    public function g2fa(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $authenticationKey = $this->g2faService->generateAuthenticationKey();
        $qRImage = $this->g2faService->generateQRCodeInline($user, $authenticationKey);

        return view('front.cabinet.profile.g2fa', compact('user', 'qRImage', 'authenticationKey'));
    }

    public function g2faEnable(G2faEnableRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        try {
            $this->g2faService->g2faEnable($request, $user);
        } catch (\Exception $exception) {
            return redirect()->back()->with('danger', $exception->getMessage());
        }
        return redirect()->route('front.cabinet.profile.show')->with('success', trans('site.cabinet.2gaf-message-enable'));
    }

    public function g2faDisable(G2faDisabledRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        try {
            $this->g2faService->g2faDisable($request, $user);
        } catch (\Exception $exception) {
            return redirect()->back()->with('danger', $exception->getMessage());
        }
        return redirect()->route('front.cabinet.profile.show')->with('success', trans('site.cabinet.2gaf-message-disabled'));
    }

    public function contacts(Request $request)
    {
        $user = $request->user();
        return view('front.cabinet.profile.contacts', compact('user'));
    }

    public function submitContact(CreateRequest $request)
    {
        $this->formService->sendMessage($request);
        return redirect()->back()->with('success', trans('site.form.message-send'));
    }

    public function changePassword(PasswordRequest $request)
    {
        $this->userService->changePassword($request->get('password'), $request->user());
        return redirect()->back()->with('success', trans('site.page.cabinet.change-password-message'));
    }

    public function verifySend(VerifyRequest $request)
    {
        $this->userService->verifySend($request, $request->user());
        return redirect()->back()->with('success', trans('site.page.cabinet.verify-send-message'));
    }

    public function changeType(ChangeTypeRequest $request)
    {
        $this->userService->changeType($request, $request->user());
        if ($request->user()->is_investor) {
            return redirect()->route('front.cabinet.investor.home');
        }
        return redirect()->route('front.cabinet.trader.home');
    }

    public function changeSend(ChangeRequest $request)
    {
        $this->userService->changeSend($request, $request->user());
        return redirect()->back()->with('success', trans('site.page.cabinet.request-change-profile-message'));
    }

    public function changeNotify(ChangeNotifyRequest $request)
    {
        $this->userService->changeNotify($request, $request->user());
        return redirect()->back()->with('success', trans('site.page.cabinet.change-profile-save-message'));
    }

    public function sendPromo(PromoRequest $request)
    {
        $this->userService->sendPromo($request, $request->user());
        return redirect()->back()->with('success', trans('site.page.cabinet.invitation-sent-message'));
    }

    public function sendPhoneVerification(Request $request)
    {
        try {
            $this->phoneService->requestPhoneVerification(auth()->user(), Carbon::now());
        } catch (\Exception $exception) {
            return redirect()->back()->with('danger', $exception->getMessage());
        }
        return redirect()->back()->with('success', trans('site.page.cabinet.confirmation-code-sent-message'));
    }

    public function verifyPhone(Request $request)
    {
        try {
            $this->phoneService->verifyPhone(auth()->user(), $request->get('token'), Carbon::now());
        } catch (\Exception $exception) {
            return redirect()->back()->with('danger', $exception->getMessage());
        }
        return redirect()->back()->with('success', trans('site.page.cabinet.number-confirmed-message'));
    }


    public function getSmsCode(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();
        try {
            $this->userService->getSmsCode($user, $request->get('type'));
            return response()->json([]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()]);
        }
    }
}
