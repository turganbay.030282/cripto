<?php

namespace App\Http\Controllers\Front\Cabinet\Trader;

use App\Entity\Contract\Contract;
use App\Entity\Handbook\Asset;
use App\Http\Controllers\Controller;
use App\Services\Contract\ContractService;
use App\Services\Contract\Repositories\EloquentContractRepository;
use App\Services\Contract\Requests\ContractPaymentRequest;
use App\Services\Tpl\PdfService;

class ContractsController extends Controller
{
    private $repository;
    private $pdfService;
    private $service;

    public function __construct(ContractService $service, EloquentContractRepository $repository, PdfService $pdfService)
    {
        $this->repository = $repository;
        $this->pdfService = $pdfService;
        $this->service = $service;
    }

    public function index()
    {
        $user = auth()->user();
        $assetContracts = Asset::with(['contracts' => function ($query) use ($user) {
            $query->approve();
            $query->where('user_id', $user->id);
            $query->orderBy('id', 'desc');
        }, 'contracts.typeAsset', 'coinValue'])
            ->whereHas('contracts', function ($query) use ($user) {
                $query->approve();
                $query->where('user_id', $user->id);
                $query->orderBy('id', 'desc');
            })
            ->orderBy('id', 'desc')
            ->get();

        $creditContractValue = $this->repository->creditContractValue($user);
        $creditContractNext = $this->repository->creditContractNext($user);

        return view('front.cabinet.trader.contracts.index', compact('user', 'assetContracts', 'creditContractValue', 'creditContractNext'));
    }

    public function archive()
    {
        $user = auth()->user();
        $assetContracts = Asset::with(['contracts' => function ($query) use ($user) {
            $query->close();
            $query->where('user_id', $user->id);
            $query->orderBy('id', 'desc');
        }, 'contracts.typeAsset', 'coinValue'])
            ->whereHas('contracts', function ($query) use ($user) {
                $query->close();
                $query->where('user_id', $user->id);
                $query->orderBy('id', 'desc');
            })
            ->orderBy('id', 'desc')
            ->get();

        $creditContractValue = $this->repository->creditContractValue($user);
        $creditContractNext = $this->repository->creditContractNext($user);

        return view('front.cabinet.trader.contracts.archive', compact('user', 'assetContracts', 'creditContractValue', 'creditContractNext'));
    }

    public function payContract(ContractPaymentRequest $request, Contract $contract)
    {
        try {
            $this->service->paymentTraderContract($request, $contract);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', trans('site.cabinet.form-error'));
        }
        return redirect()->back()->with('success', trans('site.cabinet.form-payContract-send'));
    }

    public function payAllContract(ContractPaymentRequest $request)
    {
        try {
            $this->service->paymentAllTraderContract($request);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', trans('site.cabinet.form-error'). ' ('.$e->getMessage() .')');
        }
        return redirect()->back()->with('success', trans('site.cabinet.form-payContract-send'));
    }

    public function download(Contract $contract)
    {
        return $this->pdfService->downloadFromHtml($contract->document, 'contract-' . $contract->number . '.pdf');
    }

    public function print(Contract $contract)
    {
        $filename = 'ccontract-' . $contract->number . '.pdf';
        $this->pdfService->generateFromHtml($contract->document, $filename, storage_path('app/public/temp/'));
        return url('storage/temp/' . $filename);
    }

    public function redeem(ContractPaymentRequest $request, Contract $contract)
    {
        try {
            $this->service->redeem($request, $contract);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', trans('site.cabinet.form-error'));
        }
        return redirect()->back()->with('success', trans('site.cabinet.form-payContract-send'));
    }

    public function sell(Contract $contract)
    {
        try {
            $this->service->sell($contract);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', trans('site.cabinet.form-error'));
        }
        return redirect()->back()->with('success', trans('site.cabinet.form-sellContract-send'));
    }
}
