<?php

namespace App\Http\Controllers\Front\Cabinet\Trader;

use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Services\User\CreditLevelVerifyService;
use App\Services\User\Requests\Verify\CreditLevel\VerifyCreditLevelFinishRequest;
use App\Services\User\Requests\Verify\CreditLevel\VerifyCreditLevelFirstRequest;
use App\Services\User\Requests\Verify\CreditLevel\VerifyCreditLevelSecondRequest;
use App\Services\User\Requests\Verify\CreditLevel\VerifyCreditLevelUrFinishRequest;
use App\Services\User\Requests\Verify\CreditLevel\VerifyCreditLevelUrFirstRequest;
use App\Services\User\Requests\Verify\CreditLevel\VerifyCreditLevelUrSecondRequest;
use App\Services\User\Requests\Verify\CreditLevel\VerifyCreditLevelUrThirdRequest;
use Illuminate\Http\Request;
use Validator;

class CreditLevelController extends Controller
{
    private $creditLevelVerifyService;

    public function __construct(
        CreditLevelVerifyService $creditLevelVerifyService
    )
    {
        $this->creditLevelVerifyService = $creditLevelVerifyService;
    }

    public function creditLevelFirst(VerifyCreditLevelFirstRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        session()->put('creditLevel', $request->all());
        return redirect()->route('front.cabinet.trader.home', ['q2' => 1]);
    }

    public function creditLevelSecond(VerifyCreditLevelSecondRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $level1 = session()->has('creditLevel') ? session()->get('creditLevel') : [];
        session()->put('creditLevel', array_merge($level1, $request->all()));
        return redirect()->route('front.cabinet.trader.home', ['q3' => 1])->withInput($request->all());
    }

    public function creditLevelFinish(VerifyCreditLevelFinishRequest $request)
    {
        $validator = Validator::make($request->all(), []);
        $countFiles = 0;
        if ($request->has('file_statement')) $countFiles += 1;
        if ($request->has('file_income_work')) $countFiles += 1;
        if ($request->has('file_utility_bill')) $countFiles += 1;
        if ($countFiles < 2) {
            $validator->errors()->add(
                'files_selected', trans('site.cabinet.credit-level-Bill-utilities-files')
            );
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        /** @var User $user */
        $user = auth()->user();
        try {
            $this->creditLevelVerifyService->creditLevelVerifySend($request, $user);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('front.cabinet.trader.home', ['q3' => 1])->withInput()->with('danger', trans('site.cabinet.form-error'));
        }
        return redirect()->route('front.cabinet.trader.home', ['q4' => 1]);
    }

    public function creditLevelFirstUr(VerifyCreditLevelUrFirstRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        session()->put('creditLevelUr', $request->all());
        return redirect()->route('front.cabinet.trader.home', ['q2' => 1]);
    }

    public function creditLevelSecondUr(VerifyCreditLevelUrSecondRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $level1 = session()->has('creditLevelUr') ? session()->get('creditLevelUr') : [];
        session()->put('creditLevelUr', array_merge($level1, $request->all()));
        return redirect()->route('front.cabinet.trader.home', ['q3' => 1])->withInput($request->all());
    }

    public function creditLevelThirdUr(VerifyCreditLevelUrThirdRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $level2 = session()->has('creditLevelUr') ? session()->get('creditLevelUr') : [];
        session()->put('creditLevelUr', array_merge($level2, $request->all()));
        return redirect()->route('front.cabinet.trader.home', ['q4' => 1])->withInput($request->all());
    }

    public function creditLevelFinishUr(VerifyCreditLevelUrFinishRequest $request)
    {
        $validator = Validator::make($request->all(), []);
        $countFiles = 0;
        if ($request->has('file_statement')) $countFiles += 1;
        if ($request->has('file_income_work')) $countFiles += 1;
        if ($request->has('file_utility_bill')) $countFiles += 1;
        if ($countFiles < 2) {
            $validator->errors()->add(
                'files_selected', trans('site.cabinet.credit-level-Bill-utilities-files')
            );
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        /** @var User $user */
        $user = auth()->user();
        try {
            $this->creditLevelVerifyService->creditLevelUrVerifySend($request, $user);
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->route('front.cabinet.trader.home', ['q4' => 1])->withInput()->with('danger', trans('site.cabinet.form-error'));
        }
        return redirect()->route('front.cabinet.trader.home', ['q5' => 1]);
    }

    public function creditLevelFinishUrRow(Request $request)
    {
        return view('front.cabinet.trader.parts._modals_credit_level_ur_person', ['row' => $request->get('row') + 1])->render();
    }
}
