<?php

namespace App\Http\Controllers\Front\Cabinet\Trader;

use App\Entity\Handbook\Asset;
use App\Entity\User;
use App\Entity\UserVerification;
use App\Helpers\NumberHelper;
use App\Http\Controllers\Controller;
use App\Services\Contract\ContractService;
use App\Services\Contract\Requests\ContractCreateRequest;
use App\Services\Tpl\PdfService;
use App\Services\Tpl\TplService;
use App\Services\User\Requests\Verify\VerifyCreditLineRequest;
use App\Services\User\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $service;
    private $contractService;
    private $tplService;
    private $pdfService;

    public function __construct(
        UserService $service,
        ContractService $contractService,
        TplService $tplService,
        PdfService $pdfService
    )
    {
        $this->service = $service;
        $this->contractService = $contractService;
        $this->tplService = $tplService;
        $this->pdfService = $pdfService;
    }

    public function show()
    {
        /** @var User $user */
        $user = auth()->user();
        $activeFirsts = Asset::forTrader()->typeFirst()->with('coinValue')->get();
        $activeSeconds = Asset::forTrader()->typeSecond()->with('coinValue')->get();
        $lisTypeDocuments = UserVerification::lisTypeDocuments();
        $userVerification = $user->userVerification;
        if ($user->is_ur) {
            $creditLevelVerification = $user->creditLevelUrVerification;
        } else {
            $creditLevelVerification = $user->creditLevelVerification;
        }

        $tplDocument = $this->tplService->docForTrader($user);
        $hasContract = $user->hasContract();

        return view('front.cabinet.trader.home', compact('user', 'activeFirsts',
            'activeSeconds', 'userVerification', 'creditLevelVerification', 'lisTypeDocuments',
            'tplDocument', 'hasContract'));
    }

    public function creditLine(VerifyCreditLineRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $this->service->creditLineVerifySend($request, $user);
        return redirect()->back()->with('success', trans('site.cabinet.form-requested-send'));
    }

    public function contract(ContractCreateRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        try {
            $this->contractService->addContract($request, $user);
        } catch (\DomainException $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', trans('site.cabinet.form-error'));
        }
        return redirect()->back()->with('success', trans('site.cabinet.form-contract-create'));
    }

    public function graphTrader(Request $request)
    {
        $activeFirsts = Asset::find($request->get('asset'));

        return response()->json([
            'id' => uniqid(),
            'color' => $request->get('color'),
            'symbol' => $activeFirsts->coinValue->symbol,
            'ohlcvY' => json_decode($activeFirsts->coinValue->ohlcv_y, true),
            'ohlcvX' => json_decode($activeFirsts->coinValue->ohlcv_x, true),
        ]);
    }

    public function traderDocument(Request $request)
    {
        return response()->json(['text' => $this->tplDocument($request)]);
    }

    public function traderDocumentPdf(Request $request)
    {
        $filename = 'c-' . uniqid(auth()->id()) . '.pdf';
        if (is_file(storage_path('app/public/temp/' . $filename))) {
            unlink(storage_path('app/public/temp/' . $filename));
        }
        $this->pdfService->generateFromHtml($request->get('text'), $filename, storage_path('app/public/temp/'));
        return url('storage/temp/' . $filename);
    }

    private function tplDocument(Request $request)
    {
        $text = '';
        if ($request->get('asset')) {
            $asset = Asset::find($request->get('asset'));
            if ($asset) {
                $coinValue = $asset->coinValue;
                if (!$coinValue) {
                    return $text;
                }
                $period = $request->get('period');
                $amount = $request->get('amount');
                $rate = $request->get('rate');
                $text = $this->tplService->docForTrader(
                    auth()->user(),
                    $period,
                    $asset->coinValue->symbol,
                    $amount,
                    NumberHelper::format_number_cripto($amount / $asset->coinValue->price),
                    $rate,
                    Carbon::now()->format('d.m.Y')
                );
            }
        }

        return $text;
    }

    public function requestPhoneVerification(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();
        try {
            $this->contractService->requestPhoneVerification($user, Carbon::now());
            return response()->json([]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()]);
        }
    }

    public function calcFirstPayment(Request $request)
    {
        return NumberHelper::calcTraderAnnuityFirstPayment($request->get('amount'), $request->get('period'), $request->get('rate'));
    }
}
