<?php

namespace App\Http\Controllers\Front\Cabinet\Trader;

use App\Http\Controllers\Controller;
use App\Services\Transaction\PayoutService;
use App\Services\Transaction\Requests\Payout\BuyAssetRequest;
use App\Services\Transaction\Requests\Payout\PayoutAssetRequest;
use App\Services\Transaction\Requests\Payout\TransferAssetRequest;
use App\Services\Transaction\Requests\PayoutRequest;
use App\Entity\User;
use App\Services\Transaction\Requests\TransferToInvestorRequest;
use Illuminate\Http\Request;

class PayoutsController extends Controller
{
    private $service;

    public function __construct(PayoutService $service)
    {
        $this->service = $service;
    }

    public function index(PayoutRequest $request)
    {
        if($request->get('amount') <= 0){
            return redirect()->back()->with('danger', trans('site.cabinet.form-un-correct-amount'));
        }

        /** @var User $user */
        $user = auth()->user();
        try {
            $this->service->payout($request, $user);
        } catch (\DomainException $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', trans('site.cabinet.form-error'));
        }

        return redirect()->back()->with('success', trans('site.cabinet.form-payContract-send'));
    }

    public function transferToInvestor(TransferToInvestorRequest $request)
    {
        if($request->get('amount') <= 0){
            return redirect()->back()->with('danger', trans('site.cabinet.form-un-correct-amount'));
        }

        /** @var User $user */
        $user = auth()->user();
        try {
            $this->service->transferToInvestor($request, $user);
        } catch (\DomainException $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', trans('site.cabinet.form-error'));
        }

        return redirect()->back()->with('success', trans('site.cabinet.form-transferToInvestor-send'));
    }

    public function payoutAsset(PayoutAssetRequest $request)
    {
        if($request->get('coin_value') <= 0){
            return redirect()->back()->with('danger', trans('site.cabinet.form-un-correct-amount'));
        }

        /** @var User $user */
        $user = auth()->user();
        try {
            $this->service->payoutAsset($request, $user);
        } catch (\DomainException $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', trans('site.cabinet.form-error'));
        }

        return redirect()->back()->with('success', trans('site.cabinet.form-transferToInvestor-send'));
    }

    public function transferAsset(TransferAssetRequest $request)
    {
        if($request->get('coin_value') <= 0){
            return redirect()->back()->with('danger', trans('site.cabinet.form-un-correct-amount'));
        }

        /** @var User $user */
        $user = auth()->user();
        try {
            $this->service->transferAsset($request, $user);
        } catch (\DomainException $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', trans('site.cabinet.form-error'));
        }

        return redirect()->back()->with('success', trans('site.cabinet.form-transferAsset-send'));
    }

    public function buyAsset(BuyAssetRequest $request)
    {
        if($request->get('amount') <= 0){
            return redirect()->back()->with('danger', trans('site.cabinet.form-un-correct-amount'));
        }

        /** @var User $user */
        $user = auth()->user();
        try {
            $this->service->buyAsset($request, $user);
        } catch (\DomainException $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        } catch (\Exception $e) {
            \Log::error('Exception', [$e]);
            return redirect()->back()->with('danger', trans('site.cabinet.form-error'));
        }

        return redirect()->back()->with('success', trans('site.cabinet.form-buyAsset-send'));
    }

}
