<?php

namespace App\Http\Controllers\Front\Cabinet\Trader;

use App\Entity\Handbook\Asset;
use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Services\Contract\Repositories\EloquentContractRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PortfoliosController extends Controller
{
    private $repository;

    public function __construct(EloquentContractRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $assetContracts = Asset::with(['contracts' => function ($query) use ($user) {
            $query->approve();
            $query->where('user_id', $user->id);
            $query->orderBy('id', 'desc');
        }, 'coinValue'])
            ->whereHas('contracts', function ($query) use ($user) {
                $query->approve();
                $query->where('user_id', $user->id);
                $query->orderBy('id', 'desc');
            })
            ->orderBy('id', 'desc')
            ->get();

        $portfolioQuery = $user->portfolioGraphs()
            ->orderBy('tm', 'asc');

        $period = $request->get('period', '1m');
        switch ($period) {
            case "1d":
                $portfolio = $portfolioQuery->where('created_at', '>=', Carbon::now()->subDays(1)->startOfDay());
            case "7d":
                $portfolio = $portfolioQuery->where('created_at', '>=', Carbon::now()->subDays(7));
                break;
            case "1m":
                $portfolio = $portfolioQuery->where('created_at', '>=', Carbon::now()->subMonth());
                break;
            case "3m":
                $portfolio = $portfolioQuery->where('created_at', '>=', Carbon::now()->subMonths(3));
                break;
            case "1y":
                $portfolio = $portfolioQuery->where('created_at', '>=', Carbon::now()->subMonths(12));
                break;
            case "all":
                $portfolio = $portfolioQuery;
                break;
        }

        $portfolio = $portfolio->get();
        $graphs = [];
        if ($portfolio) {
            $x = [];
            $y = [];
            foreach ($portfolio as $item) {
                $x[] = date('d.m', $item->tm);
                $y[] = $item->amount;
            }

            $graphs['x'] = $x;
            $graphs['y'] = $y;
        }

        $portfolioValue = $this->repository->portfolioValue($user);

        return view('front.cabinet.trader.portfolios.index', compact('user', 'assetContracts', 'portfolioValue', 'graphs'));
    }

}
