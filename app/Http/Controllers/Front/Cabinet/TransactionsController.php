<?php

namespace App\Http\Controllers\Front\Cabinet;

use App\Entity\Transfer\Transaction;
use App\Helpers\StatusHelper;
use App\Http\Controllers\Controller;
use App\Traits\Status;

class TransactionsController extends Controller
{
    public function index()
    {
        $userTransactions = Transaction::query()
            ->with('coinmarketcap')
            ->where('user_id', auth()->id())
            ->orderBy('created_at', 'desc')
            ->paginate(30);
        return view('front.cabinet.transactions.index', compact('userTransactions'));
    }


    public function reject(Transaction $transaction): \Illuminate\Http\RedirectResponse
    {
        if ($transaction->status !== StatusHelper::STATUS_NEW){
            return redirect()->back()->with('danger', __('site.form.message-error'));
        }

        if ($transaction->type_transaction === Transaction::TYPE_TRADER_CONTRACT_DELAYS_SELL){
            return redirect()->back()->with('danger', __('site.form.message-error'));
        }

        try {
            $transaction->update(['status' => StatusHelper::STATUS_REJECT]);
        } catch (\Exception $e) {
            info('Exception', [$e]);
            return redirect()->back()->with('danger', $e->getMessage());
        }
        return redirect()->back()->with('success', 'Платеж отклонен');
    }
}
