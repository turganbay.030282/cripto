<?php

namespace App\Http\Controllers\Front;

use App\Entity\Content\Faq\FaqCategory;
use App\Http\Controllers\Controller;
use App\Services\Handbook\Repositories\EloquentHandbookRepository;

class ConvertController extends Controller
{
    protected $handbookRepository;

    public function __construct(
        EloquentHandbookRepository $handbookRepository
    )
    {
        $this->handbookRepository = $handbookRepository;
    }

    public function index()
    {
        $user = auth()->user();
        $categories = FaqCategory::with('items')->where('faq_categories.is_convert', 1)->orderByTranslation('title', 'asc')->get();
        $assets = $this->handbookRepository->listAsset2();

        return view('front.cabinet.convert.index', compact('user', 'categories', 'assets'));
    }
}
