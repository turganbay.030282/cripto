<?php

namespace App\Http\Controllers\Front;

use App\Entity\Coinmarketcap;
use App\Entity\Content\BlockPackage;
use App\Entity\Content\Faq\FaqItem;
use App\Entity\Content\PageMain;
use App\Entity\Handbook\Asset;
use App\Entity\Handbook\TypeTransaction;
use App\Entity\Setting;
use App\Helpers\NumberHelper;
use App\Http\Controllers\Controller;
use App\Services\Form\Requests\SubscribeCreateRequest;
use App\Services\Form\SubscribeService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $service;

    public function __construct(SubscribeService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $pageHome = PageMain::firstOrFail();
        $faqItems = FaqItem::orderBy('id', 'desc')->take(6)->get();
        $blockPackages = BlockPackage::where('type', 1)->orderBy('id', 'asc')->get();
        $typeTransactions = TypeTransaction::trader()->orderBy('value', 'desc')->get();

        $symbols = Asset::forGraph()->with('coinValue')->orderBy('id', 'asc')->get();
        $symbolCalc = Coinmarketcap::whereIn('symbol', ['BTC', 'LTC'])->orderBy('symbol', 'asc')->get();
        return view('front.home', compact('symbols', 'pageHome', 'faqItems', 'blockPackages', 'symbolCalc', 'typeTransactions'));
    }

    public function calcInvest(Request $request)
    {
        $amount = $request->get('amount');
        $dai = $request->get('dai', 0);
        if ($dai) {
            $amount = Setting::first()->course_investor * $amount;
        }

        $typeTransaction = TypeTransaction::investor()
            ->where('min', '<=', $amount)
            ->where('max', '>=', $amount)
            ->first();
        if (!$typeTransaction) {
            $typeTransaction = TypeTransaction::investor()
                ->where('min', '>', $amount)
                ->first();
        }

        if (!$typeTransaction) {
            return response()->json([]);
        }
        return response()->json([
            'name' => $typeTransaction->name,
            'value' => $typeTransaction->getValueBonus(auth()->user(), false)
        ]);
    }

    public function calcTrader(Request $request)
    {
        $amount = $request->get('amount');
        $onload = $request->get('onload');
        $assetId = $request->get('asset');
        $typeTransaction = TypeTransaction::trader()
            ->where('min', '<=', $amount)
            ->where('max', '>=', $amount)
            ->first();

        if (!$typeTransaction) {
            return response()->json([]);
        }

        $graph = '';
        if ($onload && $assetId) {
            $asset = Asset::find($assetId);
            if ($asset) {
                $coinValue = $asset->coinValue;
                if ($coinValue) {
                    $graph = $coinValue->ohlcv_y;
                }
            }
        }

        return response()->json([
            'name' => $typeTransaction->name,
            'value' => $typeTransaction->getValueBonus(auth()->user()),
            'graph' => $graph
        ]);
    }

    public function calcTraderAnnuity(Request $request)
    {
        $amount = $request->get('amount');
        $period = $request->get('period');
        $type = $request->get('type');
        $onload = $request->get('onload');
        $assetId = $request->get('asset');

        if ($type) {
            $typeTransaction = TypeTransaction::find($type);
        } else {
            $typeTransaction = TypeTransaction::trader()
                ->where('min', '<=', $amount)
                ->where('max', '>=', $amount)
                ->first();
        }

        if (!$typeTransaction) {
            return response()->json([]);
        }

        $graph = '';
        if ($onload && $assetId) {
            $asset = Asset::find($assetId);
            if ($asset) {
                $coinValue = $asset->coinValue;
                if ($coinValue) {
                    $graph = $coinValue->ohlcv_y;
                }
            }
        }

        return response()->json([
            'name' => $typeTransaction->name,
            'value' => $typeTransaction->value,
            'amount' => NumberHelper::calcTraderAnnuityFirstPaymentRevert($amount, $period, $typeTransaction->value),
            'graph' => $graph
        ]);
    }

    public function subscribers(SubscribeCreateRequest $request)
    {
        try {
            $this->service->sendMessage($request);
        } catch (\Exception $e) {
            return redirect()->back()->with('danger', trans('site.form.message-error'));
        }
        return redirect()->back()->with('success', trans('site.form.message-send'));
    }
}
