<?php

namespace App\Http\Controllers\Front;

use App\Entity\Content\Blog\BlogCategory;
use App\Entity\Content\Blog\BlogItem;
use App\Http\Controllers\Controller;
use App\Services\Blog\BlogService;

class NewsController extends Controller
{
    private $service;

    public function __construct(BlogService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $news = BlogItem::with('author')->publish()->orderBy("id", "desc")->paginate(9);
        $newPopulars = BlogItem::with('author')->publish()->orderBy("count_view", "desc")->take(3)->get();
        $categories = BlogCategory::orderByTranslation("title", "asc")->get();
        return view('front.news.index', compact('news', 'categories', 'newPopulars'));
    }

    public function category($slug)
    {
        $category = BlogCategory::where("slug", $slug)->firstOrFail();
        $news = BlogItem::whereHas('categories', function ($query) use ($category) {
            $query->where('id', $category->id);
        })->with('author')->publish()->orderBy("id", "desc")->paginate(9);
        $newPopulars = BlogItem::with('author')->publish()->orderBy("count_view", "desc")->take(3)->get();
        $categories = BlogCategory::orderByTranslation("title", "asc")->get();
        return view('front.news.category', compact('category', 'news', 'categories', 'newPopulars'));
    }

    public function show($slug)
    {
        $new = BlogItem::publish()->where("slug", $slug)->firstOrFail();
        $this->service->viewNew($new);
        $author = $new->author;
        $lastNews = BlogItem::publish()->where('id', '<>', $new->id)->orderBy("id", "desc")->paginate(4);
        return view('front.news.show', compact('new', 'author', 'lastNews'));
    }

}
