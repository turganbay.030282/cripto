<?php

namespace App\Http\Controllers\Front;

use App\Entity\Content\Page;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{

    public function show($slug)
    {
        $page = Page::publish()->where("slug", $slug)->firstOrFail();
        return view('front.pages.show', compact('page'));
    }

}
