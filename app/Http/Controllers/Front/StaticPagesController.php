<?php

namespace App\Http\Controllers\Front;

use App\Entity\Content\BlockPackage;
use App\Entity\Content\Certificate;
use App\Entity\Content\Faq\FaqCategory;
use App\Entity\Content\PageAbout;
use App\Entity\Content\PageBonus;
use App\Entity\Content\PageInvestor;
use App\Entity\Handbook\Asset;
use App\Http\Controllers\Controller;
use App\Services\Form\FormService;
use App\Services\Form\Requests\CreateGuestRequest;
use Illuminate\Http\Request;

class StaticPagesController extends Controller
{
    /**
     * @var FormService
     */
    private $formService;

    public function __construct(FormService $formService)
    {
        $this->formService = $formService;
    }

    public function about()
    {
        $pageAbout = PageAbout::firstOrFail();

        return view('front.static-pages.about', compact('pageAbout'));
    }

    public function investors()
    {
        $pageInvestor = PageInvestor::firstOrFail();
        $blockPackages = BlockPackage::where('type', 2)->orderBy('id', 'asc')->get();
        $assets = Asset::with('coinValue')->whereNotNull('calc')->get();

        return view('front.static-pages.investors', compact('blockPackages', 'pageInvestor', 'assets'));
    }

    public function contact()
    {
        return view('front.static-pages.contact');
    }

    public function exchange()
    {
        return view('front.static-pages.exchange');
    }

    public function submitContact(CreateGuestRequest $request)
    {
        $this->formService->sendGuestMessage($request);

        return redirect()->back()->with('success', trans('site.form.message-send'));
    }

    public function faq()
    {
        $cats = FaqCategory::with('items')->where('faq_categories.is_convert', 0)
            ->orderByTranslation('title', 'asc')->get();
        $category = $cats->first();
        $categories = $cats->slice(1);

        return view('front.static-pages.faq', compact('category', 'categories'));
    }

    public function cert()
    {
        $certificates = Certificate::orderBy('id', 'desc')->get();

        return view('front.static-pages.cert', compact('certificates'));
    }

    public function bonuses(Request $request)
    {
        $user = $request->user();
        $locale = app()->getLocale();
        $pageBonus = PageBonus::firstOrFail();

        return view('front.static-pages.bonuses', compact('locale', 'pageBonus', 'user'));
    }

}
