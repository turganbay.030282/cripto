<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Services\User\UserService;
use Illuminate\Http\Request;

class VerificationsController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function confirm(Request $request)
    {
        try {
            \Log::error('VerificationsController', ['request' => $request->all()]);
            $this->userService->verifyByWebHook($request);
            return response()->json([], 200);
        } catch (\Exception $e) {
            \Log::error('VerificationsController', ['Exception' => $e]);
        }
    }
}
