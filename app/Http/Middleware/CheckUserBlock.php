<?php

namespace App\Http\Middleware;

use App\Entity\User;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class CheckUserBlock extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check()) {
            /** @var User $user */
            $user = Auth::user();

            if ($user->isBlock() && !$request->is('cabinet/trader/contracts/*/payment')) {
                if ($user->is_investor) {
                    return url()->current() == route('front.cabinet.investor.home') ? $next($request) : redirect()->route('front.cabinet.investor.home');
                }

                return url()->current() == route('front.cabinet.trader.home') ? $next($request) : redirect()->route('front.cabinet.trader.home');
            }
        }

        return $next($request);
    }
}
