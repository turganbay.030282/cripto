<?php

namespace App\Http\Middleware;

use App\Entity\User;
use Carbon\Carbon;
use Closure;
use Auth;
use Cache;

class LastUserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            /** @var User $user */
            $user = Auth::user();
            $locale = \App::getLocale();
            if($user->lang != $locale){
                $user->changeLocale($locale);
            }
        }
        return $next($request);
    }
}
