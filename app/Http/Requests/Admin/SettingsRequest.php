<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $array = [
            'crop_blog_width' => 'required|integer',
            'crop_blog_height' => 'required|integer',
            'crop_ava_width' => 'required|integer',
            'crop_ava_height' => 'required|integer',
            'crop_cert_width' => 'required|integer',
            'crop_cert_height' => 'required|integer',
            'contact_phone' => 'required|string|max:255',
            'contact_email' => 'required|string|max:255',
            'soc_fb' => 'required|string|max:255',
            'soc_tw' => 'required|string|max:255',
            'soc_in' => 'required|string|max:255',
            'soc_yt' => 'nullable|string|max:255',
            'is_manual_verify' => 'required|boolean',
            'commission_trader' => 'required|numeric',
            'commission_investor' => 'required|numeric',
            'commission_sell_trader' => 'required|numeric',
            'course_investor' => 'required|numeric',
            'conversion_fee' => 'required|numeric',
            'wallet_number' => 'nullable|string|max:255',
            'wallet_img' => 'nullable|image|mimes:jpg,jpeg,png,svg',
            'bonus_ref_credit' => 'required|numeric',
            'bonus_ref_deposit' => 'required|numeric',
            'percent_ref_credit' => 'required|numeric',
            'percent_ref_deposit' => 'required|numeric',
            'penalty' => 'required|numeric',
        ];

        foreach (config('translatable.locales') as $locale) {
            $array[$locale . '.contact_address'] = 'required|string|max:255';
            $array[$locale . '.req_sia'] = 'required|string|max:255';
            $array[$locale . '.req_reg'] = 'required|string|max:255';
            $array[$locale . '.req_seb'] = 'required|string|max:255';
            $array[$locale . '.req_swift'] = 'required|string|max:255';
            $array[$locale . '.req_iban'] = 'required|string|max:255';
            $array[$locale . '.req_bank_address'] = 'required|string|max:255';
            $array[$locale . '.footer_text_1'] = 'required|string';
            $array[$locale . '.footer_text_2'] = 'nullable|string';
            $array[$locale . '.file_politic'] = 'nullable|file|mimes:pdf';
        }

        return $array;
    }

}
