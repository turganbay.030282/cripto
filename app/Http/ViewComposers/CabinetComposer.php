<?php

namespace App\Http\ViewComposers;

use App\Entity\User;
use Illuminate\View\View;

class CabinetComposer
{
    public function compose(View $view): void
    {
        /** @var User $user */
        $user = auth()->user();
        $view->with('user', $user);

        $creditLevel = $user->creditLevel;
        $investorRank = $user->investor_rank;
        $view->with('userCreditLevel', $creditLevel ? $creditLevel->name : '');
        $view->with('userInvestorRank', $investorRank);
    }
}
