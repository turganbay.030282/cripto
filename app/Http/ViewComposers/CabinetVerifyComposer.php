<?php

namespace App\Http\ViewComposers;

use App\Entity\User;
use App\Services\User\UserService;
use Illuminate\View\View;

class CabinetVerifyComposer
{
    private $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function compose(View $view): void
    {
        $user = auth()->user();
        $token = null;
        if (!$user->is_verification) {
            $customerId = auth()->id() . '-' . uniqid(auth()->id());
            $array = $this->service->setVerifyToken($customerId);
            $view->with('userGetIdCustomer', $array[0]);
            $view->with('userGetIdToken', $array[1]);
        }
    }
}
