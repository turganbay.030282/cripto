<?php

namespace App\Http\ViewComposers;

use App\Entity\Contract\Contract;
use App\Entity\Form;
use App\Entity\Handbook\Asset;
use App\Entity\Investor\DepositLineVerification;
use App\Entity\Investor\InvestorRankVerification;
use App\Entity\Trader\CreditLevelVerification;
use App\Entity\Trader\CreditLineVerification;
use App\Entity\User;
use App\Entity\UserChange;
use App\Entity\UserVerification;
use Illuminate\View\View;

class ClientHeaderComposer
{
    public function compose(View $view): void
    {
        /** @var User $user */
        $user = auth()->user();
        $view->with(
            'headerUserNotifications',
            $user ? $user->notifications()->new()->orderBy('created_at', 'desc')->take(6)->get() : collect([])
        );
    }
}
