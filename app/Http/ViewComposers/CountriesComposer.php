<?php

namespace App\Http\ViewComposers;

use App\Entity\Handbook\Country;
use App\Entity\Handbook\CountryTranslation;
use Illuminate\View\View;

class CountriesComposer
{
    public function compose(View $view): void
    {
        $view->with('countries', CountryTranslation::where('locale', app()->getLocale())->get()->pluck('title', 'title')->all());
    }
}
