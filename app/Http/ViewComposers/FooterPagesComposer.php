<?php

namespace App\Http\ViewComposers;

use App\Entity\Content\Page;
use Illuminate\View\View;

class FooterPagesComposer
{
    public function compose(View $view): void
    {
        $view->with('footerPages', Page::publish()->orderByTranslation('title', 'asc')->get());
    }
}
