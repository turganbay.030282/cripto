<?php

namespace App\Http\ViewComposers;

use App\Entity\User;
use App\Services\Contract\Repositories\EloquentContractRepository;
use Illuminate\View\View;

class InvestorSidebarComposer
{
    private $repository;

    public function __construct(EloquentContractRepository $repository)
    {
        $this->repository = $repository;
    }

    public function compose(View $view): void
    {
        /** @var User $user */
        $user = auth()->user();
        $view->with('user', $user);
        $view->with('investorContracts', $this->repository->investorContracts($user));
        $view->with('sInvestorProfit', $this->repository->investorProfit($user));
        $view->with('sInvestorAssets', $this->repository->investorAssets());
        $view->with('sBalances', $user->balances);
    }
}
