<?php

namespace App\Http\ViewComposers;

use App\Entity\Setting;
use Illuminate\View\View;

class SettingComposer
{
    public function compose(View $view): void
    {
        $view->with('setting', Setting::orderBy('id', 'asc')->first());
    }
}
