<?php

namespace App\Http\ViewComposers;

use App\Entity\Contract\Contract;
use App\Entity\Form;
use App\Entity\Investor\DepositLineVerification;
use App\Entity\Investor\InvestorRankVerification;
use App\Entity\Trader\CreditLevelUrVerification;
use App\Entity\Trader\CreditLevelVerification;
use App\Entity\Trader\CreditLineVerification;
use App\Entity\Transfer\Convert;
use App\Entity\Transfer\Transaction;
use App\Entity\UserChange;
use App\Entity\UserVerification;
use App\Helpers\StatusHelper;
use Illuminate\View\View;

class TotalComposer
{
    public function compose(View $view): void
    {
        $view->with('totalVerifications', UserVerification::wait()->count());
        $view->with('totalUserChanges', UserChange::wait()->count());
        $view->with('totalCreditLevelVerifications', CreditLevelVerification::wait()->count() + CreditLevelUrVerification::wait()->count());
        $view->with('totalCreditLineVerifications', CreditLineVerification::wait()->count());
        $view->with('totalInvestorRankVerifications', InvestorRankVerification::wait()->count());
        $view->with('totalDepositLineVerifications', DepositLineVerification::wait()->count());
        $view->with('totalForms', Form::count());
        $view->with('totalTransactions', Transaction::notApprove()->count());
        $view->with('totalConverts', Convert::new()->count());

        $countCredit = Contract::credit()
            ->has('payments', '<=', 1)
            ->whereHas('payments', function($query){
                $query->where('status', '<>', StatusHelper::STATUS_APPROVE);
            })->count();
        $view->with('totalContracts', Contract::deposit()->new()->count() + $countCredit);
    }
}
