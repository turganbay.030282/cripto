<?php

namespace App\Http\ViewComposers;

use App\Entity\Handbook\Asset;
use App\Entity\User;
use App\Helpers\NumberHelper;
use App\Services\Contract\Repositories\EloquentContractRepository;
use App\Services\Handbook\Repositories\EloquentHandbookRepository;
use App\Services\User\Repositories\UserRepository;
use Illuminate\View\View;

class TraderSidebarComposer
{
    private $repository;
    private $userRepository;
    private $handbookRepository;

    public function __construct(
        EloquentContractRepository $repository,
        EloquentHandbookRepository $handbookRepository,
        UserRepository $userRepository
    )
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->handbookRepository = $handbookRepository;
    }

    public function compose(View $view): void
    {
        /** @var User $user */
        $user = auth()->user();

        $view->with('user', $user);

        $view->with('sAmountContracts', $this->repository->creditContractValue($user));
        $view->with('sAmountPortfolioValue', $this->repository->portfolioValue($user));
        $view->with('sContractPays', $this->repository->creditContracts($user));

        $view->with('assetContracts', $this->repository->assetContracts($user));

        $view->with('sAssets', $this->handbookRepository->listAsset());

        $view->with('contractFreeAssets', $this->userRepository->contractFreeAssets($user));


    }
}
