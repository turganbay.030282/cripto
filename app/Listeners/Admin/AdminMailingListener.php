<?php

namespace App\Listeners\Admin;

use App\Entity\Mail\AdminMailing;
use App\Entity\Notifications;
use App\Entity\User;
use App\Events\Admin\AdminMailingEvent;
use App\Mail\Admin\Tpl;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminMailingListener implements ShouldQueue
{
    public function handle(AdminMailingEvent $event): void
    {
        /** @var AdminMailing $user */
        $adminMailing = $event->adminMailing;
        $users = null;
        $userQueries = User::user()->active();
        if ($adminMailing->isAll()) {
            $users = $userQueries->get();
        } elseif ($adminMailing->isInvestor()) {
            $users = $userQueries->where('is_investor', true)->get();
        } elseif ($adminMailing->isTrader()) {
            $users = $userQueries->where('is_investor', false)->get();
        } elseif ($adminMailing->isOnlySelect()) {
            $users = $adminMailing->adminMailingUsers;
        }

        if ($users) {
            foreach ($users as $user) {
                $user->notifications()->create([
                    'user_id' => $user->id,
                    'title' => $adminMailing->subject,
                    'body' => $adminMailing->body,
                ]);
                \Mail::to($user->email)->send(new Tpl($adminMailing));
            }
        }
    }
}
