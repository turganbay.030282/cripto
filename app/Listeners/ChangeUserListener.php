<?php

namespace App\Listeners;

use App\Entity\User;
use App\Entity\UserChange;
use App\Events\ChangeUserEvent;
use App\Mail\UserChange\Reject;
use App\Mail\UserChange\Approve;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class ChangeUserListener implements ShouldQueue
{
    public function handle(ChangeUserEvent $event): void
    {
        /** @var User $user */
        $user = $event->user;

        /** @var UserChange $userChange */
        $userChange = $event->userChange;

        if ($userChange->isReject()) {
            Mail::to($user)->send(new Reject($userChange));
        } else {
            Mail::to($user)->send(new Approve($userChange));
        }
    }
}
