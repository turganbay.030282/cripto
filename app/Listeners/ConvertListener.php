<?php

namespace App\Listeners;

use App\Entity\User;
use App\Events\ConvertEvent;
use App\Mail\NewConvert;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class ConvertListener implements ShouldQueue
{
    public function handle(ConvertEvent $event): void
    {
        /** @var User $user */
        $convert = $event->convert;

        Mail::to('assetscore@assetscore.io')
            ->send(new NewConvert($convert));
    }
}
