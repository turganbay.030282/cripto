<?php

namespace App\Listeners;

use App\Entity\Trader\CreditLevelVerification;
use App\Entity\User;
use App\Events\CreditLevelEvent;
use App\Mail\CreditLevel\Reject;
use App\Mail\CreditLevel\Approve;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class CreditLevelListener implements ShouldQueue
{
    public function handle(CreditLevelEvent $event): void
    {
        /** @var User $user */
        $user = $event->user;

        /** @var CreditLevelVerification $verification */
        $creditLevelVerification = $event->creditLevelVerification;

        if ($creditLevelVerification->isReject()) {
            Mail::to($user)->send(new Reject($user, $creditLevelVerification));
        } else {
            Mail::to($user)->send(new Approve($user));
        }
    }
}
