<?php

namespace App\Listeners;

use App\Entity\Trader\CreditLevelUrVerification;
use App\Entity\User;
use App\Events\CreditLevelUrEvent;
use App\Mail\CreditLevel\RejectUr;
use App\Mail\CreditLevel\Approve;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class CreditLevelUrListener implements ShouldQueue
{
    public function handle(CreditLevelUrEvent $event): void
    {
        /** @var User $user */
        $user = $event->user;

        /** @var CreditLevelUrVerification $verification */
        $creditLevelUrVerification = $event->creditLevelUrVerification;

        if ($creditLevelUrVerification->isReject()) {
            Mail::to($user)->send(new RejectUr($user, $creditLevelUrVerification));
        } else {
            Mail::to($user)->send(new Approve($user));
        }
    }
}
