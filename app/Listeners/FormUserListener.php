<?php

namespace App\Listeners;

use App\Entity\Form;
use App\Entity\User;
use App\Events\FormUserEvent;
use App\Mail\Form\FormMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class FormUserListener implements ShouldQueue
{
    public function handle(FormUserEvent $event): void
    {
        /** @var Form $form */
        $form = $event->form;
        Mail::to(User::admin()->get())->send(new FormMail($form));
    }
}
