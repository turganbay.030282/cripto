<?php

namespace App\Listeners;

use App\Entity\Tpl\NotifyText;
use App\Entity\User;
use App\Events\TplUserEvent;
use App\Mail\TplNotify;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class TplUserListener implements ShouldQueue
{

    public function handle(TplUserEvent $event): void
    {
        /** @var User $user */
        $user = $event->user;

        /** @var NotifyText $notifyText */
        $notifyText = $event->notifyText;

        $text = $notifyText->{'body:' . $user->lang};
        if ($event->text) {
            $text = $event->text;
        }

        $user->notifications()->create([
            'title' => $notifyText->{'subject:' . $user->lang},
            'body' => $text
        ]);

        Mail::to($user)->send(new TplNotify(
            $notifyText->{'subject:' . $user->lang},
            $text,
            $event->attachments
        ));
    }

}
