<?php

namespace App\Listeners\User;

use App\Entity\User;
use App\Events\User\PromoEvent;
use App\Mail\User\Tpl;
use Illuminate\Contracts\Queue\ShouldQueue;

class PromoListener implements ShouldQueue
{
    public function handle(PromoEvent $event): void
    {
        /** @var User $user */
        $user = $event->user;
        $email = $event->email;

        \Mail::to($email)->send(new Tpl(
            $user->full_name,
            $user->promo_code
        ));
    }
}
