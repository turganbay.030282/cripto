<?php

namespace App\Listeners\User;

use App\Entity\User;
use App\Events\User\PromoEvent;
use App\Mail\TplNotify;
use App\Mail\User\Tpl;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class RegisterRequestListener implements ShouldQueue
{
    public function handle(Registered $event): void
    {
        /** @var User $user */
        $user = $event->user;

        Mail::to('assetscore@assetscore.io')->send(new TplNotify(
            'Новая регистрация',
            "Новая регистрация. Пользователь: id - {$user->id}, email - {$user->email}"
        ));
    }
}
