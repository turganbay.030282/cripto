<?php

namespace App\Listeners;

use App\Entity\User;
use App\Entity\UserVerification;
use App\Events\VerifyUserEvent;
use App\Mail\Verify\Reject;
use App\Mail\Verify\Approve;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class VerifyUserListener implements ShouldQueue
{
    public function handle(VerifyUserEvent $event): void
    {
        /** @var User $user */
        $user = $event->user;

        /** @var UserVerification $verification */
        $verification = $event->verification;

        if ($verification->isReject()) {
            Mail::to($user)->send(new Reject($user, $verification));
        } else {
            Mail::to($user)->send(new Approve($user));
        }
    }
}
