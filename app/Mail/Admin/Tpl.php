<?php

namespace App\Mail\Admin;

use App\Entity\Mail\AdminMailing;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Tpl extends Mailable
{
    use Queueable, SerializesModels;

    public $adminMailing;

    public function __construct(AdminMailing $adminMailing)
    {
        $this->adminMailing = $adminMailing;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
            return $this->subject($this->adminMailing->subject)
            ->view('emails.admin.tpl');
    }
}
