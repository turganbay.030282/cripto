<?php

namespace App\Mail\Auth;

use App\Entity\Tpl\NotifyText;
use App\Entity\User;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyMail extends Mailable
{
    use SerializesModels;

    public $user;
    public $notify;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->notify = NotifyText::find(1);
    }

    public function build()
    {
        return $this
            ->subject($this->notify->{'subject:'.$this->user->lang})
            ->view('emails.auth.register.verify');
    }
}
