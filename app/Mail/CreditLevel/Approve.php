<?php

namespace App\Mail\CreditLevel;

use App\Entity\User;
use App\Entity\UserVerification;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Approve extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $body;
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->subject = trans('site.mail.credit-level-subject', [], $user->lang);
        $this->body = '<p>'.trans('site.mail.credit-level-approve', [], $user->lang).'</p>';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('site.mail.credit-level-subject', [], $this->user->lang))
            ->view('emails.tpl');
    }
}
