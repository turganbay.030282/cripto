<?php

namespace App\Mail\CreditLevel;

use App\Entity\Trader\CreditLevelVerification;
use App\Entity\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Reject extends Mailable
{
    use Queueable, SerializesModels;

    public $creditLevelVerification;
    public $subject;
    public $body;
    public $user;

    public function __construct(User $user, CreditLevelVerification $creditLevelVerification)
    {
        $this->creditLevelVerification = $creditLevelVerification;
        $this->user = $user;
        $this->subject = trans('site.mail.credit-level-subject', [], $user->lang);
        $this->body = '<p>'.trans('site.mail.credit-level-reject', [], $user->lang).'</p>';
        if ($creditLevelVerification->comment) {
            $this->body .= '<p>' . $creditLevelVerification->comment . '</p>';
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('site.mail.credit-level-subject', [], $this->user->lang))
            ->view('emails.tpl');
    }
}
