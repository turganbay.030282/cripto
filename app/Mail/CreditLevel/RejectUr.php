<?php

namespace App\Mail\CreditLevel;

use App\Entity\Trader\CreditLevelUrVerification;
use App\Entity\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RejectUr extends Mailable
{
    use Queueable, SerializesModels;

    public $creditLevelUrVerification;
    public $subject;
    public $body;
    public $user;

    public function __construct(User $user, CreditLevelUrVerification $creditLevelUrVerification)
    {
        $this->creditLevelUrVerification = $creditLevelUrVerification;
        $this->user = $user;
        $this->subject = trans('site.mail.credit-level-subject', [], $user->lang);
        $this->body = '<p>'.trans('site.mail.credit-level-reject', [], $user->lang).'</p>';
        if ($creditLevelUrVerification->comment) {
            $this->body .= '<p>' . $creditLevelUrVerification->comment . '</p>';
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('site.mail.credit-level-subject', [], $this->user->lang))
            ->view('emails.tpl');
    }
}
