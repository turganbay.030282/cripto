<?php

namespace App\Mail;

use App\Entity\Transfer\Convert;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewConvert extends Mailable
{
    use Queueable, SerializesModels;

    public $convert;

    public function __construct(Convert $convert)
    {
        $this->convert = $convert;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Новая заявка на конвертацию')
            ->view('emails.convert.new');
    }
}
