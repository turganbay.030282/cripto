<?php

namespace App\Mail\User;

use App\Entity\Mail\AdminMailing;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Tpl extends Mailable
{
    use Queueable, SerializesModels;

    public $userName;
    public $userPromo;

    public function __construct(string $userName, string $userPromo)
    {
        $this->userName = $userName;
        $this->userPromo = $userPromo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Регистрации на сервисе')
            ->view('emails.user.tpl');
    }
}
