<?php

namespace App\Mail\UserChange;

use App\Entity\UserChange;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Approve extends Mailable
{
    use Queueable, SerializesModels;

    public $userChange;

    public function __construct(UserChange $userChange)
    {
        $this->userChange = $userChange;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Заявление на изменение данных')
            ->view('emails.change.approve');
    }
}
