<?php

namespace App\Mail\Verify;

use App\Entity\User;
use App\Entity\UserVerification;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Reject extends Mailable
{
    use Queueable, SerializesModels;

    public $verification;
    public $subject;
    public $body;
    public $user;

    public function __construct(User $user, UserVerification $verification)
    {
        $this->verification = $verification;
        $this->user = $user;
        $this->subject = trans('site.mail.verify-subject', [], $user->lang);
        $this->body = '<p>'.trans('site.mail.verify-body-reject', [], $user->lang).'</p>';
        if ($verification->comment) {
            $this->body .= '<p>' . $verification->comment . '</p>';
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('site.mail.verify-subject', [], $this->user->lang))
            ->view('emails.tpl');
    }
}
