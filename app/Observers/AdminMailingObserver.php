<?php

namespace App\Observers;

use App\Entity\Mail\AdminMailing;
use App\Events\Admin\AdminMailingEvent;

class AdminMailingObserver
{
    public function created(AdminMailing $adminMailing)
    {
        broadcast(new AdminMailingEvent($adminMailing));
    }
}
