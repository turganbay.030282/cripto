<?php

namespace App\Observers;

use App\Entity\User;
use App\Services\Statistic\StatisticUserService;
use Carbon\Carbon;

class UserObserver
{
    /**
     * @var StatisticUserService
     */
    private $service;

    public function __construct(StatisticUserService $service)
    {
        $this->service = $service;
    }

    public function created(User $user)
    {
        if ($user->isUser()) {
            $this->service->register(Carbon::now()->month, Carbon::now()->year, $user->is_investor);
        }
    }

    public function updated(User $user)
    {
        if ($user->isDirty('is_verification') && $user->is_verification) {
            $this->service->verify(Carbon::now()->month, Carbon::now()->year);
        }
    }
}
