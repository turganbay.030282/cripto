<?php

namespace App\Providers;

use App\Entity\Mail\AdminMailing;
use App\Entity\User;
use App\Observers\AdminMailingObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;
use Form;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        AdminMailing::observe(AdminMailingObserver::class);
        User::observe(UserObserver::class);

        Form::component('error', 'components.form.error', ['name']);
        Form::component('btn_save', 'components.form.btn_save', []);
    }

    private function registerBindings()
    {
    }
}
