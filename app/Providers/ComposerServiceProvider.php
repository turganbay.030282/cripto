<?php

namespace App\Providers;

use App\Http\ViewComposers\CabinetComposer;
use App\Http\ViewComposers\CabinetVerifyComposer;
use App\Http\ViewComposers\ClientHeaderComposer;
use App\Http\ViewComposers\CountriesComposer;
use App\Http\ViewComposers\FooterPagesComposer;
use App\Http\ViewComposers\InvestorSidebarComposer;
use App\Http\ViewComposers\SettingComposer;
use App\Http\ViewComposers\TotalComposer;
use App\Http\ViewComposers\TraderSidebarComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        View::composer('admin.layouts.partials.menu._admin', TotalComposer::class);

        View::composer('front.*', SettingComposer::class);
        View::composer('emails.*', SettingComposer::class);

        View::composer('front.layouts.parts._footer', FooterPagesComposer::class);

        View::composer('front.layouts.parts._sidebar_trader', TraderSidebarComposer::class);

        View::composer('front.layouts.parts._sidebar_investor', InvestorSidebarComposer::class);

        View::composer('front.layouts.parts._header', ClientHeaderComposer::class);

        View::composer('front.layouts.cabinet', CabinetComposer::class);

        View::composer('front.cabinet.parts.modals._verify', CabinetVerifyComposer::class);

        View::composer('front.cabinet.trader.parts._modals_credit_level', CountriesComposer::class);
        View::composer('front.cabinet.investor.parts._modal_investor_rank', CountriesComposer::class);
        View::composer('front.cabinet.investor.parts._modal_investor_rank_ur', CountriesComposer::class);
    }
}
