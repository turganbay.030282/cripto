<?php

namespace App\Providers;

use App\Events\Admin\AdminMailingEvent;
use App\Events\ChangeUserEvent;
use App\Events\ConvertEvent;
use App\Events\CreditLevelEvent;
use App\Events\CreditLevelUrEvent;
use App\Events\FormUserEvent;
use App\Events\TplUserEvent;
use App\Events\User\PromoEvent;
use App\Events\VerifyUserEvent;
use App\Listeners\Admin\AdminMailingListener;
use App\Listeners\ChangeUserListener;
use App\Listeners\ConvertListener;
use App\Listeners\CreditLevelListener;
use App\Listeners\CreditLevelUrListener;
use App\Listeners\FormUserListener;
use App\Listeners\TplUserListener;
use App\Listeners\User\PromoListener;
use App\Listeners\User\RegisterRequestListener;
use App\Listeners\VerifyUserListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
            RegisterRequestListener::class,
        ],
        VerifyUserEvent::class => [
            VerifyUserListener::class,
        ],
        ChangeUserEvent::class => [
            ChangeUserListener::class,
        ],
        FormUserEvent::class => [
            FormUserListener::class,
        ],
        AdminMailingEvent::class => [
            AdminMailingListener::class,
        ],
        PromoEvent::class => [
            PromoListener::class,
        ],
        CreditLevelEvent::class => [
            CreditLevelListener::class,
        ],
        CreditLevelUrEvent::class => [
            CreditLevelUrListener::class,
        ],

        TplUserEvent::class => [
            TplUserListener::class,
        ],

        ConvertEvent::class => [
            ConvertListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
