<?php

namespace App\Providers;

use App\AppServices\Sms\ArraySender;
use App\AppServices\Sms\Esteria;
use App\AppServices\Sms\SmsSender;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(SmsSender::class, function (Application $app) {
            $config = config('services.sms');

            switch ($config['driver']) {
                case 'esteria':
                    $params = $config['drivers']['esteria'];
                    if (!empty($params['url'])) {
                        return new Esteria($params['app_key'], $params['url']);
                    }
                    return new Esteria($params['app_key']);
                case 'array':
                    return new ArraySender();
                default:
                    throw new \InvalidArgumentException('Undefined SMS driver ' . $config['driver']);
            }
        });
    }
}
