<?php

namespace App\Services\AdminMailing;

use App\Entity\Mail\AdminMailing;
use App\Events\Admin\AdminMailingEvent;
use App\Services\AdminMailing\Requests\AdminMailingRequest;

class AdminMailingService
{

    public function new(AdminMailingRequest $request): AdminMailing
    {
        return \DB::transaction(function () use ($request) {
            $adminMailing = AdminMailing::create($request->only('list_user', 'subject', 'body'));
            if ($adminMailing->isOnlySelect() && $request->get('users')) {
                $adminMailing->adminMailingUsers()->attach($request->get('users'));
            }

            return $adminMailing;
        });
    }

}
