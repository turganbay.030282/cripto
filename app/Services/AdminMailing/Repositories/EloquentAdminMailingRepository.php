<?php

namespace App\Services\AdminMailing\Repositories;

use App\Entity\Mail\AdminMailing;
use App\Services\AdminMailing\Requests\FilterRequest;
use Illuminate\Database\Eloquent\Builder;

class EloquentAdminMailingRepository
{
    public function find(int $id)
    {
        return AdminMailing::find($id);
    }

    public function search(FilterRequest $request)
    {
        $query = AdminMailing::sortable(['id' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    private function filters(Builder $query, FilterRequest $request)
    {
        if ($request->get('id')) {
            $query->where('id', $request->get('id'));
        }
        if ($request->get('subject')) {
            $query->where('subject', 'like', '%' . $request->get('subject') . '%');
        }
        if ($request->get('list_user')) {
            $query->where('list_user', $request->get('list_user'));
        }
    }

}
