<?php

namespace App\Services\AdminMailing\Requests;

use App\Entity\Mail\AdminMailing;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AdminMailingRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'list_user' => ['required', 'string', Rule::in(array_keys(AdminMailing::listListTo()))],
            'subject' => 'required|string|max:255',
            'body' => 'required|string',
            'users.*' => 'nullable|integer|exists:users,id'
        ];
    }
}
