<?php

namespace App\Services\AdminMailing\Requests;

use App\Entity\Mail\AdminMailing;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer',
            'subject' => 'nullable|string|max:255',
            'list_user' => ['nullable', 'string', Rule::in(array_keys(AdminMailing::listListTo()))],
        ];
    }
}
