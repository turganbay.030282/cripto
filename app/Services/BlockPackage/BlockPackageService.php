<?php

namespace App\Services\BlockPackage;

use App\Entity\Content\BlockPackage;
use App\Services\BlockPackage\Requests\CreateRequest;
use App\Services\BlockPackage\Requests\UpdateRequest;

class BlockPackageService
{
    public function new(CreateRequest $request): BlockPackage
    {
        return \DB::transaction(function () use ($request) {
            $blockPackage = BlockPackage::create($request->except('photo'));

            $photo = $request["photo"];
            if (!empty($photo)) {
                $blockPackage->addImage($photo, null);
            }

            return $blockPackage;
        });
    }

    public function edit(UpdateRequest $request, BlockPackage $blockPackage): void
    {
        \DB::transaction(function () use ($request, $blockPackage) {
            $blockPackage->update($request->except('photo'));

            $photo = $request["photo"];
            if (!empty($photo)) {
                $blockPackage->editImage($photo, null);
            }
        });
    }

    public function restore($id)
    {
        BlockPackage::onlyTrashed()
            ->where('id', $id)
            ->restore();
    }
}
