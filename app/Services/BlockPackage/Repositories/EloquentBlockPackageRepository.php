<?php

namespace App\Services\BlockPackage\Repositories;

use App\Entity\Content\BlockPackage;
use App\Services\BlockPackage\Requests\FilterRequest;
use Illuminate\Database\Eloquent\Builder;

class EloquentBlockPackageRepository
{
    public function find(int $id)
    {
        return BlockPackage::find($id);
    }

    public function search(FilterRequest $request)
    {
        $query = BlockPackage::sortable(['created_at' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    private function filters(Builder $query, FilterRequest $request)
    {
        if ($request->get('id')) {
            $query->where('id', $request->get('id'));
        }
        if ($request->get('name')) {
            $query->where('name', '%'.$request->get('name').'%');
        }
        if ($request->get('desc')) {
            $query->where('desc', '%'.$request->get('desc').'%');
        }
        if ($request->get('price')) {
            $query->where('price', '%'.$request->get('price').'%');
        }
    }
}
