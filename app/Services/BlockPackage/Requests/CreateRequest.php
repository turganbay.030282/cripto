<?php

namespace App\Services\BlockPackage\Requests;

use App\Helpers\PublishHelper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name.*' => 'required|string|max:255',
            'desc.*' => 'nullable|string',
            'price.*' => 'required|string',
            'photo' => 'required|image',
            'type' => 'required|integer',
        ];
    }
}
