<?php

namespace App\Services\BlockPackage\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer',
            'name' => 'nullable|string|max:255',
            'desc' => 'nullable|string',
            'price' => 'nullable|string',
        ];
    }
}
