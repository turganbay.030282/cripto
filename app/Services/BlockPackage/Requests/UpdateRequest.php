<?php

namespace App\Services\BlockPackage\Requests;

class UpdateRequest extends CreateRequest
{
    public function rules(): array
    {
        $rules = parent::rules();
        $rules['photo'] = 'nullable|image';
        return $rules;
    }
}
