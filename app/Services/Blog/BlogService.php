<?php

namespace App\Services\Blog;

use App\Entity\Content\Blog\BlogCategory;
use App\Entity\Content\Blog\BlogItem;
use App\Services\Blog\Requests\CreateCategoryRequest;
use App\Services\Blog\Requests\UpdateCategoryRequest;
use App\Services\Blog\Requests\CreateRequest;
use App\Services\Blog\Requests\UpdateRequest;

class BlogService
{

    public function newCategory(CreateCategoryRequest $request): BlogCategory
    {
        return BlogCategory::create($request->all());
    }

    public function editCategory(UpdateCategoryRequest $request, BlogCategory $category): void
    {
        $category->update($request->all());
    }

    public function new(CreateRequest $request): BlogItem
    {
        return \DB::transaction(function() use($request) {
            $request['author_id'] = auth()->id();
            $blog = BlogItem::create($request->except('categories', 'photo', 'crop'));
            $blog->categories()->sync($request->get('categories'));

            $photo = $request["photo"];
            if(!empty($photo)){
                $blog->addImage($photo, $request["crop"]);
            }

            $cover = $request["cover"];
            if(!empty($cover)){
                $blog->addImage($cover, null, 'cover', 'covers');
            }

            return $blog;
        });
    }

    public function edit(UpdateRequest $request, BlogItem $blog): void
    {

        \DB::transaction(function() use($request, $blog) {
            $blog->update($request->except('categories', 'photo', 'crop'));
            $blog->categories()->sync($request->get('categories'));

            $photo = $request["photo"];
            if(!empty($photo)){
                $blog->editImage($photo, $request["crop"]);
            }

            $cover = $request["cover"];
            if(!empty($cover)){
                $blog->editImage($cover, null, 'cover', 'covers');
            }
        });
    }

    public function viewNew(BlogItem $new): void
    {
        $key = 'blog' . $new->id;
        if (!\Session::has($key)) {
            $new->increment('count_view');
            \Session::put($key, 1);
        }
    }

}
