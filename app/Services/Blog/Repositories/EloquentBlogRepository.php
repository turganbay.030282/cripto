<?php

namespace App\Services\Blog\Repositories;

use App\Entity\Content\Blog\BlogCategory;
use App\Entity\Content\Blog\BlogItem;
use App\Services\Blog\Requests\FilterRequest;
use Illuminate\Database\Eloquent\Builder;

class EloquentBlogRepository
{

    public function find(int $id)
    {
        return BlogItem::find($id);
    }

    public function findCategory(int $id)
    {
        return BlogCategory::find($id);
    }

    public function search(FilterRequest $request)
    {
        $query = BlogItem::sortable(['id' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    public function searchCategory(FilterRequest $request)
    {
        $query = BlogCategory::sortable(['id' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    public function getCategory()
    {
        return BlogCategory::get();
    }

    private function filters(Builder $query, FilterRequest $request)
    {
        if ($request->get('id')) {
            $query->where('id', $request->get('id'));
        }
        if ($request->get('title')) {
            $query->where('title', $request->get('title'));
        }
        if ($request->get('description')) {
            $query->where('description', $request->get('description'));
        }
        if ($request->get('publish')) {
            $query->where('publish', $request->get('publish'));
        }
    }

}
