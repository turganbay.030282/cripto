<?php

namespace App\Services\Blog\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title.*' => 'required|string|max:255',
            'slug' => 'required|string|max:255|unique:blog_categories,id',
            'description.*' => 'nullable|string',
        ];
    }
}
