<?php

namespace App\Services\Blog\Requests;

use App\Helpers\PublishHelper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title.*' => 'required|string|max:255',
            'slug' => 'required|string|max:255|unique:blog_categories,id',
            'publish' => ['integer',  Rule::in(array_keys(PublishHelper::statusList()))],
            'categories.*' => 'required|exists:blog_categories,id',
            'photo' => 'required|image|mimes:jpg,jpeg,png,svg',
            'crop' => 'nullable|string',
            'description.*' => 'required|string',
            'content.*' => 'required|string',
            'meta_title.*' => 'nullable|string|max:255',
            'meta_desc.*' => 'nullable|string|max:500',
        ];
    }
}
