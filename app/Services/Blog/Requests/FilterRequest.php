<?php

namespace App\Services\Blog\Requests;

use App\Helpers\PublishHelper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer',
            'title' => 'nullable|string|max:255',
            'publish' => ['nullable', 'integer',  Rule::in(array_keys(PublishHelper::statusList()))],
        ];
    }
}
