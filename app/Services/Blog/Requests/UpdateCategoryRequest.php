<?php

namespace App\Services\Blog\Requests;

use App\Entity\Content\Blog\BlogCategory;

/**
 * @property BlogCategory $blog_category
 */
class UpdateCategoryRequest extends CreateCategoryRequest
{
    public function rules(): array
    {
        $rules = parent::rules();
        $rules['slug'] = 'required|string|max:255|unique:blog_categories,slug,'.$this->blog_category->id;
        return $rules;
    }
}
