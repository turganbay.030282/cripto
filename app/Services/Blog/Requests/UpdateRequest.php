<?php

namespace App\Services\Blog\Requests;


use App\Entity\Content\Blog\BlogItem;

/**
 * @property BlogItem $blog
 */
class UpdateRequest extends CreateRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = parent::rules();
        $rules['slug'] = 'required|string|max:255|unique:blog_categories,slug,' . $this->blog->id;
        $rules['photo'] = 'nullable|image|mimes:jpg,jpeg,png,svg';
        return $rules;
    }
}
