<?php

namespace App\Services\Certificate;

use App\Entity\Content\Certificate;
use App\Services\Certificate\Requests\CreateRequest;
use App\Services\Certificate\Requests\UpdateRequest;

class CertificateService
{

    public function new(CreateRequest $request): Certificate
    {
        return \DB::transaction(function () use ($request) {
            $certificate = Certificate::create($request->except('photo', 'crop', 'file'));

            $file = $request["file"];
            if (!empty($file)) {
                $certificate->addFile($file);
            }

            $photo = $request["photo"];
            if (!empty($photo)) {
                $certificate->addImage($photo, $request["crop"]);
            }

            $cover = $request["cover"];
            if (!empty($cover)) {
                $certificate->addImage($cover, null, 'cover', 'covers');
            }

            return $certificate;
        });
    }

    public function edit(UpdateRequest $request, Certificate $certificate): void
    {
        \DB::transaction(function () use ($request, $certificate) {
            $certificate->update($request->except('photo', 'crop'));

            $file = $request["file"];
            if (!empty($file)) {
                $certificate->editFile($file);
            }

            $photo = $request["photo"];
            if (!empty($photo)) {
                $certificate->editImage($photo, $request["crop"]);
            }

            $cover = $request["cover"];
            if (!empty($cover)) {
                $certificate->editImage($cover, null, 'cover', 'covers');
            }
        });
    }

    public function restore($id)
    {
        Certificate::onlyTrashed()
            ->where('id', $id)
            ->restore();
    }

}
