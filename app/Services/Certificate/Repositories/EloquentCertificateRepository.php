<?php

namespace App\Services\Certificate\Repositories;

use App\Entity\Content\Certificate;
use App\Services\Certificate\Requests\FilterRequest;
use Illuminate\Database\Eloquent\Builder;

class EloquentCertificateRepository
{
    public function find(int $id)
    {
        return Certificate::find($id);
    }

    public function search(FilterRequest $request)
    {
        $query = Certificate::sortable(['created_at' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    private function filters(Builder $query, FilterRequest $request)
    {
        if ($request->get('id')) {
            $query->where('id', $request->get('id'));
        }
        if ($request->get('title')) {
            $query->where('title', $request->get('title'));
        }
        if ($request->get('description')) {
            $query->where('description', $request->get('description'));
        }
        if ($request->get('publish')) {
            $query->where('publish', $request->get('publish'));
        }
    }

}
