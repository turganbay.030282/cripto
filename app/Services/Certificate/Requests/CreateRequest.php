<?php

namespace App\Services\Certificate\Requests;

use App\Helpers\PublishHelper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title.*' => 'required|string|max:255',
            'description.*' => 'required|string',
            'file' => 'required|file|mimes:pdf',
            'photo' => 'required|image',
            'crop' => 'nullable|string',
        ];
    }
}
