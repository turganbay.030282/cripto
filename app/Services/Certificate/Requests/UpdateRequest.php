<?php

namespace App\Services\Certificate\Requests;

class UpdateRequest extends CreateRequest
{
    public function rules(): array
    {
        $rules = parent::rules();
        $rules['photo'] = 'nullable|image';
        $rules['file'] = 'nullable|file|mimes:pdf';
        return $rules;
    }
}
