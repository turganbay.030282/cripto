<?php

namespace App\Services\Contract;

use App\AppServices\Sms\SmsSender;
use App\Entity\CodeVerifiedField;
use App\Entity\Contract\Contract;
use App\Entity\Handbook\Asset;
use App\Entity\Handbook\TypeTransaction;
use App\Entity\Setting;
use App\Entity\Transfer\Transaction;
use App\Entity\User;
use App\Helpers\StatusHelper;
use App\Helpers\TypeUserHelper;
use App\Services\Contract\Requests\ContractCreateRequest;
use App\Services\Contract\Requests\ContractDepositCreateRequest;
use App\Services\Contract\Requests\ContractPaymentRequest;
use App\Services\Tpl\TplService;
use App\Services\User\G2faService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ContractService
{
    /**
     * @var TplService
     */
    private $tplService;
    /**
     * @var SmsSender
     */
    private $sms;
    /**
     * @var G2faService
     */
    private $g2faService;

    public function __construct(TplService $tplService, G2faService $g2faService, SmsSender $sms)
    {
        $this->tplService = $tplService;
        $this->sms = $sms;
        $this->g2faService = $g2faService;
    }

    public function addContract(ContractCreateRequest $request, User $user): Model
    {
        if (!$user->is_verification) {
            throw new \DomainException(trans('site.cabinet.user-not-verification'));
        }
        if (!$user->credit_level_id) {
            throw new \DomainException(trans('site.cabinet.user-not-credit-level'));
        }
        if ($user->balance_trader < $request->get('amount')) {
            throw new \DomainException(trans('site.cabinet.credit-line-lt'));
        }
        if ($user->hasExpiredContractPayment()) {
            throw new \DomainException(trans('site.cabinet.trader-payment-expired'));
        }

        $codeVerified = $user->codeVerifies()->traderContract()->orderBy('id', 'desc')->first();
        if ($request->get('verify_code') !== $codeVerified->verify_token) {
            throw new \DomainException(trans('site.cabinet.Incorrect-verify-code'));
        }

        if ($user->google2fa_enable && !$this->g2faService->verifySecretUser($user, $request->get('g_verify_code'))) {
            throw new \DomainException(trans('site.cabinet.2gaf-invalid-verification-code'));
        }

        return DB::transaction(function () use ($request, $user, $codeVerified) {
            $asset = Asset::findOrFail($request->get('type_asset_id'));
            $typeTransaction = TypeTransaction::trader()
                ->where('min', '<=', $request->get('amount'))
                ->where('max', '>=', $request->get('amount'))
                ->firstOrFail();

            $request['number'] = $user->contract_number;
            $request['type'] = Contract::TYPE_CREDIT;
            $request['amount_asset'] = number_format($request->get('amount') / $asset->coinValue->price, 8, '.', '');
            $request['document'] = $this->tplService->docForTrader(
                $user,
                $request->get('period'),
                $asset->coinValue->symbol,
                $request->get('amount'),
                $request->get('amount_asset'),
                $typeTransaction->getValueBonus(auth()->user()),
                Carbon::now()->format('d.m.Y')
            );
            $contract = $user->contracts()->create(array_merge(
                ['rate' => $typeTransaction->getValueBonus(auth()->user())],
                $request->only(
                    'number',
                    'type_asset_id',
                    'period',
                    'amount',
                    'amount_asset',
                    'document'
                )
            ));

            $contract->addImage($request['pay_ticket'], null, 'pay_ticket', 'contract_payments');

            $codeVerified->update(['verify_token_expire' => Carbon::now()]);

            $index = 1;
            for ($i = 0; $i < $contract->period; $i++) {
                $contract->payments()->create([
                    'main_amount' => format_number($contract->amount / $contract->period),
                    'balance_owed' => format_number($contract->amount - $index * $contract->amount / $contract->period),
                    'percent' => format_number(format_number($contract->calcTraderAmount()) - format_number($contract->amount / $contract->period)),
                    'amount' => format_number($contract->calcTraderAmount()),
                    'date' => $contract->created_at->addMonths($i)->format("d.m.Y"),
                    'pay_ticket' => $i == 0 ? $contract->pay_ticket : null
                ]);
                $index++;
            }

//            $payment = $contract->payments->first();
//            $setting = Setting::first();
//            $payment->update([
//                'penalty' => format_number($contract->amount * $setting->penalty / 100)
//            ]);
            Transaction::create([
                'number' => $contract->user_id . $contract->number . ($user->transactions->count() + 1),
                'credit_payment_id' => $contract->payments->first()->id,
                'user_id' => $contract->user_id,
                'amount' => format_number($contract->calcTraderAmount()),
                'type_user' => TypeUserHelper::TYPE_USER_TRADER,
                'type_transaction' => Transaction::TYPE_CREDIT,
                'status' => StatusHelper::STATUS_NEW
            ]);

            return $contract;
        });
    }

    public function addPortfolioGraph(User $user, $amount)
    {
        $user->portfolioGraphs()->create([
            'amount' => $amount,
            'tm' => time()
        ]);
    }

    public function addContractDeposit(ContractDepositCreateRequest $request, User $user): Model
    {
        $codeVerified = $user->codeVerifies()->investorContract()->orderBy('id', 'desc')->first();
        if ($request->get('verify_code') !== $codeVerified->verify_token) {
            throw new \DomainException(trans('site.cabinet.Incorrect-verify-code'));
        }

        if ($user->google2fa_enable && !$this->g2faService->verifySecretUser($user, $request->get('g_verify_code'))) {
            throw new \DomainException(trans('site.cabinet.2gaf-invalid-verification-code'));
        }

        return DB::transaction(function () use ($request, $user) {
            $asset = Asset::findOrFail($request->get('type_asset_id'));
            $rate = Asset::getMonthPercent(
                $asset->id, $request->get('amount'), $request->get('period')
            );

            $request['number'] = $user->contract_number;
            $request['type'] = Contract::TYPE_DEPOSIT;
            $request['document'] = $this->tplService->docForInvestor(
                $user,
                $asset->id,
                $request->get('period'),
                $request->get('amount'),
                $rate
            );

            /** @var Contract $contract */
            $contract = $user->contracts()->create(array_merge(
                [
                    'rate' => $rate,
                    'course' => $asset->coinValue->price,
                    'amount_asset' => numeric_fmt($request->get('amount') / $asset->coinValue->price)
                ],
                $request->only(
                    'type_asset_id',
                    'number',
                    'type',
                    'period',
                    'amount',
                    'document'
                )
            ));
            //$contract->addImage($request['pay_ticket'], null, 'pay_ticket');

            $index = 1;
            for ($i = $contract->period; $i > 0; $i--) {
                $percent = $contract->calcInvestorAmount();
                $amount = $contract->amount + $index * $percent;
                $contract->investProfits()->create([
                    'percent' => format_number($percent),
                    'amount' => format_number($amount),
                    'percent_dai' => numeric_fmt($percent / $asset->coinValue->price),
                    'date' => $contract->created_at->addMonths($index)->format("d.m.Y")
                ]);
                $index++;
            }

            $transaction = Transaction::create([
                'number' => $contract->user_id . $contract->number . ($user->transactions->count() + 1),
                'coinmarketcap_id' => $asset->coinmarketcap_id,
                'deposit_payment_id' => $contract->id,
                'user_id' => $contract->user_id,
                'amount' => format_number($contract->amount),
                'amount_dai' => numeric_fmt($contract->amount / $asset->coinValue->price),
                'type_user' => TypeUserHelper::TYPE_USER_INVESTOR,
                'type_transaction' => Transaction::TYPE_DEPOSIT,
                'status' => StatusHelper::STATUS_NEW
            ]);

            $user->moneyToBalance($transaction->coinmarketcap_id, 0);

            return $contract;
        });
    }

    public function addContractDepositDai(ContractDepositCreateRequest $request, User $user): Model
    {
        $codeVerified = $user->codeVerifies()->investorContract()->orderBy('id', 'desc')->first();
        if ($request->get('verify_code') !== $codeVerified->verify_token) {
            throw new \DomainException(trans('site.cabinet.Incorrect-verify-code'));
        }

        if ($user->google2fa_enable && !$this->g2faService->verifySecretUser($user, $request->get('g_verify_code'))) {
            throw new \DomainException(trans('site.cabinet.2gaf-invalid-verification-code'));
        }

        $balance = $user->balances->where('asset_id', $request->get('type_asset_id'))->first();

        if (!$balance || $balance->amount < $request->get('amount')) {
            throw new \DomainException(trans('site.cabinet.investor-deposit-dai-error-balance'));
        }

        return DB::transaction(function () use ($request, $user) {
            $asset = Asset::findOrFail($request->get('type_asset_id'));
            $rate = Asset::getMonthPercent(
                $asset->id, $request->get('amount'), $request->get('period')
            );

            $request['number'] = $user->contract_number;
            $request['type'] = Contract::TYPE_DEPOSIT;
            $request['document'] = $this->tplService->docForInvestorDai(
                $user,
                $asset->id,
                $request->get('period'),
                $request->get('amount'),
                $rate
            );

            /** @var Contract $contract */
            $contract = $user->contracts()->create(array_merge(
                [
                    'rate' => $rate,
                    'course' => $asset->coinValue->price,
                    'amount' => format_number($request->get('amount') * $asset->coinValue->price),
                    'amount_asset' => numeric_fmt($request->get('amount')),
                ],
                $request->only(
                    'type_asset_id',
                    'number',
                    'type',
                    'period',
                    'document'
                )
            ));
            //$contract->addImage($request['pay_ticket'], null, 'pay_ticket');

            $index = 1;
            for ($i = $contract->period; $i > 0; $i--) {
                $percent = $contract->calcInvestorAmount();
                $amount = $contract->amount + $index * $percent;
                $contract->investProfits()->create([
                    'percent' => format_number($percent),
                    'amount' => format_number($amount),
                    'percent_dai' => numeric_fmt($percent / $asset->coinValue->price),
                    'date' => $contract->created_at->addMonths($index)->format("d.m.Y")
                ]);
                $index++;
            }

            $transaction = Transaction::create([
                'number' => $contract->user_id . $contract->number . ($user->transactions->count() + 1),
                'deposit_payment_id' => $contract->id,
                'coinmarketcap_id' => $asset->coinmarketcap_id,
                'user_id' => $contract->user_id,
                'amount' => $contract->amount,
                'amount_dai' => $contract->amount_asset,
                'type_user' => TypeUserHelper::TYPE_USER_INVESTOR,
                'type_transaction' => Transaction::TYPE_DEPOSIT_DAI,
                'status' => StatusHelper::STATUS_NEW
            ]);

            $user->moneyToBalance($transaction->coinmarketcap_id, 0);

            return $contract;
        });
    }

    public function requestPhoneVerification(User $user, Carbon $now): void
    {
        if (empty($user->phone)) {
            throw new \DomainException(trans('site.cabinet.phone-empty'));
        }

        if ($user->hasExpiredContractPayment()) {
            throw new \DomainException(trans('site.cabinet.trader-payment-expired'));
        }

        $codeVerified = $user->codeVerifies()->traderContract()->orderBy('id', 'desc')->first();
        if (!empty($codeVerified->verify_token) && $codeVerified->verify_token_expire && $codeVerified->verify_token_expire->gt($now)) {
            throw new \DomainException(trans('site.cabinet.Code-already-send'));
        }

        $codeVerifiedNew = $user->codeVerifies()->create([
            'type' => CodeVerifiedField::TYPE_TRADER_CONTRACT,
            'verify_token' => (string)random_int(1000000, 9999999),
            'verify_token_expire' => $now->copy()->addSeconds(180)
        ]);

        $this->sms->send($user->phone, trans('site.cabinet.Verification-code') . ': ' . $codeVerifiedNew->verify_token);
    }

    public function requestPhoneVerificationInvestor(User $user, Carbon $now): void
    {
        if (empty($user->phone)) {
            throw new \DomainException(trans('site.cabinet.phone-empty'));
        }

        $codeVerified = $user->codeVerifies()->traderContract()->orderBy('id', 'desc')->first();
        if (!empty($codeVerified->verify_token) && $codeVerified->verify_token_expire && $codeVerified->verify_token_expire->gt($now)) {
            throw new \DomainException(trans('site.cabinet.Code-already-send'));
        }

        $codeVerifiedNew = $user->codeVerifies()->create([
            'type' => CodeVerifiedField::TYPE_INVESTOR_CONTRACT,
            'verify_token' => (string)random_int(1000000, 9999999),
            'verify_token_expire' => $now->copy()->addSeconds(180)
        ]);

        $this->sms->send($user->phone, trans('site.cabinet.Verification-code') . ': ' . $codeVerifiedNew->verify_token);
    }

    public function paymentTraderContract(ContractPaymentRequest $request, Contract $contract): void
    {
        if ($contract->user_id != auth()->id()) {
            abort(403);
        }

        $payment = $contract->getPayment();
        if (!$payment) {
            throw new \DomainException('Credit payment not found');
        }
        if ($payment->pay_ticket) {
            throw new \DomainException('Credit payment already send');
        }
        $client = $contract->client;
        $payment->addImage($request['pay_ticket'], null, 'pay_ticket');
        Transaction::create([
            'number' => $contract->user_id . $contract->number . ($client->transactions->count() + 1),
            'user_id' => $contract->user_id,
            'credit_payment_id' => $payment->id,
            'amount' => $payment->amount,
            'type_user' => TypeUserHelper::TYPE_USER_TRADER,
            'type_transaction' => Transaction::TYPE_CREDIT,
            'status' => StatusHelper::STATUS_NEW
        ]);
    }


    public function paymentAllTraderContract(ContractPaymentRequest $request): void
    {
        DB::transaction(function () use ($request) {
            $contracts = Contract::query()->whereIn('id', $request->get('contractIds'))->get();
            if ($contracts){
                $numbers = explode(',', $request->get('contract_payment_numbers'));
                if (count($numbers) !== $contracts->count()) {
                    throw new \DomainException('Credit payment numbers not found');
                }
                foreach ($contracts as $contract){
                    if ($contract->user_id != auth()->id()) {
                        abort(403);
                    }
                    $payment = $contract->getPayment();

                    if (!$payment) {
                        throw new \DomainException('Credit payment not found');
                    }

//                    if ($payment->pay_ticket) {
//                        throw new \DomainException('Credit payment already send');
//                    }

                    $payment->addImage($request['pay_ticket'], null, 'pay_ticket');
                    Transaction::create([
                        'number' => trim(array_shift($numbers)),
                        'user_id' => $contract->user_id,
                        'credit_payment_id' => $payment->id,
                        'amount' => $payment->amount,
                        'type_user' => TypeUserHelper::TYPE_USER_TRADER,
                        'type_transaction' => Transaction::TYPE_CREDIT,
                        'status' => StatusHelper::STATUS_NEW
                    ]);
                }
            }
        });
    }

    public function redeem(ContractPaymentRequest $request, Contract $contract): void
    {
        if ($contract->user_id != auth()->id()) {
            abort(403);
        }

        if ($contract->isClose()) {
            throw new \DomainException('Contract is already closed');
        }

        $payment = $contract->getPayment();
        if (!$payment) {
            throw new \DomainException('Credit payment not found');
        }
        if ($payment->pay_ticket) {
            throw new \DomainException('Credit payment already send');
        }

        DB::transaction(function () use ($request, $contract, $payment) {
            $client = $contract->client;
            $setting = Setting::firstOrFail();

            $contract->update(['status' => StatusHelper::STATUS_WAIT_CLOSE]);
            $payment->addImage($request['pay_ticket'], null, 'pay_ticket');

            Transaction::create([
                'is_redeem' => true,
                'number' => $client->contract_payment_number,
                'user_id' => $client->id,
                'credit_payment_id' => $payment->id,
                'amount' => $contract->getCurrentContractPaymentAmount() +
                    format_number(
                        $contract->amount * $setting->commission_sell_trader / 100
                    ),
                'type_user' => TypeUserHelper::TYPE_USER_TRADER,
                'type_transaction' => Transaction::TYPE_CREDIT,
                'status' => StatusHelper::STATUS_NEW
            ]);
        });
    }

    public function sell(Contract $contract): void
    {
        if ($contract->user_id != auth()->id()) {
            abort(403);
        }
        if ($contract->isClose()) {
            throw new \DomainException('Contract is already closed');
        }
        if (!($contract->firstPayment && $contract->firstPayment->isApprove())) {
            throw new \DomainException('First payment is not approve');
        }

        DB::transaction(function () use ($contract) {
            $client = $contract->client;
            $setting = Setting::firstOrFail();
            $contract->update(['status' => StatusHelper::STATUS_WAIT_CLOSE]);
            Transaction::create([
                'is_sell' => true,
                'user_id' => $client->id,
                'credit_payment_id' => $contract->getPayment()->id,
                'amount' => format_number($contract->getCurrentAmountAssetCost()) -
                    format_number(
                        $contract->getCurrentAmountAssetCost() * $setting->commission_trader / 100
                    ) - $contract->getCurrentContractPaymentAmount(),
                'type_user' => TypeUserHelper::TYPE_USER_TRADER,
                'type_transaction' => Transaction::TYPE_TRADER_CONTRACT_SELL,
                'type' => Transaction::TYPE_REQUEST,
                'status' => StatusHelper::STATUS_NEW
            ]);
        });
    }

    public function terminate(Contract $contract): void
    {
        if ($contract->user_id != auth()->id()) {
            abort(403);
        }

        if ($contract->isClose()) {
            throw new \DomainException('Contract is already closed');
        }

        DB::transaction(function () use ($contract) {
            $setting = Setting::firstOrFail();
            $contract->update(['status' => StatusHelper::STATUS_WAIT_CLOSE]);

            Transaction::create([
                'user_id' => $contract->user_id,
                'deposit_payment_id' => $contract->id,
                'amount' => format_number($contract->amount - $contract->amount * $setting->commission_investor / 100),
                'amount_dai' => numeric_fmt(($contract->amount - $contract->amount * $setting->commission_investor / 100) / $contract->typeAsset->coinValue->price),
                'type_user' => TypeUserHelper::TYPE_USER_INVESTOR,
                'type_transaction' => Transaction::TYPE_DEPOSIT,
                'status' => StatusHelper::STATUS_NEW,
                'type' => Transaction::TYPE_OUT,
            ]);
        });
    }

}
