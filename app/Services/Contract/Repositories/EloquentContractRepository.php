<?php

namespace App\Services\Contract\Repositories;

use App\Entity\Contract\Contract;
use App\Entity\Handbook\Asset;
use App\Entity\User;
use App\Helpers\NumberHelper;
use App\Helpers\StatusHelper;
use App\Services\Contract\Requests\FilterRequest;
use Illuminate\Database\Eloquent\Builder;

class EloquentContractRepository
{
    public function find(int $id)
    {
        return Contract::find($id);
    }

    public function search(FilterRequest $request)
    {
        $query = Contract::with('client', 'typeAsset')->sortable(['created_at' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    private function filters(Builder $query, FilterRequest $request)
    {
        if ($request->get('id')) {
            $query->where('id', $request->get('id'));
        }
        if ($request->get('asset_id')) {
            $query->where('type_asset_id', $request->get('asset_id'));
        }
        if ($request->get('number')) {
            $query->where('number', 'LIKE', '%' . $request->get('number') . '%');
        }
        if ($request->get('period')) {
            $query->where('period', $request->get('period'));
        }
        if ($request->get('rate')) {
            $query->where('rate', $request->get('rate'));
        }
    }

    public function portfolioValue($user)
    {
        $assetContracts = Asset::with(['contracts' => function ($query) use ($user) {
                $query->approve();
                $query->where('user_id', $user->id);
                $query->orderBy('id', 'desc');
            }, 'coinValue'])
            ->whereHas('contracts', function ($query) use ($user) {
                $query->approve();
                $query->where('user_id', $user->id);
                $query->orderBy('id', 'desc');
            })
            ->orderBy('id', 'desc')
            ->get();

        $total = 0;
        foreach ($assetContracts as $assetContract) {
            $total += NumberHelper::format_number($assetContract->contracts->sum('amount_asset') * $assetContract->coinValue->price);
        }
        return $total;
    }

    public function assetContracts(User $user)
    {
        return Asset::with(['contracts' => function ($query) use ($user) {
                $query->credit();
                $query->approve();
                $query->where('user_id', $user->id);
                $query->orderBy('id', 'desc');
            }])
            ->whereHas('contracts', function ($query) use ($user) {
                $query->credit();
                $query->approve();
                $query->where('user_id', $user->id);
                $query->orderBy('id', 'desc');
            })
            ->orderBy('id', 'desc')
            ->get();
    }

    public function investorContracts(User $user)
    {
        return Contract::deposit()
            ->active()
            ->where('user_id', $user->id)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function investorProfit(User $user)
    {
        $investorProfit = 0;
        foreach ($user->contractInvestors->where('status', '<>', StatusHelper::STATUS_CLOSE)->all() as $contractInvestor) {
            $investorProfit += $contractInvestor->getAmountPercentOnCurrentDate();
        }
        return $investorProfit - $user->balance_investor;
    }

    public function investorAssets()
    {
        return Asset::forInvestor()->with('coinValue')->get();
    }

    public function creditContractValue($user)
    {
        return NumberHelper::format_number($user->contracts()->credit()->approve()->sum('amount'));
    }

    public function creditContractValueUsers()
    {
        return Contract::credit()->sum('amount');
    }

    public function creditContracts($user)
    {
        return $user->contracts()->credit()->approve()->get();
    }

    public function creditContractNext($user)
    {
        $contracts = $user->contracts()->credit()->approve()->get();
        if (!$contracts)
            return null;

        $currDate = null;
        $date = null;
        $contractSelect = null;
        foreach ($contracts as $key => $contract) {
            $currDate = $contract->getNextDatePayment();
            if (!$date) {
                $contractSelect = $contract;
                $date = $currDate;
            }
            if ($currDate < $date) {
                $date = $currDate;
                $contractSelect = $contract;
            }
        }

        return $contractSelect;
    }
}
