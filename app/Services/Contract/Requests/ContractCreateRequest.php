<?php

namespace App\Services\Contract\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContractCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'type_asset_id' => 'required|integer|exists:assets,id',
            'pay_ticket' => 'required|file|mimes:jpg,jpeg,png,gif,bmp,pdf,doc,docx',
            'period' => 'required|integer',
            'rate' => 'required|numeric',
            'amount' => 'required|numeric',
            'verify_code' => 'required|integer',
            'g_verify_code' => 'nullable|string|max:10',
            'agree' => 'accepted',
        ];
    }
}
