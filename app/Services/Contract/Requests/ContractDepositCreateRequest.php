<?php

namespace App\Services\Contract\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContractDepositCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'period' => 'required|integer',
            //'pay_ticket' => 'required|file|mimes:jpg,jpeg,png,gif,bmp,pdf,doc,docx',
            'rate' => 'required|numeric',
            'amount' => 'required|numeric',
            'type_asset_id' => 'required|exists:assets,id',
            'agree' => 'accepted',
            //'verify_code' => 'required|integer',
            'g_verify_code' => 'nullable|string|max:10',
        ];
    }
}
