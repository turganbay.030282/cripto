<?php

namespace App\Services\Contract\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer',
            'asset_id' => 'nullable|integer|exists:assets,id',
            'number' => 'nullable|string|max:255',
            'period' => 'nullable|integer',
            'rate' => 'nullable|integer',
        ];
    }
}
