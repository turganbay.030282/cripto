<?php

namespace App\Services\Convert;

use App\Entity\Coinmarketcap;
use App\Entity\Handbook\Asset;
use App\Entity\Tpl\NotifyText;
use App\Entity\Transfer\Convert;
use App\Entity\User;
use App\Events\ConvertEvent;
use App\Events\TplUserEvent;
use App\Services\Convert\Requests\ConvertRequest;

class ConvertService
{
    /**
     * @param ConvertRequest $request
     * @param User $user
     * @return Convert
     */
    public function request(ConvertRequest $request, User $user): Convert
    {
        $inputs = $request->only('type_asset_id');
        $inputs['user_id'] = $user->id;

        $asset = Asset::findOrFail($request->get('type_asset_id'));
        $coinValue = $asset->coinValue;

        if ($request->has('type-sell')) {
            $inputs['type'] = Convert::TYPE_SELL;
            $inputs['amount_asset'] = $request->get('amount_asset');
            $inputs['amount'] = format_number($request->get('amount_asset') * $coinValue->price);
        } else {
            $inputs['type'] = Convert::TYPE_BUY;
            $inputs['amount'] = $request->get('amount');
            $inputs['amount_asset'] = numeric_fmt($request->get('amount') / $coinValue->price);
        }

        $convert = Convert::create($inputs);

        event(new ConvertEvent($convert));

        $this->notify($convert, NotifyText::find(NotifyText::CONVERT_SEND));
        return $convert;
    }

    /**
     * @param Convert $convert
     */
    public function close(Convert $convert): void
    {
        $convert->update(['status' => Convert::STATUS_CLOSE]);
        $this->notify($convert, NotifyText::find(NotifyText::CONVERT_CLOSED));
    }

    public function cancel(Convert $convert)
    {
        $convert->update(['status' => Convert::STATUS_CANCELED]);
        $this->notify($convert, NotifyText::find(NotifyText::CONVERT_CANCELED));
    }

    /**
     * @param Convert $convert
     */
    public function notify(Convert $convert, NotifyText $notifyText): void
    {
        $coinSymbol = Coinmarketcap::find($convert->type_asset_id)->symbol;
        $text = $notifyText->{'body:' . $convert->client->lang} . ' ' . $convert->type . '; ' . $convert->amount_asset . ' ' . $coinSymbol . '; ' . $convert->amount . ' EURO.';
        event(new TplUserEvent($convert->client, $notifyText, [], $text));
    }


}
