<?php

namespace App\Services\Convert\Repositories;

use App\Entity\Transfer\Convert;
use App\Services\Convert\Requests\FilterRequest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class ConvertRepository
{
    public function find(int $id)
    {
        return Convert::find($id);
    }

    public function search(FilterRequest $request)
    {
        $query = Convert::with('client')->sortable(['created_at' => 'desc']);

        $this->filters($query, $request);

        return $query;
    }

    private function filters(Builder $query, FilterRequest $request)
    {
        if ($request->has('id')) {
            $query->where('id', $request->get('id'));
        }
        if ($request->get('client')) {
            $query->whereHas('client', function($query) use($request) {
                $query->where('first_name', 'LIKE', '%' . $request->get('client') . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $request->get('client') . '%');
                $query->orWhere('email', 'LIKE', '%' . $request->get('client') . '%');
                $query->orWhere('phone', 'LIKE', '%' . $request->get('client') . '%');
            });
        }
        if ($request->get('amount_from')) {
            $query->where('amount', '>=', $request->get('amount_from'));
        }
        if ($request->get('amount_to')) {
            $query->where('amount', '<=', $request->get('amount_to'));
        }
        if ($request->get('from')) {
            $query->where('converts.created_at', '>=', Carbon::parse($request->get('from'))->startOfDay());
        }
        if ($request->get('to')) {
            $query->where('converts.created_at', '<=', Carbon::parse($request->get('to'))->endOfDay());
        }
    }

}
