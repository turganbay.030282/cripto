<?php

namespace App\Services\Convert\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConvertRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {

        return [
            'type_asset_id' => 'required|integer|exists:coinmarketcaps,id',
            'amount' => 'required|numeric|between:10000,2500000',
            'amount_asset' => 'required|numeric',
        ];
    }
}
