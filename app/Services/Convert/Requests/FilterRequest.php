<?php

namespace App\Services\Convert\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer',
            'client' => 'nullable|string',
            'amount_from' => 'nullable|numeric',
            'amount_to' =>  'nullable|numeric',
            'from' =>  'nullable|string',
            'to' =>  'nullable|string',
        ];
    }
}
