<?php

namespace App\Services\Faq;

use App\Entity\Content\Faq\FaqCategory;
use App\Entity\Content\Faq\FaqItem;
use App\Services\Faq\Requests\CreateCategoryRequest;
use App\Services\Faq\Requests\UpdateCategoryRequest;
use App\Services\Faq\Requests\CreateRequest;
use App\Services\Faq\Requests\UpdateRequest;

class FaqService
{

    public function newCategory(CreateCategoryRequest $request): FaqCategory
    {
        return FaqCategory::create($request->all());
    }

    public function editCategory(UpdateCategoryRequest $request, FaqCategory $category): void
    {
        $category->update($request->all());
    }

    public function new(CreateRequest $request): FaqItem
    {
        return FaqItem::create($request->all());
    }

    public function edit(UpdateRequest $request, FaqItem $faqItem): void
    {
        $faqItem->update($request->all());
    }

}
