<?php

namespace App\Services\Faq\Repositories;

use App\Entity\Content\Faq\FaqCategory;
use App\Entity\Content\Faq\FaqItem;
use App\Services\Faq\Requests\FilterRequest;
use Illuminate\Database\Eloquent\Builder;

class EloquentFaqRepository
{

    public function find(int $id)
    {
        return FaqItem::find($id);
    }

    public function findCategory(int $id)
    {
        return FaqCategory::find($id);
    }

    public function search(FilterRequest $request)
    {
        $query = FaqItem::with('category')->sortable(['id' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    public function searchCategory(FilterRequest $request)
    {
        $query = FaqCategory::sortable(['id' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    public function getCategory()
    {
        return FaqCategory::get();
    }

    private function filters(Builder $query, FilterRequest $request)
    {
        if ($request->get('id')) {
            $query->where('id', $request->get('id'));
        }
        if ($request->get('title')) {
            $query->where('title', $request->get('title'));
        }
        if ($request->get('description')) {
            $query->where('description', $request->get('description'));
        }
        if ($request->get('category')) {
            $query->where('faq_category_id', $request->get('category'));
        }
    }

}
