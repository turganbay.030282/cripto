<?php

namespace App\Services\Faq\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'is_convert' => 'required|boolean',
            'title.*' => 'required|string|max:255',
            'description.*' => 'required|string',
        ];
    }
}
