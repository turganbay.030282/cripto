<?php

namespace App\Services\Faq\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title.*' => 'required|string|max:255',
            'description.*' => 'required|string',
            'faq_category_id' => 'required|exists:faq_categories,id',
        ];
    }
}
