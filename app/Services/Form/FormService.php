<?php

namespace App\Services\Form;

use App\Entity\Form;
use App\Events\FormUserEvent;
use App\Services\Form\Requests\CreateGuestRequest;
use App\Services\Form\Requests\CreateRequest;

class FormService
{

    public function sendMessage(CreateRequest $request): Form
    {
        $form = Form::create(array_merge(
            $request->all(),
            [
                'name' => $request->user()->first_name,
                'email' => $request->user()->email,
                'user_id' => $request->user()->id,
            ]
        ));

        event(new FormUserEvent($form));

        return $form;
    }

    public function sendGuestMessage(CreateGuestRequest $request): Form
    {
        $form = Form::create($request->all());

        event(new FormUserEvent($form));

        return $form;
    }

}
