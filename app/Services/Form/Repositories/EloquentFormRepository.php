<?php

namespace App\Services\Form\Repositories;

use App\Entity\Form;
use App\Entity\Subscribe;
use App\Services\Form\Requests\FilterRequest;
use Illuminate\Database\Eloquent\Builder;

class EloquentFormRepository
{
    public function find(int $id)
    {
        return Form::find($id);
    }

    public function search(FilterRequest $request)
    {
        $query = Form::with('client')->sortable(['created_at' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    public function searchSubscribes(FilterRequest $request)
    {
        $query = Subscribe::sortable(['created_at' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    private function filters(Builder $query, FilterRequest $request)
    {
        if ($request->get('id')) {
            $query->where('id', $request->get('id'));
        }
        if ($request->get('name')) {
            $query->where('name', 'LIKE', '%' . $request->get('name') . '%');
        }
        if ($request->get('email')) {
            $query->where('email', 'LIKE', '%' . $request->get('email') . '%');
        }
        if ($request->get('phone')) {
            $query->where('phone', 'LIKE', '%' . $request->get('phone') . '%');
        }
        if ($request->get('company')) {
            $query->where('company', 'LIKE', '%' . $request->get('company') . '%');
        }
        if ($request->get('message')) {
            $query->where('message', 'LIKE', '%' . $request->get('message') . '%');
        }
    }

}
