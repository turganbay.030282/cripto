<?php

namespace App\Services\Form\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscribeCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => 'required|email'
        ];
    }
}
