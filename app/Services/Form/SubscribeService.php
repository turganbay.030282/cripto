<?php

namespace App\Services\Form;

use App\Entity\Subscribe;
use App\Services\Form\Requests\SubscribeCreateRequest;

class SubscribeService
{
    public function sendMessage(SubscribeCreateRequest $request): ?Subscribe
    {
        if (Subscribe::where('email', $request->get('email'))->exists()) {
            throw new \DomainException(trans('site.cabinet.subscribe-error'));
        }
        return Subscribe::create($request->only('email'));
    }
}
