<?php

namespace App\Services\Handbook;

use App\Entity\Handbook\Asset;
use App\Services\Handbook\Requests\AssetsCreateRequest;
use App\Services\Handbook\Requests\AssetsUpdateRequest;

class AssetService
{
    public function new(AssetsCreateRequest $request): Asset
    {
        $asset = Asset::create($request->except('wallet_img'));

        $wallet_img = $request['wallet_img'];
        if ($wallet_img) {
            $asset->addImage($wallet_img, null, 'wallet_img');
        }

        return $asset;
    }

    public function edit(AssetsUpdateRequest $request, Asset $asset): void
    {
        $asset->update($request->except('wallet_img'));

        $wallet_img = $request['wallet_img'];
        if ($wallet_img) {
            $asset->addImage($wallet_img, null, 'wallet_img');
        }
    }
}
