<?php

namespace App\Services\Handbook;

use App\Entity\Handbook\CreditLevel;
use App\Services\Handbook\Requests\CreateRequest;
use App\Services\Handbook\Requests\UpdateRequest;

class CreditLevelService
{

    public function new(CreateRequest $request): CreditLevel
    {
        list($request['range_from'], $request['range_to']) = explode('-', $request->get('range'));
        return CreditLevel::create($request->except('range'));
    }

    public function edit(UpdateRequest $request, CreditLevel $creditLevel): void
    {
        list($request['range_from'], $request['range_to']) = explode('-', $request->get('range'));
        $creditLevel->update($request->except('range'));
    }

}
