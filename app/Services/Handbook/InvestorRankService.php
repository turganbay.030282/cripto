<?php

namespace App\Services\Handbook;

use App\Entity\Handbook\InvestorRank;
use App\Services\Handbook\Requests\CreateRequest;
use App\Services\Handbook\Requests\UpdateRequest;

class InvestorRankService
{

    public function new(CreateRequest $request): InvestorRank
    {
        list($request['range_from'], $request['range_to']) = explode('-', $request->get('range'));
        return InvestorRank::create($request->except('range'));
    }

    public function edit(UpdateRequest $request, InvestorRank $investorRank): void
    {
        list($request['range_from'], $request['range_to']) = explode('-', $request->get('range'));
        $investorRank->update($request->except('range'));
    }

}
