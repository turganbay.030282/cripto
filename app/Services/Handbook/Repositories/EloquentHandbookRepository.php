<?php

namespace App\Services\Handbook\Repositories;

use App\Entity\Handbook\Asset;
use App\Entity\Handbook\CreditLevel;
use App\Entity\Handbook\InvestorRank;
use App\Entity\Handbook\TypeStatus;
use App\Entity\Handbook\TypeTransaction;
use App\Entity\Handbook\TypeTwoStatus;
use App\Services\Handbook\Requests\FilterRequest;
use App\Services\Handbook\Requests\FilterTypeTransactionRequest;
use Illuminate\Database\Eloquent\Builder;

class EloquentHandbookRepository
{

    public function searchCreditLevel(FilterRequest $request)
    {
        $query = CreditLevel::sortable(['id' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    public function searchInvestorRank(FilterRequest $request)
    {
        $query = InvestorRank::sortable(['id' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    public function searchTypeTransaction(FilterTypeTransactionRequest $request)
    {
        $query = TypeTransaction::sortable(['id' => 'desc']);
        $this->filterTypeTransactions($query, $request);
        return $query;
    }

    public function searchTypeStatus()
    {
        $query = TypeStatus::sortable(['name' => 'asc']);
        return $query;
    }

    public function searchTypeTwoStatus()
    {
        $query = TypeTwoStatus::sortable(['name' => 'asc']);
        return $query;
    }

    public function searchAsset(FilterRequest $request)
    {
        $query = Asset::with('coinValue')->sortable(['id' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    public static function listAsset()
    {
        $result = [];
        $items = Asset::with('coinValue')->get();
        foreach ($items as $item){
            $result[$item->coinValue->id] = $item->coinValue->symbol;
        }
        return $result;
    }

    public static function listAsset2()
    {
        $result = [];
        $items = Asset::with('coinValue')->get();
        foreach ($items as $item){
            $result[$item->id] = $item->coinValue->symbol;
        }
        return $result;
    }

    private function filters(Builder $query, FilterRequest $request)
    {
        if ($request->get('id')) {
            $query->where('id', $request->get('id'));
        }
        if ($request->get('name')) {
            $query->where('name', $request->get('name'));
        }
        if ($request->get('type')) {
            $query->where('type', $request->get('type'));
        }
        if ($request->get('range_from')) {
            $query->where('range_from', '>=', $request->get('range_from'));
        }
        if ($request->get('range_to')) {
            $query->where('range_to', '<=', $request->get('range_to'));
        }
    }

    private function filterTypeTransactions(Builder $query, FilterTypeTransactionRequest $request)
    {
        if ($request->get('id')) {
            $query->where('id', $request->get('id'));
        }
        if ($request->get('type')) {
            $query->where('type', $request->get('type'));
        }
        if ($request->get('value_from')) {
            $query->where('value', '>=', $request->get('value_from'));
        }
        if ($request->get('value_to')) {
            $query->where('value', '<=', $request->get('value_to'));
        }
    }
}
