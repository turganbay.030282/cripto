<?php

namespace App\Services\Handbook\Requests;

use App\Entity\Handbook\Asset;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AssetsCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $array['type'] = ['required', 'string', Rule::in(array_keys(Asset::listType()))];
        $array['calc'] = ['required', 'array'];
        $array['wallet_number'] = 'required|string|max:255';
        $array['wallet_img'] = 'nullable|image|mimes:jpg,jpeg,png,svg';

        foreach (config('translatable.locales') as $locale){
            $array[$locale.'.name'] = 'required|string|max:255';
        }
        return $array;
    }
}
