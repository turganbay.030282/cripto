<?php

namespace App\Services\Handbook\Requests;

use App\Entity\Handbook\Asset;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $array['range'] = 'required|string|regex:/^[0-9]{1,3}-[0-9]{1,3}+$/i';
        foreach (config('translatable.locales') as $locale){
            $array[$locale.'.name'] = 'required|string|max:255';
        }
        return $array;
    }
}
