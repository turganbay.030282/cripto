<?php

namespace App\Services\Handbook\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTypeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $array['name'] = 'required|string|max:255';
        return $array;
    }
}
