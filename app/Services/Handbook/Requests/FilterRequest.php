<?php

namespace App\Services\Handbook\Requests;

use App\Entity\Handbook\Asset;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer',
            'name' => 'nullable|string|max:255',
            'type' => ['nullable', 'string', Rule::in(array_keys(Asset::listType()))],
            'range_from' => 'nullable|integer',
            'range_to' => 'nullable|integer',
        ];
    }
}
