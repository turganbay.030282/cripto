<?php

namespace App\Services\Handbook\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilterTypeTransactionRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer',
            'type' => 'nullable|string|max:255',
            'value_from' => 'nullable|numeric',
            'value_to' => 'nullable|numeric',
        ];
    }
}
