<?php

namespace App\Services\Handbook\Requests;

use App\Entity\Handbook\Asset;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TypeTransactionCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $array = [
            'type' => 'required|string|max:255',
            'value' => 'required|numeric',
            'period' => 'nullable|integer',
            'min' => 'required|integer',
            'max' => 'required|integer',
            'level' => 'required|integer',
        ];
        foreach (config('translatable.locales') as $locale){
            $array[$locale.'.name'] = 'required|string|max:255';
        }
        return $array;
    }
}
