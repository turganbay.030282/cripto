<?php

namespace App\Services\Handbook;

use App\Entity\Handbook\TypeStatus;
use App\Entity\Handbook\TypeTwoStatus;
use App\Entity\Handbook\TypeTransaction;
use App\Services\Handbook\Requests\CreateTypeRequest;
use App\Services\Handbook\Requests\TypeTransactionCreateRequest;
use App\Services\Handbook\Requests\TypeTransactionUpdateRequest;

class TypeTransactionService
{

    public function new(TypeTransactionCreateRequest $request): TypeTransaction
    {
        return TypeTransaction::create($request->all());
    }

    public function edit(TypeTransactionUpdateRequest $request, TypeTransaction $typeTransaction): void
    {
        $typeTransaction->update($request->all());
    }

    public function newTypeStatus(CreateTypeRequest $request): TypeStatus
    {
        return TypeStatus::create($request->all());
    }

    public function editTypeStatus(CreateTypeRequest $request, TypeStatus $typeStatus): void
    {
        $typeStatus->update($request->all());
    }

    public function newTypeTwoStatus(CreateTypeRequest $request): TypeTwoStatus
    {
        return TypeTwoStatus::create($request->all());
    }

    public function editTypeTwoStatus(CreateTypeRequest $request, TypeTwoStatus $typeStatusTwo): void
    {
        $typeStatusTwo->update($request->all());
    }

}
