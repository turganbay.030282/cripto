<?php

namespace App\Services\Page;

use App\Entity\Content\Page;
use App\Entity\Content\PageAbout;
use App\Entity\Content\PageBonus;
use App\Entity\Content\PageInvestor;
use App\Entity\Content\PageMain;
use App\Services\Page\Requests\CreateRequest;
use App\Services\Page\Requests\PageAboutRequest;
use App\Services\Page\Requests\PageBonusRequest;
use App\Services\Page\Requests\PageInvestorRequest;
use App\Services\Page\Requests\PageMainRequest;
use App\Services\Page\Requests\UpdateRequest;

class PageService
{

    public function new(CreateRequest $request): Page
    {
        return Page::create($request->all());
    }

    public function edit(UpdateRequest $request, Page $page): void
    {
        $page->update($request->all());
    }

    public function editPageAbout(PageAboutRequest $request, PageAbout $pageAbout)
    {
        \DB::transaction(function () use ($request, $pageAbout) {
            $pageAbout->update($request->except(
                'benefit_b_icon',
                'benefit_b_icon2',
                'benefit_b_icon3',
                'benefit_b_icon4',
                'benefit_b_icon5',
                'benefit_b_icon6'
            ));

            for ($i = 1; $i <= 6; $i++) {
                $field = 'benefit_b_icon' . ($i != 1 ? $i : '');
                $benefitIcon = $request[$field];
                if (!empty($benefitIcon)) {
                    $pageAbout->addImage($benefitIcon, null, $field);
                }
            }
        });
    }

    public function editPageBonus(PageBonusRequest $request, PageBonus $pageBonus)
    {
        \DB::transaction(function () use ($request, $pageBonus) {
            $pageBonus->update($request->except(
                'photo1',
                'photo2',
                'photo3',
                'photo4'
            ));

            for ($i = 1; $i <= 4; $i++) {
                $field = 'photo' . $i;
                $benefitIcon = $request[$field];
                if (!empty($benefitIcon)) {
                    $pageBonus->addImage($benefitIcon, null, $field);
                }
            }
        });
    }

    public function editPageMain(PageMainRequest $request, PageMain $pageMain)
    {
        $pageMain->update($request->all());
    }


    public function editPageInvestor(PageInvestorRequest $request, PageInvestor $pageInvestor)
    {
        \DB::transaction(function () use ($request, $pageInvestor) {
            $pageInvestor->update($request->except(
                'about_b_icon',
                'about_b_icon2',
                'about_b_icon3'
            ));

            for ($i = 1; $i <= 3; $i++) {
                $field = 'about_b_icon' . ($i != 1 ? $i : '');
                $benefitIcon = $request[$field];
                if (!empty($benefitIcon)) {
                    $pageInvestor->addImage($benefitIcon, null, $field);
                }
            }
        });
    }

    public function restore($id)
    {
        Page::onlyTrashed()
            ->where('id', $id)
            ->restore();
    }
}
