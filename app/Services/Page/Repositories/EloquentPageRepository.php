<?php

namespace App\Services\Page\Repositories;

use App\Entity\Content\Page;
use App\Services\Page\Requests\FilterRequest;
use Illuminate\Database\Eloquent\Builder;

class EloquentPageRepository
{
    public function find(int $id)
    {
        return Page::find($id);
    }

    public function search(FilterRequest $request)
    {
        $query = Page::sortable(['created_at' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    private function filters(Builder $query, FilterRequest $request)
    {
        if ($request->get('id')) {
            $query->where('id', $request->get('id'));
        }
        if ($request->get('title')) {
            $query->where('title', $request->get('title'));
        }
        if ($request->get('description')) {
            $query->where('description', $request->get('description'));
        }
        if ($request->get('publish')) {
            $query->where('publish', $request->get('publish'));
        }
    }

}
