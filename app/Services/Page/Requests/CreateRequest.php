<?php

namespace App\Services\Page\Requests;

use App\Helpers\PublishHelper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $array = [
            'publish' => ['integer', Rule::in(array_keys(PublishHelper::statusList()))],
            'slug' => 'required|string|max:255|unique:pages',
        ];
        foreach (config('translatable.locales') as $locale) {
            $array[$locale . '.title'] = 'required|string|max:255';
            $array[$locale . '.content'] = 'required|string';
            $array[$locale . '.meta_title'] = 'nullable|string|max:255';
            $array[$locale . '.meta_desc'] = 'nullable|string|max:500';
        }
        return $array;
    }
}
