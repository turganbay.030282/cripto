<?php

namespace App\Services\Page\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageBonusRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $array = [
            'photo1' => 'nullable|image',
            'photo2' => 'nullable|image',
            'photo3' => 'nullable|image',
            'photo4' => 'nullable|image',
        ];
        foreach (config('translatable.locales') as $locale) {
            $array[$locale . '.title'] = 'required|string|max:255';
            $array[$locale . '.subtitle'] = 'required|string|max:255';
            $array[$locale . '.title1'] = 'required|string|max:255';
            $array[$locale . '.content1'] = 'required|string';
            $array[$locale . '.title2'] = 'required|string|max:255';
            $array[$locale . '.content2'] = 'required|string';
            $array[$locale . '.title3'] = 'required|string|max:255';
            $array[$locale . '.content3'] = 'required|string';
        }
        return $array;
    }
}
