<?php

namespace App\Services\Page\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageInvestorRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $array = [
            'about_b_icon' => 'nullable|image',
            'about_b_icon2' => 'nullable|image',
            'about_b_icon3' => 'nullable|image',
        ];
        foreach (config('translatable.locales') as $locale) {
            $array[$locale . '.title'] = 'required|string|max:255';
            $array[$locale . '.desc'] = 'required|string';
            $array[$locale . '.btn_text'] = 'required|string|max:255';
            $array[$locale . '.about_title'] = 'required|string|max:255';
            $array[$locale . '.about_sub_title'] = 'required|string|max:255';
            $array[$locale . '.about_b_title'] = 'required|string|max:255';
            $array[$locale . '.about_b_desc'] = 'required|string|max:255';
            $array[$locale . '.about_b_title2'] = 'required|string|max:255';
            $array[$locale . '.about_b_desc2'] = 'required|string|max:255';
            $array[$locale . '.about_b_title3'] = 'required|string|max:255';
            $array[$locale . '.about_b_desc3'] = 'required|string|max:255';
            $array[$locale . '.benefit_title'] = 'required|string|max:255';
            $array[$locale . '.benefit_b_title'] = 'required|string|max:255';
            $array[$locale . '.benefit_b_desc'] = 'required|string|max:255';
            $array[$locale . '.benefit_b_title2'] = 'required|string|max:255';
            $array[$locale . '.benefit_b_desc2'] = 'required|string|max:255';
            $array[$locale . '.benefit_b_title3'] = 'required|string|max:255';
            $array[$locale . '.benefit_b_desc3'] = 'required|string|max:255';
            $array[$locale . '.benefit_b_title4'] = 'required|string|max:255';
            $array[$locale . '.benefit_b_desc4'] = 'required|string|max:255';
            $array[$locale . '.benefit_b_title5'] = 'required|string|max:255';
            $array[$locale . '.benefit_b_desc5'] = 'required|string|max:255';
            $array[$locale . '.benefit_b_title6'] = 'required|string|max:255';
            $array[$locale . '.benefit_b_desc6'] = 'required|string|max:255';
        }
        return $array;
    }
}
