<?php

namespace App\Services\Page\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageMainRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $array = [];
        foreach (config('translatable.locales') as $locale) {
            $array[$locale . '.title'] = 'required|string|max:255';
            $array[$locale . '.sub_title'] = 'nullable|string|max:255';
            $array[$locale . '.title_inv'] = 'required|string|max:255';
            $array[$locale . '.sub_title_inv'] = 'nullable|string|max:255';
            $array[$locale . '.title_offer_tr'] = 'required|string|max:255';
            $array[$locale . '.title_offer_inv'] = 'required|string|max:255';
            $array[$locale . '.desc_offer_tr'] = 'required|string';
            $array[$locale . '.desc_offer_inv'] = 'required|string';
            $array[$locale . '.offer_tr_title'] = 'required|string|max:255';
            $array[$locale . '.offer_tr_sub_title'] = 'required|string|max:255';
            $array[$locale . '.offer_tr_desc'] = 'required|string';
            $array[$locale . '.offer_tr_btn'] = 'required|string|max:255';
            $array[$locale . '.offer_inv_title'] = 'required|string|max:255';
            $array[$locale . '.offer_inv_sub_title'] = 'required|string|max:255';
            $array[$locale . '.offer_inv_desc'] = 'required|string';
            $array[$locale . '.offer_inv_btn'] = 'required|string|max:255';
        }
        return $array;
    }
}
