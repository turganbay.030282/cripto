<?php

namespace App\Services\Page\Requests;

use App\Entity\Content\Page;

/**
 * @property Page $page
 */
class UpdateRequest extends CreateRequest
{
    public function rules(): array
    {
        $rules = parent::rules();
        $rules['slug'] = 'required|string|max:255|unique:pages,slug,' . $this->page->id;
        return $rules;
    }
}
