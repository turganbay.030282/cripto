<?php

namespace App\Services\Statistic;

use App\Entity\Statistic\StatisticActiveSession;
use App\Entity\Statistic\StatisticUser;
use App\Entity\User;
use Carbon\Carbon;

class StatisticUserService
{
    public function register(
        int $month,
        int $year,
        bool $isInvestor
    )
    {
        $statistic = StatisticUser::where('month', $month)->where('year', $year)->first();
        if (!$statistic) {
            StatisticUser::create([
                'month' => $month,
                'year' => $year,
                'register' => 1,
                'trader' => $isInvestor ? 0 : 1,
                'investor' => $isInvestor ? 1 : 0,
            ]);
        } else {
            $statistic->update([
                'register' => $statistic->register + 1,
                'trader' => $isInvestor ? $statistic->trader : $statistic->trader + 1,
                'investor' => $isInvestor ? $statistic->investor + 1 : $statistic->investor,
            ]);
        }
    }

    public function verify(
        int $month,
        int $year
    )
    {
        $statistic = StatisticUser::where('month', $month)->where('year', $year)->first();
        if (!$statistic) {
            StatisticUser::create([
                'month' => $month,
                'year' => $year,
                'verify' => 1
            ]);
        } else {
            $statistic->update([
                'verify' => $statistic->verify + 1
            ]);
        }
    }

    public function login(User $user)
    {
        $statistic = StatisticActiveSession::whereDate('date_at', Carbon::now())->first();
        if (!$statistic) {
            StatisticActiveSession::create([
                'investor' => $user->is_investor ? 1 : 0,
                'trader' => !$user->is_investor ? 1 : 0,
                'date_at' => Carbon::now()
            ]);
        } else {
            $statistic->update([
                'investor' => $user->is_investor ? $statistic->investor + 1 : 0,
                'trader' => !$user->is_investor ? $statistic->trader + 1 : 0,
            ]);
        }
    }
}
