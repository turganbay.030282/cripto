<?php

namespace App\Services\Tpl;

use App\Entity\Tpl\NotifyText;
use App\Services\Tpl\Requests\CreateNotifyTextRequest;
use App\Services\Tpl\Requests\UpdateNotifyTextRequest;

class NotifyTextService
{

    public function new(CreateNotifyTextRequest $request): NotifyText
    {
        return NotifyText::create($request->all());
    }

    public function edit(UpdateNotifyTextRequest $request, NotifyText $notifyText): void
    {
        $data = $request->all();
        $data['active'] =  $request->has('active');

        $notifyText->update($data);
    }

}
