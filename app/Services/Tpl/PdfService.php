<?php

namespace App\Services\Tpl;

use Illuminate\Http\Response;

class PdfService
{
    public function generateFromHtml($html, $filename, $path)
    {
        if (!is_file($path . $filename)) {
            $pdf = $this->createFromHtml($html);
            $pdf->save($path . $filename);
        }
    }

    public function downloadFromHtml($html, $filename): Response
    {
        $pdf = $this->createFromHtml($html);
        return $pdf->download($filename);
    }

    public function createFromHtml($html)
    {
        $html = "<html><head><style>body { font-family: DejaVu Sans }</style>" .
            "<body>" . $html . "</body>" .
            "</head></html>";


        /** @var \Barryvdh\DomPDF\PDF $pdf */
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($html);
        return $pdf;
    }
}
