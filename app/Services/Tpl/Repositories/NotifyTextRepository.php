<?php

namespace App\Services\Tpl\Repositories;

use App\Entity\Tpl\NotifyText;
use App\Services\Tpl\Requests\FilterNotifyTextRequest;
use Illuminate\Database\Eloquent\Builder;

class NotifyTextRepository
{

    public function find(int $id)
    {
        return NotifyText::find($id);
    }

    public function search(FilterNotifyTextRequest $request)
    {
        $query = NotifyText::sortable(['id' => 'desc']);

        $this->filters($query, $request);

        return $query;
    }

    private function filters(Builder $query, FilterNotifyTextRequest $request)
    {
        if ($request->get('id')) {
            $query->where('id', $request->get('id'));
        }

        if ($request->get('subject')) {
            $query->where('subject', '%' . $request->get('subject') . '%');
        }

        if ($request->get('body')) {
            $query->where('body', '%' . $request->get('body') . '%');
        }
    }

}
