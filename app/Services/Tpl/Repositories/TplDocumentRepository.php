<?php

namespace App\Services\Tpl\Repositories;

use App\Entity\Tpl\TplDocument;
use App\Services\Tpl\Requests\FilterRequest;
use Illuminate\Database\Eloquent\Builder;

class TplDocumentRepository
{
    public function find(int $id)
    {
        return TplDocument::find($id);
    }

    public function search(FilterRequest $request)
    {
        $query = TplDocument::sortable(['id' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    private function filters(Builder $query, FilterRequest $request)
    {
        if ($request->get('id')) {
            $query->where('id', $request->get('id'));
        }
        if ($request->get('title')) {
            $query->where('title', $request->get('title'));
        }
    }
}
