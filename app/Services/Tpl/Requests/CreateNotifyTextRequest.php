<?php

namespace App\Services\Tpl\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateNotifyTextRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $array = [];
        
        foreach (config('translatable.locales') as $locale) {
            $array[$locale . '.subject'] = 'required|string|max:255';
            $array[$locale . '.body'] = 'required|string';
        }

        return $array;
    }
}
