<?php

namespace App\Services\Tpl\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name*' => 'required|string|max:255',
            'content.*' => 'nullable|string',
        ];
    }
}
