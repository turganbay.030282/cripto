<?php

namespace App\Services\Tpl\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilterNotifyTextRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer',
            'subject' => 'nullable|string|max:255',
            'body' => 'nullable|string|max:255',
        ];
    }
}
