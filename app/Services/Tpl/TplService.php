<?php

namespace App\Services\Tpl;

use App\Entity\Handbook\Asset;
use App\Entity\Setting;
use App\Entity\Tpl\TplDocument;
use App\Entity\User;
use App\Helpers\NumberHelper;
use App\Services\Contract\Repositories\EloquentContractRepository;
use App\Services\Tpl\Requests\CreateRequest;
use App\Services\Tpl\Requests\UpdateRequest;
use Carbon\Carbon;

class TplService
{
    /**
     * @var EloquentContractRepository
     */
    private $repository;

    public function __construct(EloquentContractRepository $repository)
    {
        $this->repository = $repository;
    }

    public function new(CreateRequest $request): TplDocument
    {
        return TplDocument::create($request->all());
    }

    public function edit(UpdateRequest $request, TplDocument $tplDocument): void
    {
        $tplDocument->update($request->all());
    }

    public function docForTrader(
        User $user,
        $period = 1,
        $asset = '',
        $amount = 0,
        $amount_asset = 0,
        $rate = 0,
        $day_pay = '',
        $commision = 0
    )
    {
        $template = '';
        $tplDocument = TplDocument::find(1);
        if (!$tplDocument) {
            return $template;
        }

        $userVerification = $user->userVerification;
        $documentText = $tplDocument->{'content:' . app()->getLocale()};
        $month_pay = $rate ? NumberHelper::calcTraderAnnuityFirstPayment($amount, $period, $rate) : 0;
        $template = $this->replace([
            '{{$fio}}' => $userVerification && $userVerification->full_name ? $userVerification->full_name : '',
            '{{$address}}' => $userVerification && $userVerification->address ? $userVerification->address : '',
            '{{$email}}' => $user->email,
            '{{$phone}}' => $user->phone,
            '{{$period}}' => $period,
            '{{$asset}}' => $asset,
            '{{$amount}}' => $amount,
            '{{$amount_asset}}' => $amount_asset,
            '{{$day_pay}}' => $day_pay,
            '{{$commision_p}}' => NumberHelper::format_number($rate),
            '{{$commision}}' => NumberHelper::format_number($period * $month_pay - $amount),
            '{{$month_pay}}' => NumberHelper::format_number($month_pay),
            '{{$first_pay}}' => NumberHelper::format_number($month_pay),
            '{{$table}}' => $this->traderTable($period, $amount, $rate)
        ], $documentText);

        return $template;
    }

    public function docForInvestor(
        User $user,
        $assetId,
        $period = 1,
        $amount = 0,
        $rate = 0
    )
    {
        $template = '';
        $tplDocument = TplDocument::find(2);
        if (!$tplDocument) {
            return $template;
        }

        $userVerification = $user->userVerification;
        $documentText = $tplDocument->{'content:' . app()->getLocale()};
        $asset = Asset::findOrFail($assetId);

        $percent = NumberHelper::calcInvestorFirstPayment($amount, $rate);
        $amountDai = numeric_fmt($percent / $asset->coinValue->price);

        $template = $this->replace([
            '{{$fio}}' => $userVerification && $userVerification->full_name ? $userVerification->full_name : '',
            '{{$address}}' => $userVerification && $userVerification->address ? $userVerification->address : '',
            '{{$email}}' => $user->email,
            '{{$phone}}' => $user->phone,
            '{{$period}}' => $period,
            '{{$amount}}' => $amount,
            '{{symbol}}' => $asset->coinValue->symbol,
            '{{$amount_month_percent}}' => NumberHelper::format_number($rate / 12),
            '{{$amount_month_eur}}' => $amountDai . ' ' . $asset->coinValue->symbol,
            '{{$amount_dai}}' => NumberHelper::format_number_cripto($amount / $asset->coinValue->price),
            '{{$table}}' => $this->investorTable(
                $period, $amount, $percent, $asset, 0, 1, 1, 0, 0
            )
        ], $documentText);

        return $template;
    }

    public function docForInvestorDai(
        User $user,
        $assetId,
        $period = 1,
        $amount = 0,
        $rate = 0
    )
    {
        $template = '';
        $tplDocument = TplDocument::find(3);
        if (!$tplDocument) {
            return $template;
        }

        $asset = Asset::findOrFail($assetId);
        $userVerification = $user->userVerification;
        $documentText = $tplDocument->{'content:' . app()->getLocale()};

        $amountMonthEur = format_number($amount / $asset->coinValue->price);
//        $amountDai = numeric_fmt($amount / $asset->coinValue->price / $asset->coinValue->price);
        $percent = NumberHelper::calcInvestorFirstPayment($amount, $rate);

        $template = $this->replace([
            '{{$fio}}' => $userVerification && $userVerification->full_name ? $userVerification->full_name : '',
            '{{$address}}' => $userVerification && $userVerification->address ? $userVerification->address : '',
            '{{$email}}' => $user->email,
            '{{$phone}}' => $user->phone,
            '{{$period}}' => $period,
            '{{symbol}}' => $asset->coinValue->symbol,
            '{{$amount_month_percent}}' => NumberHelper::format_number($rate / 12),
            '{{$amount_month_eur}}' => NumberHelper::format_number($percent) . ' ' . $asset->coinValue->symbol,
            '{{$amount_dai}}' => $amount,
            '{{$table}}' => $this->investorTable($period, $amount, $percent, $asset, 1, 0, 0)
        ], $documentText);

        return $template;
    }

    private static function replace(array $array, string $template)
    {
        foreach ($array as $key => $value) {
            $template = str_replace($key, $value, $template);
        }
        return $template;
    }

    private function traderTable($period, $amount, $rate)
    {
        $index = 1;
        $payments = collect([]);

        for ($i = 0; $i < $period; $i++) {
            $calcTraderAmount = NumberHelper::calcTraderAnnuityFirstPayment($amount, $period, $rate);

            $item = new \stdClass();
            $item->date = Carbon::now()->addMonths($i);
            $item->main_amount = format_number($amount / $period);
            $item->balance_owed = format_number($amount - $index * $amount / $period);
            $item->percent = format_number(format_number($calcTraderAmount) - format_number($amount / $period));
            $item->amount = format_number($calcTraderAmount);

            $payments->push($item);

            $index++;
        }

        return view('parts.trader_table', compact('payments'))->render();
    }

    private function investorTable(
        $period, $amount, $percent, Asset $asset, $isMoney = 0, $visibleColumn = 1,
        $visibleColumn2 = 1, $visibleColumn3 = 1, $visibleColumn4 = 1
    )
    {
        $index = 1;
        $payments = collect([]);
        $symbol = $asset->coinValue->symbol ?? '';

        for ($i = $period; $i > 0; $i--) {
            $amountNew = $amount + $index * $percent;
            $item = new \stdClass();
            $item->percent = NumberHelper::format_number($percent);
            $item->amount = $isMoney ? numeric_fmt($amountNew) : format_number($amountNew);
            $item->percent_dai = numeric_fmt($percent / $asset->coinValue->price);
            $item->amount_dai = numeric_fmt($amountNew / $asset->coinValue->price);
            $item->date = Carbon::now()->addMonths($index)->format("d.m.Y");

            $payments->push($item);
            $index++;
        }

        return view('parts.investor_table', compact(
            'payments', 'symbol', 'visibleColumn',
            'visibleColumn2', 'visibleColumn3', 'visibleColumn4'
        ))->render();
    }

}
