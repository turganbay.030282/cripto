<?php

namespace App\Services\Transaction;

use App\Entity\Transfer\BonusPayment;
use App\Entity\User;
use App\Helpers\StatusHelper;
use App\Helpers\TypeUserHelper;
use Illuminate\Support\Facades\DB;
use App\Entity\Transfer\Transaction;
use App\Services\Transaction\Requests\BonusPayout\BonusPayoutDaiRequest;
use App\Services\Transaction\Requests\BonusPayout\BonusPayoutRequest;

class BonusPaymentService
{

    public function payout(BonusPayoutRequest $request, User $client)
    {
        if ($client->amount_bonus < $request->get('amount')) {
            throw new \DomainException(trans('site.page.cabinet.sidebar-payot-error-amount'));
        }

        DB::transaction(function () use ($request, $client) {
            BonusPayment::create([
                'user_id' => $client->id,
                'amount' => format_number($request->get('amount'))
            ]);
            $client->update([
                'payout_balance' => format_number($client->payout_balance + $request->get('amount'))
            ]);

            Transaction::create([
                'number' => $client->id,
                'user_id' => $client->id,
                'amount' => $request->get('amount'),
                'type_user' => TypeUserHelper::TYPE_USER_TRADER,
                'type_transaction' => Transaction::TYPE_BONUS,
                'type' => Transaction::TYPE_TRANSFER,
                'status' => StatusHelper::STATUS_APPROVE
            ]);
        });
    }

    public function payoutApprove(Transaction $transaction)
    {
        DB::transaction(function () use ($transaction) {
            $client = $transaction->client;

            BonusPayment::create([
                'user_id' => $client->id,
                'amount' => $transaction->amount
            ]);

            $transaction->update([
                'status' => StatusHelper::STATUS_APPROVE
            ]);
        });
    }

    public function payoutDai(BonusPayoutDaiRequest $request, User $client)
    {
        if ($client->amount_bonus < $request->get('amount')) {
            throw new \DomainException(trans('site.page.cabinet.sidebar-payot-error-amount'));
        }

        DB::transaction(function () use ($request, $client) {
            BonusPayment::create([
                'user_id' => $client->id,
                'amount' => format_number($request->get('amount'))
            ]);
            $client->update([
                'balance_investor' => format_number($client->balance_investor + $request->get('amount'))
            ]);

            Transaction::create([
                'number' => $client->id,
                'user_id' => $client->id,
                'amount' => $request->get('amount'),
                'type_user' => TypeUserHelper::TYPE_USER_TRADER,
                'type_transaction' => Transaction::TYPE_BONUS,
                'type' => Transaction::TYPE_TRANSFER,
                'status' => StatusHelper::STATUS_APPROVE
            ]);
        });
    }

}
