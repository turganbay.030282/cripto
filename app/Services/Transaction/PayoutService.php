<?php

namespace App\Services\Transaction;

use App\AppServices\Google\Google2FAAuthenticator;
use App\Entity\Handbook\Asset;
use App\Entity\Tpl\NotifyText;
use App\Entity\Transfer\Transaction;
use App\Entity\User;
use App\Events\TplUserEvent;
use App\Events\TransactionApprove;
use App\Helpers\NumberHelper;
use App\Helpers\StatusHelper;
use App\Helpers\TypeUserHelper;
use App\Services\Transaction\Requests\Payout\BuyAssetRequest;
use App\Services\Transaction\Requests\Payout\PayoutAssetRequest;
use App\Services\Transaction\Requests\Payout\TransferAssetRequest;
use App\Services\Transaction\Requests\PayoutRequest;
use App\Services\Transaction\Requests\TransferToInvestorRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PayoutService
{

    public function payout(PayoutRequest $request, User $client)
    {
        $codeVerified = $client->codeVerifies()->payout()->orderBy('id', 'desc')->first();
        if ($request->get('sms_code') !== $codeVerified->verify_token) {
            throw new \DomainException(trans('site.cabinet.Incorrect-verify-code'));
        }
        $client->codeVerifies()->payout()->delete();

        if ($client->payout_balance < $request->get('amount')) {
            throw new \DomainException(trans('site.page.cabinet.sidebar-payot-error-amount'));
        }

        DB::transaction(function () use ($request, $client) {
            Transaction::create([
                'user_id' => $client->id,
                'amount' => $request->get('amount'),
                'type_user' => TypeUserHelper::TYPE_USER_TRADER,
                'type_transaction' => Transaction::TYPE_PAYOUT,
                'wallet_to' => $request->get('wallet'),
                'type' => Transaction::TYPE_OUT,
                'status' => StatusHelper::STATUS_NEW
            ]);
        });

        event(new TransactionApprove($client));
    }

    public function transferToInvestor(TransferToInvestorRequest $request, User $client)
    {
        if ($client->payout_balance < $request->get('amount')) {
            throw new \DomainException(trans('site.page.cabinet.sidebar-payot-error-amount'));
        }

        $amount = format_number($request->get('amount'));
        $client->update([
            'balance_investor' => $client->balance_investor + $amount,
            'payout_balance' => $client->payout_balance - $amount,
        ]);
    }

    public function payoutAsset(PayoutAssetRequest $request, User $client)
    {
        $codeVerified = $client->codeVerifies()->payout()->orderBy('id', 'desc')->first();
        if ($request->get('sms_code') !== $codeVerified->verify_token) {
            throw new \DomainException(trans('site.cabinet.Incorrect-verify-code'));
        }
        $client->codeVerifies()->payout()->delete();

        if (!$this->reLogin2FA($request)) {
            throw new \DomainException(trans('site.cabinet.Incorrect-2FA-code'));
        }

        $assets = $client->contractFreeAssets()
            ->with('coinmarketcap')
            ->whereHas('coinmarketcap', function ($q) use ($request) {
                $q->where('coinmarketcaps.id', $request->get('coin'));
            })
            ->get();

        $amountAsset = $assets->sum('amount_asset');
        if ($amountAsset < $request->get('coin_value')) {
            throw new \DomainException(trans('site.page.cabinet.sidebar-payot-error-amount'));
        }

        $transaction = Transaction::create([
            'wallet_to' => $request->get('wallet_to'),
            'coinmarketcap_id' => $request->get('coin'),
            'user_id' => $client->id,
            'amount' => $request->get('amount_tmp'),
            'amount_dai' => $request->get('coin_value'),
            'type_user' => TypeUserHelper::TYPE_USER_TRADER,
            'type_transaction' => Transaction::TYPE_TRADER_ACTIVE_EURO,
            'type' => Transaction::TYPE_REQUEST,
            'status' => StatusHelper::STATUS_NEW
        ]);

        //'trader_active_euro' Актив в Евро
        $notifyText = NotifyText::find(Transaction::NOTY_TRADER_ACTIVE_EURO);
        $text = $notifyText->{'body:' . $transaction->client->lang} . ' ' . $transaction->coinmarketcap->symbol . ' ' . $transaction->amount_dai.', Euro '.$transaction->amount;
        event(new TplUserEvent($transaction->client, $notifyText, [], $text));
        event(new TransactionApprove($client));
    }

    public function transferAsset(TransferAssetRequest $request, User $client)
    {
        $codeVerified = $client->codeVerifies()->payout()->orderBy('id', 'desc')->first();
        if ($request->get('sms_code') !== $codeVerified->verify_token) {
            throw new \DomainException(trans('site.cabinet.Incorrect-verify-code'));
        }
        $client->codeVerifies()->payout()->delete();

        if (!$this->reLogin2FA($request)) {
            throw new \DomainException(trans('site.cabinet.Incorrect-2FA-code'));
        }

        $assets = $client->contractFreeAssets()
            ->with('coinmarketcap')
            ->whereHas('coinmarketcap', function ($q) use ($request) {
                $q->where('coinmarketcaps.id', $request->get('coin'));
            })
            ->get();

        $amountAsset = $assets->sum('amount_asset');
        if ($amountAsset < $request->get('coin_value')) {
            throw new \DomainException(trans('site.page.cabinet.sidebar-payot-error-amount'));
        }

        $transaction = Transaction::create([
            'user_id' => $client->id,
            'coinmarketcap_id' => $request->get('coin'),
            'amount' => $request->get('coin_value'),
            'amount_dai' => $request->get('coin_value'),
            'type_user' => TypeUserHelper::TYPE_USER_TRADER,
            'type_transaction' => Transaction::TYPE_TRANSFER_ASSET,
            'type' => Transaction::TYPE_OUT,
            'status' => StatusHelper::STATUS_NEW,
            'wallet' => $request->get('wallet')
        ]);

        //'transfer_asset' Цифра в кошелек
        $notifyText = NotifyText::find(Transaction::NOTY_TRANSFER_ASSET);
        $text = $notifyText->{'body:' . $transaction->client->lang} . ' ' . $transaction->coinmarketcap->symbol . ' ' . $transaction->amount_dai;
        event(new TplUserEvent($transaction->client, $notifyText, [], $text));
        event(new TransactionApprove($client));
    }

    public function buyAsset(BuyAssetRequest $request, User $client)
    {
        if ($client->payout_balance < $request->get('amount')) {
            throw new \DomainException(trans('site.page.cabinet.sidebar-payot-error-amount'));
        }

        $asset = Asset::where('coinmarketcap_id', $request->get('coin'))->firstOrFail();
        Transaction::create([
            'coinmarketcap_id' => $request->get('coin'),
            'user_id' => $client->id,
            'amount' => $request->get('amount'),
            'amount_dai' => NumberHelper::format_number_cripto($request->get('amount') / $asset->coinValue->price),
            'type_user' => TypeUserHelper::TYPE_USER_TRADER,
            'type_transaction' => Transaction::TYPE_TRADER_PAY_IN,
            'type' => Transaction::TYPE_REQUEST,
            'status' => StatusHelper::STATUS_NEW
        ]);
    }

    private function reLogin2FA(Request $request)
    {
        (new Google2FAAuthenticator(request()))->logout();
        $authenticator = app(Google2FAAuthenticator::class)->boot($request);
        return $authenticator->isAuthenticated();
    }

}
