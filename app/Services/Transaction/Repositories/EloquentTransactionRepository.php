<?php

namespace App\Services\Transaction\Repositories;

use App\Entity\Transfer\Transaction;
use App\Services\Transaction\Requests\FilterRequest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class EloquentTransactionRepository
{
    public function find(int $id)
    {
        return Transaction::find($id);
    }

    public function search(FilterRequest $request)
    {
        $query = Transaction::with('client.balances', 'depositPayment', 'creditPayment.contract')->sortable(['created_at' => 'desc']);
        $this->filters($query, $request);
        return $query;
    }

    private function filters(Builder $query, FilterRequest $request)
    {
        if ($request->has('id')) {
            $query->where('id', $request->get('id'));
        }
        if ($request->get('client')) {
            $query->whereHas('client', function($query) use($request) {
                $query->where('first_name', 'LIKE', '%' . $request->get('client') . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $request->get('client') . '%');
                $query->orWhere('email', 'LIKE', '%' . $request->get('client') . '%');
                $query->orWhere('phone', 'LIKE', '%' . $request->get('client') . '%');
            });
        }
        if ($request->get('amount_from')) {
            $query->where('amount', '>=', $request->get('amount_from'));
        }
        if ($request->get('amount_to')) {
            $query->where('amount', '<=', $request->get('amount_to'));
        }
        if ($request->get('type_user')) {
            $query->where('type_user', $request->get('type_user'));
        }
        if ($request->get('type_tr')) {
            $query->where('type_transaction', $request->get('type_tr'));
        }
        if ($request->get('from')) {
            $query->where('created_at', '>=', Carbon::parse($request->get('from'))->startOfDay());
        }
        if ($request->get('to')) {
            $query->where('created_at', '<=', Carbon::parse($request->get('to'))->endOfDay());
        }
    }

}
