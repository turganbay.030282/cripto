<?php

namespace App\Services\Transaction\Requests\BonusPayout;

use Illuminate\Foundation\Http\FormRequest;

class BonusPayoutRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'amount' => 'required|numeric|min:0.01',
            //'wallet' => 'required|string',
        ];
    }
}
