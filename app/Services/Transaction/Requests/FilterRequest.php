<?php

namespace App\Services\Transaction\Requests;

use App\Entity\Transfer\Transaction;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer',
            'client' => 'nullable|string',
            'amount_from' => 'nullable|numeric',
            'amount_to' =>  'nullable|numeric',
            'type_user' => ['nullable', 'string', 'max:20', Rule::in(array_keys(Transaction::typeUserList()))],
            'type_tr' => ['nullable', 'string', 'max:20', Rule::in(array_keys(Transaction::typeList()))],
            'from' =>  'nullable|string',
            'to' =>  'nullable|string',
        ];
    }
}
