<?php

namespace App\Services\Transaction\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PayDepositRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'amount' => 'required|numeric',
            'pay_ticket' => 'required|file|mimes:jpg,jpeg,png,gif,bmp,pdf,doc,docx',
            'verify_code' => 'required|integer',
        ];
    }
}
