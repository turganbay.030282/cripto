<?php

namespace App\Services\Transaction\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PayInDaiRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'from' => 'required|string',
            'to' => 'required|string',
            'amount' => 'required|numeric',
        ];
    }
}
