<?php

namespace App\Services\Transaction\Requests\Payout;

use Illuminate\Foundation\Http\FormRequest;

class BuyAssetRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'coin' => 'required|integer|exists:coinmarketcaps,id',
            'amount' => 'required|numeric',
        ];
    }
}
