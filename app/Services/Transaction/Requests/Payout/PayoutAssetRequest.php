<?php

namespace App\Services\Transaction\Requests\Payout;

use Illuminate\Foundation\Http\FormRequest;

class PayoutAssetRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'coin' => 'required|integer|exists:coinmarketcaps,id',
            'coin_value' => 'required|numeric',
            'sms_code' => 'required|integer',
//            'wallet_to' => 'required',
        ];
    }
}
