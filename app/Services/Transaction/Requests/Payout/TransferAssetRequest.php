<?php

namespace App\Services\Transaction\Requests\Payout;

use Illuminate\Foundation\Http\FormRequest;

class TransferAssetRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'coin' => 'required|integer|exists:coinmarketcaps,id',
            'coin_value' => 'required|numeric',
            'wallet' => 'required|string',
            'sms_code' => 'required|integer',
        ];
    }
}
