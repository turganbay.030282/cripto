<?php

namespace App\Services\Transaction\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PayoutDaiRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'amount' => 'required|numeric',
            'wallet' => 'required|string',
            'sms_code' => 'required|integer',
            'type_asset_id' => 'required|exists:assets,id',
        ];
    }
}
