<?php

namespace App\Services\Transaction\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransferToInvestorRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'amount' => 'required|numeric'
        ];
    }
}
