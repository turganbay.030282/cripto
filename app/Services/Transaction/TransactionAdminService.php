<?php

namespace App\Services\Transaction;

use App\Entity\Handbook\Asset;
use App\Entity\Handbook\TypeTransaction;
use App\Entity\Setting;
use App\Entity\Tpl\NotifyText;
use App\Entity\Transfer\Transaction;
use App\Events\TplUserEvent;
use App\Events\TransactionApprove;
use App\Excel\Export\ContractExport;
use App\Helpers\StatusHelper;
use App\Services\Contract\Requests\ContractPaymentRequest;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class TransactionAdminService
{

    public function approveTransaction(Transaction $transaction): void
    {
        DB::transaction(function () use ($transaction) {
            $transaction->update(['status' => StatusHelper::STATUS_APPROVE]);
            $client = $transaction->client;

            if ($transaction->isPayout()) {//Трейдер вывод в кошелек
                $client->update([
                    'payout_balance' => format_number($client->payout_balance - $transaction->amount)
                ]);
                event(new TplUserEvent(
                    $client,
                    NotifyText::find(13)
                ));
            }

            if ($transaction->type_transaction == Transaction::TYPE_CREDIT) {//Договора трейдера заключен
                $creditPayment = $transaction->creditPayment;
                $creditPayment->update(['status' => StatusHelper::STATUS_APPROVE]);

                $contract = $creditPayment->contract;

                if ($transaction->is_redeem) {

                    foreach ($contract->payments as $payment) {
                        if ($payment->balance_owed >= $transaction->amount) {
                            continue;
                        }
                        $payment->update([
                            'main_amount' => 0,
                            'balance_owed' => 0,
                            'amount' => 0
                        ]);
                    }
                    $contract->update(['status' => StatusHelper::STATUS_CLOSE]);
                    $client->contractFreeAssets()->create([
                        'contract_id' => $contract->id,
                        'type_asset_id' => $contract->type_asset_id,
                        'amount_asset' => $contract->amount_asset,
                    ]);

                    event(new TplUserEvent(
                        $client,
                        NotifyText::find(15)
                    ));
                } else {
                    if ($contract->isNew()) {
                        $contract->update(['status' => StatusHelper::STATUS_APPROVE]);
                        $client->update([
                            'balance_trader' => $client->balance_trader - $contract->amount
                        ]);

                        try {
                            $filename = 'contract_' . date('Ymd') . '.xlsx';
                            Excel::store(new ContractExport($contract), 'contracts/' . $filename);
                            $file = storage_path() . '/app/public/contracts/' . $filename;
                            $notifyText = NotifyText::find(Transaction::NOTY_TRADER_CONTRACT_START);
                            $text = $notifyText->{'body:' . $transaction->client->lang} . ' №' . $contract->number;
                            event(new TplUserEvent($transaction->client, $notifyText, [['file'=> $file, 'options'=> []]], $text));
//                            unlink(storage_path() . '/app/public/contracts/' . $filename);
                        } catch (\Exception $exception) {
                            event(new TplUserEvent(
                                $client,
                                NotifyText::find(Transaction::NOTY_TRADER_CONTRACT_START)
                            ));
                            \Log::error('Contract mail', [$exception]);
                        }
                    }

                    if ($contract->countLastPayment() === 0) {//последний платеж
                        $contract->update(['status' => StatusHelper::STATUS_CLOSE]);

                        $client->contractFreeAssets()->create([
                            'contract_id' => $contract->id,
                            'type_asset_id' => $contract->type_asset_id,
                            'amount_asset' => $contract->amount_asset,
                        ]);
                        $filename = 'contract_' . date('Ymd') . '.xlsx';
                        Excel::store(new ContractExport($contract), 'contracts/' . $filename);
                        $file = storage_path() . '/app/public/contracts/' . $filename;

                        $notifyText = NotifyText::find(Transaction::NOTY_TRADER_CONTRACT_FINISHED);
                        $text = $notifyText->{'body:' . $transaction->client->lang} . ' №' . $contract->number;
                        event(new TplUserEvent($transaction->client, $notifyText, [['file'=> $file, 'options'=> []]], $text));
                    }
                }
            }

            if ($transaction->type_transaction == Transaction::TYPE_DEPOSIT) {
                $inputs['balance_investor'] = $client->balance_investor - $transaction->amount;
                $inputs['free_active'] = $client->free_active + $transaction->amount_dai;
                if (!$client->investor_rank) {
                    $typeTransaction = TypeTransaction::investor()
                        ->where('min', '<=', $transaction->amount)
                        ->where('max', '>=', $transaction->amount)
                        ->firstOrFail();
                    if ($typeTransaction && $typeTransaction->level) {
                        $inputs['investor_rank'] = $typeTransaction->level;
                    }
                }
                $client->update($inputs);

                $transaction->depositPayment->update(['status' => StatusHelper::STATUS_APPROVE]);
            }
        });
    }

    public function approveTraderOutTransaction(ContractPaymentRequest $request, Transaction $transaction): void
    {
        DB::transaction(function () use ($transaction, $request) {
            $client = $transaction->client;

            $transaction->update(['status' => StatusHelper::STATUS_APPROVE]);

            if ($transaction->isTransferAsset()) {
                $assets = $client->contractFreeAssets()
                    ->with('coinmarketcap')
                    ->whereHas('coinmarketcap', function ($q) use ($request, $transaction) {
                        $q->where('coinmarketcaps.id', $transaction->coinmarketcap_id);
                    })
                    ->get();

                $amountAsset = $assets->sum('amount_asset');
                $total = $transaction->amount_dai;

                if ($amountAsset < $total) {
                    throw new \DomainException(trans('site.page.cabinet.sidebar-payot-error-amount'));
                }

                $transaction->addImage($request['pay_ticket'], null, 'pay_ticket');

                foreach ($assets as $asset) {
                    if ($total > 0) {
                        if ($asset->amount_asset >= $total) {
                            $asset->update(['amount_asset' => $asset->amount_asset - $total]);
                            $total = 0;
                        } else {
                            $total -= $asset->amount_asset;
                            $asset->update(['amount_asset' => 0]);
                        }
                    }
                }

            } elseif ($transaction->isTraderActiveEuro()) {
                $assets = $client->contractFreeAssets()
                    ->with('coinmarketcap')
                    ->whereHas('coinmarketcap', function ($q) use ($transaction) {
                        $q->where('coinmarketcaps.id', $transaction->coinmarketcap_id);
                    })
                    ->get();

                $amountAsset = $assets->sum('amount_asset');
                if ($amountAsset < $transaction->amount_dai) {
                    throw new \DomainException(trans('site.page.cabinet.sidebar-payot-error-amount'));
                }

                $client->update([
                    'payout_balance' => format_number($client->payout_balance + $transaction->amount)
                ]);

                $total = $transaction->amount_dai;
                foreach ($assets as $asset) {
                    if ($total > 0) {
                        if ($asset->amount_asset >= $total) {
                            $asset->update(['amount_asset' => $asset->amount_asset - $total]);
                            $total = 0;
                        } else {
                            $total -= $asset->amount_asset;
                            $asset->update(['amount_asset' => 0]);
                        }
                    }
                }
            } else {
                $creditPayment = $transaction->creditPayment;
                $contract = $creditPayment->contract;

                $contract->update(['status' => StatusHelper::STATUS_CLOSE]);

                $client->update([
                    'payout_balance' => format_number($client->payout_balance + $transaction->amount),
                ]);

                foreach ($contract->payments as $payment) {
                    if ($payment->isApprove()) {
                        continue;
                    }
                    $payment->update([
                        'main_amount' => 0,
                        'balance_owed' => 0,
                        'amount' => 0
                    ]);
                }
            }
        });
    }

    public function approveTraderSellTransaction(Transaction $transaction): void
    {
        DB::transaction(function () use ($transaction) {
            $client = $transaction->client;

            $transaction->update(['status' => StatusHelper::STATUS_APPROVE]);

            if ($transaction->is_sell) {
                $creditPayment = $transaction->creditPayment;
                $contract = $creditPayment->contract;

                $penaltySum = 0;
                foreach ($contract->payments as $payment) {
                    $penaltySum += $payment->penalty;
                    if ($payment->isApprove()) {
                        continue;
                    }
                    $payment->update([
                        'main_amount' => 0,
                        'balance_owed' => 0,
                        'amount' => 0
                    ]);
                }

                $contract->update(['status' => StatusHelper::STATUS_CLOSE]);
                $client->update([
                    'payout_balance' => format_number($client->payout_balance + ($transaction->amount - $penaltySum))
                ]);

                if ($transaction->isTraderContractDalaysSell()){
                    $notifyText = NotifyText::find(Transaction::NOTY_TRADER_CONTRACT_DELAYS_SELL);
                } else {
                    $notifyText = NotifyText::find(Transaction::NOTY_TRADER_PREMATURE_SALE_APPROVAL);
                }
                $text = $notifyText->{'body:' . $transaction->client->lang} . ' №' . $contract->number;
                event(new TplUserEvent($transaction->client, $notifyText, [], $text));
            }
        });
    }

    public function approveTraderTransaction(Transaction $transaction): void
    {
        DB::transaction(function () use ($transaction) {
            $client = $transaction->client;

            $transaction->update(['status' => StatusHelper::STATUS_APPROVE]);

            if ($transaction->isTraderActiveEuro()) { // трейдер вывод средств из активов с коммисией
                $assets = $client->contractFreeAssets()
                    ->with('coinmarketcap')
                    ->whereHas('coinmarketcap', function ($q) use ($transaction) {
                        $q->where('coinmarketcaps.id', $transaction->coinmarketcap_id);
                    })
                    ->get();

                $amountAsset = $assets->sum('amount_asset');
                if ($amountAsset < $transaction->amount_dai) {
                    throw new \DomainException(trans('site.page.cabinet.sidebar-payot-error-amount'));
                }

                $setting = Setting::first();

                $client->update([
                    'payout_balance' => format_number($client->payout_balance + $transaction->amount - $transaction->amount * $setting->conversion_fee / 100)
                ]);

                $total = $transaction->amount_dai;
                foreach ($assets as $asset) {
                    if ($total > 0) {
                        if ($asset->amount_asset >= $total) {
                            $asset->update(['amount_asset' => $asset->amount_asset - $total]);
                            $total = 0;
                        } else {
                            $total -= $asset->amount_asset;
                            $asset->update(['amount_asset' => 0]);
                        }
                    }
                }
                //'trader_active_euro' Актив в Евро
                $notifyText = NotifyText::find(Transaction::NOTY_TRADER_WITHDRAWAL_EURO);
                $text = $notifyText->{'body:' . $client->lang} . ' ' . $transaction->coinmarketcap->symbol . ' ' . $transaction->amount_dai.', Euro '.$transaction->amount;
                event(new TplUserEvent($client, $notifyText, [], $text));
                event(new TransactionApprove($client));

            } elseif ($transaction->isTraderPayIn()) {
                if ($client->payout_balance < $transaction->amount) {
                    throw new \DomainException(trans('site.page.cabinet.sidebar-payot-error-amount'));
                }

                $asset = Asset::where('coinmarketcap_id', $transaction->coinmarketcap_id)->firstOrFail();
                $client->update([
                    'payout_balance' => ($client->payout_balance - $transaction->amount)
                ]);
                $client->contractFreeAssets()->create([
                    'type_asset_id' => $asset->id,
                    'amount_asset' => $transaction->amount_dai
                ]);
            } else {
                $creditPayment = $transaction->creditPayment;
                $contract = $creditPayment->contract;

                $contract->update(['status' => StatusHelper::STATUS_CLOSE]);

                $client->update([
                    'payout_balance' => format_number($client->payout_balance + $transaction->amount)
                ]);

                foreach ($contract->payments as $payment) {
                    if ($payment->isApprove()) {
                        continue;
                    }
                    $payment->update([
                        'main_amount' => 0,
                        'balance_owed' => 0,
                        'amount' => 0
                    ]);
                }
            }
        });
    }

    public function approveInvOutTransaction(ContractPaymentRequest $request, Transaction $transaction): void
    {
        DB::transaction(function () use ($request, $transaction) {
            $transaction->update(['status' => StatusHelper::STATUS_APPROVE]);
            $client = $transaction->client;
            $asset = Asset::where('coinmarketcap_id', $transaction->coinmarketcap_id)->first();
            $balance = $client->balances()->where('asset_id', $asset->id)->firstOrFail();
            $transaction->addImage($request['pay_ticket'], null, 'pay_ticket');

            if ($transaction->isStakingDai()) {
                $balance->update([
                    'amount' => numeric_fmt($balance->amount - $transaction->amount)
                ]);

                event(new TplUserEvent(
                    $client,
                    NotifyText::find(20)
                ));
            } elseif ($transaction->isStakingEuro()) {
                $balance->update([
                    'amount' => numeric_fmt($balance->amount - $transaction->amount_dai)
                ]);

                event(new TplUserEvent(
                    $client,
                    NotifyText::find(19)
                ));
            }
        });
    }

    public function approveStakingTransaction(Transaction $transaction): void
    {
        DB::transaction(function () use ($transaction) {
            $transaction->update(['status' => StatusHelper::STATUS_APPROVE]);
            $client = $transaction->client;
            $asset = Asset::where('coinmarketcap_id', $transaction->coinmarketcap_id)->first();
            $balance = $client->balances()->where('asset_id', $asset->id)->firstOrFail();

            if ($transaction->isStakingDai()) {
                $balance->update([
                    'amount' => numeric_fmt($balance->amount - $transaction->amount)
                ]);

                event(new TplUserEvent(
                    $client,
                    NotifyText::find(27)
                ));
            } elseif ($transaction->isStakingEuro()) {
                $balance->update([
                    'amount' => numeric_fmt($balance->amount - $transaction->amount_dai)
                ]);

                event(new TplUserEvent(
                    $client,
                    NotifyText::find(19)
                ));
            }

            event(new TransactionApprove($client));
        });
    }

    /*Розрыв договора инвестора*/
    public function approveInvTerminateContractTransaction(Transaction $transaction): void
    {
        DB::transaction(function () use ($transaction) {
            $transaction->update(['status' => StatusHelper::STATUS_APPROVE]);
            $client = $transaction->client;

            $contract = $transaction->depositPayment;
            $contract->update(['status' => StatusHelper::STATUS_CLOSE]);

            $balance = $client->balances->where('asset_id', $contract->type_asset_id)->first();

            $balance->update([
                'amount' => numeric_fmt($balance->amount + $transaction->amount_dai)
            ]);

            $investProfits = $contract->investProfits;
            if ($investProfits->isNotEmpty()) {
                foreach ($investProfits as $investProfit) {
                    if ($transaction->amount >= $investProfit->amount) {
                        continue;
                    }
                    $investProfit->update([
                        'percent' => 0,
                        'amount' => 0
                    ]);
                }
            }

            event(new TplUserEvent(
                $client,
                NotifyText::find(22)
            ));
        });
    }

    public function approveInvDepositTransaction(Transaction $transaction): void
    {
        DB::transaction(function () use ($transaction) {
            $transaction->update(['status' => StatusHelper::STATUS_APPROVE]);
            $client = $transaction->client;

            if ($transaction->isContribution()) {
                $client->update(['balance_investor' => format_number($client->balance_investor + $transaction->amount)]);
                event(new TplUserEvent(
                    $client,
                    NotifyText::find(17)
                ));
            } else {
                $contract = $transaction->depositPayment;
                $contract->update(['status' => StatusHelper::STATUS_CLOSE]);

                $client->update(['balance_investor' => format_number($client->balance_investor - $contract->amount)]);

                $investProfits = $contract->investProfits;
                if ($investProfits->isNotEmpty()) {
                    foreach ($investProfits as $investProfit) {
                        if ($transaction->amount >= $investProfit->amount) {
                            continue;
                        }
                        $investProfit->update([
                            'percent' => 0,
                            'amount' => 0
                        ]);
                    }
                }
            }
        });
    }

    public function approveInvOutPayTransaction(Transaction $transaction): void
    {
        DB::transaction(function () use ($transaction) {
            $client = $transaction->client;

            $transaction->update(['status' => StatusHelper::STATUS_APPROVE]);
            $depositPayment = $transaction->depositPayment;
            $depositPayment->update(['status' => StatusHelper::STATUS_APPROVE]);

            if ($transaction->isDeposit()) {//заключение договора инвестора
                $client->update([
                    'balance_investor' => format_number($client->balance_investor - $transaction->amount)
                ]);
            }

            if ($transaction->isDepositDai()) {//заключение договора инвестора в DAI
                $balance = $client->balances->where('asset_id', $depositPayment->type_asset_id)->first();

                $balance->update([
                    'amount' => numeric_fmt($balance->amount - $transaction->amount_dai)
                ]);
            }
        });

        try {
            $contract = $transaction->depositPayment;
            $filename = 'contract_' . date('Ymd') . '.xlsx';
            Excel::store(new ContractExport($contract), 'contracts/' . $filename);
            $file = storage_path() . '/app/public/contracts/' . $filename;
            $notifyText = NotifyText::find(Transaction::NOTY_STAKING_CONTRACT_START);
            $text = $notifyText->{'body:' . $transaction->client->lang} . ' №' . $contract->number;
            event(new TplUserEvent($transaction->client, $notifyText, [['file'=> $file, 'options'=> []]], $text));

//            unlink(storage_path() . '/app/public/contracts/' . $filename);
        } catch (\Exception $exception) {
            \Log::error('Contract mail', [$exception]);
        }
    }

    public function approveInvPayInDaiTransaction(Transaction $transaction): void
    {
        DB::transaction(function () use ($transaction) {
            $transaction->update(['status' => StatusHelper::STATUS_APPROVE]);
            $client = $transaction->client;

            $client->moneyToBalance($transaction->coinmarketcap_id, $transaction->amount);

//            $client->update([
//                'free_active' => format_number($client->free_active + $transaction->amount)
//            ]);

            event(new TplUserEvent(
                $client,
                NotifyText::find(18)
            ));
        });
    }
}
