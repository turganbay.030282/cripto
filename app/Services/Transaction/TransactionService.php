<?php

namespace App\Services\Transaction;

use App\AppServices\Google\Google2FAAuthenticator;
use App\AppServices\Sms\SmsSender;
use App\Entity\CodeVerifiedField;
use App\Entity\Handbook\Asset;
use App\Entity\Setting;
use App\Entity\Tpl\NotifyText;
use App\Entity\Transfer\Transaction;
use App\Entity\User;
use App\Events\TplUserEvent;
use App\Helpers\StatusHelper;
use App\Helpers\TypeUserHelper;
use App\Services\Transaction\Requests\PayDepositRequest;
use App\Services\Transaction\Requests\PayInDaiRequest;
use App\Services\Transaction\Requests\PayoutDaiRequest;
use App\Services\Transaction\Requests\PayoutRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionService
{
    /**
     * @var SmsSender
     */
    private $sms;

    public function __construct(SmsSender $sms)
    {
        $this->sms = $sms;
    }

    public function createDeposit(PayDepositRequest $request, User $user): Transaction
    {
        $codeVerified = $user->codeVerifies()->investorDepositPay()->orderBy('id', 'desc')->first();
        if ($request->get('verify_code') !== $codeVerified->verify_token) {
            throw new \DomainException(trans('site.cabinet.Incorrect-verify-code'));
        }

        return DB::transaction(function () use ($request, $user) {
            $setting = Setting::first();
            $transaction = Transaction::create([
                'number' => $user->deposit_euro_number,
                'user_id' => $user->id,
                'amount' => format_number($request->get('amount')),
                'amount_dai' => format_number($request->get('amount')) / $setting->course_investor,
                'type_user' => TypeUserHelper::TYPE_USER_INVESTOR,
                'type_transaction' => Transaction::TYPE_CONTRIBUTION,
                'type' => Transaction::TYPE_IN,
                'status' => StatusHelper::STATUS_NEW
            ]);
            $transaction->addImage($request['pay_ticket'], null, 'pay_ticket');
            return $transaction;
        });
    }

    public function createPayOutEuro(PayoutRequest $request, User $user): Transaction
    {
        if (!$this->reLogin2FA($request)) {
            throw new \DomainException(trans('site.cabinet.Incorrect-2FA-code'));
        }
        $asset = Asset::findOrFail($request->get('type_asset_id'));
        $balance = $user->balances->where('asset_id', $request->get('type_asset_id'))->first();
//        $amount_dai = numeric_fmt($request->get('amount') / $asset->coinValue->price);
//
//        if (!$balance || $amount_dai > $balance->amount) {
//            throw new \DomainException(trans('site.cabinet.Incorrect-payout-amount'));
//        }

        $transaction = Transaction::create([
            'user_id' => $user->id,
            'amount' => format_number($balance->amount * $asset->coinValue->price),
            'coinmarketcap_id' => $asset->coinmarketcap_id,
            'amount_dai' => $request->get('amount_dai'),
            'type_user' => TypeUserHelper::TYPE_USER_INVESTOR,
            'type_transaction' => Transaction::TYPE_STAKING_EURO,
            'type' => Transaction::TYPE_OUT,
            'status' => StatusHelper::STATUS_NEW,
            'wallet_to' => $request->get('wallet')
        ]);

        //staking_euro' Стейкинг в EURO
        $notifyText = NotifyText::find(Transaction::NOTY_STAKING_EURO);
        $text = $notifyText->{'body:' . $transaction->client->lang} . ' ' . $transaction->coinmarketcap->symbol . ' ' . $transaction->amount_dai;
        event(new TplUserEvent($transaction->client, $notifyText, [], $text));
        return $transaction;
    }

    public function createPayOutDai(PayoutDaiRequest $request, User $user): Transaction
    {
        if (!$this->reLogin2FA($request)) {
            throw new \DomainException(trans('site.cabinet.Incorrect-2FA-code'));
        }

        $codeVerified = $user->codeVerifies()->investorDepositPayOutDai()->orderBy('id', 'desc')->first();
        if ($request->get('sms_code') !== $codeVerified->verify_token) {
            throw new \DomainException(trans('site.cabinet.Incorrect-verify-code'));
        }

        $user->codeVerifies()->investorDepositPayOutDai()->delete();

        $asset = Asset::findOrFail($request->get('type_asset_id'));
        $balance = $user->balances->where('asset_id', $request->get('type_asset_id'))->first();

        if ($request->get('amount') > $balance->amount) {
            throw new \DomainException(trans('site.cabinet.Incorrect-payout-amount'));
        }

        $transaction = Transaction::create([
            'user_id' => $user->id,
            'coinmarketcap_id' => $asset->coinmarketcap_id,
            'amount' => numeric_fmt($request->get('amount')),
            'wallet' => $request->get('wallet'),
            'type_user' => TypeUserHelper::TYPE_USER_INVESTOR,
            'type_transaction' => Transaction::TYPE_STAKING_DAI,
            'type' => Transaction::TYPE_OUT,
            'status' => StatusHelper::STATUS_NEW
        ]);

        // 'staking_dai' Стейкинг в DAI
        $notifyText = NotifyText::find(Transaction::NOTY_STAKING_DAI);
        $text = $notifyText->{'body:' . $transaction->client->lang} . ' ' . $transaction->coinmarketcap->symbol . ' ' . $transaction->amount;
        event(new TplUserEvent($transaction->client, $notifyText, [], $text));
        return $transaction;
    }

    public function createPayInDai(PayInDaiRequest $request, User $user): Transaction
    {
        return DB::transaction(function () use ($request, $user) {
            $asset = Asset::findOrFail($request->get('type_asset_id'));

            $transaction = Transaction::create([
                'user_id' => $user->id,
                'coinmarketcap_id' => $asset->coinmarketcap_id,
                'amount' => format_number($request->get('amount')),
                'wallet' => $request->get('from'),
                'wallet_to' => $asset->wallet_number,
                'type_user' => TypeUserHelper::TYPE_USER_INVESTOR,
                'type_transaction' => Transaction::TYPE_INVESTOR_PAY_IN_DAI,
                'type' => Transaction::TYPE_CONTR,
                'status' => StatusHelper::STATUS_NEW
            ]);

            $transaction->client->moneyToBalance($transaction->coinmarketcap_id, 0);

            return $transaction;
        });
    }

    public function requestPhoneDeposit(User $user, Carbon $now): void
    {
        if (empty($user->phone)) {
            throw new \DomainException(trans('site.cabinet.phone-empty'));
        }

        $codeVerified = $user->codeVerifies()->investorDepositPay()->orderBy('id', 'desc')->first();
        if (!empty($codeVerified->verify_token) && $codeVerified->verify_token_expire && $codeVerified->verify_token_expire->gt($now)) {
            throw new \DomainException(trans('site.cabinet.Code-already-send'));
        }

        $codeVerifiedNew = $user->codeVerifies()->create([
            'type' => CodeVerifiedField::TYPE_INVESTOR_DEPOSIT_PAY,
            'verify_token' => (string)random_int(1000000, 9999999),
            'verify_token_expire' => $now->copy()->addSeconds(180)
        ]);

        $this->sms->send($user->phone, trans('site.cabinet.Verification-code') . ': ' . $codeVerifiedNew->verify_token);
    }

    public function requestPhonePayoutDai(User $user, Carbon $now): void
    {
        if (empty($user->phone)) {
            throw new \DomainException(trans('site.cabinet.phone-empty'));
        }

        $codeVerified = $user->codeVerifies()->investorDepositPayOutDai()->orderBy('id', 'desc')->first();
        if (!empty($codeVerified->verify_token) && $codeVerified->verify_token_expire && $codeVerified->verify_token_expire->gt($now)) {
            throw new \DomainException(trans('site.cabinet.Code-already-send'));
        }

        $codeVerifiedNew = $user->codeVerifies()->create([
            'type' => CodeVerifiedField::TYPE_INVESTOR_DEPOSIT_PAY_OUT_DAI,
            'verify_token' => (string)random_int(1000000, 9999999),
            'verify_token_expire' => $now->copy()->addSeconds(180)
        ]);

        $this->sms->send($user->phone, trans('site.cabinet.Verification-code') . ': ' . $codeVerifiedNew->verify_token);
    }

    private function reLogin2FA(Request $request)
    {
        (new Google2FAAuthenticator(request()))->logout();
        $authenticator = app(Google2FAAuthenticator::class)->boot($request);
        return $authenticator->isAuthenticated();
    }

}
