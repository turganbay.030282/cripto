<?php

namespace App\Services\Translation\Repositories;

use App\Entity\Translation;
use App\Services\Translation\Requests\Filter\FilterRequest;
use Illuminate\Database\Eloquent\Builder;

class TranslationRepository
{
    public function searchAdminTranslate(FilterRequest $request)
    {
        $query = Translation::where('group', Translation::group_admin)->sortable(["id" => "desc"]);
        $this->filters($query, $request);
        return $query;
    }

    public function searchSiteTranslate(FilterRequest $request)
    {
        $query = Translation::where('group', Translation::group_front)->sortable(["id" => "desc"]);
        $this->filters($query, $request);
        return $query;
    }

    private function filters(Builder $query, FilterRequest $request)
    {
        if (!empty($value = $request->get('key'))) {
            $query->where('key', 'like', '%' . $value . '%');
        }
        if (!empty($value = $request->get('value'))) {
            $query->where('text', 'like', '%' . $value . '%');
        }
    }
}
