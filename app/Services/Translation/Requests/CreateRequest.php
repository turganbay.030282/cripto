<?php

namespace App\Services\Translation\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $array['key'] = 'required|string|max:1000';
        foreach (config('translatable.locales') as $locale){
            $array['text.'.$locale] = 'nullable|string|max:1000';
        }
        return $array;
    }
}
