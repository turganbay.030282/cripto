<?php

namespace App\Services\Translation\Requests\Filter;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'key' => 'nullable|string|max:255',
            'value' => 'nullable|string|max:255'
        ];
    }
}
