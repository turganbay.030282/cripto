<?php

namespace App\Services\Translation;

use App\Entity\Translation;
use App\Services\Translation\Requests\CreateRequest;
use App\Services\Translation\Requests\UpdateRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Filesystem\Filesystem;

class TranslationService
{
    protected $app;
    protected $files;

    public function __construct(Application $app, Filesystem $files)
    {
        $this->app = $app;
        $this->files = $files;
    }

    public function create(CreateRequest $request): Translation
    {
        $request['group'] = Translation::group_front;
        $translation = Translation::create($request->all());
        $this->fromDb(Translation::group_front);
        return $translation;
    }

    public function update(UpdateRequest $request, Translation $translation): void
    {
        $translation->update($request->all());
        $this->fromDb(Translation::group_front);
    }

    public function createForAdminPanel(CreateRequest $request): Translation
    {
        $request['group'] = Translation::group_admin;
        $translation = Translation::create($request->all());
        $this->fromDb(Translation::group_admin);
        return $translation;
    }

    public function updateForAdminPanel(UpdateRequest $request, Translation $translation): void
    {
        $translation->update($request->all());
        $this->fromDb(Translation::group_admin);
    }

    public function toDb($group)
    {
        $translationsEn = \Arr::dot(\Lang::getLoader()->load('en', $group));
        $translationsRu = \Arr::dot(\Lang::getLoader()->load('ru', $group));
        if ($translationsEn) {
            foreach ($translationsEn as $key => $textEn) {
                $this->addTranslation($group, $key, $textEn, $translationsRu[$key]);
            }
        }
    }

    public function fromDb($group)
    {
        $translations = Translation::where('group', $group)->get();
        $tree = $this->makeTree($translations);
        foreach ($tree as $locale => $groups) {
            if (isset($groups[$group])) {
                $translations = $groups[$group];
                $path = $this->app['path.lang'];

                $locale_path = $locale . DIRECTORY_SEPARATOR . $group;
                $subfolders = explode(DIRECTORY_SEPARATOR, $locale_path);
                array_pop($subfolders);

                $subfolder_level = '';
                foreach ($subfolders as $subfolder) {
                    $subfolder_level = $subfolder_level . $subfolder . DIRECTORY_SEPARATOR;

                    $temp_path = rtrim($path . DIRECTORY_SEPARATOR . $subfolder_level, DIRECTORY_SEPARATOR);
                    if (!is_dir($temp_path)) {
                        mkdir($temp_path, 0777, true);
                    }
                }

                $path = $path . DIRECTORY_SEPARATOR . $locale . DIRECTORY_SEPARATOR . $group . '.php';

                $output = "<?php\n\nreturn " . var_export($translations, true) . ';' . \PHP_EOL;
                $this->files->put($path, $output);
            }
        }
    }

    private function addTranslation($group, $key, $textEn, $textRu)
    {
        $inputs = [];
        $inputs['group'] = $group;
        $inputs['key'] = $key;
        $inputs['text']['en'] = $textEn;
        $inputs['text']['ru'] = $textRu;
        if(!Translation::where('group', $group)->where('key', $key)->exists()){
            Translation::create($inputs);
        }
    }

    private function makeTree($translations)
    {
        $array = [];
        foreach ($translations as $translation) {
            foreach ($translation['text'] as $locale => $value) {
                \Arr::set(
                    $array[$locale][$translation->group],
                    $translation->key,
                    $value
                );
            }
        }

        return $array;
    }
}
