<?php

namespace App\Services\User;

use App\Entity\Tpl\NotifyText;
use App\Entity\Trader\CreditLevelUrVerification;
use App\Entity\Trader\CreditLevelVerification;
use App\Entity\Trader\CreditLineVerification;
use App\Entity\User;
use App\Entity\UserVerification;
use App\Events\TplUserEvent;
use App\Services\User\Requests\Verify\CreditLevel\VerifyCreditLevelFinishRequest;
use App\Services\User\Requests\Verify\CreditLevel\VerifyCreditLevelManagerRequest;
use App\Services\User\Requests\Verify\CreditLevel\VerifyCreditLevelUrFinishRequest;

class CreditLevelVerifyService
{
    public function creditLevelVerifySend(VerifyCreditLevelFinishRequest $request, User $user): void
    {
        \DB::transaction(function () use ($user, $request) {
            $creditLevelData = session()->has('creditLevel') ? session()->get('creditLevel') : [];
            $inputs = array_merge($creditLevelData, $request->all());

            $userVerification = $user->creditLevelVerifications()->create(
                array_filter($inputs, function ($key) {
                    return !in_array($key, ['_token', 'file_statement', 'file_income_work', 'file_utility_bill']);
                }, ARRAY_FILTER_USE_KEY)
            );
            if ($request->has('file_statement')) {
                $userVerification->addFile($inputs['file_statement'], 'file_statement');
            }
            if ($request->has('file_income_work')) {
                $userVerification->addFile($inputs['file_income_work'], 'file_income_work');
            }
            if ($request->has('file_utility_bill')) {
                $userVerification->addFile($inputs['file_utility_bill'], 'file_utility_bill');
            }
        });
        session()->flash('creditLevel');
    }

    public function creditLevelUrVerifySend(VerifyCreditLevelUrFinishRequest $request, User $user): void
    {
        \DB::transaction(function () use ($user, $request) {
            $creditLevelData = session()->has('creditLevelUr') ? session()->get('creditLevelUr') : [];
            $inputs = array_merge($creditLevelData, $request->all());

            $userVerification = $user->creditLevelUrVerifications()->create(
                array_filter($inputs, function ($key) {
                    return !in_array($key, ['_token', 'file_statement', 'file_income_work', 'file_utility_bill', 'name', 'surname', 'personal_code', 'capital_amount']);
                }, ARRAY_FILTER_USE_KEY)
            );
            if ($request->has('file_statement')) {
                $userVerification->addFile($inputs['file_statement'], 'file_statement');
            }
            if ($request->has('file_income_work')) {
                $userVerification->addFile($inputs['file_income_work'], 'file_income_work');
            }
            if ($request->has('file_utility_bill')) {
                $userVerification->addFile($inputs['file_utility_bill'], 'file_utility_bill');
            }

            foreach ($inputs['name'] as $key => $name) {
                $userVerification->persons()->create([
                    'name' => $name,
                    'surname' => $inputs['surname'][$key],
                    'personal_code' => $inputs['personal_code'][$key],
                    'capital_amount' => $inputs['capital_amount'][$key],
                ]);
            }
        });
        session()->flash('creditLevelUr');
    }

    public function creditLevelVerify(VerifyCreditLevelManagerRequest $request, CreditLevelVerification $creditLevelVerification): void
    {
        \DB::transaction(function () use ($request, $creditLevelVerification) {
            $creditLevelVerification->update([
                'amount' => $request->get('amount'),
                'manager_id' => $request->user()->id,
                'status' => $request->get('status'),
                'comment' => $request->get('comment')
            ]);
            $user = $creditLevelVerification->user;
            if ($request->get('status') == UserVerification::STATUS_APPROVED) {
                $user->update([
                    'credit_level_id' => $request->get('credit_level_id'),
                    'balance_trader' => $user->balance_trader + $creditLevelVerification->amount
                ]);
                $user->creditLineVerifications()->create([
                    'amount' => $creditLevelVerification->amount,
                    'manager_id' => $request->user()->id,
                    'status' => CreditLineVerification::STATUS_APPROVED,
                    'comment' => $request->get('comment')
                ]);

                $userVerification = $user->userVerification;
                if ($userVerification && (!$userVerification->address || $userVerification->address == 'address')) {
                    $userVerification->update(['address' => $creditLevelVerification->address]);
                }

                event(new TplUserEvent(
                    $user,
                    NotifyText::find(5)
                ));
            } else {
                event(new TplUserEvent(
                    $user,
                    NotifyText::find(6)
                ));
            }
        });
    }

    public function creditLevelUrVerify(VerifyCreditLevelManagerRequest $request, CreditLevelUrVerification $creditLevelUrVerification): void
    {
        \DB::transaction(function () use ($request, $creditLevelUrVerification) {
            $creditLevelUrVerification->update([
                'amount' => $request->get('amount'),
                'manager_id' => $request->user()->id,
                'status' => $request->get('status'),
                'comment' => $request->get('comment')
            ]);
            $user = $creditLevelUrVerification->user;
            if ($request->get('status') == UserVerification::STATUS_APPROVED) {
                $user->update([
                    'credit_level_id' => $request->get('credit_level_id'),
                    'balance_trader' => $user->balance_trader + $creditLevelUrVerification->amount
                ]);

                $user->creditLineVerifications()->create([
                    'amount' => $creditLevelUrVerification->amount,
                    'manager_id' => $request->user()->id,
                    'status' => CreditLineVerification::STATUS_APPROVED,
                    'comment' => $request->get('comment')
                ]);

                $userVerification = $user->userVerification;
                if ($userVerification && (!$userVerification->address || $userVerification->address == 'address')) {
                    $userVerification->update(['address' => $creditLevelUrVerification->address]);
                }

                event(new TplUserEvent(
                    $user,
                    NotifyText::find(5)
                ));
            } else {
                event(new TplUserEvent(
                    $user,
                    NotifyText::find(6)
                ));
            }
        });
    }
}
