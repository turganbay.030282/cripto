<?php

namespace App\Services\User;

use App\Entity\User;
use App\Services\User\Requests\G2faDisabledRequest;
use App\Services\User\Requests\G2faEnableRequest;
use Carbon\Carbon;

class G2faService
{
    private $google2fa;

    public function __construct()
    {
        $this->google2fa = app('pragmarx.google2fa');
    }

    public function generateAuthenticationKey()
    {
        $authenticationKey = $this->google2fa->generateSecretKey();
        session()->flash('authenticationKey', $authenticationKey);
        return $authenticationKey;
    }

    public function generateQRCodeInline(User $user, $authenticationKey)
    {
        return $this->google2fa->getQRCodeInline(
            config('app.name'),
            $user->email,
            $authenticationKey
        );
    }

    public function g2faEnable(G2faEnableRequest $request, User $user)
    {
        $secret = $request->get('secret');
        $valid = $this->google2fa->verifyKey(session('authenticationKey'), $secret);
        if (!$valid) {
            throw new \DomainException(trans('site.cabinet.2gaf-invalid-verification-code'));
        }

        $user->update([
            'google2fa_enable' => true,
            'google2fa_secret' => session('authenticationKey')
        ]);
    }

    public function verifySecretUser(User $user, string $secret)
    {
        $valid = $this->google2fa->verifyKey($user->google2fa_secret, $secret);
        if (!$valid) {
            return false;
        }
        return true;
    }

    public function g2faDisable(G2faDisabledRequest $request, User $user)
    {
        if (!(\Hash::check($request->get('current-password'), auth()->user()->password))) {
            throw new \DomainException(trans('site.cabinet.2gaf-password-not-matches'));
        }

        $user->update([
            'google2fa_enable' => false
        ]);
    }
}
