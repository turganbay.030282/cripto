<?php

namespace App\Services\User;

use App\Entity\Handbook\TypeTransaction;
use App\Entity\Investor\InvestorRankVerification;
use App\Entity\Tpl\NotifyText;
use App\Entity\User;
use App\Entity\UserVerification;
use App\Events\TplUserEvent;
use App\Services\User\Requests\Verify\InvestorRank\InvestRankThirdRequest;
use App\Services\User\Requests\Verify\InvestorRank\InvestRankUrFourRequest;
use App\Services\User\Requests\Verify\InvestorRank\VerifyInvestRankManagerRequest;

class InvestorRankService
{
    public function investorRankFinish(InvestRankThirdRequest $request, User $user): void
    {
        $data = session()->has('investorRank') ? session()->get('investorRank') : [];
        $inputs = array_merge($data, $request->all());

        $user->investorRankVerifications()->create($inputs);

        session()->flash('creditLevel');
    }

    public function investorRankFinishUr(InvestRankUrFourRequest $request, User $user): void
    {
        $data = session()->has('investorRankUr') ? session()->get('investorRankUr') : [];
        $inputs = array_merge($data, $request->all());

        $user->investorRankVerifications()->create($inputs);

        session()->flash('investorRankUr');
    }

    public function investRankVerify(VerifyInvestRankManagerRequest $request, InvestorRankVerification $investorRankVerification): void
    {
        \DB::transaction(function () use ($request, $investorRankVerification) {
            $investorRankVerification->update([
                'manager_id' => $request->user()->id,
                'status' => $request->get('status'),
                'comment' => $request->get('comment')
            ]);

            $user = $investorRankVerification->user;

            if ($request->get('status') == UserVerification::STATUS_APPROVED) {
                $typeTransaction = TypeTransaction::findOrFail($request->get('investor_rank_id'));

                $user->update(['investor_rank' => $typeTransaction->level]);

                $userVerification = $user->userVerification;
                if ($userVerification && (!$userVerification->address || $userVerification->address == 'address')) {
                    $userVerification->update(['address' => $investorRankVerification->address]);
                }

                event(new TplUserEvent(
                    $user,
                    NotifyText::find(7)
                ));
            } else {
                event(new TplUserEvent(
                    $user,
                    NotifyText::find(8)
                ));
            }
        });
    }

}
