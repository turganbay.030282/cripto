<?php

namespace App\Services\User;

use App\AppServices\Sms\SmsSender;
use App\Entity\CodeVerifiedField;
use App\Entity\RegisterVerifiedCode;
use App\Entity\User;
use Carbon\Carbon;

class PhoneService
{
    private $sms;

    public function __construct(SmsSender $sms)
    {
        $this->sms = $sms;
    }

    public function unVerifyPhone(User $user): void
    {
        $user->update(['phone_verified' => false]);
    }

    public function registerPhoneRequest(string $phone): void
    {
        if (empty($phone)) {
            throw new \DomainException(trans('site.cabinet.phone-empty'));
        }

        if (User::where('phone', $phone)->whereIn('status', [User::STATUS_ACTIVE, User::STATUS_BLOCK])->exists()) {
            throw new \DomainException(trans('site.page.register-already-register'));
        }

        $codeVerifiedNew = RegisterVerifiedCode::create([
            'phone' => $phone,
            'verify_token' => (string)random_int(1000000, 9999999),
            'verify_token_expire' => Carbon::now()->addSeconds(180)
        ]);

        $this->sms->send($phone, trans('site.cabinet.Verification-code') . ': ' . $codeVerifiedNew->verify_token);
    }

    public function requestPhoneVerification(User $user, Carbon $now): void
    {
        if (empty($user->phone)) {
            throw new \DomainException(trans('site.cabinet.phone-empty'));
        }

        $codeVerified = $user->codeVerifies()->verifyPhone()->orderBy('id', 'desc')->first();
        if (!empty($codeVerified->verify_token) && $codeVerified->verify_token_expire && $codeVerified->verify_token_expire->gt($now)) {
            throw new \DomainException(trans('site.cabinet.Code-already-send'));
        }

        $codeVerifiedNew = $user->codeVerifies()->create([
            'type' => CodeVerifiedField::TYPE_VERIFY,
            'verify_token' => (string)random_int(1000000, 9999999),
            'verify_token_expire' => $now->copy()->addSeconds(180)
        ]);

        $this->sms->send($user->phone, trans('site.cabinet.Verification-code') . ': ' . $codeVerifiedNew->verify_token);
    }

    public function verifyPhone(User $user, $token, Carbon $now): void
    {
        $codeVerified = $user->codeVerifies()->verifyPhone()->orderBy('id', 'desc')->first();
        if ($token !== $codeVerified->verify_token) {
            throw new \DomainException(trans('site.cabinet.Incorrect-verify-code'));
        }

//        if ($codeVerified->verify_token_expire->lt($now)) {
//            throw new \DomainException('Token is expired.');
//        }

        $user->update(['phone_verified' => true]);
    }

}
