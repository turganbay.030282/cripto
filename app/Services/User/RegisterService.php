<?php

namespace App\Services\User;

use App\Entity\RegisterVerifiedCode;
use App\Entity\Statistic\StatisticPromoCode;
use App\Entity\User;
use App\Mail\Auth\VerifyMail;
use App\Services\User\Requests\RegisterRequest;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegisterService
{
    private $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function register(RegisterRequest $request): User
    {
        if (!RegisterVerifiedCode::where('phone', $request['phone'])->where('verify_token', $request['verify_token'])->exists()) {
            throw new \DomainException(trans('site.cabinet.Incorrect-verify-code'));
        }

        $referer_id = null;
        $ref = \Request::cookie('ref');
        if ($ref) {
            $referrer = User::where('promo_code', $ref)->first();
            $referer_id = $referrer ? $referrer->id : null;
        }
        $user = User::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'phone_verified' => true,
            'status' => User::STATUS_WAIT,
            'role' => User::ROLE_USER,
            'password' => Hash::make($request['password']),
            'is_investor' => $request['is_investor'],
            'email_verify_token' => Str::uuid(),
            'promo_code' => $this->generatePromo(),
            'referer_id' => $referer_id,
            'is_ur' => $request->get('is_ur', 0),
            'lang' => \App::getLocale()
        ]);

        $statisticPromoCode = StatisticPromoCode::where('email', $user->email)->first();
        if ($statisticPromoCode) {
            $statisticPromoCode->update([
                'reg_user_id' => $user->id
            ]);
        }

        $this->mailer->to($user)->send(new VerifyMail($user));
        event(new Registered($user));

        return $user;
    }

    public function verify($id): void
    {
        /** @var User $user */
        $user = User::findOrFail($id);
        if (!$user->isWait()) {
            throw new \DomainException(trans('site.page.cabinet.user-already-verified'));
        }
        $user->update([
            'status' => User::STATUS_ACTIVE,
            'email_verify_token' => null
        ]);
    }

    public function resend($email): void
    {
        /** @var User $user */
        $user = User::where('email', $email)->first();
        if (!$user) {
            throw new \DomainException(trans('site.page.cabinet.user-not-found'));
        }
        $this->mailer->to($user)->send(new VerifyMail($user));
    }

    public function updatePromo(User $user): void
    {
        $user->update(['promo_code' => $this->generatePromo()]);
    }

    public function generatePromo($prefix = "")
    {
        $code = $prefix . $this->generateCode(8);
        while (User::where('promo_code', $code)->exists()) {
            $code = $prefix . $this->generateCode(8);
        }
        return $code;
    }

    private function generateCode($count)
    {
        $code = "";
        $chars = '12345ABCDEFGHIJKLMNOPQRSTUVWXYZ67890';
        for ($ichars = 1; $ichars <= $count; ++$ichars) {
            $random = str_shuffle($chars);
            $code .= $random[0];
        }
        return $code;
    }
}
