<?php

namespace App\Services\User\Repositories;

use App\Entity\Investor\DepositLineVerification;
use App\Entity\Investor\InvestorRankVerification;
use App\Entity\Trader\CreditLevelUrVerification;
use App\Entity\Trader\CreditLevelVerification;
use App\Entity\Trader\CreditLineVerification;
use App\Entity\User;
use App\Entity\UserChange;
use App\Entity\UserVerification;
use App\Services\User\Requests\Filter\FilterChangeRequest;
use App\Services\User\Requests\Filter\FilterCreditLevelVerificationsRequest;
use App\Services\User\Requests\Filter\FilterCreditLineVerificationsRequest;
use App\Services\User\Requests\Filter\FilterDepositLineVerificationsRequest;
use App\Services\User\Requests\Filter\FilterInvestorRankVerificationsRequest;
use App\Services\User\Requests\Filter\FilterRequest;
use App\Services\User\Requests\Filter\FilterVerificationRequest;

class EloquentUserRepository
{

    public function search(FilterRequest $request)
    {
        $query = User::sortable(["id" => "desc"])->with(
            'userVerification',
            'creditLevelVerification',
            'creditLevelUrVerification',
            'investorRankVerification',
            'creditLineVerification',
            'depositLineVerification'
        );

        if (!empty($value = $request->get(''))) {
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('name'))) {
            $query->where(function ($query) use ($value) {
                $query->where('first_name', 'like', '%' . $value . '%');
                $query->orWhere('last_name', 'like', '%' . $value . '%');
            });
        }
        if (!empty($value = $request->get('email'))) {
            $query->where('email', 'like', '%' . $value . '%');
        }
        if (!empty($value = $request->get('phone'))) {
            $query->where('phone', 'like', '%' . $value . '%');
        }
        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }
        if (!empty($value = $request->get('role'))) {
            $query->where('role', $value);
        }
        return $query;
    }

    public function searchVerification(FilterVerificationRequest $request)
    {
        $query = UserVerification::sortable(["id" => "desc"]);

        if (!empty($value = $request->get(''))) {
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('name'))) {
            $query->where('name', 'like', '%' . $value . '%');
        }
        if (!empty($value = $request->get('email'))) {
            $query->where('email', 'like', '%' . $value . '%');
        }
        if (!empty($value = $request->get('phone'))) {
            $query->where('phone', 'like', '%' . $value . '%');
        }
        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }
        return $query;
    }

    public function searchCreditLevelVerification(FilterCreditLevelVerificationsRequest $request)
    {
        $query = CreditLevelVerification::sortable(["id" => "desc"]);

        if (!empty($value = $request->get(''))) {
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }
        return $query;
    }

    public function searchCreditLevelUrVerification(FilterCreditLevelVerificationsRequest $request)
    {
        $query = CreditLevelUrVerification::sortable(["id" => "desc"]);

        if (!empty($value = $request->get(''))) {
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }
        return $query;
    }

    public function searchCreditLineVerification(FilterCreditLineVerificationsRequest $request)
    {
        $query = CreditLineVerification::sortable(["id" => "desc"]);

        if (!empty($value = $request->get(''))) {
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }
        return $query;
    }

    public function searchInvestorRankVerification(FilterInvestorRankVerificationsRequest $request)
    {
        $query = InvestorRankVerification::sortable(["id" => "desc"]);

        if (!empty($value = $request->get(''))) {
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }
        return $query;
    }

    public function searchDepositLineVerification(FilterDepositLineVerificationsRequest $request)
    {
        $query = DepositLineVerification::sortable(["id" => "desc"]);

        if (!empty($value = $request->get(''))) {
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }
        return $query;
    }

    public function searchChange(FilterChangeRequest $request)
    {
        $query = UserChange::sortable(["id" => "desc"]);

        if (!empty($value = $request->get(''))) {
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('name'))) {
            $query->where('name', 'like', '%' . $value . '%');
        }
        if (!empty($value = $request->get('email'))) {
            $query->where('email', 'like', '%' . $value . '%');
        }
        if (!empty($value = $request->get('phone'))) {
            $query->where('phone', 'like', '%' . $value . '%');
        }
        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }
        return $query;
    }

}
