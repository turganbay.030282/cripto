<?php

namespace App\Services\User\Repositories;

use App\Entity\User;

class UserRepository
{

    public function contractFreeAssets(User $user)
    {
        $assets = $user->contractFreeAssets()
            ->with('coinmarketcap')
            ->orderBy('type_asset_id', 'asc')
            ->get();

        $result = collect([]);
        if($assets->isNotEmpty()){
            $grouped = $assets->groupBy('type_asset_id');
            foreach ($grouped as $id => $group){
                $result->push([
                    'id' => $group->first()->coinmarketcap->id,
                    'symbol' => $group->first()->coinmarketcap->symbol,
                    'amount_asset' => $group->sum('amount_asset')
                ]);
            }
        }

        return $result;
    }

}
