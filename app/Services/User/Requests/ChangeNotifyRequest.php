<?php

namespace App\Services\User\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangeNotifyRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'notify_news' => 'required|boolean',
            'notify_promo' => 'required|boolean',
            'notify_by_email' => 'required|boolean',
            'notify_by_sms' => 'required|boolean',
            'notify_by_push' => 'required|boolean',
        ];
    }
}
