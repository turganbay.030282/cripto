<?php

namespace App\Services\User\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangeTypeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'is_investor' => 'required|boolean'
        ];
    }

}
