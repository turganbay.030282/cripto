<?php

namespace App\Services\User\Requests;

use App\Entity\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'phone' => 'nullable|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'role' => ['required', 'string', Rule::in(array_keys(User::listRoleUser()))],
            'status' => ['required', 'string', Rule::in(array_keys(User::listStatus()))],
            'password' => 'required|string|min:6',
            'photo' => 'required|image|mimes:jpg,jpeg,png,svg',
            'crop' => 'nullable|string',
            'position' => 'nullable|string|max:255',
            'wallets.*' => 'required|string|max:255',
        ];
    }

}
