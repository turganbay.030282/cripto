<?php

namespace App\Services\User\Requests\Filter;

use App\Entity\User;
use App\Entity\UserVerification;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterChangeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer|exists:user_verifications,id',
            'email' => 'nullable|email',
            'phone' => 'nullable|string|max:255',
            'name' => 'nullable|string|max:255',
            'status' => ['nullable',  'string',  Rule::in(array_keys(UserVerification::listStatus()))],
        ];
    }
}
