<?php

namespace App\Services\User\Requests\Filter;

use App\Entity\Trader\CreditLevelVerification;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterCreditLevelVerificationsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer|exists:credit_level_verifications,id',
            'status' => ['nullable',  'string',  Rule::in(array_keys(CreditLevelVerification::listStatus()))],
        ];
    }
}
