<?php

namespace App\Services\User\Requests\Filter;

use App\Entity\Trader\CreditLineVerification;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterDepositLineVerificationsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer|exists:deposit_line_verifications,id',
            'status' => ['nullable',  'string',  Rule::in(array_keys(CreditLineVerification::listStatus()))],
        ];
    }
}
