<?php

namespace App\Services\User\Requests\Filter;

use App\Entity\Investor\InvestorRankVerification;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterInvestorRankVerificationsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer|exists:investor_rank_verifications,id',
            'status' => ['nullable',  'string',  Rule::in(array_keys(InvestorRankVerification::listStatus()))],
        ];
    }
}
