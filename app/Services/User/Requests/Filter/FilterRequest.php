<?php

namespace App\Services\User\Requests\Filter;

use App\Entity\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'nullable|integer|exists:users,id',
            'email' => 'nullable|email',
            'phone' => 'nullable|string|max:255',
            'name' => 'nullable|string|max:255',
            'status' => ['nullable',  'string',  Rule::in(array_keys(User::listStatus()))],
            'role' => ['nullable',  'string',  Rule::in(array_keys(User::listRoleUser()))],
        ];
    }
}
