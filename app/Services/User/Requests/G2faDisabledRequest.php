<?php

namespace App\Services\User\Requests;

use Illuminate\Foundation\Http\FormRequest;

class G2faDisabledRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'current-password' => 'required|string|max:255',
        ];
    }

}
