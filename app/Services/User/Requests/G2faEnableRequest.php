<?php

namespace App\Services\User\Requests;

use Illuminate\Foundation\Http\FormRequest;

class G2faEnableRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'secret' => 'required|string|max:255',
        ];
    }

}
