<?php

namespace App\Services\User\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'first_name' => 'required|string|max:255|regex:/^[a-zA-ZÑñ\s]+$/',
            'last_name' => 'required|string|max:255|regex:/^[a-zA-ZÑñ\s]+$/',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'politics' => 'accepted',
            'age' => 'accepted',
            'is_investor' => 'required|boolean',
            'g-recaptcha-response' => production() ? 'required|captcha' : 'nullable',
            'verify_token' => 'required|integer',
            'is_ur' => 'nullable|boolean',
        ];
    }

}
