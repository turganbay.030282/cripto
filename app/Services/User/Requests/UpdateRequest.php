<?php

namespace App\Services\User\Requests;

use App\Entity\User;

/**
 * @property User $user
 */
class UpdateRequest extends CreateRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $array = parent::rules();
        $array['email'] = 'required|string|email|max:255|unique:users,email,' . $this->user->id;
        $array['password'] = 'nullable|string|min:6';
        $array['photo'] = 'nullable|image|mimes:jpg,jpeg,png,svg';
        $array['wallet.*'] = 'required|string|max:255';
        return $array;
    }
}
