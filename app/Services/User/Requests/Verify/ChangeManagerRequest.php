<?php

namespace App\Services\User\Requests\Verify;

use App\Entity\UserChange;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChangeManagerRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'comment' => 'nullable|string',
            'status' => ['required', 'string', Rule::in([UserChange::STATUS_REJECT, UserChange::STATUS_APPROVED])],
        ];
    }

}
