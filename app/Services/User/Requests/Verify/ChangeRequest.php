<?php

namespace App\Services\User\Requests\Verify;

use Illuminate\Foundation\Http\FormRequest;

class ChangeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'birthday' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'email' => 'required|string|max:255|unique:users,email,'.$this->user()->id,
            'phone' => 'required|string|max:255|unique:users,phone,'.$this->user()->id,
            'card_id' => 'nullable|image',
        ];
    }

}
