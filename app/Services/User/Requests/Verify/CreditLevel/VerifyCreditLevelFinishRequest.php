<?php

namespace App\Services\User\Requests\Verify\CreditLevel;

use Illuminate\Foundation\Http\FormRequest;

class VerifyCreditLevelFinishRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'credit_liabilities' => 'nullable|string',
            'credit_liabilities_month' => 'nullable|integer',
            'credit_type' => 'nullable|string',
            'credit_date_from' => 'nullable|date',
            'credit_date_to' => 'nullable|date',
            'file_statement' => 'nullable|file|mimes:pdf',
            'file_income_work' => 'nullable|file|mimes:pdf',
            'file_utility_bill' => 'nullable|file|mimes:pdf',
            'amount' => 'required|numeric',
            'accepted' => 'accepted',
        ];
    }
}
