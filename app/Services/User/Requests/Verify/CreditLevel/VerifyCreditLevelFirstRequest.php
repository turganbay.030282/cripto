<?php

namespace App\Services\User\Requests\Verify\CreditLevel;

use Illuminate\Foundation\Http\FormRequest;

class VerifyCreditLevelFirstRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'sex' => 'required|string',
            'personal_code' => 'required|string',
            'family_status' => 'required|string',
            'political_functions' => 'required|string',
            'phone' => 'required|string',
            'dependents' => 'required|string',
            'type_accommodation' => 'required|string',
            'education' => 'required|string',
            'citizenship' => 'required|string'
        ];
    }
}
