<?php

namespace App\Services\User\Requests\Verify\CreditLevel;

use App\Entity\Trader\CreditLevelVerification;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VerifyCreditLevelManagerRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'amount' => 'required|numeric',
            'credit_level_id' => ['required', 'integer', 'exists:credit_levels,id', Rule::requiredIf(function(){return $this->status == CreditLevelVerification::STATUS_APPROVED;})],
            'comment' => 'nullable|string',
            'status' => ['required', 'string', Rule::in([CreditLevelVerification::STATUS_REJECT, CreditLevelVerification::STATUS_APPROVED])],
        ];
    }

}
