<?php

namespace App\Services\User\Requests\Verify\CreditLevel;

use Illuminate\Foundation\Http\FormRequest;

class VerifyCreditLevelRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'amount' => 'required|integer',
        ];
    }

}
