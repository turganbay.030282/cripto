<?php

namespace App\Services\User\Requests\Verify\CreditLevel;

use Illuminate\Foundation\Http\FormRequest;

class VerifyCreditLevelSecondRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'residence_county' => 'required|string',
            'residence_city' => 'required|string',
            'residence_street' => 'required|string',
            'residence_postcode' => 'required|string',
            'income_source' => 'required|string',
            'income_net' => 'required|integer',
            'income_additional' => 'required|integer',
            'income_place' => 'required|string',
            'income_position' => 'required|string',
            'income_work_experience' => 'required|string'
        ];
    }
}
