<?php

namespace App\Services\User\Requests\Verify\CreditLevel;

use Illuminate\Foundation\Http\FormRequest;

class VerifyCreditLevelUrFinishRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'file_statement' => 'nullable|file|mimes:pdf',
            'file_income_work' => 'nullable|file|mimes:pdf',
            'file_utility_bill' => 'nullable|file|mimes:pdf',
            'amount' => 'required|numeric',
            'accepted' => 'accepted',
        ];
    }
}
