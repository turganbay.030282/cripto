<?php

namespace App\Services\User\Requests\Verify\CreditLevel;

use Illuminate\Foundation\Http\FormRequest;

class VerifyCreditLevelUrFirstRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'company_name' => 'required|string|max:255',
            'reg_number' => 'required|string|max:255',
            'register_date' => 'required|date',
            'ur_address' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'contact_phone' => 'required|string|max:255',
            'email' => 'required|email',
            'primary_occupation' => 'required|string|max:255'
        ];
    }
}
