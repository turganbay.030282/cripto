<?php

namespace App\Services\User\Requests\Verify\CreditLevel;

use Illuminate\Foundation\Http\FormRequest;

class VerifyCreditLevelUrSecondRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name.*' => 'required|string',
            'surname.*' => 'required|string',
            'personal_code.*' => 'required|string',
            'capital_amount.*' => 'required|string|max:255',
            'person_name' => 'required|string|max:255',
            'person_surname' => 'required|string|max:255',
            'person_personal_code' => 'required|string|max:255',
            'person_turnover_prev' => 'required|string|max:255',
            'person_profit_prev' => 'required|string|max:255',
            'person_banks' => 'required|string|max:255',
        ];
    }
}
