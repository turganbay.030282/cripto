<?php

namespace App\Services\User\Requests\Verify\CreditLevel;

use Illuminate\Foundation\Http\FormRequest;

class VerifyCreditLevelUrThirdRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'creditor' => [$this->not_credit ? 'nullable' : 'required', 'string', 'max:255'],
            'start_amount' => [$this->not_credit ? 'nullable' : 'required', 'string', 'max:255'],
            'balance_obligations' => [$this->not_credit ? 'nullable' : 'required', 'string', 'max:255'],
            'credit_date_to' => [$this->not_credit ? 'nullable' : 'required', 'date'],
            'month_payments' => [$this->not_credit ? 'nullable' : 'required', 'string', 'max:255'],
            'credit_target' => [$this->not_credit ? 'nullable' : 'required', 'string', 'max:255'],
            'credit_source' => [$this->not_credit ? 'nullable' : 'required', 'string', 'max:255'],
        ];
    }
}

