<?php

namespace App\Services\User\Requests\Verify\InvestorRank;

use Illuminate\Foundation\Http\FormRequest;

class InvestRankUrFirstRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'company_name' => 'required|string|max:255',
            'company_reg_number' => 'required|string|max:255',
            'company_date_reg' => 'required|date',
            'company_country_reg' => 'required|string|max:255',
            'company_address' => 'required|string|max:255',
            'company_ur_address' => 'required|string|max:255',
            'company_field_activity' => 'required|string|max:255',
            'company_phone' => 'required|string|max:255',
            'company_email' => 'required|email',
        ];
    }

}
