<?php

namespace App\Services\User\Requests\Verify\InvestorRank;

use Illuminate\Foundation\Http\FormRequest;

class InvestRankUrFourRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'not_pep' => 'nullable|boolean',
            'politics' => 'nullable|string|max:255',
        ];
    }

}
