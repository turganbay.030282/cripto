<?php

namespace App\Services\User\Requests\Verify\InvestorRank;

use Illuminate\Foundation\Http\FormRequest;

class InvestRankUrSecondRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'full_name' => 'required|string|max:255',
            'birthday' => 'required|date',
            'sex' => 'required|string|max:255',
            'bin' => 'required|string|max:255',
            'place_birth' => 'required|string|max:255',
            'position' => 'required|string|max:255',
            'doc_name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|email',
            'professional_activities' => 'required|string|max:255',
            'volume_1' => 'nullable|boolean',
            'volume_2' => 'nullable|boolean',
            'volume_3' => 'nullable|boolean',
            'volume_4' => 'nullable|boolean',
            'volume_5' => 'nullable|boolean',
        ];
    }

}
