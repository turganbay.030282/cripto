<?php

namespace App\Services\User\Requests\Verify\InvestorRank;

use Illuminate\Foundation\Http\FormRequest;

class InvestRankUrThirdRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'source_business_activities' => 'nullable|boolean',
            'source_dividend' => 'nullable|boolean',
            'source_loans' => 'nullable|boolean',
            'source_income_assets' => 'nullable|boolean',
            'source_contributions' => 'nullable|boolean',
            'source_prize' => 'nullable|boolean',
            'source_other' => 'nullable|boolean',
            'source_other_text' => 'nullable|string|max:255',
            'source_country' => 'required|string|max:255',
            'beneficiary_name' => 'nullable|string|max:255',
        ];
    }

}
