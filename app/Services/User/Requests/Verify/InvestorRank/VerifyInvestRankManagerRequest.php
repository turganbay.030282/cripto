<?php

namespace App\Services\User\Requests\Verify\InvestorRank;

use App\Entity\Investor\InvestorRankVerification;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VerifyInvestRankManagerRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'investor_rank_id' => ['required', 'integer', Rule::requiredIf(function(){return $this->status == InvestorRankVerification::STATUS_APPROVED;})],
            'comment' => 'nullable|string',
            'status' => ['required', 'string', Rule::in([InvestorRankVerification::STATUS_REJECT, InvestorRankVerification::STATUS_APPROVED])],
        ];
    }

}
