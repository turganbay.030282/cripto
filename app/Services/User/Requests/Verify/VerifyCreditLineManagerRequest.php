<?php

namespace App\Services\User\Requests\Verify;

use App\Entity\Trader\CreditLineVerification;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VerifyCreditLineManagerRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'amount' => 'required|numeric',
            'comment' => 'nullable|string',
            'status' => ['required', 'string', Rule::in([CreditLineVerification::STATUS_REJECT, CreditLineVerification::STATUS_APPROVED])],
        ];
    }

}
