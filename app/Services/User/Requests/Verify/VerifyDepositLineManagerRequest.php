<?php

namespace App\Services\User\Requests\Verify;

use App\Entity\Investor\DepositLineVerification;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VerifyDepositLineManagerRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'comment' => 'nullable|string',
            'status' => ['required', 'string', Rule::in([DepositLineVerification::STATUS_REJECT, DepositLineVerification::STATUS_APPROVED])],
        ];
    }
}
