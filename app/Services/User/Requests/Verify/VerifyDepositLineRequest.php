<?php

namespace App\Services\User\Requests\Verify;

use Illuminate\Foundation\Http\FormRequest;

class VerifyDepositLineRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'amount' => 'required|integer',
        ];
    }

}
