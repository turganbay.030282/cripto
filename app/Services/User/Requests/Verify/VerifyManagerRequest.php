<?php

namespace App\Services\User\Requests\Verify;

use App\Entity\UserVerification;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VerifyManagerRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'comment' => 'nullable|string',
            'status' => ['required', 'string', Rule::in([UserVerification::STATUS_REJECT, UserVerification::STATUS_APPROVED])],
        ];
    }

}
