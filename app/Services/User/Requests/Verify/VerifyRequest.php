<?php

namespace App\Services\User\Requests\Verify;

use App\Entity\UserVerification;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VerifyRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'birthday' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'type_doc' => ['required', 'string', Rule::in(array_keys(UserVerification::lisTypeDocuments()))],
            'card_id' => 'required|file|mimes:jpg,jpeg,png,gif,bmp,pdf,doc,docx',
            'card_id_back' => 'nullable|file|mimes:jpg,jpeg,png,gif,bmp,pdf,doc,docx',
        ];
    }
}
