<?php

namespace App\Services\User;

use App\Api\GetId\GetIdApiSdkProvider;
use App\Api\GetId\GetIdResponse;
use App\AppServices\Sms\SmsSender;
use App\Entity\Investor\DepositLineVerification;
use App\Entity\Statistic\StatisticPromoCode;
use App\Entity\Tpl\NotifyText;
use App\Entity\Trader\CreditLineVerification;
use App\Entity\User;
use App\Entity\UserChange;
use App\Entity\UserVerification;
use App\Events\ChangeUserEvent;
use App\Events\TplUserEvent;
use App\Events\User\PromoEvent;
use App\Services\User\Requests\ChangeNotifyRequest;
use App\Services\User\Requests\PromoRequest;
use App\Services\User\Requests\Verify\ChangeManagerRequest;
use App\Services\User\Requests\Verify\ChangeRequest;
use App\Services\User\Requests\ChangeTypeRequest;
use App\Services\User\Requests\CreateRequest;
use App\Services\User\Requests\UpdateRequest;
use App\Services\User\Requests\Verify\VerifyCreditLineManagerRequest;
use App\Services\User\Requests\Verify\VerifyCreditLineRequest;
use App\Services\User\Requests\Verify\VerifyDepositLineManagerRequest;
use App\Services\User\Requests\Verify\VerifyDepositLineRequest;
use App\Services\User\Requests\Verify\VerifyManagerRequest;
use App\Services\User\Requests\Verify\VerifyRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Cookie;

class UserService
{
    protected $sms;

    public function __construct(SmsSender $sms)
    {
        $this->sms = $sms;
    }

    public function create(CreateRequest $request): User
    {
        return \DB::transaction(function () use ($request) {
            $user = User::create(array_merge($request->only(
                'first_name', 'last_name', 'phone', 'email', 'role', 'status', 'position'
            ), [
                "password" => bcrypt($request->get('password')),
            ]));

            foreach ($request->get('wallet') as $key => $wallet) {
                $id = $request->get('id')[$key];

                if ($id) {
                    $user->wallets()->where('id', $id)->update([
                        'wallet' => $wallet
                    ]);
                } else {
                    $user->wallets()->create([
                        'wallet' => $wallet
                    ]);
                }
            }

            $photo = $request["photo"];
            if (!empty($photo)) {
                $user->addImage($photo, $request["crop"]);
            }

            $cover = $request["cover"];
            if (!empty($cover)) {
                $user->addImage($cover, null, 'cover', 'covers');
            }
        });
    }

    public function edit(UpdateRequest $request, User $user): void
    {
        \DB::transaction(function () use ($user, $request) {
            $user->update($request->only(
                'first_name', 'last_name', 'phone', 'email', 'role', 'status', 'position'
            ));

            foreach ($request->get('wallet') as $key => $wallet) {
                $id = $request->get('id')[$key];

                if ($id) {
                    $user->wallets()->where('id', $id)->update([
                        'wallet' => $wallet
                    ]);
                } else {
                    $user->wallets()->create([
                        'wallet' => $wallet
                    ]);
                }
            }

            if (!empty($request->get('password')))
                $user->update(['password' => bcrypt($request->get('password'))]);

            $photo = $request["photo"];
            if (!empty($photo)) {
                $user->editImage($photo, $request["crop"]);
            }

            $cover = $request["cover"];
            if (!empty($cover)) {
                $user->editImage($cover, null, 'cover', 'covers');
            }
        });
    }

    public function changePassword($password, User $user): void
    {
        $user->update(['password' => bcrypt($password)]);
        event(new TplUserEvent(
            $user,
            NotifyText::find(24)
        ));
    }

    public function remove(User $user): void
    {
        $user->delete();
        event(new TplUserEvent(
            $user,
            NotifyText::find(NotifyText::USER_DELETE)
        ));
    }

    public function delete($userId): void
    {
        \DB::transaction(function () use ($userId) {
            $user = User::withTrashed()->where('id', $userId)->firstOrFail();
            $user->codeVerifies()->delete();
            $user->forceDelete();
        });
    }

    public function verifySend(VerifyRequest $request, User $user): void
    {
        \DB::transaction(function () use ($user, $request) {
            $userVerification = $user->userVerifications()->create($request->only('name', 'last_name', 'birthday', 'address', 'phone', 'type_doc'));

            $file = $request["card_id"];
            if (!empty($file)) {
                $userVerification->addImage($file, null, 'card_id');
            }

            $file2 = $request["card_id_back"];
            if (!empty($file2) && $userVerification->type_doc != UserVerification::DOCUMENT_TYPE_PASSPORT) {
                $userVerification->addImage($file2, null, 'card_id_back');
            }
        });
    }

    public function verify(VerifyManagerRequest $request, UserVerification $verification): void
    {
        \DB::transaction(function () use ($request, $verification) {
            $verification->update([
                'manager_id' => $request->user()->id,
                'status' => $request->get('status'),
                'comment' => $request->get('comment')
            ]);
            if ($request->get('status') == UserVerification::STATUS_APPROVED) {
                $verification->user->update(['is_verification' => true]);
                event(new TplUserEvent(
                    $verification->user,
                    NotifyText::find(3)
                ));
            } else {
                event(new TplUserEvent(
                    $verification->user,
                    NotifyText::find(4)
                ));
            }
        });
    }

    public function verifyAuto(UserVerification $verification): void
    {
        $response = (new \App\Api\GetId\GetIdApiProvider())->applicationStatus(['request_id' => $verification->external_id]);
        if ($response->overallResult->status != UserVerification::STATUS_APPROVED) {
            $verification->update([
                'status' => UserVerification::STATUS_REJECT,
                'data_api' => (array)$response
            ]);
            event(new TplUserEvent(
                $verification->user,
                NotifyText::find(4)
            ));
            throw new \DomainException(trans('site.page.cabinet.validation-error'));
        }

        \DB::transaction(function () use ($verification, $response) {
            $verification->update([
                'status' => UserVerification::STATUS_APPROVED,
                'data_api' => (array)$response
            ]);
            $verification->user->update(['is_verification' => true]);
            event(new TplUserEvent(
                $verification->user,
                NotifyText::find(3)
            ));
        });
    }

    public function verifyByWebHook(Request $request): void
    {
        $response = new GetIdResponse($request->all());
        if (!isset($response->getResponse()->overallResult)) {
            return;
        }

        $responsePluck = explode('-', $response->getClientId());
        $user = User::findOrFail($responsePluck[0]);

        $inputs = [];
        $inputs['external_id'] = $response->getId();
        $inputs['user_id'] = $user->id;
        $inputs['comment'] = $response->getStatus();
        $inputs['data_api'] = (array)$response->getResponse();

        $fields = $response->getAppFields();
        $inputs['name'] = $fields[0]->content;
        $inputs['last_name'] = $fields[1]->content;
        $inputs['birthday'] = $fields[3]->content;
        $inputs['address'] = 'address';
        $inputs['phone'] = 'phone';

        if (!$response->isApprove()) {
            $inputs['status'] = UserVerification::STATUS_REJECT;
            $verification = UserVerification::create($inputs);
            event(new TplUserEvent(
                $verification->user,
                NotifyText::find(4)
            ));
            throw new \DomainException(trans('site.page.cabinet.verification-error'));
        }

        \DB::transaction(function () use ($user, $inputs, $response) {
            $inputs['status'] = UserVerification::STATUS_APPROVED;
            $verification = UserVerification::create($inputs);
            $user->update(['is_verification' => true]);
            event(new TplUserEvent(
                $verification->user,
                NotifyText::find(3)
            ));
        });
    }

    public function setVerifyToken($customerId)
    {
        $getIdToken = Cookie::get('getIdToken');
        if ($getIdToken != null) {
            return [Cookie::get('getCustomerId'), $getIdToken];
        }

        try {
            $token = (new GetIdApiSdkProvider())->getToken($customerId);
        } catch (\Exception $exception) {
            \Log::error('setVerifyToken', [$exception]);
            return [null, null];
        }

        Cookie::queue('getIdToken', json_encode($token), 5);
        Cookie::queue('getCustomerId', $customerId, 5);
        return [$customerId, json_encode($token)];
    }

    public function changeType(ChangeTypeRequest $request, User $user): void
    {
        if ($user->is_investor) {
            $user->update(['is_investor' => false]);
        } else {
            $user->update(['is_investor' => true]);
        }
    }

    public function changeNotify(ChangeNotifyRequest $request, User $user): void
    {
        $user->update($request->all());
    }

    public function changeSend(ChangeRequest $request, User $user): void
    {
        \DB::transaction(function () use ($user, $request) {
            $userChange = $user->userChanges()->create($request->only('first_name', 'last_name', 'birthday', 'address', 'email', 'phone'));

            $file = $request["card_id"];
            if (!empty($file)) {
                $userChange->addImage($file, null, 'card_id', 'user_verifications');
            }
        });
    }

    public function changeApprove(ChangeManagerRequest $request, UserChange $userChange): void
    {
        \DB::transaction(function () use ($request, $userChange) {
            $userChange->update([
                'manager_id' => $request->user()->id,
                'status' => $request->get('status'),
                'comment' => $request->get('comment')
            ]);
            if ($request->get('status') == UserVerification::STATUS_APPROVED) {
                $user = $userChange->user;
                $userVerification = $user->userVerification;
                $user->update([
                    'first_name' => $userChange->first_name,
                    'last_name' => $userChange->last_name,
                    'email' => $userChange->email,
                    'phone' => $userChange->phone,
                    'phone_verified' => $userChange->phone != $user->phone ? false : true
                ]);
                if ($userChange->card_id) {
                    $user->addImage(new UploadedFile(storage_path('app/public/user_verifications/') . $userChange->card_id, $userChange->card_id), null);
                }
                if ($userVerification) {
                    $userVerification->update([
                        'name' => $userChange->first_name,
                        'birthday' => $userChange->birthday,
                        'address' => $userChange->address,
                        'phone' => $userChange->phone,
                        'card_id' => $userChange->card_id
                    ]);
                }
                event(new TplUserEvent(
                    $user,
                    NotifyText::find(23)
                ));
            }
        });
    }

    public function creditLineVerifySend(VerifyCreditLineRequest $request, User $user): void
    {
        \DB::transaction(function () use ($user, $request) {
            $userCreditLineVerification = $user->creditLineVerifications()->create($request->all());
        });
    }

    public function creditLineVerify(VerifyCreditLineManagerRequest $request, CreditLineVerification $creditLineVerification): void
    {
        \DB::transaction(function () use ($request, $creditLineVerification) {
            $creditLineVerification->update([
                'manager_id' => $request->user()->id,
                'amount' => $request->get('amount'),
                'status' => $request->get('status'),
                'comment' => $request->get('comment')
            ]);

            $user = $creditLineVerification->user;
            if ($creditLineVerification->isApprove()) {
                $user->update([
                    'balance_trader' => $user->balance_trader + $creditLineVerification->amount
                ]);
                event(new TplUserEvent(
                    $user,
                    NotifyText::find(9)
                ));
            } else {
                event(new TplUserEvent(
                    $user,
                    NotifyText::find(10)
                ));
            }
        });
    }

    public function depositLineVerifySend(VerifyDepositLineRequest $request, User $user): void
    {
        \DB::transaction(function () use ($user, $request) {
            $userDepositLineVerification = $user->depositLineVerifications()->create($request->all());
        });
    }

    public function depositLineVerify(VerifyDepositLineManagerRequest $request, DepositLineVerification $depositLineVerification): void
    {
        \DB::transaction(function () use ($request, $depositLineVerification) {
            $depositLineVerification->update([
                'manager_id' => $request->user()->id,
                'status' => $request->get('status'),
                'comment' => $request->get('comment')
            ]);
            if ($depositLineVerification->isApprove()) {
                $user = $depositLineVerification->user;
                $user->update(['balance_investor' => $user->balance_investor + $depositLineVerification->amount]);
            }
            //event(new VerifyUserEvent($creditLevelVerification, $creditLevelVerification->user));
        });
    }

    public function sendPromo(PromoRequest $request, User $user): void
    {
        StatisticPromoCode::create([
            'user_id' => $user->id,
            'email' => $request->get('email')
        ]);
        event(new PromoEvent($user, $request->get('email')));
    }


    public function getSmsCode(User $user, $type): void
    {
        if (empty($user->phone)) {
            throw new \DomainException(trans('site.cabinet.phone-empty'));
        }

        $codeVerifiedNew = $user->codeVerifies()->create([
            'type' => $type,
            'verify_token' => (string)random_int(1000000, 9999999),
            'verify_token_expire' => Carbon::now()->copy()->addSeconds(180)
        ]);

        $this->sms->send($user->phone, trans('site.cabinet.Verification-code') . ': ' . $codeVerifiedNew->verify_token);
    }
}
