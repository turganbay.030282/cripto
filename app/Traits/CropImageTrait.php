<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

trait CropImageTrait
{
    public function addImage($photo, $crop, $attribute=null, $folder=null)
    {
        if(!$photo) return;

        $attr = $attribute ? $attribute : 'photo';
        $folder = $folder ? $folder : $this->table;

        $this->uploadImage($photo, $crop, $attr, $folder);
    }

    public function editImage($photo, $crop, $attribute=null, $folder=null)
    {
        if(!$photo) return;

        $attr = $attribute ? $attribute : 'photo';
        $folder = $folder ? $folder : $this->table;

        if(is_file(storage_path() . "/app/public/{$folder}/crop-{$this->$attr}"))
            unlink(storage_path() . "/app/public/{$folder}/crop-{$this->$attr}");

        if(is_file(storage_path() . "/app/public/{$folder}/{$this->$attr}"))
            unlink(storage_path() . "/app/public/{$folder}/{$this->$attr}");

        $this->uploadImage($photo, $crop, $attr, $folder);
    }

    public function uploadImage(UploadedFile $photo, $crop, $attr, $folder)
    {
        $extension = $photo->extension();
        if($photo->getClientOriginalExtension() == 'svg')
            $extension = 'svg';

        $filename = uniqid() . "." . $extension;

        if($photo->storeAs($folder . '/', $filename)){
            $this->update([$attr => $filename]);
            if(!empty($crop)){
                Image::make(file_get_contents($crop))->save( storage_path() . "/app/public/{$folder}/crop-{$filename}");
            }
        }
    }

    public function removeImage($attribute=null, $folder=null)
    {
        $attr = $attribute ? $attribute : 'photo';
        $folder = $folder ? $folder : $this->table;

        if(is_file(storage_path() . "/app/public/{$folder}/crop-{$this->$attr}"))
            unlink(storage_path() . "/app/public/{$folder}/crop-{$this->$attr}");

        if(is_file(storage_path() . "/app/public/{$folder}/{$this->$attr}"))
            unlink(storage_path() . "/app/public/{$folder}/{$this->$attr}");
    }

    public function getPhotoUrl($attribute=null, $folder=null)
    {
        $attr = $attribute ? $attribute : 'photo';
        if(!empty($this->$attr)){
            $folder = $folder ? $folder : $this->table;

            return url("/storage/{$folder}/{$this->$attr}");
        }

        if($attr == 'cover')
            return url("/front/images/intro-bg.png");

        return "";
    }

    public function getCropPhotoUrl($attribute=null, $folder=null)
    {
        $attr = $attribute ? $attribute : 'photo';
        if(!empty($this->$attr)){
            $folder = $folder ? $folder : $this->table;

            return url("/storage/{$folder}/crop-{$this->$attr}");
        }
        return "";
    }

}
