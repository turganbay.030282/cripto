<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

trait FileTrait
{
    public function addFile($file, $attribute = 'file', $folder = null)
    {
        if (!$file) return;
        $folder = $folder ? $folder : $this->table;
        $this->uploadFile($file, $attribute, $folder);
    }

    public function editFile($file, $attribute = 'file', $folder = null)
    {
        if (!$file) return;
        $folder = $folder ? $folder : $this->table;

        if (is_file(storage_path() . "/app/public/{$folder}/{$this->$attribute}"))
            unlink(storage_path() . "/app/public/{$folder}/{$this->$attribute}");

        $this->uploadFile($file, $attribute, $folder);
    }

    public function uploadFile(UploadedFile $file, $attr, $folder)
    {
        $filename = $file->getClientOriginalName();
        if ($file->storeAs($folder . '/', $filename)) {
            $this->update([$attr => $filename]);
        }
    }

    public function removeFile($attribute = null, $folder = null)
    {
        $folder = $folder ? $folder : $this->table;

        if (is_file(storage_path() . "/app/public/{$folder}/{$this->$attribute}"))
            unlink(storage_path() . "/app/public/{$folder}/{$this->$attribute}");
    }

    public function getFileUrl($attribute = 'file', $folder = null)
    {
        if (!empty($attribute)) {
            $folder = $folder ? $folder : $this->table;
            return url("/storage/{$folder}/{$this->$attribute}");
        }
        return "";
    }

}
