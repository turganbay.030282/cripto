<?php

namespace App\Traits;

use App\Helpers\ArrayHelper;
use Illuminate\Database\Eloquent\Builder;

trait Role
{

    public static function listRoleUser(): array
    {
        return [
            self::ROLE_ADMIN => 'Администратор',
            self::ROLE_USER => 'Пользователь',
        ];
    }

    public function isAdmin(): bool
    {
        return $this->role === self::ROLE_ADMIN;
    }

    public function isUser(): bool
    {
        return $this->role === self::ROLE_USER;
    }

    public function getRoleNameAttribute()
    {
        return ArrayHelper::getValue(self::listRoleUser(), $this->role);
    }

    public function scopeAdmin(Builder $query)
    {
        return $query->where('role', self::ROLE_ADMIN);
    }

    public function scopeUser(Builder $query)
    {
        return $query->where('role', self::ROLE_USER);
    }

}
