<?php


namespace App\Traits;

trait Seo
{

    public function getSeoTitle(): string
    {
        return $this->translate(app()->getLocale())->meta_title ?: $this->translate(app()->getLocale())->title;
    }

}
