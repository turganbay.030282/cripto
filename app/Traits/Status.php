<?php


namespace App\Traits;

use App\Helpers\ArrayHelper;
use App\Helpers\PublishHelper;
use Illuminate\Database\Eloquent\Builder;

trait Status
{

    public function isPublish(): bool
    {
        return $this->publish == PublishHelper::publish_on;
    }

    public static function statusList()
    {
        return [
            self::publish_off => 'Нет',
            self::publish_on => 'Да',
        ];
    }

    public function scopePublish(Builder $query)
    {
        return $query->where('publish', PublishHelper::publish_on);
    }

    public function getStatusNameAttribute()
    {
        return ArrayHelper::getValue(PublishHelper::listStatus(), $this->publish);
    }

}
