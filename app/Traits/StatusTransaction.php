<?php

namespace App\Traits;

use App\Helpers\ArrayHelper;
use App\Helpers\StatusHelper;
use Illuminate\Database\Eloquent\Builder;

trait StatusTransaction
{
    public static function listStatus(): array
    {
        return [
            StatusHelper::STATUS_NEW => 'не проверен',
            StatusHelper::STATUS_APPROVE => 'проверен',
        ];
    }
    public static function listAllStatus(): array
    {
        return [
            StatusHelper::STATUS_NEW => 'не проверен',
            StatusHelper::STATUS_APPROVE => 'проверен',
            StatusHelper::STATUS_REJECT => 'отклонен',
            StatusHelper::STATUS_WAIT_CLOSE => 'не проверен',
            StatusHelper::STATUS_CLOSE => 'закрыт',
        ];
    }

    public function isNew(): bool
    {
        return $this->status == StatusHelper::STATUS_NEW;
    }

    public function isApprove(): bool
    {
        return $this->status == StatusHelper::STATUS_APPROVE;
    }

    public function isReject(): bool
    {
        return $this->status == StatusHelper::STATUS_REJECT;
    }

    public function isWaitClose(): bool
    {
        return $this->status == StatusHelper::STATUS_WAIT_CLOSE;
    }

    public function isClose(): bool
    {
        return $this->status == StatusHelper::STATUS_CLOSE;
    }

    public function isActive(): bool
    {
        return $this->status == StatusHelper::STATUS_NEW || $this->status == StatusHelper::STATUS_APPROVE;
    }

    public function getStatusNameAttribute()
    {
        return ArrayHelper::getValue(self::listAllStatus(), $this->status);
    }

    public function getStatusNameHtmlAttribute()
    {
        if($this->isApprove()){
            return '<span class="label label-success">'.ArrayHelper::getValue(self::listAllStatus(), $this->status).'</span>';
        }
        if($this->isReject()){
            return '<span class="label label-danger">'.ArrayHelper::getValue(self::listAllStatus(), $this->status).'</span>';
        }
        if($this->isClose()){
            return '<span class="label label-primary">'.ArrayHelper::getValue(self::listAllStatus(), $this->status).'</span>';
        }
        return '<span class="label label-default">'.ArrayHelper::getValue(self::listAllStatus(), $this->status).'</span>';
    }

    public function scopeNew(Builder $query)
    {
        return $query->where('status', StatusHelper::STATUS_NEW);
    }

    public function scopeApprove(Builder $query)
    {
        return $query->where('status', StatusHelper::STATUS_APPROVE);
    }

    public function scopeClose(Builder $query)
    {
        return $query->where('status', StatusHelper::STATUS_CLOSE);
    }

    public function scopeWaitClose(Builder $query)
    {
        return $query->where('status', StatusHelper::STATUS_WAIT_CLOSE);
    }

    public function scopeActive(Builder $query)
    {
        return $query->where('status', '<>', StatusHelper::STATUS_CLOSE);
    }

    public function scopeNotApprove(Builder $query)
    {
        return $query->where('status', '<>', StatusHelper::STATUS_APPROVE);
    }
}
