<?php


namespace App\Traits;


use App\Helpers\ArrayHelper;
use Illuminate\Database\Eloquent\Builder;

trait StatusUser
{

    public static function listStatus(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_WAIT => 'Не подтвержден',
            self::STATUS_BLOCK => 'Приостановлен',
        ];
    }

    public function isActive(): bool
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isBlock(): bool
    {
        return $this->status == self::STATUS_BLOCK;
    }

    public function isWait(): bool
    {
        return $this->status == self::STATUS_WAIT;
    }

    public function getStatusNameAttribute()
    {
        return ArrayHelper::getValue(self::listStatus(), $this->status);
    }

    public function scopeActive(Builder $query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeWait(Builder $query)
    {
        return $query->where('status', self::STATUS_WAIT);
    }

    public function scopeBlock(Builder $query)
    {
        return $query->where('status', self::STATUS_BLOCK);
    }

}
