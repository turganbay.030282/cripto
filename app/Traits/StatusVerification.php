<?php

namespace App\Traits;

use App\Helpers\ArrayHelper;
use Illuminate\Database\Eloquent\Builder;

trait StatusVerification
{

    public static function listStatus()
    {
        return [
            self::STATUS_WAIT => 'На проверке',
            self::STATUS_APPROVED => 'Верифицирован',
            self::STATUS_REJECT => 'Отклонен',
        ];
    }

    public function isWait()
    {
        return $this->status == self::STATUS_WAIT;
    }

    public function isReject()
    {
        return $this->status == self::STATUS_REJECT;
    }

    public function isApprove()
    {
        return $this->status == self::STATUS_APPROVED;
    }

    public function getStatusNameAttribute()
    {
        return ArrayHelper::getValue(self::listStatus(), $this->status);
    }

    public function scopeReject(Builder $query)
    {
        return $query->where('status', self::STATUS_REJECT);
    }

    public function scopeVerify(Builder $query)
    {
        return $query->where('status', self::STATUS_APPROVED);
    }

    public function scopeWait(Builder $query)
    {
        return $query->where('status', self::STATUS_WAIT);
    }

    public function scopeHistory(Builder $query)
    {
        return $query->where('status', '<>', self::STATUS_WAIT);
    }

}
