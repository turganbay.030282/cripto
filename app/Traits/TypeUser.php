<?php


namespace App\Traits;

use App\Helpers\ArrayHelper;
use App\Helpers\TypeUserHelper;
use Illuminate\Database\Eloquent\Builder;

trait TypeUser
{
    public function isTrader(): bool
    {
        return $this->type == TypeUserHelper::TYPE_USER_TRADER;
    }

    public function isInvestor(): bool
    {
        return $this->type == TypeUserHelper::TYPE_USER_INVESTOR;
    }

    public static function typeUserList()
    {
        return [
            TypeUserHelper::TYPE_USER_TRADER => 'Трейдер',
            TypeUserHelper::TYPE_USER_INVESTOR => 'Инвестор',
        ];
    }

    public function getTypeNameAttribute()
    {
        return ArrayHelper::getValue(TypeUserHelper::typeUserList(), $this->type);
    }

    public function scopeTrader(Builder $query)
    {
        return $query->where('type', TypeUserHelper::TYPE_USER_TRADER);
    }

    public function scopeInvestor(Builder $query)
    {
        return $query->where('type', TypeUserHelper::TYPE_USER_INVESTOR);
    }
}
