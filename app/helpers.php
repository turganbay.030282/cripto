<?php

if (!function_exists('trans_field')) {
    function trans_field($model, $field)
    {
        $trans = $model->translate(app()->getLocale())->$field;
        if ($trans) {
            return $trans;
        }
        return $model->$field;
    }
}

if (!function_exists('numeric_fmt')) {
    function numeric_fmt($amount)
    {
        return number_format($amount,8,'.', '');
    }
}


if (!function_exists('format_number')) {
    function format_number($amount)
    {
        return \App\Helpers\NumberHelper::format_number($amount);
    }
}


if (!function_exists('production')) {
    function production()
    {
        return config('app.env') === 'production';
    }
}


function conclusion(float $course, float $price, float $settingConversionFee)
{
    $amount = $course * $price;
    return $amount - percent($course, $price, $settingConversionFee);
}
function percent(float $course, float $price, float $settingConversionFee)
{
    $amount = $course * $price;
    return $amount * $settingConversionFee / 100;
}

