<?php

return [
    'api_timeout' => 3,

    'coin_market_cap_api' => [
        'base_url' => env('COIN_MARKET_CAP_API_BASE_URL', 'https://sandbox-api.coinmarketcap.com/'),
        'version' => env('COIN_MARKET_CAP_VERSION', 'v1'),
        'key' => env('COIN_MARKET_CAP_API_KEY', 'b54bcf4d-1bca-4e8e-9a24-22ff2c3d462c'),
        'cache_time' => env('COIN_MARKET_CAP_API_CACHE_TIME', 600),
    ],

    'get_id_api' => [
        'base_url' => env('GET_ID_API_BASE_URL', 'https://spxon.sb.getid.dev/api'),//https://spxon.getid.dev/api
        'base_url_sdk' => env('GET_ID_API_BASE_URL_SDK', 'https://assetshub.sb.getid.dev'),
        'version' => env('GET_ID_VERSION', 'v1'),
        'key' => env('GET_ID_API_KEY', '1231223'),
        'key-sdk' => env('GET_ID_API_KEY_SDK', '1231223'),
        'cache_time' => env('GET_ID_API_CACHE_TIME', 600),
    ],

];
