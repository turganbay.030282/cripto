<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('phone')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('email_verify_token')->nullable();
            $table->string('photo')->nullable();
            $table->string('position')->nullable();
            $table->string('password');
            $table->string('role')->default(\App\Entity\User::ROLE_USER);
            $table->string('status')->default(\App\Entity\User::STATUS_WAIT);
            $table->rememberToken();
            $table->timestamp('last_login_at')->nullable();
            $table->double('balance_trader',10,2)->default(0);
            $table->double('balance_investor',10,2)->default(0);
            $table->boolean('is_investor')->default(false);
            $table->boolean('is_verification')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
