<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('crop_blog_width');
            $table->integer('crop_blog_height');
            $table->integer('crop_ava_width');
            $table->integer('crop_ava_height');
            $table->integer('crop_cert_width');
            $table->integer('crop_cert_height');
            $table->string('contact_phone')->nullable()->default('+371 22 18 77 83');
            $table->string('contact_email')->nullable()->default('support@assetscore.io');
            $table->string('soc_fb')->nullable()->default('#');
            $table->string('soc_tw')->nullable()->default('#');
            $table->string('soc_in')->nullable()->default('#');
            $table->integer('value_gold')->default(1700);
            $table->integer('value_btc')->default(9200);
        });
        Schema::create('setting_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('setting_id');
            $table->foreign('setting_id')->references('id')->on('settings')->onDelete('cascade');
            $table->string('locale')->index();
            $table->string('contact_address')->nullable()->default('Kr, Barona 45 - 512, Rīga, Latvia');
            $table->string('req_sia')->nullable()->default('SIA Crypto Credit Hub');
            $table->string('req_reg')->nullable()->default('Рег. № НДС LV40003272854');
            $table->string('req_seb')->nullable()->default('SEB Banka AS: LV55UNLA005001478945');
            $table->string('req_swift')->nullable()->default('Swift UNLALV2X');
            $table->text('footer_text_1')->nullable();
            $table->text('footer_text_2')->nullable();
            $table->unique(['setting_id', 'locale']);
        });

        DB::table('settings')->insert([
            'id' => 1,
            'crop_blog_width' => 246,
            'crop_blog_height' => 158,
            'crop_ava_width' => 100,
            'crop_ava_height' => 100,
            'crop_cert_width' => 373,
            'crop_cert_height' => 288,
        ]);
        DB::table('setting_translations')->insert([
            'setting_id' => 1,
            'locale' => 'ru',
            'footer_text_1' => 'Asset Credit Hub - дает клиентам инструмент, которым миллиардеры обладали в течении десятилетий.'
        ]);
        DB::table('setting_translations')->insert([
            'setting_id' => 1,
            'locale' => 'en',
            'footer_text_1' => 'Asset Credit Hub - дает клиентам инструмент, которым миллиардеры обладали в течении десятилетий.'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_translations');
        Schema::dropIfExists('settings');
    }
}
