<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_categories', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->unique();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('blog_category_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('blog_category_id');
            $table->foreign('blog_category_id')->references('id')->on('blog_categories')->onDelete('cascade');
            $table->string('locale')->index();
            $table->string('title')->nullable();
            $table->unique(['blog_category_id', 'locale']);
        });

        Schema::create('blog_items', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->unique();
            $table->string('photo')->nullable();
            $table->integer('publish')->default(\App\Helpers\PublishHelper::publish_off);
            $table->integer('count_view')->default(0);
            $table->integer('count_like')->default(0);
            $table->unsignedBigInteger('author_id')->nullable();
            $table->foreign('author_id')
                ->references('id')
                ->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('blog_item_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('blog_item_id');
            $table->foreign('blog_item_id')->references('id')->on('blog_items')->onDelete('cascade');
            $table->string('locale')->index();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->longText('content')->nullable();
            $table->string('meta_title')->nullable();
            $table->text('meta_desc')->nullable();
            $table->unique(['blog_item_id', 'locale']);
        });

        Schema::create('blog_categories_items', function (Blueprint $table) {
            $table->unsignedBigInteger('blog_category_id')->index();
            $table->foreign('blog_category_id')
                ->references('id')
                ->on('blog_categories');

            $table->unsignedBigInteger('blog_item_id')->index();
            $table->foreign('blog_item_id')
                ->references('id')
                ->on('blog_items');

            $table->primary(['blog_category_id', 'blog_item_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_categories_items');
        Schema::dropIfExists('blog_item_translations');
        Schema::dropIfExists('blog_items');
        Schema::dropIfExists('blog_category_translations');
        Schema::dropIfExists('blog_categories');
    }
}
