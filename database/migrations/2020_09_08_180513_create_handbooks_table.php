<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHandbooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_levels', function (Blueprint $table) {
            $table->id();
            $table->integer('range_from');
            $table->integer('range_to');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('credit_level_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('credit_level_id');
            $table->foreign('credit_level_id','fxk-credit_levels-credit_level_id')->references('id')->on('credit_levels')->onDelete('cascade');
            $table->string('locale')->index();
            $table->string('name')->nullable();
            $table->unique(['credit_level_id', 'locale'],'unique-credit_level-locale');
        });

        Schema::create('investor_ranks', function (Blueprint $table) {
            $table->id();
            $table->integer('range_from');
            $table->integer('range_to');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('investor_rank_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('investor_rank_id');
            $table->foreign('investor_rank_id','fxk-rank_translation-rank_id')->references('id')->on('investor_ranks')->onDelete('cascade');
            $table->string('locale')->index();
            $table->string('name')->nullable();
            $table->unique(['investor_rank_id', 'locale'],'unique-investor_rank-locale');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investor_rank_translations');
        Schema::dropIfExists('investor_ranks');
        Schema::dropIfExists('credit_level_translations');
        Schema::dropIfExists('credit_levels');
    }
}
