<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLevelRankVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_level_verifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('manager_id')->unsigned()->nullable();
            $table->foreign('manager_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');

            $table->integer('amount')->default(0);

            $table->string('status')->default(\App\Entity\UserVerification::STATUS_WAIT);
            $table->text('comment')->nullable();
            $table->timestamps();
        });
        Schema::create('investor_rank_verifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('manager_id')->unsigned()->nullable();
            $table->foreign('manager_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');

            $table->integer('amount')->default(0);

            $table->string('status')->default(\App\Entity\UserVerification::STATUS_WAIT);
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_level_verifications');
        Schema::dropIfExists('investor_rank_verifications');
    }
}
