<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreditLevelToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('credit_level_id')->unsigned()->nullable();
            $table->foreign('credit_level_id')
                ->references('id')
                ->on('credit_levels')
                ->onDelete('SET NULL');

            $table->unsignedBigInteger('investor_rank_id')->unsigned()->nullable();
            $table->foreign('investor_rank_id')
                ->references('id')
                ->on('investor_ranks')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('credit_level_id');
            $table->dropColumn('investor_rank_id');
        });
    }
}
