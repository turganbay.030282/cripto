<?php

use App\Entity\Mail\AdminMailing;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminMailingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_mailings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('admin_id')->unsigned()->nullable();
            $table->foreign('admin_id')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL');

            $table->string('list_user')->default(AdminMailing::LIST_ALL);
            $table->string('subject');
            $table->longText('body');
            $table->timestamps();
        });

        Schema::create('admin_mailing_users', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
            $table->unsignedBigInteger('admin_mailing_id')->unsigned();
            $table->foreign('admin_mailing_id')
                ->references('id')
                ->on('admin_mailings')
                ->onDelete('CASCADE');
            $table->primary(['user_id', 'admin_mailing_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_mailing_users');
        Schema::dropIfExists('admin_mailings');
    }
}
