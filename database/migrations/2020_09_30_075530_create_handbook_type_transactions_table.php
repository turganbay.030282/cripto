<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHandbookTypeTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->decimal('value');
            $table->integer('period')->nullable();
            $table->integer('min')->nullable();
            $table->integer('max')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('type_transaction_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('type_transaction_id');
            $table->foreign('type_transaction_id','fxk-type_trans_id-type_tran')->references('id')->on('type_transactions')->onDelete('cascade');
            $table->string('locale')->index();
            $table->string('name')->nullable();
            $table->unique(['type_transaction_id', 'locale'],'unique-type_trans-locale');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_transaction_translations');
        Schema::dropIfExists('type_transactions');
    }
}
