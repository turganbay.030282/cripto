<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlockPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_packages', function (Blueprint $table) {
            $table->id();
            $table->string('photo')->nullable();
            $table->integer('type')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('block_package_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('block_package_id');
            $table->foreign('block_package_id')->references('id')->on('block_packages')->onDelete('cascade');
            $table->string('locale')->index();
            $table->string('name')->nullable();
            $table->string('price')->nullable();
            $table->string('desc')->nullable();
            $table->unique(['block_package_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_packages');
    }
}
