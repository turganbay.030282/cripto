<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_main', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        });
        Schema::create('page_main_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('page_main_id')->nullable();
            $table->foreign('page_main_id')->references('id')->on('page_main')->onDelete('cascade');
            $table->string('locale')->index();
            $table->string('title');
            $table->string('title_offer_tr');
            $table->string('title_offer_inv');
            $table->string('desc_offer_tr');
            $table->string('desc_offer_inv');
            $table->string('offer_tr_title');
            $table->string('offer_tr_sub_title');
            $table->text('offer_tr_desc');
            $table->string('offer_inv_title');
            $table->string('offer_inv_sub_title');
            $table->text('offer_inv_desc');
            $table->string('offer_tr_btn');
            $table->string('offer_inv_btn');
            $table->unique(['page_main_id', 'locale']);
        });
        DB::table('page_main')->insert([
            'id' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('page_main_translations')->insert([
            'page_main_id' => 1,
            'locale' => 'ru',
            'title' => 'Возможность заниматься инвестициями, не имея собственного капитала',
            'title_offer_tr' => 'Оффер для Трейдера',
            'title_offer_inv' => 'Оффер для Инвестора',
            'desc_offer_tr' => 'Успех от трейдинга за деньги инвестора',
            'desc_offer_inv' => 'Стабильный доход для крупного инвестора',
            'offer_tr_title' => 'Наше предложение для оффера',
            'offer_tr_sub_title' => 'Желаете заняться инвестициями, но нет капитала?',
            'offer_tr_desc' => '<p>Желаете заняться инвестициями, но нет капитала? Воспользуйтесь нашим!</p><p>Первым 100 верифицированным пользователям мы предлагаем получить быструю кредитную линию в 500 Евро для покупки ктивов!</p>',
            'offer_inv_title' => 'Наше предложение для инвестора',
            'offer_inv_sub_title' => 'У вас есть деньги, но ставки по депозитам вас не устраивают?',
            'offer_inv_desc' => '<p>У вас есть свободные деньги, но ставки по депозитам в банках вас не устраивают? Взгляните на наши</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin semper, justo a feugiat feugiat, felis felis porta augue, in vestibulum sapien ante.</p>',
            'offer_tr_btn' => 'Как это работает',
            'offer_inv_btn' => 'Ставки по депозитам',
        ]);
        DB::table('page_main_translations')->insert([
            'page_main_id' => 1,
            'locale' => 'en',
            'title' => 'Возможность заниматься инвестициями, не имея собственного капитала',
            'title_offer_tr' => 'Оффер для Трейдера',
            'title_offer_inv' => 'Оффер для Инвестора',
            'desc_offer_tr' => 'Успех от трейдинга за деньги инвестора',
            'desc_offer_inv' => 'Стабильный доход для крупного инвестора',
            'offer_tr_title' => 'Наше предложение для оффера',
            'offer_tr_sub_title' => 'Желаете заняться инвестициями, но нет капитала?',
            'offer_tr_desc' => '<p>Желаете заняться инвестициями, но нет капитала? Воспользуйтесь нашим!</p><p>Первым 100 верифицированным пользователям мы предлагаем получить быструю кредитную линию в 500 Евро для покупки ктивов!</p>',
            'offer_inv_title' => 'Наше предложение для инвестора',
            'offer_inv_sub_title' => 'У вас есть деньги, но ставки по депозитам вас не устраивают?',
            'offer_inv_desc' => '<p>У вас есть свободные деньги, но ставки по депозитам в банках вас не устраивают? Взгляните на наши</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin semper, justo a feugiat feugiat, felis felis porta augue, in vestibulum sapien ante.</p>',
            'offer_tr_btn' => 'Как это работает',
            'offer_inv_btn' => 'Ставки по депозитам',
        ]);

        Schema::create('page_investor', function (Blueprint $table) {
            $table->id();
            $table->string('about_b_icon');
            $table->string('about_b_icon2');
            $table->string('about_b_icon3');
            $table->timestamps();
        });
        Schema::create('page_investor_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('page_investor_id')->nullable();
            $table->foreign('page_investor_id')->references('id')->on('page_investor')->onDelete('cascade');
            $table->string('locale')->index();
            $table->string('title');
            $table->text('desc');
            $table->string('btn_text');
            $table->string('about_title');
            $table->string('about_sub_title');
            $table->string('about_b_title');
            $table->string('about_b_desc');
            $table->string('about_b_title2');
            $table->string('about_b_desc2');
            $table->string('about_b_title3');
            $table->string('about_b_desc3');
            $table->string('benefit_title');
            $table->string('benefit_b_title');
            $table->string('benefit_b_desc');
            $table->string('benefit_b_title2');
            $table->string('benefit_b_desc2');
            $table->string('benefit_b_title3');
            $table->string('benefit_b_desc3');
            $table->string('benefit_b_title4');
            $table->string('benefit_b_desc4');
            $table->string('benefit_b_title5');
            $table->string('benefit_b_desc5');
            $table->string('benefit_b_title6');
            $table->string('benefit_b_desc6');
            $table->unique(['page_investor_id', 'locale']);
        });

        DB::table('page_investor')->insert([
            'id' => 1,
            'about_b_icon' => 'Profit-1.png',
            'about_b_icon2' => 'Profit-2.png',
            'about_b_icon3' => 'Profit-3.png',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('page_investor_translations')->insert([
            'page_investor_id' => 1,
            'locale' => 'ru',
            'title' => 'Стать инвестором в компании CCH',
            'desc' => 'Ищете, куда надёжно вложить свободные деньги, а банковские ставки по депозитам не покрывают даже инфляцию? Наше решение – депозитарный вклад в инвестиционный фонд Asset Crypto Hub, обеспечиваемый реальными активами!',
            'btn_text' => 'Ставки по депозитам',
            'about_title' => 'Информация о нас',
            'about_sub_title' => 'Как начать получать прибыль',
            'about_b_title' => 'Открыть депозит',
            'about_b_desc' => 'Пополни баланс вашего личного счета, выбери удобный для себя депозит.',
            'about_b_title2' => 'Получи начисления',
            'about_b_desc2' => 'Следи за начислениями по открытым депозитам в личном кабинете.',
            'about_b_title3' => 'Выводи прибыль',
            'about_b_desc3' => 'Автоматически выводи прибыль удобным для себя способом на свой кошелек.',
            'benefit_title' => 'Преимущества работы с нами',
            'benefit_b_title' => 'Выбор инвестиций',
            'benefit_b_desc' => 'Инвестор выбирает удобную инвестицию.',
            'benefit_b_title2' => 'Надежные инвестиции',
            'benefit_b_desc2' => 'Никаких рисков, только прибыль с выводом на кошелек в любое время.',
            'benefit_b_title3' => 'Перевод средств',
            'benefit_b_desc3' => 'Все на определенных условиях согласно договору.',
            'benefit_b_title4' => 'Плата за проценты',
            'benefit_b_desc4' => 'Проценты выплачиваются в полном объеме в соответствии с договором.',
            'benefit_b_title5' => 'Перевод средств',
            'benefit_b_desc5' => 'Средства распределяются между каждым инвестором и перечисляются на расчетные счета.',
            'benefit_b_title6' => 'Прибыль',
            'benefit_b_desc6' => 'Инвестору выплачиваются проценты в соответствии с процентными ставками и условиями вкладов.',
        ]);
        DB::table('page_investor_translations')->insert([
            'page_investor_id' => 1,
            'locale' => 'en',
            'title' => 'Стать инвестором в компании CCH',
            'desc' => 'Ищете, куда надёжно вложить свободные деньги, а банковские ставки по депозитам не покрывают даже инфляцию? Наше решение – депозитарный вклад в инвестиционный фонд Asset Crypto Hub, обеспечиваемый реальными активами!',
            'btn_text' => 'Ставки по депозитам',
            'about_title' => 'Информация о нас',
            'about_sub_title' => 'Как начать получать прибыль',
            'about_b_title' => 'Открыть депозит',
            'about_b_desc' => 'Пополни баланс вашего личного счета, выбери удобный для себя депозит.',
            'about_b_title2' => 'Получи начисления',
            'about_b_desc2' => 'Следи за начислениями по открытым депозитам в личном кабинете.',
            'about_b_title3' => 'Выводи прибыль',
            'about_b_desc3' => 'Автоматически выводи прибыль удобным для себя способом на свой кошелек.',
            'benefit_title' => 'Преимущества работы с нами',
            'benefit_b_title' => 'Выбор инвестиций',
            'benefit_b_desc' => 'Инвестор выбирает удобную инвестицию.',
            'benefit_b_title2' => 'Надежные инвестиции',
            'benefit_b_desc2' => 'Никаких рисков, только прибыль с выводом на кошелек в любое время.',
            'benefit_b_title3' => 'Перевод средств',
            'benefit_b_desc3' => 'Все на определенных условиях согласно договору.',
            'benefit_b_title4' => 'Плата за проценты',
            'benefit_b_desc4' => 'Проценты выплачиваются в полном объеме в соответствии с договором.',
            'benefit_b_title5' => 'Перевод средств',
            'benefit_b_desc5' => 'Средства распределяются между каждым инвестором и перечисляются на расчетные счета.',
            'benefit_b_title6' => 'Прибыль',
            'benefit_b_desc6' => 'Инвестору выплачиваются проценты в соответствии с процентными ставками и условиями вкладов.',
        ]);

        Schema::create('page_about', function (Blueprint $table) {
            $table->id();
            $table->string('benefit_b_icon');
            $table->string('benefit_b_icon2');
            $table->string('benefit_b_icon3');
            $table->string('benefit_b_icon4');
            $table->string('benefit_b_icon5');
            $table->string('benefit_b_icon6');
            $table->timestamps();
        });
        Schema::create('page_about_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('page_about_id')->nullable();
            $table->foreign('page_about_id')->references('id')->on('page_about')->onDelete('cascade');
            $table->string('locale')->index();
            $table->string('title');
            $table->text('desc');
            $table->string('btn_text');
            $table->string('benefit_title');
            $table->string('benefit_sub_title');
            $table->string('benefit_b_title');
            $table->string('benefit_b_desc');
            $table->string('benefit_b_title2');
            $table->string('benefit_b_desc2');
            $table->string('benefit_b_title3');
            $table->string('benefit_b_desc3');
            $table->string('benefit_b_title4');
            $table->string('benefit_b_desc4');
            $table->string('benefit_b_title5');
            $table->string('benefit_b_desc5');
            $table->string('benefit_b_title6');
            $table->string('benefit_b_desc6');
            $table->string('priority_title');
            $table->string('priority_sub_title');
            $table->string('priority_b_title');
            $table->text('priority_b_desc');
            $table->string('priority_b_title2');
            $table->text('priority_b_desc2');
            $table->string('priority_b_title3');
            $table->text('priority_b_desc3');
            $table->string('priority_b_title4');
            $table->text('priority_b_desc4');
            $table->unique(['page_about_id', 'locale']);
        });
        DB::table('page_about')->insert([
            'id' => 1,
            'benefit_b_icon' => 'ListAbout-1.png',
            'benefit_b_icon2' => 'ListAbout-2.png',
            'benefit_b_icon3' => 'ListAbout-3.png',
            'benefit_b_icon4' => 'ListAbout-4.png',
            'benefit_b_icon5' => 'ListAbout-5.png',
            'benefit_b_icon6' => 'ListAbout-6.png',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('page_about_translations')->insert([
            'page_about_id' => 1,
            'locale' => 'ru',
            'title' => 'Инвестируй вместе с Crypto Credit Hub',
            'desc' => 'Мы опытная команда по анализу рынка и всех сегментов. Мы анализируем сотни стартапов и инвестируем в самые прибыльные, наша команда быстро растет и стремительно охватывает большую часть стартап-рынка.',
            'btn_text' => 'Начать зарабатывать',
            'benefit_title' => 'Почему стоит работать с нами?',
            'benefit_sub_title' => 'Преимущества работы с нами',
            'benefit_b_title' => 'Безопасность',
            'benefit_b_desc' => 'Максимальная безопасность ваших персональных данных, платежей и данных ваших партнеров',
            'benefit_b_title2' => 'Никакого риска',
            'benefit_b_desc2' => 'Все вклады наших инвесторов диверсифицированы, что позволяет нам полностью исключить риски.',
            'benefit_b_title3' => 'Стабильность',
            'benefit_b_desc3' => 'Возможность получения стабильного дохода, который превышает процентную ставку по депозитам',
            'benefit_b_title4' => 'Поддержка',
            'benefit_b_desc4' => 'У нас работают лучшие специалисты, которые готовы ответить на любые ваши вопросы 24/7',
            'benefit_b_title5' => 'Легальность',
            'benefit_b_desc5' => 'Мы ведем абсолютно легальную и открытую деятельность',
            'benefit_b_title6' => 'Уникальность',
            'benefit_b_desc6' => 'Наша платформа одна из лучших инвестиционных инструментов',
            'priority_title' => 'Открытия',
            'priority_sub_title' => 'Наши приоритеты',
            'priority_b_title' => 'Быть самыми надежными',
            'priority_b_desc' => '<p>Мы фильтруем каждое решение. Доверие - это взаимодействия клиентов с нами как с компанией. Что требует от нас быть мировым классом в области безопасности, соответствия требованиям, технологии, поддержки клиентов, дизайна и многого другого.</p>',
            'priority_b_title2' => 'Быть простыми в использовании',
            'priority_b_desc2' => '<p>Цифровая валюта - это мощная технология. Crypto Credit Hub - создали замечательных продукт, который приносит всем преимущества.</p>',
            'priority_b_title3' => 'Отличная коммуникация',
            'priority_b_desc3' => '<ul><li>Мы эффективно обмениваемся информацией, улучшая сотрудничество и производительность.</li><li>Мы лаконичны, откровенны и доброжелательны.</li><li>Мы практикуемся в активном и внимательном слушание.</li><li>Мы говорим с людьми напрямую о ваших проблемах.</li></ul>',
            'priority_b_title4' => 'Эффективное выполнение',
            'priority_b_desc4' => '<ul><li>Мы выполняем быстро высококачественную работу, работая умнее, а не сложнее.</li><li>Мы ценим выполнение задач, а не просто говорим о них.</li><li>Мы предпочитаем автоматизацию ручной работе.</li><li>Мы расставляем приоритеты, сосредоточившись на 20%, которые принесут нам 80% отдачи.</li></ul>',
        ]);
        DB::table('page_about_translations')->insert([
            'page_about_id' => 1,
            'locale' => 'en',
            'title' => 'Инвестируй вместе с Crypto Credit Hub',
            'desc' => 'Мы опытная команда по анализу рынка и всех сегментов. Мы анализируем сотни стартапов и инвестируем в самые прибыльные, наша команда быстро растет и стремительно охватывает большую часть стартап-рынка.',
            'btn_text' => 'Начать зарабатывать',
            'benefit_title' => 'Почему стоит работать с нами?',
            'benefit_sub_title' => 'Преимущества работы с нами',
            'benefit_b_title' => 'Безопасность',
            'benefit_b_desc' => 'Максимальная безопасность ваших персональных данных, платежей и данных ваших партнеров',
            'benefit_b_title2' => 'Никакого риска',
            'benefit_b_desc2' => 'Все вклады наших инвесторов диверсифицированы, что позволяет нам полностью исключить риски.',
            'benefit_b_title3' => 'Стабильность',
            'benefit_b_desc3' => 'Возможность получения стабильного дохода, который превышает процентную ставку по депозитам',
            'benefit_b_title4' => 'Поддержка',
            'benefit_b_desc4' => 'У нас работают лучшие специалисты, которые готовы ответить на любые ваши вопросы 24/7',
            'benefit_b_title5' => 'Легальность',
            'benefit_b_desc5' => 'Мы ведем абсолютно легальную и открытую деятельность',
            'benefit_b_title6' => 'Уникальность',
            'benefit_b_desc6' => 'Наша платформа одна из лучших инвестиционных инструментов',
            'priority_title' => 'Открытия',
            'priority_sub_title' => 'Наши приоритеты',
            'priority_b_title' => 'Быть самыми надежными',
            'priority_b_desc' => '<p>Мы фильтруем каждое решение. Доверие - это взаимодействия клиентов с нами как с компанией. Что требует от нас быть мировым классом в области безопасности, соответствия требованиям, технологии, поддержки клиентов, дизайна и многого другого.</p>',
            'priority_b_title2' => 'Быть простыми в использовании',
            'priority_b_desc2' => '<p>Цифровая валюта - это мощная технология. Crypto Credit Hub - создали замечательных продукт, который приносит всем преимущества.</p>',
            'priority_b_title3' => 'Отличная коммуникация',
            'priority_b_desc3' => '<ul><li>Мы эффективно обмениваемся информацией, улучшая сотрудничество и производительность.</li><li>Мы лаконичны, откровенны и доброжелательны.</li><li>Мы практикуемся в активном и внимательном слушание.</li><li>Мы говорим с людьми напрямую о ваших проблемах.</li></ul>',
            'priority_b_title4' => 'Эффективное выполнение',
            'priority_b_desc4' => '<ul><li>Мы выполняем быстро высококачественную работу, работая умнее, а не сложнее.</li><li>Мы ценим выполнение задач, а не просто говорим о них.</li><li>Мы предпочитаем автоматизацию ручной работе.</li><li>Мы расставляем приоритеты, сосредоточившись на 20%, которые принесут нам 80% отдачи.</li></ul>',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_main_translations');
        Schema::dropIfExists('page_main');
        Schema::dropIfExists('page_investor_translations');
        Schema::dropIfExists('page_investor');
        Schema::dropIfExists('page_about_translations');
        Schema::dropIfExists('page_about');
    }
}
