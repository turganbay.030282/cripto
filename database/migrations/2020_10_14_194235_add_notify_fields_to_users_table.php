<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNotifyFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('notify_news')->default(false);
            $table->boolean('notify_promo')->default(false);
            $table->boolean('notify_by_email')->default(false);
            $table->boolean('notify_by_sms')->default(false);
            $table->boolean('notify_by_push')->default(false);
            $table->string('promo_code')->nullable();
            $table->unsignedBigInteger('referer_id')->nullable();
            $table->foreign('referer_id')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('notify_news');
            $table->dropColumn('notify_promo');
            $table->dropColumn('notify_by_email');
            $table->dropColumn('notify_by_sms');
            $table->dropColumn('notify_by_push');
        });
    }
}
