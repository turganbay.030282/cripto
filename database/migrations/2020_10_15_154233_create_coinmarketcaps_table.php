<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoinmarketcapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coinmarketcaps', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('symbol');
            $table->double('price');
            $table->double('volume_24h');
            $table->double('percent_change_1h')->nullable();
            $table->double('percent_change_24h')->nullable();
            $table->double('percent_change_7d')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coinmarketcaps');
    }
}
