<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSubTitleToPageMainTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('page_main_translations', function (Blueprint $table) {
            $table->string('sub_title')->nullable();
            $table->text('desc_offer_tr')->change();
            $table->text('desc_offer_inv')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('page_main_translations', function (Blueprint $table) {
            $table->dropColumn('sub_title');
            $table->string('desc_offer_tr')->change();
            $table->string('desc_offer_inv')->change();
        });
    }
}
