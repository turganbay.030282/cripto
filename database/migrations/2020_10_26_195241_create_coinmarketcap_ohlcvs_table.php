<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoinmarketcapOhlcvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coinmarketcap_ohlcvs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('coinmarketcap_id');
            $table->foreign('coinmarketcap_id')
                ->references('id')
                ->on('coinmarketcaps')
                ->onDelete('CASCADE');
            $table->timestamp('time_open')->nullable();
            $table->timestamp('time_close')->nullable();
            $table->timestamp('time_high')->nullable();
            $table->timestamp('time_low')->nullable();
            $table->timestamp('timestamp')->nullable();
            $table->double('open');
            $table->double('high');
            $table->double('low');
            $table->double('close');
            $table->double('volume')->nullable();
            $table->double('market_cap')->nullable();
            $table->jsonb('data');
            $table->timestamps();
        });

        Schema::table('coinmarketcaps', function (Blueprint $table) {
            $table->longText('ohlcv_x')->nullable();
            $table->longText('ohlcv_y')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coinmarketcaps', function (Blueprint $table) {
            $table->dropColumn('ohlcv_x');
            $table->dropColumn('ohlcv_y');
        });
        Schema::dropIfExists('coinmarketcap_ohlcvs');
    }
}
