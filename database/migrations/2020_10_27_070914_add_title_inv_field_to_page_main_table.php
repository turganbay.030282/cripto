<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTitleInvFieldToPageMainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('page_main_translations', function (Blueprint $table) {
            $table->string('title_inv')->nullable();
            $table->string('sub_title_inv')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('page_main_translations', function (Blueprint $table) {
            $table->dropColumn('title_inv');
            $table->dropColumn('sub_title_inv');
        });
    }
}
