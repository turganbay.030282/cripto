<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForGraphToAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->unsignedBigInteger('coinmarketcap_id')->nullable();
            $table->foreign('coinmarketcap_id')
                ->references('id')
                ->on('coinmarketcaps')
                ->onDelete('CASCADE');
            $table->boolean('for_graph')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropColumn('coinmarketcap_id');
            $table->dropColumn('for_graph');
        });
    }
}
