<?php

use App\Entity\UserVerification;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDocTypeToUserVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_verifications', function (Blueprint $table) {
            $table->string('type_doc')->default(UserVerification::DOCUMENT_TYPE_PASSPORT);
            $table->string('card_id_back')->nullable();
            $table->string('external_id')->nullable();
            $table->jsonb('data_api')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_verifications', function (Blueprint $table) {
            $table->dropColumn('type_doc');
            $table->dropColumn('card_id_back');
            $table->dropColumn('external_id');
            $table->dropColumn('data_api');
        });
    }
}
