<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTplDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tpl_documents', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->timestamps();
        });
        Schema::create('tpl_document_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('tpl_document_id');
            $table->foreign('tpl_document_id')->references('id')->on('tpl_documents')->onDelete('cascade');
            $table->string('locale')->index();
            $table->longText('content');
            $table->unique(['tpl_document_id', 'locale']);
        });

        Schema::table('contracts', function (Blueprint $table) {
            $table->longText('document')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn('document');
        });
        Schema::dropIfExists('tpl_document_translations');
        Schema::dropIfExists('tpl_documents');
    }
}
