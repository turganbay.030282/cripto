<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLevelToTypeTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_transactions', function (Blueprint $table) {
            $table->integer('level')->nullable();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->integer('investor_rank')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_transactions', function (Blueprint $table) {
            $table->dropColumn('level');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('investor_rank');
        });
    }
}
