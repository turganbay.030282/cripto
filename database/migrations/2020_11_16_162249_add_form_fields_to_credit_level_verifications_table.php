<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFormFieldsToCreditLevelVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credit_level_verifications', function (Blueprint $table) {
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('sex')->nullable();
            $table->string('personal_code')->nullable();
            $table->string('family_status')->nullable();
            $table->string('political_functions')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('dependents')->nullable();
            $table->string('type_accommodation')->nullable();
            $table->string('education')->nullable();
            $table->string('citizenship')->nullable();

            $table->string('residence_county')->nullable();
            $table->string('residence_city')->nullable();
            $table->string('residence_street')->nullable();
            $table->string('residence_postcode')->nullable();
            $table->string('income_source')->nullable();
            $table->integer('income_net')->nullable();
            $table->integer('income_additional')->nullable();
            $table->string('income_place')->nullable();
            $table->string('income_position')->nullable();
            $table->integer('income_work_experience')->nullable();

            $table->string('credit_liabilities')->default(false);
            $table->integer('credit_liabilities_month')->default(0);
            $table->string('credit_type')->nullable();
            $table->date('credit_date_from')->nullable();
            $table->date('credit_date_to')->nullable();

            $table->string('file_statement')->nullable();
            $table->string('file_income_work')->nullable();
            $table->string('file_utility_bill')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credit_level_verifications', function (Blueprint $table) {
            $table->dropcolumn('first_name');
            $table->dropcolumn('last_name');
            $table->dropcolumn('sex');
            $table->dropcolumn('personal_code');
            $table->dropcolumn('family_status');
            $table->dropcolumn('political_functions');
            $table->dropcolumn('phone');
            $table->dropcolumn('email');
            $table->dropcolumn('dependents');
            $table->dropcolumn('type_accommodation');
            $table->dropcolumn('education');
            $table->dropcolumn('citizenship');
            $table->dropcolumn('residence_county');
            $table->dropcolumn('residence_city');
            $table->dropcolumn('residence_street');
            $table->dropcolumn('residence_postcode');
            $table->dropcolumn('income_source');
            $table->dropcolumn('income_net');
            $table->dropcolumn('income_additional');
            $table->dropcolumn('income_place');
            $table->dropcolumn('income_position');
            $table->dropcolumn('income_work_experience');
            $table->dropcolumn('credit_liabilities');
            $table->dropcolumn('credit_liabilities_month');
            $table->dropcolumn('credit_type');
            $table->dropcolumn('credit_date_from');
            $table->dropcolumn('credit_date_to');
            $table->dropcolumn('file_statement');
            $table->dropcolumn('file_income_work');
            $table->dropcolumn('file_utility_bill');
        });
    }
}
