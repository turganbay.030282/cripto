<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCreditLiabilitiesMonthToCreditLevelVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credit_level_verifications', function (Blueprint $table) {
            $table->string('credit_liabilities_month')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credit_level_verifications', function (Blueprint $table) {
            $table->string('credit_liabilities_month')->default(0)->change();
        });
    }
}
