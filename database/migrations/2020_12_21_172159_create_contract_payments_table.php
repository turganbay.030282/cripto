<?php

use App\Entity\Contract\ContractPayment;
use App\Entity\Transfer\Transaction;
use App\Helpers\StatusHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->string('status')->default(StatusHelper::STATUS_NEW);
        });

        Schema::create('contract_payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contract_id');
            $table->foreign('contract_id')->references('id')->on('contracts')->onDelete('cascade');
            $table->double('main_amount');
            $table->double('percent');
            $table->double('amount');
            $table->date('date');
            $table->string('status')->default(StatusHelper::STATUS_NEW);
            $table->string('pay_ticket')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::dropIfExists('contract_payments');
    }
}
