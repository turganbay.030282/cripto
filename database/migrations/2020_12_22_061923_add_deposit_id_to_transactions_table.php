<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDepositIdToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->unsignedBigInteger('deposit_payment_id')->nullable();
            $table->foreign('deposit_payment_id')->references('id')->on('contracts')->onDelete('cascade');
            $table->unsignedBigInteger('credit_payment_id')->nullable();
            $table->foreign('credit_payment_id')->references('id')->on('contract_payments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('deposit_payment_id');
            $table->dropColumn('credit_payment_id');
        });
    }
}
