<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditLevelUrVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_level_ur_verifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('manager_id')->unsigned()->nullable();
            $table->boolean('is_ur')->default(true);
            $table->foreign('manager_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
            $table->integer('amount')->default(0);
            $table->string('status')->default(\App\Entity\UserVerification::STATUS_WAIT);
            $table->text('comment')->nullable();

            $table->string('company_name')->nullable();
            $table->string('reg_number')->nullable();
            $table->date('register_date')->nullable();
            $table->string('ur_address')->nullable();
            $table->string('address')->nullable();
            $table->string('contact_phone')->nullable();
            $table->string('email')->nullable();
            $table->string('primary_occupation')->nullable();
            $table->string('capital_amount')->nullable();
            $table->string('person_name')->nullable();
            $table->string('person_surname')->nullable();
            $table->string('person_personal_code')->nullable();
            $table->string('person_turnover_prev')->nullable();
            $table->string('person_profit_prev')->nullable();
            $table->string('person_banks')->nullable();
            $table->string('creditor')->nullable();
            $table->string('start_amount')->nullable();
            $table->string('balance_obligations')->nullable();
            $table->date('credit_date_to')->nullable();
            $table->string('month_payments')->nullable();
            $table->string('credit_target')->nullable();
            $table->string('credit_source')->nullable();
            $table->string('file_statement')->nullable();
            $table->string('file_income_work')->nullable();
            $table->string('file_utility_bill')->nullable();
            $table->timestamps();
        });

        Schema::create('credit_level_owners', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('credit_level_id')->unsigned()->nullable();
            $table->foreign('credit_level_id')
                ->references('id')
                ->on('credit_level_ur_verifications')
                ->onDelete('CASCADE');
            $table->string('name');
            $table->string('surname');
            $table->string('personal_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_level_owners');
        Schema::dropIfExists('credit_level_ur_verifications');
    }
}
