<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCommisionToSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_payments', function (Blueprint $table) {
            $table->double('balance_owed')->default(0);
        });
        Schema::table('settings', function (Blueprint $table) {
            $table->integer('commission_trader')->default(3);
            $table->integer('commission_investor')->default(5);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_payments', function (Blueprint $table) {
            $table->dropColumn('balance_owed');
        });
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('commission_trader');
            $table->dropColumn('commission_investor');
        });
    }
}
