<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFormsFieldsToInvestorRankVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investor_rank_verifications', function (Blueprint $table) {
            $table->string('company_name')->nullable();
            $table->string('company_reg_number')->nullable();
            $table->date('company_date_reg')->nullable();
            $table->string('company_country_reg')->nullable();
            $table->string('company_address')->nullable();
            $table->string('company_ur_address')->nullable();
            $table->string('company_field_activity')->nullable();
            $table->string('company_phone')->nullable();
            $table->string('company_email')->nullable();

            $table->string('full_name')->nullable();
            $table->date('birthday')->nullable();
            $table->string('sex')->nullable();
            $table->string('bin')->nullable();
            $table->string('place_birth')->nullable();
            $table->string('country')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('professional_activities')->nullable();
            $table->boolean('volume_1')->default(0);
            $table->boolean('volume_2')->default(0);
            $table->boolean('volume_3')->default(0);
            $table->boolean('volume_4')->default(0);
            $table->boolean('volume_5')->default(0);
            $table->string('position')->nullable();
            $table->string('doc_name')->nullable();

            $table->boolean('source_business_activities')->default(0);
            $table->boolean('source_dividend')->default(0);
            $table->boolean('source_loans')->default(0);
            $table->boolean('source_income_assets')->default(0);
            $table->boolean('source_contributions')->default(0);
            $table->boolean('source_prize')->default(0);
            $table->boolean('source_other')->default(0);
            $table->string('source_other_text')->nullable();
            $table->string('source_country')->nullable();

            $table->boolean('i_beneficiary')->default(1);
            $table->string('beneficiary_name')->nullable();

            $table->boolean('not_pep')->default(1);
            $table->string('politics')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investor_rank_verifications', function (Blueprint $table) {
            $table->dropColumn('company_name');
            $table->dropColumn('company_reg_number');
            $table->dropColumn('company_date_reg');
            $table->dropColumn('company_country_reg');
            $table->dropColumn('company_address');
            $table->dropColumn('company_ur_address');
            $table->dropColumn('company_field_activity');
            $table->dropColumn('company_phone');
            $table->dropColumn('company_email');
            $table->dropColumn('full_name');
            $table->dropColumn('birthday');
            $table->dropColumn('sex');
            $table->dropColumn('bin');
            $table->dropColumn('place_birth');
            $table->dropColumn('country');
            $table->dropColumn('address');
            $table->dropColumn('phone');
            $table->dropColumn('email');
            $table->dropColumn('professional_activities');
            $table->dropColumn('volume_1');
            $table->dropColumn('volume_2');
            $table->dropColumn('volume_3');
            $table->dropColumn('volume_4');
            $table->dropColumn('volume_5');
            $table->dropColumn('position');
            $table->dropColumn('doc_name');
            $table->dropColumn('source_business_activities');
            $table->dropColumn('source_dividend');
            $table->dropColumn('source_loans');
            $table->dropColumn('source_income_assets');
            $table->dropColumn('source_contributions');
            $table->dropColumn('source_prize');
            $table->dropColumn('source_other');
            $table->dropColumn('source_other_text');
            $table->dropColumn('source_country');
            $table->dropColumn('i_beneficiary');
            $table->dropColumn('beneficiary_name');
            $table->dropColumn('not_pep');
            $table->dropColumn('politics');
        });
    }
}
