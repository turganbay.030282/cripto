<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPayTicketToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->double('free_active')->default(0);
        });
        Schema::table('transactions', function (Blueprint $table) {
            $table->string('pay_ticket')->nullable();
            $table->double('amount_dai')->default(0);
        });
        Schema::table('contract_invest_profits', function (Blueprint $table) {
            $table->double('percent_dai')->default(0);
            $table->boolean('is_pay')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('free_active');
        });
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('pay_ticket');
            $table->dropColumn('amount_dai');
        });
        Schema::table('contract_invest_profits', function (Blueprint $table) {
            $table->dropColumn('percent_dai');
            $table->dropColumn('is_pay');
        });
    }
}
