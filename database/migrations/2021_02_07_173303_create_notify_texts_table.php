<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotifyTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notify_texts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        });
        Schema::create('notify_text_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('notify_text_id');
            $table->foreign('notify_text_id')->references('id')->on('notify_texts')->onDelete('cascade');
            $table->string('locale')->index();
            $table->string('subject');
            $table->text('body');
            $table->unique(['notify_text_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notify_text_translations');
        Schema::dropIfExists('notify_texts');
    }
}
