<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRegUserIdToStatisticPromoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statistic_promo_codes', function (Blueprint $table) {
            $table->unsignedBigInteger('reg_user_id')->index()->nullable();
            $table->foreign('reg_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('statistic_promo_codes', function (Blueprint $table) {
            $table->dropForeign('reg_user_id');
            $table->dropColumn('reg_user_id');
        });
    }
}
