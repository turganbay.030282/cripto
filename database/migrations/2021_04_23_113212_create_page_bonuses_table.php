<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_bonus', function (Blueprint $table) {
            $table->id();
            $table->string('photo1')->nullable();
            $table->string('photo2')->nullable();
            $table->string('photo3')->nullable();
            $table->string('photo4')->nullable();
            $table->timestamps();
        });
        Schema::create('page_bonus_translations', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('page_bonus_id')->nullable();
            $table->foreign('page_bonus_id')->references('id')->on('page_bonus')->onDelete('cascade');
            $table->string('locale')->index();
            $table->string('title')->nullable();
            $table->string('subtitle')->nullable();
            $table->string('title1')->nullable();
            $table->text('content1')->nullable();
            $table->string('title2')->nullable();
            $table->text('content2')->nullable();
            $table->string('title3')->nullable();
            $table->text('content3')->nullable();
            $table->string('meta_title')->nullable();
            $table->text('meta_desc')->nullable();
            $table->unique(['page_bonus_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_bonus_translations');
        Schema::dropIfExists('page_bonuses');
    }
}
