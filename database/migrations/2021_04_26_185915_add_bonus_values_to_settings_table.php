<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBonusValuesToSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->decimal('bonus_ref_credit')->default(1);
            $table->decimal('bonus_ref_deposit')->default(1);
            $table->decimal('percent_ref_credit')->default(0.25);
            $table->decimal('percent_ref_deposit')->default(0.5);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('bonus_ref_credit');
            $table->dropColumn('bonus_ref_deposit');
            $table->dropColumn('percent_ref_credit');
            $table->dropColumn('percent_ref_deposit');
        });
    }
}
