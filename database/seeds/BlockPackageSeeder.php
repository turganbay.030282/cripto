<?php

use App\Entity\Content\BlockPackage;
use Illuminate\Database\Seeder;

class BlockPackageSeeder extends Seeder
{
    public function run()
    {
        $path = storage_path('/app/public/block_packages');
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        \File::copy(public_path('/img/Paket-1.svg'), storage_path('/app/public/block_packages/paket-trader-1.svg'));
        \File::copy(public_path('/img/Paket-2.svg'), storage_path('/app/public/block_packages/paket-trader-2.svg'));
        \File::copy(public_path('/img/Paket-3.svg'), storage_path('/app/public/block_packages/paket-trader-3.svg'));
        \File::copy(public_path('/img/Paket-1.svg'), storage_path('/app/public/block_packages/paket-investor-1.svg'));
        \File::copy(public_path('/img/Paket-2.svg'), storage_path('/app/public/block_packages/paket-investor-2.svg'));
        \File::copy(public_path('/img/Paket-3.svg'), storage_path('/app/public/block_packages/paket-investor-3.svg'));

        $this->addRecord(
            'Basic',
            'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
            'Возьми от 500 € до 10,000 €',
            1,
            'paket-trader-1.svg'
        );
        $this->addRecord(
            'Silver',
            'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
            'Возьми от 10,000 € до 50,000 €',
            1,
            'paket-trader-2.svg'
        );
        $this->addRecord(
            'Gold',
            'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
            'Возьми от 50,000 € до 100,000 €',
            1,
            'paket-trader-3.svg'
        );

        $this->addRecord(
            'Basic',
            'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
            'Возьми от 500 € до 10,000 €',
            2,
            'paket-investor-1.svg'
        );
        $this->addRecord(
            'Silver',
            'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
            'Возьми от 10,000 € до 50,000 €',
            2,
            'paket-investor-2.svg'
        );
        $this->addRecord(
            'Gold',
            'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
            'Возьми от 50,000 € до 100,000 €',
            2,
            'paket-investor-3.svg'
        );
    }

    private function addRecord($name, $desc, $price, $type, $photo)
    {
        $locales = config('translatable.locales');
        $fieldsInsert['type'] = $type;
        $fieldsInsert['photo'] = $photo;
        foreach ($locales as $locale) {
            $fieldsInsert[$locale] = [
                'name' => $name,
                'desc' => $desc,
                'price' => $price
            ];
        }
        return BlockPackage::create($fieldsInsert);
    }
}
