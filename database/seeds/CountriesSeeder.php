<?php

use Illuminate\Database\Seeder;
use App\Entity\Handbook\Country;

class CountriesSeeder extends Seeder
{
    /**
     * @var Country
     */
    private $country;

    /**
     * RolesAndPermissionsSeeder constructor.
     * @param Country $country
     */
    public function __construct(Country $country)
    {
        $this->country = $country;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        foreach (config('translatable.locales') as $locale) {
            $countries = require base_path('vendor/umpirsky/country-list/data/' . $locale . '/country.php');

            if (is_array($countries)) {
                foreach ($countries as $countryCode => $countryName) {
                    $country = Country::firstOrCreate([
                        'code' => $countryCode,
                    ]);
                    $country->translations()->create([
                        'locale' => $locale,
                        'title' => $countryName,
                    ]);
                }
            }
        }
    }
}
