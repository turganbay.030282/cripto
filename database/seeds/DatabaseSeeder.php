<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TranslationSeeder::class);
        $this->call(TplDocumentSeeder::class);
        $this->call(NotifyTextSeeder::class);
//        $this->call(UserSeeder::class);
//        $this->call(PageSeeder::class);
//        $this->call(HandbookSeeder::class);
//        $this->call(PageContentSeeder::class);
//        $this->call(BlockPackageSeeder::class);
//        $this->call(DirectorySeeder::class);
//        $this->call(HandbookTypeSeeder::class);
    }
}
