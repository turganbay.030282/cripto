<?php

use Illuminate\Database\Seeder;

class DirectorySeeder extends Seeder
{
    public function run()
    {
        $path = storage_path('/app/public/contracts');
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $path = storage_path('/app/public/credit_level_verifications');
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $path = storage_path('/app/public/credit_level_ur_verifications');
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $path = storage_path('/app/public/settings');
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }
    }
}
