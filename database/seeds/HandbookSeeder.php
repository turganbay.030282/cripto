<?php

use App\Entity\Handbook\CreditLevel;
use App\Entity\Handbook\InvestorRank;
use App\Entity\Handbook\Asset;
use App\Entity\Handbook\TypeTransaction;
use Illuminate\Database\Seeder;

class HandbookSeeder extends Seeder
{
    public function run()
    {
        $cl1 = CreditLevel::create(['range_from' => 10, 'range_to' => 99]);
        \App\Entity\Handbook\CreditLevelTranslation::create([
            'credit_level_id' => $cl1->id,
            'locale' => 'ru',
            'name' => 'Level 1'
        ]);
        \App\Entity\Handbook\CreditLevelTranslation::create([
            'credit_level_id' => $cl1->id,
            'locale' => 'en',
            'name' => 'Level 1'
        ]);
        $cl2 = CreditLevel::create(['range_from' => 100, 'range_to' => 400]);
        \App\Entity\Handbook\CreditLevelTranslation::create([
            'credit_level_id' => $cl2->id,
            'locale' => 'ru',
            'name' => 'Level 2'
        ]);
        \App\Entity\Handbook\CreditLevelTranslation::create([
            'credit_level_id' => $cl2->id,
            'locale' => 'en',
            'name' => 'Level 2'
        ]);

        $ir1 = InvestorRank::create(['range_from' => 10, 'range_to' => 99]);
        \App\Entity\Handbook\InvestorRankTranslation::create([
            'investor_rank_id' => $ir1->id,
            'locale' => 'ru',
            'name' => 'Rank 1'
        ]);
        \App\Entity\Handbook\InvestorRankTranslation::create([
            'investor_rank_id' => $ir1->id,
            'locale' => 'en',
            'name' => 'Rank 1'
        ]);
        $ir2 = InvestorRank::create(['range_from' => 100, 'range_to' => 400]);
        \App\Entity\Handbook\InvestorRankTranslation::create([
            'investor_rank_id' => $ir2->id,
            'locale' => 'ru',
            'name' => 'Rank 2'
        ]);
        \App\Entity\Handbook\InvestorRankTranslation::create([
            'investor_rank_id' => $ir2->id,
            'locale' => 'en',
            'name' => 'Rank 2'
        ]);

        $a1 = Asset::create(['type' => Asset::TYPE_NATURAL]);
        \App\Entity\Handbook\AssetTranslation::create([
            'asset_id' => $a1->id,
            'locale' => 'ru',
            'name' => 'Золото AUX'
        ]);
        \App\Entity\Handbook\AssetTranslation::create([
            'asset_id' => $a1->id,
            'locale' => 'en',
            'name' => 'Золото AUX'
        ]);
        $a2 = Asset::create(['type' => Asset::TYPE_VIRTUAL]);
        \App\Entity\Handbook\AssetTranslation::create([
            'asset_id' => $a2->id,
            'locale' => 'ru',
            'name' => 'Биткоин BTX'
        ]);
        \App\Entity\Handbook\AssetTranslation::create([
            'asset_id' => $a2->id,
            'locale' => 'en',
            'name' => 'Биткоин BTX'
        ]);

        $tt1 = TypeTransaction::create([
            'id' => 1,
            'type' => \App\Helpers\TypeUserHelper::TYPE_USER_TRADER,
            'value' => 16,
            'period' => 36,
            'min' => 50,
            'max' => 10000,
        ]);
        \App\Entity\Handbook\TypeTransactionTranslation::create([
            'type_transaction_id' => $tt1->id,
            'locale' => 'ru',
            'name' => 'Бронза',
        ]);
        \App\Entity\Handbook\TypeTransactionTranslation::create([
            'type_transaction_id' => $tt1->id,
            'locale' => 'en',
            'name' => 'Бронза',
        ]);

        $tt2 = TypeTransaction::create([
            'id' => 2,
            'type' => \App\Helpers\TypeUserHelper::TYPE_USER_TRADER,
            'value' => 18,
            'period' => 36,
            'min' => 10001,
            'max' => 50000,
        ]);
        \App\Entity\Handbook\TypeTransactionTranslation::create([
            'type_transaction_id' => $tt2->id,
            'locale' => 'ru',
            'name' => 'Серебро',
        ]);
        \App\Entity\Handbook\TypeTransactionTranslation::create([
            'type_transaction_id' => $tt2->id,
            'locale' => 'en',
            'name' => 'Серебро',
        ]);

        $tt3 = TypeTransaction::create([
            'id' => 3,
            'type' => \App\Helpers\TypeUserHelper::TYPE_USER_TRADER,
            'value' => 20,
            'period' => 36,
            'min' => 50001,
            'max' => 100000,
        ]);
        \App\Entity\Handbook\TypeTransactionTranslation::create([
            'type_transaction_id' => $tt3->id,
            'locale' => 'ru',
            'name' => 'Золотой',
        ]);
        \App\Entity\Handbook\TypeTransactionTranslation::create([
            'type_transaction_id' => $tt3->id,
            'locale' => 'en',
            'name' => 'Золотой',
        ]);

        $tt4 = TypeTransaction::create([
            'id' => 4,
            'type' => \App\Helpers\TypeUserHelper::TYPE_USER_INVESTOR,
            'value' => 8,
            'period' => 24,
            'min' => 50,
            'max' => 10000,
        ]);
        \App\Entity\Handbook\TypeTransactionTranslation::create([
            'type_transaction_id' => $tt4->id,
            'locale' => 'ru',
            'name' => 'Бронза',
        ]);
        \App\Entity\Handbook\TypeTransactionTranslation::create([
            'type_transaction_id' => $tt4->id,
            'locale' => 'en',
            'name' => 'Бронза',
        ]);

        $tt5 = TypeTransaction::create([
            'id' => 5,
            'type' => \App\Helpers\TypeUserHelper::TYPE_USER_INVESTOR,
            'value' => 9,
            'period' => 36,
            'min' => 10001,
            'max' => 50000,
        ]);
        \App\Entity\Handbook\TypeTransactionTranslation::create([
            'type_transaction_id' => $tt5->id,
            'locale' => 'ru',
            'name' => 'Серебро',
        ]);
        \App\Entity\Handbook\TypeTransactionTranslation::create([
            'type_transaction_id' => $tt5->id,
            'locale' => 'en',
            'name' => 'Серебро',
        ]);

        $tt6 = TypeTransaction::create([
            'id' => 6,
            'type' => \App\Helpers\TypeUserHelper::TYPE_USER_INVESTOR,
            'value' => 10,
            'period' => 60,
            'min' => 50001,
            'max' => 100000,
        ]);
        \App\Entity\Handbook\TypeTransactionTranslation::create([
            'type_transaction_id' => $tt6->id,
            'locale' => 'ru',
            'name' => 'Золотой',
        ]);
        \App\Entity\Handbook\TypeTransactionTranslation::create([
            'type_transaction_id' => $tt6->id,
            'locale' => 'en',
            'name' => 'Золотой',
        ]);
    }
}
