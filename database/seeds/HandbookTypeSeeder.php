<?php

use App\Entity\Handbook\TypeStatus;
use App\Entity\Handbook\TypeTwoStatus;
use Illuminate\Database\Seeder;

class HandbookTypeSeeder extends Seeder
{

    public function run()
    {
        $this->createType('credit', 'Кредит');
        $this->createType('deposit', 'Депозит');
        $this->createType('purchase', 'Покупка');
        $this->createType('payout', 'Трейдер вывод');
        $this->createType('contribution', 'Взнос');
        $this->createType('staking_euro', 'Стейкинг в EURO');
        $this->createType('staking_dai', 'Стейкинг в DAI');
        $this->createType('transfer_asset', 'Цифра в кошелек');
        $this->createType('trader_active_euro', 'Актив в Евро');
        $this->createType('trader_pay_in', 'Трейдер покупка');
        $this->createType('inv_pay_in_dai', 'DAI в кошелек');
        $this->createType('trader_contract_sell', 'Досрочная продажа');

        $this->createTypeTwo('in', 'Входящие');
        $this->createTypeTwo('out', 'Исходящие');
        $this->createTypeTwo('request', 'Запрос');
        $this->createTypeTwo('contr', 'Взнос');
    }

    private function createType($key, $name)
    {
        TypeStatus::create([
            'key' => $key,
            'name' => $name
        ]);
    }

    private function createTypeTwo($key, $name)
    {
        TypeTwoStatus::create([
            'key' => $key,
            'name' => $name
        ]);
    }

}
