<?php

use App\Entity\Tpl\NotifyText;
use Illuminate\Database\Seeder;

class NotifyTextSeeder extends Seeder
{
    public function run()
    {
        NotifyText::query()->delete();
        $data = json_decode(file_get_contents('NotifyText.json'), 1);
        foreach ($data as $datum){
            foreach ($datum['translations'] as $translation){
                $datum[$translation['locale']] = [
                    'subject'=>$translation['subject'],
                    'body'=>$translation['body'],
                ];
            }
            unset($datum['translations']);
            NotifyText::create($datum);
        }

        /*
        $this->create(1, 'Регистрация', 'Мы рады, что вы зарегистрировались на нашем сайте! Осталось совсем немного — подтвердить почту.');//+
        $this->create(2, 'Регистрация на сервисе', 'Вам отправлена ссылка для регистрации');//+

        $this->create(3, 'Верификация', 'Верификация успешно пройдена');//+
        $this->create(4, 'Верификация', 'Верификация отклонена');//+

        $this->create(5, 'Кредитный скорринг', 'Скорринг успешно пройден');//+
        $this->create(6, 'Кредитный скорринг', 'Скорринг отклонен');//+

        $this->create(7, 'Инвестиционный скорринг', 'Скорринг успешно пройден');//+
        $this->create(8, 'Инвестиционный скорринг', 'Скорринг отклонен');//+

        $this->create(9, 'Кредитная линия', 'Кредитная линия одобрена');//+
        $this->create(10, 'Кредитная линия', 'Кредитная линия отказ');//+

        $this->create(11, 'Договор трейдера', 'Договора трейдера заключен');//+

        $this->create(12, 'Трейдер вывод в евро', 'Вывод в евро одобрен');//+
        $this->create(13, 'Трейдер вывод в кошелек', 'Вывод в кошелек одобрен');//+
        $this->create(14, 'Трейдер вывод доступных к выводу', 'Вывод одобрен');

        $this->create(15, 'Договор трейдер досрочное погашение', 'Досрочное погашение одобрено');//+
        $this->create(16, 'Договор трейдер досрочная продажа', 'Досрочная продажа одобрена');//+

        $this->create(17, 'Инвестор внесение средств', 'внесение средств одобрено');//+

        $this->create(18, 'Инвестора внесение в DAI', 'внесение в DAI одобрено');//+
        $this->create(19, 'Инвестор вывод DAI в евро', 'вывод DAI в евро одобрен');//+
        $this->create(20, 'Инвестор вывод DAI в кошелек', 'вывод DAI в кошелек одобрен');//+

        $this->create(21, 'Договор инвестора', 'Договор инвестора заключен');//+
        $this->create(22, 'Досрочное расторжение договора Инвестора', 'Договор досрочно расторгнут');//+

        $this->create(23, 'Персональные данные изменены', 'Персональные данные изменены');//+
        $this->create(24, 'Пароль изменен', 'Пароль изменен');//+
        */
    }

    private function create(
        $id,
        $subject,
        $body
    )
    {
        $array = [];
        $array['id'] = $id;

        foreach (config('translatable.locales') as $locale) {
            $array[$locale]['subject'] = $subject;
            $array[$locale]['body'] = $body;
        }
        NotifyText::create($array);
    }

}
