<?php

use Illuminate\Database\Seeder;

class PageBonusSeeder extends Seeder
{
    public function run()
    {
        $path = storage_path('/app/public/page_bonuses');
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        \File::copy(public_path('/img/img_partners.png'), storage_path('/app/public/page_bonuses/img_partners.png'));

        $p1 = \App\Entity\Content\PageBonus::create([
            'photo1' => 'img_partners.png',
        ]);
        foreach (config('translatable.locales') as $locale)
        \App\Entity\Content\PageBonusTranslation::create([
            'locale' => $locale,
            'page_bonus_id' => $p1->id,
            'title' => 'Приглашай друзей, зарабатывайте вместе.',
            'subtitle' => 'Получай % за сделки друга и с его инвестиций.',
            'title1' => 'Что вы получаете?',
            'content1' => '<ul><li>0.25% от каждой сделки Вашего друга по крипто валютам;</li><li>0.5% от суммы стейкинга Вашего друга.</li><li>Лимит на приглашенного.</li></ul>',
            'title2' => 'Что получает Ваш друг?',
            'content2' => '<ul><li>1% скидки на комиссию системы при покупке криптовалюты в кредит;</li><li>Дополнительный 1% от суммы к начислению депозита при вступлении в стейкинг.</li><li><span>*</span> данные бонусы начисляются только при первых сделках Холдера или Стейк-Холдера.</li></ul>',
            'title3' => 'Преимущества реферальной прогарммы AssetCore:',
            'content3' => '<ul><li>Моментальное начисление;</li><li>Возможность использоваться сразу;</li><li>Нет ограничений по заработанным суммам и кол-ву привлеченных участников;</li><li>Несгораемые начисления.</li></ul>',
        ]);
    }
}
