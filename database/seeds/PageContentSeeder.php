<?php

use Illuminate\Database\Seeder;

class PageContentSeeder extends Seeder
{
    public function run()
    {
        $path = storage_path('/app/public/page_investor');
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }
        \File::copy(public_path('/img/Profit-1.png'), storage_path('/app/public/page_investor/Profit-1.png'));
        \File::copy(public_path('/img/Profit-2.png'), storage_path('/app/public/page_investor/Profit-2.png'));
        \File::copy(public_path('/img/Profit-3.png'), storage_path('/app/public/page_investor/Profit-3.png'));

        $path = storage_path('/app/public/page_about');
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }
        \File::copy(public_path('/img/ListAbout-1.png'), storage_path('/app/public/page_about/ListAbout-1.png'));
        \File::copy(public_path('/img/ListAbout-2.png'), storage_path('/app/public/page_about/ListAbout-2.png'));
        \File::copy(public_path('/img/ListAbout-3.png'), storage_path('/app/public/page_about/ListAbout-3.png'));
        \File::copy(public_path('/img/ListAbout-4.png'), storage_path('/app/public/page_about/ListAbout-4.png'));
        \File::copy(public_path('/img/ListAbout-5.png'), storage_path('/app/public/page_about/ListAbout-5.png'));
        \File::copy(public_path('/img/ListAbout-6.png'), storage_path('/app/public/page_about/ListAbout-6.png'));
    }
}
