<?php

use App\Entity\Content\Page;
use App\Entity\Content\PageTranslation;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    public function run()
    {
        $p1 = Page::create([
            'slug' => 'rules',
            'publish' => \App\Helpers\PublishHelper::publish_on,
        ]);
        PageTranslation::create([
            'locale' => 'ru',
            'page_id' => $p1->id,
            'title' => 'Правила пользования',
            'content' => 'Правила пользования',
        ]);
        PageTranslation::create([
            'locale' => 'en',
            'page_id' => $p1->id,
            'title' => 'Правила пользования',
            'content' => 'Правила пользования',
        ]);

        $p2 = Page::create([
            'slug' => 'privacy-policy',
            'publish' => \App\Helpers\PublishHelper::publish_on,
        ]);
        PageTranslation::create([
            'locale' => 'ru',
            'page_id' => $p2->id,
            'title' => 'Политика конфиденциальности',
            'content' => 'Политика конфиденциальности',
        ]);
        PageTranslation::create([
            'locale' => 'en',
            'page_id' => $p2->id,
            'title' => 'Политика конфиденциальности',
            'content' => 'Политика конфиденциальности',
        ]);
    }
}
