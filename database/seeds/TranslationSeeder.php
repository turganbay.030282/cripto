<?php

use App\Entity\Translation;
use Illuminate\Database\Seeder;

class TranslationSeeder extends Seeder
{
    public function run()
    {
        Translation::query()->delete();
        $data = json_decode(file_get_contents('Translation.json'), 1);
        foreach ($data as $datum){
            Translation::create($datum);
        }
    }
}
