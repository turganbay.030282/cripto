<?php

use App\Entity\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::create(
            [
                'first_name' => 'Admin',
                'last_name' => 'Admin',
                'email' => 'admin_cripto@mail.com',
                'phone' => '+380937523786',
                'password' => bcrypt('L4Kd04jn30Kdj'),
                'status' => User::STATUS_ACTIVE,
                'role' => User::ROLE_ADMIN,
            ]
        );
        User::create(
            [
                'first_name' => 'Roma',
                'last_name' => 'investor',
                'email' => 'investor@mail.com',
                'phone' => '+38011111111',
                'password' => bcrypt('123456'),
                'status' => User::STATUS_ACTIVE,
                'role' => User::ROLE_USER,
                'is_investor' => true,
            ]
        );
        User::create(
            [
                'first_name' => 'Roma',
                'last_name' => 'trader',
                'email' => 'trader@mail.com',
                'phone' => '+38011111112',
                'password' => bcrypt('123456'),
                'status' => User::STATUS_ACTIVE,
                'role' => User::ROLE_USER,
                'is_investor' => false,
            ]
        );
    }
}
