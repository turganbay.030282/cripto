function translit(str)
{
    var ru=("А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ы-ы-Э-э-Ю-ю-Я-я").split("-");
    var en=("a-a-b-b-v-v-g-g-g-g-d-d-e-e-e-e-e-e-zh-zh-z-z-i-i-i-i-i-i-j-j-k-k-l-l-m-m-n-n-o-o-p-p-r-r-s-s-t-t-u-u-f-f-h-h-ts-ts-ch-ch-sh-sh-sch-sch-y-y-e-e-yu-yu-ya-ya").split("-");

    var res = '';
    for(var i=0, l=str.length; i<l; i++)
    {
        var s = str.charAt(i), n = ru.indexOf(s);
        if(n >= 0) {
            res += en[n];
        }else if(s == "." || s == " "){
            res += "-";
        }else{
            if(s == "?" || s == "!" || s == ":" || s == ";" || s == "#" || s == "Ъ" || s == "ъ" || s == "Ь" || s == "ь" || s == "@" || s == "$" || s == "%" || s == "^" || s == "&" || s == "*" || s == "(" || s == ")"){
            }else{
                res += s;
            }
        }
    }
    res = res.toLowerCase();
    return res;
}

function password_generator( len ) {
    var length = (len)?(len):(10);
    var string = "abcdefghijklmnopqrstuvwxyz"; //to upper
    var numeric = '0123456789';
    var punctuation = '!@#$%^&*()_+~`|}{[]\:;?><,./-=';
    var password = "";
    var character = "";
    var crunch = true;
    while( password.length<length ) {
        entity1 = Math.ceil(string.length * Math.random()*Math.random());
        entity2 = Math.ceil(numeric.length * Math.random()*Math.random());
        entity3 = Math.ceil(punctuation.length * Math.random()*Math.random());
        hold = string.charAt( entity1 );
        hold = (entity1%2==0)?(hold.toUpperCase()):(hold);
        character += hold;
        character += numeric.charAt( entity2 );
        character += punctuation.charAt( entity3 );
        password = character;
    }
    return password;
}

$(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("input#title_ru").on("change", function() {
        var str = $(this).val();
        $("input#slug").val(translit(str));
    });

    if($('.editor').size() > 0){
        var options = {
            filebrowserImageBrowseUrl: '/filemanager?type=Images',
            filebrowserImageUploadUrl: '/filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/filemanager?type=Files',
            filebrowserUploadUrl: '/filemanager/upload?type=Files&_token=',
            allowedContent: true
        };
        $('.editor').ckeditor(options);
    }

    if($('.select-list').size() > 0)
        $(".select-list").select2();

    if($('.select-list-multiple').size() > 0)
        $(".select-list-multiple").select2();

    $(".popup").fancybox();

    $(".picker-date").datetimepicker({
        viewMode: 'days',
        sideBySide: true,
        showClose: true,
        stepping: 1,
        format: 'YYYY-MM-DD',
        locale: 'ru',
    });

    $(".picker-date-format").datetimepicker({
        viewMode: 'days',
        sideBySide: true,
        showClose: true,
        stepping: 1,
        format: 'DD.MM.YYYY',
        locale: 'ru',
    });

    $(".picker-time").datetimepicker({
        format: 'HH:mm',
        locale: 'ru',
    });

    $(".picker-full-time").datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
        locale: 'ru',
    });

    $('#select-lang').on('change', function(){
        location.href = $(this).val();
    });

    $('form').on('submit', function () {
        $(this).find('button[type="submit"]').each(function (index, element) {
            if ($(element).attr('name') != 'excel') {
                $(element).prop('disabled', true);
            }
        });
    });

    $('button.btn-confirmation').on('click', function (e) {
        e.preventDefault();

        let isAction = confirm("Вы уверены, что хотите удалить?");

        if (isAction) {
            $(this).closest('form').submit();
        }
    });
});
