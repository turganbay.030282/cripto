$(document).ready(function(){
    $('.TopMenu > ul > li').has( 'ul' ).addClass('ArrowDownMenu');
    $('.TopMenu > ul > li').hover(function(){
        $(this).find('ul').stop().slideToggle(200);
    });
    $('.LinkUserBox').click(function (){
        $('.UserBox').toggle();
        return false;
    });
    $('.LinkBellBox,.RespBell').click(function (){
        $('.BellBox').toggle();
        return false;
    });
    $('#RespBurg').click(function (){
        $('.TopMenu').toggle();
        $(this).toggleClass('active');
        return false;
    });
    $('.Mycheckbox, .MycheckboxModal, select, .MyRadio, input[type=file],input[type=number]').styler({
        selectSearch: true
    });
    $('.ShowTable').click(function (){
        $('.Tablehidden').css({'display':'table-row'});
        $('.ShowTable').hide();
        $('.HideTable').show();
        return false;
    });
    $('.HideTable').click(function (){
        $('.Tablehidden').css({'display':'none'});
        $('.HideTable').hide();
        $('.ShowTable').show();
        return false;
    });
    $('.ADDActivBTN').click(function (){
        $('.AddActiv').toggle();
        return false;
    });
    $('.ADDPaymentBTN').click(function (){
        $('.AddPayment').toggle();
        return false;
    });
    $(".RangeSliderMonth").ionRangeSlider({
        grid: true,
        skin: "big",
        min: 3,
        max: 60,
        from: 20,
        postfix: " Month"
    });
    $(".RangeSliderMoney").ionRangeSlider({
        grid: true,
        skin: "big",
        min: 50,
        max: 100000,
        from: 50,
        step: 10,
        postfix: " EURO"
    });
    $(".RangeSliderMoneyDai").ionRangeSlider({
        grid: true,
        skin: "big",
        min: 50,
        max: 100000,
        from: 50,
        step: 10,
        postfix: " DAI"
    });
    $(".RangeSliderMoney1").ionRangeSlider({
        grid: true,
        skin: "big",
        min: 50,
        max: 100000,
        from: 50,
        step: 100,
        postfix: " EURO"
    });
    $(".RangeSliderMoney2").ionRangeSlider({
        grid: true,
        skin: "big",
        min: 17,
        max: 240,
        from: 17,
        step: 10,
        postfix: " EURO"
    });
 });
