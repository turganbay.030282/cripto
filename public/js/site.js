$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#select-lang').on('change', function () {
        location.href = $(this).val();
    });

    $(".picker-date").datepicker({
        format: 'yyyy-mm-dd'
    });

    if ($(".timer-reload").length > 0) {
        $('.btn-reload').on('click', function (e) {
            e.preventDefault();
            location.href = location.pathname;
        });

        var index = 30;
        var timerId = setInterval(function () {
            $(".timer-reload").html(index);
            --index;
            console.log(index);
            if (index <= 0) {
                location.href = location.pathname
            }
        }, 1000);
    }

    if ($(".show-other-modal").length > 0) {
        $('.show-other-modal').on('click', function (e) {
            $(this).closest('.modal').modal('hide');
        });
    }
});

function sendMessage(text){
    $('.x_content.bs-example-popovers').remove();
    let tpl = '<div class="x_content bs-example-popovers"><div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><ul><li><strong>'+text+'</strong></li></ul></div></div>';
    $(tpl).appendTo('.wrapp');
}

function calcTraderSimple(amount, month, percent) {
    let p = percent * 1 / 12;
    let value = amount * month * (1 - (month * percent) / (12 * 100));
    return value.toFixed(2);
}

function calcTraderAnnuityFirstPayment(amount, period, rate) {
    let monthPercent = (rate / (100 * 12)).toFixed(3);
    let coefficient = (monthPercent * ((Math.pow(1 + monthPercent, period)) / (Math.pow(1 + monthPercent, period) - 1))).toFixed(3);
    ;
    return (amount * coefficient).toFixed(3);
}

function downloadFile(response) {
    var blob = new Blob([response], {type: 'application/pdf'});
    var url = URL.createObjectURL(blob);
    location.assign(url);
}

function setGraphic(elementId, dataValue, labelsValue, color1, color2, yAxes = true, xAxes = false) {
    var GraphHomeCab = document.getElementById(elementId).getContext('2d');
    var gradient = GraphHomeCab.createLinearGradient(0, 0, 0, 400);
    gradient.addColorStop(0, color1);
    gradient.addColorStop(.125, 'rgba(255,193,119,0)');

    return new Chart(GraphHomeCab, {
        type: 'line',
        data: {
            labels: labelsValue,
            datasets: [{
                label: '€',
                data: dataValue,
                backgroundColor: gradient,
                borderColor: color2,
                borderJoinStyle: 'round',
                borderCapStyle: 'round',
                borderWidth: 1,
                pointRadius: 0,
                pointHitRadius: 10,
                lineTension: .2,
            }]
        },
        options: {
            //aspectRatio: 6,
            maintainAspectRatio: false,
            responsive: true,
            legend: {
                display: false
            },
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    display: xAxes,
                    gridLines: {},
                    ticks: {
                        minRotation: 90
                    }
                }],
                yAxes: [{
                    display: yAxes,
                    gridLines: {}
                }]
            },
            tooltips: {
                callbacks: {
                    title: function () {
                    }
                },
                displayColors: false,
                yPadding: 0,
                xPadding: 0,
                position: 'nearest',
                caretSize: 8,
                backgroundColor: 'rgba(255,255,255,.9)',
                bodyFontSize: 12,
                bodyFontColor: '#303030'
            }
        }
    });
}
