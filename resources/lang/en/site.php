<?php

return array (
  'page' => 
  array (
    'start-earning' => 'Start earning',
    'name' => 'Name',
    'in-24-hours' => 'In 24 hours',
    'price' => 'Price',
    'changes' => 'Changes',
    'price-chart' => 'Price chart',
    'bargain' => 'Accumulate',
    'btn-buy' => 'Buy',
    'link-watch-more' => 'Watch more',
    'link-hide' => 'Hide',
    'form-trader-calc-title' => 'Holder\'s profit calculation form',
    'form-trader-loan-month' => 'Loan term in months 1',
    'form-trader-payment-monthly' => 'Your monthly payment',
    'register-now' => 'Register now',
    'start' => 'Start',
    'text-experienced-team' => 'We are an experienced team and invest in the most profitable investments, our team is growing rapidly and rapidly covering most of the market.',
    'all-questions' => 'All questions',
    'our-packages' => 'Customer status.
After passing the scoring, each client will be assigned a status that will open access to one of three packages of start-up capital.',
    'calc-trader-investment-assets' => 'Accumulation plan for virtual assets',
    'calc-trader-average-loan-amount' => 'Purchase amount',
    'calc-trader-annual-interest-rate' => 'Сommission',
    'calc-trader-virtual-investment-assets' => 'Accumulation plan for virtual assets',
    'calc-trader-maximum-available-volume' => 'Asset volume',
    'calc-troy-ounce' => 'Troy ounce',
    'header' => 
    array (
      'registration' => 'Registration',
    ),
    'show-more' => 'Show more',
    'your-notifications' => 'Your notifications',
    'info-about-us' => 'Information about us',
    'cert-title' => 'Certificates and licenses',
    'contact-info' => 'Contact Information',
    'contact-Send-message' => 'Send us a message',
    'contact-form-Your-name' => 'Your name',
    'contact-form-Your-E-mail' => 'Enter Email',
    'contact-phone' => 'Phone',
    'contact-enter-phone' => 'Enter phone',
    'contact-enter-company' => 'Enter company',
    'contact-write-us' => 'Write to us',
    'contact-Enter-your-message' => 'Enter your message',
    'form-btn-send' => 'Send',
    'investor-making-profit' => 'How to start making profit',
    'calc-investor-Interactive-form-calculating' => 'Form for calculating the stake-holders profit',
    'calc-investor-Investment-term' => 'Staking term',
    'calc-investor-Investment-amount' => 'Staking amount',
    'calc-investor-Profit-calculation-form' => 'Profit calculation form',
    'calc-investor-Deposit-rank' => 'Deposit rank',
    'calc-investor-Total-income' => 'Total income',
    'calc-investor-Your-profit' => 'Your profit',
    'profile' => 
    array (
      'Personal-data' => 'Personal data',
      'Email' => 'Email',
      'Name' => 'Name',
      'LastName' => 'Last name',
      'Birthday' => 'Birthday',
      'Actual-address' => 'Actual address of residence',
      'Contact-number' => 'Contact number',
      'Document-scan' => 'Profile pic',
      'Change-password' => 'Change password',
      'Old-password' => 'Old Password',
      'New-password' => 'New Password',
      'repeat-New-password' => 'Repeat new password',
      'Verify-phone-number' => 'Verify your phone number',
      'add-extra-protection' => 'Add an extra layer of protection to your account by verifying with your phone',
      'Code-country' => 'Code of the country',
      'Phone-number' => 'Phone number',
      'Send-SMS' => 'Send SMS',
      'Confirm-your-phone-number' => 'Confirm your phone number',
      'Enter-seven-digit' => 'Enter the seven-digit code that was sent to your number',
      'Enter-number' => 'Enter number',
      'Didnt-receive-SMS' => 'Didn\'t receive SMS?',
      'Send-another-one' => 'Send another one?',
      'Configuring-notifications' => 'Configuring notifications',
      'Choose-which-notifications-receive' => 'Choose which notifications you want to receive',
      'Offers-promotions' => 'Offers / promotions',
      'Select-service' => 'Select the service through which you will receive notification',
      'SMS' => 'SMS',
      'Push' => 'Push notifications',
      'Save' => 'Save',
      'current-password' => 'Current password',
      'tab-security' => 'Security',
      'btn-change-profile' => 'Change data via support',
      '2fa-title-on' => '2-Step Verification is ON',
      '2fa-title-off' => 'For your own safety please enable 2-Step Verification',
      '2fa-step-title' => '2-Step Verification',
      '2fa-step-important' => 'IMPORTANT',
      '2fa-step-desc' => 'Always write down your Authentication key!',
      '2fa-step-auth-key' => 'Authentication Key',
      '2fa-step-qr-code' => 'QR Code',
    ),
    'cabinet' => 
    array (
      'Invite-friends' => 'Invite friends',
      'Invite-upload-digital-photos' => 'To verify your identity, upload digital photos of the following documents',
      'Enter-friend-email' => 'Enter friend\'s email',
      'Referral-link' => 'Referral link',
      'Application-for-Verification' => 'Application for Verification',
      'Contact-phone-number' => 'Contact phone number',
      'Upload-passport-ID' => 'Front side of document',
      'magazine-text' => 'To confirm you are you, please upload digital photos of the following document',
      'magazine-Search' => 'Search by magazine',
      'magazine-table-number' => 'Number',
      'table-date-and-time' => 'Date and time',
      'table-Name' => 'Name',
      'table-Message' => 'Message',
      'table-Report-support' => 'Report to support',
      'table-Report-problem-us' => 'Report a problem to us',
      'Available-actions-section' => 'Available actions section',
      'Pass-Verification' => 'Pass Verification',
      'Verification-passed-successfully' => 'Verification passed successfully',
      'Get-Credit-Level' => 'Get Credit Level',
      'Credit-Level' => 'Credit Level',
      'Buy-Asset' => 'Buy Asset',
      'Invest' => 'Stake',
      'Calculation-form' => '.',
      'Interactive-form-trader-profit' => 'Interactive form for calculating the holder\'s profit',
      'Collateral-asset' => 'Collateral asset',
      'Deposit-amount' => 'Deposit amount',
      'Maximum-allowable-purchase' => 'Maximum allowable purchase of an asset',
      'Purchase-an-asset' => 'Purchase an asset',
      'Credit-line' => 'Request a credit line increase',
      'Profit' => 'Profit',
      'Available-balance' => 'Available balance',
      'Withdraw-funds' => 'Withdraw funds',
      'Contract' => 'Contract',
      'Asset-price-purchase' => 'Asset price at the time of purchase',
      'Current-asset-price' => 'Current asset price',
      'Interest-rate' => 'Interest rate',
      'Term' => 'Term',
      'months' => 'months',
      'Portfolio-liquidity' => 'Portfolio liquidity',
      'Invested' => 'Transferred into agreements',
      'Portfolio-cost' => 'Portfolio cost',
      'Profitability' => 'Profitability',
      'Pay' => 'Pay',
      'Deposit' => 'Deposit',
      'Make-Deposit' => 'Deposit in Euro',
      'My-Deposit' => 'My agreements is staking',
      'Credit-amount' => 'Credit amount',
      'Credit-Level-Application' => 'Application for credit level',
      'Credit-Line-Application' => 'Application for a credit line',
      'btn-Refuse' => 'Refuse',
      'btn-Confirm' => 'Confirm',
      'Conclusion-Trader-Agreement' => 'Conclusion of a Holder Agreement',
      'Contract-duration' => 'Contract duration',
      'Text-Agreement' => 'Text of the Agreement',
      'PRINT' => 'PRINT',
      'Agree-data-processing' => 'Agree to data processing',
      'Subscribe' => 'Send sms',
      'Check-your-mobile-device' => 'Check your mobile device',
      'Your-verification-code' => 'Your verification code',
      'digit-code' => 'digit code',
      'Traders-Cabinet' => 'Holders',
      'Investors-Cabinet' => 'Stake-holder',
      'Summary' => 'Summary',
      'Total-cost' => 'Total cost',
      'Portfolio-graph' => 'Portfolio graph',
      'List-of-assets' => 'List of assets',
      'Asset' => 'Asset',
      'Asset-Ticker' => 'Asset Ticker',
      'Asset-name' => 'Asset name',
      'Current-value' => 'Current value',
      'Amount-scope-contracts' => 'Amount scope of contracts',
      'amount-total-cost' => 'Total cost',
      'Unused-credit-line' => 'Unused credit line',
      'Amount-outstanding-payments' => 'Amount of outstanding payments for each asset',
      'Next-payment' => 'Next payment',
      'Difficulty-paying' => 'Attention! Difficulty paying?',
      'concluded-contracts' => 'List of concluded contracts',
      'Active-contract' => 'Active contract',
      'Open-contract' => 'Open contract',
      'Date-conclusion' => 'Date of conclusion',
      'Number-of-contract' => 'Number of contract',
      'volume-asset-in-contract' => 'The volume of the asset in the contract',
      'amount-deal' => 'The amount of the deal',
      'amount-paid-payments' => 'Amount of paid payments',
      'Download' => 'Download',
      'Redeem-early' => 'Redeem early',
      'Terminate' => 'Early dissolution',
      'Make-a-deposit' => 'Deposit',
      'Amount-to-be-paid' => 'Amount to be paid',
      'Deposit-term' => 'Deposit term',
      'inv-Deposit-amount' => 'Deposit amount',
      'Select-Investment-Rank' => 'Staking scorring',
      'Investment-Rank' => 'Investment Rank',
      'Withdrawal-amount' => 'Withdrawal amount',
      'Enter-the-amount' => 'Enter the amount',
      'Choose-bank-account' => 'Choose a bank account',
      'Cancel' => 'Cancel',
      'Request' => 'Request',
      'My-contracts' => 'My contracts',
      'Amount-of-investment' => 'Amount of staking',
      'Contract-expiration-date' => 'Contract expiration date',
      'Portfolio-my-active' => 'My Assets',
      'Portfolio-to-pay' => 'To pay',
      'Upload-passport-ID-back' => 'Reverse side of the document',
      'type-doc' => 'Document type',
      'Selected-asset' => 'Selected asset',
      'verify-send-message' => 'Verification request sent',
      'change-password-message' => 'Password changed',
      'request-change-profile-message' => 'Request for data change has been sent',
      'change-profile-save-message' => 'Data saved',
      'invitation-sent-message' => 'The invitation is sent',
      'confirmation-code-sent-message' => 'Confirmation code sent',
      'number-confirmed-message' => 'Number confirmed',
      'user-already-verified' => 'User is already verified.',
      'user-not-found' => 'User not found',
      'validation-error' => 'Validation error',
      'verification-error' => 'Verification error',
      'Total-cost-invest' => 'Investment',
      'Total-cost-percent' => 'Accumulated amount',
      'Conclusion-Investor-Agreement' => 'Conclusion of the Stake-holders Agreement',
      'Invite-friends-Descrip' => 'Enter your friend\'s email or copy the referral link',
      'investor-Invite-upload-digital-photos' => 'To confirm you are you, please upload digital photos of the following ducuments',
      'sidebar-Credit-line' => 'Available by credit line',
      'sidebar-Profit' => 'Profit',
      'sidebar-Available-balance' => 'Left credit line',
      'sidebar-inv-Deposit' => 'Loaded money',
      'sidebar-inv-Profit' => 'Free actives',
      'sidebar-inv-Available-balance' => 'Funds in staking',
      'contract-sell-active' => 'Sell asset',
      'trader-contract-profit-lost' => 'Profit/loss',
      'contract-inv-terminate-title' => 'Early termination',
      'contract-inv-terminate-investing' => 'Investment',
      'contract-inv-terminate-profit-amount' => 'Accumulated amount',
      'contract-inv-terminate-commission-system' => 'System commission',
      'contract-inv-terminate-get' => 'Get',
      'contract-inv-terminate-btn' => 'Get',
      'contract-inv-terminate-message-success' => 'Termination has been submitted. Wait for confirmation',
      'close-contract' => 'Closed',
      'contract-trader-sell-title' => 'Sell asset',
      'contract-trader-sell-cost' => 'The current value of the asset',
      'contract-trader-sell-debit' => 'Remaining principal debt',
      'contract-trader-sell-commission' => 'System commission',
      'contract-trader-sell-get' => 'Withdraw funds',
      'contract-trader-sell-btn' => 'Withdraw',
      'contract-trader-redeem-title' => 'Repay early',
      'contract-trader-redeem-cost' => 'The current value of the asset',
      'contract-trader-redeem-debit' => 'Remaining principal debt',
      'contract-trader-redeem-btn' => 'Pay',
      'contract-trader-redeem-payment' => 'Pay',
      'wait-close-contract' => 'Request has been sent',
      'sidebar-close-trader-contracts' => 'Archive of contracts',
      'sidebar-close-investor-contracts' => 'Archive of contracts',
      'trader-title-archive-contracts' => 'Closed contracts',
      'investor-title-archive-contracts' => 'Closed contracts',
      'sidebar-available-payout' => 'My balance',
      'trader-payout-title' => 'Withdrawal',
      'trader-payout-amount' => 'Withdrawal amount',
      'trader-payout-amount-placeholder' => 'Withdraw',
      'trader-payout-select-paymethod' => 'Select account',
      'trader-payout-btn-request' => 'Request',
      'Investment-Rank-verify' => 'Scorring done',
      'contract-inv-terminate-get-percent' => 'Accumulated %',
      'amount-asset' => 'Amount of the asset',
      'trader-free-active' => 'Available assets',
      'sidebar-inv-courses' => 'Exchange Rate',
      'inv-sidebar-calc-title' => 'Calculator for EURO conversion',
      'inv-sidebar-withdraw-dai-title' => 'Withdraw coins',
      'pay-to-dai-funds' => 'Deposit in DAI',
      'сhoose-wallet-number' => 'Enter your wallet number',
      'btn-transfer-to-investor' => 'Transfer to stake-holder account',
      'sidebar-title-calc-payot' => 'Assets to euro converter',
      'sidebar-cost-active' => 'Asset value',
      'sidebar-title-transfer-active' => 'Transfer of assets',
      'sidebar-btn-transfer-active' => 'Transfer assets',
      'sidebar-title-buy-active' => 'Buy asset',
      'sidebar-btn-buy-active' => 'Buy asset',
      'sidebar-payot-error-amount' => 'The amount is higher than permitted',
      'modal-title-transfer-investor' => 'Application for transferring free funds to the stake-holder\'s account',
      'modal-amount-transfer-investor' => 'Transfer amount',
      'modal-btn-transfer-investor' => 'Transfer',
      'inv-payin-field-from' => 'To',
      'inv-payin-field-to' => 'From',
      'inv-payin-field-amount' => 'Amount',
      'inv-payin-dai' => 'Deposit funds',
      'sidebar-conversion_fee' => 'Commission',
      'sidebar-to-conclusion' => 'To withdraw',
      'bonuses-name' => 'Name of the invitee',
      'bonuses-type' => 'Contract type',
      'bonuses-date' => 'Date of creation',
      'bonuses-amount' => 'Bonus amount',
      'bonuses-payout' => 'Withdraw on holders',
      'bonuses-payout-dai' => 'Move to staking',
      'bonuses-total' => 'Bonuses\' balance',
      'bonuses-registered' => 'Registered',
      'bonuses-earned' => 'Earned',
      'bonuses-withdrawn' => 'Withdrawn',
      'bonuses-copy-url' => 'Copy',
      'bonuses-details' => 'Detailing by bonuses',
      'bonuses-start' => 'Start',
      'convert-title' => 'OTC',
      'convert-description' => 'Change criptocurrency on one platform',
      'convert-placeholder-amount' => 'Введите сумму в пределах 20-2500000',
      'convert-placeholder-amount_asset' => 'Введите сумму в пределах 0.0002-2',
      'convert-asset-from' => 'Из',
      'convert-asset-to' => 'B',
      'convert-btn-buy' => 'Купить',
      'convert-btn-sell' => 'Продать',
      'convert-success' => 'Запрос успешно отправлен',
      'my-balance-money' => 'Баланс монет',
      'btn-pay-to' => 'Внести',
      'investor-asset1' => 'Staking for euro',
      'investor-asset2' => 'Staking in coins',
      'inv-sidebar-calc-money-title' => 'Монета',
      'withdrawal-select-symbol' => 'Выберите монету',
    ),
    'register-rules' => 'Agree with terms',
    'register-18' => 'I am 18',
    'register-trader' => 'Holder',
    'register-investor' => 'Stake-holder',
    'register-have-account' => 'Have account?',
    'register-Sign-Up' => 'Sign Up',
    'register-First-Name' => 'First Name',
    'register-Last-Name' => 'Last Name',
    'register-Email-Address' => 'Email Address',
    'register-Password' => 'Password',
    'register-Confirm-Password' => 'Confirm Password',
    'register-title' => 'Sign Up',
    'login-forgot' => 'Forgot password?',
    'login-dont-have-account' => 'Dont have account?',
    'register-thanks' => 'Thank you for your registration!',
    'register-send-email' => 'We have sent on your mail for activation',
    'register-link-activation' => 'Go to link for activation',
    'register-send-repeat' => 'Resend',
    'register-phone' => 'Phone',
    'register' => 
    array (
      'send-sms' => 'Send code',
    ),
    'calc-investor-annual-interest-rate' => 'Planned profitability',
    'login-login' => 'Login',
    'form-payment-title' => 'Payment form',
    'form-payment-name' => 'Receiver name',
    'form-payment-country-bank' => 'Country of bank',
    'form-payment-name-bank' => 'Name of the bank',
    'form-payment-address-delivery' => 'Address of the recipient',
    'form-payment-swift' => 'SWIFT/BIC',
    'register-is_ur' => 'Legal entity',
    'form-payment-bank_address' => 'Address of the bank',
    'form-payment-iban' => 'Recipient IBAN',
    'form-payment-number-pay' => 'An invoice for payment №',
    'form-payment-amount' => 'Amount',
    'credit-level-ur-company_name' => 'Company name',
    'credit-level-ur-reg_number' => 'Registration number',
    'credit-level-ur-register_date' => 'Registration date',
    'credit-level-ur-ur_address' => 'Actual address',
    'credit-level-ur-address' => 'Actual address',
    'credit-level-ur-contact_phone' => 'Contact number',
    'credit-level-ur-email' => 'Email',
    'credit-level-ur-primary_occupation' => 'Primary occupation',
    'credit-level-ur-title21' => 'Enterprise owners',
    'credit-level-ur-name' => 'Name',
    'credit-level-ur-surname' => 'Surname',
    'credit-level-ur-personal_cod' => 'Identification number',
    'credit-level-ur-title22' => 'Persons with the right of representation',
    'credit-level-ur-capital_amount' => 'Capital shares / shares (number and ratio in %)',
    'credit-level-ur-creditor' => 'Creditor',
    'credit-level-ur-start_amount' => 'Initial amount',
    'credit-level-ur-balance_obligations' => 'Remaining liabilities',
    'credit-level-ur-credit_date_to' => 'Maturity',
    'credit-level-ur-month_payments' => 'Monthly payment',
    'credit-level-ur-credit_target' => 'Purpose of using the loan',
    'credit-level-ur-credit_source' => 'Sources of loan repayment',
    'credit-level-ur-title3' => 'Existing obligations',
    'credit-level-ur-person_name' => 'Name',
    'credit-level-ur-person_surname' => 'Surname',
    'credit-level-ur-person_personal_code' => 'Identification number',
    'credit-level-ur-person_turnover_prev' => 'Turnover for the previous reporting period',
    'credit-level-ur-person_profit_prev' => 'Profit for the previous reporting period',
    'credit-level-ur-person_banks' => 'Banks in which accounts are opened',
    'credit-level-ur-add-owners' => '+ add another owner',
    'credit-level-ur-personal_code' => 'Identification number',
    'calc-trader-title2' => 'Profit in 2020',
    'register-message-register' => 'Check your email and click on the link to verify.',
    'register-message-verify-error' => 'Sorry your link cannot be identified.',
    'register-message-verify-success' => 'Your e-mail is verified. You can now login.',
    'register-message-resend' => 'Check your email and click on the link to verify.',
    'investor' => 
    array (
      'anket-title' => 'Customer’s questionnaire',
      'anket-phiz-title1' => 'Customer’s personal data',
      'anket-source_business_activities' => 'Salary',
      'anket-full_name' => 'Name and Surname',
      'anket-birthday' => 'Date of birth',
      'anket-bin' => 'ID code',
      'anket-place_birth' => 'Place of Birth',
      'anket-position' => 'Position',
      'anket-doc_name' => 'Name of the document serving as the basis for the right of representation',
      'anket-county' => 'Country of tax residence',
      'anket-address' => 'Residential address',
      'anket-phone' => 'Telephone number',
      'anket-email' => 'Email',
      'anket-professional_activities' => 'Economic or professional activity',
      'anket-volume-transactions' => 'Estimated total volume of transactions per calendar year',
      'anket-volume1' => 'until 1000 €',
      'anket-volume2' => '1000€ - 5000€',
      'anket-volume3' => '5.000€ - 15.000€',
      'anket-volume4' => '15.000€ - 50.000€',
      'anket-volume5' => 'over 50.000€',
      'anket-ur-title2' => 'Personal data of the representative',
      'anket-title-source_business_activities' => 'Source of finance',
      'anket-source_dividend' => 'Business income and dividends',
      'anket-source_loans' => 'Loans',
      'anket-source_income_assets' => 'Income from sale of assets',
      'anket-source_contributions' => 'Contributions etc.',
      'anket-source_prize' => 'Prize',
      'anket-ur-source_business_activities' => 'Business activity',
      'anket-ur-source_dividend' => 'Dividends',
      'anket-source_other' => 'Other',
      'anket-source_country' => 'Country of origin of funds',
      'anket-data-beneficiary' => 'Beneficiary data',
      'anket-data-beneficiary-text' => 'The actual beneficiaries of the company are the following persons',
      'anket-ur-data-beneficiary-text' => 'The actual beneficiaries of the company are the following persons',
      'anket-i-agree-beneficiary' => 'I confirm that I am the actual beneficial owner of the proposed transaction <br> or',
      'anket-real-beneficiary' => 'namely, to perform the transaction, the real beneficiary is',
      'anket-beneficiary_name' => 'if the beneficial owner is another person, the same questionnaire must be completed with the data of the beneficial owner',
      'anket-ur-beneficiary_name' => 'if the beneficial owner is another person, the same questionnaire must be completed with the data of the beneficial owner',
      'anket-title3' => 'Data of the politically exposed person',
      'anket-pep' => 'I certify that neither I nor my representative is a politically exposed person or a member of his or her family or a close associate (PEP*)<br> or',
      'anket-politics' => 'I am associated with the following politically exposed persons, their family members or close associates',
      'anket-send-anket-text' => 'By submitting the questionnaire, I confirm that the information provided is correct.',
      'anket-pep1' => '*PEP means a natural person who performs or has performed important functions of public authority during the last year or who is a family member or close associate of such person. The head of state, the head of government, a minister, a deputy minister, an assistant minister performs important functions of public authority; member of the parliament; Judge of the Supreme Court, the Constitutional Court and other higher courts; member of the national supervisory authority and the supervisory board of the central bank; Ambassador, Chargé d\'Affaires and Senior Officer of the Defense Forces; member of the management, supervisory and administrative body of a state-owned company.',
      'anket-pep2' => 'A family member is a spouse or mate; child; the child\'s other parent; the child\'s spouse or mate.
A close colleague is an individual who has a close business relationship with a public authority or with whom the public authority is a joint beneficial owner of a legal person or a contractual legal entity; a person who, as the beneficial owner, has full legal personality or a contractual legal entity known to be established for the performance of essential functions of public authority',
      'anket-ur-title1' => 'Client data',
      'anket-ur-company_name' => 'Company name',
      'anket-ur-company_reg_number' => 'Registration code or number',
      'anket-ur-company_date_reg' => 'Registration date',
      'anket-ur-company_country_reg' => 'Country of registration',
      'anket-ur-company_address' => 'Actual address',
      'anket-ur-company_ur_address' => 'Legal address (if different from the actual address)',
      'anket-ur-is-company_ur_address' => 'Filled in automatically if the actual address match by clicking on the checkmark',
      'anket-ur-company_field_activity' => 'Field of activity',
      'anket-ur-company_phone' => 'Phone number',
      'anket-ur-company_email' => 'Email',
    ),
    'modal' => 
    array (
      'btn-delete-block' => 'Delete',
    ),
    'register-already-register' => 'Phone is already register',
    'credit-level-ur-not-credit_source' => 'No credit obligations',
    'home-example-calc' => 'An example of calculating the purchase of an asset in the amount of 1000 euros per year',
    'home-example-calc-date' => 'Date',
    'home-example-calc-main-debt' => 'Main amount',
    'home-example-calc-balance-owed' => 'Residual amount',
    'home-example-calc-per-month' => 'Commission per month',
    'home-example-calc-amount' => 'Amount',
    'form-trader-loan-month2' => 'Staking term in months',
    'form-investor-loan-month' => 'Staking term in months',
    'form-investor-loan-month2' => 'Staking term in months',
    'form-payment-penalty' => 'Fine',
    'form-payment-amount-all' => 'Total to pay',
    'form-status-block-text' => 'Your account has been suspended. Please pay for the obligations under the contract. In case of questions, please contact support@assetscore.io.',
    'our-packages-investor' => 'Customer status.
After passing the scoring, each client will be assigned a status that will open access to one of three packages of start-up capital.',
    'investor-term1' => '3 Month',
    'investor-term2' => '6 months',
    'investor-term3' => '9 months',
    'investor-title-term' => 'TERM',
    'investor-title-stake' => 'CRO STAKE',
    'investor-stake1' => '400 or less',
    'investor-stake2' => '4,000',
    'investor-stake3' => '40,000 or more',
    'investor-btn-text' => 'Register now',
    'investor-title-reward-rates' => 'Current token reward rates',
    'investor-term4' => '12 months',
    'investor-term5' => '24 months',
    'investor-term6' => '36 months',
    'investor-term7' => '48 months',
    'investor-term8' => '60 months',
  ),
  'header' => 
  array (
    'profile' => 'Profile',
    'magazine' => 'Magazine',
    'help' => 'Help',
    'logout' => 'Logout',
    'login' => 'Login',
    'bonuses' => 'Detailing bonuses',
  ),
  'menu' => 
  array (
    'main' => 'Home',
    'about' => 'About',
    'faq' => 'FAQ',
    'news' => 'News',
    'investors' => 'Stake holders',
    'contacts' => 'Contacts',
    'statistics' => 'Statistics',
    'Payments' => 'Payments',
    'Assets' => 'Assets',
    'Users' => 'Users',
    'Configuration' => 'Configuration',
    'Agreements' => 'Agreements',
    'Portfolio' => 'Portfolio',
    'bonuses' => 'Bonuses',
    'detail-bonuses' => 'Detailing bonuses',
  ),
  'footer' => 
  array (
    'Company' => 'Company',
    'rights' => 'All rights reserved',
    'Certificates-licenses' => 'Certificates and licenses',
    'subscriber' => 'SUBSCRIBE TO NEWS',
    'Enter-e-mail' => 'Enter your e-mail',
    'Subscribe' => 'Subscribe',
    'Contacts' => 'Contacts',
    'Links' => 'Links',
    'Requisites' => 'Requisites',
  ),
  'news' => 
  array (
    'title' => 'News',
    'most-interesting' => 'The most interesting',
    'Read-more' => 'Read more',
    'Most-Popular' => 'Most Popular',
    'Categories' => 'Categories',
    'Last news' => 'Last news',
  ),
  'doc' => 
  array (
    'type-passport' => 'passport',
    'type-id-card' => 'id-card',
    'type-residence-permit' => 'Residence permit',
  ),
  'cabinet' => 
  array (
    'credit-level-info-gender' => 'Your gender',
    'phone-empty' => 'Phone number is empty.',
    'Verification-code' => 'Verification code',
    'Code-already-send' => 'Code is already send',
    'Incorrect-verify-code' => 'Incorrect verify code',
    'credit-line-lt' => 'The line of credit is less than the amount of the contract!',
    'form-error' => 'An error has occurred',
    'form-requested-send' => 'Application submitted',
    'subscribe-error' => 'Email is already exists',
    '2gaf-invalid-verification-code' => 'Invalid Verification Code, Please try again.',
    '2gaf-password-not-matches' => 'Your password does not matches with your account password. Please try again.',
    '2gaf-message-enable' => 'g2fa is Enable',
    '2gaf-message-disabled' => '2FA is now Disabled',
    '2gaf-placeholder-verify-code' => 'Verification Code(6 digit PIN)',
    '2gaf-btn-disable' => 'Disable 2-Step Verification',
    '2gaf-btn-enable' => 'Enable 2-Step Verification',
    '2gaf-title-verify-code' => 'Verification code Google',
    'form-contract-create' => 'Holder\'s contract successfully created',
    'contract-pay-ticket' => 'Make payment',
    'credit-level-questionnaire' => 'Application form',
    'credit-level-info-client' => 'Client information',
    'credit-level-info-Personal-code' => 'Identification number',
    'credit-level-info-Family-status' => 'Family status',
    'credit-level-info-Phone-number' => 'Phone number',
    'credit-level-info-Political-functions' => 'Political functions are performed',
    'credit-level-info-Dependents' => 'Dependents',
    'credit-level-info-Accommodation-type' => 'Accommodation type',
    'credit-level-info-Education' => 'Education',
    'credit-level-info-Citizenship' => 'Citizenship',
    'credit-level-info-next-step' => 'The next step',
    'credit-level-Location' => 'Location',
    'credit-level-Country' => 'Country',
    'credit-level-City' => 'City, region',
    'credit-level-Street' => 'Street, house number, apartment or county',
    'credit-level-postcode' => 'Please enter your postcode',
    'credit-level-Income-information' => 'Income information',
    'credit-level-Income-Source' => 'Source of income',
    'credit-level-Income-Net' => 'Net income, EUR',
    'credit-level-Income-Additional' => 'Additional income, EUR',
    'credit-level-Place-work' => 'Place of work',
    'credit-level-Position' => 'Position',
    'credit-level-experience-job' => 'Work experience from the last job',
    'credit-level-Credit-liabilities' => 'Credit liabilities',
    'credit-level-have-credit-obligations' => 'Do you have credit obligations?',
    'credit-level-Monthly-payment' => 'Monthly payment for general loan obligations, EUR',
    'credit-level-Loan-information' => 'Loan information',
    'credit-level-Loan-Select-type' => 'Select type of loan',
    'credit-level-Credit-term' => 'Credit term',
    'credit-level-Credit-From' => 'From',
    'credit-level-Credit-To' => 'To',
    'credit-level-Account-statement' => 'Account statement',
    'credit-level-quicker-consideration' => 'For a quicker consideration of the application, please attach the following documents',
    'credit-level-statement-last-6' => 'Account statement for the last 6 months (including all types of transactions, no filters)',
    'credit-level-uploaded-documents' => 'Files of uploaded documents must be in PDF format and not exceed 1.5 MB / file.',
    'credit-level-Certificate-place-work' => 'Certificate of income from the place of work',
    'credit-level-Bill-utilities' => 'Bill for utilities / household services with the name, surname of the owner and address',
    'credit-level-Provide-two-doc' => 'Provide two of these documents',
    'credit-level-agree' => 'I agree with the terms of service, I allow the processing of my personal data and confirm that all data provided is true.',
    'credit-level-Thank-you' => 'Thank you, we have received your questionnaire and will contact you shortly',
    'credit-level-Attach-PDF' => 'Attach a PDF file',
    'user-not-verification' => 'You are not verified',
    'user-not-credit-level' => 'You don\'t have a credit level',
    'credit-level-yes' => 'yes',
    'credit-level-no' => 'no',
    'credit-level-male' => 'male',
    'credit-level-female' => 'female',
    'credit-level-sex-other' => 'other',
    'credit-level-type-own' => 'own',
    'credit-level-type-rent' => 'rent',
    'credit-level-type-with-parents' => 'with parents',
    'credit-level-education-high' => 'high',
    'credit-level-education-middle' => 'middle',
    'credit-level-education-initial' => 'initial',
    'credit-income-salary' => 'salary',
    'credit-income-pension' => 'pension',
    'credit-income-allowance' => 'allowance',
    'credit-income-from' => 'business income',
    'credit-credit-consumer' => 'consumer credit',
    'credit-credit-consolidation' => 'consolidation of loans into one',
    'credit-credit-auto' => 'auto leasing',
    'credit-credit-loan' => 'loan for home or apartment renovation',
    'credit-credit-land-loan' => 'land loan',
    'credit-credit-secured-loan' => 'loan secured by real estate',
    'credit-credit-business-loan' => 'business loan',
    'credit-credit-student-loan' => 'student loan',
    'credit-credit-other' => 'other',
    'form-payContract-send' => 'The payment was successfully sent. Awaiting confirmation',
    'form-sellContract-send' => 'Withdrawal request submitted. Wait for confirmation',
    'credit-level-Bill-utilities-files' => 'Load at least 2 docs',
    'trader-payment-expired' => 'payment is overdue',
    'investor-deposit-pay-send' => 'Deposit request successfully sent',
    'investor-deposit-pay-out-send' => 'Withdrawal request successfully sent',
    'Incorrect-payout-amount\'' => 'The withdrawal amount is more free assets',
    'form-transferToInvestor-send' => 'Request to transfer funds from assets has been sent',
    'form-transferAsset-send' => 'Withdrawal request successfully sent',
    'form-buyAsset-send' => 'Assets have been successfully added',
    'Incorrect-payout-amount' => 'The amount is higher than permitted',
    'investor-deposit-dai-error-balance' => 'Not enough free assets',
    'form-browse-file' => 'Pay',
    'form-un-correct-amount' => 'Un correct amount',
    'contract-pay-ticket_modal' => 'Attach a payment order from the bank',
    'form-browse-file_modal' => 'Attach file',
    'contract-investor-table-amount-dai' => 'Accumulated in',
    'contract-investor-table-amount' => 'Accumulated amount',
    'contract-investor-table-dai' => 'DAI',
    'contract-investor-table-percent' => 'Accrued %',
    'contract-investor-table-date' => 'Date',
    'contract-trader-table-date' => 'Date',
    'contract-trader-table-main-debt' => 'Main debt',
    'contract-trader-table-other-debt' => 'Remaining debt',
    'contract-trader-table-percent' => '% month',
    'contract-trader-table-amount' => 'Amount',
    'investor-deposit-pay-in-send' => 'Запрос на пополнение успешно отправлен',
  ),
  'form' => 
  array (
    'message-send' => 'Message sent',
    'message-error' => 'Error sending',
  ),
  'active' => 
  array (
    'type-1' => 'Asset 1',
    'type-2' => 'Asset 2',
  ),
  'user' => 
  array (
    'phone-verify' => 'Verified',
    'phone-un-verify' => 'Not verified (enter sms below)',
    'phone-btn-verify' => 'Confirm',
    'phone-repeat-send' => 'Resend via',
  ),
  'auth' => 
  array (
    'login' => 
    array (
      'email-not-verify' => 'Your email has not been verified',
    ),
  ),
  'mail' => 
  array (
    'verify-subject' => 'Verification',
    'verify-body' => 'Verification passed successfully.',
    'verify-body-reject' => 'Verification denied, please try again.',
    'credit-level-subject' => 'Credit level',
    'credit-level-approve' => 'The credit level has been successfully established.',
    'credit-level-reject' => 'Credit level denied, please try again.',
  ),
);
