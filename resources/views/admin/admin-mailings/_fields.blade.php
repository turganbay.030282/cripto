<div class="form-group">
    {!! Form::label('subject', 'Тема письма') !!}
    {{ Form::text('subject', null, ['class' => 'form-control',]) }}
    {!! Form::error('subject') !!}
</div>
<div class="form-group">
    {!! Form::label('body', 'Текст письма') !!}
    {{ Form::textarea('body', null, ['class' => 'editor form-control summernote', 'rows' => 4]) }}
    {!! Form::error('body') !!}
</div>
<div class="form-group">
    {!! Form::label('list_user', 'Список пользователей') !!}
    {{ Form::select('list_user', $listListUser, null, ['class' => 'form-control']) }}
    {!! Form::error('list_user') !!}
</div>
<div class="form-group form-group-users">
    {!! Form::label('users', 'Пользователи') !!}
    {!! Form::select('users[]', $users, null, ['class' => 'form-control select-list', 'multiple' => true]) !!}
    {!! Form::error('users') !!}
</div>
{{ Form::btn_save() }}


@section('script2')
    <script>
        $('#list_user').on('change', function(){
            if ($(this).val() == 'only_select'){
                $('.form-group-users').removeClass('hide');
            }else{
                $('.form-group-users').addClass('hide');
            }
        });
    </script>
@endsection
