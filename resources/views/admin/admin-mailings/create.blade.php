@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Рассылка: Создать</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::open(['url' => route('admin.admin-mailings.store')]) !!}
                        @include('admin.admin-mailings._fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
