@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Рассылка</h3>
@endsection

@section('resource-content')

    <div class="page-filter clearfix">
        <form action="?" method="GET">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="id" class="col-form-label">ID</label>
                        <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        {!! Form::error('id') !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="subject" class="col-form-label">Тема письма</label>
                        <input id="subject" class="form-control" name="subject" value="{{ request('subject') }}">
                        {!! Form::error('subject') !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="list_user" class="col-form-label">Список пользователей</label>
                        <select id="list_user" class="form-control" name="list_user">
                            <option value=""></option>
                            @foreach ($listListUser as $value => $label)
                                <option value="{{ $value }}" {{ $value == request('list_user') ? 'selected' : '' }}>{{ $label }}</option>
                            @endforeach
                        </select>
                        {!! Form::error('list_user') !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group text-right">
                        <label class="col-form-label">&nbsp;</label><br />
                        <button title="Поиск" type="submit" class="btn bg-orange"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <a title="Очистить" href="?" class="btn btn-default"><i class="fa fa-retweet" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <div class="row">
                        <div class="col-xs-12">
                            <a href="{{ route('admin.admin-mailings.create') }}" class="btn btn-success btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Создать</a>
                        </div>
                    </div>

                    <table class="table table-hover jambo_table">
                        <thead>
                        <tr>
                            <th>@sortablelink('id', 'ID')</th>
                            <th>@sortablelink('list_user', 'Список пользователей')</th>
                            <th>@sortablelink('subject', 'Тема письма')</th>
                            <th>@sortablelink('created_at', 'Дата создания')</th>
                            <th class="text-right">Действия</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($items as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name_list_user }}</td>
                                <td>{{ $item->subject}}</td>
                                <td>{{ $item->created_at->format("d.m.Y H:i:s") }}</td>
                                <td class="text-right">
                                    <a href="{{ route('admin.admin-mailings.show', $item) }}" class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    {{ $items->appends(request()->all())->links() }}

                </div>
            </div>
        </div>
    </div>

@endsection
