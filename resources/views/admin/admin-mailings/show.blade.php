@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Рассылка: просмотр {{ $adminMailing->id }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $adminMailing->id }}</td>
                            </tr>
                            <tr>
                                <th>Список пользователей</th><td>{{ $adminMailing->name_list_user }}</td>
                            </tr>
                            <tr>
                                <th>Тема письма</th><td>{{ $adminMailing->subject }}</td>
                            </tr>
                            <tr>
                                <th>Текст письма</th><td>{{ $adminMailing->body }}</td>
                            </tr>
                            <tr>
                                <th>Дата создания</th><td>{{ $adminMailing->created_at->format("d.m.Y H:i:s") }}</td>
                            </tr>
                        </tbody>
                    </table>

                    @if($adminMailingUsers->isNotEmpty())
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>Пользователи:</th>
                                    <td>
                                        @foreach($adminMailingUsers as $adminMailingUser)
                                            <a href="{{ route('admin.user.users.show', $adminMailingUser) }}" target="_blank">
                                                {{ $adminMailingUser->full_name }}
                                            </a>
                                        @endforeach
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
