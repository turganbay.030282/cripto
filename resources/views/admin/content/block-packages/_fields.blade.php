<div class="form-group">
    {!! Form::label('photo', 'Изображение') !!}
    @if($image)
        <div>
            <a href="{{ $blockPackage->getPhotoUrl() }}" class="popup">
                <img src="{{ $blockPackage->getPhotoUrl() }}" style="height: 40px;width: auto;">
            </a>
        </div>
    @endif
    {{ Form::file('photo', ['class' => 'form-control']) }}
    {{ Form::error('photo') }}
</div>

<div class="form-group">
    <div class="checkbox">
        <label>
            {!! Form::radio('type', 1, !$blockPackage ? true : null) !!} Трейдерам
        </label>
    </div>
    <div class="checkbox">
        <label>
            {!! Form::radio('type', 2) !!} Инвесторам
        </label>
    </div>
    {{ Form::error('type') }}
</div>

<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[name]', 'Название') !!}
                    {{ Form::text($locale.'[name]', $blockPackage ? $blockPackage->{'name:'.$locale} : null, ['class' => 'form-control', 'id' => 'title_'.$locale]) }}
                    {{ Form::error($locale . '.name') }}
                </div>
                <div class="form-group">
                    {!! Form::label($locale.'[price]', 'Цена') !!}
                    {{ Form::text($locale.'[price]', $blockPackage ? $blockPackage->{'price:'.$locale} : null, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.price') }}
                </div>

                <div class="form-group">
                    {!! Form::label($locale.'[desc]', 'Описание') !!}
                    {{ Form::textarea($locale.'[desc]', $blockPackage ? $blockPackage->{'desc:'.$locale} : null, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.desc') }}
                </div>
            </div>
        @endforeach
    </div>
</div>

{{ Form::btn_save() }}
