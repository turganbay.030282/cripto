@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Наши пакеты: изменить {{ $blockPackage->title }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($blockPackage, ['method' => 'PUT', 'url' => route('admin.content.block-packages.update', $blockPackage), 'enctype' => 'multipart/form-data']) !!}
                        @include('admin.content.block-packages._fields', ['image' => $blockPackage->getPhotoUrl()])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
