@extends('admin.layouts.resource-layout')

@section('title')
    <h3>{{ $isArchive ? 'Архив' : '' }} Наши пакеты</h3>
@endsection

@section('resource-content')

    <div class="page-filter clearfix">
        <form action="?" method="GET">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="id" class="col-form-label">ID</label>
                        <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        {!! Form::error('id') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="name" class="col-form-label">Название</label>
                        <input id="name" class="form-control" name="name" value="{{ request('name') }}">
                        {!! Form::error('name') !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="price" class="col-form-label">Цена</label>
                        <input id="price" class="form-control" name="price" value="{{ request('price') }}">
                        {!! Form::error('price') !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="desc" class="col-form-label">Описание</label>
                        <input id="desc" class="form-control" name="desc" value="{{ request('desc') }}">
                        {!! Form::error('desc') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group text-right">
                        <label class="col-form-label">&nbsp;</label><br />
                        <button title="Поиск" type="submit" class="btn bg-orange"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <a title="Очистить" href="?" class="btn btn-default"><i class="fa fa-retweet" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <div class="row">
                        <div class="col-xs-6">
                            <a href="{{ route('admin.content.block-packages.create') }}" class="btn btn-success btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Создать</a>
                        </div>
                        <div class="col-xs-6 text-right">
                            @if($isArchive)
                                <a href="{{route("admin.content.block-packages.index")}}" class="btn btn-info btn-sm">
                                    <i class="fa fa-archive" aria-hidden="true"></i></i> Активные
                                </a>
                            @else
                                <a href="{{route("admin.content.block-packages.archive")}}" class="btn btn-warning btn-sm">
                                    <i class="fa fa-archive" aria-hidden="true"></i></i> Архив
                                </a>
                            @endif
                        </div>
                    </div>

                    <table class="table table-hover  table-border jambo_table">
                        <thead>
                            <tr>
                                <th>@sortablelink('id', 'ID')</th>
                                <th>@sortablelink('name', 'Название')</th>
                                <th>@sortablelink('price', 'Цена')</th>
                                <th>@sortablelink('desc', 'Описание')</th>
                                <th>@sortablelink('type', 'Страница')</th>
                                <th>Изображение</th>
                                <th class="text-right">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>
                                        <a href="{{ route('admin.content.block-packages.show', $item) }}">{{ $item->name }}</a>
                                    </td>
                                    <td>{{ $item->price }}</td>
                                    <td>{{ $item->desc }}</td>
                                    <td>
                                        @if($item->type == 1)
                                            трейдерам
                                        @else
                                            инвесторам
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ $item->getPhotoUrl() }}" class="popup">
                                            <img src="{{ $item->getPhotoUrl() }}" alt="" style="height: 40px;width: auto;">
                                        </a>
                                    </td>
                                    <td class="text-right">
                                        @if($isArchive)
                                            {!! Form::open(['url' => route('admin.content.block-packages.restore', $item), 'class' => 'form-inline-block']) !!}
                                                <input type="hidden" name="id" value="{{ $item->id }}">
                                                <div class="btn-group">
                                                    <button class="btn btn-success btn-xs"><i class="fa fa-reply" aria-hidden="true"></i></button>
                                                </div>
                                            {!! Form::close() !!}
                                        @else
                                            <a href="{{ route('admin.content.block-packages.show', $item) }}" class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            <a href="{{ route('admin.content.block-packages.edit', $item) }}" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'url' => route('admin.content.block-packages.destroy', $item), 'class' => 'form-inline-block']) !!}
                                                <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                            {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $items->appends(request()->all())->links() }}

                </div>
            </div>
        </div>
    </div>

@endsection
