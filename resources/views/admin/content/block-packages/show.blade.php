@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Наши пакеты: просмотр {{ $blockPackage->title }}</h3>
@endsection

@section('resource-content')
    <div class="title_right">
        <a href="{{ route("admin.content.block-packages.edit", $blockPackage) }}" class="btn btn-warning btn-sm">
            <i class="fa fa-pencil-square-o"></i> Изменить
        </a>
        {!! Form::open(['method' => 'DELETE', 'url' => route('admin.content.block-packages.destroy', $blockPackage), 'class' => 'form-inline-block']) !!}
            <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Удалить</button>
        {!! Form::close() !!}
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $blockPackage->id }}</td>
                            </tr>
                            <tr>
                                <th>Название</th><td>{{ $blockPackage->name }}</td>
                            </tr>
                            <tr>
                                <th>Цена</th><td>{{ $blockPackage->price }}</td>
                            </tr>
                            <tr>
                                <th>Описание</th><td>{{ $blockPackage->desc }}</td>
                            </tr>
                            <tr>
                                <th>Изображение</th>
                                <td>
                                    <a href="{{ $blockPackage->getPhotoUrl() }}" class="popup">
                                        <img src="{{ $blockPackage->getPhotoUrl() }}" style="height: 40px;width: auto;">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Страница</th>
                                <td>
                                    @if($blockPackage->type == 1)
                                        трейдерам
                                    @else
                                        инвесторам
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
