<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[title]', 'Название') !!}
                    {{ Form::text($locale.'[title]', $blogCategory ? $blogCategory->{'title:'.$locale} : null, ['class' => 'form-control', 'id' => 'title_'.$locale]) }}
                    {{ Form::error($locale . '.title') }}
                </div>
            </div>
        @endforeach
    </div>
</div>

<div class="form-group">
    {!! Form::label('slug', 'Slug') !!}
    {{ Form::text('slug', null, ['class' => 'form-control title', 'id' => 'slug']) }}
    {{ Form::error('slug') }}
</div>

{{ Form::btn_save() }}
