@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Категории новостей: Изменить {{ $blogCategory->title }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($blogCategory, ['url' => route('admin.content.blog-categories.update', $blogCategory), 'method' => 'PUT']) !!}
                        @include('admin.content.blog-categories._fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
