<div class="form-group">
    {!! Form::hidden('publish', 1) !!}
    <div class="checkbox">
        <label>
            {!! Form::checkbox('publish', 2) !!}Опубликовано
        </label>
    </div>
    {{ Form::error('publish') }}
</div>

<div class="form-group">
    {!! Form::label('categories', 'Категории') !!}
    {!! Form::select('categories[]', $categories, null, ['class' => 'form-control select-list', 'multiple' => true]) !!}
    {{ Form::error('categories') }}
</div>

<h3>Изображение</h3>
<div class="imageWrapper">
    <div class="image_container">
        <div class="portfolio-image portfolio-image-1" data-key="1" style="width: {{$setting->crop_blogs_width}}px; height: {{$setting->crop_blogs_height}}px;">
            <div class="chess_underlay" style="width: {{$setting->crop_blogs_width}}px; height: {{$setting->crop_blogs_height}}px;">
                <img class="crop-image imgNew" src="{{ $image }}">
            </div>
            <label title="Загрузить изображение" for="inputImage1" class="file-btn">
                {!! Form::file('photo', ['class' => 'hide origin-file', 'id' => 'inputImage1', 'accept' => 'image/*']) !!}
                Загрузить
            </label>
            {!! Form::hidden('crop', 0, ['class' => 'hide crop-file']) !!}
        </div>
        {{ Form::error('photo') }}
    </div>
</div>

<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[title]', 'Заголовок') !!}
                    {{ Form::text($locale.'[title]', $blog ? $blog->{'title:'.$locale} : null, ['class' => 'form-control', 'id' => 'title_'.$locale]) }}
                    {{ Form::error($locale . '.title') }}
                </div>

                <div class="form-group">
                    {!! Form::label($locale.'[description]', 'Описание') !!}
                    {{ Form::textarea($locale.'[description]', $blog ? $blog->{'description:'.$locale} : null, ['class' => 'form-control', 'rows' => 4]) }}
                    {{ Form::error($locale . '.description') }}
                </div>

                <div class="form-group">
                    {!! Form::label($locale.'[content]', 'Контент') !!}
                    {{ Form::textarea($locale.'[content]', $blog ? $blog->{'content:'.$locale} : null, ['class' => 'editor summernote form-control']) }}
                    {{ Form::error($locale . '.content') }}
                </div>

                <hr>
                <h3>SEO</h3>

                <div class="form-group">
                    {!! Form::label($locale.'[meta_title]', 'Meta title') !!}
                    {{ Form::text($locale.'[meta_title]', $blog ? $blog->{'meta_title:'.$locale} : null, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.meta_title') }}
                </div>

                <div class="form-group">
                    {!! Form::label($locale.'[meta_desc]', 'Meta description') !!}
                    {{ Form::textarea($locale.'[meta_desc]', $blog ? $blog->{'meta_desc:'.$locale} : null, ['class' => 'form-control', 'rows' => 3]) }}
                    {{ Form::error($locale . '.meta_desc') }}
                </div>
            </div>
        @endforeach
    </div>
</div>

<div class="form-group">
    {!! Form::label('slug', 'Slug') !!}
    {{ Form::text('slug', null, ['class' => 'form-control title', 'id' => 'slug']) }}
    {{ Form::error('slug') }}
</div>

{{ Form::btn_save() }}
