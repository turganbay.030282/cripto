@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Новости: создать</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::open(['url' => route('admin.content.blogs.store'), 'enctype' => 'multipart/form-data']) !!}
                        @include('admin.content.blogs._fields', ['image' => null, 'blog' => null])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    @include('admin.parts._crop', ['width' => $setting->crop_blog_width, 'height' => $setting->crop_blog_height])
@endsection
