@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Новости: изменить {{ $blog->title }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($blog, ['method' => 'PUT', 'url' => route('admin.content.blogs.update', $blog), 'enctype' => 'multipart/form-data']) !!}
                        @include('admin.content.blogs._fields', ['image' => $blog->getCropPhotoUrl()])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    @include('admin.parts._crop', ['width' => $setting->crop_blog_width, 'height' => $setting->crop_blog_height])
@endsection
