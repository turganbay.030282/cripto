@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Новости: просмотр {{ $blog->title }}</h3>
@endsection

@section('resource-content')
    <div class="title_right">
        <a href="{{ route("admin.content.blogs.edit", $blog) }}" class="btn btn-warning btn-sm">
            <i class="fa fa-pencil-square-o"></i> Изменить
        </a>
        {!! Form::open(['method' => 'DELETE', 'url' => route('admin.content.blogs.destroy', $blog), 'class' => 'form-inline-block']) !!}
            <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Удалить</button>
        {!! Form::close() !!}
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $blog->id }}</td>
                            </tr>
                            <tr>
                                <th>Название</th><td>{{ $blog->title }}</td>
                            </tr>
                            <tr>
                                <th>Slug</th><td>{{ $blog->slug }}</td>
                            </tr>
                            <tr>
                                <th>Опубликовано</th>
                                <td>
                                    @if ($blog->isPublish())
                                        <span class="label label-success">Да</span>
                                    @else
                                        <span class="label label-default">Нет</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Изображение</th>
                                <td>
                                    <a href="{{ $blog->getPhotoUrl() }}" class="popup">
                                        <img src="{{ $blog->getCropPhotoUrl() }}" alt="" style="height: 40px;width: auto;">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Количество просмотров</th><td>{{ $blog->count_view }}</td>
                            </tr>
                            <tr>
                                <th>Автор</th><td>{{ $blog->author->full_name }}</td>
                            </tr>
                            <tr>
                                <th>Описание</th><td>{{ $blog->description }}</td>
                            </tr>
                            <tr>
                                <th>Контент</th><td>{{ $blog->content }}</td>
                            </tr>
                            <tr>
                                <th>Meta title</th><td>{{ $blog->meta_title }}</td>
                            </tr>
                            <tr>
                                <th>Meta description</th><td>{{ $blog->meta_desc }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
