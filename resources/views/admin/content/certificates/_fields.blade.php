<h3>Изображение</h3>
<div class="imageWrapper">
    <div class="image_container">
        <div class="portfolio-image portfolio-image-1" data-key="1" style="width: {{$setting->crop_cert_width}}px; height: {{$setting->crop_cert_height}}px;">
            <div class="chess_underlay" style="width: {{$setting->crop_cert_width}}px; height: {{$setting->crop_cert_height}}px;">
                <img class="crop-image imgNew" src="{{ $image }}">
            </div>
            <label title="Загрузить изображение" for="inputImage1" class="file-btn">
                {!! Form::file('photo', ['class' => 'hide origin-file', 'id' => 'inputImage1', 'accept' => 'image/*']) !!}
                Загрузить
            </label>
            {!! Form::hidden('crop', 0, ['class' => 'hide crop-file']) !!}
        </div>
        {{ Form::error('photo') }}
    </div>
</div>

<div class="form-group">
    {!! Form::label('file', 'Файл') !!}
    @if($certificate && $certificate->file)
        <div>
            <a href="{{ $certificate->getFileUrl() }}" target="_blank">
                {{ $certificate->file }}
            </a>
        </div>
    @endif
    {{ Form::file('file', ['class' => 'form-control']) }}
    {{ Form::error('file') }}
</div>

<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[title]', 'Название') !!}
                    {{ Form::text($locale.'[title]', $certificate ? $certificate->{'title:'.$locale} : null, ['class' => 'form-control title']) }}
                    {{ Form::error($locale . '.title') }}
                </div>

                <div class="form-group">
                    {!! Form::label($locale.'[description]', 'Описание') !!}
                    {{ Form::textarea($locale.'[description]', $certificate ? $certificate->{'description:'.$locale} : null, ['class' => 'form-control', 'rows' => 4]) }}
                    {{ Form::error($locale . '.description') }}
                </div>
            </div>
        @endforeach
    </div>
</div>

{{ Form::btn_save() }}
