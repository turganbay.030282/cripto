@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Сертификаты: создать</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::open(['url' => route('admin.content.certificates.store'), 'enctype' => 'multipart/form-data']) !!}
                        @include('admin.content.certificates._fields', ['image' => null, 'certificate' => null])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @include('admin.parts._crop', ['width' => $setting->crop_cert_width, 'height' => $setting->crop_cert_height])
@endsection
