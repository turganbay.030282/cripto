@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Сертификаты: просмотр {{ $certificate->title }}</h3>
@endsection

@section('resource-content')
    <div class="title_right">
        <a href="{{ route("admin.content.certificates.edit", $certificate) }}" class="btn btn-warning btn-sm">
            <i class="fa fa-pencil-square-o"></i> Изменить
        </a>
        {!! Form::open(['method' => 'DELETE', 'url' => route('admin.content.certificates.destroy', $certificate), 'class' => 'form-inline-block']) !!}
            <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Удалить</button>
        {!! Form::close() !!}
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $certificate->id }}</td>
                            </tr>
                            <tr>
                                <th>Название</th><td>{{ $certificate->title }}</td>
                            </tr>
                            <tr>
                                <th>Изображение</th>
                                <td>
                                    <a href="{{ $certificate->getPhotoUrl() }}" class="popup">
                                        <img src="{{ $certificate->getCropPhotoUrl() }}" alt="" style="height: 40px;width: auto;">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Файл</th>
                                <td>
                                    <a href="{{ $certificate->getFileUrl() }}" target="_blank">
                                        {{ $certificate->file }}
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Описание</th><td>{{ $certificate->description }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
