<div class="form-group">
    {!! Form::hidden('is_convert', 0) !!}
    <div class="checkbox">
        <label>
            {!! Form::checkbox('is_convert', 1) !!} Для страницы конвертация
        </label>
    </div>
    {{ Form::error('is_convert') }}
</div>

<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[title]', 'Название') !!}
                    {{ Form::text($locale.'[title]', $faqCategory ? $faqCategory->{'title:'.$locale} : null, ['class' => 'form-control', 'id' => 'title_'.$locale]) }}
                    {{ Form::error($locale . '.title') }}
                </div>

                <div class="form-group">
                    {!! Form::label($locale.'[description]', 'Описание') !!}
                    {{ Form::textarea($locale.'[description]', $faqCategory ? $faqCategory->{'description:'.$locale} : null, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.description') }}
                </div>
            </div>
        @endforeach
    </div>
</div>

{{ Form::btn_save() }}
