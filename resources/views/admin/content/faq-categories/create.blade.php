@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Категории FAQ: Создать</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::open(['url' => route('admin.content.faq-categories.store')]) !!}
                        @include('admin.content.faq-categories._fields', ['faqCategory' => null])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
