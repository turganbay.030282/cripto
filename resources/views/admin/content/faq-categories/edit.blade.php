@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Категории FAQ: Изменить {{ $faqCategory->title }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($faqCategory, ['url' => route('admin.content.faq-categories.update', $faqCategory), 'method' => 'PUT']) !!}
                        @include('admin.content.faq-categories._fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
