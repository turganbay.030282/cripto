@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Категории FAQ: просмотр {{ $faqCategory->title }}</h3>
@endsection

@section('btn')
    <div class="title_right">
        <a href="{{route("admin.content.faq-categories.edit", $faqCategory)}}" class="btn btn-primary btn-xs">
            <i class="fa fa-pencil"></i> Изменить
        </a>
        {!! Form::open(['url' => route('admin.content.faq-categories.destroy', $faqCategory), 'method' => 'DELETE', 'class' => 'form-inline-block']) !!}
        <div class="btn-group">
            <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Удалить</button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $faqCategory->id }}</td>
                            </tr>
                            <tr>
                                <th>Название</th><td>{{ $faqCategory->title }}</td>
                            </tr>
                            <tr>
                                <th>Описание</th><td>{{ $faqCategory->description }}</td>
                            </tr>
                            <tr>
                                <th>Для страницы конвертация</th><td>{{ $faqCategory->is_convert ? 'Да' : 'Нет'}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
