<div class="form-group">
    {!! Form::label('faq_category_id', 'Категория') !!}
    {!! Form::select('faq_category_id', $categories, null, ['class' => 'form-control']) !!}
    {{ Form::error('faq_category_id') }}
</div>

<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[title]', 'Название') !!}
                    {{ Form::text($locale.'[title]', $faq ? $faq->{'title:'.$locale} : null, ['class' => 'form-control', 'id' => 'title_'.$locale]) }}
                    {{ Form::error($locale . '.title') }}
                </div>

                <div class="form-group">
                    {!! Form::label($locale.'[description]', 'Описание') !!}
                    {{ Form::textarea($locale.'[description]', $faq ? $faq->{'description:'.$locale} : null, ['class' => 'editor summernote form-control']) }}
                    {{ Form::error($locale . '.description') }}
                </div>
            </div>
        @endforeach
    </div>
</div>

{{ Form::btn_save() }}
