@extends('admin.layouts.resource-layout')

@section('title')
    <h3>FAQ: Изменить {{ $faq->title }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($faq, ['url' => route('admin.content.faqs.update', $faq), 'method' => 'PUT']) !!}
                        @include('admin.content.faqs._fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
