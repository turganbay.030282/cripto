<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[title]', 'Заголовок') !!}
                    {{ Form::text($locale.'[title]', $pageAbout->{'title:'.$locale}, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.title') }}
                </div>
                <div class="form-group">
                    {!! Form::label($locale.'[desc]', 'Описание') !!}
                    {{ Form::text($locale.'[desc]', $pageAbout->{'desc:'.$locale}, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.desc') }}
                </div>
                <div class="form-group">
                    {!! Form::label($locale.'[btn_text]', 'Кнопка') !!}
                    {{ Form::text($locale.'[btn_text]', $pageAbout->{'btn_text:'.$locale}, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.btn_text') }}
                </div>
                <hr>
                <h4>Блок преимущества</h4>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_title]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_title]', $pageAbout->{'benefit_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_title') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_sub_title]', 'Подзаголовок') !!}
                            {{ Form::text($locale.'[benefit_sub_title]', $pageAbout->{'benefit_sub_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_sub_title') }}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_title]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_b_title]', $pageAbout->{'benefit_b_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_title') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_desc]', 'Описание') !!}
                            {{ Form::text($locale.'[benefit_b_desc]', $pageAbout->{'benefit_b_desc:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_desc') }}
                        </div>
                        @if($key == 0)
                            <div class="form-group">
                                {!! Form::label('benefit_b_icon', 'Иконка') !!}
                                <div>
                                    <a href="{{ $pageAbout->getPhotoUrl('benefit_b_icon') }}" class="popup">
                                        <img src="{{ $pageAbout->getPhotoUrl('benefit_b_icon') }}" style="height: 40px;width: auto;">
                                    </a>
                                </div>
                                {{ Form::file('benefit_b_icon', ['class' => 'form-control']) }}
                                {{ Form::error('benefit_b_icon') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_title2]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_b_title2]', $pageAbout->{'benefit_b_title2:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_title2') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_desc2]', 'Описание') !!}
                            {{ Form::text($locale.'[benefit_b_desc2]', $pageAbout->{'benefit_b_desc2:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_desc2') }}
                        </div>
                        @if($key == 0)
                            <div class="form-group">
                                {!! Form::label('benefit_b_icon2', 'Иконка') !!}
                                <div>
                                    <a href="{{ $pageAbout->getPhotoUrl('benefit_b_icon2') }}" class="popup">
                                        <img src="{{ $pageAbout->getPhotoUrl('benefit_b_icon2') }}" style="height: 40px;width: auto;">
                                    </a>
                                </div>
                                {{ Form::file('benefit_b_icon2', ['class' => 'form-control']) }}
                                {{ Form::error('benefit_b_icon2') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_title3]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_b_title3]', $pageAbout->{'benefit_b_title3:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_title3') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_desc3]', 'Описание') !!}
                            {{ Form::text($locale.'[benefit_b_desc3]', $pageAbout->{'benefit_b_desc3:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_desc3') }}
                        </div>
                        @if($key == 0)
                            <div class="form-group">
                                {!! Form::label('benefit_b_icon3', 'Иконка') !!}
                                <div>
                                    <a href="{{ $pageAbout->getPhotoUrl('benefit_b_icon3') }}" class="popup">
                                        <img src="{{ $pageAbout->getPhotoUrl('benefit_b_icon3') }}" style="height: 40px;width: auto;">
                                    </a>
                                </div>
                                {{ Form::file('benefit_b_icon3', ['class' => 'form-control']) }}
                                {{ Form::error('benefit_b_icon3') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_title4]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_b_title4]', $pageAbout->{'benefit_b_title4:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_title4') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_desc4]', 'Описание') !!}
                            {{ Form::text($locale.'[benefit_b_desc4]', $pageAbout->{'benefit_b_desc4:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_desc4') }}
                        </div>
                        @if($key == 0)
                            <div class="form-group">
                                {!! Form::label('benefit_b_icon4', 'Иконка') !!}
                                <div>
                                    <a href="{{ $pageAbout->getPhotoUrl('benefit_b_icon4') }}" class="popup">
                                        <img src="{{ $pageAbout->getPhotoUrl('benefit_b_icon4') }}" style="height: 40px;width: auto;">
                                    </a>
                                </div>
                                {{ Form::file('benefit_b_icon4', ['class' => 'form-control']) }}
                                {{ Form::error('benefit_b_icon4') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_title5]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_b_title5]', $pageAbout->{'benefit_b_title5:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_title5') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_desc5]', 'Описание') !!}
                            {{ Form::text($locale.'[benefit_b_desc5]', $pageAbout->{'benefit_b_desc5:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_desc5') }}
                        </div>
                        @if($key == 0)
                            <div class="form-group">
                                {!! Form::label('benefit_b_icon5', 'Иконка') !!}
                                <div>
                                    <a href="{{ $pageAbout->getPhotoUrl('benefit_b_icon5') }}" class="popup">
                                        <img src="{{ $pageAbout->getPhotoUrl('benefit_b_icon5') }}" style="height: 40px;width: auto;">
                                    </a>
                                </div>
                                {{ Form::file('benefit_b_icon5', ['class' => 'form-control']) }}
                                {{ Form::error('benefit_b_icon5') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_title6]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_b_title6]', $pageAbout->{'benefit_b_title6:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_title6') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_desc6]', 'Описание') !!}
                            {{ Form::text($locale.'[benefit_b_desc6]', $pageAbout->{'benefit_b_desc6:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_desc6') }}
                        </div>
                        @if($key == 0)
                            <div class="form-group">
                                {!! Form::label('benefit_b_icon6', 'Иконка') !!}
                                <div>
                                    <a href="{{ $pageAbout->getPhotoUrl('benefit_b_icon6') }}" class="popup">
                                        <img src="{{ $pageAbout->getPhotoUrl('benefit_b_icon6') }}" style="height: 40px;width: auto;">
                                    </a>
                                </div>
                                {{ Form::file('benefit_b_icon6', ['class' => 'form-control']) }}
                                {{ Form::error('benefit_b_icon6') }}
                            </div>
                        @endif
                    </div>
                </div>

                <hr>
                <h4>Блок приоритеты</h4>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            {!! Form::label($locale.'[priority_title]', 'Заголовок') !!}
                            {{ Form::text($locale.'[priority_title]', $pageAbout->{'priority_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.priority_title') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[priority_sub_title]', 'Подзаголовок') !!}
                            {{ Form::text($locale.'[priority_sub_title]', $pageAbout->{'priority_sub_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.priority_sub_title') }}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            {!! Form::label($locale.'[priority_b_title]', 'Заголовок') !!}
                            {{ Form::text($locale.'[priority_b_title]', $pageAbout->{'priority_b_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.priority_b_title') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[priority_b_desc]', 'Описание') !!}
                            {{ Form::textarea($locale.'[priority_b_desc]', $pageAbout->{'priority_b_desc:'.$locale}, ['class' => 'editor form-control summernote']) }}
                            {{ Form::error($locale . '.priority_b_desc') }}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            {!! Form::label($locale.'[priority_b_title2]', 'Заголовок') !!}
                            {{ Form::text($locale.'[priority_b_title2]', $pageAbout->{'priority_b_title2:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.priority_b_title2') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[priority_b_desc2]', 'Описание') !!}
                            {{ Form::textarea($locale.'[priority_b_desc2]', $pageAbout->{'priority_b_desc2:'.$locale}, ['class' => 'editor form-control summernote']) }}
                            {{ Form::error($locale . '.priority_b_desc2') }}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            {!! Form::label($locale.'[priority_b_title3]', 'Заголовок') !!}
                            {{ Form::text($locale.'[priority_b_title3]', $pageAbout->{'priority_b_title3:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.priority_b_title3') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[priority_b_desc3]', 'Описание') !!}
                            {{ Form::textarea($locale.'[priority_b_desc3]', $pageAbout->{'priority_b_desc3:'.$locale}, ['class' => 'editor form-control summernote']) }}
                            {{ Form::error($locale . '.priority_b_desc3') }}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            {!! Form::label($locale.'[priority_b_title4]', 'Заголовок') !!}
                            {{ Form::text($locale.'[priority_b_title4]', $pageAbout->{'priority_b_title4:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.priority_b_title4') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[priority_b_desc4]', 'Описание') !!}
                            {{ Form::textarea($locale.'[priority_b_desc4]', $pageAbout->{'priority_b_desc4:'.$locale}, ['class' => 'editor form-control summernote']) }}
                            {{ Form::error($locale . '.priority_b_desc4') }}
                        </div>
                    </div>
                </div>

            </div>
        @endforeach
    </div>
</div>

{{ Form::btn_save() }}
