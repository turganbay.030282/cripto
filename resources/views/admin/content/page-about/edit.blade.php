@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Страница О нас: изменить</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($pageAbout, ['method' => 'PUT', 'url' => route('admin.content.page-about.update', $pageAbout), 'enctype' => 'multipart/form-data']) !!}
                        @include('admin.content.page-about._fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
