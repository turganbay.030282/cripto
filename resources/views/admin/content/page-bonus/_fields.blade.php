<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[title]', 'Заголовок') !!}
                    {{ Form::text($locale.'[title]', $pageBonus->{'title:'.$locale}, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.title') }}
                </div>
                <div class="form-group">
                    {!! Form::label($locale.'[subtitle]', 'Подзаголовок') !!}
                    {{ Form::text($locale.'[subtitle]', $pageBonus->{'subtitle:'.$locale}, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.subtitle') }}
                </div>
                <div class="form-group">
                    {!! Form::label($locale.'[title1]', 'Заголовок 1') !!}
                    {{ Form::text($locale.'[title1]', $pageBonus->{'title1:'.$locale}, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.title1') }}
                </div>
                <div class="form-group">
                    {!! Form::label($locale.'[content1]', 'Описание 1') !!}
                    {{ Form::textarea($locale.'[content1]', $pageBonus->{'content1:'.$locale}, ['class' => 'editor summernote form-control']) }}
                    {{ Form::error($locale . '.content1') }}
                </div>
                <div class="form-group">
                    {!! Form::label($locale.'[title2]', 'Заголовок 2') !!}
                    {{ Form::text($locale.'[title2]', $pageBonus->{'title2:'.$locale}, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.title2') }}
                </div>
                <div class="form-group">
                    {!! Form::label($locale.'[content2]', 'Описание 2') !!}
                    {{ Form::textarea($locale.'[content2]', $pageBonus->{'content2:'.$locale}, ['class' => 'editor summernote form-control']) }}
                    {{ Form::error($locale . '.content2') }}
                </div>
                <div class="form-group">
                    {!! Form::label($locale.'[title3]', 'Заголовок 3') !!}
                    {{ Form::text($locale.'[title3]', $pageBonus->{'title3:'.$locale}, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.title3') }}
                </div>
                <div class="form-group">
                    {!! Form::label($locale.'[content3]', 'Описание 3') !!}
                    {{ Form::textarea($locale.'[content3]', $pageBonus->{'content3:'.$locale}, ['class' => 'editor summernote form-control']) }}
                    {{ Form::error($locale . '.content3') }}
                </div>
            </div>
        @endforeach
    </div>
</div>

{{ Form::btn_save() }}
