@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Страница Бонусы: изменить</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($pageBonus, ['method' => 'PUT', 'url' => route('admin.content.page-bonus.update', $pageBonus), 'enctype' => 'multipart/form-data']) !!}
                        @include('admin.content.page-bonus._fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
