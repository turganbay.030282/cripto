<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[title]', 'Заголовок') !!}
                    {{ Form::text($locale.'[title]', $pageInvestor->{'title:'.$locale}, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.title') }}
                </div>
                <div class="form-group">
                    {!! Form::label($locale.'[desc]', 'Описание') !!}
                    {{ Form::text($locale.'[desc]', $pageInvestor->{'desc:'.$locale}, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.desc') }}
                </div>
                <div class="form-group">
                    {!! Form::label($locale.'[btn_text]', 'Кнопка') !!}
                    {{ Form::text($locale.'[btn_text]', $pageInvestor->{'btn_text:'.$locale}, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.btn_text') }}
                </div>

                <hr>
                <h4>Блок о нас</h4>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            {!! Form::label($locale.'[about_title]', 'Заголовок') !!}
                            {{ Form::text($locale.'[about_title]', $pageInvestor->{'about_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.about_title') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[about_sub_title]', 'Подзаголовок') !!}
                            {{ Form::text($locale.'[about_sub_title]', $pageInvestor->{'about_sub_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.about_sub_title') }}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[about_b_title]', 'Заголовок') !!}
                            {{ Form::text($locale.'[about_b_title]', $pageInvestor->{'about_b_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.about_b_title') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[about_b_desc]', 'Описание') !!}
                            {{ Form::text($locale.'[about_b_desc]', $pageInvestor->{'about_b_desc:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.about_b_desc') }}
                        </div>
                        @if($key == 0)
                            <div class="form-group">
                                {!! Form::label('about_b_icon', 'Иконка') !!}
                                <div>
                                    <a href="{{ $pageInvestor->getPhotoUrl('about_b_icon') }}" class="popup">
                                        <img src="{{ $pageInvestor->getPhotoUrl('about_b_icon') }}" style="height: 40px;width: auto;">
                                    </a>
                                </div>
                                {{ Form::file('about_b_icon', ['class' => 'form-control']) }}
                                {{ Form::error('about_b_icon') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[about_b_title2]', 'Заголовок') !!}
                            {{ Form::text($locale.'[about_b_title2]', $pageInvestor->{'about_b_title2:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.about_b_title2') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[about_b_desc2]', 'Описание') !!}
                            {{ Form::text($locale.'[about_b_desc2]', $pageInvestor->{'about_b_desc2:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.about_b_desc2') }}
                        </div>
                        @if($key == 0)
                            <div class="form-group">
                                {!! Form::label('about_b_icon2', 'Иконка') !!}
                                <div>
                                    <a href="{{ $pageInvestor->getPhotoUrl('about_b_icon2') }}" class="popup">
                                        <img src="{{ $pageInvestor->getPhotoUrl('about_b_icon2') }}" style="height: 40px;width: auto;">
                                    </a>
                                </div>
                                {{ Form::file('about_b_icon2', ['class' => 'form-control']) }}
                                {{ Form::error('about_b_icon2') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[about_b_title3]', 'Заголовок') !!}
                            {{ Form::text($locale.'[about_b_title3]', $pageInvestor->{'about_b_title3:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.about_b_title3') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[about_b_desc3]', 'Описание') !!}
                            {{ Form::text($locale.'[about_b_desc3]', $pageInvestor->{'about_b_desc3:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.about_b_desc3') }}
                        </div>
                        @if($key == 0)
                            <div class="form-group">
                                {!! Form::label('about_b_icon3', 'Иконка') !!}
                                <div>
                                    <a href="{{ $pageInvestor->getPhotoUrl('about_b_icon3') }}" class="popup">
                                        <img src="{{ $pageInvestor->getPhotoUrl('about_b_icon3') }}" style="height: 40px;width: auto;">
                                    </a>
                                </div>
                                {{ Form::file('about_b_icon3', ['class' => 'form-control']) }}
                                {{ Form::error('about_b_icon3') }}
                            </div>
                        @endif
                    </div>
                </div>

                <hr>
                <h4>Блок преимущества</h4>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_title]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_title]', $pageInvestor->{'benefit_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_title') }}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_title]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_b_title]', $pageInvestor->{'benefit_b_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_title') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_desc]', 'Описание') !!}
                            {{ Form::text($locale.'[benefit_b_desc]', $pageInvestor->{'benefit_b_desc:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_desc') }}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_title2]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_b_title2]', $pageInvestor->{'benefit_b_title2:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_title2') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_desc2]', 'Описание') !!}
                            {{ Form::text($locale.'[benefit_b_desc2]', $pageInvestor->{'benefit_b_desc2:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_desc2') }}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_title3]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_b_title3]', $pageInvestor->{'benefit_b_title3:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_title3') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_desc3]', 'Описание') !!}
                            {{ Form::text($locale.'[benefit_b_desc3]', $pageInvestor->{'benefit_b_desc3:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_desc3') }}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_title4]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_b_title4]', $pageInvestor->{'benefit_b_title4:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_title4') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_desc4]', 'Описание') !!}
                            {{ Form::text($locale.'[benefit_b_desc4]', $pageInvestor->{'benefit_b_desc4:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_desc4') }}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_title5]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_b_title5]', $pageInvestor->{'benefit_b_title5:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_title5') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_desc5]', 'Описание') !!}
                            {{ Form::text($locale.'[benefit_b_desc5]', $pageInvestor->{'benefit_b_desc5:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_desc5') }}
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_title6]', 'Заголовок') !!}
                            {{ Form::text($locale.'[benefit_b_title6]', $pageInvestor->{'benefit_b_title6:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_title6') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[benefit_b_desc6]', 'Описание') !!}
                            {{ Form::text($locale.'[benefit_b_desc6]', $pageInvestor->{'benefit_b_desc6:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.benefit_b_desc6') }}
                        </div>
                    </div>
                </div>

            </div>
        @endforeach
    </div>
</div>

{{ Form::btn_save() }}
