@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Страница Инвесторы: изменить</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($pageInvestor, ['method' => 'PUT', 'url' => route('admin.content.page-investor.update', $pageInvestor), 'enctype' => 'multipart/form-data']) !!}
                        @include('admin.content.page-investor._fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
