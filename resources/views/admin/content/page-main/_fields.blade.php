<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            {!! Form::label($locale.'[title]', 'Заголовок трейдера') !!}
                            {{ Form::text($locale.'[title]', $pageMain->{'title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.title') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[sub_title]', 'Подзаголовок трейдера') !!}
                            {{ Form::text($locale.'[sub_title]', $pageMain->{'sub_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.sub_title') }}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            {!! Form::label($locale.'[title_inv]', 'Заголовок инвестора') !!}
                            {{ Form::text($locale.'[title_inv]', $pageMain->{'title_inv:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.title_inv') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[sub_title_inv]', 'Подзаголовок инвестора') !!}
                            {{ Form::text($locale.'[sub_title_inv]', $pageMain->{'sub_title_inv:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.sub_title_inv') }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            {!! Form::label($locale.'[title_offer_tr]', 'Заголовок оффер трейдера') !!}
                            {{ Form::text($locale.'[title_offer_tr]', $pageMain->{'title_offer_tr:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.title_offer_tr') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[desc_offer_tr]', 'Описание оффер трейдера') !!}
                            {{ Form::textarea($locale.'[desc_offer_tr]', $pageMain->{'desc_offer_tr:'.$locale}, ['class' => 'editor summernote form-control']) }}
                            {{ Form::error($locale . '.desc_offer_tr') }}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            {!! Form::label($locale.'[title_offer_inv]', 'Заголовок оффер инвестора') !!}
                            {{ Form::text($locale.'[title_offer_inv]', $pageMain->{'title_offer_inv:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.title_offer_inv') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[desc_offer_inv]', 'Описание оффер инвестора') !!}
                            {{ Form::textarea($locale.'[desc_offer_inv]', $pageMain->{'desc_offer_inv:'.$locale}, ['class' => 'editor summernote form-control']) }}
                            {{ Form::error($locale . '.desc_offer_inv') }}
                        </div>
                    </div>
                </div>

                <hr>
                <h4>Блок предложений</h4>

                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            {!! Form::label($locale.'[offer_tr_title]', 'Заголовок') !!}
                            {{ Form::text($locale.'[offer_tr_title]', $pageMain->{'offer_tr_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.offer_tr_title') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[offer_tr_sub_title]', 'Подзаголовок') !!}
                            {{ Form::text($locale.'[offer_tr_sub_title]', $pageMain->{'offer_tr_sub_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.offer_tr_sub_title') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[offer_tr_desc]', 'Описание') !!}
                            {{ Form::textarea($locale.'[offer_tr_desc]', $pageMain->{'offer_tr_desc:'.$locale}, ['class' => 'editor form-control summernote', 'rows' => 4]) }}
                            {{ Form::error($locale . '.offer_tr_desc') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[offer_tr_btn]', 'Кнопка') !!}
                            {{ Form::text($locale.'[offer_tr_btn]', $pageMain->{'offer_tr_btn:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.offer_tr_btn') }}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            {!! Form::label($locale.'[offer_inv_title]', 'Заголовок') !!}
                            {{ Form::text($locale.'[offer_inv_title]', $pageMain->{'offer_inv_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.offer_inv_title') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[offer_inv_sub_title]', 'Подзаголовок') !!}
                            {{ Form::text($locale.'[offer_inv_sub_title]', $pageMain->{'offer_inv_sub_title:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.offer_inv_sub_title') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[offer_inv_desc]', 'Описание') !!}
                            {{ Form::textarea($locale.'[offer_inv_desc]', $pageMain->{'offer_inv_desc:'.$locale}, ['class' => 'editor form-control summernote', 'rows' => 4]) }}
                            {{ Form::error($locale . '.offer_inv_desc') }}
                        </div>
                        <div class="form-group">
                            {!! Form::label($locale.'[offer_inv_btn]', 'Кнопка') !!}
                            {{ Form::text($locale.'[offer_inv_btn]', $pageMain->{'offer_inv_btn:'.$locale}, ['class' => 'form-control']) }}
                            {{ Form::error($locale . '.offer_inv_btn') }}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

{{ Form::btn_save() }}
