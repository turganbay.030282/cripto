@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Страница Home: изменить</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($pageMain, ['method' => 'PUT', 'url' => route('admin.content.page-main.update', $pageMain), 'enctype' => 'multipart/form-data']) !!}
                        @include('admin.content.page-main._fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
