<div class="form-group">
    {!! Form::hidden('publish', 1) !!}
    <div class="checkbox">
        <label>
            {!! Form::checkbox('publish', 2) !!}Опубликовано
        </label>
    </div>
    {{ Form::error('publish') }}
</div>

<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[title]', 'Заголовок') !!}
                    {{ Form::text($locale.'[title]', $page ? $page->{'title:'.$locale} : null, ['class' => 'form-control', 'id' => 'title_'.$locale]) }}
                    {{ Form::error($locale . '.title') }}
                </div>

                <div class="form-group">
                    {!! Form::label($locale.'[content]', 'Контент') !!}
                    {{ Form::textarea($locale.'[content]', $page ? $page->{'content:'.$locale} : null, ['class' => 'editor summernote form-control']) }}
                    {{ Form::error($locale . '.content') }}
                </div>

                <hr>
                <h3>SEO</h3>

                <div class="form-group">
                    {!! Form::label($locale.'[meta_title]', 'Meta title') !!}
                    {{ Form::text($locale.'[meta_title]', $page ? $page->{'meta_title:'.$locale} : null, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.meta_title') }}
                </div>

                <div class="form-group">
                    {!! Form::label($locale.'[meta_desc]', 'Meta description') !!}
                    {{ Form::textarea($locale.'[meta_desc]', $page ? $page->{'meta_desc:'.$locale} : null, ['class' => 'form-control', 'rows' => 3]) }}
                    {{ Form::error($locale . '.meta_desc') }}
                </div>
            </div>
        @endforeach
    </div>
</div>

<div class="form-group">
    {!! Form::label('slug', trans('admin.forms.title-slug'), ['class' => 'col-form-label']) !!}
    {{ Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
    {{ Form::error('slug') }}
</div>

{{ Form::btn_save() }}
