@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Страницы: изменить {{ $page->title }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($page, ['method' => 'PUT', 'url' => route('admin.content.pages.update', $page), 'enctype' => 'multipart/form-data']) !!}
                        @include('admin.content.pages._fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
