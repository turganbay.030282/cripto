@extends('admin.layouts.resource-layout')

@section('title')
    <h3>{{ $isArchive ? 'Архив' : '' }} Страницы</h3>
@endsection

@section('resource-content')

    <div class="page-filter clearfix">
        <form action="?" method="GET">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="id" class="col-form-label">ID</label>
                        <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        {!! Form::error('id') !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="title" class="col-form-label">Название</label>
                        <input id="title" class="form-control" name="title" value="{{ request('title') }}">
                        {!! Form::error('title') !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="publish" class="col-form-label">Опубликовано</label>
                        <select id="publish" class="form-control" name="publish">
                            <option value=""></option>
                            @foreach ($statuses as $value => $label)
                                <option value="{{ $value }}" {{ $value == request('publish') ? 'selected' : '' }}>{{ $label }}</option>
                            @endforeach
                        </select>
                        {!! Form::error('publish') !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group text-right">
                        <label class="col-form-label">&nbsp;</label><br />
                        <button title="Поиск" type="submit" class="btn bg-orange"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <a title="Очистить" href="?" class="btn btn-default"><i class="fa fa-retweet" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <div class="row">
                        <div class="col-xs-6">
                            <a href="{{ route('admin.content.pages.create') }}" class="btn btn-success btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Создать</a>
                        </div>
                        <div class="col-xs-6 text-right">
                            @if($isArchive)
                                <a href="{{route("admin.content.pages.index")}}" class="btn btn-info btn-sm">
                                    <i class="fa fa-archive" aria-hidden="true"></i></i> Активные
                                </a>
                            @else
                                <a href="{{route("admin.content.pages.archive")}}" class="btn btn-warning btn-sm">
                                    <i class="fa fa-archive" aria-hidden="true"></i></i> Архив
                                </a>
                            @endif
                        </div>
                    </div>

                    <table class="table table-hover  table-border jambo_table">
                        <thead>
                            <tr>
                                <th>@sortablelink('id', 'ID')</th>
                                <th>@sortablelink('title', 'Заголовок')</th>
                                <th>@sortablelink('slug', 'Slug')</th>
                                <th>@sortablelink('publish', 'Опубликовано')</th>
                                <th class="text-right">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>
                                        <a href="{{ route('admin.content.pages.show', $item) }}">{{ $item->title }}</a>
                                    </td>
                                    <td>{{ $item->slug }}</td>
                                    <td>
                                        @if ($item->isPublish())
                                            <span class="label label-success">Да</span>
                                        @else
                                            <span class="label label-default">Нет</span>
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        @if($isArchive)
                                            {!! Form::open(['url' => route('admin.content.pages.restore', $item), 'class' => 'form-inline-block']) !!}
                                                <input type="hidden" name="id" value="{{ $item->id }}">
                                                <div class="btn-group">
                                                    <button class="btn btn-success btn-xs"><i class="fa fa-reply" aria-hidden="true"></i></button>
                                                </div>
                                            {!! Form::close() !!}
                                        @else
                                            <a href="{{ route('admin.content.pages.show', $item) }}" class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            <a href="{{ route('admin.content.pages.edit', $item) }}" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'url' => route('admin.content.blogs.destroy', $item), 'class' => 'form-inline-block']) !!}
                                                <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                            {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $items->appends(request()->all())->links() }}

                </div>
            </div>
        </div>
    </div>

@endsection
