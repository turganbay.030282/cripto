@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Страницы: просмотр {{ $page->title }}</h3>
@endsection

@section('resource-content')
    <div class="title_right">
        <a href="{{ route("admin.content.pages.edit", $page) }}" class="btn btn-warning btn-sm">
            <i class="fa fa-pencil-square-o"></i> Изменить
        </a>
        {!! Form::open(['method' => 'DELETE', 'url' => route('admin.content.pages.destroy', $page), 'class' => 'form-inline-block']) !!}
            <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Удалить</button>
        {!! Form::close() !!}
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $page->id }}</td>
                            </tr>
                            <tr>
                                <th>Название</th><td>{{ $page->title }}</td>
                            </tr>
                            <tr>
                                <th>Slug</th><td>{{ $page->slug }}</td>
                            </tr>
                            <tr>
                                <th>Опубликовано</th>
                                <td>
                                    @if ($page->isPublish())
                                        <span class="label label-success">Да</span>
                                    @else
                                        <span class="label label-default">Нет</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Описание</th><td>{{ $page->description }}</td>
                            </tr>
                            <tr>
                                <th>Контент</th><td>{{ $page->content }}</td>
                            </tr>
                            <tr>
                                <th>Meta title</th><td>{{ $page->meta_title }}</td>
                            </tr>
                            <tr>
                                <th>Meta description</th><td>{{ $page->meta_desc }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
