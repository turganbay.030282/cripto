<table class="table table-bordered table-striped">
    <tbody>
        <tr>
            <th>Номер</th>
            <td style="text-align: right">{{ $contract->number }}</td>
        </tr>
        <tr>
            <th>Тип</th>
            <td style="text-align: right">{{ $contract->type }}</td>
        </tr>
        <tr>
            <th>Сумма</th>
            <td style="text-align: right">{{ $contract->amount }}
                @if($contract->isTypeDeposit())
                    = {{ $contract->amount_asset }} DAI
                @endif
            </td>
        </tr>
        <tr>
            <th>Количество</th>
            <td style="text-align: right">{{ $contract->amount_asset }}</td>
        </tr>
        <tr>
            <th>Пользователь</th>
            <td style="text-align: right">{{ $contract->client->full_name }}</td>
        </tr>
        <tr>
            <th>Тип актива</th>
            <td style="text-align: right">
                @if($contract->typeAsset)
                    {{ $contract->typeAsset->name }}
                @endif
            </td>
        </tr>
        <tr>
            <th>Срок</th>
            <td style="text-align: right">{{ $contract->period }}</td>
        </tr>
        <tr>
            <th>Ставка</th>
            <td style="text-align: right">{{ $contract->rate }}</td>
        </tr>
        @if($contract->isTypeDeposit())
            <tr>
                <th>Статус</th>
                <td style="text-align: right">{!! $contract->status_name !!}</td>
            </tr>
        @endif
        <tr>
            <th>Дата создания</th>
            <td style="text-align: right">{{ $contract->created_at->format("d.m.Y H:i:s") }}</td>
        </tr>
    </tbody>
</table>

@if($contract->isTypeCredit())
        <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Дата</th>
            <th>Основной долг</th>
            <th>Остаток долга</th>
            <th>%  мес</th>
            <th>Сумма</th>
            <th>Статус</th>
        </tr>
        </thead>
        <tbody>
        @if($contract->payments->isNotEmpty())
            @foreach($contract->payments as $key => $payment)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $payment->date->format("d.m.Y") }}</td>
                    <td>{{ $payment->main_amount }}</td>
                    <td>{{ $payment->balance_owed }}</td>
                    <td>{{ $payment->percent }}</td>
                    <td>{{ $payment->amount }}</td>
                    <td>
                        @if($payment->pay_ticket)
                            {!! $payment->status_name !!}
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endif

@if($contract->isTypeDeposit())
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Дата</th>
            <th>Начисленный %</th>
            <th>DAI</th>
            <th>Накопленная сумма</th>
        </tr>
        </thead>
        <tbody>
        @if($contract->investProfits->isNotEmpty())
            @foreach($contract->investProfits as $key => $payment)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $payment->date->format("d.m.Y") }}</td>
                    <td>{{ $payment->percent }}</td>
                    <td>{{ $payment->percent_dai }}</td>
                    <td>{{ $payment->amount }}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endif
