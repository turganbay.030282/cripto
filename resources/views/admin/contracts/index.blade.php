@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Договора</h3>
@endsection

@section('resource-content')
    <div class="page-filter clearfix">
        <form action="?" method="GET">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="number" class="col-form-label">Номер</label>
                        <input id="number" class="form-control" name="number" value="{{ request('number') }}">
                        {!! Form::error('number') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="asset_id" class="col-form-label">Тип актива</label>
                        <select id="asset_id" class="form-control" name="asset_id">
                            <option value=""></option>
                            @foreach ($assets as $value => $label)
                                <option value="{{ $value }}" {{ $value == request('asset_id') ? 'selected' : '' }}>{{ $label }}</option>
                            @endforeach
                        </select>
                        {!! Form::error('asset_id') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="period" class="col-form-label">@lang('site.page.cabinet.Term')</label>
                        <input id="period" class="form-control" name="period" value="{{ request('period') }}">
                        {!! Form::error('period') !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="rate" class="col-form-label">Ставка</label>
                        <input id="rate" class="form-control" name="rate" value="{{ request('rate') }}">
                        {!! Form::error('rate') !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group text-right">
                        <label class="col-form-label">&nbsp;</label><br />
                        <button title="Поиск" type="submit" class="btn bg-orange"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <a title="Очистить" href="?" class="btn btn-default"><i class="fa fa-retweet" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-hover jambo_table">
                        <thead>
                        <tr>
                            <th>@sortablelink('number', 'Номер')</th>
                            <th>@sortablelink('type', 'Тип')</th>
                            <th>@sortablelink('amount', 'Сумма')</th>
                            <th>@sortablelink('amount_asset', 'Количество')</th>
                            <th>@sortablelink('client.name', 'Пользователь')</th>
                            <th>@sortablelink('type_asset_id', 'Тип актива')</th>
                            <th>@sortablelink('period', 'Срок')</th>
                            <th>@sortablelink('rate', 'Ставка')</th>
                            <th>@sortablelink('status', 'Статус')</th>
                            <th>@sortablelink('created_at', 'Дата создания')</th>
                            <th class="text-right">Действия</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($items as $item)
                            <tr class="{{ $item->isContractTransactionApprove() ? '' : 'bg-row-red' }}">
                                <td>{{ $item->number }}</td>
                                <td>{{ $item->type }}</td>
                                <td>{{ $item->amount}}</td>
                                <td>{{ $item->amount_asset}}</td>
                                <td>
                                    @if($item->client)
                                        <a href="{{ route('admin.user.users.show', $item->client) }}" target="_blank">
                                            {{ $item->client->full_name }}
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    @if($item->typeAsset)
                                        {{ $item->typeAsset->name }}
                                    @endif
                                </td>
                                <td>{{ $item->period }}</td>
                                <td>{{ $item->rate }}</td>
                                <td>
                                    {{$item->status}}
{{--                                    @if($item->isActive())--}}
{{--                                        <span class="label label-success">Активный</span>--}}
{{--                                    @endif--}}
{{--                                    @if($item->isClose())--}}
{{--                                        <span class="label label-default">Закрыт</span>--}}
{{--                                    @endif--}}
                                </td>
                                <td>{{ $item->created_at->format("d.m.Y H:i:s") }}</td>
                                <td class="text-right">
                                    <a href="{{ route('admin.contracts.pdf', $item) }}" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
                                    <a href="{{ route('admin.contracts.show', $item) }}" class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>

                                    {!! Form::open(['url' => route('admin.contracts.destroy', $item), 'method' => 'DELETE', 'style' => 'display: inline-block;vertical-align: top;']) !!}
                                        <div class="btn-group">
                                            <button class="btn btn-danger btn-xs btn-confirmation"><i class="fa fa-trash-o"></i></button>
                                        </div>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    {{ $items->appends(request()->all())->links() }}

                </div>
            </div>
        </div>
    </div>

@endsection
