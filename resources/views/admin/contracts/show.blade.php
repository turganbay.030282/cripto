@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Договора: просмотр {{ $contract->number }}</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $contract->id }}</td>
                            </tr>
                            <tr>
                                <th>Номер</th><td>{{ $contract->number }}</td>
                            </tr>
                            <tr>
                                <th>Тип</th><td>{{ $contract->type }}</td>
                            </tr>
                            <tr>
                                <th>Сумма</th><td>
                                    @if($contract->isTypeDeposit())
                                        {{ $contract->amount_asset }} {{ $contract->typeAsset->coinValue->symbol ?? '' }}
                                    @else
                                        {{ $contract->amount }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Количество</th><td>{{ $contract->amount_asset }}</td>
                            </tr>
                            <tr>
                                <th>Пользователь</th><td>{{ $contract->client->full_name }}</td>
                            </tr>
                            <tr>
                                <th>Тип актива</th>
                                <td>
                                    @if($contract->typeAsset)
                                        {{ $contract->typeAsset->name }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Срок</th><td>{{ $contract->period }}</td>
                            </tr>
                            <tr>
                                <th>Ставка</th><td>{{ $contract->rate }}</td>
                            </tr>
                            @if($contract->isTypeDeposit())
                            <tr>
                                <th>Квитанция</th>
                                <td>
                                    <a href="{{ $contract->getPhotoUrl('pay_ticket') }}" class="popup">
                                        <img src="{{ $contract->getPhotoUrl('pay_ticket') }}" alt="" style="height: 40px;width: auto;">
                                    </a>
                                    <br>
                                    <a href="{{ $contract->getPhotoUrl('pay_ticket') }}" download="">
                                        <i class="fa fa-download" aria-hidden="true"></i> {{ $contract->pay_ticket }}
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Статус</th><td>{!! $contract->status_name_html !!}</td>
                            </tr>
                            @endif
                            <tr>
                                <th>Дата создания</th><td>{{ $contract->created_at->format("d.m.Y H:i:s") }}</td>
                            </tr>
                        </tbody>
                    </table>

                    @if($contract->isTypeCredit())
                    <h4>график возврата кредита</h4>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Дата</th>
                                <th>Основной долг</th>
                                <th>Остаток долга</th>
                                <th>%  мес</th>
                                <th>Сумма</th>
                                <th>Квитанция</th>
                                <th>Статус</th>
                                <th>Пеня</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($contract->payments->isNotEmpty())
                                @foreach($contract->payments as $key => $payment)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $payment->date->format("d.m.Y") }}</td>
                                        <td>{{ $payment->main_amount }}</td>
                                        <td>{{ $payment->balance_owed }}</td>
                                        <td>{{ $payment->percent }}</td>
                                        <td>{{ $payment->amount }}</td>
                                        <td>
                                            @if($payment->pay_ticket)
                                                <a href="{{ $payment->getPhotoUrl('pay_ticket') }}" download="">
                                                    <i class="fa fa-download" aria-hidden="true"></i> {{ $payment->pay_ticket }}
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($payment->pay_ticket)
                                                {!! $payment->status_name_html !!}
                                            @endif
                                        </td>
                                        <td>{{ $payment->penalty }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    @endif

                    @if($contract->isTypeDeposit())
                    <h4>график начисление по депозиту</h4>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Дата</th>
{{--                                <th>Начисленный %</th>--}}
                                <th>{{ $contract->typeAsset->coinValue->symbol ?? '' }}</th>
{{--                                <th>Накопленная сумма</th>--}}
                                <th>Накопленная в {{ $contract->typeAsset->coinValue->symbol ?? '' }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($contract->investProfits->isNotEmpty())
                                @foreach($contract->investProfits as $key => $payment)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $payment->date->format("d.m.Y") }}</td>
{{--                                        <td>{{ $payment->percent }}</td>--}}
                                        <td>{{ numeric_fmt($payment->percent_dai) }}</td>
{{--                                        <td>{{ $payment->amount }}</td>--}}
                                        <td>{{ numeric_fmt($payment->amount / ($contract->course ? $contract->course : ($contract->typeAsset->coinValue->price ?? 1))) }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection
