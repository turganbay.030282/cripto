@extends('admin.layouts.resource-layout')

@section('title')
    <h3>ОТС</h3>
@endsection

@section('resource-content')
    <div class="page-filter clearfix">
        <form action="?" method="GET">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="client" class="col-form-label">Пользователь</label>
                        <input id="client" class="form-control" name="client" value="{{ request('client') }}">
                        {!! Form::error('client') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="amount_from" class="col-form-label">Сумма от</label>
                        <input id="amount_from" class="form-control" name="amount_from" value="{{ request('amount_from') }}">
                        {!! Form::error('amount_from') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="amount_to" class="col-form-label">Сумма до</label>
                        <input id="amount_to" class="form-control" name="amount_to" value="{{ request('amount_to') }}">
                        {!! Form::error('amount_to') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="from" class="col-form-label">Дата от</label>
                        <input id="from" class="form-control picker-date-format" name="from" value="{{ request('from') }}">
                        {!! Form::error('from') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="to" class="col-form-label">Дата до</label>
                        <input id="to" class="form-control picker-date-format" name="to" value="{{ request('to') }}">
                        {!! Form::error('to') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group text-right">
                        <label class="col-form-label">&nbsp;</label><br />
                        <button title="Поиск" type="submit" class="btn bg-orange"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <a title="Очистить" href="?" class="btn btn-default"><i class="fa fa-retweet" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-hover jambo_table">
                        <thead>
                        <tr>
                            <th>@sortablelink('id', 'ID')</th>
                            <th>@sortablelink('type', 'Тип конвертации')</th>
                            <th>@sortablelink('created_at', 'Дата создания')</th>
                            <th>@sortablelink('client.name', 'Пользователь')</th>
                            <th>@sortablelink('amount', 'Сумма EUR')</th>
                            <th>Вид монеты</th>
                            <th>@sortablelink('amount_asset', 'Кол-во монет')</th>
                            <th>@sortablelink('status', 'Статус')</th>
                            <th class="text-right">Действия</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($items as $item)
                            <tr>
                                <td>
                                    {{ $item->id }}
                                </td>
                                <td>{{ $item->type }}</td>
                                <td>{{ $item->created_at }}</td>
                                <td>
                                    @if($item->client)
                                        <a href="{{ route('admin.user.users.show', $item->client) }}" target="_blank">
                                            {{ $item->client->full_name }}
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    {{ format_number($item->amount) }}
                                </td>
                                <td>{{ $item->typeAsset->name }}</td>
                                <td>{{ numeric_fmt($item->amount_asset) }}</td>
                                <td>{{ $item->status }}</td>
                                <td class="text-right">
                                    @if($item->isNew())
                                        {!! Form::open(['url' => route('admin.converts.close', $item), 'class' => 'form-inline-block']) !!}
                                            <input type="hidden" name="id" value="{{ $item->id }}">
                                            <div class="btn-group">
                                                <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                                            </div>
                                        {!! Form::close() !!}

                                        {!! Form::open(['url' => route('admin.converts.cancel', $item), 'class' => 'form-inline-block']) !!}
                                            <input type="hidden" name="id" value="{{ $item->id }}">
                                            <input type="hidden" name="_method" value="PUT">
                                            <div class="btn-group">
                                                <button class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button>
                                            </div>
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    {{ $items->appends(request()->all())->links() }}

                </div>
            </div>
        </div>
    </div>

@endsection
