<div class="form-group">
    {!! Form::hidden('for_graph', 0) !!}
    <div class="checkbox">
        <label>
            {!! Form::checkbox('for_graph', 1) !!} Отображать на главной
        </label>
    </div>
    {{ Form::error('for_graph') }}
</div>

<div class="form-group">
    {!! Form::hidden('for_trader', 0) !!}
    <div class="checkbox">
        <label>
            {!! Form::checkbox('for_trader', 1) !!} Отображать для трейдера
        </label>
    </div>
    {{ Form::error('for_trader') }}
</div>

<div class="form-group">
    {!! Form::hidden('for_investor', 0) !!}
    <div class="checkbox">
        <label>
            {!! Form::checkbox('for_investor', 1) !!} Отображать для инвестора
        </label>
    </div>
    {{ Form::error('for_investor') }}
</div>

<div class="form-group">
    {!! Form::label('type', 'Тип') !!}
    {{ Form::select('type', $types, null, ['class' => 'form-control']) }}
    {{ Form::error('type') }}
</div>

<div class="form-group">
    {!! Form::label('coinmarketcap_id', 'Актив') !!}
    {{ Form::select('coinmarketcap_id', $coins, null, ['class' => 'form-control select-list-ajax']) }}
    {{ Form::error('coinmarketcap_id') }}
</div>

<div class="form-group">
    {{ Form::label('wallet_number', 'Кошелек номер', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    {{ Form::text('wallet_number', null, ['class' => 'form-control']) }}
    {{ Form::error('wallet_number') }}
</div>

<div class="form-group">
    {{ Form::label('wallet_img', 'Кошелек QR-код', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    @if($asset && $asset->wallet_img)
        <div>
            <a href="{{ $asset->getPhotoUrl('wallet_img') }}" class="popup">
                <img src="{{ $asset->getPhotoUrl('wallet_img') }}" style="height: 40px;width: auto;">
            </a>
        </div>
    @endif
    {{ Form::file('wallet_img', ['class' => 'form-control']) }}
    {{ Form::error('wallet_img') }}
</div>

<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[name]', 'Название') !!}
                    {{ Form::text($locale.'[name]', $asset ? $asset->{'name:'.$locale} : null, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.name') }}
                </div>
            </div>
        @endforeach
    </div>
</div>

<hr>

<table class="table">
    <thead>
        <tr>
            <td></td>
            @for ($i = 1; $i <= 8; $i++)
                <td>{{ trans('site.page.investor-term'.$i) }}</td>
            @endfor
        </tr>
    </thead>
    <tbody>
        @for ($j = 1; $j <= 1; $j++)
            <tr>
                <td>{{ trans("site.page.investor-stake{$j}") }}</td>
                @for ($i = 1; $i <= 8; $i++)
                    <td>
                        {{ Form::text("calc[td{$j}{$i}]", $asset->calc["td{$j}{$i}"] ?? null, ['class' => 'form-control']) }}
                    </td>
                @endfor
            </tr>
        @endfor
    </tbody>
</table>

<hr>

{{ Form::btn_save() }}

@section('script2')
    <script>
        $('.select-list-ajax').select2({
            language: 'ru',
            ajax: {
                url: '{{route('admin.handbook.assets.coins')}}',
                dataType: 'json'
            }
        });
    </script>
@endsection
