@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Активы: Создать</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::open(['url' => route('admin.handbook.assets.store'), 'enctype' => 'multipart/form-data']) !!}
                        @include('admin.handbook.assets._fields', ['asset' => null])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
