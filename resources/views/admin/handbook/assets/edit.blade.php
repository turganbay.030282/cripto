@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Активы: Изменить {{ $asset->name }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($asset, ['url' => route('admin.handbook.assets.update', $asset), 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                        @include('admin.handbook.assets._fields', ['asset' => $asset])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
