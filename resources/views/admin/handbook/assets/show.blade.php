@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Активы: просмотр {{ $asset->name }}</h3>
@endsection

@section('btn')
    <div class="title_right">
        <a href="{{route("admin.handbook.assets.edit", $asset)}}" class="btn btn-primary btn-xs">
            <i class="fa fa-pencil"></i> Изменить
        </a>
        {!! Form::open(['url' => route('admin.handbook.assets.destroy', $asset), 'method' => 'DELETE', 'class' => 'form-inline-block']) !!}
        <div class="btn-group">
            <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Удалить</button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $asset->id }}</td>
                            </tr>
                            <tr>
                                <th>Название</th><td>{{ $asset->name }}</td>
                            </tr>
                            <tr>
                                <th>Символ</th><td>{{ $asset->coinmarketcap_id ? $asset->coinValue->symbol : '' }}</td>
                            </tr>
                            <tr>
                                <th>Отображать на главной</th>
                                <td>
                                    @if ($asset->for_graph)
                                        <span class="label label-success">Да</span>
                                    @else
                                        <span class="label label-default">Нет</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Отображать для трейдера</th>
                                <td>
                                    @if ($asset->for_trader)
                                        <span class="label label-success">Да</span>
                                    @else
                                        <span class="label label-default">Нет</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Отображать для инвестора</th>
                                <td>
                                    @if ($asset->for_investor)
                                        <span class="label label-success">Да</span>
                                    @else
                                        <span class="label label-default">Нет</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Тип</th><td>{{ $asset->type_name }}</td>
                            </tr>
                            <tr>
                                <th>Кошелек номер</th>
                                <td>{{ $asset->wallet_number }}</td>
                            </tr>
                            <tr>
                                <th>Кошелек QR-код</th>
                                <td>
                                    @if($asset && $asset->wallet_img)
                                        <div>
                                            <a href="{{ $asset->getPhotoUrl('wallet_img') }}" class="popup">
                                                <img src="{{ $asset->getPhotoUrl('wallet_img') }}" style="height: 40px;width: auto;">
                                            </a>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
