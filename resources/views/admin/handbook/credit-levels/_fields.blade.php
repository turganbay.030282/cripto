<div class="form-group">
    {!! Form::label('range', 'Диапазон сроков(дней)') !!}
    {{ Form::text('range', null, ['class' => 'form-control']) }}
    {{ Form::error('range') }}
</div>

<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[name]', 'Название') !!}
                    {{ Form::text($locale.'[name]', $creditLevel ? $creditLevel->{'name:'.$locale} : null, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.name') }}
                </div>
            </div>
        @endforeach
    </div>
</div>

{{ Form::btn_save() }}
