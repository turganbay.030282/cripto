@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Кредитный уровень: Создать</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::open(['url' => route('admin.handbook.credit-levels.store')]) !!}
                        @include('admin.handbook.credit-levels._fields', ['creditLevel' => null])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
