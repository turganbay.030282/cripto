@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Кредитный уровень: Изменить {{ $creditLevel->title }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($creditLevel, ['url' => route('admin.handbook.credit-levels.update', $creditLevel), 'method' => 'PUT']) !!}
                        @include('admin.handbook.credit-levels._fields', ['creditLevel' => $creditLevel])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
