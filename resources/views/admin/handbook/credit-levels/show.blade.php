@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Кредитный уровень: просмотр {{ $creditLevel->name }}</h3>
@endsection

@section('btn')
    <div class="title_right">
        <a href="{{route("admin.handbook.credit-levels.edit", $creditLevel)}}" class="btn btn-primary btn-xs">
            <i class="fa fa-pencil"></i> Изменить
        </a>
        {!! Form::open(['url' => route('admin.handbook.credit-levels.destroy', $creditLevel), 'method' => 'DELETE', 'class' => 'form-inline-block']) !!}
        <div class="btn-group">
            <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Удалить</button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $creditLevel->id }}</td>
                            </tr>
                            <tr>
                                <th>Название</th><td>{{ $creditLevel->name }}</td>
                            </tr>
                            <tr>
                                <th>Диапазон сроков(дней)</th><td>{{ $creditLevel->range }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
