@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Ранг Инвестора: Создать</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::open(['url' => route('admin.handbook.investor-ranks.store')]) !!}
                        @include('admin.handbook.investor-ranks._fields', ['investorRank' => null])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
