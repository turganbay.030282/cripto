@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Ранг Инвестора: Изменить {{ $investorRank->title }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($investorRank, ['url' => route('admin.handbook.investor-ranks.update', $investorRank), 'method' => 'PUT']) !!}
                        @include('admin.handbook.investor-ranks._fields', ['investorRank' => $investorRank])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
