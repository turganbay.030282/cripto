@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Ранг Инвестора: просмотр {{ $investorRank->name }}</h3>
@endsection

@section('btn')
    <div class="title_right">
        <a href="{{route("admin.handbook.investor-ranks.edit", $investorRank)}}" class="btn btn-primary btn-xs">
            <i class="fa fa-pencil"></i> Изменить
        </a>
        {!! Form::open(['url' => route('admin.handbook.investor-ranks.destroy', $investorRank), 'method' => 'DELETE', 'class' => 'form-inline-block']) !!}
        <div class="btn-group">
            <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Удалить</button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $investorRank->id }}</td>
                            </tr>
                            <tr>
                                <th>Название</th><td>{{ $investorRank->name }}</td>
                            </tr>
                            <tr>
                                <th>Диапазон сроков(дней)</th><td>{{ $investorRank->range }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
