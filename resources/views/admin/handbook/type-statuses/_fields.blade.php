<div class="form-group hide">
    {!! Form::label('key', 'key') !!}
    {{ Form::text('key', null, ['class' => 'form-control']) }}
    {{ Form::error('key') }}
</div>

<div class="form-group">
    {!! Form::label('name', 'Название') !!}
    {{ Form::text('name', null, ['class' => 'form-control']) }}
    {{ Form::error('name') }}
</div>

{{ Form::btn_save() }}
