@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Тип транзакци: Изменить {{ $typeStatus->name }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($typeStatus, ['url' => route('admin.handbook.type-statuses.update', $typeStatus), 'method' => 'PUT']) !!}
                        @include('admin.handbook.type-statuses._fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
