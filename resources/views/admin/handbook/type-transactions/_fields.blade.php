<div class="form-group">
    {!! Form::label('type', 'Тип') !!}
    {{ Form::select('type', $types, null, ['class' => 'form-control']) }}
    {{ Form::error('type') }}
</div>
<div class="form-group">
    {!! Form::label('value', 'Значение') !!}
    {{ Form::text('value', null, ['class' => 'form-control']) }}
    {{ Form::error('value') }}
</div>
<div class="form-group">
    {!! Form::label('period', 'Срок') !!}
    {{ Form::text('period', null, ['class' => 'form-control']) }}
    {{ Form::error('period') }}
</div>
<div class="form-group">
    {!! Form::label('min', 'Мин') !!}
    {{ Form::text('min', null, ['class' => 'form-control']) }}
    {{ Form::error('min') }}
</div>
<div class="form-group">
    {!! Form::label('max', 'Макс') !!}
    {{ Form::text('max', null, ['class' => 'form-control']) }}
    {{ Form::error('max') }}
</div>
<div class="form-group">
    {!! Form::label('level', 'Уровень') !!}
    {{ Form::text('level', null, ['class' => 'form-control']) }}
    {{ Form::error('level') }}
</div>

<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[name]', 'Название') !!}
                    {{ Form::text($locale.'[name]', $typeTransaction ? $typeTransaction->{'name:'.$locale} : null, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.name') }}
                </div>
            </div>
        @endforeach
    </div>
</div>

{{ Form::btn_save() }}
