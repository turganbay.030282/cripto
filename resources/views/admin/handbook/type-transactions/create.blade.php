@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Параметры пакетов: Создать</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::open(['url' => route('admin.handbook.type-transactions.store')]) !!}
                        @include('admin.handbook.type-transactions._fields', ['typeTransaction' => null])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
