@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Параметры пакетов: Изменить {{ $typeTransaction->title }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($typeTransaction, ['url' => route('admin.handbook.type-transactions.update', $typeTransaction), 'method' => 'PUT']) !!}
                        @include('admin.handbook.type-transactions._fields', ['typeTransaction' => $typeTransaction])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
