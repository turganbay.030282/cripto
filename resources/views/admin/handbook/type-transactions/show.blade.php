@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Параметры пакетов: просмотр {{ $typeTransaction->name }}</h3>
@endsection

@section('btn')
    <div class="title_right">
        <a href="{{route("admin.handbook.type-transactions.edit", $typeTransaction)}}" class="btn btn-primary btn-xs">
            <i class="fa fa-pencil"></i> Изменить
        </a>
        {!! Form::open(['url' => route('admin.handbook.type-transactions.destroy', $typeTransaction), 'method' => 'DELETE', 'class' => 'form-inline-block']) !!}
        <div class="btn-group">
            <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Удалить</button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $typeTransaction->id }}</td>
                            </tr>
                            <tr>
                                <th>Название</th><td>{{ $typeTransaction->name }}</td>
                            </tr>
                            <tr>
                                <th>Тип</th><td>{{ $typeTransaction->type_name }}</td>
                            </tr>
                            <tr>
                                <th>Уровень</th><td>{{ $typeTransaction->level }}</td>
                            </tr>
                            <tr>
                                <th>Значение</th><td>{{ $typeTransaction->value }}</td>
                            </tr>
                            <tr>
                                <th>@lang('site.page.cabinet.Term')</th><td>{{ $typeTransaction->period }}</td>
                            </tr>
                            <tr>
                                <th>Мин</th><td>{{ $typeTransaction->min }}</td>
                            </tr>
                            <tr>
                                <th>Макс</th><td>{{ $typeTransaction->max }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
