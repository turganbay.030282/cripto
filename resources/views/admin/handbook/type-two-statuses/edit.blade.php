@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Тип 2: Изменить {{ $typeTwoStatus->name }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($typeTwoStatus, ['url' => route('admin.handbook.type-two-statuses.update', $typeTwoStatus), 'method' => 'PUT']) !!}
                        @include('admin.handbook.type-two-statuses._fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
