<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Панель администратора</title>

        <!-- bootstrap-datetimepicker -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css">
        <!-- Bootstrap -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="/assets/admin/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="/assets/admin/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet">
        <!-- Switchery -->
        <link href="/assets/admin/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
        <!-- bootstrap-progressbar -->
        <link href="/assets/admin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- Fancybox -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        @yield('stylesheet')
        <link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz&display=swap" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="/assets/admin/build/css/custom.min.css?v=1.1.1" rel="stylesheet">
        <link href="/assets/admin/build/css/app.css?v=1.0.8" rel="stylesheet">

    </head>
    <body class="nav-md">


    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="{{route('front.home')}}" class="site_title"><i class="fa fa-buysellads"></i> <span>{{ env('APP_NAME') }}</span></a>
                    </div>
                    <div class="clearfix"></div>

                    @include ('admin.layouts.partials.sidebar')

                </div>
            </div>

            @include("admin.layouts.partials.top-nav")


            <!-- page content -->
            <div class="right_col" role="main">
                <div>
                    @yield('content')
                </div>
            </div>
            <!-- /page content -->


            <!-- footer content -->
            <footer>
                <div class="pull-right">
                    {{config("name")}}
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>

        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <!-- Bootstrap -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- moment -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
        <!-- FastClick -->
        <script src="/assets/admin/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="/assets/admin/vendors/nprogress/nprogress.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="/assets/admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- iCheck -->
        <script src="/assets/admin/vendors/iCheck/icheck.min.js"></script>
        <!-- Switchery -->
        <script src="/assets/admin/vendors/switchery/dist/switchery.min.js"></script>
        <!-- Fancybox -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="/assets/admin/build/js/custom.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
        <!-- select2 -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/i18n/ru.js"></script>

        <!-- bootstrap-datetimepicker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

        <!-- 2GIS -->
        <script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>

        <!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->

        <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
        <script src="https://www.gstatic.com/firebasejs/6.6.1/firebase-app.js"></script>
        <!-- Add Firebase products that you want to use -->
        <script src="https://www.gstatic.com/firebasejs/6.6.1/firebase-auth.js"></script>
        <script src="https://www.gstatic.com/firebasejs/6.6.1/firebase-firestore.js"></script>

        <script src="/assets/admin/build/js/app.js?v=1.0.10"></script>

        @yield('script')
        @yield('script2')
        @yield('script3')

    </body>
</html>
