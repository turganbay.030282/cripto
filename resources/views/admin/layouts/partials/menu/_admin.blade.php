<li class="{{ Request::is('admin') || Request::is('ru/admin') ? ' active' : '' }}">
    <a href="{{route("admin.home")}}"><i class="fa fa-home"></i> Статистика</a>
</li>

<li class="{{ Request::is('admin/user/users*') || Request::is('ru/admin/user/users*') ? ' active' : '' }}">
    <a href="{{route("admin.user.users.index")}}"><i class="fa fa-users"></i> Пользователи</a>
</li>

@php
    $isActive = Request::is('admin/user/user-verifications*') ||
                Request::is('admin/user/user-changes*') ||
                Request::is('admin/user/invest-rank-verifications*') ||
                Request::is('admin/user/credit-level-verifications*') ||
                Request::is('admin/user/credit-line-verifications*') ||
                Request::is('admin/user/deposit-line-verifications*');
    $totalAllVerification = $totalVerifications +
    $totalUserChanges +
    $totalCreditLevelVerifications +
    $totalCreditLineVerifications +
    $totalInvestorRankVerifications +
    $totalDepositLineVerifications;
@endphp
<li class="{{ $isActive ? ' active' : '' }}">
    <a><i class="fa fa-check-square-o"></i> Заявления <span class="fa fa-chevron-down"></span>
        @if($totalAllVerification)
            <span class="label label-danger pull-right">{{ $totalAllVerification }}</span>
        @endif
    </a>
    <ul class="nav child_menu" style="{{ ( $isActive ) ? 'display:block;' : '' }}">
        <li class="{{ Request::is('admin/user/user-verifications*') || Request::is('ru/admin/user/user-verifications*') ? ' active' : '' }}">
            <a href="{{route("admin.user.user-verifications.index")}}">
                Верификация
                @if($totalVerifications)
                    <span class="label label-danger pull-right">{{ $totalVerifications }}</span>
                @endif
            </a>
        </li>
        <li class="{{ Request::is('admin/user/user-changes*')? ' active' : '' }}">
            <a href="{{route("admin.user.user-changes.index")}}">
                Изменение данных
                @if($totalUserChanges)
                    <span class="label label-danger pull-right">{{ $totalUserChanges }}</span>
                @endif
            </a>
        </li>
        <li class="{{ Request::is('admin/user/credit-level-verifications*') ? ' active' : '' }}">
            <a href="{{route("admin.user.credit-level-verifications.index")}}">
                Кредитный скорринг
                @if($totalCreditLevelVerifications)
                    <span class="label label-danger pull-right">{{ $totalCreditLevelVerifications }}</span>
                @endif
            </a>
        </li>
        <li class="{{ Request::is('admin/user/credit-line-verifications*') ? ' active' : '' }}">
            <a href="{{route("admin.user.credit-line-verifications.index")}}">
                Кредитная линия
                @if($totalCreditLineVerifications)
                    <span class="label label-danger pull-right">{{ $totalCreditLineVerifications }}</span>
                @endif
            </a>
        </li>
        <li class="{{ Request::is('admin/user/invest-rank-verifications*') ? ' active' : '' }}">
            <a href="{{route("admin.user.invest-rank-verifications.index")}}">
                Инвестиционный скорринг
                @if($totalInvestorRankVerifications)
                    <span class="label label-danger pull-right">{{ $totalInvestorRankVerifications }}</span>
                @endif
            </a>
        </li>
        <li class="{{ Request::is('admin/user/deposit-line-verifications*') ? ' active' : '' }}">
            <a href="{{route("admin.user.deposit-line-verifications.index")}}">
                Депозит
                @if($totalDepositLineVerifications)
                    <span class="label label-danger pull-right">{{ $totalDepositLineVerifications }}</span>
                @endif
            </a>
        </li>
    </ul>
</li>


@php
    $isActive = Request::is('admin/handbook*');
@endphp
<li class="{{ $isActive ? ' active' : '' }}">
    <a><i class="fa fa-book"></i> Справочники <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu" style="{{ ( $isActive ) ? 'display:block;' : '' }}">
        <li class="{{ Request::is('admin/handbook/type-transactions*') ? ' active' : '' }}">
            <a href="{{route("admin.handbook.type-transactions.index")}}">Параметры пакетов</a>
        </li>
        <li class="{{ Request::is('admin/handbook/credit-levels*') ? ' active' : '' }}">
            <a href="{{route("admin.handbook.credit-levels.index")}}">Кредитный уровень</a>
        </li>
{{--        <li class="{{ Request::is('admin/handbook/investor-ranks*') ? ' active' : '' }}">--}}
{{--            <a href="{{route("admin.handbook.investor-ranks.index")}}">Ранг Инвестора</a>--}}
{{--        </li>--}}
        <li class="{{ Request::is('admin/handbook/assets*') ? ' active' : '' }}">
            <a href="{{route("admin.handbook.assets.index")}}">Активы</a>
        </li>
        <li class="{{ Request::is('admin/handbook/type-statuses*') ? ' active' : '' }}">
            <a href="{{route("admin.handbook.type-statuses.index")}}">Тип транзакций</a>
        </li>
        <li class="{{ Request::is('admin/handbook/type-two-statuses*') ? ' active' : '' }}">
            <a href="{{route("admin.handbook.type-two-statuses.index")}}">Тип 2</a>
        </li>
    </ul>
</li>

@php
    $isActive = Request::is('admin/content*');
@endphp
<li class="{{ $isActive ? ' active' : '' }}">
    <a><i class="fa fa-columns"></i> Контент <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu" style="{{ ( $isActive ) ? 'display:block;' : '' }}">
        <li class="{{ Request::is('admin/content/pages*') ? ' active' : '' }}">
            <a href="{{route("admin.content.pages.index")}}">Страницы</a>
        </li>
        <li class="{{ Request::is('admin/content/page-main*') ? ' active' : '' }}">
            <a href="{{route("admin.content.page-main.edit")}}">Страница Home</a>
        </li>
        <li class="{{ Request::is('admin/content/page-about*') ? ' active' : '' }}">
            <a href="{{route("admin.content.page-about.edit")}}">Страница О нас</a>
        </li>
        <li class="{{ Request::is('admin/content/page-investor*') ? ' active' : '' }}">
            <a href="{{route("admin.content.page-investor.edit")}}">Страница Инвесторам</a>
        </li>
        <li class="{{ Request::is('admin/content/page-bonus*') ? ' active' : '' }}">
            <a href="{{route("admin.content.page-bonus.edit")}}">Страница Бонусы</a>
        </li>
        <li class="{{ Request::is('admin/content/blog-categories*') ? ' active' : '' }}">
            <a href="{{route("admin.content.blog-categories.index")}}">Категории новостей</a>
        </li>
        <li class="{{ Request::is('admin/content/blogs*') ? ' active' : '' }}">
            <a href="{{route("admin.content.blogs.index")}}">Новости</a>
        </li>
        <li class="{{ Request::is('admin/content/certificates*') ? ' active' : '' }}">
            <a href="{{route("admin.content.certificates.index")}}">Сертификаты</a>
        </li>
        <li class="{{ Request::is('admin/content/faq-categories*') ? ' active' : '' }}">
            <a href="{{route("admin.content.faq-categories.index")}}">Категории FAQ</a>
        </li>
        <li class="{{ Request::is('admin/content/faqs*') ? ' active' : '' }}">
            <a href="{{route("admin.content.faqs.index")}}">FAQ</a>
        </li>
        <li class="{{ Request::is('admin/content/block-packages*') ? ' active' : '' }}">
            <a href="{{route("admin.content.block-packages.index")}}">Наши пакеты</a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('admin/transactions*') ? ' active' : '' }}">
    <a href="{{route("admin.transactions.index")}}">
        <i class="fa fa-exchange"></i> Платежи
        @if($totalTransactions)
            <span class="label label-danger pull-right">{{ $totalTransactions }}</span>
        @endif
    </a>
</li>

<li class="{{ Request::is('admin/contracts*') ? ' active' : '' }}">
    <a href="{{route("admin.contracts.index")}}">
        <i class="fa fa-compress"></i> Договора
        @if($totalContracts)
            <span class="label label-danger pull-right">{{ $totalContracts }}</span>
        @endif
    </a>
</li>

<li class="{{ Request::is('admin/converts*') ? ' active' : '' }}">
    <a href="{{route("admin.converts.index")}}">
        <i class="fa fa-exchange"></i> OTC
        @if($totalConverts)
            <span class="label label-danger pull-right">{{ $totalConverts }}</span>
        @endif
    </a>
</li>

<li class="{{ Request::is('admin/tpl-documents*') ? ' active' : '' }}">
    <a href="{{route("admin.tpl-documents.index")}}">
        <i class="fa fa-file-text"></i> Шаблоны документов
    </a>
</li>

<li class="{{ Request::is('admin/notify-texts*') ? ' active' : '' }}">
    <a href="{{route("admin.notify-texts.index")}}">
        <i class="fa fa-file-text"></i> Шаблоны уведомлений
    </a>
</li>

<li class="{{ Request::is('admin/admin-mailings*') ? ' active' : '' }}">
    <a href="{{route("admin.admin-mailings.index")}}">
        <i class="fa fa-envelope-o"></i> Рассылка
    </a>
</li>

<li class="{{ Request::is('admin/forms*') ? ' active' : '' }}">
    <a href="{{route("admin.forms.index")}}">
        <i class="fa fa-envelope"></i> Формы
        @if($totalForms)
            <span class="label label-danger pull-right">{{ $totalForms }}</span>
        @endif
    </a>
</li>


<li class="{{ Request::is('admin/referrals*') ? ' active' : '' }}">
    <a><i class="fa fa-link"></i> Реф. программа <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu" style="{{ ( Request::is('admin/referrals*') ) ? 'display:block;' : '' }}">
        <li class="{{ Request::is('admin/referrals/statistic-promo-codes*') ? ' active' : '' }}">
            <a href="{{route("admin.referrals.statistic-promo-codes.index")}}">Реф.ссылки</a>
        </li>
        <li class="{{ Request::is('admin/referrals/statistics/invites*') ? ' active' : '' }}">
            <a href="{{route("admin.referrals.statistics.invites")}}">Приглашенные</a>
        </li>
        <li class="{{ Request::is('admin/referrals/statistics/ref-partners*') ? ' active' : '' }}">
            <a href="{{route("admin.referrals.statistics.partners")}}">Реферал-партнеры</a>
        </li>
        <li class="{{ Request::is('admin/referrals/statistics/analytics*') ? ' active' : '' }}">
            <a href="{{route("admin.referrals.statistics.analytics")}}">Аналитика</a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('admin/subscribes*') ? ' active' : '' }}">
    <a href="{{route("admin.subscribes.index")}}">
        <i class="fa fa-envelope-o"></i> Подписаться на новости
    </a>
</li>

<li class="{{ Request::is('admin/translations*') ? ' active' : '' }}">
    <a href="{{route("admin.translations.index")}}"><i class="fa fa-language"></i> Переводы</a>
</li>

<li class="{{ Request::is('admin/settings*') ? ' active' : '' }}">
    <a href="{{route("admin.settings.index")}}"><i class="fa fa-cogs"></i> Настройки</a>
</li>
