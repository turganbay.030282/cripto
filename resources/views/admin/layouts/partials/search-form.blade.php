@if($showSearchForm)
<div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
        <form method="get" action="?">
            <div class="input-group">
                <input type="text" class="form-control" name="search" value="{{request("search")}}" placeholder="Поиск...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">Искать!</button>
                </span>
            </div>
        </form>
    </div>
</div>
@endif
