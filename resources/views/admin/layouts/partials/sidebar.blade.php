<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">

            @include('admin.layouts.partials.menu._admin')

        </ul>
    </div>

</div>
<!-- /sidebar menu -->
