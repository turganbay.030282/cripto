<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <div class="title">
                @yield('title')
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        {{auth()->user()->name}}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="javascript:;">{{date("d.m.Y H:i")}}</a></li>
                        <li><a href="{{route('admin.user.profile.changePassword')}}"><i class="fa fa-key" aria-hidden="true"></i> Сменить пароль</a></li>
                        <li>
                            <form method="post" action="{{route("logout")}}" class="logout">
                                @csrf
                                @method('POST')
                                <button style="border: 0;outline: 0;background: none;float: right">
                                    <i class="fa fa-sign-out pull-right"></i>
                                    Выход
                                </button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->
