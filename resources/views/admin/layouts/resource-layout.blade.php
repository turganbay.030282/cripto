@extends('admin.layouts.layout')

@section('content')

    @include('admin.layouts.partials._flash')

    @yield('btn')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @yield('resource-content')

@endsection
