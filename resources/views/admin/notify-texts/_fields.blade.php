<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[subject]', 'Тема') !!}
                    {{ Form::text($locale.'[subject]', $notifyText ? $notifyText->{'subject:'.$locale} : null, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.subject') }}
                </div>

                <div class="form-group">
                    {!! Form::label($locale.'[body]', 'Текст') !!}
                    {{ Form::textarea($locale.'[body]', $notifyText ? $notifyText->{'body:'.$locale} : null, ['class' => 'form-control']) }}
                    {{ Form::error($locale . '.body') }}
                </div>
            </div>
        @endforeach
    </div>
    <div class="form-group">
        {!! Form::label('Активен') !!}
        {{ Form::checkbox('active', null,  $notifyText ? $notifyText->active : 0) }}
    </div>
</div>

{{ Form::btn_save() }}
