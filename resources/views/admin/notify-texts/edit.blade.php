@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Шаблоны уведомлений: изменить {{ $notifyText->title }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($notifyText, ['method' => 'PUT', 'url' => route('admin.notify-texts.update', $notifyText)]) !!}
                        @include('admin.notify-texts._fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
