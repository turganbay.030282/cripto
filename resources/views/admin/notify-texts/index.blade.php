@extends('admin.layouts.resource-layout')

@section('title')
    <h3>{{ $isArchive ? 'Архив' : '' }} Шаблоны уведомлений</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <div class="row">
                        <div class="col-xs-12">
                            <a href="{{ route('admin.notify-texts.create') }}" class="btn btn-success btn-sm"><i
                                    class="fa fa-plus" aria-hidden="true"></i> Создать</a>
                        </div>
                    </div>

                    <table class="table table-hover  table-border jambo_table">
                        <thead>
                        <tr>
                            <th>@sortablelink('id', 'ID')</th>
                            <th>@sortablelink('subject', 'Тема')</th>
                            <th>@sortablelink('body', 'Текст')</th>
                            <th>@sortablelink('active', 'Подключен')</th>
                            <th class="text-right">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>
                                    <a href="{{ route('admin.notify-texts.show', $item) }}">
                                        {{ $item->subject }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.notify-texts.show', $item) }}">
                                        {{ $item->body }}
                                    </a>
                                </td>
                                <td>
                                    @if( $item->active )
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    @endif
                                </td>
                                <td class="text-right">
                                    <a href="{{ route('admin.notify-texts.show', $item) }}" class="btn btn-info btn-xs"><i
                                            class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="{{ route('admin.notify-texts.edit', $item) }}"
                                       class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o"
                                                                         aria-hidden="true"></i></a>
                                    {!! Form::model($item, ['method' => 'DELETE', 'url' => route('admin.notify-texts.update', $item)]) !!}
                                    <button class="btn btn-danger btn-xs" type="submit">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{ $items->appends(request()->all())->links() }}

                </div>
            </div>
        </div>
    </div>

@endsection
