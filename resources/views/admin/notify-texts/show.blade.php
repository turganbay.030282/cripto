@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Шаблоны уведомлений: просмотр {{ $notifyText->title }}</h3>
@endsection

@section('resource-content')
    <div class="title_right">
        <a href="{{ route("admin.notify-texts.edit", $notifyText) }}" class="btn btn-warning btn-sm">
            <i class="fa fa-pencil-square-o"></i> Изменить
        </a>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $notifyText->id }}</td>
                            </tr>
                            <tr>
                                <th>Тема</th><td>{{ $notifyText->subject }}</td>
                            </tr>
                            <tr>
                                <th>Текст</th><td>{{ $notifyText->body }}</td>
                            </tr>
                            <tr>
                                <th>Активен</th><td>
                                    @if( $notifyText->active )
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
