<div class="modal fade modal-crop" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Изменить изображение</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <label for="">Обрезка</label>
                        <div class="img-preview-sm" style="width: 100%;height: {{$height}}px;">
                            <img src="" alt="" id="origin-image">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <label for="">Просмотр</label>
                        <div class="img-preview-sm" style="width: {{$width > 300 ? intval($width/1.5) : $width}}px; height: {{$width > 300 ? intval($height/1.5) : $height}}px;">
                            <img src="" alt="" id="crop-image">
                            <div id="crop-image-preview"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-success" id="submit_changes">Применить изменения</button>
            </div>

        </div>
    </div>
</div>

@section('stylesheet')

    <link  href="/assets/admin/vendors/cropperjs/dist/cropper.min.css" rel="stylesheet">
    <style>
        #crop-image-preview{
            overflow: hidden;
            width: {{$width > 300 ? intval($width/1.5) : $width}}px!important;
            height: {{$width > 300 ? intval($height/1.5) : $height}}px!important;
        }
        .image_container .portfolio-image {
            width: {{$width}}px;
            height: {{$height}}px;
        }
    </style>
@endsection

@section('script')
    <script src="/assets/admin/vendors/cropperjs/dist/cropper.min.js"></script>
    <script src="/assets/admin/vendors/cropperjs/dist/jquery-cropper.min.js"></script>
    <script src="/assets/admin/vendors/cropperjs/dist/canvas-toBlob.js"></script>

    <script>
        var modal = $('.modal-crop');
        var image = $('#origin-image');
        var cropper;
        var key;
        var currentKey;

        $(document).on("change", "input.origin-file", function () {
            modal.modal('show');
            currentKey = $(this).closest(".portfolio-image").data("key")*1;
            if (this.files && this.files[0]) {
                var fileUpload = this.files[0]
                modal.on('shown.bs.modal', function (event) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#origin-image').attr('src', e.target.result);
                        $('#crop-image').attr('src', e.target.result);

                        if(cropper){
                            image.cropper("destroy");
                            cropper = null;
                        }

                        image.cropper({
                            aspectRatio: {{number_format(floatval($width/$height), 2, ".", "")}},
                            viewMode: 1,
                            preview: '#crop-image-preview'
                        });
                        cropper = image.data('cropper');
                        $('#crop-image').hide();
                    };
                    reader.readAsDataURL(fileUpload);
                });
            }
        });
        $("#submit_changes").on("click", function(e){
            e.preventDefault();
            if(cropper){
                var dataURL = cropper.getCroppedCanvas(
                    {
                        width: {{$width}},
                        height: {{$height}}
                    }
                ).toDataURL('image/png');

                if(currentKey && dataURL){
                    var classImgSalonValue = ".portfolio-image-"+currentKey+" input.crop-file";
                    var classImgSalon = ".portfolio-image-"+currentKey+" img.imgNew";

                    $(classImgSalonValue).val(dataURL);
                    $(classImgSalon).attr("src", dataURL);

                    modal.modal('hide');
                    modal.trigger('hidden.bs.modal');
                }

                image.cropper("destroy");
                cropper = null;
                $("#origin-image").attr("src", "");
            }
        });

        modal.on('hidden.bs.modal', function () {
            image.cropper("destroy");
            cropper = null;
            $("#origin-image").attr("src", "");
        });
    </script>

@endsection
