@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Настройки: изменить</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    {!! Form::model($setting, ['url' => route('admin.settings.update', $setting), 'method' => 'PUT', 'class' => 'form-horizontal form-label-left', 'enctype' => 'multipart/form-data']) !!}

                        <div class="form-group">
                            {{ Form::label('commission_trader', 'Комиссия холдера на стоимость актива', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('commission_trader', null, ['class' => 'form-control']) }}
                                {{ Form::error('commission_trader') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('commission_sell_trader', 'Комиссия холдера при досрочном в евро', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('commission_sell_trader', null, ['class' => 'form-control']) }}
                                {{ Form::error('commission_sell_trader') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('conversion_fee', 'Комиссия системы при выводе в евро и монетах', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('conversion_fee', null, ['class' => 'form-control']) }}
                                {{ Form::error('conversion_fee') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('commission_investor', 'Комиссия стейкхолдера при досрочном', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('commission_investor', null, ['class' => 'form-control']) }}
                                {{ Form::error('commission_investor') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('course_investor', 'Курс стейкхолдера по DAI', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('course_investor', null, ['class' => 'form-control']) }}
                                {{ Form::error('course_investor') }}
                            </div>
                        </div>

{{--                        <div class="form-group">--}}
{{--                            {{ Form::label('wallet_number', 'Кошелек номер', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}--}}
{{--                            <div class="col-md-9 col-sm-9 col-xs-12">--}}
{{--                                {{ Form::text('wallet_number', null, ['class' => 'form-control']) }}--}}
{{--                                {{ Form::error('wallet_number') }}--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            {{ Form::label('wallet_img', 'Кошелек QR-код', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}--}}
{{--                            <div class="col-md-9 col-sm-9 col-xs-12">--}}
{{--                                <div>--}}
{{--                                    <a href="{{ $setting->getPhotoUrl('wallet_img') }}" class="popup">--}}
{{--                                        <img src="{{ $setting->getPhotoUrl('wallet_img') }}" style="height: 40px;width: auto;">--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                                {{ Form::file('wallet_img', ['class' => 'form-control']) }}--}}
{{--                                {{ Form::error('wallet_img') }}--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="form-group">
                            {{ Form::label('bonus_ref_credit', '% минус за кредит', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('bonus_ref_credit', null, ['class' => 'form-control']) }}
                                {{ Form::error('bonus_ref_credit') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('bonus_ref_deposit', '% плюс за депозит', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('bonus_ref_deposit', null, ['class' => 'form-control']) }}
                                {{ Form::error('bonus_ref_deposit') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('percent_ref_credit', '% за кредит', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('percent_ref_credit', null, ['class' => 'form-control']) }}
                                {{ Form::error('percent_ref_credit') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('percent_ref_deposit', '% за депозит', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('percent_ref_deposit', null, ['class' => 'form-control']) }}
                                {{ Form::error('percent_ref_deposit') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('penalty', 'пеня холдера, %', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('penalty', null, ['class' => 'form-control']) }}
                                {{ Form::error('penalty') }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12">&nbsp;</div>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::hidden('is_manual_verify', 0) !!}
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('is_manual_verify', 1, $setting->is_manual_verify ? true : null) !!} Включить ручную верификацию
                                    </label>
                                </div>
                                {{ Form::error('is_manual_verify') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('crop_blog_width', 'Изображение Новости ширина', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('crop_blog_width', $setting->crop_blog_width, ['class' => 'form-control']) }}
                                {{ Form::error('crop_blog_width') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('crop_blog_height', 'Изображение Новости высота', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('crop_blog_height', $setting->crop_blog_height, ['class' => 'form-control']) }}
                                {{ Form::error('crop_blog_height') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('crop_ava_width', 'Изображение Аватар ширина', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('crop_ava_width', $setting->crop_ava_width, ['class' => 'form-control']) }}
                                {{ Form::error('crop_ava_width') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('crop_ava_height', 'Изображение Аватар высота', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('crop_ava_height', $setting->crop_ava_height, ['class' => 'form-control']) }}
                                {{ Form::error('crop_ava_height') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('crop_cert_width', 'Изображение Сертификаты ширина', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('crop_cert_width', $setting->crop_cert_width, ['class' => 'form-control']) }}
                                {{ Form::error('crop_cert_width') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('crop_cert_height', 'Изображение Сертификаты высота', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('crop_cert_height', $setting->crop_cert_height, ['class' => 'form-control']) }}
                                {{ Form::error('crop_cert_height') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('contact_phone', 'Контакты: телефон', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('contact_phone', $setting->contact_phone, ['class' => 'form-control']) }}
                                {{ Form::error('contact_phone') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('contact_email', 'Контакты: email', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('contact_email', $setting->contact_email, ['class' => 'form-control']) }}
                                {{ Form::error('contact_email') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('soc_fb', 'Соц.сети: facebook', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('soc_fb', $setting->soc_fb, ['class' => 'form-control']) }}
                                {{ Form::error('soc_fb') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('soc_tw', 'Соц.сети: twitter', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('soc_tw', $setting->soc_tw, ['class' => 'form-control']) }}
                                {{ Form::error('soc_tw') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('soc_in', 'Соц.сети: instagram', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('soc_in', $setting->soc_in, ['class' => 'form-control']) }}
                                {{ Form::error('soc_in') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('soc_yt', 'Соц.сети: youtube', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('soc_yt', $setting->soc_yt, ['class' => 'form-control']) }}
                                {{ Form::error('soc_yt') }}
                            </div>
                        </div>

                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                @foreach(config('translatable.locales') as $key => $locale)
                                    <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
                                @endforeach
                            </ul>
                            <div class="tab-content">
                                @foreach(config('translatable.locales') as $key => $locale)
                                    <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">

                                        <div class="form-group">
                                            {!! Form::label($locale.'[file_politic]', 'Файл соглашения', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                @if($setting->{'file_politic:'.$locale})
                                                    <a href="{{ $setting->translations->where('locale', $locale)->first()->getFileUrl('file_politic', 'settings') }}" download="">{{ $setting->{'file_politic:'.$locale} }}</a>
                                                @endif
                                                {{ Form::file($locale.'[file_politic]', ['class' => 'form-control']) }}
                                                {{ Form::error($locale . '.file_politic') }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label($locale.'[contact_address]', 'Название', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::text($locale.'[contact_address]', $setting ? $setting->{'contact_address:'.$locale} : null, ['class' => 'form-control']) }}
                                                {{ Form::error($locale . '.contact_address') }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label($locale.'[req_iban]', 'Реквизиты: IBAN', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::text($locale.'[req_iban]', $setting ? $setting->{'req_iban:'.$locale} : null, ['class' => 'form-control']) }}
                                                {{ Form::error($locale . '.req_iban') }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label($locale.'[req_sia]', 'Реквизиты: Имя получателя', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::text($locale.'[req_sia]', $setting ? $setting->{'req_sia:'.$locale} : null, ['class' => 'form-control']) }}
                                                {{ Form::error($locale . '.req_sia') }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label($locale.'[req_reg]', 'Реквизиты: адрес получателя', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::text($locale.'[req_reg]', $setting ? $setting->{'req_reg:'.$locale} : null, ['class' => 'form-control']) }}
                                                {{ Form::error($locale . '.req_reg') }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label($locale.'[req_seb]', 'Реквизиты: Название банка', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::text($locale.'[req_seb]', $setting ? $setting->{'req_seb:'.$locale} : null, ['class' => 'form-control']) }}
                                                {{ Form::error($locale . '.req_seb') }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label($locale.'[req_bank_address]', 'Реквизиты: Адрес банка', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::text($locale.'[req_bank_address]', $setting ? $setting->{'req_bank_address:'.$locale} : null, ['class' => 'form-control']) }}
                                                {{ Form::error($locale . '.req_bank_address') }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label($locale.'[req_swift]', 'Реквизиты: Swift', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::text($locale.'[req_swift]', $setting ? $setting->{'req_swift:'.$locale} : null, ['class' => 'form-control']) }}
                                                {{ Form::error($locale . '.req_swift') }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label($locale.'[footer_text_1]', 'Footer текст', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::textarea($locale.'[footer_text_1]', $setting ? $setting->{'footer_text_1:'.$locale} : null, ['class' => 'form-control', 'rows' => 3]) }}
                                                {{ Form::error($locale . '.footer_text_1') }}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12 col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </div>
                        </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
