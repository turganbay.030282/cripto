@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Статистика отправки реферальной ссылки </h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-hover jambo_table">
                        <thead>
                        <tr>
                            <th>@sortablelink('id', 'ID')</th>
                            <th>@sortablelink('created_at', 'Дата создания')</th>
                            <th>@sortablelink('user.name', 'Пользователь')</th>
                            <th>@sortablelink('email', 'Email')</th>
                            <th>@sortablelink('registerUser.created_at', 'Дата регистрации')</th>
                            <th class="text-right">Действия</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($items as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->created_at->format("d.m.Y H:i:s") }}</td>
                                <td>
                                    @if($item->user)
                                        <a href="{{ route('admin.user.users.show', $item->user) }}" target="_blank">
                                            {{ $item->user->full_name }}
                                        </a>
                                    @endif
                                </td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->registerUser ? $item->registerUser->created_at : '' }}</td>
                                <td class="text-right">
                                    {!! Form::open(['method' => 'DELETE', 'url' => route('admin.referrals.statistic-promo-codes.destroy', $item), 'class' => 'form-inline-block']) !!}
                                    <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    {{ $items->appends(request()->all())->links() }}

                </div>
            </div>
        </div>
    </div>

@endsection
