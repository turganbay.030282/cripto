<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Имя</th>
            <th>Имя приглашенного</th>
            <th>Тип договора</th>
            <th>Номер договора</th>
            <th>Сумма договора</th>
            <th>Дата создания</th>
            <th>Тип бонуса</th>
            <th>Сумма бонуса</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($items as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->client->referer->full_name }}</td>
                <td>{{ $item->client->full_name }}</td>
                <td>{{ $item->type }}</td>
                <td>{{ $item->number }}</td>
                <td>{{ $item->amount }}</td>
                <td>{{ $item->created_at }}</td>
                <td>{{ $item->bonus_type }}</td>
                <td>{{ $item->bonus_amount_partner }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
