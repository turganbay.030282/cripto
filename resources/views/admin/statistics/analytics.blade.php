@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Аналитика</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-hover jambo_table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Имя</th>
                            <th>Кол-во отправленных приглашений</th>
                            <th>Кол-во регистраций по пригл-ям</th>
                            <th>%</th>
                            <th>Заработано</th>
                            <th>Выведено</th>
                            <th>Остаток</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->full_name }}</td>
                                <td>{{ $item->statistic_promo_codes_count }}</td>
                                <td>{{ $item->referrals_count }}</td>
                                <td>{{ $item->referrals_count ? format_number($item->statistic_promo_codes_count/$item->referrals_count)*100 : 0}}%</td>
                                <td>{{ $item->bonus_total }}</td>
                                <td>{{ $item->bonus_payment_amount }}</td>
                                <td>{{ format_number($item->amount_bonus) }}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    {{ $items->appends(request()->all())->links() }}

                </div>
            </div>
        </div>
    </div>

@endsection
