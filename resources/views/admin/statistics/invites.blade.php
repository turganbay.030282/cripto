@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Приглашенные</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-hover jambo_table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Имя</th>
                                <th>Тип договора</th>
                                <th>Номер договора</th>
                                <th>Сумма договора</th>
                                <th>Дата создания</th>
                                <th>Тип бонуса</th>
                                <th>Сумма бонуса</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->client->full_name }}</td>
                                    <td>{{ $item->type }}</td>
                                    <td>{{ $item->number }}</td>
                                    <td>{{ $item->amount }}</td>
                                    <td>{{ $item->created_at }}</td>
                                    <td>{{ $item->bonus_type }} %</td>
                                    <td>{{ $item->bonus_amount }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $items->appends(request()->all())->links() }}

                </div>
            </div>
        </div>
    </div>

@endsection
