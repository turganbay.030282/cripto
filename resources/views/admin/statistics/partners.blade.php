@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Реферал-партнеры</h3>
@endsection

@section('resource-content')

    <div class="page-filter clearfix">
        <form action="?" method="GET">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="id" class="col-form-label">ID</label>
                        <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="name" class="col-form-label">Имя</label>
                        <input id="name" class="form-control" name="name" value="{{ request('name') }}">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="from" class="col-form-label">Дата от</label>
                        <input id="from" class="form-control picker-date-format" name="from" value="{{ request('from') }}">
                        {!! Form::error('from') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="to" class="col-form-label">Дата до</label>
                        <input id="to" class="form-control picker-date-format" name="to" value="{{ request('to') }}">
                        {!! Form::error('to') !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group text-right">
                        <label class="col-form-label">&nbsp;</label><br />
                        <button title="Поиск" type="submit" class="btn bg-orange"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <button title="Скачать" type="submit" class="btn btn-info" name="excel"><i class="fa fa-download" aria-hidden="true"></i></button>
                        <a title="Очистить" href="?" class="btn btn-default"><i class="fa fa-retweet" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-hover jambo_table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Имя</th>
                                <th>Имя приглашенного</th>
                                <th>Тип договора</th>
                                <th>Номер договора</th>
                                <th>Сумма договора</th>
                                <th>Дата создания</th>
                                <th>Тип бонуса</th>
                                <th>Сумма бонуса</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->client->referer ? $item->client->referer->full_name : '' }}</td>
                                <td>{{ $item->client->full_name }}</td>
                                <td>{{ $item->type }}</td>
                                <td>
                                    <a href="{{ route('admin.contracts.show', $item) }}" target="_blank">
                                        {{ $item->number }}
                                    </a>
                                </td>
                                <td>{{ $item->amount }}</td>
                                <td>{{ $item->created_at }}</td>
                                <td>бонус</td>
                                <td>{{ $item->bonus_amount_partner }}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    {{ $items->appends(request()->all())->links() }}

                </div>
            </div>
        </div>
    </div>

@endsection
