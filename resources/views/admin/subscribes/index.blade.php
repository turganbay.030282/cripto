@extends('admin.layouts.resource-layout')

@section('title')
    <h3>{{ $isArchive ? 'Архив' : '' }} Подписаться на новости - рассылка</h3>
@endsection

@section('resource-content')
    <div class="page-filter clearfix">
        <form action="?" method="GET">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="id" class="col-form-label">ID</label>
                        <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        {!! Form::error('id') !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="email" class="col-form-label">Email</label>
                        <input id="email" class="form-control" name="email" value="{{ request('email') }}">
                        {!! Form::error('email') !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group text-right">
                        <label class="col-form-label">&nbsp;</label><br />
                        <button title="Поиск" type="submit" class="btn bg-orange"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <a title="Очистить" href="?" class="btn btn-default"><i class="fa fa-retweet" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <div class="row">
                        <div class="col-xs-12 text-right">
                            @if($isArchive)
                                <a href="{{route("admin.subscribes.index")}}" class="btn btn-info btn-sm">
                                    <i class="fa fa-archive" aria-hidden="true"></i></i> Активные
                                </a>
                            @else
                                <a href="{{route("admin.subscribes.archive")}}" class="btn btn-warning btn-sm">
                                    <i class="fa fa-archive" aria-hidden="true"></i></i> Архив
                                </a>
                            @endif
                        </div>
                    </div>

                    <table class="table table-hover jambo_table">
                        <thead>
                        <tr>
                            <th>@sortablelink('email', 'Email')</th>
                            <th>@sortablelink('created_at', 'Дата создания')</th>
                            <th class="text-right">Действия</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($forms as $form)
                            <tr>
                                <td>{{ $form->email }}</td>
                                <td>{{ $form->created_at->format("d.m.Y H:i:s") }}</td>
                                <td class="text-right">
                                    @if($isArchive)
                                        {!! Form::open(['url' => route('admin.subscribes.restore', $form), 'class' => 'form-inline-block']) !!}
                                        <input type="hidden" name="id" value="{{ $form->id }}">
                                        <div class="btn-group">
                                            <button class="btn btn-success btn-xs"><i class="fa fa-reply" aria-hidden="true"></i></button>
                                        </div>
                                        {!! Form::close() !!}
                                    @else
                                        {!! Form::open(['method' => 'DELETE', 'url' => route('admin.subscribes.destroy', $form), 'class' => 'form-inline-block']) !!}
                                            <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    {{ $forms->appends(request()->all())->links() }}

                </div>
            </div>
        </div>
    </div>

@endsection
