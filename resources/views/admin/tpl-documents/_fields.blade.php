<div class="form-group">
    {!! Form::label('title', 'Название') !!}
    {{ Form::text('title', null, ['class' => 'form-control']) }}
    {{ Form::error('title') }}
</div>

<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach(config('translatable.locales') as $key => $locale)
            <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">{{$locale}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('translatable.locales') as $key => $locale)
            <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                <div class="form-group">
                    {!! Form::label($locale.'[content]', 'Контент') !!}
                    {{ Form::textarea($locale.'[content]', $tplDocument ? $tplDocument->{'content:'.$locale} : null, ['class' => 'editor summernote form-control']) }}
                    {{ Form::error($locale . '.content') }}
                </div>
            </div>
        @endforeach
    </div>
</div>

{{ Form::btn_save() }}
