@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Шаблоны документов: создать</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::open(['url' => route('admin.tpl-documents.store'), 'enctype' => 'multipart/form-data']) !!}
                        @include('admin.tpl-documents._fields', ['tplDocument' => null])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
