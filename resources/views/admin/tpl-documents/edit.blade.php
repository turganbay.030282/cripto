@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Шаблоны документов: изменить {{ $tplDocument->title }}</h3>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($tplDocument, ['method' => 'PUT', 'url' => route('admin.tpl-documents.update', $tplDocument)]) !!}
                        @include('admin.tpl-documents._fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
