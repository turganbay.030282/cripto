@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Шаблоны документов: просмотр {{ $tplDocument->title }}</h3>
@endsection

@section('resource-content')
    <div class="title_right">
        <a href="{{ route("admin.tpl-documents.edit", $tplDocument) }}" class="btn btn-warning btn-sm">
            <i class="fa fa-pencil-square-o"></i> Изменить
        </a>
        {!! Form::open(['method' => 'DELETE', 'url' => route('admin.tpl-documents.destroy', $tplDocument), 'class' => 'form-inline-block']) !!}
            <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Удалить</button>
        {!! Form::close() !!}
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $tplDocument->id }}</td>
                            </tr>
                            <tr>
                                <th>Название</th><td>{{ $tplDocument->title }}</td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            @foreach(config('translatable.locales') as $key => $locale)
                                <li class="{{$key == 0 ? 'active' : ''}}"><a href="#tab_{{$key}}" data-toggle="tab">Содержимое {{$locale}}</a></li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach(config('translatable.locales') as $key => $locale)
                                <div class="tab-pane {{$key == 0 ? 'active' : ''}}" id="tab_{{$key}}">
                                    {{ $tplDocument->{'content:'.$locale} }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
