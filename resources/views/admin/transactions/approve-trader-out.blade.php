@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Подтвердить платеж</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    {!! Form::open(['url' => route('admin.transactions.approve-trader-out', $transaction), 'class' => 'form-horizontal form-label-left', 'enctype' => 'multipart/form-data']) !!}

                        <div class="form-group">
                            {{ Form::label('pay_ticket', 'Квитанция', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::file('pay_ticket', ['class' => 'form-control']) }}
                                {{ Form::error('pay_ticket') }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12 col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </div>
                        </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
