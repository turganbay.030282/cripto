@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Платежи</h3>
@endsection

@section('resource-content')
    <div class="page-filter clearfix">
        <form action="?" method="GET">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="client" class="col-form-label">Пользователь</label>
                        <input id="client" class="form-control" name="client" value="{{ request('client') }}">
                        {!! Form::error('client') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="amount_from" class="col-form-label">Сумма от</label>
                        <input id="amount_from" class="form-control" name="amount_from"
                               value="{{ request('amount_from') }}">
                        {!! Form::error('amount_from') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="amount_to" class="col-form-label">Сумма до</label>
                        <input id="amount_to" class="form-control" name="amount_to" value="{{ request('amount_to') }}">
                        {!! Form::error('amount_to') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="from" class="col-form-label">Дата от</label>
                        <input id="from" class="form-control picker-date-format" name="from"
                               value="{{ request('from') }}">
                        {!! Form::error('from') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="to" class="col-form-label">Дата до</label>
                        <input id="to" class="form-control picker-date-format" name="to" value="{{ request('to') }}">
                        {!! Form::error('to') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group text-right">
                        <label class="col-form-label">&nbsp;</label><br/>
                        <button title="Поиск" type="submit" class="btn bg-orange"><i class="fa fa-search"
                                                                                     aria-hidden="true"></i></button>
                        <a title="Очистить" href="?" class="btn btn-default"><i class="fa fa-retweet"
                                                                                aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-hover jambo_table">
                        <thead>
                        <tr>
                            <th>@sortablelink('id', 'ID')</th>
                            <th>@sortablelink('number', 'Номер счета')</th>
                            <th>@sortablelink('number', 'Номер договора')</th>
                            <th>@sortablelink('client.name', 'Пользователь')</th>
                            <th>@sortablelink('amount', 'Сумма')</th>
                            <th>@sortablelink('type_transaction', 'Тип транзакции')</th>
                            <th>@sortablelink('type', 'Тип 2')</th>
                            <th>Монета</th>
                            <th>@sortablelink('wallet', 'Кошелек')</th>
                            <th>Платежка</th>
                            <th>@sortablelink('created_at', 'Дата создания')</th>
                            <th>@sortablelink('is_redeem', 'Досрочно')</th>
                            <th>@sortablelink('status', 'Статус')</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($items as $item)
                            <tr class="{{ $item->isNew() ? 'bg-row-red' : '' }}">
                                <td>
                                    @if(($item->isInvestor() && $item->isTypeOut() && !($item->isStakingEuro() || $item->isStakingDai() || $item->isContribution() || $item->isInvestorPayInDai())) || $item->is_redeem || $item->is_sell)
                                        <a href="#" data-toggle="modal" data-target="#DogovorDosrochno{{$item->id}}"><i
                                                class="fa fa-eject" aria-hidden="true"></i> {{ $item->id }}</a>
                                    @elseif($item->isDepositDai())
                                        <a href="#" data-toggle="modal" data-target="#DepositDai{{$item->id}}"><i
                                                class="fa fa-eject" aria-hidden="true"></i> {{ $item->id }}</a>
                                    @elseif($item->isStakingEuro())
                                        <a href="#" data-toggle="modal" data-target="#StakingEuro{{$item->id}}"><i
                                                class="fa fa-eject" aria-hidden="true"></i> {{ $item->id }}</a>
                                    @elseif($item->isInvestorPayInDai())
                                        <a href="#" data-toggle="modal" data-target="#InvestorPayInDai{{$item->id}}"><i
                                                class="fa fa-eject" aria-hidden="true"></i> {{ $item->id }}</a>
                                    @elseif($item->isStakingDai())
                                        <a href="#" data-toggle="modal" data-target="#StakingDai{{$item->id}}"><i
                                                class="fa fa-eject" aria-hidden="true"></i> {{ $item->id }}</a>
                                    @elseif($item->isTraderActiveEuro())
                                        <a href="#" data-toggle="modal" data-target="#TraderActiveEuro{{$item->id}}"><i
                                                class="fa fa-eject" aria-hidden="true"></i> {{ $item->id }}</a>
                                    @elseif($item->isTraderPayIn())
                                        <a href="#" data-toggle="modal" data-target="#TraderPayIn{{$item->id}}"><i
                                                class="fa fa-eject" aria-hidden="true"></i> {{ $item->id }}</a>
                                    @elseif($item->isTransferAsset())
                                        <a href="#" data-toggle="modal" data-target="#TransferAsset{{$item->id}}"><i
                                                class="fa fa-eject" aria-hidden="true"></i> {{ $item->id }}</a>
                                    @elseif($item->isPayout())
                                        <a href="#" data-toggle="modal" data-target="#TraderOut{{$item->id}}"><i
                                                class="fa fa-eject" aria-hidden="true"></i> {{ $item->id }}</a>
                                    @else
                                        {{ $item->id }}
                                    @endif
                                </td>
                                <td>{{ $item->number }}</td>
                                <td>
                                    @if($item->isDeposit() || $item->isDepositDai())
                                        @if($item->deposit_payment_id)
                                            <a href="{{ route('admin.contracts.show', $item->depositPayment) }}"
                                               target="_blank">
                                                {{ $item->depositPayment->number }}
                                            </a>
                                        @endif
                                    @elseif($item->isCredit() || $item->isTraderContractSell() || $item->isTraderContractDalaysSell())
                                        @if($item->credit_payment_id)
                                            <a href="{{ route('admin.contracts.show', $item->creditPayment->contract) }}"
                                               target="_blank">
                                                {{ $item->creditPayment->contract->number }}
                                            </a>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if($item->client)
                                        <a href="{{ route('admin.user.users.show', $item->client) }}" target="_blank">
                                            {{ $item->client->full_name }}
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    @if($item->isStakingEuro())
                                        {{ $item->amount_dai }}
                                    @elseif($item->isDepositDai())
                                        {{ $item->amount_dai }}
                                    @elseif($item->isTraderActiveEuro())
                                        {{ $item->amount_dai }}
                                    @else
                                        {{ $item->amount }}
                                    @endif
                                </td>
                                <td>{{ $item->type_transaction_name }}</td>
                                <td>{!! $item->type_name_html !!}</td>
                                <td>{{ $item->coinmarketcap->symbol ?? '' }} </td>
                                <td>
                                    {{ $item->wallet }}
                                    @if($item->isStakingEuro() || $item->isTraderActiveEuro())
                                        {{ $item->wallet_to }}
                                    @endif
                                </td>
                                <td>
                                    @if($item->payment)
                                        <a href="{{ $item->payment }}" download="">
                                            <i class="fa fa-download" aria-hidden="true"></i> download
                                        </a>
                                    @endif
                                    @if($item->pay_ticket)
                                        <a href="{{ $item->getPhotoUrl('pay_ticket') }}" download="">
                                            <i class="fa fa-download" aria-hidden="true"></i> download
                                        </a>
                                    @endif
                                </td>
                                <td>{{ $item->created_at->format("d.m.Y H:i:s") }}</td>
                                <td>
                                    @if($item->is_redeem || ($item->isInvestor() && $item->isTypeOut()))
                                        <span class="label label-danger">Да</span>
                                    @endif
                                </td>
                                <td>
                                    {!! $item->status_name_html !!}
                                    @if($item->isNew())
                                        @if($item->isBonus())
                                            {!! Form::open(['url' => route('admin.transactions.approveBonus', $item), 'class' => 'form-inline-block']) !!}
                                            <input type="hidden" name="id" value="{{ $item->id }}">
                                            <div class="btn-group">
                                                <button class="btn btn-success btn-xs"><i class="fa fa-check"></i>
                                                </button>
                                            </div>
                                            {!! Form::close() !!}

                                            @include('admin.transactions.rejected_form', $item)
                                        @elseif($item->isInvestor())
                                            @if($item->isContribution())
                                                {!! Form::open(['url' => route('admin.transactions.approve-deposit-out', $item), 'class' => 'form-inline-block']) !!}
                                                <input type="hidden" name="id" value="{{ $item->id }}">
                                                <div class="btn-group">
                                                    <button class="btn btn-success btn-xs"><i class="fa fa-check"></i>
                                                    </button>
                                                </div>
                                                {!! Form::close() !!}

                                                @include('admin.transactions.rejected_form', $item)
                                            @elseif($item->isInvestorPayInDai())
                                                {!! Form::open(['url' => route('admin.transactions.approveInvPayInDai', $item), 'class' => 'form-inline-block']) !!}
                                                <input type="hidden" name="id" value="{{ $item->id }}">
                                                <div class="btn-group">
                                                    <button class="btn btn-success btn-xs"><i class="fa fa-check"></i>
                                                    </button>
                                                </div>
                                                {!! Form::close() !!}

                                                @include('admin.transactions.rejected_form', $item)
                                            @elseif($item->isDeposit() || $item->isDepositDai())
                                                {!! Form::open(['url' => route('admin.transactions.approve-inv-out-pay', $item), 'class' => 'form-inline-block']) !!}
                                                <input type="hidden" name="id" value="{{ $item->id }}">
                                                <div class="btn-group">
                                                    <button class="btn btn-success btn-xs"><i class="fa fa-check"></i>
                                                    </button>
                                                </div>
                                                {!! Form::close() !!}

                                                @include('admin.transactions.rejected_form', $item)
                                            @elseif($item->isTypeIn())
                                                {!! Form::open(['url' => route('admin.transactions.approve-inv-out', $item), 'class' => 'form-inline-block']) !!}
                                                <div class="btn-group">
                                                    <button class="btn btn-success btn-xs"><i class="fa fa-check"></i>
                                                    </button>
                                                </div>
                                                {!! Form::close() !!}

                                                @include('admin.transactions.rejected_form', $item)
                                            @elseif($item->isStakingDai() ||$item->isStakingEuro())
                                                {!! Form::open(['url' => route('admin.transactions.approve-staking', $item), 'class' => 'form-inline-block']) !!}
                                                <div class="btn-group">
                                                    <button class="btn btn-success btn-xs"><i class="fa fa-check"></i>
                                                    </button>
                                                </div>
                                                {!! Form::close() !!}

                                                @include('admin.transactions.rejected_form', $item)
                                            @else
                                                <a class="btn btn-success btn-xs"
                                                   href="{{ route('admin.transactions.approve-inv-out-edit', $item) }}"><i
                                                        class="fa fa-check"></i></a>
                                                @include('admin.transactions.rejected_form', $item)
                                            @endif
                                        @elseif($item->is_sell)
                                            {!! Form::open(['url' => route('admin.transactions.approveTraderIsSell', $item), 'class' => 'form-inline-block']) !!}
                                            <input type="hidden" name="id" value="{{ $item->id }}">
                                            <div class="btn-group">
                                                <button class="btn btn-success btn-xs"><i class="fa fa-check"></i>
                                                </button>
                                            </div>
                                            {!! Form::close() !!}

                                            @include('admin.transactions.rejected_form', $item)
                                        @elseif($item->isTransferAsset())
                                            {!! Form::open(['url' => route('admin.transactions.approve-trader-out', $item), 'class' => 'form-inline-block']) !!}
                                            <input type="hidden" name="id" value="{{ $item->id }}">
                                            <div class="btn-group">
                                                <button class="btn btn-success btn-xs">
                                                    <i class="fa fa-check"></i>
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                            @include('admin.transactions.rejected_form', $item)
                                        @elseif($item->isTraderActiveEuro() || $item->isTraderPayIn())
                                            {!! Form::open(['url' => route('admin.transactions.approveTraderTransaction', $item), 'class' => 'form-inline-block']) !!}
                                            <input type="hidden" name="id" value="{{ $item->id }}">
                                            <div class="btn-group">
                                                <button class="btn btn-success btn-xs"><i class="fa fa-check"></i>
                                                </button>
                                            </div>
                                            {!! Form::close() !!}

                                            @include('admin.transactions.rejected_form',$item)
                                        @else
                                            {!! Form::open(['url' => route('admin.transactions.approve', $item), 'class' => 'form-inline-block']) !!}
                                            <input type="hidden" name="id" value="{{ $item->id }}">
                                            <div class="btn-group">
                                                <button class="btn btn-success btn-xs"><i class="fa fa-check"></i>
                                                </button>
                                            </div>
                                            {!! Form::close() !!}

                                            @include('admin.transactions.rejected_form', $item)
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    {{ $items->appends(request()->all())->links() }}

                </div>
            </div>
        </div>
    </div>

    @foreach ($items as $item)
        @if($item->isInvestor() && $item->isTypeOut() && !($item->isStakingEuro() || $item->isStakingDai() || $item->isContribution()))
            <div class="modal" id="DogovorDosrochno{{ $item->id }}" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Детали</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="LeftFormText d-flex align-items-center">
                                        <h6>Инвестиция:</h6>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="RightFormText d-flex align-items-center">
                                        {{--                                        <h6>{{ format_number($item->depositPayment->amount) }} = {{ format_number($item->depositPayment->amount / $item->depositPayment->typeAsset->coinValue->price ?? 1) }}--}}
                                        {{--                                            {{ $item->depositPayment->typeAsset->coinValue->symbol ?? '' }}</h6>--}}
                                        <h6>{{ format_number($item->depositPayment->amount_asset) }}
                                            {{ $item->depositPayment->typeAsset->coinValue->symbol ?? '' }}</h6>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="LeftFormText d-flex align-items-center">
                                        <h6>Начисленный %:</h6>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="RightFormText d-flex align-items-center">
                                        <h6>{{ format_number($item->depositPayment->amount_asset) - format_number
                                        ($item->depositPayment->getAmountPercentOnCurrentDateDai()) }}</h6>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="LeftFormText d-flex align-items-center">
                                        <h6>Накопленная сумма:</h6>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="RightFormText d-flex align-items-center">
                                        {{--                                        <h6>{{ format_number($item->depositPayment->getAmountPercentOnCurrentDate()) }} = {{ $item->depositPayment->getAmountPercentOnCurrentDateDai() }} {{ $item->depositPayment->typeAsset->coinValue->symbol ?? '' }}</h6>--}}
                                        <h6>{{ $item->depositPayment->getAmountPercentOnCurrentDateDai() }} {{ $item->depositPayment->typeAsset->coinValue->symbol ?? '' }}</h6>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="LeftFormText d-flex align-items-center">
                                        <h6>Комиссия системы:</h6>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="RightFormText d-flex align-items-center">
                                        <h6>{{ format_number(($item->depositPayment->amount_asset*$setting->commission_investor/100)) }} {{ $item->depositPayment->typeAsset->coinValue->symbol ?? '' }}</h6>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="LeftFormText d-flex align-items-center">
                                        <h6>Получить:</h6>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="RightFormText d-flex align-items-center">
                                        {{--                                        <h6>{{ format_number($item->depositPayment->amount - $item->depositPayment->amount*$setting->commission_investor/100) }} = {{ format_number(($item->depositPayment->amount - $item->depositPayment->amount*$setting->commission_investor/100) / ($item->depositPayment->typeAsset->coinValue->price ?? 1)) }} {{ $item->depositPayment->typeAsset->coinValue->symbol ?? '' }}</h6>--}}
                                        <h6>{{ format_number(($item->depositPayment->amount_asset - $item->depositPayment->amount_asset*$setting->commission_investor/100)) }} {{ $item->depositPayment->typeAsset->coinValue->symbol ?? '' }}</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($item->is_redeem)
            <div class="modal HeaderBlue" id="DogovorDosrochno{{ $item->id }}" tabindex="-1" role="dialog"
                 data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Детали</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="BoxPaddActive">
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Сумма договора:</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->creditPayment->contract->amount }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Количество актива:</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ numeric_fmt( $item->creditPayment->contract->amount_asset) }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Текущая стоимость актива:</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->creditPayment->contract->getCurrentAmountAssetCost() }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Остаток основного долга:</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->creditPayment->contract->getCurrentContractPaymentAmount() }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Комиссия системы:</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ format_number($item->creditPayment->contract->amount*$setting->commission_sell_trader/100) }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Оплатить:</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ format_number($item->creditPayment->contract->getCurrentContractPaymentAmount()) + format_number($item->creditPayment->contract->amount*$setting->commission_sell_trader/100)}}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($item->is_sell)
            <div class="modal HeaderBlue" id="DogovorDosrochno{{ $item->id }}" tabindex="-1" role="dialog"
                 data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Детали</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="BoxPaddActive">
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Сумма договора:</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->creditPayment->contract->amount }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Количество актива:</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ numeric_fmt($item->creditPayment->contract->amount_asset) }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Текущая стоимость актива:</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->creditPayment->contract->getCurrentAmountAssetCost() }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Остаток основного долга:</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->creditPayment->contract->getCurrentContractPaymentAmount() }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Комиссия системы:</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ format_number($item->creditPayment->contract->getCurrentAmountAssetCost()*$setting->commission_trader/100) }} </h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Пеня:</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->creditPayment->contract->payments->sum('penalty') }} </h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Вывести средства:</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>
                                                {{
                                                    format_number($item->creditPayment->contract->getCurrentAmountAssetCost())
                                                    - format_number($item->creditPayment->contract->getCurrentAmountAssetCost()*$setting->commission_trader/100)
                                                    - $item->creditPayment->contract->getCurrentContractPaymentAmount()
                                                    - $item->creditPayment->contract->payments->sum('penalty')
                                                }}
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($item->isPayout())
            <!-- Modal DepositOut -->
            <div class="modal HeaderBlue" id="TraderOut{{ $item->id }}" tabindex="-1" role="dialog"
                 data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Детали</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="BoxPaddActive">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Доступно к выводу:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->client->payout_balance }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Выводимая сумма:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->amount }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>На счет:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->wallet_to }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($item->isTraderPayIn())
            <!-- Modal DepositOut -->
            <div class="modal HeaderBlue" id="TraderPayIn{{ $item->id }}" tabindex="-1" role="dialog"
                 data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Детали</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="BoxPaddActive">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Доступно к выводу:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->client->payout_balance }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Покупается на сумму</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->amount }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Кол-во актива:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->amount_dai }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($item->isTraderActiveEuro())
            <!-- Modal DepositOut -->
            <div class="modal HeaderBlue" id="TraderActiveEuro{{ $item->id }}" tabindex="-1" role="dialog"
                 data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Детали</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="BoxPaddActive">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Сумма</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>
                                                {{ format_number($item->amount_dai *  $item->coinmarketcap->price) }}
{{--                                                {{ $item->amount }}--}}
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Комиссия за конвертацию</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>
                                                {{ format_number($item->amount_dai *  $item->coinmarketcap->price * $setting->conversion_fee / 100) }}
{{--                                                {{ format_number($item->amount * $setting->conversion_fee / 100) }}--}}
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>К выводу:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>
                                                {{ format_number($item->amount) }}
{{--                                                {{ format_number($item->amount - $item->amount * $setting->conversion_fee / 100) }}--}}
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Кол-во актива:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->amount_dai }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Кошелек:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->wallet_to }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($item->isTransferAsset())
            <!-- Modal DepositOut -->
            <div class="modal HeaderBlue" id="TransferAsset{{ $item->id }}" tabindex="-1" role="dialog"
                 data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Детали</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="BoxPaddActive">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Свободные
                                                активы {{ $item->coinmarketcap ? $item->coinmarketcap->symbol : '' }}
                                                :</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ numeric_fmt($item->getSumFreeActive()) }}</h6>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Комиссия:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ numeric_fmt(percent($item->amount_dai, 1, $setting->conversion_fee )) }}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Сумма к выводу:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ numeric_fmt(conclusion($item->amount_dai, 1, $setting->conversion_fee )) }}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Кошелек:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->wallet }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($item->isStakingDai())
            <!-- Modal DepositOut -->
            <div class="modal HeaderBlue" id="StakingDai{{ $item->id }}" tabindex="-1" role="dialog"
                 data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Детали</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="BoxPaddActive">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Баласн {{ $item->coinmarketcap->symbol ?? '' }}:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ numeric_fmt($item->client->getBalanceByCoin($item->coinmarketcap_id)) }}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Кол-во {{ $item->coinmarketcap->symbol ?? '' }}:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ numeric_fmt($item->amount) }}</h6>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Комиссия:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ numeric_fmt(percent($item->amount, 1, $setting->conversion_fee )) }}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Сумма к выводу:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ numeric_fmt(conclusion($item->amount, 1, $setting->conversion_fee )) }}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Номер кошелька:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->wallet }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($item->isStakingEuro())
            <!-- Modal DepositOut -->
            <div class="modal HeaderBlue" id="StakingEuro{{ $item->id }}" tabindex="-1" role="dialog"
                 data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Детали</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="BoxPaddActive">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Кол-во {{ $item->coinmarketcap->symbol ?? '' }}:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->amount_dai }}</h6>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Комиссия:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ format_number(percent($item->amount_dai, 1, $setting->conversion_fee )) }}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>К выводу:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ format_number(conclusion($item->amount_dai, 1, $setting->conversion_fee )) }}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Кол-во Евро:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->amount }}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Кошелек:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->wallet_to }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($item->isDepositDai())
            <!-- Modal DepositOut -->
            <div class="modal HeaderBlue" id="DepositDai{{ $item->id }}" tabindex="-1" role="dialog"
                 data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Детали</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="BoxPaddActive">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Свободные активы {{ $item->coinmarketcap->symbol ?? '' }}:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
{{--                                            <h6>{{ $item->client->free_active }}</h6>--}}
                                            <h6>
                                                {{ $item->client->balances->where('asset_id', $item->coinmarketcap_id)->first()->amount }}
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Инвестиция в {{ $item->coinmarketcap->symbol ?? '' }}:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->amount_dai }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($item->isInvestorPayInDai())
            <!-- Modal DepositOut -->
            <div class="modal HeaderBlue" id="InvestorPayInDai{{ $item->id }}" tabindex="-1" role="dialog"
                 data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Детали</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="BoxPaddActive">
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Сумма :</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->amount }}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Откуда:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->wallet }}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Куда:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->wallet_to }}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>Монета:</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-12">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $item->coinmarketcap->symbol ?? '' }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@endsection
