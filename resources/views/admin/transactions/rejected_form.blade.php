{!! Form::open(['url' => route('admin.transactions.reject', $item), 'class' => 'form-inline-block']) !!}
<input type="hidden" name="_method" value="PUT">
<div class="btn-group">
    <button class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button>
</div>
{!! Form::close() !!}
