@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Переводы: изменить</h3>
@endsection

@section('parent_page')
    <a href="{{ route('admin.translations.index') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
@endsection

@section('resource-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::model($translation, ['method' => 'PUT', 'url' => route('admin.translations.update', $translation)]) !!}
                        @include('admin.translations.fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
