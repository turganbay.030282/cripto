<div class="form-group @isset($translation) hidden @endisset">
    {!! Form::label('key', trans('admin.forms.title-key')) !!}
    {{ Form::text('key', null, ['class' => 'form-control']) }}
    {{ Form::error('key') }}
</div>

@foreach(config('translatable.locales') as $key => $locale)
    <div class="form-group">
        {!! Form::label('text.'. $locale, trans('admin.forms.title-value') . ' ' . $locale) !!}
        {{ Form::textarea('text['.$locale.']', null, ['class' => 'form-control', 'rows' => 5]) }}
        {{ Form::error('text.'. $locale) }}
    </div>
@endforeach

{{ Form::btn_save() }}
