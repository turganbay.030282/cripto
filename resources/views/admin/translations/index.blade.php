@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Переводы</h3>
@endsection
@section('parent_page')
    <a href="{{ route('admin.translations.index') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
@endsection
@section('resource-content')
    <div class="page-filter clearfix">
        <form action="?" method="GET">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="key" class="col-form-label">@lang('admin.forms.title-key')</label>
                        <input id="key" class="form-control" name="key" value="{{ request('key') }}">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="value" class="col-form-label">@lang('admin.forms.title-value')</label>
                        <input id="value" class="form-control" name="value" value="{{ request('value') }}">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="col-form-label">&nbsp;</label><br />
                        <button title="@lang('admin.forms.btn-search')" type="submit" class="btn bg-orange"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <a title="Очистить" href="?" class="btn btn-default"><i class="fa fa-retweet" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <a href="{{ route("admin.translations.create") }}" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i> @lang('admin.forms.btn-create')
                    </a>

                    <table class="table table-hover table-border jambo_table">
                        <thead>
                        <tr class="headings">
                            <th>@sortablelink('id', 'ID')</th>
                            <th>@lang('admin.forms.title-key')</th>
                            <th>@lang('admin.forms.title-value')</th>
                            <th class="text-right">@lang('admin.forms.title-actions')</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($translations as $translation)
                            <tr>
                                <td>{{ $translation->id }}</td>
                                <td>{{ $translation->key }}</td>
                                <td>{{ $translation->text[config('app.locale')] }}</td>
                                <td class="text-right">
                                    <a href="{{ route('admin.translations.edit', $translation) }}" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @endforeach

                    </table>

                    {{ $translations->appends(request()->all())->links() }}

                </div>
            </div>
        </div>
    </div>

@endsection
