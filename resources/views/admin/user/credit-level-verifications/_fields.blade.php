<div class="form-group">
    {!! Form::label('amount', 'Сумма кредита') !!}
    {{ Form::text('amount', $creditLevelVerification->amount, ['class' => 'form-control']) }}
    {{ Form::error('amount') }}
</div>
<div class="form-group">
    {!! Form::label('credit_level_id', 'Кредитный уровень') !!}
    {{ Form::select('credit_level_id', $levels, null, ['class' => 'form-control']) }}
    {{ Form::error('credit_level_id') }}
</div>
<div class="form-group">
    {!! Form::label('title', 'Статус') !!}
    {{ Form::select('status', $statuses, null, ['class' => 'form-control']) }}
    {{ Form::error('status') }}
</div>
<div class="form-group">
    {!! Form::label('comment', 'Причина отклонения') !!}
    {{ Form::text('comment', null, ['class' => 'form-control']) }}
    {{ Form::error('comment') }}
</div>
{{ Form::btn_save() }}
