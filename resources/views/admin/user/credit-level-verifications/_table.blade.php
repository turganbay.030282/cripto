<table class="table table-bordered table-striped">
    <tbody>
    <tr>
        <th>ID</th><td>{{ $creditLevelVerification->id }}</td>
    </tr>
    <tr>
        <th>Сумма</th><td>{{ $creditLevelVerification->amount }}</td>
    </tr>
    <tr>
        <th>Имя</th><td>{{ $creditLevelVerification->first_name }}</td>
    </tr>
    <tr>
        <th>Фамилия</th><td>{{ $creditLevelVerification->last_name }}</td>
    </tr>
    <tr>
        <th>Пол</th><td>{{ $creditLevelVerification->sex }}</td>
    </tr>
    <tr>
        <th>Персональный код</th><td>{{ $creditLevelVerification->personal_code }}</td>
    </tr>
    <tr>
        <th>Семейное положение</th><td>{{ $creditLevelVerification->family_status }}</td>
    </tr>
    <tr>
        <th>Политические функции выполняются</th><td>{{ $creditLevelVerification->political_functions }}</td>
    </tr>
    <tr>
        <th>Номер телефона</th><td>{{ $creditLevelVerification->phone }}</td>
    </tr>
    <tr>
        <th>Иждивенцы</th><td>{{ $creditLevelVerification->dependents }}</td>
    </tr>
    <tr>
        <th>Тип проживания</th><td>{{ $creditLevelVerification->type_accommodation }}</td>
    </tr>
    <tr>
        <th>Образование</th><td>{{ $creditLevelVerification->education }}</td>
    </tr>
    <tr>
        <th>Гражданство</th><td>{{ $creditLevelVerification->citizenship }}</td>
    </tr>
    <tr>
        <th>Место жительства Страна</th><td>{{ $creditLevelVerification->residence_county }}</td>
    </tr>
    <tr>
        <th>Место жительства Город, область</th><td>{{ $creditLevelVerification->residence_city }}</td>
    </tr>
    <tr>
        <th>Место жительства Улица, номер дома, квартиры или уезд</th><td>{{ $creditLevelVerification->residence_street }}</td>
    </tr>
    <tr>
        <th>Место жительства почтовый индекс</th><td>{{ $creditLevelVerification->residence_postcode }}</td>
    </tr>
    <tr>
        <th>Источник дохода</th><td>{{ $creditLevelVerification->income_source }}</td>
    </tr>
    <tr>
        <th>Нетто доходы, EUR</th><td>{{ $creditLevelVerification->income_net }}</td>
    </tr>
    <tr>
        <th>Дополнительные доходы, EUR</th><td>{{ $creditLevelVerification->income_additional }}</td>
    </tr>
    <tr>
        <th>Место работы</th><td>{{ $creditLevelVerification->income_place }}</td>
    </tr>
    <tr>
        <th>Должность</th><td>{{ $creditLevelVerification->income_position }}</td>
    </tr>
    <tr>
        <th>Стаж с последнего места работы</th><td>{{ $creditLevelVerification->income_work_experience }}</td>
    </tr>
    <tr>
        <th>Имеются ли у вас кредитные обязательства?</th><td>{{ $creditLevelVerification->credit_liabilities }}</td>
    </tr>
    <tr>
        <th>Ежемесячный платёж по общим кредитным обязательствам, EUR</th><td>{{ $creditLevelVerification->credit_liabilities_month }}</td>
    </tr>
    @if($creditLevelVerification->credit_liabilities == 'yes')
        <tr>
            <th>Вид кредита</th><td>{{ $creditLevelVerification->credit_type }}</td>
        </tr>
        <tr>
            <th>Срок кредита</th><td>{{ $creditLevelVerification->credit_date_from }} - {{ $creditLevelVerification->credit_date_to }}</td>
        </tr>
    @endif
    <tr>
        <th>1. Выписка со счета за последние 6 месяцев</th>
        <td>
            @if($creditLevelVerification->file_statement)
                <a href="{{ $creditLevelVerification->getFileUrl('file_statement') }}" download=""><i class="fa fa-download" aria-hidden="true"></i></a>
            @endif
        </td>
    </tr>
    <tr>
        <th>2. Справка о доходах с места работы</th>
        <td>
            @if($creditLevelVerification->file_income_work)
                <a href="{{ $creditLevelVerification->getFileUrl('file_income_work') }}" download=""><i class="fa fa-download" aria-hidden="true"></i></a>
            @endif
        </td>
    </tr>
    <tr>
        <th>3. Счет за коммунальные платежи/хозяйственные услуги</th>
        <td>
            @if($creditLevelVerification->file_utility_bill)
                <a href="{{ $creditLevelVerification->getFileUrl('file_utility_bill') }}" download=""><i class="fa fa-download" aria-hidden="true"></i></a>
            @endif
        </td>
    </tr>
    <tr>
        <th>Дата создания</th><td>{{ $creditLevelVerification->created_at->format('d.m.Y H:i:s') }}</td>
    </tr>
    <tr>
        <th>Дата изменения</th><td>{{ $creditLevelVerification->updated_at->format('d.m.Y H:i:s') }}</td>
    </tr>
    </tbody>
</table>
