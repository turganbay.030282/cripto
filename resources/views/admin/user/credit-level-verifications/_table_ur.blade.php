<table class="table table-bordered table-striped">
    <tbody>
    <tr>
        <th>ID</th><td>{{ $creditLevelVerification->id }}</td>
    </tr>
    <tr>
        <th>Сумма</th><td>{{ $creditLevelVerification->amount }}</td>
    </tr>
    <tr>
        <th colspan="2"><h4><strong>Информация о клиенте</strong></h4></th>
    </tr>
    <tr>
        <th>Название компании</th><td>{{ $creditLevelVerification->company_name }}</td>
    </tr>
    <tr>
        <th>Регистрационный номер</th><td>{{ $creditLevelVerification->reg_number }}</td>
    </tr>
    <tr>
        <th>Дата регистрации</th><td>{{ $creditLevelVerification->register_date }}</td>
    </tr>
    <tr>
        <th>Юридический адрес</th><td>{{ $creditLevelVerification->ur_address }}</td>
    </tr>
    <tr>
        <th>Фактический адрес</th><td>{{ $creditLevelVerification->address }}</td>
    </tr>
    <tr>
        <th>Контактный телефон</th><td>{{ $creditLevelVerification->contact_phone }}</td>
    </tr>
    <tr>
        <th>Email</th><td>{{ $creditLevelVerification->email }}</td>
    </tr>
    <tr>
        <th>Основной вид деятельности</th><td>{{ $creditLevelVerification->primary_occupation }}</td>
    </tr>
    @if($creditLevelVerification->persons->isNotEmpty())
        <tr>
            <th colspan="2"><h4><strong>Существующие обязательства</strong></h4></th>
        </tr>
        @foreach($creditLevelVerification->persons as $person)
            <tr>
                <th>Имя</th><td>{{ $person->name }}</td>
            </tr>
            <tr>
                <th>Фамилия</th><td>{{ $person->surname }}</td>
            </tr>
            <tr>
                <th>Идентификационный номер</th><td>{{ $person->personal_code }}</td>
            </tr>
            <tr>
                <th>Доли капитала / акции (кол-во и соотношение в %)</th><td>{{ $person->capital_amount }}</td>
            </tr>
        @endforeach
    @endif
    <tr>
        <th colspan="2"><h4><strong>Лица с правом представительства</strong></h4></th>
    </tr>
    <tr>
        <th>Имя</th><td>{{ $creditLevelVerification->person_name }}</td>
    </tr>
    <tr>
        <th>Фамилия</th><td>{{ $creditLevelVerification->person_surname }}</td>
    </tr>
    <tr>
        <th>Идентификационный номер</th><td>{{ $creditLevelVerification->person_personal_code }}</td>
    </tr>
    <tr>
        <th>Оборот за предыдущий расчетный период</th><td>{{ $creditLevelVerification->person_turnover_prev }}</td>
    </tr>
    <tr>
        <th>Прибыль за предыдущий расчетный период</th><td>{{ $creditLevelVerification->person_profit_prev }}</td>
    </tr>
    <tr>
        <th>Банки, в которых открыты расчетные счета</th><td>{{ $creditLevelVerification->person_banks }}</td>
    </tr>

    <tr>
        <th colspan="2"><h4><strong>Существующие обязательства</strong></h4></th>
    </tr>
    <tr>
        <th>Кредитор</th><td>{{ $creditLevelVerification->creditor }}</td>
    </tr>
    <tr>
        <th>Начальная сумма</th><td>{{ $creditLevelVerification->start_amount }}</td>
    </tr>
    <tr>
        <th>Остаток обязательств</th><td>{{ $creditLevelVerification->balance_obligations }}</td>
    </tr>
    <tr>
        <th>Срок погашения</th><td>{{ $creditLevelVerification->credit_date_to }}</td>
    </tr>
    <tr>
        <th>Ежемесячный платеж</th><td>{{ $creditLevelVerification->month_payments }}</td>
    </tr>
    <tr>
        <th>Цель использования кредита</th><td>{{ $creditLevelVerification->credit_target }}</td>
    </tr>
    <tr>
        <th>Источники погашения кредита</th><td>{{ $creditLevelVerification->credit_source }}</td>
    </tr>
    <tr>
        <th colspan="2"><h4><strong>Выписка со счета</strong></h4></th>
    </tr>
    <tr>
        <th>1. Выписка со счета за последние 6 месяцев</th>
        <td>
            @if($creditLevelVerification->file_statement)
                <a href="{{ $creditLevelVerification->getFileUrl('file_statement') }}" download=""><i class="fa fa-download" aria-hidden="true"></i></a>
            @endif
        </td>
    </tr>
    <tr>
        <th>2. Справка о доходах с места работы</th>
        <td>
            @if($creditLevelVerification->file_income_work)
                <a href="{{ $creditLevelVerification->getFileUrl('file_income_work') }}" download=""><i class="fa fa-download" aria-hidden="true"></i></a>
            @endif
        </td>
    </tr>
    <tr>
        <th>3. Счет за коммунальные платежи/хозяйственные услуги</th>
        <td>
            @if($creditLevelVerification->file_utility_bill)
                <a href="{{ $creditLevelVerification->getFileUrl('file_utility_bill') }}" download=""><i class="fa fa-download" aria-hidden="true"></i></a>
            @endif
        </td>
    </tr>
    <tr>
        <th>Дата создания</th><td>{{ $creditLevelVerification->created_at->format('d.m.Y H:i:s') }}</td>
    </tr>
    <tr>
        <th>Дата изменения</th><td>{{ $creditLevelVerification->updated_at->format('d.m.Y H:i:s') }}</td>
    </tr>
    </tbody>
</table>
