@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Заявления на Кредитный скорринг</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::open(['url' => route('admin.user.credit-level-verifications.updateUr', $creditLevelUrVerification), 'method' => 'PUT']) !!}
                        @include('admin.user.credit-level-verifications._fields', ['creditLevelVerification' => $creditLevelUrVerification])
                    {!! Form::close() !!}

                    @include('admin.user.credit-level-verifications._table_ur', ['creditLevelVerification' => $creditLevelUrVerification])
                </div>
            </div>
        </div>
    </div>

@endsection
