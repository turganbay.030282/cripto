@extends('admin.layouts.resource-layout')

@section('title')
    <h3>{{ $isArchive ? 'Архив' : '' }} Заявления на Кредитный скорринг</h3>
@endsection

@section('resource-content')

    <div class="page-filter clearfix">
        <form action="?" method="GET">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="id" class="col-form-label">ID</label>
                        <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        {!! Form::error('id') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="status" class="col-form-label">Статус</label>
                        <select id="status" class="form-control" name="status">
                            <option value=""></option>
                            @foreach ($statuses as $value => $label)
                                <option value="{{ $value }}" {{ $value == request('status') ? 'selected' : '' }}>{{ $label }}</option>
                            @endforeach
                        </select>
                        {!! Form::error('status') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group text-right">
                        <label class="col-form-label">&nbsp;</label><br />
                        <button title="Поиск" type="submit" class="btn bg-orange"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <a title="Очистить" href="?" class="btn btn-default"><i class="fa fa-retweet" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <div class="row">
                        <div class="col-xs-12 text-right">
                            @if($isArchive)
                                <a href="{{route("admin.user.credit-level-verifications.index")}}" class="btn btn-info btn-sm">
                                    <i class="fa fa-archive" aria-hidden="true"></i></i> Активные
                                </a>
                            @else
                                <a href="{{route("admin.user.credit-level-verifications.archive")}}" class="btn btn-warning btn-sm">
                                    <i class="fa fa-archive" aria-hidden="true"></i></i> Архив
                                </a>
                                <a href="{{route("admin.user.credit-level-verifications.archiveUr")}}" class="btn btn-warning btn-sm">
                                    <i class="fa fa-archive" aria-hidden="true"></i></i> Архив Юр.лица
                                </a>
                            @endif
                        </div>
                    </div>

                    <table class="table table-hover jambo_table">
                        <thead>
                            <tr>
                                <th>@sortablelink('id', 'ID')</th>
                                <th>Юр. лицо</th>
                                <th>@sortablelink('user.last_name', 'Клиент')</th>
                                <th>@sortablelink('amount', 'Сумма')</th>
                                <th>@sortablelink('status', 'Статус')</th>
                                <th>Рейтинг</th>
                                <th>@sortablelink('created_at', 'Дата создания')</th>
                                <th class="text-right">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    {{$user->id}}
                                </td>
                                <td>
                                    @isset($user->is_ur)
                                        <span class="label label-success">Да</span>
                                    @endif
                                </td>
                                <td>
                                    @if($user->user)
                                        <a href="{{ route('admin.user.users.show', $user->user) }}" target="_blank">
                                            {{ $user->user->full_name }}
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    {{$user->amount}}
                                </td>
                                <td>
                                    <span class="label {{ $user->isWait() ? 'label-warning' : ($user->isReject() ? 'label-danger' : 'label-success') }}">
                                        {{ $user->status_name }}
                                    </span>
                                </td>
                                <td>
                                    @if($user->user)
                                        {{ $user->user->creditLevel ? $user->user->creditLevel->name : '' }}
                                    @endif
                                </td>
                                <td>
                                    {{ $user->created_at }}
                                </td>
                                <td class="text-right">
                                    @if($user->is_ur)
                                        <a href="{{ route('admin.user.credit-level-verifications.showUr', $user) }}" class="btn btn-warning btn-xs" title="Просмотр">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    @else
                                        <a href="{{ route('admin.user.credit-level-verifications.show', $user) }}" class="btn btn-warning btn-xs" title="Просмотр">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    @endif
                                    @if(!$isArchive)
                                            @if($user->is_ur)
                                                <a href="{{ route('admin.user.credit-level-verifications.editUr', $user) }}" class="btn btn-info btn-xs" title="Изменить">
                                                    <i class="fa fa-check"></i>
                                                </a>
                                            @else
                                                <a href="{{ route('admin.user.credit-level-verifications.edit', $user) }}" class="btn btn-info btn-xs" title="Изменить">
                                                    <i class="fa fa-check"></i>
                                                </a>
                                            @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

@endsection
