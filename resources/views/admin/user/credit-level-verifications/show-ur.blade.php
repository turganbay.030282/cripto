@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Заявления на Кредитный скорринг Юр.лица: просмотр {{ $creditLevelVerification->id }}</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    @include('admin.user.credit-level-verifications._table_ur')
                </div>
            </div>
        </div>
    </div>

@endsection
