@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Заявления на Кредитную линию</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-hover jambo_table">
                        <tbody>
                            <tr>
                                <td>ID</td>
                                <td>{{$creditLineVerification->id}}</td>
                            </tr>
                            <tr>
                                <td>@lang('site.page.cabinet.Credit-amount')</td>
                                <td>{{$creditLineVerification->amount}}</td>
                            </tr>
                        </tbody>
                    </table>

                    {!! Form::open(['url' => route('admin.user.credit-line-verifications.update', $creditLineVerification), 'method' => 'PUT']) !!}
                        @include('admin.user.credit-line-verifications._fields')
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection
