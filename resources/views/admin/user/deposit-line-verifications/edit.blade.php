@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Заявления на Депозит</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-hover jambo_table">
                        <tbody>
                            <tr>
                                <td>ID</td>
                                <td>{{$depositLineVerification->id}}</td>
                            </tr>
                            <tr>
                                <td>@lang('site.page.cabinet.Credit-amount')</td>
                                <td>{{$depositLineVerification->amount}}</td>
                            </tr>
                        </tbody>
                    </table>

                    {!! Form::open(['url' => route('admin.user.deposit-line-verifications.update', $depositLineVerification), 'method' => 'PUT']) !!}
                        @include('admin.user.deposit-line-verifications._fields')
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection
