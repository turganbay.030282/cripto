<div class="form-group">
    {!! Form::label('investor_rank_id', 'Инвестиционный ранг') !!}
    {{ Form::select('investor_rank_id', $ranks, null, ['class' => 'form-control']) }}
    {{ Form::error('investor_rank_id') }}
</div>
<div class="form-group">
    {!! Form::label('title', 'Статус') !!}
    {{ Form::select('status', $statuses, null, ['class' => 'form-control']) }}
    {{ Form::error('status') }}
</div>
<div class="form-group">
    {!! Form::label('comment', 'Причина отклонения') !!}
    {{ Form::text('comment', null, ['class' => 'form-control']) }}
    {{ Form::error('comment') }}
</div>
{{ Form::btn_save() }}
