<table class="table table-bordered table-striped">
    <tbody>
    <tr>
        <th>ID</th>
        <td width="40%">{{ $investRankVerification->id }}</td>
    </tr>
    <tr>
        <th colspan="2"><h4><strong>Персональные данные клиента</strong></h4></th>
    </tr>
    <tr>
        <th>Имя и фамилия</th><td>{{ $investRankVerification->full_name }}</td>
    </tr>
    <tr>
        <th>Дата рождения</th><td>{{ $investRankVerification->birthday }}</td>
    </tr>
    <tr>
        <th>Пол</th><td>{{ $investRankVerification->sex }}</td>
    </tr>
    <tr>
        <th>Идентификационный код</th><td>{{ $investRankVerification->bin }}</td>
    </tr>
    <tr>
        <th>Место рождения</th><td>{{ $investRankVerification->place_birth }}</td>
    </tr>
    <tr>
        <th>Страна налогового резидентства</th><td>{{ $investRankVerification->country }}</td>
    </tr>
    <tr>
        <th>Адрес проживания</th><td>{{ $investRankVerification->address }}</td>
    </tr>
    <tr>
        <th>Телефонный номер</th><td>{{ $investRankVerification->phone }}</td>
    </tr>
    <tr>
        <th>Электронная почта</th><td>{{ $investRankVerification->email }}</td>
    </tr>
    <tr>
        <th>Хозяйственная или профессиональная деятельность</th>
        <td>{{ $investRankVerification->professional_activities }}</td>
    </tr>
    <tr>
        <th>Предполагаемый общий объем транзакций за календарный год</th>
        <td>
            @if($investRankVerification->volume_1)
                @lang('site.page.investor.anket-volume1')<br>
            @endif
            @if($investRankVerification->volume_2)
                @lang('site.page.investor.anket-volume2')<br>
            @endif
            @if($investRankVerification->volume_3)
                @lang('site.page.investor.anket-volume3')<br>
            @endif
            @if($investRankVerification->volume_4)
                @lang('site.page.investor.anket-volume4')<br>
            @endif
            @if($investRankVerification->volume_5)
                @lang('site.page.investor.anket-volume5')<br>
            @endif
        </td>
    </tr>
    <tr>
        <th colspan="2"><h4><strong>Источник средств</strong></h4></th>
    </tr>
    <tr>
        <td colspan="2">
            @if($investRankVerification->source_business_activities)
                @lang('site.page.investor.anket-source_business_activities')<br>
            @endif
            @if($investRankVerification->source_dividend)
                @lang('site.page.investor.anket-source_dividend')<br>
            @endif
            @if($investRankVerification->source_loans)
                @lang('site.page.investor.anket-source_loans')<br>
            @endif
            @if($investRankVerification->source_income_assets)
                @lang('site.page.investor.anket-source_income_assets')<br>
            @endif
            @if($investRankVerification->source_contributions)
                @lang('site.page.investor.anket-source_contributions')<br>
            @endif
            @if($investRankVerification->source_prize)
                @lang('site.page.investor.anket-source_prize')<br>
            @endif
            @if($investRankVerification->source_other)
                @lang('site.page.investor.anket-source_other') : {{ $investRankVerification->source_other_text }}
            @endif
        </td>
    </tr>
    <tr>
        <th>Страна происхождения средств</th><td>{{ $investRankVerification->source_country }}</td>
    </tr>
    <tr>
        <th>Я подтверждаю, что являюсь фактическим бенефициаром предлагаемой сделки</th>
        <td>
            @if($investRankVerification->i_beneficiary)
                Да
            @else
                Нет
            @endif
        </td>
    </tr>
    <tr>
        <th>для совершения операции реальным бенефициаром является:</th><td>{{ $investRankVerification->beneficiary_name }}</td>
    </tr>
    <tr>
        <th colspan="2"><h4><strong>Данные политически значимого лица</strong></h4></th>
    </tr>

    <tr>
        <th>Я подтверждаю, я не являюсь политически значимым лицом, членом его или ее семьи или близким соратником (PEP*)</th>
        <td>
            @if($investRankVerification->not_pep)
                Да
            @else
                Нет
            @endif
        </td>
    </tr>
    <tr>
        <th>Я связан со следующими политически значимыми лицами, членами их семей или близкими соратниками:</th><td>{{ $investRankVerification->politics }}</td>
    </tr>
    <tr>
        <th>Дата создания</th><td>{{ $investRankVerification->created_at->format('d.m.Y H:i:s') }}</td>
    </tr>
    <tr>
        <th>Дата изменения</th><td>{{ $investRankVerification->updated_at->format('d.m.Y H:i:s') }}</td>
    </tr>
    </tbody>
</table>
