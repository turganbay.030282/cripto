@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Заявления на Инвестиционный ранг</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-hover jambo_table">
                        <tbody>
                            <tr>
                                <td>ID</td>
                                <td>{{$investRankVerification->id}}</td>
                            </tr>
{{--                            <tr>--}}
{{--                                <td>Сумма инвестици</td>--}}
{{--                                <td>{{$investRankVerification->amount}}</td>--}}
{{--                            </tr>--}}
                        </tbody>
                    </table>

                    {!! Form::open(['url' => route('admin.user.invest-rank-verifications.update', $investRankVerification), 'method' => 'PUT']) !!}
                        @include('admin.user.invest-rank-verifications._fields')
                    {!! Form::close() !!}

                    @if($investRankVerification->user->is_ur)
                        @include('admin.user.invest-rank-verifications._table_ur')
                    @else
                        @include('admin.user.invest-rank-verifications._table')
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection
