@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Заявления на Инвестиционный ранг: просмотр {{ $investRankVerification->id }}</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    @if($user->is_ur)
                        @include('admin.user.invest-rank-verifications._table_ur')
                    @else
                        @include('admin.user.invest-rank-verifications._table')
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
