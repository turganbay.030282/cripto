@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Изменить пароль</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    {!! Form::open(['url' => route('admin.user.profile.setPassword'), 'class' => 'form-horizontal form-label-left']) !!}

                        <div class="form-group">
                            {{ Form::label('password', 'Пароль', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('password', null, ['class' => 'form-control']) }}
                                {{ Form::error('password') }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('password', 'Повторите пароль', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('password_confirmation', null, ['class' => 'form-control']) }}
                                {{ Form::error('password_confirmation') }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12 col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-success">Изменить</button>
                            </div>
                        </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection


@section('stylesheet')
    <style>
        #generate-password{
            cursor: pointer;
        }
    </style>
@endsection
@section('script')
    <script>
        $("#generate-password").on("click", function(){
            $("#password").val(password_generator(7));
        });
    </script>
@endsection
