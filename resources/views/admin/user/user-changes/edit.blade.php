@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Заявления на изменение данных</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-hover jambo_table">
                        <tbody>
                            <tr>
                                <td>ID</td>
                                <td>{{$userChange->id}}</td>
                            </tr>
                            <tr>
                                <td>Имя</td>
                                <td>{{$userChange->first_name}}</td>
                            </tr>
                            <tr>
                                <td>Фамилия</td>
                                <td>{{$userChange->last_name}}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{{$userChange->email}}</td>
                            </tr>
                            <tr>
                                <td>Телефон</td>
                                <td>{{$userChange->phone}}</td>
                            </tr>
                            <tr>
                                <td>Дата рождения</td>
                                <td>{{$userChange->birthday}}</td>
                            </tr>
                            <tr>
                                <td>Пасспорт</td>
                                <td>
                                    <a href="{{ $userChange->getPhotoUrl('card_id', 'user_verifications') }}" class="popup">
                                        <img src="{{ $userChange->getPhotoUrl('card_id', 'user_verifications') }}" alt="" style="height: 40px;width: auto;">
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    {!! Form::open(['url' => route('admin.user.user-changes.update', $userChange), 'method' => 'PUT']) !!}
                        @include('admin.user.user-changes._fields')
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection
