@extends('admin.layouts.resource-layout')

@section('title')
    <h3>{{ $isArchive ? 'Архив' : '' }} Заявления на изменение данных</h3>
@endsection

@section('resource-content')

    <div class="page-filter clearfix">
        <form action="?" method="GET">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="id" class="col-form-label">ID</label>
                        <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        {!! Form::error('id') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="name" class="col-form-label">Имя</label>
                        <input id="name" class="form-control" name="name" value="{{ request('name') }}">
                        {!! Form::error('name') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="email" class="col-form-label">Email</label>
                        <input id="email" class="form-control" name="email" value="{{ request('email') }}">
                        {!! Form::error('email') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="phone" class="col-form-label">Телефон</label>
                        <input id="phone" class="form-control" name="phone" value="{{ request('phone') }}">
                        {!! Form::error('phone') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="status" class="col-form-label">Статус</label>
                        <select id="status" class="form-control" name="status">
                            <option value=""></option>
                            @foreach ($statuses as $value => $label)
                                <option value="{{ $value }}" {{ $value == request('status') ? 'selected' : '' }}>{{ $label }}</option>
                            @endforeach
                        </select>
                        {!! Form::error('status') !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group text-right">
                        <label class="col-form-label">&nbsp;</label><br />
                        <button title="Поиск" type="submit" class="btn bg-orange"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <a title="Очистить" href="?" class="btn btn-default"><i class="fa fa-retweet" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <div class="row">
                        <div class="col-xs-12 text-right">
                            @if($isArchive)
                                <a href="{{route("admin.user.user-changes.index")}}" class="btn btn-info btn-sm">
                                    <i class="fa fa-archive" aria-hidden="true"></i></i> Активные
                                </a>
                            @else
                                <a href="{{route("admin.user.user-changes.archive")}}" class="btn btn-warning btn-sm">
                                    <i class="fa fa-archive" aria-hidden="true"></i></i> Архив
                                </a>
                            @endif
                        </div>
                    </div>

                    <table class="table table-hover jambo_table">
                        <thead>
                            <tr>
                                <th>@sortablelink('id', 'ID')</th>
                                <th>@sortablelink('user.last_name', 'Клиент')</th>
                                <th>@sortablelink('first_name', 'Имя')</th>
                                <th>@sortablelink('last_name', 'Фамилия')</th>
                                <th>@sortablelink('email', 'Email')</th>
                                <th>@sortablelink('phone', 'Телефон')</th>
                                <th>@sortablelink('birthday', 'Дата рождения')</th>
                                <th>@sortablelink('card_id', 'Пасспорт')</th>
                                <th>@sortablelink('status', 'Статус')</th>
                                <th>@sortablelink('created_at', 'Дата создания')</th>
                                <th class="text-right">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    {{$user->id}}
                                </td>
                                <td>
                                    @if($user->user)
                                        <a href="{{ route('admin.user.users.show', $user->user) }}" target="_blank">
                                            {{ $user->user->full_name }}
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    {{$user->first_name}}
                                </td>
                                <td>
                                    {{$user->last_name}}
                                </td>
                                <td>
                                    {{$user->email}}
                                </td>
                                <td>
                                    {{$user->phone}}
                                </td>
                                <td>
                                    {{$user->birthday}}
                                </td>
                                <td>
                                    <a href="{{ $user->getPhotoUrl('card_id', 'user_verifications') }}" class="popup">
                                        <img src="{{ $user->getPhotoUrl('card_id', 'user_verifications') }}" alt="" style="height: 40px;width: auto;">
                                    </a>
                                </td>
                                <td>
                                    <span class="label {{ $user->isWait() ? 'label-warning' : ($user->isReject() ? 'label-danger' : 'label-success') }}">
                                        {{ $user->status_name }}
                                    </span>
                                </td>
                                <td>
                                    {{ $user->created_at }}
                                </td>
                                <td class="text-right">
                                    @if(!$isArchive)
                                        <a href="{{ route('admin.user.user-changes.edit', $user) }}" class="btn btn-info btn-xs" title="Изменить">
                                            <i class="fa fa-check"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $users->appends(request()->input())->links() }}

                </div>
            </div>
        </div>
    </div>

@endsection
