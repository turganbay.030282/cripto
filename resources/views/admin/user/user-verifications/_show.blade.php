<table class="table table-hover jambo_table">
    <tbody>
        <tr>
            <td>ID</td>
            <td>{{$userVerification->id}}</td>
        </tr>
        <tr>
            <td>Фамилия</td>
            <td>{{$userVerification->last_name}}</td>
        </tr>
        <tr>
            <td>Имя</td>
            <td>{{$userVerification->name}}</td>
        </tr>
        <tr>
            <td>Телефон</td>
            <td>{{$userVerification->phone}}</td>
        </tr>
        <tr>
            <td>Дата рождения</td>
            <td>{{$userVerification->birthday}}</td>
        </tr>
        <tr>
            <td>Тип документа</td>
            <td>{{$userVerification->type_doc}}</td>
        </tr>
        <tr>
            <td>Документ</td>
            <td>
                <a href="{{ $userVerification->getPhotoUrl('card_id') }}" class="popup">
                    <img src="{{ $userVerification->getPhotoUrl('card_id') }}" alt="" style="height: 40px;width: auto;">
                </a>
{{--                <br>--}}
{{--                <a href="{{ $userVerification->getPhotoUrl('card_id') }}" download="">--}}
{{--                    <i class="fa fa-download" aria-hidden="true"></i> {{ $userVerification->card_id }}--}}
{{--                </a>--}}
            </td>
        </tr>
        <tr>
            <td>Документ обратная сторона</td>
            <td>
                @if($userVerification->card_id_back)
                    <a href="{{ $userVerification->getPhotoUrl('card_id_back') }}" class="popup">
                        <img src="{{ $userVerification->getPhotoUrl('card_id_back') }}" alt="" style="height: 40px;width: auto;">
                    </a>
                    <br>
                    <a href="{{ $userVerification->getPhotoUrl('card_id_back') }}" download="">
                        <i class="fa fa-download" aria-hidden="true"></i> {{ $userVerification->card_id_back }}
                    </a>
                @endif
            </td>
        </tr>
    </tbody>
</table>
