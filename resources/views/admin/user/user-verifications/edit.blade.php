@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Заявления на Верификацию</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    @include('admin.user.user-verifications._show')

                    {!! Form::open(['url' => route('admin.user.user-verifications.update', $userVerification), 'method' => 'PUT']) !!}
                        @include('admin.user.user-verifications._fields')
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection
