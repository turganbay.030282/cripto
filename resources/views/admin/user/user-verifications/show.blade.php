@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Заявления на Верификацию</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    @include('admin.user.user-verifications._show')

{{--                    <h5>Данные АПИ по верификации</h5>--}}
{{--                    <table class="table table-hover jambo_table">--}}
{{--                        <tbody>--}}
{{--                            @if($userVerification->data_api)--}}
{{--                                @foreach($userVerification->data_api as $key => $detail)--}}
{{--                                    <tr>--}}
{{--                                        <th>{{$key}}</th>--}}
{{--                                        <td>{{json_encode($detail)}}</td>--}}
{{--                                    </tr>--}}
{{--                                @endforeach--}}
{{--                            @endif--}}
{{--                        </tbody>--}}
{{--                    </table>--}}

                </div>
            </div>
        </div>
    </div>

@endsection
