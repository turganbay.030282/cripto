<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Имя</label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{ Form::text('first_name', null, ['class' => 'form-control']) }}
        {{ Form::error('first_name') }}
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Фамилия</label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{ Form::text('last_name', null, ['class' => 'form-control']) }}
        {{ Form::error('last_name') }}
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{ Form::email('email', null, ['class' => 'form-control']) }}
        {{ Form::error('email') }}
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Телефон</label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{ Form::text('phone', null, ['class' => 'form-control']) }}
        {{ Form::error('phone') }}
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Статус</label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{ Form::select('status', $statuses, null, ['class' => 'form-control']) }}
        {{ Form::error('status') }}
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Роль</label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        {{ Form::select('role', $roles, null, ['class' => 'form-control']) }}
        {{ Form::error('role') }}
    </div>
</div>

<hr>

<div class="form-group">
    <div class="control-label col-md-3 col-sm-3 col-xs-12">
        <h5><strong>Банковские счета</strong></h5>
        <a href="{{ route('admin.user.users.addWallet') }}" class="btn btn-success btn-sm" id="render-cat-parts">
            <i class="fa fa-plus-square" aria-hidden="true"></i> счет
        </a>
    </div>
    <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="cats items">
            @if(old('wallet'))
                @foreach(old('wallet') as $key => $serv)
                    @include('admin.user.users._row_wallet', ['key' => $key, 'wallet' => null])
                @endforeach
            @elseif($user && $user->wallets->isNotEmpty())
                @foreach($user->wallets as $key => $wallet)
                    @include('admin.user.users._row_wallet', ['key' => $key, 'wallet' => $wallet])
                @endforeach
            @else
                @include('admin.user.users._row_wallet', ['key' => 0, 'wallet' => null])
            @endif
        </div>
    </div>
</div>

<hr>

{{--<div class="form-group">--}}
{{--    <label class="control-label col-md-3 col-sm-3 col-xs-12">Должность</label>--}}
{{--    <div class="col-md-9 col-sm-9 col-xs-12">--}}
{{--        {{ Form::text('position', null, ['class' => 'form-control']) }}--}}
{{--        {{ Form::error('position') }}--}}
{{--    </div>--}}
{{--</div>--}}

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Аватар</label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="imageWrapper">
            <div class="image_container">
                <div class="portfolio-image portfolio-image-1" data-key="1" style="width: {{$setting->crop_ava_width}}px; height: {{$setting->crop_ava_height}}px;">
                    <div class="chess_underlay" style="width: {{$setting->crop_ava_width}}px; height: {{$setting->crop_ava_height}}px;">
                        <img class="crop-image imgNew" src="{{ $image }}">
                    </div>
                    <label title="Загрузить изображение" for="inputImage1" class="file-btn">
                        {!! Form::file('photo', ['class' => 'hide origin-file', 'id' => 'inputImage1', 'accept' => 'image/*']) !!}
                        Загрузить
                    </label>
                    {!! Form::hidden('crop', 0, ['class' => 'hide crop-file']) !!}
                </div>
                {{ Form::error('photo') }}
            </div>
        </div>
    </div>
</div>

<hr>

@section('script')
    <script>
        $('#render-cat-parts').on('click', function (e) {
            e.preventDefault();
            let action = $(this).attr('href');
            let key = $('.cats .row').last().data('key');
            $.ajax({
                type: "POST",
                url: action,
                data: {key: key},
                success: function (result) {
                    $('.cats').append(result);
                },
                error: function (jqXHR, exception) {
                    console.log(jqXHR, exception);
                },
            });
        });

        $(document).on('click', '.items .remove-row', function (e) {
            e.preventDefault();
            let id = $(this).data('id');
            let row = $(this).closest('.row');
            if (id) {
                let action = $(this).attr('href');
                $.ajax({
                    type: "POST",
                    url: action,
                    data: {id: id},
                    success: function (result) {
                        row.remove();
                    },
                    error: function (jqXHR, exception) {
                        console.log(jqXHR, exception);
                    },
                });
            } else {
                row.remove();
            }
        });
    </script>
@endsection
