@if($wallet)
    <div class="row" data-key="{{ $key }}">
        <div class="col-sm-1" style="padding-top: 5px;">
            <a href="{{ route('admin.user.users.removeWallet', $wallet) }}" class="btn btn-danger btn-xs remove-row"
               data-id="{{ $wallet->id }}">
                <i class="fa fa-minus" aria-hidden="true"></i>
            </a>
            <input type="hidden" name="id[{{ $key }}]" value="{{ $wallet->id }}">
        </div>
        <div class="col-sm-11">
            <div class="form-group">
                {{ Form::text('wallet['.$key.']', $wallet->wallet, ['class' => 'form-control']) }}
                {{ Form::error('wallet.'.$key) }}
            </div>
        </div>
    </div>
@else
    <div class="row" data-key="{{ $key }}">
        <div class="col-sm-1" style="padding-top: 5px;">
            <a href="#" class="btn btn-danger btn-xs remove-row">
                <i class="fa fa-minus" aria-hidden="true"></i>
            </a>
            <input type="hidden" name="id[{{ $key }}]">
        </div>
        <div class="col-sm-11">
            <div class="form-group">
                {{ Form::text('wallet['.$key.']', null, ['class' => 'form-control']) }}
                {{ Form::error('wallet.'.$key) }}
            </div>
        </div>
    </div>
@endif
