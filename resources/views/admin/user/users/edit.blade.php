@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Пользователи: Изменить {{$user->id}}</h3>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    {!! Form::model($user, ['url' => route('admin.user.users.update', $user), 'method' => 'PUT', 'class' => 'form-horizontal form-label-left', 'enctype' => 'multipart/form-data']) !!}

                        @include('admin.user.users._fields', ['image' => $user->getCropPhotoUrl()])

                        <div class="form-group">
                            <label for="type" class="control-label col-md-3 col-sm-3 col-xs-12">Пароль</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <span id="generate-password" class="label label-danger">Сгенерировать пароль</span>
                                <input id="password" class="form-control" type="text" name="password">
                                {{ Form::error('password') }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12 col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </div>
                        </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

    @include('admin.parts._crop', ['width' => $setting->crop_ava_width, 'height' => $setting->crop_ava_height])

@endsection

@section('stylesheet')
    <style>
        #generate-password{
            cursor: pointer;
        }
    </style>
@endsection

@section('script')
    <script>
        $("#generate-password").on("click", function(){
            $("#password").val(password_generator(7));
        });
    </script>
@endsection
