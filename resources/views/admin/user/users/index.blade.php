@extends('admin.layouts.resource-layout')

@section('title')
    <h3>{{ $isArchive ? 'Архив' : '' }} Пользователи</h3>
@endsection

@section('resource-content')

    <style>
        .jambo_table th{
            font-size: 10px;
        }
        .jambo_table.table>tbody>tr>td{
            padding: 4px;
        }
    </style>
    <div class="page-filter clearfix">
        <form action="?" method="GET">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="id" class="col-form-label">ID</label>
                        <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="name" class="col-form-label">Имя</label>
                        <input id="name" class="form-control" name="name" value="{{ request('name') }}">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="status" class="col-form-label">Статус</label>
                        <select id="status" class="form-control" name="status">
                            <option value=""></option>
                            @foreach ($statuses as $value => $label)
                                <option value="{{ $value }}" {{ $value == request('status') ? 'selected' : '' }}>
                                    {{ $label }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="role" class="col-form-label">Роль</label>
                        <select id="role" class="form-control" name="role">
                            <option value=""></option>
                            @foreach ($roles as $value => $label)
                                <option value="{{ $value }}" {{ $value == request('role') ? 'selected' : '' }}>
                                    {{ $label }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="email" class="col-form-label">Email</label>
                        <input id="email" class="form-control" name="email" value="{{ request('email') }}">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group text-right">
                        <label class="col-form-label">&nbsp;</label><br/>
                        <button title="Поиск" type="submit" class="btn bg-orange">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                        <a title="Очистить" href="?" class="btn btn-default">
                            <i class="fa fa-retweet" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="{{route("admin.user.users.create")}}" class="btn btn-success btn-sm">
                                <i class="fa fa-plus"></i> Создать
                            </a>
                        </div>
                        <div class="col-xs-6 text-right">
                            @if($isArchive)
                                <a href="{{route("admin.user.users.index")}}" class="btn btn-info btn-sm">
                                    <i class="fa fa-archive" aria-hidden="true"></i> Активные
                                </a>
                            @else
                                <a href="{{route("admin.user.users.archive")}}" class="btn btn-warning btn-sm">
                                    <i class="fa fa-archive" aria-hidden="true"></i> Архив
                                </a>
                            @endif
                        </div>
                    </div>


                    <table class="table table-hover jambo_table">
                        <thead>
                        <tr>
                            <th>@sortablelink('id', 'ID')</th>
                            <th>@sortablelink('first_name', 'Имя')</th>
                            <th>@sortablelink('last_name', 'Фамилия')</th>
                            <th>@sortablelink('is_ur', 'Юр. лицо')</th>
                            <th>@sortablelink('email', 'Email')</th>
                            <th>@sortablelink('phone', 'Телефон')</th>
                            <th>@sortablelink('balance_trader', 'Баланс трейдер')</th>
                            <th>@sortablelink('balance_investor', 'Баланс инвестор')</th>
                            <th>@sortablelink('role', 'Роль')</th>
                            <th>@sortablelink('status', 'Статус')</th>
                            <th>@sortablelink('created_at', 'Создан')</th>
                            <th>Верификации</th>
                            <th width="50"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    {{$user->id}}
                                </td>
                                <td>
                                    {{$user->first_name}}
                                </td>
                                <td>
                                    {{$user->last_name}}
                                </td>
                                <td>
                                    @if($user->is_ur)
                                        <span class="label label-success">Да</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin.user.users.show', $user) }}">
                                        {{$user->email}}
                                    </a>
                                </td>
                                <td>
                                    {{$user->phone}}
                                </td>
                                <td>
                                    {{$user->balance_trader}}
                                </td>
                                <td>
                                    {{$user->balance_investor}}
                                </td>
                                <td>
                                    <span class="label label-primary">{{ $user->role_name }}</span>
                                </td>
                                <td>
                                    <span class="label label-{{ $user->isBlock() ? 'danger' : 'success' }}">
                                        {{ $user->status_name }}
                                    </span>
                                </td>
                                <td>
                                    {{ $user->created_at }}
                                </td>
                                <td>
                                    @if($user->userVerification)
                                        <a href="/admin/user/user-verifications/{{$user->userVerification->id}}"
                                           class="label label-success">
                                            UV
                                        </a>
                                    @endif
                                    @if($user->creditLevelVerification)
                                        <a href="/admin/user/credit-level-verifications/{{$user->creditLevelVerification->id}}"
                                           class="label label-success">
                                            CLV
                                        </a>
                                    @endif
                                    @if($user->investorRankVerification)
                                        <a href="/admin/user/invest-rank-verifications/{{$user->investorRankVerification->id}}"
                                           class="label label-success">
                                            IRV
                                        </a>
                                    @endif
                                    @if($user->depositLineVerification)
                                        <a href="{{route("admin.user.deposit-line-verifications.archive")}}"
                                           class="label label-success">
                                            DLV
                                        </a>
                                    @endif
                                    {{--
                                        @if($user->creditLineVerification)
                                            <a href="/admin/user/credit-line-verifications/{{$user->creditLineVerification->id}}"
                                                class="label label-success">
                                                CLNV
                                            </a>
                                        @endif
                                        @if($user->creditLevelUrVerification)
                                            <a class="label label-success">CLUV</a>
                                        @endif
                                    --}}
                                </td>
                                <td class="text-right">
                                    @if($isArchive)
                                        {!! Form::open(['url' => route('admin.user.users.restore'), 'style' => 'display: inline-block;vertical-align: top;']) !!}
                                        {{ Form::hidden('id', $user->id) }}
                                        <div class="btn-group">
                                            <button class="btn btn-success btn-xs"><i class="fa fa-reply"
                                                                                      aria-hidden="true"></i></button>
                                        </div>
                                        {!! Form::close() !!}
                                        {!! Form::open(['url' => route('admin.user.users.delete'), 'method' => 'POST', 'style' => 'display: inline-block;vertical-align: top;']) !!}
                                        {{ Form::hidden('id', $user->id) }}
                                        <div class="btn-group">
                                            <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                                        </div>
                                        {!! Form::close() !!}
                                    @else
                                        <a href="{{ route('admin.user.users.show', $user) }}"
                                           class="btn btn-warning btn-xs" title="Просмотр">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('admin.user.users.edit', $user) }}"
                                           class="btn btn-info btn-xs" title="Изменить">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        {!! Form::open(['url' => route('admin.user.users.destroy', $user), 'method' => 'DELETE', 'style' => 'display: inline-block;vertical-align: top;']) !!}
                                        <div class="btn-group">
                                            <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                                        </div>
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $users->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
