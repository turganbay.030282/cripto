@extends('admin.layouts.resource-layout')

@section('title')
    <h3>Пользователи: просмотр {{ $user->id }}</h3>
@endsection

@section('btn')
    <div class="title_right">
        <a href="{{route("admin.user.users.edit", $user)}}" class="btn btn-primary btn-xs">
            <i class="fa fa-pencil"></i> Изменить
        </a>
        {!! Form::open(['url' => route('admin.user.users.destroy', $user), 'method' => 'DELETE', 'style' => 'display: inline-block;vertical-align: top;']) !!}
            <div class="btn-group">
                <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Удалить</button>
            </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('resource-content')

    <div class="row">
        <div class="col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $user->id }}</td>
                            </tr>
                            <tr>
                                <th>Имя</th><td>{{ $user->first_name }}</td>
                            </tr>
                            <tr>
                                <th>Фамилия</th><td>{{ $user->last_name }}</td>
                            </tr>
                            <tr>
                                <th>Email</th><td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <th>Телефон</th><td>{{ $user->phone }}</td>
                            </tr>
                            <tr>
                                <th>Статус</th>
                                <td>
                                    <span class="label label-success">{{ $user->status_name }}</span>
                                </td>
                            </tr>
                            <tr>
                                <th>Роль</th>
                                <td>
                                    <span class="label label-primary">{{ $user->role_name }}</span>
                                </td>
                            </tr>
                            <tr>
                                <th>Банковские счета</th>
                                <td>{{ $user->wallets_name }}</td>
                            </tr>
{{--                            <tr>--}}
{{--                                <th>Должность</th>--}}
{{--                                <td>{{ $user->position }}</td>--}}
{{--                            </tr>--}}
                            <tr>
                                <th>Аватар</th>
                                <td>
                                    <a href="{{ $user->getPhotoUrl() }}" class="popup">
                                        <img src="{{ $user->getCropPhotoUrl() }}" alt="" style="height: 40px;width: auto;">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <th>Дата создания</th><td>{{ $user->created_at->format('d.m.Y H:i:s') }}</td>
                            </tr>
                            <tr>
                                <th>Дата изменения</th><td>{{ $user->updated_at->format('d.m.Y H:i:s') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
