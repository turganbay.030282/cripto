<!DOCTYPE html>
<html lang="ru">
<head>
    @include('front.layouts.parts._head')
    <title>@lang('site.header.login') - Cripto</title>
</head>
<body>
@include('front.layouts.parts._header_auth')
<div class="wrapp Login">
    <div class="Content">
        <div class="container">
            @include("front.layouts.parts._flash")
            <div class="LoginBox">
                <h1>@lang('site.header.login')</h1>
                {!! Form::open(['url' => route('2faVerify')]) !!}
                <div class="form-group">
                    {!! Form::text('verify_code', null, ['placeholder' => trans('site.cabinet.2gaf-placeholder-verify-code')]) !!}
                    {{ Form::error('verify_code') }}
                </div>
                <div class="form-group text-center">
                    {!! Form::submit('Login', ['class' => 'btns-orange']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@include('front.layouts.parts._scripts')
</body>
</html>
