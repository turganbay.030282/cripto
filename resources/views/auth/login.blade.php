<!DOCTYPE html>
<html lang="ru">
<head>
    @include('front.layouts.parts._head')
    <title>@lang('site.header.login') - {{ env('APP_NAME') }}</title>
</head>
<body>
@include('front.layouts.parts._header_auth')
<div class="wrapp Login">
    <div class="Content">
        <div class="container">
            @include("front.layouts.parts._flash")
            <div class="LoginBox">
                <h1>@lang('site.header.login')</h1>
                {!! Form::open(['url' => route('login')]) !!}
                    <div class="form-group">
                        {!! Form::text('email', null, ['placeholder' => trans('site.page.register-Email-Address')]) !!}
                        {{ Form::error('email') }}
                    </div>
                    <div class="form-group">
                        {!! Form::password('password', ['placeholder' => trans('site.page.register-Password')]) !!}
                        {{ Form::error('password') }}
                    </div>
                    {!! NoCaptcha::display() !!}
                    {{ Form::error('g-recaptcha-response') }}
                    <div class="form-group text-center">
                        {!! Form::submit(trans('site.page.login-login'), ['class' => 'btns-orange']) !!}
                    </div>
                    <div class="form-group LinkGroup">
                        <a href="{{ route('password.request') }}">@lang('site.page.login-forgot')</a>
                        <a href="{{ route('register') }}">@lang('site.page.login-dont-have-account')</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
{!! NoCaptcha::renderJs() !!}
@include('front.layouts.parts._scripts')
</body>
</html>
