<!DOCTYPE html>
<html lang="ru">
<head>
    @include('front.layouts.parts._head')
    <title>Reset - Cripto</title>
</head>
<body>
@include('front.layouts.parts._header_auth')
<div class="wrapp Login">
    <div class="Content">
        <div class="container">
            @include("front.layouts.parts._flash")
            <div class="LoginBox text-center">
                <h1>Reset your password</h1>
                <h5>Enter your username, or the email that you used to register</h5>
                {!! Form::open(['url' => route('password.email')]) !!}
                <div class="form-group">
                    {!! Form::text('email', null, ['placeholder' => 'Email Address']) !!}
                    {{ Form::error('email') }}
                </div>
                <div class="form-group text-center">
                    {!! Form::submit('Reset', ['class' => 'btns-orange']) !!}
                </div>
                <div class="form-group LinkGroup">
                    <a href="{{ route('register') }}">Dont have account?</a>
                    <a href="{{ route('login') }}">@lang('site.header.login')</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@include('front.layouts.parts._scripts')
</body>
</html>
