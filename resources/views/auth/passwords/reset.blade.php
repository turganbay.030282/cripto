<!DOCTYPE html>
<html lang="ru">
<head>
    @include('front.layouts.parts._head')
    <title>Reset - Cripto</title>
</head>
<body>
@include('front.layouts.parts._header_auth')
<div class="wrapp Login">
    <div class="Content">
        <div class="container">
            @include("front.layouts.parts._flash")
            <div class="LoginBox text-center">
                <h1>Reset your password</h1>
                <h5>Enter your username, or the email that you used to register</h5>
                {!! Form::open(['url' => route('password.update')]) !!}
                <div class="form-group">
                    {!! Form::hidden('email', $email) !!}
                    {!! Form::hidden('token', $token) !!}
                    {{ Form::error('token   ') }}
                    {{ Form::password('password', ['placeholder' => 'Password']) }}
                    {{ Form::error('password') }}
                </div>
                <div class="form-group">
                    {{ Form::password('password_confirmation', ['placeholder' => 'Confirm Password']) }}
                </div>
                <div class="form-group text-center">
                    {!! Form::submit('Reset Password', ['class' => 'btns-orange']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@include('front.layouts.parts._scripts')
</body>
</html>
