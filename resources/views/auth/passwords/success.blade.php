<!DOCTYPE html>
<html lang="ru">
<head>
    @include('front.layouts.parts._head')
    <title>Reset - Cripto</title>
</head>
<body>
@include('front.layouts.parts._header_auth')
<div class="wrapp Login">
    <div class="Content">
        <div class="container">
            <div class="LoginBox text-center">
                <h1>Your password has been successfully changed!</h1>
                <div class="form-group text-center">
                    <a href="{{ route('front.home') }}" class="btns-orange">Home page</a>
                </div>
            </div>
        </div>
    </div>
</div>
@include('front.layouts.parts._scripts')
</body>
</html>
