<!DOCTYPE html>
<html lang="ru">
<head>
    @include('front.layouts.parts._head')
    <link rel="stylesheet" href="/js/intl-tel-input/css/intlTelInput.css">
    <title>@lang('site.page.register-title') - {{ env('APP_NAME') }}</title>
</head>
<body>
@include('front.layouts.parts._header_auth')
<div class="wrapp Login">
    <div class="Content">
        <div class="container">
            @include("front.layouts.parts._flash")
            <div class="LoginBox">
                <h1>@lang('site.page.register-Sign-Up')</h1>
                {!! Form::open(['url' => route('register')]) !!}
                    <div class="form-group">
                        {{ Form::text('first_name', null, ['placeholder' => trans('site.page.register-First-Name'), 'required' => true]) }}
                        {{ Form::error('first_name') }}
                    </div>
                    <div class="form-group">
                        {{ Form::text('last_name', null, ['placeholder' => trans('site.page.register-Last-Name'), 'required' => true]) }}
                        {{ Form::error('last_name') }}
                    </div>
                    <div class="form-group">
                        {{ Form::text('email', null, ['placeholder' => trans('site.page.register-Email-Address'), 'required' => true]) }}
                        {{ Form::error('email') }}
                    </div>
                    <div class="form-group">
                        {{ Form::password('password', ['placeholder' => trans('site.page.register-Password'), 'required' => true]) }}
                        {{ Form::error('password') }}
                    </div>
                    <div class="form-group">
                        {{ Form::password('password_confirmation', ['placeholder' => trans('site.page.register-Confirm-Password'), 'required' => true]) }}
                    </div>
                    <div class="form-group">
                        {{ Form::text('full_phone', null, ['id' => 'full_phone', 'required' => true]) }}
                        {{ Form::hidden('phone', null, ['id' => 'reg-phone']) }}
                        <span id="error-msg" class="invalid-feedback" style="color: red;display: none;"><strong>@lang('validation.regex', ['attribute' => 'phone'])</strong></span>
                        {{ Form::error('phone') }}
                    </div>
                    <div class="form-group text-center">
                        <button class="btns-green send-sms" type="button">@lang('site.page.register.send-sms')</button>
                    </div>
                    <div class="form-group form-group-verify_code" style="{{ $errors->first('verify_token') ? '' : 'display: none;' }}">
                        {{ Form::text('verify_token', null, ['placeholder' => trans('site.page.cabinet.Your-verification-code'), 'required' => true]) }}
                        {!! Form::error('verify_token') !!}
                    </div>
                    <div class="form-group" style="display: none;">
                        <div class="row">
                            <div class="col-6">
                                <label class="PositLab">
                                    {!! Form::radio('is_investor', 0, true, ['class' => 'Myradiobox']) !!}
                                    <div class="LeftLab">@lang('site.page.register-trader')</div>
                                </label>
                            </div>
                            <div class="col-6">
                                <label class="PositLab">
                                    {!! Form::radio('is_investor', 1, null, ['class' => 'Myradiobox']) !!}
                                    <div class="LeftLab">@lang('site.page.register-investor')</div>
                                </label>
                            </div>
                        </div>
                        {{ Form::error('is_investor') }}
                    </div>
                    <div class="form-group">
                        <label class="PositLab">
                            {!! Form::checkbox('is_ur', 1, null, ['class' => 'Mycheckbox']) !!}
                            <div class="LeftLab">@lang('site.page.register-is_ur')</div>
                        </label>
                        {!! Form::error('is_ur') !!}
                    </div>
                    <div class="form-group">
                        <label class="PositLab">
                            {!! Form::checkbox('politics', 1, null, ['class' => 'Mycheckbox', 'required' => true]) !!}
                            <div class="LeftLab">
                                @if($filePolitic)
                                    <a href="{{ $filePolitic }}" target="_blank">@lang('site.page.register-rules')</a>
                                @else
                                    @lang('site.page.register-rules')
                                @endif
                            </div>
                            {{ Form::error('politics') }}
                        </label>
                        <label class="PositLab">
                            {!! Form::checkbox('age', 1, null, ['class' => 'Mycheckbox', 'required' => true]) !!}
                            <div class="LeftLab">@lang('site.page.register-18')</div>
                            {{ Form::error('age') }}
                        </label>
                    </div>
                    {!! NoCaptcha::display() !!}
                    {{ Form::error('g-recaptcha-response') }}
                    <div class="form-group text-center">
                        {!! Form::submit(trans('site.page.register-Sign-Up'), ['class' => 'btns-orange']) !!}
                    </div>
                    <div class="form-group LinkGroup">
                        @lang('site.page.register-have-account') <a href="{{ route('login') }}">@lang('site.header.login')</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
{!! NoCaptcha::renderJs() !!}
@include('front.layouts.parts._scripts')
<script src="/js/intl-tel-input/js/intlTelInput-jquery.js"></script>
<script>
    $(document).ready(function () {
        let telInput = $("#full_phone")
        telInput.intlTelInput({
            initialCountry: "auto",
            separateDialCode:true,
            autoHideDialCode:false,
            preferredCountries: ['ee'],
            excludeCountries: ['kp', 'ir', 'af', 'bs', 'bb', 'bw', 'kh', 'gh', 'iq',
                'jm', 'mu', 'mm', 'ni', 'pk', 'pa', 'sy', 'tt', 'ug', 'vu', 'ye', 'zw',
                'al', 'is', 'mn'],
            geoIpLookup: function(success, failure) {
                $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "ee";
                    success(countryCode);
                });
            },
            utilsScript: "/js/intl-tel-input/js/utils.js"
        });
        telInput.blur(function () {
            if ($.trim(telInput.val())) {
                if (telInput.intlTelInput('isValidNumber')) {
                    $('#error-msg').hide();
                    $('#reg-phone').val(telInput.intlTelInput('getNumber'));
                } else {
                    $('#error-msg').show();
                }
            }
        });
        telInput.keydown(function () {
            $('#error-msg').hide();
        });

        $('.send-sms').on('click', function(e){
            e.preventDefault();
            if (!telInput.intlTelInput('isValidNumber')) {
                $('#error-msg').show();
                return;
            }

            $.ajax({
                type: "POST",
                url: '{{ (route('register.sendVerifyCode')) }}',
                data: {phone: telInput.intlTelInput('getNumber')},
                success: function (result) {
                        if (result.hasOwnProperty('errors')) {
                            sendMessage(result.errors.phone);
                            return;
                        }
                    if (result.hasOwnProperty('error')) {
                        sendMessage(result.error);
                        return;
                    }
                    $('.form-group-verify_code').show();
                    $('.send-sms').closest('.form-group').hide();
                },
                error: function(data) {
                    if(data.status == 422){
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors['errors'], function(index, value) {
                            sendMessage(value[0]);
                        });
                    }
                    console.log(data);
                }
            });
        });
    });
</script>
</body>
</html>
