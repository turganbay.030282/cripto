<!DOCTYPE html>
<html lang="ru">
<head>
    @include('front.layouts.parts._head')
    <title>@lang('site.page.register-thanks') - {{ env('APP_NAME') }}</title>
</head>
<body>
@include('front.layouts.parts._header_auth')
<div class="wrapp Login">
    <div class="Content">
        <div class="container">
            @include("front.layouts.parts._flash")
            <div class="LoginBox">
                <div class="LoginBox text-center">
                    <h1>@lang('site.page.register-thanks')</h1>
                    <h6>@lang('site.page.register-send-email')</h6>
                    <h6>{{ $email }}</h6>
                    <h6>@lang('site.page.register-link-activation')</h6>
                    {!! Form::open(['url' => route('register.resend')]) !!}
                        {!! Form::hidden('email', $email) !!}
                        <div class="form-group text-center">
                            {!! Form::submit(trans('site.page.register-send-repeat'), ['class' => 'btns-orange']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@include('front.layouts.parts._scripts')
<script>
    $(document).ready(function () {
        setInterval(function () {
            $.ajax({
                type: "GET",
                url: '{{ route('register.verifyEmail', $email) }}',
                success: function (result) {
                    if (result.hasOwnProperty('location')) {
                        location.href = result.location;
                    }
                }
            });
        }, 10000);
    });
</script>
</body>
</html>
