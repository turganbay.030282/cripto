@if(auth()->user()->google2fa_enable)
    @if(isset($class))
        <div class="row">
            <div class="{{$class}}">
                <div class="LeftFormText d-flex align-items-center">
                    <h6>@lang('site.page.cabinet.Your-2FA-code')</h6>
                </div>
            </div>
            <div class="{{$class}}">
                <div class="form-group">
                    {!! Form::text('verify_code', null, ['class' => 'verify_code', 'placeholder' => '6 -' . __('site.page.cabinet.Your-2FA-code')]) !!}
                    <div class="FZ16">
                        {!! Form::error('verify_code') !!}
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="col-xl-4 col-4">
            <div class="LeftFormText d-flex align-items-center">
                <h6>@lang('site.page.cabinet.Your-2FA-code')</h6>
            </div>
        </div>
        <div class="col-xl-8 col-8">
            <div class="form-group">
                {!! Form::text('verify_code', null, ['class' => 'verify_code', 'placeholder' => '6 -' . __('site.page.cabinet.Your-2FA-code')]) !!}
                <div class="FZ16">
                    {!! Form::error('verify_code') !!}
                </div>
            </div>
        </div>
    @endif
@endif

