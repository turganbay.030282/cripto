@if ($errors->has($name))
    <span class="invalid-feedback" style="color: red;display: block;"><strong>{{ $errors->first($name) }}</strong></span>
@endif
