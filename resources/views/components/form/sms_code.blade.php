<div class="col-12 text-center">
    <button class="btns-green get-sms-code" data-url="{{$url}}">
        @lang('site.page.cabinet.Subscribe')
    </button>
</div>
<div class="col-12 mt-3">
    <h5>@lang('site.page.cabinet.Check-your-mobile-device') {{ auth()->user()->phone }}</h5>
</div>
<div class="col-xl-4 col-4">
    <div class="LeftFormText d-flex align-items-center">
        <h6>@lang('site.page.cabinet.Your-verification-code')</h6>
    </div>
</div>
<div class="col-xl-8 col-8">
    <div class="form-group">
        <input type="text" placeholder="7 - @lang('site.page.cabinet.digit-code')" name="sms_code" class="sms_code" required>
        {!! Form::error('sms_code') !!}
    </div>
</div>
