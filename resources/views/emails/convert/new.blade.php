<table cellpadding="5" border="1" width="100%">
    <tbody>
        <tr>
            <td width="200">ID</td>
            <td>{{ $convert->id }}</td>
        </tr>    
        <tr>
            <td>Тип конвертации</td>
            <td>{{ $convert->type }}</td>
        </tr>    
        <tr>
            <td>Дата создания</td>
            <td>{{ $convert->created_at }}</td>
        </tr>    
        <tr>
            <td>Пользователь</td>
            <td>
                @if($convert->client)
                    <a href="{{ route('admin.user.users.show', $convert->client) }}" target="_blank">
                        {{ $convert->client->full_name }}
                    </a>
                @endif
            </td>
        </tr>    
        <tr>
            <td>Сумма EUR</td>
            <td>{{ format_number($convert->amount) }}</td>
        </tr>    
        <tr>
            <td>Вид монеты</td>
            <td>{{ $convert->typeAsset->name }}</td>
        </tr>    
        <tr>
            <td>Кол-во монет</td>
            <td>{{ numeric_fmt($convert->amount_asset) }}</td>
        </tr>    
    </tbody>
</table>
