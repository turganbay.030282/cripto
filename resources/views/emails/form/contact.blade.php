<table style="width: 100%">
    <tbody>
        <tr>
            <th style="width: 30%">Имя</th>
            <td style="width: 70%">{{ $form->name }}</td>
        </tr>
        <tr>
            <th style="width: 30%">Телефон</th>
            <td style="width: 70%">{{ $form->phone }}</td>
        </tr>
        <tr>
            <th style="width: 30%">Email</th>
            <td style="width: 70%">{{ $form->email }}</td>
        </tr>
        <tr>
            <th style="width: 30%">Компания</th>
            <td style="width: 70%">{{ $form->company }}</td>
        </tr>
        <tr>
            <th style="width: 30%">Сообщение</th>
            <td style="width: 70%">{{ $form->message }}</td>
        </tr>
        <tr>
            <th style="width: 30%">Дата создания</th>
            <td style="width: 70%">{{ $form->created_at->format("d.m.Y H:i:s") }}</td>
        </tr>
    </tbody>
</table>
