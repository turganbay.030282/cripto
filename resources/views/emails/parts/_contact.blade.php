<!-- Contact -->
<layout label='Contact'>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
        <tr>
            <td valign="top" align="center" class="p30-15" style="padding: 30px 0px 30px 0px;">
                <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                    <tr>
                        <td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="h3 center pb15" style="color:#000000; font-family:'Lato', Arial ,sans-serif; font-size:24px; line-height:32px; font-weight:bold; text-align:center; padding-bottom:15px;"><multiline>Контактная информация</multiline></td>
                                </tr>
                                <tr>
                                    <td align="center" style="padding-bottom: 30px;">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
{{--                                                <th class="column-top" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">--}}
{{--                                                    <table class="center" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">--}}
{{--                                                        <tr>--}}
{{--                                                            <td class="img" width="50" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="{{ env('APP_URL') }}/email/images/ph.png" width="34" height="34" editable="true" border="0" alt="" /></td>--}}
{{--                                                            <td class="text" style="color:#777777; font-family:'Lato', Arial,sans-serif; font-size:16px; line-height:30px; text-align:left;"><multiline><a href="tel:+371 765 867 87" target="_blank" class="link" style="color:#777777; text-decoration:none;"><strong class="link" style="color:#777777; text-decoration:none;">+371 765 867 87</strong></a></multiline></td>--}}
{{--                                                        </tr>--}}
{{--                                                    </table>--}}
{{--                                                </th>--}}
                                                <th class="column-empty2" width="30" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;"></th>
                                                <th class="column-top" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                                    <table class="center" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
                                                        <tr>
                                                            <td class="img" width="50" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="{{ env('APP_URL') }}/email/images/em.png" width="34" height="34" editable="true" border="0" alt="" /></td>
                                                            <td class="text" style="color:#777777; font-family:'Lato', Arial,sans-serif; font-size:16px; line-height:30px; text-align:left;"><multiline><a href="support@assetscore.io" target="_blank" class="link" style="color:#777777; text-decoration:none;"><strong class="link" style="color:#777777; text-decoration:none;">support@assetscore.io</strong></a></multiline></td>
                                                        </tr>
                                                    </table>
                                                </th>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
{{--                                <tr>--}}
{{--                                    <td>--}}
{{--                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" dir="rtl" style="direction: rtl;">--}}
{{--                                            <tr>--}}
{{--                                                <th class="column"  dir="ltr" width="433" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">--}}
{{--                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">--}}
{{--                                                    </table>--}}
{{--                                                </th>--}}
{{--                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">--}}
{{--                                                    <tr>--}}
{{--                                                        <td class="h4 pb15 m-center" style="color:#000000; font-family:'Lato', Arial ,sans-serif; font-size:20px; line-height:28px; text-align:center; font-weight:bold; padding-bottom:15px;"><multiline>Наш адрес</multiline></td>--}}
{{--                                                    </tr>--}}
{{--                                                    <tr>--}}
{{--                                                        <td class="text pb20 m-center" style="color:#777777; font-family:'Lato', Arial,sans-serif; font-size:16px; line-height:30px; text-align:center; padding-bottom:10px;"><multiline>{{ trans_field($setting, 'req_reg') }}</multiline></td>--}}
{{--                                                    </tr>--}}
{{--                                                </table>--}}
{{--                                            </tr>--}}
{{--                                        </table>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</layout>
<!-- END Contact -->
