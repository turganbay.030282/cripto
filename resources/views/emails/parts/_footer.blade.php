<!-- Footer -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
    <tr>
        <td valign="top" align="center" class="p30-15" style="padding: 50px 0px 50px 0px;">
            <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                <tr>
                    <td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="padding-bottom: 30px;">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="img" width="55" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="#" target="_blank"><img src="{{ env('APP_URL') }}/email/images/t7_ico_facebook.jpg" width="34" height="34" editable="true" border="0" alt="" /></a></td>
                                            <td class="img" width="55" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="#" target="_blank"><img src="{{ env('APP_URL') }}/email/images/t7_ico_twitter.jpg" width="34" height="34" editable="true" border="0" alt="" /></a></td>
                                            <td class="img" width="55" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="#" target="_blank"><img src="{{ env('APP_URL') }}/email/images/t7_ico_instagram.jpg" width="34" height="34" editable="true" border="0" alt="" /></a></td>
                                            <td class="img" width="34" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="#" target="_blank"><img src="{{ env('APP_URL') }}/email/images/t7_ico_linkedin.jpg" width="34" height="34" editable="true" border="0" alt="" /></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-footer2" style="color:#999999; font-family:'Lato', Arial,sans-serif; font-size:12px; line-height:26px; text-align:center;">
                                    <unsubscribe class="link-grey" style="color:#999999; text-decoration:none;"><span class="link-grey-u" style="color:#999999; text-decoration:underline;">Unsubscribe</span> from this mailing list.</unsubscribe>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END Footer -->
