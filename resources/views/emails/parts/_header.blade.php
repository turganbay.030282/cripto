<!-- Header -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center" class="p30-15" style="padding-top: 20px; padding-bottom: 28px;">
            <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                <tr>
                    <td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                        <!-- Header -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th class="column" width="145" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="img m-center" style="font-size:0pt; line-height:0pt; text-align:center;"><img src="{{ env('APP_URL') }}/email/images/logo.png" width="186" height="41" editable="true" border="0" alt="" /></td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                        </table>
                        <!-- END Header -->
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END Header -->
