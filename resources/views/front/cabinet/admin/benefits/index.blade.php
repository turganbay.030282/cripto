@extends('front.layouts.admin')

@section('content')
    <h1 class="Title text-left MarginBot15">Активы</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mollis mollis urna, sit amet porta tellus blanditatQuisque est risus, dignissim vitae quam vitae, tempor ultrices urna.</p>
    <div class="MarginTop30 MarginBot40">
        <span class="Bold">Добавить актив</span>
        <a href="#" class="btns-orange ADDActivBTN"><i class="fas fa-plus"></i> Добавить</a>
    </div>
    <div class="AddActiv ListActive">
        <h3>Добавить новый актив</h3>
        <div class="ListAmin">
            <div class="row">
                <div class="col-md-6 col-12 d-flex align-items-center">
                    <div class="Text1">Тип актива:</div>
                </div>
                <div class="col-md-6 col-12">
                    <select>
                        <option>Выберите актив</option>
                        <option>актив 1</option>
                        <option>актив 2</option>
                        <option>актив 3</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="ListAmin">
            <div class="row">
                <div class="col-md-6 col-12 d-flex align-items-center">
                    <div class="Text1">@lang('site.page.cabinet.Asset-name'):</div>
                </div>
                <div class="col-md-6 col-12">
                    <input type="text">
                </div>
            </div>
        </div>
        <div class="ListAmin">
            <div class="row">
                <div class="col-md-6 col-12 d-flex align-items-center">
                    <div class="Text1">Параметры актива:</div>
                </div>
                <div class="col-md-6 col-12">
                    <input type="text">
                </div>
            </div>
        </div>
        <div class="ListAmin">
            <div class="row">
                <div class="col-md-6 col-12 d-flex align-items-center">
                    <div class="Text1">Настройка загрузки данных:</div>
                </div>
                <div class="col-md-6 col-12">
                    <select>
                        <option>XML</option>
                        <option>XML</option>
                        <option>XML</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="ListAmin">
            <div class="row">
                <div class="col-md-6 col-12 d-flex align-items-center">
                    <div class="Text1">Адрес обращения:</div>
                </div>
                <div class="col-md-6 col-12">
                    <input type="text">
                </div>
            </div>
        </div>
        <div class="ListAmin">
            <div class="row">
                <div class="col-md-6 col-12 d-flex align-items-center">
                </div>
                <div class="col-md-6 col-12">
                    <button class="btns-green">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
    <h2 class="TopLineSilver Bold">Натуральные активы</h2>
    <div class="row">
        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="ListActive">
                <h3>@lang('site.page.cabinet.Asset') 1</h3>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">@lang('site.page.cabinet.Asset-name'):</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">Ripple one</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">Параметры актива:</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">XRP</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">Настройка загрузки данных:</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">JSON</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-green">Скрыть актив</button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-red">удалить актив</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="ListActive">
                <h3>@lang('site.page.cabinet.Asset') 1</h3>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">@lang('site.page.cabinet.Asset-name'):</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">Ripple one</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">Параметры актива:</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">XRP</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">Настройка загрузки данных:</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">JSON</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-green">Скрыть актив</button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-red">удалить актив</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="ListActive">
                <h3>@lang('site.page.cabinet.Asset') 1</h3>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">@lang('site.page.cabinet.Asset-name'):</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">Ripple one</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">Параметры актива:</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">XRP</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">Настройка загрузки данных:</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">JSON</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-green">Скрыть актив</button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-red">удалить актив</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h2 class="Bold">Виртуальные Активы</h2>
    <div class="row">
        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="ListActive">
                <h3>@lang('site.page.cabinet.Asset') 1</h3>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">@lang('site.page.cabinet.Asset-name'):</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">Ripple one</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">Параметры актива:</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">XRP</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">Настройка загрузки данных:</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">JSON</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-green">Скрыть актив</button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-red">удалить актив</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="ListActive">
                <h3>@lang('site.page.cabinet.Asset') 1</h3>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">@lang('site.page.cabinet.Asset-name'):</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">Ripple one</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">Параметры актива:</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">XRP</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">Настройка загрузки данных:</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">JSON</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-green">Скрыть актив</button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-red">удалить актив</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="ListActive">
                <h3>@lang('site.page.cabinet.Asset') 1</h3>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">@lang('site.page.cabinet.Asset-name'):</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">Ripple one</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">Параметры актива:</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">XRP</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-5 col-12 d-flex align-items-center">
                            <div class="Text1">Настройка загрузки данных:</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center">
                            <div class="Text2">JSON</div>
                        </div>
                        <div class="col-md-4 col-12 d-flex align-items-center">
                            <a href="#" class="Underline">Редактировать</a>
                        </div>
                    </div>
                </div>
                <div class="ListAmin">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-green">Скрыть актив</button>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-red">удалить актив</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
@endsection
