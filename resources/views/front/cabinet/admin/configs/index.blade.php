@extends('front.layouts.admin')

@section('content')
    <h1 class="Title text-left MarginBot15">Конфигурация</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mollis mollis urna, sit amet porta tellus blandit at. Quisque est risus, dignissim vitae quam vitae, tempor ultrices urna.</p>
    <div class="ConfigList">
        <div class="row no-gutters">
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="BoxLeftRightC clear">
                    <div class="LeftC">
                        <div class="Bold">Перейти:</div>
                    </div>
                    <div class="RightC">
                        <a href="admin-config-1.html" class="btns-orange">Глобальная конфигурация</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="BoxLeftRightC clear">
                    <div class="LeftC">
                        <div class="Bold">Поиск Пользователя:</div>
                    </div>
                    <div class="RightC">
                        <select>
                            <option>Найти пользователя</option>
                            <option>Системные</option>
                            <option>Трейдеры</option>
                            <option>Инвесторы</option>
                            <option>Поддержка</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="BoxLeftRightC clear text-right">
                    <div class="LeftC DisplayInline">
                        <div class="Bold">Перейти:</div>
                    </div>
                    <div class="RightC FloatRight">
                        <a href="admin-users.html" class="btns-orange">Таблица Пользователей</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ConfigList">
        <h2 class="Bold">Комиссия</h2>
        <div class="row">
            <div class="col-md-5 col-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th class="text-center">Комиссия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Расторжение договора Трейдера</td>
                            <td class="text-center">3,5%</td>
                        </tr>
                        <tr>
                            <td>Расторжение договора Инвестора</td>
                            <td class="text-center">5%</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ConfigList">
        <h2 class="Bold">Секция Кредиты</h2>
        <p class="Bold">Натуральные Активы</p>
        <div class="table-responsive">
            <table class="table table-striped table-bordered text-center TableConfig">
                <thead>
                <tr>
                    <th>@lang('site.page.cabinet.Asset-name')</th>
                    <th>Бронзовый Ранг</th>
                    <th>Серебряный Ранг</th>
                    <th>Золотой Ранг</th>
                    <th>(Предусмотреть<br>добавление в будущем)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>XAU Gold</td>
                    <td><input type="text" value="7%"></td>
                    <td><input type="text" value="8%"></td>
                    <td><input type="text" value="9%"></td>
                    <td><input type="text" value="X"></td>
                </tr>
                <tr>
                    <td>XAU Gold</td>
                    <td><input type="text" value="7%"></td>
                    <td><input type="text" value="8%"></td>
                    <td><input type="text" value="9%"></td>
                    <td><input type="text" value="X"></td>
                </tr>
                <tr>
                    <td>X</td>
                    <td><input type="text" value="X"></td>
                    <td><input type="text" value="X"></td>
                    <td><input type="text" value="X"></td>
                    <td><input type="text" value="X"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="ConfigList">
        <p class="Bold">Виртуальные Активы</p>
        <div class="table-responsive">
            <table class="table table-striped table-bordered text-center TableConfig">
                <thead>
                <tr>
                    <th>@lang('site.page.cabinet.Asset-name')</th>
                    <th>Бронзовый Ранг</th>
                    <th>Серебряный Ранг</th>
                    <th>Золотой Ранг</th>
                    <th>(Предусмотреть<br>добавление в будущем)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>XAU Gold</td>
                    <td><input type="text" value="7%"></td>
                    <td><input type="text" value="8%"></td>
                    <td><input type="text" value="9%"></td>
                    <td><input type="text" value="X"></td>
                </tr>
                <tr>
                    <td>XAU Gold</td>
                    <td><input type="text" value="7%"></td>
                    <td><input type="text" value="8%"></td>
                    <td><input type="text" value="9%"></td>
                    <td><input type="text" value="X"></td>
                </tr>
                <tr>
                    <td>X</td>
                    <td><input type="text" value="X"></td>
                    <td><input type="text" value="X"></td>
                    <td><input type="text" value="X"></td>
                    <td><input type="text" value="X"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="ConfigList">
        <p class="Bold">Добавление Кредитного Уровня в Систему</p>
        <div class="row">
            <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                <div class="ListActive">
                    <h3>Добавить новый Кредитный Уровень</h3>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">ID:Название:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <input type="text">
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Тип кредитного уровня:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <select>
                                    <option>Выберите тип</option>
                                    <option>актив 1</option>
                                    <option>актив 2</option>
                                    <option>актив 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Параметры:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <input type="text">
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Тип актива:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <select>
                                    <option>Выберите тип</option>
                                    <option>актив 1</option>
                                    <option>актив 2</option>
                                    <option>актив 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Диапозон сроков:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <input type="text">
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                            </div>
                            <div class="col-md-6 col-12">
                                <label class="BoxLab d-flex align-items-center">
                                    <input type="radio" name="gr1">
                                    <span>Активен</span>
                                </label>
                                <label class="BoxLab d-flex align-items-center">
                                    <input type="radio" name="gr1">
                                    <span>Скрыть</span>
                                </label>
                                <button class="btns-green">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                <div class="ListActive">
                    <h3>Удалить ранг инвестора</h3>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Выбрать из списка:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <select>
                                    <option>Выбрать</option>
                                    <option>актив 1</option>
                                    <option>актив 2</option>
                                    <option>актив 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Код страны:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <input type="text" placeholder="Латвия (+371)">
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Номер телефона:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="BoxNumbPos">
                                    <input class="Pos1" type="text" value="+371">
                                    <input class="Pos2" type="text" placeholder="22187783">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="Text1">Введите семизначный код</div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Ввести номер:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <input type="text">
                            </div>
                        </div>
                        <div class="text-right MarginTop15">
                            Не получили СМС?  <a href="#">Отправить еще одно?</a>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="text-center">
                            <button class="btns-green">Выслать SMS</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ConfigList">
        <h2 class="Bold">Секция Депозита</h2>
        <p class="Bold">Натуральные Активы</p>
        <div class="table-responsive">
            <table class="table table-striped table-bordered text-center TableConfig">
                <thead>
                <tr>
                    <th>@lang('site.page.cabinet.Asset-name')</th>
                    <th>Бронзовый Ранг</th>
                    <th>Серебряный Ранг</th>
                    <th>Золотой Ранг</th>
                    <th>(Предусмотреть<br>добавление в будущем)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>XAU Gold</td>
                    <td><input type="text" value="7%"></td>
                    <td><input type="text" value="8%"></td>
                    <td><input type="text" value="9%"></td>
                    <td><input type="text" value="X"></td>
                </tr>
                <tr>
                    <td>XAU Gold</td>
                    <td><input type="text" value="7%"></td>
                    <td><input type="text" value="8%"></td>
                    <td><input type="text" value="9%"></td>
                    <td><input type="text" value="X"></td>
                </tr>
                <tr>
                    <td>X</td>
                    <td><input type="text" value="X"></td>
                    <td><input type="text" value="X"></td>
                    <td><input type="text" value="X"></td>
                    <td><input type="text" value="X"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="ConfigList">
        <p class="Bold">Виртуальные Активы</p>
        <div class="table-responsive">
            <table class="table table-striped table-bordered text-center TableConfig">
                <thead>
                <tr>
                    <th>@lang('site.page.cabinet.Asset-name')</th>
                    <th>Бронзовый Ранг</th>
                    <th>Серебряный Ранг</th>
                    <th>Золотой Ранг</th>
                    <th>(Предусмотреть<br>добавление в будущем)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>XAU Gold</td>
                    <td><input type="text" value="7%"></td>
                    <td><input type="text" value="8%"></td>
                    <td><input type="text" value="9%"></td>
                    <td><input type="text" value="X"></td>
                </tr>
                <tr>
                    <td>XAU Gold</td>
                    <td><input type="text" value="7%"></td>
                    <td><input type="text" value="8%"></td>
                    <td><input type="text" value="9%"></td>
                    <td><input type="text" value="X"></td>
                </tr>
                <tr>
                    <td>X</td>
                    <td><input type="text" value="X"></td>
                    <td><input type="text" value="X"></td>
                    <td><input type="text" value="X"></td>
                    <td><input type="text" value="X"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="ConfigList">
        <p class="Bold">Добавление Кредитного Уровня в Систему</p>
        <div class="row">
            <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                <div class="ListActive">
                    <h3>Добавить новый Кредитный Уровень</h3>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">ID:Название:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <input type="text">
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Тип кредитного уровня:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <select>
                                    <option>Выберите тип</option>
                                    <option>актив 1</option>
                                    <option>актив 2</option>
                                    <option>актив 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Параметры:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <input type="text">
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Тип актива:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <select>
                                    <option>Выберите тип</option>
                                    <option>актив 1</option>
                                    <option>актив 2</option>
                                    <option>актив 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Диапозон сроков:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <input type="text">
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                            </div>
                            <div class="col-md-6 col-12">
                                <label class="BoxLab d-flex align-items-center">
                                    <input type="radio" name="gr1">
                                    <span>Активен</span>
                                </label>
                                <label class="BoxLab d-flex align-items-center">
                                    <input type="radio" name="gr1">
                                    <span>Скрыть</span>
                                </label>
                                <button class="btns-green">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                <div class="ListActive">
                    <h3>Удалить ранг инвестора</h3>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Выбрать из списка:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <select>
                                    <option>Выбрать</option>
                                    <option>актив 1</option>
                                    <option>актив 2</option>
                                    <option>актив 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Код страны:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <input type="text" placeholder="Латвия (+371)">
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Номер телефона:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="BoxNumbPos">
                                    <input class="Pos1" type="text" value="+371">
                                    <input class="Pos2" type="text" placeholder="22187783">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="Text1">Введите семизначный код</div>
                    </div>
                    <div class="ListAmin">
                        <div class="row">
                            <div class="col-md-6 col-12 d-flex align-items-center">
                                <div class="Text1">Ввести номер:</div>
                            </div>
                            <div class="col-md-6 col-12">
                                <input type="text">
                            </div>
                        </div>
                        <div class="text-right MarginTop15">
                            Не получили СМС?  <a href="#">Отправить еще одно?</a>
                        </div>
                    </div>
                    <div class="ListAmin">
                        <div class="text-center">
                            <button class="btns-green">Выслать SMS</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
@endsection
