@extends('front.layouts.admin')

@section('content')
    <h1 class="Title text-left MarginBot15">Статистика</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mollis mollis urna, sit amet porta tellus blanditat. Quisque est risus, dignissim vitae quam vitae, tempor ultrices urna.</p>
    <h2 class="Bold">Пользователи</h2>
    <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div class="BoxGrap">
                <h3>Регистрация и верификация</h3>
                <canvas id="GraphLine" width="100%"></canvas>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div class="BoxGrap">
                <h3>Активные Сессий</h3>
                <canvas id="GraphColumn1" width="100%"></canvas>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div class="BoxGrap">
                <h3>Регистрация и верификация</h3>
                <canvas id="Discolumn" width="100%"></canvas>
            </div>
        </div>
    </div>
    <h2 class="Bold">Финансы</h2>
    <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div class="BoxGrap">
                <h3>Пул</h3>
                <canvas id="Discolumn1" width="100%"></canvas>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div class="BoxGrap">
                <h3>Объём выплат</h3>
                <canvas id="GraphColumn2" width="100%"></canvas>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div class="BoxGrap">
                <h3>Объём входящих платежей</h3>
                <canvas id="GraphColumn3" width="100%"></canvas>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div class="BoxGrap">
                <h3>@lang('site.page.cabinet.Profit')</h3>
                <canvas id="GraphLine1" width="100%"></canvas>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div class="BoxGrap">
                <h3>Выплаты и платежи</h3>
                <canvas id="GraphColumn4" width="100%"></canvas>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/js/Chart.min.js"></script>
    <script>

        //GraphLine
        var GraphLine = document.getElementById("GraphLine");
        var GraphLineDataFirst = {
            label: "Зарегистрировавшихся",
            data: [220, 280, 300, 250, 320, 280, 240, 245],
            lineTension: 0,
            borderColor: 'rgba(101, 164, 94, 1)',
            lineTension: 0.2,
            fill: false,
            pointBackgroundColor: 'rgba(101, 164, 94, 1)',
            pointStyle: 'rectRounded'
        };
        var GraphLineDataSecond = {
            label: "Прошедшие Верификацию",
            data: [220, 300, 240, 240, 280, 290, 280, 240],
            lineTension: 0,
            borderColor: 'rgba(249, 154, 14, 1)',
            lineTension: 0.2,
            fill: false,
            pointBackgroundColor: 'rgba(249, 154, 14, 1)',
            pointStyle: 'rectRounded'
        };
        var GraphLineChartData = {
            labels: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август"],
            datasets: [GraphLineDataFirst, GraphLineDataSecond]
        };
        var GraphlineChartOptions = {
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    boxWidth: 12,
                    fontColor: 'black'
                }
            }
        };
        var GraphLinelineChart = new Chart(GraphLine, {
            type: 'line',
            data: GraphLineChartData,
            options: GraphlineChartOptions
        });
        // END GraphLine
        //GraphLine1
        var GraphLine1 = document.getElementById("GraphLine1");
        var GraphLine1DataFirst = {
            label: "Cashflow in",
            data: [220, 280, 300, 250, 320, 280, 240, 245],
            lineTension: 0,
            borderColor: 'rgba(101, 164, 94, 1)',
            lineTension: 0.2,
            fill: false,
            pointBackgroundColor: 'rgba(101, 164, 94, 1)',
            pointStyle: 'rectRounded'
        };
        var GraphLine1DataSecond = {
            label: "Cashflow out",
            data: [220, 300, 240, 240, 280, 290, 280, 240],
            lineTension: 0,
            borderColor: 'rgba(249, 154, 14, 1)',
            lineTension: 0.2,
            fill: false,
            pointBackgroundColor: 'rgba(249, 154, 14, 1)',
            pointStyle: 'rectRounded'
        };
        var GraphLine1ChartData = {
            labels: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август"],
            datasets: [GraphLine1DataFirst, GraphLine1DataSecond]
        };
        var GraphLine1ChartOptions = {
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    boxWidth: 12,
                    fontColor: 'black'
                }
            }
        };
        var GraphLineline1Chart = new Chart(GraphLine1, {
            type: 'line',
            data: GraphLine1ChartData,
            options: GraphLine1ChartOptions
        });
        // END GraphLine1
        //GraphColumn1
        var GraphColumn1 = document.getElementById("GraphColumn1");
        var GraphColumn1DataFirst = {
            label: "Трейдеры",
            data: [245, 280, 300, 250, 280, 280, 248],
            lineTension: 0,
            borderColor: 'rgba(101, 164, 94, 1)',
            lineTension: 0.2,
            fill: false,
            backgroundColor: 'rgba(101, 164, 94, 1)',
        };
        var GraphColumn1DataSecond = {
            label: "Инвесторы",
            data: [250, 275, 295, 245, 275, 285, 250],
            lineTension: 0,
            borderColor: 'rgba(249, 154, 14, 1)',
            lineTension: 0.2,
            fill: false,
            backgroundColor: 'rgba(249, 154, 14, 1)',
        };
        var GraphColumn1ChartData = {
            labels: ["ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС"],
            datasets: [GraphColumn1DataFirst, GraphColumn1DataSecond]
        };
        var GraphColumn1ChartOptions = {
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    boxWidth: 12,
                    fontColor: 'black'
                }
            }
        };
        var GraphColumn1lineChart = new Chart(GraphColumn1, {
            type: 'bar',
            data: GraphColumn1ChartData,
            options: GraphColumn1ChartOptions
        });
        // END GraphColumn1
        //Discolumn
        var Discolumn = document.getElementById("Discolumn");
        var oilData = {
            labels: [
                "Зарегистрированные Инвесторы",
                "Зарегистрированные Трейдеры",
            ],
            datasets: [
                {
                    data: [30, 70],
                    backgroundColor: [
                        "rgba(249, 154, 14, 1)",
                        "rgba(101, 164, 94, 1)",
                    ]
                }]
        };
        var DiscolumnChartOptions = {
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    boxWidth: 12,
                    fontColor: 'black'
                }
            }
        };
        var DiscolumnChart = new Chart(Discolumn, {
            type: 'pie',
            data: oilData,
            options: DiscolumnChartOptions
        });
        // END Discolumn
        //Discolumn1
        var Discolumn1 = document.getElementById("Discolumn1");
        var oilData = {
            labels: [
                "Пул свободных средств",
                "Пул кредитования",
            ],
            datasets: [
                {
                    data: [30, 70],
                    backgroundColor: [
                        "rgba(249, 154, 14, 1)",
                        "rgba(101, 164, 94, 1)",
                    ]
                }]
        };
        var DiscolumnChartOptions = {
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    boxWidth: 12,
                    fontColor: 'black'
                }
            }
        };
        var Discolumn1Chart = new Chart(Discolumn1, {
            type: 'pie',
            data: oilData,
            options: DiscolumnChartOptions
        });
        // END Discolumn1
        //GraphColumn2
        var GraphColumn2 = document.getElementById("GraphColumn2");
        var GraphColumn2DataFirst = {
            label: "Трейдеры",
            data: [245, 280, 300, 250, 280, 280, 248],
            lineTension: 0,
            borderColor: 'rgba(101, 164, 94, 1)',
            lineTension: 0.2,
            fill: false,
            backgroundColor: 'rgba(101, 164, 94, 1)',
        };
        var GraphColumn2ChartData = {
            labels: ["ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС"],
            datasets: [GraphColumn2DataFirst]
        };
        var GraphColumn2ChartOptions = {
            legend: {
                display: false,
                position: 'bottom',
                labels: {
                    boxWidth: 12,
                    fontColor: 'black'
                }
            }
        };
        var GraphColumn2lineChart = new Chart(GraphColumn2, {
            type: 'bar',
            data: GraphColumn2ChartData,
            options: GraphColumn2ChartOptions
        });
        // END GraphColumn2
        //GraphColumn3
        var GraphColumn3 = document.getElementById("GraphColumn3");
        var GraphColumn3DataFirst = {
            label: "Трейдеры",
            data: [245, 280, 300, 250, 280, 280, 248],
            lineTension: 0,
            borderColor: 'rgba(101, 164, 94, 1)',
            lineTension: 0.2,
            fill: false,
            backgroundColor: 'rgba(101, 164, 94, 1)',
        };
        var GraphColumn3ChartData = {
            labels: ["ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС"],
            datasets: [GraphColumn3DataFirst]
        };
        var GraphColumn3ChartOptions = {
            legend: {
                display: false,
                position: 'bottom',
                labels: {
                    boxWidth: 12,
                    fontColor: 'black'
                }
            }
        };
        var GraphColumn3lineChart = new Chart(GraphColumn3, {
            type: 'bar',
            data: GraphColumn3ChartData,
            options: GraphColumn3ChartOptions
        });
        // END GraphColumn3
        //GraphColumn4
        var GraphColumn4 = document.getElementById("GraphColumn4");
        var GraphColumn4DataFirst = {
            label: "Money In",
            data: [245, 280, 300, 250, 280, 280, 248],
            lineTension: 0,
            borderColor: 'rgba(101, 164, 94, 1)',
            lineTension: 0.2,
            fill: false,
            backgroundColor: 'rgba(101, 164, 94, 1)',
        };
        var GraphColumn4DataSecond = {
            label: "Money Out",
            data: [250, 275, 295, 245, 275, 285, 250],
            lineTension: 0,
            borderColor: 'rgba(249, 154, 14, 1)',
            lineTension: 0.2,
            fill: false,
            backgroundColor: 'rgba(249, 154, 14, 1)',
        };
        var GraphColumn4ChartData = {
            labels: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август"],
            datasets: [GraphColumn4DataFirst, GraphColumn4DataSecond]
        };
        var GraphColumn4ChartOptions = {
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    boxWidth: 12,
                    fontColor: 'black'
                }
            }
        };
        var GraphColumn4lineChart = new Chart(GraphColumn4, {
            type: 'bar',
            data: GraphColumn4ChartData,
            options: GraphColumn4ChartOptions
        });
        // END GraphColumn4
    </script>
@endsection
