@extends('front.layouts.admin')

@section('content')
    <h1 class="Title text-left MarginBot15">Платежи</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mollis mollis urna, sit amet porta tellus blanditatQuisque est risus, dignissim vitae quam vitae, tempor ultrices urna.</p>
    <div class="MarginTop30 MarginBot40">
        <span class="Bold">Создать платеж</span>
        <a href="#" class="btns-orange ADDPaymentBTN"><i class="fas fa-plus"></i> Создать</a>
    </div>
    <div class="AddPayment ListActive">
        <h3>Добавить новый платеж</h3>
        <div class="ListAmin">
            <div class="row">
                <div class="col-md-6 col-12 d-flex align-items-center">
                    <div class="Text1">Шаблон:</div>
                </div>
                <div class="col-md-6 col-12">
                    <select>
                        <option>Выберите шаблон</option>
                        <option>Шаблон 1</option>
                        <option>Шаблон 2</option>
                        <option>Шаблон 3</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="ListAmin">
            <div class="row">
                <div class="col-md-6 col-12 d-flex align-items-center">
                    <div class="Text1">Дата платежа:</div>
                </div>
                <div class="col-md-6 col-12">
                    <input type="text">
                </div>
            </div>
        </div>
        <div class="ListAmin">
            <div class="row">
                <div class="col-md-6 col-12 d-flex align-items-center">
                    <div class="Text1">Кому:</div>
                </div>
                <div class="col-md-6 col-12">
                    <input type="text">
                </div>
            </div>
        </div>
        <div class="ListAmin">
            <div class="row">
                <div class="col-md-6 col-12 d-flex align-items-center">
                    <div class="Text1">Тип платежа:</div>
                </div>
                <div class="col-md-6 col-12">
                    <input type="text">
                </div>
            </div>
        </div>
        <div class="ListAmin">
            <div class="row">
                <div class="col-md-6 col-12 d-flex align-items-center">
                    <div class="Text1">Сумма:</div>
                </div>
                <div class="col-md-6 col-12">
                    <input type="text">
                </div>
            </div>
        </div>
        <div class="ListAmin">
            <div class="row">
                <div class="col-md-6 col-12 d-flex align-items-center">
                    <div class="Text1">Срок оплаты:</div>
                </div>
                <div class="col-md-6 col-12">
                    <input type="text">
                </div>
            </div>
        </div>
        <div class="ListAmin">
            <div class="row">
                <div class="col-md-6 col-12 d-flex align-items-center">
                    <div class="Text1"> Пояснение:</div>
                </div>
                <div class="col-md-6 col-12">
                    <textarea></textarea>
                </div>
            </div>
        </div>
        <div class="ListAmin">
            <div class="text-center MarginBot15">
                <button class="btns-orange">Сохранить</button>
                <button class="btns-green">Послать</button>
            </div>
        </div>
    </div>
    <div class="FiltrListBox">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="ListFilter">
                    <div class="TextLeftFiltr">Фильтр 1:</div>
                    <div class="FiltrSelec">
                        <select>
                            <option>Все</option>
                            <option>Системные</option>
                            <option>Трейдеры</option>
                            <option>Инвесторы</option>
                            <option>Поддержка</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="ListFilter">
                    <div class="TextLeftFiltr">Фильтр 2:</div>
                    <div class="FiltrSelec">
                        <select>
                            <option>Все</option>
                            <option>Системные</option>
                            <option>Трейдеры</option>
                            <option>Инвесторы</option>
                            <option>Поддержка</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="ListFilter">
                    <div class="TextLeftFiltr">Фильтр 3:</div>
                    <div class="FiltrSelec">
                        <select>
                            <option>Все</option>
                            <option>Системные</option>
                            <option>Трейдеры</option>
                            <option>Инвесторы</option>
                            <option>Поддержка</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="ListFilter">
                    <div class="TextLeftFiltr">Фильтр 4:</div>
                    <div class="FiltrSelec">
                        <select>
                            <option>Все</option>
                            <option>Системные</option>
                            <option>Трейдеры</option>
                            <option>Инвесторы</option>
                            <option>Поддержка</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="TopLineSilver"></div>
    <div class="row">
        <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="row no-gutters">
                <div class="col-xl-2 col-lg-3 col-md-12 col-sm-12 col-12">
                    <div class="Text MarginTop10 Bold">Поиск по платежам:</div>
                </div>
                <div class="col-xl-10 col-lg-9 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-md-8 col-12">
                            <div class="JournalSearchBox MarginBot15">
                                <input type="text">
                                <button></button>
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="FiltrJournal">
                                <select>
                                    <option>Все</option>
                                    <option>Системные</option>
                                    <option>Трейдеры</option>
                                    <option>Инвесторы</option>
                                    <option>Поддержка</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="text-right">
                <button class="btns-green MarginBot15">EXPORT CSV</button>
                <button class="btns-orange">Export PDF</button>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered  text-center FZ12">
            <thead>
            <tr>
                <th>ID</th>
                <th>Отправлено</th>
                <th>Абонент</th>
                <th>Тип платежа</th>
                <th>Штраф</th>
                <th>Комиссия</th>
                <th>Тело Кредита</th>
                <th>Процентная часть</th>
                <th>Сумма платежа</th>
                <th>Дата срока оплаты</th>
                <th>Пояснение</th>
                <th>Шаблон</th>
                <th>Выбрать</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>20 €</td>
                <td>43593895</td>
                <td>Срочный</td>
                <td>20 €</td>
                <td>20 €</td>
                <td>Lorem ipsum</td>
                <td>20%</td>
                <td>20 €</td>
                <td>22.09.2020</td>
                <td>Aenean mollis mollis urna</td>
                <td>Lorem ipsum</td>
                <td><a href="#" class="btns-orange SmallBTN">Выбрать</a></td>
            </tr>
            <tr>
                <td>2</td>
                <td>20 €</td>
                <td>43593895</td>
                <td>Срочный</td>
                <td>20 €</td>
                <td>20 €</td>
                <td>Lorem ipsum</td>
                <td>20%</td>
                <td>20 €</td>
                <td>22.09.2020</td>
                <td>Aenean mollis mollis urna</td>
                <td>Lorem ipsum</td>
                <td><a href="#" class="btns-orange SmallBTN">Выбрать</a></td>
            </tr>
            <tr>
                <td>3</td>
                <td>20 €</td>
                <td>43593895</td>
                <td>Срочный</td>
                <td>20 €</td>
                <td>20 €</td>
                <td>Lorem ipsum</td>
                <td>20%</td>
                <td>20 €</td>
                <td>22.09.2020</td>
                <td>Aenean mollis mollis urna</td>
                <td>Lorem ipsum</td>
                <td><a href="#" class="btns-orange SmallBTN">Выбрать</a></td>
            </tr>
            <tr>
                <td>4</td>
                <td>20 €</td>
                <td>43593895</td>
                <td>Срочный</td>
                <td>20 €</td>
                <td>20 €</td>
                <td>Lorem ipsum</td>
                <td>20%</td>
                <td>20 €</td>
                <td>22.09.2020</td>
                <td>Aenean mollis mollis urna</td>
                <td>Lorem ipsum</td>
                <td><a href="#" class="btns-orange SmallBTN">Выбрать</a></td>
            </tr>
            <tr>
                <td>5</td>
                <td>20 €</td>
                <td>43593895</td>
                <td>Срочный</td>
                <td>20 €</td>
                <td>20 €</td>
                <td>Lorem ipsum</td>
                <td>20%</td>
                <td>20 €</td>
                <td>22.09.2020</td>
                <td>Aenean mollis mollis urna</td>
                <td>Lorem ipsum</td>
                <td><a href="#" class="btns-orange SmallBTN">Выбрать</a></td>
            </tr>
            <tr>
                <td>6</td>
                <td>20 €</td>
                <td>43593895</td>
                <td>Срочный</td>
                <td>20 €</td>
                <td>20 €</td>
                <td>Lorem ipsum</td>
                <td>20%</td>
                <td>20 €</td>
                <td>22.09.2020</td>
                <td>Aenean mollis mollis urna</td>
                <td>Lorem ipsum</td>
                <td><a href="#" class="btns-orange SmallBTN">Выбрать</a></td>
            </tr>
            <tr>
                <td>7</td>
                <td>20 €</td>
                <td>43593895</td>
                <td>Срочный</td>
                <td>20 €</td>
                <td>20 €</td>
                <td>Lorem ipsum</td>
                <td>20%</td>
                <td>20 €</td>
                <td>22.09.2020</td>
                <td>Aenean mollis mollis urna</td>
                <td>Lorem ipsum</td>
                <td><a href="#" class="btns-orange SmallBTN">Выбрать</a></td>
            </tr>
            </tbody>
        </table>
        <div class="text-right Bold">
            <a href="#">Смотреть больше <i class="fas fa-caret-right"></i></a>
        </div>
    </div>
@endsection

@section('modals')
@endsection
