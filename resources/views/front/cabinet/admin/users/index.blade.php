@extends('front.layouts.admin')

@section('content')
    <h1 class="Title text-left MarginBot15">Пользователи</h1>
    <p class="MarginBot15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mollis mollis urna, sit amet porta tellus blanditatQuisque est risus, dignissim vitae quam vitae, tempor ultrices urna.</p>
    <div class="FiltrListBox">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="ListFilter">
                    <div class="TextLeftFiltr">Фильтр 1:</div>
                    <div class="FiltrSelec">
                        <select>
                            <option>Все</option>
                            <option>Системные</option>
                            <option>Трейдеры</option>
                            <option>Инвесторы</option>
                            <option>Поддержка</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="ListFilter">
                    <div class="TextLeftFiltr">Фильтр 2:</div>
                    <div class="FiltrSelec">
                        <select>
                            <option>Все</option>
                            <option>Системные</option>
                            <option>Трейдеры</option>
                            <option>Инвесторы</option>
                            <option>Поддержка</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="ListFilter">
                    <div class="TextLeftFiltr">Фильтр 3:</div>
                    <div class="FiltrSelec">
                        <select>
                            <option>Все</option>
                            <option>Системные</option>
                            <option>Трейдеры</option>
                            <option>Инвесторы</option>
                            <option>Поддержка</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="ListFilter">
                    <div class="TextLeftFiltr">Фильтр 4:</div>
                    <div class="FiltrSelec">
                        <select>
                            <option>Все</option>
                            <option>Системные</option>
                            <option>Трейдеры</option>
                            <option>Инвесторы</option>
                            <option>Поддержка</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="TopLineSilver"></div>
    <div class="row">
        <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="row no-gutters">
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                    <div class="Text MarginTop10 Bold">Поиск по пользователям:</div>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-md-8 col-12">
                            <div class="JournalSearchBox MarginBot15">
                                <input type="text">
                                <button></button>
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="FiltrJournal">
                                <select>
                                    <option>Все</option>
                                    <option>Системные</option>
                                    <option>Трейдеры</option>
                                    <option>Инвесторы</option>
                                    <option>Поддержка</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="text-right">
                <button class="btns-green MarginBot15">EXPORT CSV</button>
                <button class="btns-orange">Export PDF</button>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered text-center FZ12">
            <thead>
            <tr>
                <th>ID</th>
                <th>Фамилия имя</th>
                <th>Тип Верификации</th>
                <th>Ранг/Кредитный Уровень</th>
                <th>Cумма вклада/займа</th>
                <th>Баланс</th>
                <th>Дата регистрации</th>
                <th>Последнее подключение</th>
                <th>Действующие договора</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Алексей Иванов</td>
                <td>1</td>
                <td>Lorem ipsum</td>
                <td>20 €</td>
                <td>20 €</td>
                <td>22.12.1998</td>
                <td>22.12.2020</td>
                <td>Lorem ipsum</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Алексей Иванов</td>
                <td>1</td>
                <td>Lorem ipsum</td>
                <td>20 €</td>
                <td>20 €</td>
                <td>22.12.1998</td>
                <td>22.12.2020</td>
                <td>Lorem ipsum</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Алексей Иванов</td>
                <td>1</td>
                <td>Lorem ipsum</td>
                <td>20 €</td>
                <td>20 €</td>
                <td>22.12.1998</td>
                <td>22.12.2020</td>
                <td>Lorem ipsum</td>
            </tr>
            <tr>
                <td>4</td>
                <td>Алексей Иванов</td>
                <td>1</td>
                <td>Lorem ipsum</td>
                <td>20 €</td>
                <td>20 €</td>
                <td>22.12.1998</td>
                <td>22.12.2020</td>
                <td>Lorem ipsum</td>
            </tr>
            <tr>
                <td>5</td>
                <td>Алексей Иванов</td>
                <td>1</td>
                <td>Lorem ipsum</td>
                <td>20 €</td>
                <td>20 €</td>
                <td>22.12.1998</td>
                <td>22.12.2020</td>
                <td>Lorem ipsum</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection

@section('modals')
@endsection
