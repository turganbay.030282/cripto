@extends('front.layouts.cabinet')

@section('content')
    <div class="CabinetContent">
        <h1 class="Title text-left">@lang('site.header.bonuses')</h1>
        <div class="table-responsive MarginBot40">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">@lang('site.page.cabinet.bonuses-registered')</th>
                        <th class="text-center">@lang('site.page.cabinet.bonuses-earned')</th>
                        <th class="text-center">@lang('site.page.cabinet.bonuses-withdrawn')</th>
                        <th class="text-center">@lang('site.page.cabinet.bonuses-total')</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center">{{ $user->referrals->count() }}</td>
                        <td class="text-center">{{ $user->bonus_total }}</td>
                        <td class="text-center">{{ $user->bonus_payment_amount }}</td>
                        <td class="text-center">{{ format_number($user->amount_bonus) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-sm-7">
                <div class="form-group">
                    <a href="#" data-toggle="modal" data-target="#BonusOut" class="btns-green">{{ trans('site.page.cabinet.bonuses-payout') }}</a>
                    &nbsp;
                    <a href="#" data-toggle="modal" data-target="#BonusOutDai" class="btns-green">{{ trans('site.page.cabinet.bonuses-payout-dai') }}</a>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
{{--                        <th class="text-center">@lang('site.page.cabinet.bonuses-name')</th>--}}
                        <th class="text-center">@lang('site.page.cabinet.bonuses-type')</th>
                        <th class="text-center">@lang('site.page.cabinet.bonuses-date')</th>
                        <th class="text-center">@lang('site.page.cabinet.bonuses-amount')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bonuses as $key => $bonus)
                        <tr>
                            <td>{{ $bonus->id }}</td>
{{--                            <td class="text-center">{{ $bonus->client->full_name }}</td>--}}
                            <td class="text-center">{{ $bonus->type }}</td>
                            <td class="text-center">{{ $bonus->created_at }}</td>
                            <td class="text-center">{{ $bonus->bonus_amount_partner }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="text-right Bold">
                {{ $bonuses->appends(request()->all())->links() }}
            </div>
        </div>

    </div>
@endsection

@section('modals2')
    <!-- Modal BonusOut -->
    <div class="modal HeaderBlue" id="BonusOut" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                        <i class="fal fa-times-circle"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <h2 class="text-center">{{ trans('site.page.cabinet.trader-payout-title') }}</h2>
                    {!! Form::open(['url' => route('front.cabinet.client.bonuses.payout'), 'method' => 'POST']) !!}
                    <div class="BoxPaddActive">
                        <div class="row">
                            <div class="col-xl-4 col-4">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.cabinet.trader-payout-amount') }}:</h6>
                                </div>
                            </div>
                            <div class="col-xl-8 col-8">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('amount', null, ['placeholder' => trans('site.page.cabinet.trader-payout-amount-placeholder'), 'required' => true]) !!}
                                    {!! Form::error('amount') !!}
                                </div>
                            </div>
{{--                            <div class="col-xl-4 col-4">--}}
{{--                                <div class="LeftFormText d-flex align-items-center">--}}
{{--                                    <h6>{{ trans('site.page.cabinet.trader-payout-select-paymethod') }}:</h6>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-xl-8 col-8">--}}
{{--                                <div class="form-group LeftFormText d-flex align-items-center">--}}
{{--                                    <select name="wallet">--}}
{{--                                        <option>4045HABA2256984569450</option>--}}
{{--                                        <option>4045HABA2256984569451</option>--}}
{{--                                        <option>4045HABA2256984569452</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                        <div class="form-group text-center MarginTop30">
                            <button class="btns-red" data-dismiss="modal" aria-label="Close">@lang('site.page.cabinet.Cancel')</button>
                            <button class="btns-green btn-withdraw" type="submit">{{ trans('site.page.cabinet.trader-payout-btn-request') }}</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <!-- Modal BonusOutDai -->
    <div class="modal HeaderBlue" id="BonusOutDai" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                        <i class="fal fa-times-circle"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <h2 class="text-center">{{ trans('site.page.cabinet.trader-payout-title') }}</h2>
                    {!! Form::open(['url' => route('front.cabinet.client.bonuses.payoutDai'), 'method' => 'POST']) !!}
                    <div class="BoxPaddActive">
                        <div class="row">
                            <div class="col-xl-4 col-4">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.cabinet.trader-payout-amount') }}:</h6>
                                </div>
                            </div>
                            <div class="col-xl-8 col-8">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('amount', null, ['placeholder' => trans('site.page.cabinet.trader-payout-amount-placeholder'), 'required' => true]) !!}
                                    {!! Form::error('amount') !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center MarginTop30">
                            <button class="btns-red" data-dismiss="modal" aria-label="Close">@lang('site.page.cabinet.Cancel')</button>
                            <button class="btns-green btn-withdraw" type="submit">{{ trans('site.page.cabinet.trader-payout-btn-request') }}</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
