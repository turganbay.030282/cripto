@extends('front.layouts.cabinet')

@section('content')
    <div class="CabinetContent">
        <h1 class="Title text-left">@lang('site.header.transactions')</h1>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th class="text-center">@lang('site.page.cabinet.magazine-table-number')</th>
                    <th class="text-center">@lang('site.page.cabinet.type-conversion')</th>
                    <th class="text-center">@lang('site.page.cabinet.table-date-and-time')</th>
                    <th class="text-center">@lang('site.page.cabinet.amount-EUR')</th>
                    <th class="text-center">@lang('site.page.cabinet.coin-type')</th>
                    <th class="text-center">@lang('site.page.cabinet.amount-coin')</th>
                    <th class="text-center">@lang('site.page.cabinet.status')</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($userConverts as $item)
                    <tr>
                        <td class="text-center">{{ $item->id }}</td>
                        <td class="text-center">{{ $item->type }}</td>
                        <td class="text-center">{{ $item->created_at->format('d.m.Y H:i') }}</td>
                        <td class="text-center">{{ format_number($item->amount) }}</td>
                        <td class="text-center">{{ $item->typeAsset->name }}</td>
                        <td class="text-center">{{ numeric_fmt($item->amount_asset) }}</td>
                        <td class="text-center">{{ $item->status }}</td>
                        <td>
                            {!! Form::open(['url' => route('front.cabinet.client.conversions.cancel', $item), 'class' => 'form-inline-block']) !!}
                            <input type="hidden" name="id" value="{{ $item->id }}">
                            <input type="hidden" name="_method" value="PUT">
                            <div class="btn-group">
                                <button {{!$item->isNew() ? 'disabled' : '' }}
                                        class="btn btn-danger btn-sm">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right Bold">
                {{ $userConverts->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
@endsection
