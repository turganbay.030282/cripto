@extends('front.layouts.temp')

@section('content')
    <div class="CabHeaderSec GreenBG">
        <div class="container">
            <h1 class="text-center">{{ trans('site.page.cabinet.convert-title') }}</h1>
            <h4 class="text-center">{{ trans('site.page.cabinet.convert-description') }}</h4>
        </div>
    </div>
    <div class="ConvertContent">
        <div class="ConvertSec">
            <div class="container">
                <div class="ConvertBox">
                    {!! Form::open(['url' => route('front.cabinet.convert.request')]) !!}
                    <div class="form-group">
                        <div class="row MyRow15">
                            <div class="col-8 MyCollum15">
                                <div class="DopText">{{ trans('site.page.cabinet.convert-asset-from') }}</div>
                                {{ Form::text('amount', null, ['placeholder' => 'от 10 000 до 2 500 000 EUR', 'id' => 'amount']) }}
{{--                                {{ Form::text('amount', null, ['placeholder' => trans('site.page.cabinet.convert-placeholder-amount'), 'id' => 'amount']) }}--}}
                                {{ Form::error('amount') }}
                            </div>
                            <div class="col-4 MyCollum15">
                                <div class="DopText"></div>
                                {{ Form::select('type_id', ['EUR' => 'EUR'], null, ['disabled' => !$user]) }}
                                {{ Form::error('type_id') }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row MyRow15">
                            <div class="col-8 MyCollum15">
                                <div class="DopText">{{ trans('site.page.cabinet.convert-asset-to') }}</div>
{{--                                {{ Form::text('amount_asset', null, ['placeholder' => trans('site.page.cabinet.convert-placeholder-amount_asset'), 'id' => 'amount_asset']) }}--}}
                                {{ Form::text('amount_asset', null, ['placeholder' => 'от 0.005 до 100 BTC', 'id' => 'amount_asset']) }}
                                {{ Form::error('amount_asset') }}
                            </div>
                            <div class="col-4 MyCollum15">
                                <div class="DopText"></div>
                                {{ Form::select('type_asset_id', $assets, null, ['id' => 'type_asset_id', 'disabled' => !$user]) }}
                                {{ Form::error('type_asset_id') }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btns-orange" name="type-buy" {{$user ? '' : 'disabled'}}
                                type="submit">{{ trans('site.page.cabinet.convert-btn-buy') }}</button>
                        <button class="btns-orange" name="type-sell" {{$user ? '' : 'disabled'}}
                                type="submit">{{ trans('site.page.cabinet.convert-btn-sell') }}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>

        @if($categories->isNotEmpty())
            @foreach($categories as $key => $cat)
                <div class="WhiteBG">
                    <div class="SectionFAQ MarginTop30">
                        <div class="container">
                            <div class="CategoryFAQ">
                                <h2 class="text-normal">{{ trans_field($cat, 'title') }}</h2>
                                @if($cat->items->isNotEmpty())
                                    <div class="BoxFAQ" id="accordionFAQ{{ $cat->id }}">
                                        @foreach($cat->items as $item)
                                            <div class="FAQList">
                                                <div class="FAQHeader">
                                                    <a href="#" class="ShowMoreDetalis collapsed" data-toggle="collapse"
                                                       data-target="#FAQ{{ $cat->id }}{{ $item->id }}">
                                                        {{ trans_field($item, 'title') }}
                                                    </a>
                                                </div>
                                                <div id="FAQ{{ $cat->id }}{{ $item->id }}" class="collapse"
                                                     data-parent="#accordionFAQ{{ $cat->id }}">
                                                    <div class="card-body">
                                                        {!! trans_field($item, 'description') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
@endsection

@section('script')
    <script>
        $('#amount').on('change', function (e) {
            let amount = $(this).val() * 1;
            if (amount) {
                $.ajax({
                    url: "{{ route('front.cabinet.ajax.coinPrice') }}",
                    method: 'POST',
                    data: {coin: $('#type_asset_id').val()}
                }).done(function (response) {
                    if (response.hasOwnProperty('price')) {
                        $('#amount_asset').val((amount / response.price).toFixed(8));
                    }
                });
            }
        });

        $('#amount_asset').on('change', function (e) {
            let amount_asset = $(this).val() * 1;console.log(amount_asset);
            if (amount_asset) {
                $.ajax({
                    url: "{{ route('front.cabinet.ajax.coinPrice') }}",
                    method: 'POST',
                    data: {coin: $('#type_asset_id').val()}
                }).done(function (response) {
                    if (response.hasOwnProperty('price')) {
                        $('#amount').val((amount_asset * response.price).toFixed(2));
                    }
                });
            }
        });

        $('#type_asset_id').on('change', function (e) {
            let amount_asset = $('#amount_asset').val() * 1;
            if (amount_asset) {
                $.ajax({
                    url: "{{ route('front.cabinet.ajax.coinPrice') }}",
                    method: 'POST',
                    data: {coin: $('#type_asset_id').val()}
                }).done(function (response) {
                    if (response.hasOwnProperty('price')) {
                        $('#amount').val((amount_asset * response.price).toFixed(2));
                    }
                });
            }
        });
    </script>
@endsection
