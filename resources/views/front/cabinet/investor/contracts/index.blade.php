<?php
/** @var \App\Entity\Contract\Contract $contract */
?>

@extends('front.layouts.cabinet')

@section('modals')

@endsection

@section('content')
    <div class="CabinetContent Dogov">
        <div class="CabSec">
            <div class="ListLeftRight">
                <h2>@lang('site.page.cabinet.My-contracts')</h2><br>
{{--                <div class="row">--}}
{{--                    <div class="col-xl-6 col-12">--}}
{{--                        <div class="Height100pc d-flex align-items-center">--}}
{{--                            <div class="Text1">@lang('site.page.cabinet.Amount-of-investment'):</div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-xl-6 col-12">--}}
{{--                        <div class="Height100pc d-flex align-items-center">--}}
{{--                            <div class="Text3">{{ $assetContractsAmount }} € = DAI {{ format_number($assetContractsAmount / $setting->course_investor) }}</div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>

        @if($assetContracts->isNotEmpty())
            <h2>@lang('site.page.cabinet.concluded-contracts')</h2>
            <div class="row">
                @foreach($assetContracts as $contract)
                    <div class="col-xl-4 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="ListDogovora">
                            <h4>
                                {{ $contract->isClose() ? trans('site.page.cabinet.close-contract') : ($contract->isWaitClose() ? trans('site.page.cabinet.wait-close-contract') : trans('site.page.cabinet.Active-contract')) }}
                                {{ $contract->number }}
                            </h4>
                            <div class="ListLeftRight">
                                <div class="row">
                                    <div class="col-7">
                                        <div class="Height100pc d-flex align-items-center">
                                            <div class="Text1">@lang('site.page.cabinet.Total-cost-invest'):</div>
                                        </div>
                                    </div>
                                    <div class="col-5">
{{--                                        <div class="Text2">€ {{ $contract->amount }} = {{ $contract->typeAsset->coinValue->symbol ?? '' }} {{ format_number($contract->amount_asset) }}</div>--}}
                                        <div class="Text2">{{ $contract->typeAsset->coinValue->symbol ?? '' }} {{ format_number($contract->amount_asset) }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="ListLeftRight">
                                <div class="row">
                                    <div class="col-7">
                                        <div class="Height100pc d-flex align-items-center">
                                            <div class="Text1">@lang('site.page.cabinet.Total-cost-percent'):</div>
                                        </div>
                                    </div>
                                    <div class="col-5">
{{--                                        <div class="Text2 Green">€ {{ $contract->getAmountPercentOnCurrentDate() }} = {{ $contract->typeAsset->coinValue->symbol ?? '' }} {{ $contract->getAmountPercentOnCurrentDateDai() }}</div>--}}
                                        <div class="Text2 Green">{{ $contract->typeAsset->coinValue->symbol ?? '' }} {{ $contract->getAmountPercentOnCurrentDateDai() }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center MarginTop15">
                                <a href="#" class="btns-green" data-toggle="modal" data-target="#Dogovor{{ $contract->id }}">@lang('site.page.cabinet.Open-contract')</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
@endsection

@section('modals2')
    @if($assetContracts->isNotEmpty())
        @foreach($assetContracts as $contract)
            <div class="modal HeaderBlue" id="Dogovor{{ $contract->id }}" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                                <i class="fal fa-times-circle"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h2 class="text-center">@lang('site.page.cabinet.Contract')</h2>
                            <div class="BoxPaddActive">
                                <div class="row">
                                    <div class="col-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>@lang('site.page.cabinet.Date-conclusion'):</h6>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $contract->created_at->format('d.m.Y') }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>@lang('site.page.cabinet.Number-of-contract'):</h6>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $contract->number }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>@lang('site.page.cabinet.amount-deal'):</h6>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="RightFormText d-flex align-items-center">
{{--                                            <h6>€ {{ $contract->amount }} = {{ $contract->typeAsset->coinValue->symbol ?? '' }} {{ format_number($contract->amount / ($contract->typeAsset->coinValue->price ?? 1)) }}</h6>--}}
                                            <h6>{{ $contract->typeAsset->coinValue->symbol ?? '' }} {{ format_number($contract->amount_asset) }}</h6>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="LeftFormText d-flex align-items-center">
                                            <h6>@lang('site.page.cabinet.Contract-expiration-date'):</h6>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="RightFormText d-flex align-items-center">
                                            <h6>{{ $contract->created_at->addMonths($contract->period)->format('d.m.Y') }}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group text-center MarginTop15 BTNWidth100">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            {!! Form::open(['url' => route('front.cabinet.investor.contracts.download', $contract)]) !!}
                                            <div class="DownloadBTNS ResponsiveMarginBot15">
                                                <button class="btns-orange" type="submit">@lang('site.page.cabinet.Download')</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                        <div class="col-md-6 col-12">
                                            {!! Form::open(['url' => route('front.cabinet.investor.contracts.print', $contract)]) !!}
                                            <div class="PrintBTNS">
                                                <button type="submit" class="btns-orange btn-contract-print">@lang('site.page.cabinet.Print')</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                                @if($contract->isActive())
                                    <div class="form-group text-center BTNWidth100">
                                        <button class="btns-red" data-toggle="modal" data-target="#DogovorTerminate{{ $contract->id }}">@lang('site.page.cabinet.Terminate')</button>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if($contract->isActive())
                <div class="modal HeaderBlue" id="DogovorTerminate{{ $contract->id }}" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                                    <i class="fal fa-times-circle"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <h2 class="text-center">@lang('site.page.cabinet.contract-inv-terminate-title')</h2>
                                <div class="BoxPaddActive">
                                    <div class="row">
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.contract-inv-terminate-investing'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
{{--                                                <h6>{{ format_number($contract->amount) }} = {{ format_number($contract->amount / ($contract->typeAsset->coinValue->price ?? 1)) }}--}}
{{--                                                    {{ $contract->typeAsset->coinValue->symbol ?? '' }}</h6>--}}
                                                <h6>{{ format_number($contract->amount_asset) }}
                                                    {{ $contract->typeAsset->coinValue->symbol ?? '' }}</h6>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.contract-inv-terminate-get-percent'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>0</h6>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.contract-inv-terminate-profit-amount'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
{{--                                                <h6>{{ format_number($contract->getAmountPercentOnCurrentDate()) }} = {{ format_number($contract->getAmountPercentOnCurrentDate() / ($contract->typeAsset->coinValue->price ?? 1)) }} {{ $contract->typeAsset->coinValue->symbol ?? '' }}</h6>--}}
                                                @php($amount = format_number($contract->getAmountPercentOnCurrentDate() / ($contract->course ? $contract->course : ($contract->typeAsset->coinValue->price ?? 1))))
                                                <h6>{{ $amount < format_number($contract->amount_asset) ? format_number($contract->amount_asset) : $amount }} {{ $contract->typeAsset->coinValue->symbol ?? '' }}</h6>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.contract-inv-terminate-commission-system'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>{{ format_number($contract->amount_asset*$setting->commission_investor/100) }} {{ $contract->typeAsset->coinValue->symbol ?? '' }}</h6>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.contract-inv-terminate-get'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
{{--                                                <h6>{{ format_number($contract->amount - $contract->amount*$setting->commission_investor/100) }} = {{ format_number(($contract->amount - $contract->amount*$setting->commission_investor/100) / ($contract->typeAsset->coinValue->price ?? 1)) }} {{ $contract->typeAsset->coinValue->symbol ?? '' }}</h6>--}}
                                                <h6>{{ format_number(($contract->amount_asset - $contract->amount_asset*$setting->commission_investor/100)) }} {{ $contract->typeAsset->coinValue->symbol ?? '' }}</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center MarginTop15 BTNWidth100">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="ResponsiveMarginBot15">
                                                    <button class="btns-red" data-dismiss="modal" aria-label="Close">@lang('site.page.cabinet.Cancel')</button>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                {!! Form::open(['url' => route('front.cabinet.investor.contracts.terminate', $contract)]) !!}
                                                <div class="ResponsiveMarginBot15">
                                                    <button type="submit" class="btns-green">@lang('site.page.cabinet.contract-inv-terminate-btn')</button>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    @endif
@endsection

@section('script')
    <script>
        $('.btn-contract-print').on('click', function(e){
            e.preventDefault();
            $.ajax({
                url: $(this).closest('form').attr('action'),
                method: 'POST',
            }).done(function(response){
                var win = window.open(response, '_blank');
                win.focus();
            });
        });
    </script>
@endsection
