@extends('front.layouts.cabinet')

@section('modals')
    @if(!$user->is_verification)
        @if($setting->is_manual_verify)
            @include('front.cabinet.parts.modals._verify_manual')
        @else
            @include('front.cabinet.parts.modals._verify')
        @endif
    @endif
    @if($user->is_verification && !$user->investor_rank)
        @if($user->is_ur)
            @include('front.cabinet.investor.parts._modal_investor_rank_ur')
        @else
            @include('front.cabinet.investor.parts._modal_investor_rank')
        @endif
    @endif
    @if($user->is_verification)
        @include('front.cabinet.investor.parts._modal_contract')
        @include('front.cabinet.investor.parts._modal_contract_dai')
        @include('front.cabinet.investor.parts._modal_payout')
    @endif
    @if($user->isBlock())
        @include('front.cabinet.parts.modals._modal_block')
    @endif
@endsection

@section('content')
    <div class="CabinetContent">
        <div class="CabSec" id="CabSec1">
            <h2>
                <a href="#" class="ShowMoreDetalis" data-toggle="collapse" data-target="#CabSec1-Collaps">@lang('site.page.cabinet.Available-actions-section')</a>
            </h2>
            <div id="CabSec1-Collaps" class="collapse show" data-parent="#CabSec1">
                <div class="SecInner">
                    <div class="Descrip">@lang('site.page.cabinet.investor-Invite-upload-digital-photos')</div>
                    <div class="row no-gutters">

                        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="ListAction">
                                <div class="form-group">
                                    <div class="ActionStatus {{ $user->is_verification ? 'Yes' : 'No'}}"></div>
                                </div>
                                <h3>@lang('site.page.cabinet.Pass-Verification')</h3>
                                @if($userVerification && ($userVerification->isWait() || $userVerification->isReject()))
                                    <div class="Text">
                                        @if($userVerification->isWait())
                                            <a href="#" class="btn-reload">
                                                <i class="fas fa-sync"></i> <span class="timer-reload">30</span>
                                            </a>
                                        @else
                                            {{ $userVerification->status_name }} {{ $userVerification->comment ? ' : ' . $userVerification->comment : ''}}
                                        @endif
                                    </div>
                                @elseif($userVerification && $userVerification->isApprove())
                                    <div class="Text">
                                        @lang('site.page.cabinet.Verification-passed-successfully')
                                    </div>
                                @endif
                                @if(!$userVerification || $userVerification->isReject())
                                    <a href="#" class="btns-orange" data-toggle="modal" data-target="#VerificationLine">@lang('site.page.start')</a>
                                @endif
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="ListAction {{ $user->is_verification ? '' : 'Disalow'}}">
                                <div class="form-group">
                                    <div class="ActionStatus {{ $user->investor_rank ? 'Yes' : 'No'}}"></div>
                                </div>
                                <h3>@lang('site.page.cabinet.Select-Investment-Rank')</h3>
                                @if($investorRankVerification && ($investorRankVerification->isWait() || $investorRankVerification->isReject()))
                                    <div class="Text">
                                        @if($investorRankVerification->isWait())
                                            <a href="#" class="btn-reload">
                                                <i class="fas fa-sync"></i> <span class="timer-reload">30</span>
                                            </a>
                                        @else
                                            {{ $investorRankVerification->status_name }} {{ $investorRankVerification->comment ? ' : ' . $investorRankVerification->comment : ''}}
                                        @endif
                                    </div>
                                @elseif($investorRankVerification && $investorRankVerification->isApprove())
                                    <div class="Text">
                                        @lang('site.page.cabinet.Investment-Rank-verify')
                                    </div>
                                @endif
                                @if(!$investorRankVerification || $investorRankVerification->isReject())
                                    <a href="#" data-toggle="modal" data-target="#InvestorRank1" class="btns-orange">@lang('site.page.start')</a>
                                @endif
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="ListAction {{ $user->is_verification && $investorRankVerification ? '' : 'Disalow'}}">
                                <div class="form-group">
                                    <div class="ActionStatus  {{ $hasFirstPayIn ? 'Yes' : 'No'}}"></div>
                                </div>
                                <h3>@lang('site.page.cabinet.Make-a-deposit')</h3>
                                <div class="Text"></div>
                                @if(!$hasFirstPayIn)
                                    <a href="#" class="btns-orange" data-toggle="modal" data-target="#DepositInEuro">@lang('site.page.start')</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('front.cabinet.parts._promo')

        <div class="Calculator">
            <div class="container">
                <h2>@lang('site.page.calc-investor-Interactive-form-calculating')</h2>
                <div class="row no-gutters">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="BoxCalculator">
                            <h3>@lang('site.page.cabinet.investor-asset1'):</h3>
                            <div class="form-group">
                                <select id="asset-natural" {{ $user->isBlock() ? 'disabled' : ''}}>
                                    @foreach($actives as $active)
                                        <option value="{{ $active->id }}" data-price="{{ $active->coinValue ? $active->coinValue->price : 0 }}" data-symbol="{{ $active->coinValue ? $active->coinValue->symbol : '' }}">{{ $active->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <h3>@lang('site.page.form-investor-loan-month')</h3>
                            <div class="form-group">
                                <input type="text" class="RangeSliderMonth" name="RangeSliderMonth" value="" id="RangeSliderMonth">
                            </div>
                            <h3>@lang('site.page.cabinet.Deposit-amount'):</h3>
                            <div class="form-group">
                                <input type="text" class="RangeSliderMoney1" name="RangeSliderMoney1" value="" id="RangeSliderMoney">
                            </div>
                            <input type="text" class="RangeSliderMoney11" name="RangeSliderMoney11" id="RangeSliderMoney11" value="50">
                        </div>
                        <div class="BoxInvesMaterial">
                            <div class="HeadInvest">@lang('site.page.calc-investor-Profit-calculation-form')</div>
{{--                            <div class="TextInvest1">@lang('site.page.calc-investor-Deposit-rank')</div>--}}
{{--                            <div class="TextInvest2 LineBottom"><span id="depositLevelText">Бронза</span></div>--}}
                            <div class="TextInvest1">@lang('site.page.cabinet.inv-Deposit-amount')</div>
                            <div class="TextInvest2 LineBottom">
                                <span id="deposit_value">50</span> € = <span id="deposit_value_dai" data-course="{{ $activeFirst->coinValue->price }}">50/{{ $activeFirst->coinValue->price }}</span>
                                <span class="deposit_value_symbol">{{ $activeFirst->coinValue->symbol ?? '' }}</span>
                            </div>
                            <div class="TextInvest1">@lang('site.page.cabinet.Deposit-term')</div>
                            <div class="TextInvest2 LineBottom"><span id="deposit_month">45</span> @lang('site.page.cabinet.months')</div>
                            <div class="TextInvest1">@lang('site.page.calc-investor-annual-interest-rate')</div>
                            <div class="TextInvest2 LineBottom"><span id="percent">5</span>%</div>
                            <div class="TextInvest1">@lang('site.page.cabinet.Amount-to-be-paid')</div>
                            <div class="TextInvest2"><span id="amoun_pay">1000</span> <span class="deposit_value_symbol">{{ $activeFirst->coinValue->symbol ?? '' }}</div>
                        </div>
                        @if($user->is_verification && $investorRankVerification && !$user->isBlock())
                            <div class="text-center MarginBot25">
                                <a href="#" class="btns-orange" data-toggle="modal" data-target="#ActiveInvestorLine" id="btnActiveInvestorLine">@lang('site.page.cabinet.Invest')</a>
                            </div>
                        @endif
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="BoxCalculator">
                            <h3>@lang('site.page.cabinet.investor-asset2'):</h3>
                            <div class="form-group">
                                <select id="asset-natural2" {{ $user->isBlock() ? 'disabled' : ''}}>
                                    @foreach($actives as $active)
                                        <option value="{{ $active->id }}" data-price="{{ $active->coinValue ? $active->coinValue->price : 0 }}" data-symbol="{{ $active->coinValue ? $active->coinValue->symbol : '' }}">{{ $active->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <h3>@lang('site.page.form-investor-loan-month2')</h3>
                            <div class="form-group">
                                <input type="text" class="RangeSliderMonth" name="RangeSliderMonth2" value="" id="RangeSliderMonth2">
                            </div>
                            <h3>@lang('site.page.cabinet.Deposit-amount'):</h3>
                            <div class="form-group">
                                <input type="text" class="RangeSliderMoneyDai2" name="RangeSliderMoney2" value="" id="RangeSliderMoney2">
                            </div>
                            <input type="text" class="RangeSliderMoney21" name="RangeSliderMoney21" id="RangeSliderMoney21" value="0">
                        </div>
                        <div class="BoxInvesMaterial">
                            <div class="HeadInvest">@lang('site.page.calc-investor-Profit-calculation-form')</div>
{{--                            <div class="TextInvest1">@lang('site.page.calc-investor-Deposit-rank')</div>--}}
{{--                            <div class="TextInvest2 LineBottom"><span id="depositLevelText2">Бронза</span></div>--}}
                            <div class="TextInvest1">@lang('site.page.cabinet.inv-Deposit-amount')</div>
                            <div class="TextInvest2 LineBottom">
                                <span id="deposit_value_dai2" data-course="{{ $activeFirst->coinValue->price }}">50</span> <span class="deposit_value_symbol2">{{ $activeFirst->coinValue->symbol ?? '' }}</span>
                            </div>
                            <div class="TextInvest1">@lang('site.page.cabinet.Deposit-term')</div>
                            <div class="TextInvest2 LineBottom"><span id="deposit_month2">45</span> @lang('site.page.cabinet.months')</div>
                            <div class="TextInvest1">@lang('site.page.calc-investor-annual-interest-rate')</div>
                            <div class="TextInvest2 LineBottom"><span id="percent2">5</span>%</div>
                            <div class="TextInvest1">@lang('site.page.cabinet.Amount-to-be-paid')</div>
                            <div class="TextInvest2"><span id="amoun_pay2">1000</span> <span class="deposit_value_symbol2">{{ $activeFirst->coinValue->symbol ?? '' }}</span></div>
                        </div>
                        @if($user->is_verification && $investorRankVerification && !$user->isBlock())
                            <div class="text-center MarginBot25">
                                <a href="#" class="btns-orange" data-toggle="modal" data-target="#ActiveInvestorLineDai" id="btnActiveInvestorLineDai">@lang('site.page.cabinet.Invest')</a>
                            </div>
                        @endif
                    </div>
                </div>

                @if($activeFirsts->isNotEmpty() || $activeSeconds->isNotEmpty())
                    <h3 class="MarginTop30">@lang('site.page.cabinet.Calculation-form')</h3>
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        @if($activeFirsts->isNotEmpty())
                                <div class="BoxGrap" id="graph-wrap1">
                                    <div class="row">
                                        <div class="col-6">
                                            <h3>{{ $activeFirsts->first()->coinValue ? $activeFirsts->first()->coinValue->symbol : ''}}</h3>
                                        </div>
                                        <div class="col-6 text-right">
                                            <span></span>
                                        </div>
                                    </div>

                                    <div style="height:250px">
                                        <canvas id="GraphHomeCab1"></canvas>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            @if($activeSeconds->isNotEmpty())
                                <div class="BoxGrap" id="graph-wrap2">
                                    <div class="row">
                                        <div class="col-6">
                                            <h3>{{ $activeSeconds->first()->coinValue ? $activeSeconds->first()->coinValue->symbol : ''}}</h3>
                                        </div>
                                        <div class="col-6 text-right">
                                            <span></span>
                                        </div>
                                    </div>
                                    <div style="height:250px">
                                        <canvas id="GraphHomeCab2"></canvas>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if($user->isBlock())
        <script>
            $(document).ready(function(){
                $("#BlockUser").modal("show");
            });
        </script>
    @endif

    @if(request()->has('q1'))
        <script>
            $(document).ready(function(){
                $("#InvestorRank1").modal("show");
            });
        </script>
    @endif
    @if(request()->has('q2'))
        <script>
            $(document).ready(function(){
                $("#InvestorRank2").modal("show");
            });
        </script>
    @endif
    @if(request()->has('q3'))
        <script>
            $(document).ready(function(){
                $("#InvestorRank3").modal("show");
            });
        </script>
    @endif
    @if(request()->has('q4'))
        <script>
            $(document).ready(function(){
                $("#InvestorRank4").modal("show");
            });
        </script>
    @endif
    @if(request()->has('q5'))
        <script>
            $(document).ready(function(){
                $("#InvestorRank5").modal("show");
            });
        </script>
    @endif
    @if ($errors->has('agree') || $errors->has('amount') || $errors->has('period') || $errors->has('rate'))
        <script>
            $(document).ready(function(){
                $("#ActiveInvestorLine").modal("show");
            });
        </script>
    @endif

    <script src="/js/Chart.min.js"></script>

    @if($activeFirsts->first() && $activeFirsts->first()->coinValue && $activeFirsts->first()->coinValue->ohlcv_x && $activeFirsts->first()->coinValue->ohlcv_y)
        <script>
            var Chart1 = setGraphic("GraphHomeCab1", {!! $activeFirsts->first()->coinValue->ohlcv_y !!}, {!! $activeFirsts->first()->coinValue->ohlcv_x !!}, 'rgba(247,147,26,.5)', 'rgba(247,147,26, 1)', true, true);
        </script>
    @endif

    @if($activeSeconds->first() && $activeSeconds->first()->coinValue && $activeSeconds->first()->coinValue->ohlcv_x && $activeSeconds->first()->coinValue->ohlcv_y)
        <script>
            var Chart2 = setGraphic("GraphHomeCab2", {!! $activeSeconds->first()->coinValue->ohlcv_y !!}, {!! $activeSeconds->first()->coinValue->ohlcv_x !!}, 'rgba(101, 164, 94, .5)', 'rgba(101, 164, 94, 1)', true, true);
        </script>
    @endif

    <script type="text/javascript">

        $(".RangeSliderMoneyDai2").ionRangeSlider({
            grid: true,
            skin: "big",
            min: 0,
            max: 10000,
            from: 0,
            step: 0.1,
            postfix: " {{ $activeFirst->coinValue->symbol ?? '' }}"
        });

        $('#asset-natural').on('change', function () {
            setValueCalc();
        });


        $('#asset-natural2').on('change', function () {
            $('#RangeSliderMoney2').data("ionRangeSlider").update({
                postfix: $(this).find(':selected').data('symbol')
            });

            setValueCalc2();
        });

        function printDocument() {
            $.ajax({
                url: "{{ route('front.cabinet.investor.home.investorDocumentPdf') }}",
                method: 'POST',
                data: {text: $('#Text-Dogovor').html()}
            }).done(function(response){
                var win = window.open(response, '_blank');
                win.focus();
            });
        }

        function printDocumentDai() {
            $.ajax({
                url: "{{ route('front.cabinet.investor.home.investorDocumentPdf') }}",
                method: 'POST',
                data: {text: $('#d_Text-Dogovor').html()}
            }).done(function(response){
                var win = window.open(response, '_blank');
                win.focus();
            });
        }

        function getMonthPercent() {
            $.ajax({
                url: "{{ route('front.cabinet.investor.home.getMonthPercent') }}",
                method: 'POST',
                data: {
                    asset: $('#asset-natural').val(),
                    amount: $('#amount').val(),
                    period: $('#RangeSliderMonth').val()
                },
            }).done(function(response){
                if(response.hasOwnProperty('percent')){
                    $('#percent').text(response.percent);
                    $('#rate_text').text(response.percent);
                    $('#rate').val(response.percent);
                }
            });
        }

        function getMonthPercent2() {
            $.ajax({
                url: "{{ route('front.cabinet.investor.home.getMonthPercent') }}",
                method: 'POST',
                data: {
                    asset: $('#asset-natural2').val(),
                    amount: $('#d_amount').val(),
                    period: $('#RangeSliderMonth2').val()
                },
            }).done(function(response){
                if(response.hasOwnProperty('percent')){
                    $('#percent2').text(response.percent);
                    $('#d_rate_text').text(response.percent);
                    $('#d_rate').val(response.percent);
                }
            });
        }

        //Calculator
        function calc(amount, month, percent) {
            let value = amount * (1 + (percent * month)/(12 * 100));
            return value.toFixed(2);
        }

        function calc2(amount, month, percent) {
            let value = amount * (1 + (percent * month)/(12 * 100));
            return value.toFixed(8);
        }

        function setValueCalc() {
            getMonthPercent();

            if ($('#RangeSliderMoney').val() * 1 > 0) {
                $('#RangeSliderMoney11').val($('#RangeSliderMoney').val() * 1);
            }

            let value_money = $('#asset-natural').find(':selected').data('price')*1;
            let value_symbol = $('#asset-natural').find(':selected').data('symbol');
            $('.deposit_value_symbol').text(value_symbol);
            $('#type_asset_id').val($('#asset-natural').val());

            let value = calc2(
                ($('#RangeSliderMoney').val() / value_money).toFixed(8),
                $('#RangeSliderMonth').val(),
                $('#percent').text() * 1
            );
            $('#deposit_value').text($('#RangeSliderMoney').val());
            $('#deposit_value_dai').text(($('#RangeSliderMoney').val() / value_money).toFixed(8));
            $('#amoun_pay').text(value);
            $('#deposit_month').text($('#RangeSliderMonth').val());

            $('#amount_text').text($('#RangeSliderMoney').val());
            $('#amount_text_dai').text(($('#RangeSliderMoney').val()*1/value_money).toFixed(8));
            $('#amount').val($('#RangeSliderMoney').val());
            $('#amount_text2_dai').text(value);

            $('#period_text').text($('#RangeSliderMonth').val());
            $('#period').val($('#RangeSliderMonth').val());

            $.ajax({
                type: "POST",
                url: '{{ route('front.cabinet.investor.home.graphInvestor') }}',
                data: {asset:$('#asset-natural').val(), color:1},
                success: function (res) {
                    Chart1.destroy();
                    $('#graph-wrap1 h3').html(res.symbol)
                    $('#graph-wrap1 span').html((res.ohlcvY[res.ohlcvY.length-1]).toFixed(2))
                    $('#GraphHomeCab1').html('')
                    if(res.color == 1){
                        Chart1 = setGraphic('GraphHomeCab1', res.ohlcvY, res.ohlcvX, 'rgba(247,147,26,.5)', 'rgba(247,147,26, 1)', true, true);
                    } else {
                        Chart1 = setGraphic('GraphHomeCab1', res.ohlcvY, res.ohlcvX, 'rgba(101, 164, 94,.5)', 'rgba(101, 164, 94, 1)', true, true);
                    }
                }
            });
            {{--$.ajax({--}}
            {{--    type: "POST",--}}
            {{--    url: '{{ route('front.home.calcInvest') }}',--}}
            {{--    data: {amount: $('#RangeSliderMoney').val()},--}}
            {{--    success: function (result) {--}}
            {{--        let value_money = $('#asset-natural').find(':selected').data('price')*1;--}}
            {{--        let value_symbol = $('#asset-natural').find(':selected').data('symbol');--}}
            {{--        $('.deposit_value_symbol').text(value_symbol);--}}
            {{--        $('#type_asset_id').val($('#asset-natural').val());--}}

            {{--        if(result.hasOwnProperty('name')){--}}
            {{--            let value = calc($('#RangeSliderMoney').val(), $('#RangeSliderMonth').val(), result.value * 1);--}}
            {{--            $('#percent').text(result.value);--}}
            {{--            $('#deposit_value').text($('#RangeSliderMoney').val());--}}
            {{--            $('#deposit_value_dai').text(($('#RangeSliderMoney').val() / value_money).toFixed(8));--}}
            {{--            $('#amoun_pay').text(value);--}}
            {{--            $('#deposit_month').text($('#RangeSliderMonth').val());--}}
            {{--            //$('#depositLevelText').text(result.name);--}}

            {{--            $('#amount_text').text($('#RangeSliderMoney').val());--}}
            {{--            $('#amount_text_dai').text(($('#RangeSliderMoney').val()*1/value_money).toFixed(8));--}}
            {{--            $('#amount').val($('#RangeSliderMoney').val());--}}
            {{--            $('#amount_text2').text(value);--}}
            {{--            $('#amount_text2_dai').text((value/value_money).toFixed(8));--}}

            {{--            $('#period_text').text($('#RangeSliderMonth').val());--}}
            {{--            $('#period').val($('#RangeSliderMonth').val());--}}

            {{--            $('#rate_text').text(result.value);--}}
            {{--            $('#rate').val(result.value);--}}
            {{--        }else{--}}
            {{--            let value = calc($('#RangeSliderMoney').val(), $('#RangeSliderMonth').val(), $('#percent').text() * 1);--}}
            {{--            $('#deposit_value').text($('#RangeSliderMoney').val());--}}
            {{--            $('#deposit_value_dai').text(($('#RangeSliderMoney').val() / value_money).toFixed(8));--}}
            {{--            $('#amoun_pay').text(value);--}}
            {{--            $('#deposit_month').text($('#RangeSliderMonth').val());--}}

            {{--            $('#amount_text').text($('#RangeSliderMoney').val());--}}
            {{--            $('#amount_text_dai').text(($('#RangeSliderMoney').val()*1/value_money).toFixed(8));--}}
            {{--            $('#amount').val($('#RangeSliderMoney').val());--}}
            {{--            $('#amount_text2_dai').text((value*1/value_money).toFixed(8));--}}

            {{--            $('#period_text').text($('#RangeSliderMonth').val());--}}
            {{--            $('#period').val($('#RangeSliderMonth').val());--}}
            {{--        }--}}
            {{--    }--}}
            {{--});--}}
        }

        function setValueCalc2() {
            getMonthPercent2()

            if ($('#RangeSliderMoney2').val() * 1 > 0) {
                $('#RangeSliderMoney21').val($('#RangeSliderMoney2').val() * 1);
            }

            let value_money = $('#asset-natural2').find(':selected').data('price')*1;
            let value_symbol = $('#asset-natural2').find(':selected').data('symbol');
            $('.deposit_value_symbol2').text(value_symbol);
            $('#type_asset_id2').val($('#asset-natural2').val());

            let value = calc2($('#RangeSliderMoney2').val(), $('#RangeSliderMonth2').val(), $('#percent2').text() * 1);
            $('#deposit_value_dai2').text($('#RangeSliderMoney2').val());
            $('#amoun_pay2').text(value);
            $('#deposit_month2').text($('#RangeSliderMonth2').val());

            $('#d_amount').val($('#RangeSliderMoney2').val());
            $('#d_amount_text_dai').text($('#RangeSliderMoney2').val());
            $('#d_amount_text2_dai').text(value);

            $('#d_period_text').text($('#RangeSliderMonth2').val());
            $('#d_period').val($('#RangeSliderMonth2').val());

            $.ajax({
                type: "POST",
                url: '{{ route('front.cabinet.investor.home.graphInvestor') }}',
                data: {asset:$('#asset-natural2').val(), color:1},
                success: function (res) {
                    Chart2.destroy();
                    $('#graph-wrap2 h3').html(res.symbol)
                    $('#graph-wrap2 span').html((res.ohlcvY[res.ohlcvY.length-1]).toFixed(2))
                    $('#GraphHomeCab2').html('')
                    if(res.color == 1){
                        Chart2 = setGraphic('GraphHomeCab2', res.ohlcvY, res.ohlcvX, 'rgba(247,147,26,.5)', 'rgba(247,147,26, 1)', true, true);
                    } else {
                        Chart2 = setGraphic('GraphHomeCab2', res.ohlcvY, res.ohlcvX, 'rgba(101, 164, 94,.5)', 'rgba(101, 164, 94, 1)', true, true);
                    }
                }
            });
            {{--$.ajax({--}}
            {{--    type: "POST",--}}
            {{--    url: '{{ route('front.home.calcInvest') }}',--}}
            {{--    data: {dai: 1, amount: $('#RangeSliderMoney21').val()},--}}
            {{--    success: function (result) {--}}
            {{--        if (result.hasOwnProperty('name')) {--}}
            {{--            let value = calc2($('#RangeSliderMoney2').val(), $('#RangeSliderMonth2').val(), result.value * 1);--}}
            {{--            $('#percent2').text(result.value);--}}
            {{--            $('#deposit_value_dai2').text($('#RangeSliderMoney2').val());--}}
            {{--            $('#amoun_pay2').text(value);--}}
            {{--            $('#deposit_month2').text($('#RangeSliderMonth2').val());--}}
            {{--            //$('#depositLevelText2').text(result.name);--}}

            {{--            $('#d_amount').val($('#RangeSliderMoney2').val());--}}
            {{--            $('#d_amount_text_dai').text($('#RangeSliderMoney2').val());--}}
            {{--            $('#d_amount_text2_dai').text(value);--}}

            {{--            $('#d_period_text').text($('#RangeSliderMonth2').val());--}}
            {{--            $('#d_period').val($('#RangeSliderMonth2').val());--}}

            {{--            $('#d_rate_text').text(result.value);--}}
            {{--            $('#d_rate').val(result.value);--}}
            {{--        }else{--}}
            {{--            let value = calc2($('#RangeSliderMoney2').val(), $('#RangeSliderMonth2').val(), $('#percent2').text() * 1);--}}
            {{--            $('#deposit_value_dai2').text($('#RangeSliderMoney2').val());--}}
            {{--            $('#amoun_pay2').text(value);--}}
            {{--            $('#deposit_month2').text($('#RangeSliderMonth2').val());--}}

            {{--            $('#d_amount').val($('#RangeSliderMoney2').val());--}}
            {{--            $('#d_amount_text_dai').text($('#RangeSliderMoney2').val());--}}
            {{--            $('#d_amount_text2_dai').text(value);--}}

            {{--            $('#d_period_text').text($('#RangeSliderMonth2').val());--}}
            {{--            $('#d_period').val($('#RangeSliderMonth2').val());--}}
            {{--        }--}}
            {{--    }--}}
            {{--});--}}
        }


        $(document).ready(function(){
            $('#RangeSliderMonth').data("ionRangeSlider").update({
                onFinish: function (data) {
                    setValueCalc();
                },
            });
            $('#RangeSliderMoney').data("ionRangeSlider").update({
                onFinish: function (data) {
                    setValueCalc();
                },
            });

            $('#RangeSliderMoney11').on('change', function () {
                $('#RangeSliderMoney').data("ionRangeSlider").update({
                    from: $(this).val()
                });
                setValueCalc();
            });

            $('#RangeSliderMonth2').data("ionRangeSlider").update({
                onFinish: function (data) {
                    setValueCalc2();
                },
            });
            $('#RangeSliderMoney2').data("ionRangeSlider").update({
                onFinish: function (data) {
                    setValueCalc2();
                },
            });

            $('#RangeSliderMoney21').on('change', function () {
                $('#RangeSliderMoney2').data("ionRangeSlider").update({
                    from: $(this).val()
                });

                setValueCalc2();
            });

            setValueCalc();
            setValueCalc2();

            $('#btnActiveInvestorLine').on('click', function(e){
                setValueCalc();

                $.ajax({
                    type: "POST",
                    url: '{{ route('front.cabinet.investor.home.investorDocument') }}',
                    data: {
                        period: $('#period').val(),
                        amount: $('#amount').val(),
                        rate: $('#rate').val(),
                        asset: $('#asset-natural').val()
                    },
                    success: function (result) {
                        if(result.hasOwnProperty('text')){
                            $('#Text-Dogovor').html(result.text);
                        }
                    }
                });
            });

            $('#btnActiveInvestorLineDai').on('click', function(e){
                $.ajax({
                    type: "POST",
                    url: '{{ route('front.cabinet.investor.home.investorDocumentDai') }}',
                    data: {
                        period: $('#d_period').val(),
                        amount: $('#d_amount').val(),
                        rate: $('#d_rate').val(),
                        asset: $('#asset-natural2').val()
                    },
                    success: function (result) {
                        if(result.hasOwnProperty('text')){
                            $('#d_Text-Dogovor').html(result.text);
                        }
                    }
                });
            });
        });
    </script>

    <script type="text/javascript">
        function CopyReff() {
            var copyText = document.getElementById("CopyReff");
            copyText.select();
            document.execCommand("copy");
            alert("Copied the text: " + copyText.value);
        }
    </script>
@endsection
