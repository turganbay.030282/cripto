<!-- Modal ActiveInvestorLine -->
<div class="modal HeaderBlue" id="ActiveInvestorLineDai" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.page.cabinet.Conclusion-Investor-Agreement')</h2>
                {!! Form::open(['url' => route('front.cabinet.investor.home.contractDai'), 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                    <div class="BoxPaddActive">
                        <div class="row">
                            <div class="col-xl-7 col-6">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.cabinet.inv-Deposit-amount'):</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-6">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    <h6><strong id="d_amount_text_dai">{{ old('amount') }}</strong> <span class="deposit_value_symbol2"></span></h6>
                                    <input type="hidden" name="amount" id="d_amount" value="{{ old('amount') }}">

                                    <input type="hidden" name="type_asset_id" id="type_asset_id2" value="{{ old('type_asset_id') }}">
                                    {!! Form::error('type_asset_id') !!}
                                </div>
                            </div>
                            <div class="col-xl-7 col-6">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.cabinet.Deposit-term'):</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-6">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    <h6><span id="d_period_text">{{ old('period') }}</span> @lang('site.page.cabinet.months')</h6>
                                    <input type="hidden" name="period" id="d_period" value="{{ old('period') }}">
                                    {!! Form::error('period') !!}
                                </div>
                            </div>
                            <div class="col-xl-7 col-6">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.calc-trader-annual-interest-rate'):</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-6">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    <h6><span id="d_rate_text">{{ old('rate') }}</span>%</h6>
                                    <input type="hidden" name="rate" id="d_rate" value="{{ old('rate') }}">
                                    {!! Form::error('rate') !!}
                                </div>
                            </div>
                            <div class="col-xl-7 col-6">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.cabinet.Amount-to-be-paid'):</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-6">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    <h6><strong id="d_amount_text2_dai">{{ old('amount') }}</strong><span class="deposit_value_symbol2"></span></h6>
                                </div>
                            </div>
                        </div>
                        <h6>@lang('site.page.cabinet.Text-Agreement'):</h6>
                        <div class="TextDogovorBox">
                            <div class="TextDogovor" id="d_Text-Dogovor"></div>
                            <div class="PrintBTNS">
                                <button type="button" class="btns-green" onclick="printDocumentDai();return;">@lang('site.page.cabinet.PRINT')</button>
                            </div>
                        </div>
                        <div class="form-group MarginTop30">
                            <label>
                                <input type="checkbox" class="Mycheckbox" name="agree" required>
                                @lang('site.page.cabinet.Agree-data-processing')
                            </label>
                            {!! Form::error('agree') !!}
                        </div>
                        <div class="form-group text-center MarginBot25">
                            <button class="btns-green send-sms">@lang('site.page.cabinet.Subscribe')</button>
                        </div>
                        <h5>@lang('site.page.cabinet.Check-your-mobile-device') {{ auth()->user()->phone }}</h5>
                        <div class="row">
                            <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.cabinet.Your-verification-code')</h6>
                                </div>
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <input type="text" placeholder="7 - @lang('site.page.cabinet.digit-code')" name="verify_code" class="verify_code" required>
                                    {!! Form::error('verify_code') !!}
                                </div>
                            </div>
                        </div>
                        @if(auth()->user()->google2fa_enable)
                            <div class="row">
                                <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="LeftFormText d-flex align-items-center">
                                        <h6>@lang('site.cabinet.2gaf-title-verify-code')</h6>
                                    </div>
                                </div>
                                <div class="col-xl-7 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="form-group">
                                        <input type="text" placeholder="@lang("site.cabinet.2gaf-placeholder-verify-code")" name="g_verify_code" class="g_verify_code" required>
                                        {!! Form::error('g_verify_code') !!}
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="form-group text-center MarginTop30">
                            <button class="btns-gray" data-dismiss="modal" aria-label="Close">@lang('site.page.cabinet.btn-Refuse')</button>
                            <button class="btns-green" type="submit">@lang('site.page.cabinet.btn-Confirm')</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
