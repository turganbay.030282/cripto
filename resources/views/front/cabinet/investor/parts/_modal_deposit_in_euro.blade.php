<div class="modal HeaderBlue" id="DepositInEuro" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">{{ trans('site.page.form-payment-title') }}</h2>
                {!! Form::open(['url' => route('front.cabinet.investor.deposit.payIn'), 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                <div class="BoxPaddActive">
                    <div class="row">
                        <div class="col-5">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>{{ trans('site.page.form-payment-amount') }}</h6>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="RightFormText d-flex align-items-center">
                                <input type="text" value="{{ old('amount') }}" name="amount" required>
                                {{ Form::error('amount') }}
                            </div>
                        </div>
                    </div>
                    <h6 class="Bold MarginTop30">{{ trans('site.page.form-payment-number-pay') }}:</h6>
                    <div class="LeftFormText MarginBot25">
                        <div class="IcoCopy" onclick="CopyToBuffer3();">
                            <input type="text" value="{{ auth()->user()->deposit_euro_number }}" id="copy-buffer3" readonly>
                            <span></span>
                        </div>
                    </div>
                    <h6 class="Bold MarginTop30">{{ trans('site.page.form-payment-iban') }}:</h6>
                    <div class="LeftFormText MarginBot25">
                        <div class="IcoCopy" onclick="CopyToBuffer4();">
                            <input type="text" value="{{ trans_field($setting, 'req_iban') }}" id="copy-buffer4">
                            <span></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>{{ trans('site.page.form-payment-name') }}:</h6>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="RightFormText d-flex align-items-center">
                                <h6>{{ trans_field($setting, 'req_sia') }}</h6>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>{{ trans('site.page.form-payment-country-bank') }}</h6>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="RightFormText d-flex align-items-center">
                                <h6>Estonia</h6>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>{{ trans('site.page.form-payment-name-bank') }}</h6>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="RightFormText d-flex align-items-center">
                                <h6>{{ trans_field($setting, 'req_seb') }}</h6>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>{{ trans('site.page.form-payment-address-delivery') }}</h6>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="RightFormText d-flex align-items-center">
                                <h6>{{ trans_field($setting, 'req_reg') }}</h6>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>{{ trans('site.page.form-payment-swift') }}</h6>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="RightFormText d-flex align-items-center">
                                <h6>{{ trans_field($setting, 'req_swift') }}</h6>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>{{ trans('site.page.form-payment-bank_address') }}</h6>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="RightFormText d-flex align-items-center">
                                <h6>{{ trans_field($setting, 'req_bank_address') }}</h6>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.contract-pay-ticket'):</h6>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="RightFormText d-flex align-items-center">
                                <input type="file" id="ticket-file" name="pay_ticket" required data-placeholder="{{ trans('site.cabinet.form-browse-file') }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center MarginTop30 MarginBot25">
                        <button class="btns-green send-sms-pay">@lang('site.page.cabinet.Subscribe')</button>
                    </div>
                    <h5>@lang('site.page.cabinet.Check-your-mobile-device') {{ auth()->user()->phone }}</h5>
                    <div class="row">
                        <div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.cabinet.Your-verification-code')</h6>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <input type="text" placeholder="7 - @lang('site.page.cabinet.digit-code')" name="verify_code" class="verify_code" required value="{{ old('verify_code') }}">
                                {!! Form::error('verify_code') !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center MarginTop15 BTNWidth100">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="ResponsiveMarginBot15">
                                    <button class="btns-red" data-dismiss="modal" aria-label="Close">{{ trans('site.page.cabinet.Cancel') }}</button>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <button class="btns-green" id="send-pay-ticket" type="submit">{{ trans('site.page.cabinet.Pay') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('script-modals')
    <script>
        $('.send-sms-pay').on('click', function (e) {
            e.preventDefault();
            var el = $(this);
            $.ajax({
                url: '{{ (route('front.cabinet.investor.deposit.requestPhoneDeposit')) }}',
                success: function (result) {
                    if (result.hasOwnProperty('error')) {
                        el.closest('div').append('<span class="invalid-feedback" style="color: red;display: block;"><strong>'+result.error+'</strong></span>');
                        setTimeout(function(){
                            el.closest('div').find('.invalid-feedback').remove();
                        },10000);
                        return;
                    }
                    el.closest('div').addClass('hide');
                }
            });
        });
    </script>
@endsection
