<!-- Modal CreditLine -->
<div class="modal" id="DepositLine" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">Заявка на внесение депозита</h2>
                {!! Form::open(['url' => route('front.cabinet.investor.home.depositLine'), 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                    <div class="row">

                        <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>Сумма депозита</h6>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                {!! Form::text('amount', null, ['required' => true]) !!}
                                {!! Form::error('amount') !!}
                            </div>
                        </div>

                    </div>
                    <div class="form-group text-center">
                        {!! Form::button(trans('site.page.form-btn-send'), ['type' => 'submit', 'class' => 'btns-green']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
