<!-- Modal InvestFizQuestionnaire1 -->
<div class="modal" id="InvestorRank1" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-700 modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.page.investor.anket-title')</h2>
                <ul class="StepsQuest StepsFives">
                    <li class="active">1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>4</li>
                </ul>
                <h6 class="Bold">@lang('site.page.investor.anket-phiz-title1')</h6>
                <div class="BoxPaddActive">
                    {!! Form::open(['url' => route('front.cabinet.investor.investRanks.investorRankFirst')]) !!}
                        <div class="row">
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.investor.anket-full_name')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('full_name', null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('full_name') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.investor.anket-birthday')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('birthday', null, ['class' => 'picker-date input-required', 'required' => true]) !!}
                                </div>
                                {!! Form::error('birthday') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.cabinet.credit-level-info-gender')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::select('sex', \App\Helpers\ListsHelper::sex(), null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('sex') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.investor.anket-bin')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('bin', null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('bin') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.investor.anket-place_birth')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('place_birth', null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('place_birth') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.investor.anket-county')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center maxheight">
                                    {!! Form::select('county', $countries, null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('county') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.investor.anket-address')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('address', null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('address') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.investor.anket-phone')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {{ Form::text('full_phone', null, ['id' => 'full_phone', 'required' => true]) }}
                                    {{ Form::hidden('phone', null, ['id' => 'reg-phone']) }}
                                </div>
                                <span id="error-msg" class="invalid-feedback" style="color: red;display: none;"><strong>@lang('validation.regex', ['attribute' => 'phone'])</strong></span>
                                {!! Form::error('phone') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.investor.anket-email')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('email', null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('email') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.investor.anket-professional_activities')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('professional_activities', null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('professional_activities') !!}
                            </div>
                        </div>
                        <h6 class="Bold">@lang('site.page.investor.anket-volume-transactions'):</h6>
                        <div class="BoxModalCheck">
                            <div class="LisstMCH">
                                <label>
                                    {!! Form::hidden('volume_1', 0) !!}
                                    {!! Form::checkbox('volume_1', 1, null, ['class' => 'MycheckboxModal']) !!}
                                    <span>@lang('site.page.investor.anket-volume1')</span>
                                </label>
                            </div>
                            <div class="LisstMCH">
                                <label>
                                    {!! Form::hidden('volume_2', 0) !!}
                                    {!! Form::checkbox('volume_2', 1, null, ['class' => 'MycheckboxModal']) !!}
                                    <span>@lang('site.page.investor.anket-volume2')</span>
                                </label>
                            </div>
                            <div class="LisstMCH">
                                <label>
                                    {!! Form::hidden('volume_3', 0) !!}
                                    {!! Form::checkbox('volume_3', 1, null, ['class' => 'MycheckboxModal']) !!}
                                    <span>@lang('site.page.investor.anket-volume3')</span>
                                </label>
                            </div>
                            <div class="LisstMCH">
                                <label>
                                    {!! Form::hidden('volume_4', 0) !!}
                                    {!! Form::checkbox('volume_4', 1, null, ['class' => 'MycheckboxModal']) !!}
                                    <span>@lang('site.page.investor.anket-volume4')</span>
                                </label>
                            </div>
                            <div class="LisstMCH">
                                <label>
                                    {!! Form::hidden('volume_5', 0) !!}
                                    {!! Form::checkbox('volume_5', 1, null, ['class' => 'MycheckboxModal']) !!}
                                    <span>@lang('site.page.investor.anket-volume5')</span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="offset-xl-7 col-xl-5 offset-lg-6 col-lg-6 col-md-12 col-12">
                                <div class="BTNWidth100">
                                    {!! Form::button(trans('site.cabinet.credit-level-info-next-step'), ['type' => 'submit', 'class' => 'btns-green']) !!}
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal InvestFizQuestionnaire2 -->
<div class="modal" id="InvestorRank2" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-700 modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.page.investor.anket-title')</h2>
                <ul class="StepsQuest StepsFives">
                    <li class="done">1</li>
                    <li class="done">2</li>
                    <li class="active">3</li>
                    <li>4</li>
                </ul>
                {!! Form::open(['url' => route('front.cabinet.investor.investRanks.investorRankSecond')]) !!}
                    <h6 class="Bold">@lang('site.page.investor.anket-title-source_business_activities'):</h6>
                    <div class="BoxPaddActive">
                    <div class="InpChe">
                        <label>
                            {!! Form::hidden('source_business_activities', 0) !!}
                            {!! Form::checkbox('source_business_activities', 1, null, ['class' => 'MycheckboxModal']) !!}
                            <span>@lang('site.page.investor.anket-source_business_activities')</span>
                        </label>
                    </div>
                    <div class="InpChe">
                        <label>
                            {!! Form::hidden('source_dividend', 0) !!}
                            {!! Form::checkbox('source_dividend', 1, null, ['class' => 'MycheckboxModal']) !!}
                            <span>@lang('site.page.investor.anket-source_dividend')</span>
                        </label>
                    </div>
                    <div class="InpChe">
                        <label>
                            {!! Form::hidden('source_loans', 0) !!}
                            {!! Form::checkbox('source_loans', 1, null, ['class' => 'MycheckboxModal']) !!}
                            <span>@lang('site.page.investor.anket-source_loans')</span>
                        </label>
                    </div>
                    <div class="InpChe">
                        <label>
                            {!! Form::hidden('source_income_assets', 0) !!}
                            {!! Form::checkbox('source_income_assets', 1, null, ['class' => 'MycheckboxModal']) !!}
                            <span>@lang('site.page.investor.anket-source_income_assets')</span>
                        </label>
                    </div>
                    <div class="InpChe">
                        <label>
                            {!! Form::hidden('source_contributions', 0) !!}
                            {!! Form::checkbox('source_contributions', 1, null, ['class' => 'MycheckboxModal']) !!}
                            <span>@lang('site.page.investor.anket-source_contributions')</span>
                        </label>
                    </div>
                    <div class="InpChe">
                        <label>
                            {!! Form::hidden('source_prize', 0) !!}
                            {!! Form::checkbox('source_prize', 1, null, ['class' => 'MycheckboxModal']) !!}
                            <span>@lang('site.page.investor.anket-source_prize')</span>
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <div class="InpChe">
                                    <label>
                                        {!! Form::hidden('source_other', 0) !!}
                                        {!! Form::checkbox('source_other', 1, null, ['class' => 'MycheckboxModal', 'id' => 'source_other']) !!}
                                        <span>@lang('site.page.investor.anket-source_other')</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('source_other_text', null, ['id' => 'source_other_text']) !!}
                            </div>
                            {!! Form::error('source_other_text') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.investor.anket-source_country')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center maxheight">
                                {!! Form::select('source_country', $countries, null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('source_country') !!}
                        </div>
                    </div>
                    <h6 class="Bold LineUp">@lang('site.page.investor.anket-data-beneficiary')</h6>
                    <h6>@lang('site.page.investor.anket-data-beneficiary-text'):</h6>
                    <div class="InpChe">
                        <label>
                            {!! Form::hidden('i_beneficiary', 0) !!}
                            {!! Form::checkbox('i_beneficiary', 1, null, ['class' => 'MycheckboxModal', 'id' => 'i_beneficiary']) !!}
                            <span>@lang('site.page.investor.anket-i-agree-beneficiary')</span>
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.investor.anket-real-beneficiary'):</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText">
                                {!! Form::text('beneficiary_name', null, ['id' => 'beneficiary_name']) !!}
                                {!! Form::error('beneficiary_name') !!}
                                <p class="FZ12">@lang('site.page.investor.anket-beneficiary_name')</p>
                            </div>
                        </div>
                        <div class="offset-xl-7 col-xl-5 offset-lg-6 col-lg-6 col-md-12 col-12">
                            <div class="BTNWidth100">
                                {!! Form::button(trans('site.cabinet.credit-level-info-next-step'), ['type' => 'submit', 'class' => 'btns-green']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<!-- Modal InvestFizQuestionnaire3 -->
<div class="modal" id="InvestorRank3" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-700 modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.page.investor.anket-title')</h2>
                <ul class="StepsQuest StepsFives">
                    <li class="done">1</li>
                    <li class="done">2</li>
                    <li class="done">3</li>
                    <li class="active">4</li>
                </ul>
                <h6 class="Bold">@lang('site.page.investor.anket-title3')</h6>
                <div class="BoxPaddActive">
                    {!! Form::open(['url' => route('front.cabinet.investor.investRanks.investorRankFinish')]) !!}
                        <div class="InpChe">
                            <label>
                                {!! Form::hidden('not_pep', 0) !!}
                                {!! Form::checkbox('not_pep', 1, null, ['class' => 'MycheckboxModal']) !!}
                                <span>@lang('site.page.investor.anket-pep')</span>
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6 class="FZ12">@lang('site.page.investor.anket-politics'):</h6>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('politics') !!}
                                </div>
                                {!! Form::error('politics') !!}
                            </div>
                        </div>
                        <h6 class="Bold LineUp">@lang('site.page.investor.anket-send-anket-text')</h6>
                        <h6>{{ \Carbon\Carbon::now()->format('d.m.Y') }}</h6>
                        <div class="ModalText">
                            <p>@lang('site.page.investor.anket-pep1')</p>
                            <p>@lang('site.page.investor.anket-pep2')</p>
                        </div>
                        <div class="text-center">
                            {!! Form::button(trans('site.page.form-btn-send'), ['type' => 'submit', 'class' => 'btns-green']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal InvestYurQuestionnaire4 -->
<div class="modal" id="InvestorRank4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-700 modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.cabinet.credit-level-questionnaire')</h2>
                <ul class="StepsQuest StepsFives">
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="done"><i class="fas fa-check"></i></li>
                </ul>
                <h4 class="Bold text-center PaddingBot50">@lang('site.cabinet.credit-level-Thank-you')</h4>
            </div>
        </div>
    </div>
</div>

@section('style2')
    <link rel="stylesheet" href="/js/intl-tel-input/css/intlTelInput.css">
@endsection

@section('script2')
    <script src="/js/intl-tel-input/js/intlTelInput-jquery.js"></script>
    <script>
        $(document).ready(function(){
            $('#i_beneficiary-styler').on('click', function (e) {
                if($("#i_beneficiary").prop('checked')){
                    $('#beneficiary_name').prop('required', false);
                    $('#beneficiary_name').attr('disabled', true);
                }else{
                    $('#beneficiary_name').prop('required', true);
                    $('#beneficiary_name').attr('disabled', false);
                }
            });
            if($("#source_other").prop('checked')){
                $('#source_other_text').prop('required', true);
                $('#source_other_text').attr('disabled', false);
            }else{
                $('#source_other_text').prop('required', false);
                $('#source_other_text').attr('disabled', true);
            }
            $('#source_other-styler').on('click', function (e) {
                if($("#source_other").prop('checked')){
                    $('#source_other_text').prop('required', true);
                    $('#source_other_text').attr('disabled', false);
                }else{
                    $('#source_other_text').prop('required', false);
                    $('#source_other_text').attr('disabled', true);
                }
            });

            let telInput = $("#full_phone")
            telInput.intlTelInput({
                initialCountry: "auto",
                separateDialCode:true,
                autoHideDialCode:false,
                preferredCountries: ['ee'],
                excludeCountries: ['kp', 'ir', 'af', 'bs', 'bb', 'bw', 'kh', 'gh', 'iq',
                    'jm', 'mu', 'mm', 'ni', 'pk', 'pa', 'sy', 'tt', 'ug', 'vu', 'ye', 'zw',
                    'al', 'is', 'mn'],
                geoIpLookup: function(success, failure) {
                    $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "ee";
                        success(countryCode);
                    });
                },
                utilsScript: "/js/intl-tel-input/js/utils.js"
            });
            telInput.blur(function () {
                if ($.trim(telInput.val())) {
                    if (telInput.intlTelInput('isValidNumber')) {
                        $('#error-msg').hide();
                        $('#reg-phone').val(telInput.intlTelInput('getNumber'));
                    } else {
                        $('#error-msg').show();
                    }
                }
            });
            telInput.keydown(function () {
                $('#error-msg').hide();
            });
        });
    </script>
@endsection
