<!-- Modal DepositOut -->
<div class="modal HeaderBlue" id="PayIn{{ $investorAsset->coinValue->symbol ?? '' }}" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.page.cabinet.inv-payin-dai')</h2>
                {!! Form::open(['url' => route('front.cabinet.investor.deposit.payInDai'), 'method' => 'POST']) !!}
                    <div class="BoxPaddActive">
                        <div class="row">

                            <div class="col-xl-3 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.cabinet.inv-payin-field-from'):</h6>
                                </div>
                            </div>
                            <div class="col-xl-9 col-12">
                                <div class="form-group">
                                    @if($investorAsset->wallet_img)
                                        <img src="{{ $investorAsset->getPhotoUrl('wallet_img') }}" alt="qr-code" style="width:150px;height:150px">
                                    @endif
                                    <input name="type_asset_id" type="hidden" value="{{ $investorAsset->id }}">
                                </div>
                                <div class="form-group">
                                    <div class="IcoCopy" onclick="CopyToBufferFromInv{{ $investorAsset->id }}();">
                                        <input name="to" type="text" value="{{ $investorAsset->wallet_number }}" id="copy-inv-from-{{ $investorAsset->id }}" required>
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.cabinet.inv-payin-field-to'):</h6>
                                </div>
                            </div>
                            <div class="col-xl-9 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    <input type="text" name="from" required>
                                </div>
                            </div>
                            <div class="col-xl-3 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.cabinet.inv-payin-field-amount'):</h6>
                                </div>
                            </div>
                            <div class="col-xl-9 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    <input type="text" name="amount" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group text-center MarginTop30">
                            <button class="btns-red" data-dismiss="modal" aria-label="Close">@lang('site.page.cabinet.Cancel')</button>
                            <button class="btns-green">@lang('site.page.cabinet.Request')</button>
                        </div>

                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<script>
    function CopyToBufferFromInv{{ $investorAsset->id }}() {
        var copyText = document.getElementById("copy-inv-from-{{ $investorAsset->id }}");
        copyText.select();
        document.execCommand("copy");
        alert("Copied the text: " + copyText.value);
    }

</script>
