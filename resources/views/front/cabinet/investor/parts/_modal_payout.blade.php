<!-- Modal DepositOut -->
<div class="modal HeaderBlue" id="DepositOut" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.page.cabinet.Withdraw-funds')</h2>
                <form>
                    <div class="BoxPaddActive">
                        <div class="row">
                            <div class="col-xl-4 col-4">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.cabinet.Withdrawal-amount'):</h6>
                                </div>
                            </div>
                            <div class="col-xl-8 col-8">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    <input type="text" placeholder="@lang('site.page.cabinet.Enter-the-amount')">
                                </div>
                            </div>
                            <div class="col-xl-4 col-4">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.cabinet.Choose-bank-account'):</h6>
                                </div>
                            </div>
                            <div class="col-xl-8 col-8">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    <select>
                                        <option>{{ auth()->user()->bank_number }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center MarginTop30">
                            <button class="btns-red" data-dismiss="modal" aria-label="Close">@lang('site.page.cabinet.Cancel')</button>
                            <button class="btns-green">@lang('site.page.cabinet.Request')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
