<!-- Modal DepositOut -->
<div class="modal HeaderBlue" id="PayOutDai" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.page.cabinet.Withdraw-funds')</h2>
                {!! Form::open(['url' => route('front.cabinet.investor.deposit.payOutDai'), 'method' => 'POST']) !!}
                <div class="BoxPaddActive">
                    <div class="row">
                        <div class="col-xl-3 col-4">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.cabinet.withdrawal-select-symbol'):</h6>
                            </div>
                        </div>
                        <div class="col-xl-9 col-8">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                <select id="s-amount_dai2" name="type_asset_id" required>
                                    <option value=""></option>
                                    @foreach($sInvestorAssets as $active)
                                        <option value="{{ $active->id }}"
                                                data-price="{{ $active->coinValue ? $active->coinValue->price : 0 }}"
                                                data-symbol="{{ $active->coinValue ? $active->coinValue->symbol : '' }}"
                                                data-symbol-count="{{ $user->getBalanceByCoin($active->id) }}"
                                        >
                                            {{ $active->name }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! Form::error('type_asset_id') !!}
                            </div>
                        </div>

                        <div class="col-xl-3 col-4">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.cabinet.Withdrawal-amount'):</h6>
                            </div>
                        </div>
                        <div class="col-xl-9 col-8">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                <input type="text" placeholder="@lang('site.page.cabinet.Enter-the-amount')"
                                       name="amount" id="m-amount-dai2" required>
                            </div>
                        </div>

                        <div class="col-xl-3 col-4">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.cabinet.sidebar-conversion_fee'):</h6>
                            </div>
                        </div>
                        <div class="col-xl-9 col-8">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                <input type="text" id="m-commission2" readonly>
                            </div>
                        </div>
                        <div class="col-xl-3 col-4">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.cabinet.sidebar-to-conclusion'):</h6>
                            </div>
                        </div>
                        <div class="col-xl-9 col-8">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                <input type="text" id="m-conclusion2" readonly>
                            </div>
                        </div>

                        <div class="col-xl-3 col-4">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.cabinet.сhoose-wallet-number'):</h6>
                            </div>
                        </div>
                        <div class="col-xl-9 col-8">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                <input type="text" name="wallet" required>
                            </div>
                        </div>

                        @include('components.form.sms_code', ['url'=>route('front.cabinet.sms.get.code', ['type'=> \App\Entity\CodeVerifiedField::TYPE_INVESTOR_DEPOSIT_PAY_OUT_DAI])])

                        @include('components.form.2FA_input')

                        <div class="form-group text-center MarginTop30">
                            <button class="btns-red" data-dismiss="modal"
                                    aria-label="Close">@lang('site.page.cabinet.Cancel')</button>
                            <button class="btns-green">@lang('site.page.cabinet.Request')</button>
                        </div>

                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    @section('script-modals3')
        <script>
            $('#s-amount_dai2').on('change', function (e) {
                let course = $(this).find(':selected').data('price') * 1;
                let symbolCount = $(this).find(':selected').data('symbol-count');
                let amount = course * symbolCount;
                let percent = symbolCount * {{ $setting->conversion_fee }} / 100;
                let conclusion = symbolCount - percent;
                $('#m-amount-dai2').val(symbolCount.toFixed(2));
                $('#m-commission2').val(percent.toFixed(2));
                $('#m-conclusion2').val(conclusion.toFixed(2));
            });

            $('#m-amount-dai2').on('keyup', function (e) {
                let symbolCount = e.target.value * 1;
                let percent = symbolCount * {{ $setting->conversion_fee }} / 100;
                let conclusion = symbolCount - percent;
                $('#m-commission2').val(percent.toFixed(2));
                $('#m-conclusion2').val(conclusion.toFixed(2));
            });

            $('.send-sms-payout-dai').on('click', function (e) {
                e.preventDefault();
                var el = $(this);
                $.ajax({
                    url: '{{ (route('front.cabinet.investor.deposit.requestPhonePayoutDai')) }}',
                    success: function (result) {
                        if (result.hasOwnProperty('error')) {
                            el.closest('div').append('<span class="invalid-feedback" style="color: red;display: block;"><strong>' + result.error + '</strong></span>');
                            setTimeout(function () {
                                el.closest('div').find('.invalid-feedback').remove();
                            }, 10000);
                            return;
                        }
                        el.closest('div').addClass('hide');
                    }
                });
            });
        </script>
@endsection
