@extends('front.layouts.cabinet')

@section('content')
    <div class="CabinetContent">
        <h1 class="Title text-left">@lang('site.header.magazine')</h1>
        <p>@lang('site.page.cabinet.magazine-text')</p>

        <div class="JournalSearch">
            <div class="row">
                <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                            <div class="Text MarginTop10">@lang('site.page.cabinet.magazine-Search'):</div>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                            <div class="JournalSearchBox MarginBot15">
                                <input type="text">
                                <button></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12">
                    <button class="btns-green MarginBot15">EXPORT CSV</button>
                    <button class="btns-orange">Export PDF</button>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th class="text-center">@lang('site.page.cabinet.magazine-table-number')</th>
                    <th class="text-center">@lang('site.page.cabinet.table-date-and-time')</th>
                    <th>@lang('site.page.cabinet.table-Name')</th>
                    <th>@lang('site.page.cabinet.table-Message')</th>
                    <th>@lang('site.page.cabinet.table-Report-support')</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($userNotifications as $key => $userNotification)
                        <tr>
                            <td class="text-center">{{ $userNotifications->currentPage() * ($key+1) }}</td>
                            <td class="text-center">{{ $userNotification->created_at->format('d.m.Y') }}</td>
                            <td>{{ $userNotification->title }}</td>
                            <td>{!! $userNotification->body  !!}</td>
                            <td><a href="{{route('front.cabinet.contacts')}}">@lang('site.page.cabinet.table-Report-problem-us')</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="text-right Bold">
                {{ $userNotifications->appends(request()->all())->links() }}
{{--                <a href="#">Смотреть больше <i class="fas fa-caret-right"></i></a>--}}
            </div>
        </div>

    </div>
@endsection
