<div class="CabSec" id="CabSec2">
    <h2>
        <a href="#" class="ShowMoreDetalis collapsed" data-toggle="collapse" data-target="#CabSec2-Collaps">@lang('site.page.cabinet.Invite-friends')</a>
    </h2>
    <div id="CabSec2-Collaps" class="collapse" data-parent="#CabSec2">
        <div class="SecInner">
            <div class="Descrip">@lang('site.page.cabinet.Invite-friends-Descrip')</div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <h6>@lang('site.page.cabinet.Enter-friend-email')</h6>
                    {!! Form::open(['url' => route('front.cabinet.profile.sendPromo')]) !!}
                        {!! Form::text('email', null, ['placeholder' => 'Email', 'required' => true]) !!}
                        {!! Form::error('email') !!}
                        <div class="MarginTop15">
                            <button type="submit" class="btns-orange">@lang('site.page.form-btn-send')</button>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <h6>@lang('site.page.cabinet.Referral-link')</h6>
                    <div class="CopyReffBox">
                        <input id="CopyReff" type="text" placeholder="Email" value="{{ route('register') }}?ref={{ $user->promo_code }}">
                        <button class="CopyReffBTN" onclick="CopyReff()"></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
