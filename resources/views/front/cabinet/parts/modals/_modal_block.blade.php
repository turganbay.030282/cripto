<!-- Modal Doposit1 -->
<div class="modal HeaderBlue" id="BlockUser" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="padding:40px;">
            <div class="modal-header">
                <h2 class="text-center">
                    {{ trans('site.page.form-status-block-text') }}
                </h2>
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
