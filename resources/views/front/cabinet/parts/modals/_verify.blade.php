<style>
    #getid-component footer:before {
        position: static;
        width: auto;
        height: auto;
        content: "";
        background: none;
    }
</style>
<!-- Modal VerificationLine -->
<div class="modal" id="VerificationLine" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 700px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <div id='getid-component'></div>
            </div>
        </div>
    </div>
</div>

@section('script2')
    @php($customerId = auth()->id() . '-' . uniqid(auth()->id()))
    <script src='{{ env('GET_ID_API_SCRIPT') }}'></script>
    <script>
        const defaultConfig = {
            containerId: 'getid-component',
            onComplete: function () { console.log('Completed'); },
            onFail: function () { console.log('Failed'); },
            onExists: function () { console.log('Id already exists'); },
            onSubmit: function () { console.log('Submitted'); },
        };
        function initCallbacks(config) {
            const { redirects } = config;
            if (redirects) {
                Object.entries(redirects).map(([key, value]) => { config[key] = function () { window.location.href = value } });
            }
            return config;
        };
        const { init } = window.getidWebSdk;
        const cfg = {
            "dictionary": "en",
            "metadata": {
                "customerId": "{{ $userGetIdCustomer }}"
            },
            "flow": [
                {
                    "component": "DocumentPhoto",
                    "enableCheckPhoto": true,
                    "interactive": false,
                    "showRules": true
                },
                {
                    "component": "Form",
                    "fields": [
                        {
                            "label": "First Name",
                            "name": "First name",
                            "required": true,
                            "type": "text"
                        },
                        {
                            "label": "Last Name",
                            "name": "Last name",
                            "required": true,
                            "type": "text"
                        },
                        {
                            "label": "Email address",
                            "name": "Email",
                            "required": true,
                            "type": "email"
                        },
                        {
                            "label": "Date Of Birth",
                            "name": "Date of birth",
                            "required": true,
                            "type": "date",
                            "value": null
                        }
                    ]
                },
                {
                    "component": "Selfie",
                    "showRules": true
                },
                {
                    "component": "Liveness"
                },
                {
                    "component": "ThankYou"
                }
            ],
            "formType": "narrow",
            "isPublic": true,
            "redirects": {
                "onComplete": "{{ url()->current() }}",
                "onExists": "{{ url()->current() }}",
                "onFail": "{{ url()->current() }}",
            },
            "sdkLink": "{{ env('GET_ID_API_SCRIPT') }}",
            "title": "SDK demo page",
            "translations": {
                "ThankYou_next": "Start over"
            },
            "verificationTypes": [
                "E",
                "F",
                "W"
            ],
            "apiUrl": "{{ env('GET_ID_API_BASE_URL_SDK') }}"
        };
        const config = Object.assign(defaultConfig, initCallbacks(cfg));
        window.getidWebSdk.init(config, {!! $userGetIdToken !!});
    </script>
@endsection
