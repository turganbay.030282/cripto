<!-- Modal VerificationLine -->
<div class="modal" id="VerificationLine" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.page.cabinet.Application-for-Verification')</h2>
                {!! Form::open(['url' => route('front.cabinet.profile.verifySend'), 'enctype' => 'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.profile.LastName')</h6>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                {!! Form::text('last_name', null, ['required' => true]) !!}
                                {!! Form::error('last_name') !!}
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.profile.Name')</h6>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                {!! Form::text('name', null, ['required' => true]) !!}
                                {!! Form::error('name') !!}
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.profile.Birthday')</h6>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                {!! Form::text('birthday', null, ['required' => true, 'class' => 'datepicker']) !!}
                                {!! Form::error('birthday') !!}
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.profile.Actual-address')</h6>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                {!! Form::text('address', null, ['required' => true]) !!}
                                {!! Form::error('address') !!}
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.cabinet.Contact-phone-number')</h6>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                {!! Form::text('phone', null, ['required' => true]) !!}
                                {!! Form::error('phone') !!}
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.cabinet.type-doc')</h6>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                {!! Form::select('type_doc', $lisTypeDocuments, null, ['id' => 'type_doc', 'required' => true]) !!}
                                {!! Form::error('type_doc') !!}
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.cabinet.Upload-passport-ID')</h6>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                {!! Form::file('card_id', ['required' => true]) !!}
                                {!! Form::error('card_id') !!}
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12 back-doc hide">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.cabinet.Upload-passport-ID-back')</h6>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12 back-doc hide">
                            <div class="form-group">
                                {!! Form::file('card_id_back', ['id' => 'card_id_back']) !!}
                                {!! Form::error('card_id_back') !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        {!! Form::button(trans('site.page.form-btn-send'), ['type' => 'submit', 'class' => 'btns-green']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('script2')
    <script>
        $('#type_doc').on('change', function () {
            if ($(this).val() == 'passport') {
                $('.back-doc').addClass('hide');
            } else {
                $('.back-doc').removeClass('hide');
            }
        });
    </script>
@endsection
