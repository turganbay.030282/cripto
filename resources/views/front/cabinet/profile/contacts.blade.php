@extends('front.layouts.cabinet')

@section('content')

    <div class="CabinetContent">
        <h1 class="Title text-left">@lang('site.page.contact-info')</h1>
        <div class="ContactInfo MarginBot40">
            <div class="row no-gutters">
                <div class="col-xl-8 col-lg-8 col-md-12 col-12">
                    <div class="ContactLeft">
                        <h2>@lang('site.page.contact-Send-message')</h2>
                        {!! Form::open(['url' => route('front.cabinet.profile.submitContact')]) !!}
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                    <div class="form-group">
                                        <div class="FormText">@lang('site.page.contact-form-Your-name')</div>
                                        {!! Form::text('name', $user->full_name, ['placeholder' => trans('site.page.contact-form-Your-name'), 'readonly' => true]) !!}
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                    <div class="form-group">
                                        <div class="FormText">@lang('site.page.contact-form-Your-E-mail')</div>
                                        {!! Form::text('email', $user->email, ['placeholder' => trans('site.page.contact-form-Your-E-mail'), 'readonly' => true]) !!}
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                    <div class="form-group">
                                        <div class="FormText">@lang('site.page.contact-phone')</div>
                                        {!! Form::text('phone', $user->phone, ['placeholder' => trans('site.page.contact-enter-phone')]) !!}
                                        {!! Form::error('phone') !!}
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                    <div class="form-group">
                                        <div class="FormText">@lang('site.footer.Company')</div>
                                        {!! Form::text('company', null, ['placeholder' => trans('site.page.contact-enter-company')]) !!}
                                        {!! Form::error('company') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="FormText MarginBot15">@lang('site.page.contact-write-us')</div>
                                {!! Form::textarea('message', null, ['placeholder' => trans('site.page.contact-Enter-your-message')]) !!}
                                {!! Form::error('message') !!}
                            </div>
                            <div class="form-group text-left">
                                <button type="submit" class="btns-orange">@lang('site.page.form-btn-send')</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-12">
                    <div class="ContactRight">
                        <h2>@lang('site.page.contact-info')</h2>
                        @if($setting->contact_address)
                            <div class="Adress">{{ $setting->contact_address }}</div>
                        @endif
                        @if($setting->contact_phone)
                            <div class="Phone"><a href="tel:+{{ str_replace(['+', ' ', '(', ')'], '', $setting->contact_phone) }}">{{ $setting->contact_phone }}</a></div>
                        @endif
                        @if($setting->contact_email)
                            <div class="Email"><a href="mailto:{{ $setting->contact_email }}">{{ $setting->contact_email }}</a></div>
                        @endif
                        <div class="social">
                            @if($setting->soc_fb)
                                <a href="{{ $setting->soc_fb }}" target="_blank"><i class="fab fa-facebook-f"></i></a>
                            @endif
                            @if($setting->soc_tw)
                                <a href="{{ $setting->soc_tw }}" target="_blank"><i class="fab fa-twitter"></i></a>
                            @endif
                            @if($setting->soc_in)
                                <a href="{{ $setting->soc_in }}" target="_blank"><i class="fab fa-instagram"></i></a>
                            @endif
                            @if($setting->soc_yt)
                                    <a href="{{ $setting->soc_yt }}" target="_blank"><i class="fab fa-youtube"></i></a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="SilverBG text-center">
            <h3 class="RecvisitText">{{ $setting->req_sia }}<br>{{ $setting->req_reg }}<br>{{ $setting->req_seb }}<br>{{ $setting->req_swift }}</h3>
        </div>
    </div>

@endsection
