@extends('front.layouts.cabinet')

@section('content')
    <div class="CabinetContent">
        <h1 class="TitleDannie">{{ $user->full_name }}s <span class="IcoGold">1</span></h1>

        <div class="BoxDannie">
            <h2 class="Title text-left">@lang('site.page.profile.2fa-step-title')</h2>

            <div class="row">
                <div class="col-xl-7 col-lg-12 col-md-7 col-sm-12 col-12">
                    {!! Form::open(['url' => route('front.cabinet.profile.g2faEnable')]) !!}
                    <div class="ListDannie">
                        <p><string>@lang('site.page.profile.2fa-step-important')</string></p>
                        <p>@lang('site.page.profile.2fa-step-desc')</p>
                        <div class="row">
                            <div class="col-md-4 col-12 d-flex align-items-center">
                                <div class="Text1">@lang('site.page.profile.2fa-step-auth-key'):</div>
                            </div>
                            <div class="col-md-8 col-12">
                                {{ $authenticationKey }}
                            </div>
                        </div>
                    </div>
                    <div class="ListDannie">
                        <div class="row">
                            <div class="col-md-4 col-12 d-flex align-items-center">
                                <div class="Text1">@lang('site.page.profile.2fa-step-qr-code'):</div>
                            </div>
                            <div class="col-md-8 col-12">
                                {!! $qRImage  !!}
                            </div>
                        </div>
                    </div>
                    <div class="ListDannie">
                        <div class="row">
                            <div class="col-md-4 col-12 d-flex align-items-center">
                                <div class="Text1">@lang('site.cabinet.2gaf-placeholder-verify-code'):</div>
                            </div>
                            <div class="col-md-8 col-12">
                                {!! Form::text('secret', null, ['required' => true]) !!}
                                {!! Form::error('secret') !!}
                            </div>
                        </div>
                    </div>
                    <div class="MarginTop15">
                        {!! Form::submit(trans('site.cabinet.2gaf-btn-enable'), ['class' => 'btns-orange']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
