@extends('front.layouts.cabinet')

@section('content')
    <div class="CabinetContent">
        <h1 class="TitleDannie">{{ $user->full_name }} <span class="IcoGold">1</span></h1>
        <div class="BoxDannie">
            <h2 class="Title text-left">@lang('site.page.profile.Personal-data')</h2>

            {!! Form::open(['url' => route('front.cabinet.profile.changeSend'), 'enctype' => 'multipart/form-data']) !!}
            <div class="row">
                <div class="col-xl-7 col-lg-12 col-md-7 col-sm-12 col-12">
                    <div class="ListDannie">
                        <div class="row">
                            <div class="col-md-4 col-12 d-flex align-items-center">
                                <div class="Text1">@lang('site.page.profile.Email'):</div>
                            </div>
                            <div class="col-md-8 col-12">
                                {!! Form::text('email', $user->email, ['required' => true]) !!}
                                {!! Form::error('email') !!}
                            </div>
                        </div>
                    </div>
                    <div class="ListDannie">
                        <div class="row">
                            <div class="col-md-4 col-12 d-flex align-items-center">
                                <div class="Text1">@lang('site.page.profile.Name'):</div>
                            </div>
                            <div class="col-md-8 col-12">
                                {!! Form::text('first_name', $user->first_name, ['required' => true]) !!}
                                {!! Form::error('first_name') !!}
                            </div>
                        </div>
                    </div>
                    <div class="ListDannie">
                        <div class="row">
                            <div class="col-md-4 col-12 d-flex align-items-center">
                                <div class="Text1">@lang('site.page.profile.LastName'):</div>
                            </div>
                            <div class="col-md-8 col-12">
                                {!! Form::text('last_name', $user->last_name, ['required' => true]) !!}
                                {!! Form::error('last_name') !!}
                            </div>
                        </div>
                    </div>
                    <div class="ListDannie">
                        <div class="row">
                            <div class="col-md-4 col-12 d-flex align-items-center">
                                <div class="Text1">@lang('site.page.profile.Birthday'):</div>
                            </div>
                            <div class="col-md-8 col-12">
                                {!! Form::text('birthday', $userVerification ? $userVerification->birthday : null, ['required' => true, 'class' => 'datepicker']) !!}
                                {!! Form::error('birthday') !!}
                            </div>
                        </div>
                    </div>
                    <div class="ListDannie">
                        <div class="row">
                            <div class="col-md-4 col-12 d-flex align-items-center">
                                <div class="Text1">@lang('site.page.profile.Actual-address'):</div>
                            </div>
                            <div class="col-md-8 col-12">
                                {!! Form::text('address', $userVerification ? $userVerification->address : null, ['required' => true]) !!}
                                {!! Form::error('address') !!}
                            </div>
                        </div>
                    </div>
                    <div class="ListDannie">
                        <div class="row">
                            <div class="col-md-4 col-12 d-flex align-items-center">
                                <div class="Text1">@lang('site.page.profile.Contact-number'):</div>
                            </div>
                            <div class="col-md-8 col-12">
                                {!! Form::text('phone', $user->phone, ['required' => true]) !!}
                                {!! Form::error('phone') !!}
                                <label>* {{ $user->phone_verified ? trans('site.user.phone-verify') : trans('site.user.phone-un-verify') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-12 col-md-5 col-sm-12 col-12">
                    <h5 class="Opacity05">@lang('site.page.profile.Document-scan'):</h5>
                    @if($user->getPhotoUrl())
                        <img class="imageRespon" src="{{ $user->getPhotoUrl() }}" alt="Image">
                    @endif
                    {!! Form::file('card_id') !!}
                    {!! Form::error('card_id') !!}
                </div>
            </div>
            <div class="MarginTop15">
                {!! Form::button(trans('site.page.profile.btn-change-profile'), ['type' => 'submit', 'class' => 'btns-orange']) !!}
            </div>
            {!! Form::close() !!}
        </div>

        <div class="BoxDannie">
            <h2 class="Title text-left">@lang('site.page.profile.tab-security')</h2>
            @if($user->google2fa_enable)
                <p>@lang('site.page.profile.2fa-title-on')</p>
                {!! Form::open(['url' => route('front.cabinet.profile.g2faDisable')]) !!}
                    <div class="ListDannie">
                        <div class="row">
                            <div class="col-md-4 col-12 d-flex align-items-center">
                                <div class="Text1">@lang('site.page.profile.current-password'):</div>
                            </div>
                            <div class="col-md-8 col-12">
                                {!! Form::password('current-password', ['required' => true]) !!}
                                {!! Form::error('current-password') !!}
                            </div>
                        </div>
                    </div>
                    <div class="MarginTop15">
                        {!! Form::button(trans('site.cabinet.2gaf-btn-disable'), ['type' => 'submit', 'class' => 'btns-orange']) !!}
                    </div>
                {!! Form::close() !!}
            @else
                <p>@lang('site.page.profile.2fa-title-off')</p>
                <div class="row">
                    <div class="col-xl-7 col-lg-12 col-md-7 col-sm-12 col-12">
                        <a href="{{ route('front.cabinet.profile.g2fa') }}" class="btns-orange">@lang('site.cabinet.2gaf-btn-enable')</a>
                    </div>
                </div>
            @endif
        </div>

        <div class="BoxDannie">
            <h2 class="Title text-left">@lang('site.page.profile.Change-password')</h2>
            <div class="row">
                <div class="col-xl-7 col-lg-12 col-md-7 col-sm-12 col-12">
                    {!! Form::open(['url' => route('front.cabinet.profile.changePassword')]) !!}
                    <div class="ListDannie">
                        <div class="row">
                            <div class="col-md-4 col-12 d-flex align-items-center">
                                <div class="Text1">@lang('site.page.profile.Old-password'):</div>
                            </div>
                            <div class="col-md-8 col-12">
                                {!! Form::password('old_password') !!}
                                {!! Form::error('old_password') !!}
                            </div>
                        </div>
                    </div>
                    <div class="ListDannie">
                        <div class="row">
                            <div class="col-md-4 col-12 d-flex align-items-center">
                                <div class="Text1">@lang('site.page.profile.New-password'):</div>
                            </div>
                            <div class="col-md-8 col-12">
                                {!! Form::password('password') !!}
                                {!! Form::error('password') !!}
                            </div>
                        </div>
                    </div>
                    <div class="ListDannie">
                        <div class="row">
                            <div class="col-md-4 col-12 d-flex align-items-center">
                                <div class="Text1">@lang('site.page.profile.repeat-New-password'):</div>
                            </div>
                            <div class="col-md-8 col-12">
                                {!! Form::password('password_confirmation') !!}
                                {!! Form::error('password_confirmation') !!}
                            </div>
                        </div>
                    </div>
                    <div class="MarginTop15">
                        {!! Form::submit(trans('site.page.profile.Change-password'), ['class' => 'btns-orange']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        @if(!$user->phone_verified)
            {!! Form::open(['url' => route('front.cabinet.profile.sendPhoneVerification'), 'method' => 'GET']) !!}
            <div class="BoxDannie">
                <h2 class="Title text-left">@lang('site.page.profile.Verify-phone-number')</h2>
                <p>@lang('site.page.profile.add-extra-protection')</p>
                <div class="row">
                    <div class="col-xl-7 col-lg-12 col-md-7 col-sm-12 col-12">
{{--                        <div class="ListDannie">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-4 col-12 d-flex align-items-center">--}}
{{--                                    <div class="Text1">@lang('site.page.profile.Code-country'):</div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-8 col-12">--}}
{{--                                    <input type="text" placeholder="(+371)">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="ListDannie">
                            <div class="row">
                                <div class="col-md-4 col-12 d-flex align-items-center">
                                    <div class="Text1">@lang('site.page.profile.Phone-number'):</div>
                                </div>
                                <div class="col-md-8 col-12">
                                    <div class="BoxNumbPos">
                                        <input class="Pos2" type="text" placeholder="22187783" name="phone" value="{{ old('phone', $user->phone) }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="MarginTop15">
                            <button type="submit"  class="btns-orange">@lang('site.page.profile.Send-SMS')</button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

            <div class="BoxDannie">
                <h6 class="Bold">@lang('site.page.profile.Confirm-your-phone-number')</h6>
                <p>@lang('site.page.profile.Enter-seven-digit') <strong>+xxx xxxxxx83</strong></p>

                @if($codeVerified && $codeVerified->verify_token && $codeVerified->verify_token_expire && $codeVerified->verify_token_expire->gt(\Carbon\Carbon::now()))
                    {!! Form::open(['url' => route('front.cabinet.profile.verifyPhone'), 'method' => 'POST']) !!}
                @endif
                <div class="row">
                    <div class="col-xl-7 col-lg-12 col-md-7 col-sm-12 col-12">
                        <div class="ListDannie">
                            <div class="row">
                                <div class="col-md-4 col-12 MarginTop10">
                                    <div class="Text1">@lang('site.page.profile.Enter-number'):</div>
                                </div>
                                <div class="col-md-8 col-12">
                                    <input type="text" name="token" value="{{ old('token') }}">
                                    <p class="MarginTop5">
                                        @lang('site.page.profile.Didnt-receive-SMS') <a href="{{ route('front.cabinet.profile.sendPhoneVerification') }}">@lang('site.page.profile.Send-another-one')</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="MarginTop15">
                            <button type="submit" class="btns-orange">@lang('site.user.phone-btn-verify')</button>
                        </div>
                    </div>
                </div>
                @if($codeVerified && $codeVerified->verify_token && $codeVerified->verify_token_expire && $codeVerified->verify_token_expire->gt(\Carbon\Carbon::now()))
                    {!! Form::close() !!}
                @endif

            </div>

        @endif

        <div class="BoxDannie">
            <h2 class="Title text-left">@lang('site.page.profile.Configuring-notifications')</h2>
            <h6 class="MarginBot15">@lang('site.page.profile.Choose-which-notifications-receive')</h6>
            <div class="row">
                <div class="col-xl-7 col-lg-12 col-md-7 col-sm-12 col-12">
                    {!! Form::open(['url' => route('front.cabinet.profile.changeNotify')]) !!}
                    <div class="ListDannie">
                        <label>
                            <input type="hidden" name="notify_news" value="0">
                            <input type="checkbox" class="Mycheckbox" name="notify_news" value="1" {{ $user->notify_news ? 'checked' : '' }}>
                            @lang('site.menu.news')
                        </label>
                        <label>
                            <input type="hidden" name="notify_promo" value="0">
                            <input type="checkbox" class="Mycheckbox" name="notify_promo" value="1" {{ $user->notify_promo ? 'checked' : '' }}>
                            @lang('site.page.profile.Offers-promotions')
                        </label>
                        <h6 class="MarginBot15">@lang('site.page.profile.Select-service')</h6>
                        <label>
                            <input type="hidden" name="notify_by_email" value="0">
                            <input type="checkbox" class="Mycheckbox" name="notify_by_email" value="1" {{ $user->notify_by_email ? 'checked' : '' }}>
                            E-mail
                        </label>
                        <label>
                            <input type="hidden" name="notify_by_sms" value="0">
                            <input type="checkbox" class="Mycheckbox" name="notify_by_sms" value="1" {{ $user->notify_by_sms ? 'checked' : '' }}>
                            @lang('site.page.profile.SMS')
                        </label>
                        <label>
                            <input type="hidden" name="notify_by_push" value="0">
                            <input type="checkbox" class="Mycheckbox" name="notify_by_push" value="1" {{ $user->notify_by_push ? 'checked' : '' }}>
                            @lang('site.page.profile.Push')
                        </label>
                    </div>
                    <div class="MarginTop15">
                        <button type="submit" class="btns-orange">@lang('site.page.profile.Save')</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
