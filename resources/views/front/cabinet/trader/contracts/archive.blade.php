@extends('front.layouts.cabinet')

@section('modals')

@endsection

@section('content')

    <div class="CabinetContent Dogov">
        <h2>@lang('site.page.cabinet.trader-title-archive-contracts')</h2>

        @if($assetContracts->isNotEmpty())
            @foreach($assetContracts as $assetContract)
                <h3>{{ $assetContract->name }}</h3>
                <div class="row">
                    @foreach($assetContract->contracts as $contract)
                        <div class="col-xl-4 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="ListDogovora">
                            <h4>
                                {{ $contract->isClose() ? trans('site.page.cabinet.close-contract') : ($contract->isWaitClose() ? trans('site.page.cabinet.wait-close-contract') : trans('site.page.cabinet.Active-contract')) }}
                                {{ $contract->number }}
                            </h4>
                            <div class="ListLeftRight">
                                <div class="row">
                                    <div class="col-7">
                                        <div class="Height100pc d-flex align-items-center">
                                            <div class="Text1">@lang('site.page.cabinet.Total-cost'):</div>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="Text2">€ {{ $contract->amount }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="ListLeftRight">
                                <div class="row">
                                    <div class="col-7">
                                        <div class="Height100pc d-flex align-items-center">
                                            <div class="Text1">@lang('site.page.cabinet.trader-contract-profit-lost'):</div>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="Text2 Green">€ {{ format_number($contract->amount_asset * $contract->typeAsset->coinValue->price - $contract->amount) }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center MarginTop15">
                                <a href="#" class="btns-green" data-toggle="modal" data-target="#Dogovor{{ $contract->id }}">@lang('site.page.cabinet.Open-contract')</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            @endforeach
        @endif
    </div>

@section('modals2')
    @if($assetContracts->isNotEmpty())
        @foreach($assetContracts as $assetContract)
            @foreach($assetContract->contracts as $contract)
                <div class="modal HeaderBlue" id="Dogovor{{ $contract->id }}" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                                    <i class="fal fa-times-circle"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <h2 class="text-center">@lang('site.page.cabinet.Contract') {{ $assetContract->name }}</h2>
                                <div class="BoxPaddActive">
                                    <div class="row">
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.Date-conclusion'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>{{ $contract->created_at->format('d.m.Y') }}</h6>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.Number-of-contract'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>{{ $contract->number }}</h6>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.volume-asset-in-contract'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>{{ $contract->amount_asset }} {{ $assetContract->coinValue->symbol }}</h6>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.amount-deal'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>{{ $contract->amount }} €</h6>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.amount-paid-payments'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>0 €</h6>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.Amount-outstanding-payments'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>0 €</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center MarginTop15 BTNWidth100">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                {!! Form::open(['url' => route('front.cabinet.trader.contracts.download', $contract)]) !!}
                                                <div class="DownloadBTNS ResponsiveMarginBot15">
                                                    <button class="btns-orange">@lang('site.page.cabinet.Download')</button>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                            <div class="col-md-6 col-12">
                                                {!! Form::open(['url' => route('front.cabinet.trader.contracts.print', $contract)]) !!}
                                                <div class="PrintBTNS">
                                                    <button type="submit" class="btns-orange btn-contract-print">@lang('site.page.cabinet.Print')</button>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach
        @endforeach
    @endif

@endsection

@endsection

@section('script')
    <script>
        $('.btn-contract-print').on('click', function(e){
            e.preventDefault();
            $.ajax({
                url: $(this).closest('form').attr('action'),
                method: 'POST',
            }).done(function(response){
                var win = window.open(response, '_blank');
                win.focus();
            });
        });
    </script>
@endsection
