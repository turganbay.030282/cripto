@extends('front.layouts.cabinet')

@section('modals')

@endsection

@section('content')

    <div class="CabinetContent Dogov">
        <h2>@lang('site.page.cabinet.Summary')</h2>
        <div class="ResumeBox">
            <div class="ListLeftRight">
                <div class="row">
                    <div class="col-xl-6 col-12">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text1">@lang('site.page.cabinet.Total-cost'):</div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-12">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text3">{{ $creditContractValue }} €</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ListLeftRight">
                <div class="row">
                    <div class="col-xl-6 col-12">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text1">@lang('site.page.cabinet.Unused-credit-line'):</div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-12">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text3">{{ $user->balance_trader }} €</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ListLeftRight">
                <div class="row">
                    <div class="col-xl-6 col-12">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text1">@lang('site.page.cabinet.Amount-outstanding-payments'):</div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-12">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text3">0 €</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ListLeftRight">
                <div class="row">
                    <div class="col-xl-6 col-12">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text1">@lang('site.page.cabinet.Next-payment'):</div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-12">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text3 WhithBTN">
                                {{ $creditContractNext ? format_number($creditContractNext->calcTraderAmount()) : 0 }} € / {{ $creditContractNext ? $creditContractNext->getNextDatePayment() : '' }}
                            </div>
                            <div class="BoxBlockRight">
                                <a href="#" class="btns-orange" data-toggle="modal" data-target="#DogovorPaymentNext">@lang('site.page.cabinet.Pay')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ListLeftRight WithBorder">
                <div class="Height100pc d-flex align-items-center">
                    <div class="Text3 WhithBTN">
                        <div class="Warning">@lang('site.page.cabinet.Difficulty-paying')</div>
                    </div>
                    <div class="BoxBlockRight">
                        <a href="{{ route('front.cabinet.contacts') }}" class="btns-gray">@lang('site.header.help')</a>
                    </div>
                </div>
            </div>
        </div>

        @if($assetContracts->isNotEmpty())
        <h2>@lang('site.page.cabinet.concluded-contracts')</h2>
            @foreach($assetContracts as $assetContract)
                <h3>{{ $assetContract->name }}</h3>
                <div class="row">
                    @foreach($assetContract->contracts as $contract)
                        <div class="col-xl-4 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="ListDogovora">
                            <h4>
                                {{ $contract->isClose() ? trans('site.page.cabinet.close-contract') : ($contract->isWaitClose() ? trans('site.page.cabinet.wait-close-contract') : trans('site.page.cabinet.Active-contract')) }}
                                {{ $contract->number }}
                            </h4>
                            <div class="ListLeftRight">
                                <div class="row">
                                    <div class="col-7">
                                        <div class="Height100pc d-flex align-items-center">
                                            <div class="Text1">@lang('site.page.cabinet.Total-cost'):</div>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="Text2">€ {{ $contract->amount }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="ListLeftRight">
                                <div class="row">
                                    <div class="col-7">
                                        <div class="Height100pc d-flex align-items-center">
                                            <div class="Text1">@lang('site.page.cabinet.trader-contract-profit-lost'):</div>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="Text2 Green">€ {{ format_number($contract->amount_asset * $contract->typeAsset->coinValue->price - $contract->amount) }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center MarginTop15">
                                <a href="#" class="btns-green" data-toggle="modal" data-target="#Dogovor{{ $contract->id }}">@lang('site.page.cabinet.Open-contract')</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            @endforeach
        @endif
    </div>

@endsection

@section('modals2')
    @if($assetContracts->isNotEmpty())
        @foreach($assetContracts as $assetContract)
            @foreach($assetContract->contracts as $contract)
                <div class="modal HeaderBlue" id="Dogovor{{ $contract->id }}" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                                    <i class="fal fa-times-circle"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <h2 class="text-center">@lang('site.page.cabinet.Contract') {{ $assetContract->name }}</h2>
                                <div class="BoxPaddActive">
                                    <div class="row">
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.Date-conclusion'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>{{ $contract->created_at->format('d.m.Y') }}</h6>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.Number-of-contract'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>{{ $contract->number }}</h6>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.volume-asset-in-contract'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>{{ $contract->amount_asset }} {{ $assetContract->coinValue->symbol }}</h6>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.amount-deal'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>{{ $contract->amount }} €</h6>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.amount-paid-payments'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>0 €</h6>
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <div class="LeftFormText d-flex align-items-center">
                                                <h6>@lang('site.page.cabinet.Amount-outstanding-payments'):</h6>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="RightFormText d-flex align-items-center">
                                                <h6>0 €</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center MarginTop15 BTNWidth100">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                {!! Form::open(['url' => route('front.cabinet.trader.contracts.download', $contract)]) !!}
                                                    <div class="DownloadBTNS ResponsiveMarginBot15">
                                                        <button class="btns-orange">@lang('site.page.cabinet.Download')</button>
                                                    </div>
                                                {!! Form::close() !!}
                                            </div>
                                            <div class="col-md-6 col-12">
                                                {!! Form::open(['url' => route('front.cabinet.trader.contracts.print', $contract)]) !!}
                                                    <div class="PrintBTNS">
                                                        <button type="submit" class="btns-orange btn-contract-print">@lang('site.page.cabinet.Print')</button>
                                                    </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                    @if($contract->isActive() && $contract->firstPayment &&
                                    $contract->firstPayment->isApprove())
                                        <div class="form-group text-center BTNWidth100">
                                            <button class="btns-green show-other-modal" class="closes" data-toggle="modal" data-target="#DogovorDosrochno{{$contract->id}}">@lang('site.page.cabinet.Redeem-early')</button>
                                        </div>
                                        <div class="form-group text-center BTNWidth100">
                                            <button class="btns-red show-other-modal" data-toggle="modal" data-target="#DogovorSellActive{{$contract->id}}">@lang('site.page.cabinet.contract-sell-active')</button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if($contract->isActive() && $contract->firstPayment && $contract->firstPayment->isApprove())
                    <div class="modal HeaderBlue" id="DogovorDosrochno{{ $contract->id }}" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                                        <i class="fal fa-times-circle"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h2 class="text-center">@lang('site.page.cabinet.contract-trader-redeem-title')</h2>
                                    <div class="BoxPaddActive">
                                        <div class="row">
                                            <div class="col-7">
                                                <div class="LeftFormText d-flex align-items-center">
                                                    <h6>@lang('site.page.cabinet.amount-deal'):</h6>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="RightFormText d-flex align-items-center">
                                                    <h6>{{ $contract->amount }}</h6>
                                                </div>
                                            </div>
                                            <div class="col-7">
                                                <div class="LeftFormText d-flex align-items-center">
                                                    <h6>@lang('site.page.cabinet.amount-asset'):</h6>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="RightFormText d-flex align-items-center">
                                                    <h6>{{ numeric_fmt($contract->amount_asset) }}</h6>
                                                </div>
                                            </div>
                                            <div class="col-7">
                                                <div class="LeftFormText d-flex align-items-center">
                                                    <h6>@lang('site.page.cabinet.contract-trader-redeem-cost'):</h6>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="RightFormText d-flex align-items-center">
                                                    <h6>{{ $contract->getCurrentAmountAssetCost() }}</h6>
                                                </div>
                                            </div>
                                            <div class="col-7">
                                                <div class="LeftFormText d-flex align-items-center">
                                                    <h6>@lang('site.page.cabinet.contract-trader-redeem-debit'):</h6>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="RightFormText d-flex align-items-center">
                                                    <h6>{{ $contract->getCurrentContractPaymentAmount() }}</h6>
                                                </div>
                                            </div>
                                            <div class="col-7">
                                                <div class="LeftFormText d-flex align-items-center">
                                                    <h6>@lang('site.page.cabinet.contract-trader-sell-commission'):</h6>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="RightFormText d-flex align-items-center">
                                                    <h6>{{ format_number($contract->amount*$setting->commission_sell_trader/100) }}</h6>
                                                </div>
                                            </div>
                                            <div class="col-7">
                                                <div class="LeftFormText d-flex align-items-center">
                                                    <h6>@lang('site.page.cabinet.contract-trader-redeem-payment'):</h6>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="RightFormText d-flex align-items-center">
                                                    <h6>{{ format_number($contract->getCurrentContractPaymentAmount()) + format_number($contract->amount*$setting->commission_sell_trader/100)}}</h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group text-center MarginTop15 BTNWidth100">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="ResponsiveMarginBot15">
                                                        <button class="btns-red" data-dismiss="modal" aria-label="Close">@lang('site.page.cabinet.Cancel')</button>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    {!! Form::open(['url' => route('front.cabinet.trader.contracts.redeem', $contract)]) !!}
                                                    <div class="ResponsiveMarginBot15">
                                                        <button type="button" class="btns-green show-other-modal" data-toggle="modal" data-target="#DogovorDosrochnoPay{{$contract->id}}">@lang('site.page.cabinet.contract-trader-redeem-btn')</button>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('front.cabinet.trader.parts._modal_payment_send', ['idModal' => 'DogovorDosrochnoPay'.$contract->id, 'contract' => $creditContractNext, 'contractTraderAmount' => $contract->getCurrentContractPaymentAmount(), 'url' => route('front.cabinet.trader.contracts.redeem', $contract)])

                    <div class="modal HeaderBlue" id="DogovorSellActive{{ $contract->id }}" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                                        <i class="fal fa-times-circle"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h2 class="text-center">@lang('site.page.cabinet.contract-trader-sell-title')</h2>
                                    <div class="BoxPaddActive">
                                        <div class="row">
                                            <div class="col-7">
                                                <div class="LeftFormText d-flex align-items-center">
                                                    <h6>@lang('site.page.cabinet.amount-deal'):</h6>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="RightFormText d-flex align-items-center">
                                                    <h6>{{ $contract->amount }}</h6>
                                                </div>
                                            </div>
                                            <div class="col-7">
                                                <div class="LeftFormText d-flex align-items-center">
                                                    <h6>@lang('site.page.cabinet.amount-asset'):</h6>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="RightFormText d-flex align-items-center">
                                                    <h6>{{ numeric_fmt($contract->amount_asset) }}</h6>
                                                </div>
                                            </div>
                                            <div class="col-7">
                                                <div class="LeftFormText d-flex align-items-center">
                                                    <h6>@lang('site.page.cabinet.contract-trader-sell-cost'):</h6>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="RightFormText d-flex align-items-center">
                                                    <h6>{{ $contract->getCurrentAmountAssetCost() }}</h6>
                                                </div>
                                            </div>
                                            <div class="col-7">
                                                <div class="LeftFormText d-flex align-items-center">
                                                    <h6>@lang('site.page.cabinet.contract-trader-sell-debit'):</h6>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="RightFormText d-flex align-items-center">
                                                    <h6>{{ $contract->getCurrentContractPaymentAmount() }}</h6>
                                                </div>
                                            </div>
                                            <div class="col-7">
                                                <div class="LeftFormText d-flex align-items-center">
                                                    <h6>@lang('site.page.cabinet.contract-trader-sell-commission'):</h6>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="RightFormText d-flex align-items-center">
                                                    <h6>{{ format_number($contract->getCurrentAmountAssetCost()*$setting->commission_trader/100) }} </h6>
                                                </div>
                                            </div>
                                            <div class="col-7">
                                                <div class="LeftFormText d-flex align-items-center">
                                                    <h6>@lang('site.page.cabinet.contract-trader-sell-get'):</h6>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="RightFormText d-flex align-items-center">
                                                    <h6>{{ format_number($contract->getCurrentAmountAssetCost()) - format_number($contract->getCurrentAmountAssetCost()*$setting->commission_trader/100) - $contract->getCurrentContractPaymentAmount()}}</h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group text-center MarginTop15 BTNWidth100">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="ResponsiveMarginBot15">
                                                        <button class="btns-red" data-dismiss="modal" aria-label="Close">@lang('site.page.cabinet.Cancel')</button>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    {!! Form::open(['url' => route('front.cabinet.trader.contracts.sell', $contract)]) !!}
                                                    <div class="ResponsiveMarginBot15">
                                                        <button type="submit" class="btns-green">@lang('site.page.cabinet.contract-trader-sell-btn')</button>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif
            @endforeach
        @endforeach
    @endif

    @if($creditContractNext)
        @include('front.cabinet.trader.parts._modal_payment_send', ['idModal' => 'DogovorPaymentNext', 'contract' => $creditContractNext])
    @endif
@endsection

@section('script')
    <script>
        $('.btn-contract-print').on('click', function(e){
            e.preventDefault();
            $.ajax({
                url: $(this).closest('form').attr('action'),
                method: 'POST',
            }).done(function(response){
                var win = window.open(response, '_blank');
                win.focus();
            });
        });
    </script>
@endsection
