@extends('front.layouts.cabinet')

@section('modals')
    @if(!$user->is_verification)
        @if($setting->is_manual_verify)
            @include('front.cabinet.parts.modals._verify_manual')
        @else
            @include('front.cabinet.parts.modals._verify')
        @endif
    @endif
    @if($user->is_verification && !$user->credit_level_id)
        @if($user->is_ur)
            @include('front.cabinet.trader.parts._modals_credit_level_ur')
        @else
            @include('front.cabinet.trader.parts._modals_credit_level')
        @endif
    @endif
    @if($user->credit_level_id)
        @include('front.cabinet.trader.parts._modal_contract')
    @endif
    @if($user->isBlock())
        @include('front.cabinet.parts.modals._modal_block')
    @endif
@endsection

@section('content')
    <div class="CabinetContent">
        <div class="CabSec" id="CabSec1">
            <h2>
                <a href="#" class="ShowMoreDetalis" data-toggle="collapse" data-target="#CabSec1-Collaps">@lang('site.page.cabinet.Available-actions-section')</a>
            </h2>
            <div id="CabSec1-Collaps" class="collapse show" data-parent="#CabSec1">
                <div class="SecInner">
                    <div class="Descrip">@lang('site.page.cabinet.Invite-upload-digital-photos')</div>
                    <div class="row no-gutters">

                        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="ListAction">
                                <div class="form-group">
                                    <div class="ActionStatus {{ $user->is_verification ? 'Yes' : 'No'}}"></div>
                                </div>
                                <h3>@lang('site.page.cabinet.Pass-Verification')</h3>
                                @if($userVerification && ($userVerification->isWait() || $userVerification->isReject()))
                                    <div class="Text">
                                        @if($userVerification->isWait())
                                            <a href="#" class="btn-reload">
                                                <i class="fas fa-sync"></i> <span class="timer-reload">30</span>
                                            </a>
                                        @else
                                            {{ $userVerification->status_name }} {{ $userVerification->comment ? ' : ' . $userVerification->comment : ''}}
                                        @endif
                                    </div>
                                @elseif($userVerification && $userVerification->isApprove())
                                    <div class="Text">
                                        @lang('site.page.cabinet.Verification-passed-successfully')
                                    </div>
                                @endif
                                @if(!$userVerification || $userVerification->isReject())
                                    <a href="#" class="btns-orange" data-toggle="modal" data-target="#VerificationLine">@lang('site.page.start')</a>
                                @endif
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="ListAction {{ $user->is_verification ? '' : 'Disalow'}}">
                                <div class="form-group">
                                    <div class="ActionStatus {{ $user->credit_level_id ? 'Yes' : 'No'}}"></div>
                                </div>
                                <h3>@lang('site.page.cabinet.Get-Credit-Level')</h3>
                                @if($creditLevelVerification && ($creditLevelVerification->isWait() || $creditLevelVerification->isReject()))
                                    <div class="Text">
                                        @if($creditLevelVerification->isWait())
                                            <a href="#" class="btn-reload">
                                                <i class="fas fa-sync"></i> <span class="timer-reload">30</span>
                                            </a>
                                        @else
                                            {{ $creditLevelVerification->status_name }} {{ $creditLevelVerification->comment ? ' : ' . $creditLevelVerification->comment : ''}}
                                        @endif
                                    </div>
                                @elseif($creditLevelVerification && $creditLevelVerification->isApprove())
                                    <div class="Text">
                                        @lang('site.page.cabinet.Credit-Level') - {{ $user->creditLevel ? $user->creditLevel->name : ''}}
                                    </div>
                                @endif
                                @if(!$creditLevelVerification || $creditLevelVerification->isReject())
                                    <a href="#" class="btns-orange" data-toggle="modal" data-target="#Questionnaire1">@lang('site.page.start')</a>
                                @endif
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="ListAction {{ $user->is_verification && $user->credit_level_id && !$hasContract ? '' : 'Disalow' }}">
                                <div class="form-group">
                                    <div class="ActionStatus {{ $user->is_verification && $user->credit_level_id && $hasContract ? 'Yes' : 'No' }}"></div>
                                </div>
                                <h3>@lang('site.page.cabinet.Buy-Asset')</h3>
                                <div class="Text"></div>
                                @if($user->is_verification && $user->credit_level_id && !$hasContract)
                                    <a href="#buy-asset" class="btns-orange">@lang('site.page.cabinet.Invest')</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('front.cabinet.parts._promo')

        <div class="Calculator">
            <div class="container">
                <h2>@lang('site.page.cabinet.Interactive-form-trader-profit')</h2>
                <div class="row no-gutters">

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="BoxCalculator">
                            <h3>@lang('site.page.cabinet.Collateral-asset'):</h3>
                            <div class="form-group">
                                <select id="asset-natural" {{ $user->isBlock() ? 'disabled' : ''}}>
                                    @foreach($activeFirsts as $active)
                                        <option value="{{ $active->id }}" data-price="{{ $active->coinValue ? $active->coinValue->price : 0 }}" data-symbol="{{ $active->coinValue ? $active->coinValue->symbol : '' }}">{{ $active->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <h3>@lang('site.page.form-trader-loan-month')</h3>
                            <div class="form-group">
                                <input type="text" class="RangeSliderMonth" name="RangeSliderMonth" value="" id="RangeSliderMonth1">
                            </div>
                            <h3>@lang('site.page.cabinet.Deposit-amount'):</h3>
                            <div class="form-group">
                                <input type="text" class="RangeSliderMoney1" name="RangeSliderMoney1" value="" id="RangeSliderMoney1">
                            </div>
                            <input type="text" class="RangeSliderMoney11" name="RangeSliderMoney11" id="RangeSliderMoney11" value="50">
                        </div>
                        <div class="BoxInvesMaterial">
                            <div class="HeadInvest">@lang('site.page.calc-trader-investment-assets')</div>
                            <div class="TextInvest1">@lang('site.page.cabinet.Credit-Level')</div>
                            <div class="TextInvest2 LineBottom" id="creditLevelText1">Золотой</div>
                            <div class="TextInvest1">@lang('site.page.calc-trader-average-loan-amount')</div>
                            <div class="TextInvest2 LineBottom"><span id="credit_value_1">700</span> €</div>
                            <div class="TextInvest1">@lang('site.page.calc-trader-annual-interest-rate')</div>
                            <div class="TextInvest2 LineBottom"><span id="percent_1">5</span>%</div>
                            <div class="TextInvest1">@lang('site.page.cabinet.Maximum-allowable-purchase')</div>
                            @if($activeFirsts->isNotEmpty())
                                <div class="TextInvest2" id="value_gold">
                                    <span id="value_symbol1">{{ $activeFirsts->first()->coinValue ? $activeFirsts->first()->coinValue->symbol : ''}}</span>
                                    <span id="value_price1">{{ $activeFirsts->first()->coinValue ? numeric_fmt(50/$activeFirsts->first()->coinValue->price) : 0}}</span>
                                </div>
                            @endif
                        </div>
                        @if($user->is_verification && $user->credit_level_id && !$user->isBlock())
                            <div class="text-center MarginBot25">
                                <a href="#" class="btns-orange btn-ActiveLine" data-type="1">@lang('site.page.cabinet.Purchase-an-asset')</a>
                            </div>
                        @endif
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="BoxCalculator">
                            <h3>@lang('site.page.cabinet.Collateral-asset'):</h3>
                            <div class="form-group">
                                <select id="asset-virtual" {{ $user->isBlock() ? 'disabled' : ''}}>
                                    @foreach($activeSeconds as $active)
                                        <option value="{{ $active->id }}" data-price="{{ $active->coinValue ? $active->coinValue->price : 0 }}" data-symbol="{{ $active->coinValue ? $active->coinValue->symbol : '' }}">{{ $active->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <h3>@lang('site.page.form-trader-loan-month2')</h3>
                            <div class="form-group">
                                <input type="text" class="RangeSliderMonth" name="RangeSliderMonth" value="" id="RangeSliderMonth2">
                            </div>
                            <h3>@lang('site.page.cabinet.Deposit-amount'):</h3>
                            <div class="form-group">
                                <input type="text" class="RangeSliderMoney1" name="RangeSliderMoney2" value="" id="RangeSliderMoney2">
                            </div>
                            <input type="text" class="RangeSliderMoney21" name="RangeSliderMoney21" id="RangeSliderMoney21" value="50">
                        </div>
                        <div class="BoxInvesMaterial">
                            <div class="HeadInvest">@lang('site.page.calc-trader-virtual-investment-assets')</div>
                            <div class="TextInvest1">@lang('site.page.cabinet.Credit-Level')</div>
                            <div class="TextInvest2 LineBottom" id="creditLevelText2">Золотой</div>
                            <div class="TextInvest1">@lang('site.page.calc-trader-average-loan-amount')</div>
                            <div class="TextInvest2 LineBottom"><span id="credit_value_2">700</span> €</div>
                            <div class="TextInvest1">@lang('site.page.calc-trader-annual-interest-rate')</div>
                            <div class="TextInvest2 LineBottom"><span id="percent_2">5</span>%</div>
                            <div class="TextInvest1">@lang('site.page.cabinet.Maximum-allowable-purchase')</div>
                            @if($activeSeconds->isNotEmpty())
                                <div class="TextInvest2" id="value_btc">
                                    <span id="value_symbol2">{{ $activeSeconds->first()->coinValue ? $activeSeconds->first()->coinValue->symbol : ''}}</span>
                                    <span id="value_price2">{{ $activeSeconds->first()->coinValue ? numeric_fmt(50/$activeSeconds->first()->coinValue->price) : 0}}</span>
                                </div>
                            @endif
                        </div>
                        @if($user->is_verification && $user->credit_level_id && !$user->isBlock())
                            <div class="text-center MarginBot25" id="buy-asset">
                                <a href="#" class="btns-orange btn-ActiveLine" data-type="2">@lang('site.page.cabinet.Purchase-an-asset')</a>
                            </div>
                        @endif
                    </div>
                </div>

                @if($activeFirsts->isNotEmpty() || $activeSeconds->isNotEmpty())
                <h3 class="MarginTop30">@lang('site.page.cabinet.Calculation-form')</h3>
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        @if($activeFirsts->isNotEmpty())
                        <div class="BoxGrap" id="graph-wrap1">
                            <div class="row">
                                <div class="col-6">
                                    <h3>{{ $activeFirsts->first()->coinValue ? $activeFirsts->first()->coinValue->symbol . 'EUR' : ''}}</h3>
                                </div>
                                <div class="col-6 text-right">
                                    @php
                                        $y = json_decode($activeFirsts->first()->coinValue->ohlcv_y, true);
                                        $y = !empty($y) ? array_pop($y) : 0
                                    @endphp
                                    <span>{{format_number($y)}}</span>
                                </div>
                            </div>

                            <div style="height:250px">
                                <canvas id="GraphHomeCab1"></canvas>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        @if($activeSeconds->isNotEmpty())
                        <div class="BoxGrap" id="graph-wrap2">
                            <div class="row">
                                <div class="col-6">
                                    <h3>{{ $activeSeconds->first()->coinValue ? $activeSeconds->first()->coinValue->symbol . 'EUR' : ''}}</h3>
                                </div>
                                <div class="col-6 text-right">
                                    @php
                                        $y = json_decode($activeSeconds->first()->coinValue->ohlcv_y, true);
                                        $y = !empty($y) ? array_pop($y) : 0
                                    @endphp
                                    <span>{{format_number($y)}}</span>
                                </div>
                            </div>
                            <div style="height:250px">
                                <canvas id="GraphHomeCab2"></canvas>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection

@section('script')
    @if($user->isBlock())
        <script>
            $(document).ready(function(){
                $("#BlockUser").modal("show");
            });
        </script>
    @endif

    @if(request()->has('q1'))
    <script>
        $(document).ready(function(){
            $("#Questionnaire1").modal("show");
        });
    </script>
    @endif
    @if(request()->has('q2'))
    <script>
        $(document).ready(function(){
            $("#Questionnaire2").modal("show");
        });
    </script>
    @endif
    @if(request()->has('q3'))
    <script>
        $(document).ready(function(){
            $("#Questionnaire3").modal("show");
        });
    </script>
    @endif
    @if(request()->has('q4'))
    <script>
        $(document).ready(function(){
            $("#Questionnaire4").modal("show");
        });
    </script>
    @endif
    @if(request()->has('q5'))
    <script>
        $(document).ready(function(){
            $("#Questionnaire5").modal("show");
        });
    </script>
    @endif
    @if ($errors->has('agree'))
        <script>
            $(document).ready(function(){
                $("#ActiveLine").modal("show");
            });
        </script>
    @elseif ($errors->has('amount') )
        <script>
            $(document).ready(function(){
                $("#VerificationLine").modal("show");
                $("#CreditLine").modal("show");
            });
        </script>
    @endif

    <script src="/js/Chart.min.js"></script>

    @if($activeFirsts->first() && $activeFirsts->first()->coinValue && $activeFirsts->first()->coinValue->ohlcv_x && $activeFirsts->first()->coinValue->ohlcv_y)
        <script>
            var Chart1 = setGraphic("GraphHomeCab1", {!! $activeFirsts->first()->coinValue->ohlcv_y !!}, {!! $activeFirsts->first()->coinValue->ohlcv_x !!}, 'rgba(247,147,26,.5)', 'rgba(247,147,26, 1)', true, true);
        </script>
    @endif

    @if($activeSeconds->first() && $activeSeconds->first()->coinValue && $activeSeconds->first()->coinValue->ohlcv_x && $activeSeconds->first()->coinValue->ohlcv_y)
        <script>
            var Chart2 = setGraphic("GraphHomeCab2", {!! $activeSeconds->first()->coinValue->ohlcv_y !!}, {!! $activeSeconds->first()->coinValue->ohlcv_x !!}, 'rgba(101, 164, 94, .5)', 'rgba(101, 164, 94, 1)', true, true);
        </script>
    @endif

    <script type="text/javascript">
        function printContract() {
            $.ajax({
                url: "{{ route('front.cabinet.trader.home.traderDocumentPdf') }}",
                method: 'POST',
                data: {text: $('#Text-Dogovor').html()}
            }).done(function(response){
                var win = window.open(response, '_blank');
                win.focus();
            });
        }

        //Calculator
        function calc(amount, month, percent) {
            return amount;
            // let p = percent / 12;
            // let value = amount / (p + (p / (Math.pow((1 + p), month) - 1)));
            // return value.toFixed(2);
        }
        function setValueCalc(onload) {
            $('#RangeSliderMoney11').val($('#RangeSliderMoney1').val()*1);
            $('#RangeSliderMoney21').val($('#RangeSliderMoney2').val()*1);
            $.ajax({
                type: "POST",
                url: '{{ route('front.home.calcTrader') }}',
                data: {amount: $('#RangeSliderMoney1').val()},
                success: function (result) {
                    if(result.hasOwnProperty('name')){
                        let value = calc($('#RangeSliderMoney1').val()*1, $('#RangeSliderMonth1').val()*1, result.value * 1);
                        $('#percent_1').text(result.value);
                        $('#credit_value_1').text(value);
                        $('#creditLevelText1').text(result.name);
                        let value_gold = value / $('#asset-natural').find(':selected').data('price')*1;
                        $('#value_price1').text(value_gold.toFixed(8));
                    }
                }
            });
            $.ajax({
                type: "POST",
                url: '{{ route('front.home.calcTrader') }}',
                data: {amount: $('#RangeSliderMoney2').val(), asset:$('#asset-virtual').val(), onload:onload},
                success: function (result) {
                    if(result.hasOwnProperty('name')){
                        let value2 = calc($('#RangeSliderMoney2').val()*1, $('#RangeSliderMonth2').val()*1, result.value * 1);
                        $('#percent_2').text(result.value);
                        $('#credit_value_2').text(value2);
                        $('#creditLevelText2').text(result.name);
                        let value_btc = value2 / $('#asset-virtual').find(':selected').data('price')*1;
                        $('#value_price2').text(value_btc.toFixed(8));
                    }
                }
            });
        }
        $(document).ready(function(){
            $('#RangeSliderMonth1').data("ionRangeSlider").update({
                onFinish: function (data) {
                    setValueCalc();
                },
            });
            $('#RangeSliderMoney1').data("ionRangeSlider").update({
                onFinish: function (data) {
                    setValueCalc();
                },
            });
            $('#RangeSliderMonth2').data("ionRangeSlider").update({
                onFinish: function (data) {
                    setValueCalc();
                },
            });
            $('#RangeSliderMoney2').data("ionRangeSlider").update({
                onFinish: function (data) {
                    setValueCalc();
                },
            });

            setValueCalc();
            $('#RangeSliderMoney11').on('change', function () {
                $('#RangeSliderMoney1').data("ionRangeSlider").update({
                    from: $(this).val()
                });
                setValueCalc();
            });
            $('#RangeSliderMoney21').on('change', function () {
                $('#RangeSliderMoney2').data("ionRangeSlider").update({
                    from: $(this).val()
                });
                setValueCalc();
            });

            $('#asset-natural').on('change', function () {
                let value = $('#RangeSliderMoney1').val()*1 / $(this).find(':selected').data('price')*1;
                $('#value_symbol1').text($(this).find(':selected').data('symbol'));
                $('#value_price1').text(value.toFixed(8));
                $.ajax({
                    type: "POST",
                    url: '{{ route('front.cabinet.trader.home.graphTrader') }}',
                    data: {asset:$('#asset-natural').val(), color:1},
                    success: function (res) {
                        Chart1.destroy();
                        $('#graph-wrap1 h3').html(res.symbol)
                        $('#graph-wrap1 span').html((res.ohlcvY[res.ohlcvY.length-1]).toFixed(2))
                        $('#GraphHomeCab1').html('')
                        if(res.color == 1){
                            Chart1 = setGraphic('GraphHomeCab1', res.ohlcvY, res.ohlcvX, 'rgba(247,147,26,.5)', 'rgba(247,147,26, 1)', true, true);
                        } else {
                            Chart1 = setGraphic('GraphHomeCab1', res.ohlcvY, res.ohlcvX, 'rgba(101, 164, 94,.5)', 'rgba(101, 164, 94, 1)', true, true);
                        }
                    }
                });
            });
            $('#asset-virtual').on('change', function () {
                let value = $('#RangeSliderMoney2').val()*1 / $(this).find(':selected').data('price')*1;
                $('#value_symbol2').text($(this).find(':selected').data('symbol'));
                $('#value_price2').text(value.toFixed(8));
                $.ajax({
                    type: "POST",
                    url: '{{ route('front.cabinet.trader.home.graphTrader') }}',
                    data: {asset:$('#asset-virtual').val(), color:2},
                    success: function (res) {
                        Chart1.destroy();
                        $('#graph-wrap2 h3').html(res.symbol)
                        $('#graph-wrap2 span').html((res.ohlcvY[res.ohlcvY.length-1]).toFixed)
                        $('#GraphHomeCab2').html('')
                        if(res.color == 1){
                            Chart1 = setGraphic('GraphHomeCab2', res.ohlcvY, res.ohlcvX, 'rgba(247,147,26,.5)', 'rgba(247,147,26, 1)', true, true);
                        } else {
                            Chart1 = setGraphic('GraphHomeCab2', res.ohlcvY, res.ohlcvX, 'rgba(101, 164, 94,.5)', 'rgba(101, 164, 94, 1)', true, true);
                        }
                    }
                });
            });

            $('.btn-ActiveLine').on('click', function(){
                let type = $(this).data('type');
                let balance_trader = {{ $user->balance_trader }};
                let amount,period,type_asset_text,type_asset_id,rate;
                if (type == 1) {
                    amount = $('#RangeSliderMoney1').val();
                    period = $('#RangeSliderMonth1').val();
                    type_asset_text = $("#asset-natural option:selected").text();
                    type_asset_id = $('#asset-natural').val();
                    rate = $('#percent_1').text();
                } else {
                    amount = $('#RangeSliderMoney2').val();
                    period = $('#RangeSliderMonth2').val();
                    type_asset_text = $("#asset-virtual option:selected").text();
                    type_asset_id = $('#asset-virtual').val();
                    rate = $('#percent_2').text();
                }

                if(balance_trader < amount){
                    sendMessage('{{trans('site.page.cabinet.sidebar-payot-error-amount')}}');
                    return;
                }

                $("#ActiveLine").modal("show");

                $('#amount_text').text(amount);
                $('#amount').val(amount);
                $('#type_asset_text').text(type_asset_text);
                $('#type_asset_text2').val(type_asset_text);
                $('#type_asset_id').val(type_asset_id);
                $('#period_text').text(period);
                $('#period').val(period);
                $('#rate_text').text(rate);
                $('#rate').val(rate);

                $.ajax({
                    type: "POST",
                    url: '{{ route('front.cabinet.trader.home.traderDocument') }}',
                    data: {period: period, asset:type_asset_id, amount:amount, rate:rate},
                    success: function (result) {
                        if(result.hasOwnProperty('text')){
                            $('#Text-Dogovor').html(result.text);
                        }
                    }
                });
            });
        });
    </script>

    <script type="text/javascript">
        function CopyReff() {
            var copyText = document.getElementById("CopyReff");
            copyText.select();
            document.execCommand("copy");
            alert("Copied the text: " + copyText.value);
        }
    </script>
@endsection
