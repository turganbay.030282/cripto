<h3>{{ $symbol }}EUR</h3>
<div style="height:250px">
    <canvas id="{{ $id }}"></canvas>
</div>
<script>
    @if($color == 1)
        setGraphic('{{ $id }}', {{ $ohlcv_y }}, {{ $ohlcv_x }}, 'rgba(247,147,26,.5)', 'rgba(247,147,26, 1)', true, true);
    @else
        setGraphic('{{ $id }}', {{ $ohlcv_y }}, {{ $ohlcv_x }}, 'rgba(101, 164, 94,.5)', 'rgba(101, 164, 94, 1)', true, true);
    @endif
</script>
