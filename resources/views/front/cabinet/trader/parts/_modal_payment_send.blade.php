<!-- Modal Doposit1 -->
<div class="modal HeaderBlue" id="{{ isset($idModal) ? $idModal : 'DogovorPayment' }}" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">{{ trans('site.page.form-payment-title') }}</h2>
                {!! Form::open(['url' => isset($url) ? $url : route('front.cabinet.trader.contracts.payContract', $contract), 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                    <div class="BoxPaddActive">
                        <div class="row">
                            <div class="col-5">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.cabinet.Contract') }}</h6>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="RightFormText d-flex align-items-center">
                                    <h6>{{ $contract->number }}</h6>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.form-payment-amount') }}</h6>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="RightFormText d-flex align-items-center">
                                    <h6>{{ isset($contractTraderAmount) ? $contractTraderAmount : format_number($contract->calcTraderAmount()) }}</h6>
                                </div>
                            </div>
                        </div>
                        @php
                            $currentContractPayment = $contract->getCurrentContractPayment();
                        @endphp
                        @if($currentContractPayment && $currentContractPayment->penalty)
                        <div class="row">
                            <div class="col-5">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.form-payment-penalty') }}</h6>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="RightFormText d-flex align-items-center">
                                    <h6>{{ $currentContractPayment->penalty }}</h6>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.form-payment-amount-all') }}</h6>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="RightFormText d-flex align-items-center">
                                    <h6>{{ isset($contractTraderAmount) ? $contractTraderAmount + $currentContractPayment->penalty : format_number($contract->calcTraderAmount() + $currentContractPayment->penalty) }}</h6>
                                </div>
                            </div>
                        </div>
                        @endif

                        <h6 class="Bold MarginTop30">{{ trans('site.page.form-payment-number-pay') }}:</h6>
                        <div class="LeftFormText MarginBot25">
                            <div class="IcoCopy" onclick="CopyToBuffer();">
                                <input type="text" value="{{ auth()->user()->contract_payment_number }}" id="copy-buffer" readonly>
                                <span></span>
                            </div>
                        </div>
                        <h6 class="Bold MarginTop30">{{ trans('site.page.form-payment-iban') }}:</h6>
                        <div class="LeftFormText MarginBot25">
                            <div class="IcoCopy" onclick="CopyToBuffer2();">
                                <input type="text" value="{{ trans_field($setting, 'req_iban') }}" id="copy-buffer2">
                                <span></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.form-payment-name') }}:</h6>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="RightFormText d-flex align-items-center">
                                    <h6>{{ trans_field($setting, 'req_sia') }}</h6>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.form-payment-country-bank') }}</h6>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="RightFormText d-flex align-items-center">
                                    <h6>Estonia</h6>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.form-payment-name-bank') }}</h6>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="RightFormText d-flex align-items-center">
                                    <h6>{{ trans_field($setting, 'req_seb') }}</h6>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.form-payment-address-delivery') }}</h6>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="RightFormText d-flex align-items-center">
                                    <h6>{{ trans_field($setting, 'req_reg') }}</h6>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.form-payment-swift') }}</h6>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="RightFormText d-flex align-items-center">
                                    <h6>{{ trans_field($setting, 'req_swift') }}</h6>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.form-payment-bank_address') }}</h6>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="RightFormText d-flex align-items-center">
                                    <h6>{{ trans_field($setting, 'req_bank_address') }}</h6>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.cabinet.contract-pay-ticket'):</h6>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="RightFormText d-flex align-items-center">
                                    <input type="file" required id="ticket-file" name="pay_ticket" data-placeholder="{{ trans('site.cabinet.form-browse-file') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center MarginTop15 BTNWidth100">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="ResponsiveMarginBot15">
                                        <button class="btns-red" data-dismiss="modal" aria-label="Close">{{ trans('site.page.cabinet.Cancel') }}</button>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <button class="btns-green" id="send-pay-ticket">{{ trans('site.page.cabinet.Pay') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
