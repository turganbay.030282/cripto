<!-- Modal DepositOut -->
<div class="modal HeaderBlue" id="PayOutCoin" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.page.cabinet.sidebar-title-transfer-active')</h2>
                {!! Form::open(['url' => route('front.cabinet.trader.payout.transferAsset'), 'method' => 'POST', 'id' => 'transferAssetForm']) !!}
                {!! Form::hidden('coin', null) !!}
                <div class="BoxPaddActive">
                        <div class="row">
                            <div class="col-xl-4 col-4">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.cabinet.Withdrawal-amount'):</h6>
                                </div>
                            </div>
                            <div class="col-xl-8 col-8">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('', null, ['id' => 'stf-coin-value-transfer', 'readonly' => true]) !!}
                                    {!! Form::hidden('coin_value') !!}
                                </div>
                            </div>
                            <div class="col-xl-4 col-4">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.cabinet.сhoose-wallet-number'):</h6>
                                </div>
                            </div>
                            <div class="col-xl-8 col-8">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    <input type="text" name="wallet" required id="stf-coin-wallet">
                                </div>
                            </div>


                            @include('components.form.sms_code', ['url'=>route('front.cabinet.sms.get.code', ['type'=> \App\Entity\CodeVerifiedField::TYPE_TRANSFER])])

                            @include('components.form.2FA_input')

                        </div>
                        <div class="form-group text-center MarginTop30">
                            <button class="btns-red" data-dismiss="modal" aria-label="Close">@lang('site.page.cabinet.Cancel')</button>
                            <button class="btns-green" id="btn-coin-transfer">@lang('site.page.cabinet.sidebar-btn-transfer-active')</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
