<!-- Modal DepositOut -->
<div class="modal HeaderBlue" id="PayOutEuro" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.page.cabinet.Withdraw-funds')</h2>
                {!! Form::open(['url' => route('front.cabinet.trader.payout.payoutAsset'), 'method' => 'POST']) !!}
                <input type="hidden" name="coin" value="">
                <input type="hidden" name="coin_value" value="">


                <div class="BoxPaddActive">
                    <div class="row">
                        <div class="col-xl-4 col-4">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.cabinet.Withdrawal-amount'):</h6>
                            </div>
                        </div>
                        <div class="col-xl-8 col-8">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                <input type="text" name="amount_tmp" id="m-amount_tmp-euro" placeholder="@lang('site.page.cabinet.Enter-the-amount')" readonly required>
                            </div>
                        </div>
{{--                        <div class="col-xl-4 col-4">--}}
{{--                            <div class="LeftFormText d-flex align-items-center">--}}
{{--                                <h6>@lang('site.page.cabinet.Choose-bank-account'):</h6>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-xl-8 col-8">--}}
{{--                            <div class="form-group LeftFormText d-flex align-items-center">--}}
{{--                                <select name="wallet_to" required>--}}
{{--                                    <option value=""></option>--}}
{{--                                    @foreach(auth()->user()->wallets->pluck('wallet')->all() as $wallet)--}}
{{--                                        <option value="{{$wallet}}">{{ $wallet }}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        @include('components.form.sms_code', ['url'=>route('front.cabinet.sms.get.code', ['type'=> \App\Entity\CodeVerifiedField::TYPE_STAKING_EURO])])

                        @include('components.form.2FA_input')


                    </div>
                    <div class="form-group text-center MarginTop30">
                        <button class="btns-red" data-dismiss="modal" aria-label="Close">@lang('site.page.cabinet.Cancel')</button>
                        <button class="btns-green">@lang('site.page.cabinet.Request')</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('script-modals2')
    <script></script>
@endsection
