<!-- Modal DepositOut -->
<div class="modal HeaderBlue" id="TraderOut" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">{{ trans('site.page.cabinet.trader-payout-title') }}</h2>
                {!! Form::open(['url' => route('front.cabinet.trader.payout'), 'method' => 'POST']) !!}
                    <div class="BoxPaddActive">
                        <div class="row">
                            <div class="col-xl-4 col-4">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.cabinet.trader-payout-amount') }}:</h6>
                                </div>
                            </div>
                            <div class="col-xl-8 col-8">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('amount', null, ['placeholder' => trans('site.page.cabinet.trader-payout-amount-placeholder'), 'required' => true]) !!}
                                    {!! Form::error('amount') !!}
                                </div>
                            </div>
                            <div class="col-xl-4 col-4">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>{{ trans('site.page.cabinet.trader-payout-select-paymethod') }}:</h6>
                                </div>
                            </div>
                            <div class="col-xl-8 col-8">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    <select name="wallet">
                                        <option>{{ auth()->user()->bank_number }}</option>
                                    </select>
                                </div>
                            </div>

                            @include('components.form.sms_code', ['url'=>route('front.cabinet.sms.get.code', ['type'=> \App\Entity\CodeVerifiedField::TYPE_PAYOUT])])
                            @include('components.form.2FA_input')

                        </div>
                        <div class="form-group text-center MarginTop30">
                            <button class="btns-red" data-dismiss="modal" aria-label="Close">@lang('site.page.cabinet.Cancel')</button>
                            <button class="btns-green btn-withdraw" type="submit">{{ trans('site.page.cabinet.trader-payout-btn-request') }}</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
