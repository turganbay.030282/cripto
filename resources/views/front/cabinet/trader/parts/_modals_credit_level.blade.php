<!-- Modal Questionnaire1 -->
<div class="modal" id="Questionnaire1" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-700 modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.cabinet.credit-level-questionnaire')</h2>
                <ul class="StepsQuest">
                    <li class="active">1</li>
                    <li>2</li>
                    <li>3</li>
                </ul>
                <h6 class="Bold">@lang('site.cabinet.credit-level-info-client')</h6>
                <div class="BoxPaddActive">
                    {!! Form::open(['url' => route('front.cabinet.trader.creditLevel.creditLevelFirst'), 'method' => 'POST']) !!}
                        <div class="row">
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.profile.Name')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('first_name', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('first_name') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.profile.LastName')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('last_name', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('last_name') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-info-gender')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::select('sex', \App\Helpers\ListsHelper::sex(), null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('sex') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-info-Personal-code')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('personal_code', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('personal_code') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-info-Family-status')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('family_status', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('family_status') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-info-Political-functions')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::select('political_functions', array_reverse(\App\Helpers\ListsHelper::bool()), null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('political_functions') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-info-Phone-number')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {{ Form::text('full_phone', null, ['id' => 'full_phone', 'required' => true]) }}
                                {{ Form::hidden('phone', null, ['id' => 'reg-phone']) }}
                            </div>
                            <span id="error-msg" class="invalid-feedback" style="color: red;display: none;"><strong>@lang('validation.regex', ['attribute' => 'phone'])</strong></span>
                            {!! Form::error('phone') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-info-Dependents')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('dependents', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('dependents') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-info-Accommodation-type')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::select('type_accommodation', \App\Helpers\ListsHelper::typeAccommodations(), null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('type_accommodation') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-info-Education')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::select('education', \App\Helpers\ListsHelper::educations(), null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('education') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-info-Citizenship')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('citizenship', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('citizenship') !!}
                        </div>
                        <div class="offset-xl-7 col-xl-5 offset-lg-6 col-lg-6 col-md-12 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-green" type="submit">@lang('site.cabinet.credit-level-info-next-step')</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Questionnaire2 -->
<div class="modal" id="Questionnaire2" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-700 modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.cabinet.credit-level-questionnaire')</h2>
                <ul class="StepsQuest">
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="active">2</li>
                    <li>3</li>
                </ul>
                <h6 class="Bold">@lang('site.cabinet.credit-level-Location')</h6>
                {!! Form::open(['url' => route('front.cabinet.trader.creditLevel.creditLevelSecond'), 'method' => 'POST']) !!}
                    <div class="BoxPaddActive">
                    <div class="row">
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-Country')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center maxheight">
                                {!! Form::select('residence_county', $countries, null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('residence_county') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-City')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('residence_city', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('residence_city') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-Street')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('residence_street', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('residence_street') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-postcode')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('residence_postcode', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('residence_postcode') !!}
                        </div>
                    </div>
                    <div class="Line"></div>
                    <h6 class="Bold">@lang('site.cabinet.credit-level-Income-information')</h6>
                    <div class="row">
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-Income-Source')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::select('income_source', \App\Helpers\ListsHelper::incomeSource(), null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('income_source') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-Income-Net')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('income_net', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('income_net') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-Income-Additional')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('income_additional', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('income_additional') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-Place-work')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('income_place', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('income_place') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-Position')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('income_position', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('income_position') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-experience-job')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('income_work_experience', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('income_work_experience') !!}
                        </div>
                        <div class="offset-xl-7 col-xl-5 offset-lg-6 col-lg-6 col-md-12 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-green" type="submit">@lang('site.cabinet.credit-level-info-next-step')</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<!-- Modal Questionnaire3 -->
<div class="modal" id="Questionnaire3" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-700 modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.cabinet.credit-level-questionnaire')</h2>
                <ul class="StepsQuest">
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="active">3</li>
                </ul>
                <h6 class="Bold">@lang('site.cabinet.credit-level-Credit-liabilities')</h6>
                {!! Form::open(['url' => route('front.cabinet.trader.creditLevel.creditLevelFinish'), 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                    <div class="BoxPaddActive">
                    <div class="row">
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-have-credit-obligations')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                <select name="credit_liabilities" id="credit_liabilities">
                                    <option value="no">@lang('site.cabinet.credit-level-no')</option>
                                    <option value="yes">@lang('site.cabinet.credit-level-yes')</option>
                                </select>
                            </div>
                            {!! Form::error('credit_liabilities') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-Monthly-payment')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('credit_liabilities_month', null, ['class' => 'input-required']) !!}
                            </div>
                            {!! Form::error('credit_liabilities_month') !!}
                        </div>
                    </div>

                    <div class="loan-block hide">
                        <div class="Line"></div>
                        <h6 class="Bold">@lang('site.cabinet.credit-level-Loan-information')</h6>
                        <div class="row">
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.cabinet.credit-level-Loan-Select-type')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::select('credit_type', \App\Helpers\ListsHelper::creditType(), null, ['class' => 'input-required']) !!}
                                </div>
                                {!! Form::error('credit_type') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.cabinet.credit-level-Credit-term')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    <div class="row MyCollum15">
                                        <div class="col-6 MyCollum15">
                                            {!! Form::text('credit_date_from', null, ['class' => 'picker-date input-required', 'placeholder' => trans('site.cabinet.credit-level-Credit-From')]) !!}
                                            {!! Form::error('credit_date_from') !!}
                                        </div>
                                        <div class="col-6 MyCollum15">
                                            {!! Form::text('credit_date_to', null, ['class' => 'picker-date input-required', 'placeholder' => trans('site.cabinet.credit-level-Credit-To')]) !!}
                                            {!! Form::error('credit_date_to') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="Line"></div>
                    <h6 class="Bold">@lang('site.cabinet.credit-level-Account-statement')</h6>
                    <div class="DescripM">@lang('site.cabinet.credit-level-quicker-consideration'):</div>
                    <div class="DescripM">1. @lang('site.cabinet.credit-level-statement-last-6').</div>
                    <div class="row">
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::file('file_statement', ['data-placeholder' => trans('site.cabinet.credit-level-Attach-PDF'),  'id' => 'crl-file_statement']) !!}
                            </div>
                            {!! Form::error('file_statement') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-uploaded-documents')</h6>
                            </div>
                        </div>
                    </div>
                    <div class="DescripM">2. @lang('site.cabinet.credit-level-Certificate-place-work').</div>
                    <div class="row">
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::file('file_income_work', ['data-placeholder' => trans('site.cabinet.credit-level-Attach-PDF'),  'id' => 'crl-file_income_work']) !!}
                            </div>
                            {!! Form::error('file_income_work') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-uploaded-documents')</h6>
                            </div>
                        </div>
                    </div>
                    <div class="DescripM">3. @lang('site.cabinet.credit-level-Bill-utilities').</div>
                    <div class="row">
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::file('file_utility_bill', ['data-placeholder' => trans('site.cabinet.credit-level-Attach-PDF'),  'id' => 'crl-file_utility_bill']) !!}
                            </div>
                            {!! Form::error('file_utility_bill') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-uploaded-documents')</h6>
                            </div>
                        </div>
                    </div>
                    <h6 class="Bold">@lang('site.cabinet.credit-level-Provide-two-doc').</h6>
                    <div class="Line"></div>
                    <div class="row">
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.cabinet.Credit-amount')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('amount', null, ['required' => true,  'id' => 'crl-amount']) !!}
                            </div>
                            {!! Form::error('amount') !!}
                        </div>
                    </div>
                    <div class="ModCheck">
                        <label>
                            <input class="Mycheckbox" type="checkbox" name="accepted" id="crl-accepted">
                            <div class="TextCheck">@lang('site.cabinet.credit-level-agree')</div>
                        </label>
                        {!! Form::error('accepted') !!}
                    </div>
                    <div class="text-center">
                        <button class="btns-green btn-send" type="submit">@lang('site.page.form-btn-send')</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<!-- Modal Questionnaire4 -->
<div class="modal" id="Questionnaire4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-700 modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.cabinet.credit-level-questionnaire')</h2>
                <ul class="StepsQuest">
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="done"><i class="fas fa-check"></i></li>
                </ul>
                <h4 class="Bold text-center PaddingBot50">@lang('site.cabinet.credit-level-Thank-you')</h4>
            </div>
        </div>
    </div>
</div>

@section('style2')
    <link rel="stylesheet" href="/js/intl-tel-input/css/intlTelInput.css">
@endsection

@section('script2')
    <script src="/js/intl-tel-input/js/intlTelInput-jquery.js"></script>
    <script>
        $(document).ready(function(){
            $('#credit_liabilities').on('change', function (e) {
                if ($(this).val() == 'no') {
                    $('.input-required').prop('required', false);
                    $('.loan-block').addClass('hide');
                } else {
                    $('.input-required').prop('required', true);
                    $('.loan-block').removeClass('hide');
                }
            });

            $('.btn-send').prop('disabled', true);
            $('#Questionnaire3 input').on('change', function (e) {
                let countFile = 0;
                if ($('#crl-file_statement').val()) {
                    countFile += 1;
                }
                if ($('#crl-file_income_work').val()) {
                    countFile += 1;
                }
                if ($('#crl-file_utility_bill').val()) {
                    countFile += 1;
                }

                if (countFile >= 2 && $('#crl-amount').val() != '' && $('#crl-accepted').is(':checked')) {
                    $('.btn-send').prop('disabled', false);
                } else {
                    $('.btn-send').prop('disabled', true);
                }
            });

            let telInput = $("#full_phone")
            telInput.intlTelInput({
                initialCountry: "auto",
                separateDialCode:true,
                autoHideDialCode:false,
                preferredCountries: ['ee'],
                excludeCountries: ['kp', 'ir', 'af', 'bs', 'bb', 'bw', 'kh', 'gh', 'iq',
                    'jm', 'mu', 'mm', 'ni', 'pk', 'pa', 'sy', 'tt', 'ug', 'vu', 'ye', 'zw',
                    'al', 'is', 'mn'],
                geoIpLookup: function(success, failure) {
                    $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "ee";
                        success(countryCode);
                    });
                },
                utilsScript: "/js/intl-tel-input/js/utils.js"
            });
            telInput.blur(function () {
                if ($.trim(telInput.val())) {
                    if (telInput.intlTelInput('isValidNumber')) {
                        $('#error-msg').hide();
                        $('#reg-phone').val(telInput.intlTelInput('getNumber'));
                    } else {
                        $('#error-msg').show();
                    }
                }
            });
            telInput.keydown(function () {
                $('#error-msg').hide();
            });
        });
    </script>
@endsection
