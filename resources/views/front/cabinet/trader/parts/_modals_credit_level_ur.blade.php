<!-- Modal Questionnaire1 -->
<div class="modal" id="Questionnaire1" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-700 modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.cabinet.credit-level-questionnaire')</h2>
                <ul class="StepsQuest StepsFives">
                    <li class="active">1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>4</li>
                    <li>5</li>
                </ul>
                <h6 class="Bold">@lang('site.cabinet.credit-level-info-client')</h6>
                <div class="BoxPaddActive">
                    {!! Form::open(['url' => route('front.cabinet.trader.creditLevel.creditLevelFirstUr'), 'method' => 'POST']) !!}
                        <div class="row">
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-company_name')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('company_name', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('company_name') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-reg_number')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('reg_number', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('reg_number') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-register_date')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('register_date', null, ['class' => 'picker-date input-required', 'required' => true]) !!}
                            </div>
                            {!! Form::error('register_date') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-ur_address')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('ur_address', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('ur_address') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-address')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('address', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('address') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-contact_phone')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('contact_phone', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('contact_phone') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-email')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('email', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('email') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-primary_occupation')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('primary_occupation', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('primary_occupation') !!}
                        </div>
                        <div class="offset-xl-7 col-xl-5 offset-lg-6 col-lg-6 col-md-12 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-green" type="submit">@lang('site.cabinet.credit-level-info-next-step')</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal Questionnaire2 -->
<div class="modal" id="Questionnaire2" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-700 modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.cabinet.credit-level-questionnaire')</h2>
                <ul class="StepsQuest StepsFives">
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="active">2</li>
                    <li>3</li>
                    <li>4</li>
                    <li>5</li>
                </ul>
                <h6 class="Bold">@lang('site.page.credit-level-ur-title21')</h6>
                {!! Form::open(['url' => route('front.cabinet.trader.creditLevel.creditLevelSecondUr'), 'method' => 'POST']) !!}
                    <div class="BoxPaddActive">
                        <div class="block-owners">
                            @if(old('name'))
                                @foreach(old('name') as $key => $name)
                                    @include('front.cabinet.trader.parts._modals_credit_level_ur_person', ['row' => $key])
                                @endforeach
                            @else
                                @include('front.cabinet.trader.parts._modals_credit_level_ur_person', ['row' => 0])
                            @endif
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <button class="btns-green" type="button" id="cl-add-owners">@lang('site.page.credit-level-ur-add-owners')</button>
                                    <button class="btns-orange" type="button" id="cl-remove-owners">@lang('site.page.modal.btn-delete-block')</button>
                                </div>
                            </div>
                        </div>

                        <div class="Line"></div>
                        <h6 class="Bold">@lang('site.page.credit-level-ur-title22')</h6>

                        <div class="row">
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.credit-level-ur-person_name')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('person_name', null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('person_name') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.credit-level-ur-person_surname')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('person_surname', null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('person_surname') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.credit-level-ur-person_personal_code')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('person_personal_code', null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('person_personal_code') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.credit-level-ur-person_turnover_prev')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('person_turnover_prev', null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('person_turnover_prev') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.credit-level-ur-person_profit_prev')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('person_profit_prev', null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('person_profit_prev') !!}
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                                <div class="LeftFormText d-flex align-items-center">
                                    <h6>@lang('site.page.credit-level-ur-person_banks')</h6>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                                <div class="form-group LeftFormText d-flex align-items-center">
                                    {!! Form::text('person_banks', null, ['required' => true]) !!}
                                </div>
                                {!! Form::error('person_banks') !!}
                            </div>

                            <div class="offset-xl-7 col-xl-5 offset-lg-6 col-lg-6 col-md-12 col-12">
                                <div class="BTNWidth100">
                                    <button class="btns-green" type="submit">@lang('site.cabinet.credit-level-info-next-step')</button>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


<!-- Modal Questionnaire3 -->
<div class="modal" id="Questionnaire3" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-700 modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.cabinet.credit-level-questionnaire')</h2>
                <ul class="StepsQuest StepsFives">
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="active">3</li>
                    <li>4</li>
                    <li>5</li>
                </ul>
                <h6 class="Bold">@lang('site.page.credit-level-ur-title3')</h6>
                {!! Form::open(['url' => route('front.cabinet.trader.creditLevel.creditLevelThirdUr'), 'method' => 'POST']) !!}
                <div class="BoxPaddActive">
                    <div class="row">
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-creditor')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('creditor', null, ['required' => true, 'id' => 'creditor']) !!}
                            </div>
                            {!! Form::error('creditor') !!}
                        </div>

                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-start_amount')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('start_amount', null, ['required' => true, 'id' => 'start_amount']) !!}
                            </div>
                            {!! Form::error('start_amount') !!}
                        </div>

                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-balance_obligations')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('balance_obligations', null, ['required' => true, 'id' => 'balance_obligations']) !!}
                            </div>
                            {!! Form::error('balance_obligations') !!}
                        </div>

                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-credit_date_to')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('credit_date_to', null, ['class' => 'picker-date input-required', 'required' => true, 'id' => 'credit_date_to']) !!}
                            </div>
                            {!! Form::error('credit_date_to') !!}
                        </div>

                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-month_payments')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('month_payments', null, ['required' => true, 'id' => 'month_payments']) !!}
                            </div>
                            {!! Form::error('month_payments') !!}
                        </div>

                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-credit_target')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('credit_target', null, ['required' => true, 'id' => 'credit_target']) !!}
                            </div>
                            {!! Form::error('credit_target') !!}
                        </div>

                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.credit-level-ur-credit_source')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('credit_source', null, ['required' => true, 'id' => 'credit_source']) !!}
                            </div>
                            {!! Form::error('credit_source') !!}
                        </div>

                        <div class="col-xl-col-12">
                            <div class="ModCheck">
                                <label>
                                    <input class="Mycheckbox" type="checkbox" id="not-credit" name="not_credit" value="1">
                                    <div class="TextCheck">@lang('site.page.credit-level-ur-not-credit_source')</div>
                                </label>
                            </div>
                        </div>

                        <div class="offset-xl-7 col-xl-5 offset-lg-6 col-lg-6 col-md-12 col-12">
                            <div class="BTNWidth100">
                                <button class="btns-green" type="submit">@lang('site.cabinet.credit-level-info-next-step')</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


<!-- Modal Questionnaire4 -->
<div class="modal" id="Questionnaire4" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-700 modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.cabinet.credit-level-questionnaire')</h2>
                <ul class="StepsQuest StepsFives">
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="active">4</li>
                    <li>5</li>
                </ul>
                <h6 class="Bold">@lang('site.cabinet.credit-level-Account-statement')</h6>
                {!! Form::open(['url' => route('front.cabinet.trader.creditLevel.creditLevelFinishUr'), 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                    <div class="DescripM">@lang('site.cabinet.credit-level-quicker-consideration'):</div>
                    <div class="DescripM">1. @lang('site.cabinet.credit-level-statement-last-6').</div>
                    <div class="row">
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::file('file_statement', ['data-placeholder' => trans('site.cabinet.credit-level-Attach-PDF')]) !!}
                            </div>
                            {!! Form::error('file_statement') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-uploaded-documents')</h6>
                            </div>
                        </div>
                    </div>
                    <div class="DescripM">2. @lang('site.cabinet.credit-level-Certificate-place-work').</div>
                    <div class="row">
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::file('file_income_work', ['data-placeholder' => trans('site.cabinet.credit-level-Attach-PDF')]) !!}
                            </div>
                            {!! Form::error('file_income_work') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-uploaded-documents')</h6>
                            </div>
                        </div>
                    </div>
                    <div class="DescripM">3. @lang('site.cabinet.credit-level-Bill-utilities').</div>
                    <div class="row">
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::file('file_utility_bill', ['data-placeholder' => trans('site.cabinet.credit-level-Attach-PDF')]) !!}
                            </div>
                            {!! Form::error('file_utility_bill') !!}
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.cabinet.credit-level-uploaded-documents')</h6>
                            </div>
                        </div>
                    </div>
                    <h6 class="Bold">@lang('site.cabinet.credit-level-Provide-two-doc').</h6>
                    {!! Form::error('files_selected') !!}

                    <div class="Line"></div>
                    <div class="row">
                        <div class="col-xl-7 col-lg-6 col-md-12 col-12">
                            <div class="LeftFormText d-flex align-items-center">
                                <h6>@lang('site.page.cabinet.Credit-amount')</h6>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-12 col-12">
                            <div class="form-group LeftFormText d-flex align-items-center">
                                {!! Form::text('amount', null, ['required' => true]) !!}
                            </div>
                            {!! Form::error('amount') !!}
                        </div>
                    </div>
                    <div class="ModCheck">
                        <label>
                            <input class="Mycheckbox" type="checkbox" name="accepted">
                            <div class="TextCheck">@lang('site.cabinet.credit-level-agree')</div>
                        </label>
                        {!! Form::error('accepted') !!}
                    </div>
                    <div class="text-center">
                        <button class="btns-green" type="submit">@lang('site.page.form-btn-send')</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


<!-- Modal Questionnaire5 -->
<div class="modal" id="Questionnaire5" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-700 modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="text-center">@lang('site.cabinet.credit-level-questionnaire')</h2>
                <ul class="StepsQuest StepsFives">
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="done"><i class="fas fa-check"></i></li>
                    <li class="done"><i class="fas fa-check"></i></li>
                </ul>
                <h4 class="Bold text-center PaddingBot50">@lang('site.cabinet.credit-level-Thank-you')</h4>
            </div>
        </div>
    </div>
</div>

@section('script2')
    <script>
        $(document).ready(function(){
            $('#is_conformity-styler').on('click', function (e) {
                if($("#is_conformity").prop('checked')){
                    $('#company_ur_address').val($('#company_address').val());
                }
            });

            $('#cl-add-owners').on('click', function(e){
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: '{{ route('front.cabinet.trader.creditLevel.creditLevelFinishUrRow') }}',
                    data: {row:$('.block-owners .row').last().data('row')},
                    success: function (result) {
                        $('.block-owners').append(result);
                    }
                });
            });

            $('#cl-remove-owners').on('click', function(e){
                e.preventDefault();
                $('.block-owners .row').last().remove();
            });

            $('#not-credit-styler').on('click', function(e){
                if($("#not-credit").prop('checked')){
                    $('#creditor').prop('required', false);
                    $('#start_amount').prop('required', false);
                    $('#balance_obligations').prop('required', false);
                    $('#credit_date_to').prop('required', false);
                    $('#month_payments').prop('required', false);
                    $('#credit_target').prop('required', false);
                    $('#credit_source').prop('required', false);
                }else{
                    $('#creditor').prop('required', true);
                    $('#start_amount').prop('required', true);
                    $('#balance_obligations').prop('required', true);
                    $('#credit_date_to').prop('required', true);
                    $('#month_payments').prop('required', true);
                    $('#credit_target').prop('required', true);
                    $('#credit_source').prop('required', true);
                }
            });
        });
    </script>
@endsection
