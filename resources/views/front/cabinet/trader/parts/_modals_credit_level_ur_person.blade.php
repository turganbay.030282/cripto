<div class="row" data-row="{{ $row }}">
    <div class="col-xl-7 col-lg-6 col-md-12 col-12">
        <div class="LeftFormText d-flex align-items-center">
            <h6>@lang('site.page.credit-level-ur-name')</h6>
        </div>
    </div>
    <div class="col-xl-5 col-lg-6 col-md-12 col-12">
        <div class="form-group LeftFormText d-flex align-items-center">
            {!! Form::text('name['.$row.']', null, ['required' => true]) !!}
        </div>
        {!! Form::error('name.'.$row) !!}
    </div>
    <div class="col-xl-7 col-lg-6 col-md-12 col-12">
        <div class="LeftFormText d-flex align-items-center">
            <h6>@lang('site.page.credit-level-ur-surname')</h6>
        </div>
    </div>
    <div class="col-xl-5 col-lg-6 col-md-12 col-12">
        <div class="form-group LeftFormText d-flex align-items-center">
            {!! Form::text('surname['.$row.']', null, ['required' => true]) !!}
        </div>
        {!! Form::error('surname.'.$row) !!}
    </div>
    <div class="col-xl-7 col-lg-6 col-md-12 col-12">
        <div class="LeftFormText d-flex align-items-center">
            <h6>@lang('site.page.credit-level-ur-personal_code')</h6>
        </div>
    </div>
    <div class="col-xl-5 col-lg-6 col-md-12 col-12">
        <div class="form-group LeftFormText d-flex align-items-center">
            {!! Form::text('personal_code['.$row.']', null, ['required' => true]) !!}
        </div>
        {!! Form::error('personal_code.'.$row) !!}
    </div>
    <div class="col-xl-7 col-lg-6 col-md-12 col-12">
        <div class="LeftFormText d-flex align-items-center">
            <h6>@lang('site.page.credit-level-ur-capital_amount')</h6>
        </div>
    </div>
    <div class="col-xl-5 col-lg-6 col-md-12 col-12">
        <div class="form-group LeftFormText d-flex align-items-center">
            {!! Form::text('capital_amount['.$row.']', null, ['required' => true]) !!}
        </div>
        {!! Form::error('capital_amount['.$row.']') !!}
    </div>
</div>
