@extends('front.layouts.cabinet')

@section('modals')

@endsection

@section('content')

    <div class="CabinetContent">
        <h2 class="Title text-left">@lang('site.page.cabinet.Summary')</h2>

        <div class="ResumeGrap">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12 d-flex align-self-center">
                    <h6 class="TitleResGrap">@lang('site.page.cabinet.Total-cost'): <strong>{{ $portfolioValue }} €</strong></h6>
                </div>
                <div class="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12">
                    <ul class="GrapNumb">
                        <li class="{{ request('period') == '1d' ? 'active' : '' }}"><a href="?period=1d">1D</a></li>
                        <li class="{{ request('period') == '7d' ? 'active' : '' }}"><a href="?period=7d">7D</a></li>
                        <li class="{{ request('period', '1m') == '1m' ? 'active' : '' }}"><a href="?period=1m">1M</a></li>
                        <li class="{{ request('period') == '3m' ? 'active' : '' }}"><a href="?period=3m">3M</a></li>
                        <li class="{{ request('period') == '1y' ? 'active' : '' }}"><a href="?period=1y">1Y</a></li>
                        <li class="{{ request('period') == 'all' ? 'active' : '' }}"><a href="?period=all">ALL</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="BoxGrap MarginTop15">
                        <h3>@lang('site.page.cabinet.Portfolio-graph')</h3>
                        <div style="height:250px">
                            <canvas id="GraphLineResume"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <h2 class="Title text-left">@lang('site.page.cabinet.List-of-assets')</h2>
        @if($assetContracts->isNotEmpty())
        <div class="row">
            @foreach($assetContracts as $key => $assetContract)
                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 d-flex align-items-stretch" id="port-1">
                <div class="PortfelList">
                    <h3>@lang('site.page.cabinet.Asset') {{ $key+1 }}</h3>
                    <div class="BoxText">
                        <div class="row no-gutters">
                            <div class="col-7">
                                <div class="Text1">@lang('site.page.cabinet.Asset-Ticker'):</div>
                            </div>
                            <div class="col-5 ">
                                <div class="Text2">{{ $assetContract->coinValue->symbol }}</div>
                            </div>
                            <div class="col-7">
                                <div class="Text1">@lang('site.page.cabinet.Asset-name'):</div>
                            </div>
                            <div class="col-5">
                                <div class="Text2">{{ trans_field($assetContract, 'name') }}</div>
                            </div>
                            <div class="col-7">
                                <div class="Text1">@lang('site.page.cabinet.Current-value'):</div>
                            </div>
                            <div class="col-5">
                                <div class="Text2">€ {{ format_number($assetContract->contracts->sum('amount_asset') * $assetContract->coinValue->price) }}</div>
                            </div>
                            <div class="col-7">
                                <div class="Text1">@lang('site.page.cabinet.Amount-scope-contracts'):</div>
                            </div>
                            <div class="col-5">
                                <div class="Text2">€ {{ format_number($assetContract->contracts->sum('amount')) }}</div>
                            </div>
                            <div class="col-7">
                                <div class="Text1">@lang('site.page.cabinet.Profitability'):</div>
                            </div>
                            <div class="col-5">
                                <div class="Text2">€ {{ format_number($assetContract->contracts->sum('amount_asset') * $assetContract->coinValue->price - $assetContract->contracts->sum('amount')) }}</div>
                            </div>
                            <div class="col-7">
                                <div class="Text1">@lang('site.page.cabinet.amount-asset'):</div>
                            </div>
                            <div class="col-5">
                                <div class="Text2">{{ numeric_fmt($assetContract->contracts->sum('amount_asset')) }}</div>
                            </div>
                        </div>
                        <div class="text-center MarginTop15 BTNWidth100">
                            <a href="{{ route('front.cabinet.trader.home') }}" class="btns-green"><i class="fas fa-plus"></i> @lang('site.page.cabinet.Invest')</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endif
    </div>

@endsection

@section('script')
    @if($graphs)
    <script src="/js/Chart.min.js"></script>
    <script>
        setGraphic("GraphLineResume", {!! json_encode($graphs['y']) !!}, {!! json_encode($graphs['x']) !!}, 'rgba(247,147,26,.5)', 'rgba(247,147,26, 1)', true, true);
    </script>
    @endif
@endsection
