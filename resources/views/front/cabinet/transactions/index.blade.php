@extends('front.layouts.cabinet')

@section('content')
    <div class="CabinetContent">
        <h1 class="Title text-left">@lang('site.header.transactions')</h1>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th class="text-center">@lang('site.page.cabinet.magazine-table-number')</th>
                    <th class="text-center">@lang('site.page.cabinet.amount')</th>
                    <th class="text-center">@lang('site.page.cabinet.type-transaction')</th>
                    <th class="text-center">@lang('site.page.cabinet.inv-sidebar-calc-money-title')</th>
                    <th class="text-center">@lang('site.page.cabinet.table-date-and-time')</th>
                    <th class="text-center">@lang('site.page.cabinet.status')</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($userTransactions as $transaction)
                    <tr>
                        <td class="text-center">{{ $transaction->id }}</td>
                        <td class="text-center">{{ $transaction->amount }}</td>
                        <td class="text-center">{{ $transaction->type() }}</td>
                        <td class="text-center">{{ $transaction->coin() }}</td>
                        <td class="text-center">{{ $transaction->created_at->format('d.m.Y H:i') }}</td>
                        <td class="text-center">{{ $transaction->status() }}</td>
                        <td>
                            <form
                                action="{{ route('front.cabinet.client.transactions.reject', ['transaction'=>$transaction->id])}}"
                                method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <input type="hidden" name="_method" value="PUT">
                                <button {{!$transaction->canUserReject() ? 'disabled' : '' }}
                                        class="btn btn-danger btn-sm">
                                    <i class="fa fa-times"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right Bold">
                {{ $userTransactions->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
@endsection
