@extends('front.layouts.layout')
@section('classHeader'){{'Big Home'}}@endsection

@section('title')
<title>Главная - Assets Core</title>
@endsection
@section('header_box')
    <div class="BoxStart">
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav1" role="tabpanel" aria-labelledby="nav-tab1">
                <h1>{{ trans_field($pageHome, 'title') }}</h1>
                <h2>{{ trans_field($pageHome, 'sub_title') }}</h2>
            </div>
            <div class="tab-pane fade" id="nav2" role="tabpanel" aria-labelledby="nav-tab2">
                <div class="TitleH">{{ trans_field($pageHome, 'title_inv') }}</div>
                <h2>{{ trans_field($pageHome, 'sub_title_inv') }}</h2>
            </div>
        </div>
        <nav class="ControlsTab">
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-tab1" data-toggle="tab" href="#nav1" role="tab" aria-controls="nav1" aria-selected="true">
                <label>
                    <input class="MyRadio" type="radio" name="Start" checked>
                    <h3>{{ trans_field($pageHome, 'title_offer_tr') }}</h3>
                    {!! trans_field($pageHome, 'desc_offer_tr') !!}
                </label>
                </a>
                <a class="nav-item nav-link" id="nav-tab2" data-toggle="tab" href="#nav2" role="tab" aria-controls="nav2" aria-selected="false">
                    <label>
                        <input class="MyRadio" type="radio" name="Start">
                        <h3>{{ trans_field($pageHome, 'title_offer_inv') }}</h3>
                        {!! trans_field($pageHome, 'desc_offer_inv') !!}
                    </label>
                </a>
            </div>
        </nav>
        <div class="MarginTop30">
            <a href="{{ route('register') }}" class="btns-orange">@lang('site.page.start-earning')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="BoxTable">
        <div class="container">
            <div class="table-responsive">
                <table class="table table-striped table-borderless">
                    <thead class="TitleNowrap">
                    <tr>
                        <th>#</th>
                        <th>@lang('site.page.name')
                            <i class="fas fa-caret-up"></i>
                            <i class="fas fa-caret-down"></i>
                        </th>
                        <th>@lang('site.page.in-24-hours')
                            <i class="fas fa-caret-up"></i>
                            <i class="fas fa-caret-down"></i>
                        </th>
                        <th>@lang('site.page.price')
                            <i class="fas fa-caret-up"></i>
                            <i class="fas fa-caret-down"></i>
                        </th>
                        <th>@lang('site.page.changes')
                            <i class="fas fa-caret-up"></i>
                            <i class="fas fa-caret-down"></i>
                        </th>
                        <th>@lang('site.page.price-chart')
                            <i class="fas fa-caret-up"></i>
                            <i class="fas fa-caret-down"></i>
                        </th>
                        <th>@lang('site.page.bargain')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($symbols as $key => $symbol)
                        <tr class="{{ $key>3 ? 'Tablehidden' : ''}}">
                            <td>{{ $key+1 }}</td>
                            <td>{{$symbol->coinValue->name}} {{$symbol->coinValue->symbol}}</td>
                            <td>{{number_format($symbol->coinValue->price*$symbol->coinValue->percent_change_24h/100,2,'.',' ')}}€</td>
                            <td>{{number_format($symbol->coinValue->price,2,'.',' ')}}€</td>
                            <td><span class="{{ $symbol->coinValue->percent_change_1h > 0 ? 'green' : 'orange'}}">{{number_format($symbol->coinValue->percent_change_1h,2)}}%</span></td>
                            <td style="padding: 0">
                                <div style="height:100px!important;width:200px!important;">
                                    <canvas id="GraphHomeCab{{ $key+1 }}"></canvas>
                                </div>
                            </td>
                            <td><a href="{{ route('register') }}" class="btns-orange">@lang('site.page.btn-buy')</a></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="7">
                            <div class="BoxShowHide">
                                <a href="#" class="ShowTable">@lang('site.page.link-watch-more') <i class="far fa-angle-down"></i></a>
                                <a href="#" class="HideTable">@lang('site.page.link-hide') <i class="far fa-angle-up"></i></a>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="Offers" id="block-Offers">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 d-xl-flex d-lg-flex align-self-stretch">
                    <div class="offersList Blue">
                        <div class="contentOf">
                            <div class="InfoText">{{ trans_field($pageHome, 'offer_tr_title') }}</div>
                            <h2>{{ trans_field($pageHome, 'offer_tr_sub_title') }}</h2>
                            {!! trans_field($pageHome, 'offer_tr_desc') !!}
                        </div>
                        <div class="MarginTop30">
                            <a href="#top" class="btns-orange">{{ trans_field($pageHome, 'offer_tr_btn') }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 d-xl-flex d-lg-flex align-self-stretch">
                    <div class="offersList">
                        <div class="contentOf">
                            <div class="InfoText">{{ trans_field($pageHome, 'offer_inv_title') }}</div>
                            <h2>{{ trans_field($pageHome, 'offer_inv_sub_title') }}</h2>
                            {!! trans_field($pageHome, 'offer_inv_desc') !!}
                        </div>
                        <div class="MarginTop30">
                            <a href="{{ route('front.static-page.investors') }}" class="btns-orange">{{ trans_field($pageHome, 'offer_inv_btn') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="Calculator">
        <div class="container">
            <h2 id="top">@lang('site.page.form-trader-calc-title')</h2>
            @if($typeTransactions->isNotEmpty())
            <div class="BoxTypeCalc">
                <div class="row">
                    @foreach($typeTransactions as $key => $typeTransaction)
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <label class="ListType ListTypeTransaction {{ $key == 0 ? 'active' : '' }}" data-row="{{ $key+1 }}">
                            <input class="MyRadio" type="radio" name="typecalc" value="{{ $typeTransaction->id }}" {{ $key == 0 ? 'checked' : '' }} class="type-transaction-value">
                            <h3>{{ trans_field($typeTransaction, 'name') }}</h3>
                        </label>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
            <div class="BoxCalculator">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <h3>@lang('site.page.form-trader-loan-month')</h3>
                        <input type="text" class="RangeSliderMonth" name="RangeSliderMonth" value="" id="RangeSliderMonth1">
                        <input type="text" class="RangeSliderMonth11" name="RangeSliderMonth11" id="RangeSliderMonth11" value="20">
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <h3>@lang('site.page.form-trader-payment-monthly')</h3>
                        <input type="text" class="RangeSliderMoney2" name="RangeSliderMoney" value="" id="RangeSliderMoney1">
                        <input type="text" class="RangeSliderMoney11" name="RangeSliderMoney11" id="RangeSliderMoney11" value="50">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="BoxInvesMaterial">
                        <div class="HeadInvest">@lang('site.page.calc-trader-investment-assets')</div>
                        <div class="TextInvest1">@lang('site.page.calc-trader-average-loan-amount')</div>
                        <div class="TextInvest2 LineBottom"><span id="credit_value_1">50</span> €</div>
                        <div class="row no-gutters">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 RightBorder">
                                <div class="TextInvest1">@lang('site.page.calc-trader-annual-interest-rate')</div>
                                <div class="TextInvest2 LineBottom"><span id="percent_1">5</span>%</div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="TextInvest1">@lang('site.page.calc-trader-title2')</div>
                                <div class="TextInvest2 LineBottom">293.06%</div>
                            </div>
                        </div>
                        <div class="TextInvest1">@lang('site.page.calc-trader-maximum-available-volume')</div>
                        @if($symbolCalc->isNotEmpty())
                            <div class="TextInvest2" id="value_gold" data-value="{{ $symbolCalc->first()->price }}">{{ $symbolCalc->first()->symbol }} <span>{{numeric_fmt(50/$symbolCalc->first()->price)}}</span></div>
                        @endif
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="BoxInvesMaterial">
                        <div class="HeadInvest">@lang('site.page.calc-trader-virtual-investment-assets')</div>
                        <div class="TextInvest1">@lang('site.page.calc-trader-average-loan-amount')</div>
                        <div class="TextInvest2 LineBottom"><span id="credit_value_2">50</span> €</div>
                        <div class="row no-gutters">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 RightBorder">
                                <div class="TextInvest1">@lang('site.page.calc-trader-annual-interest-rate')</div>
                                <div class="TextInvest2 LineBottom"><span id="percent_2">5</span>%</div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="TextInvest1">@lang('site.page.calc-trader-title2')</div>
                                <div class="TextInvest2 LineBottom">153,54%</div>
                            </div>
                        </div>
                        <div class="TextInvest1">@lang('site.page.calc-trader-maximum-available-volume')</div>
                        @if($symbolCalc->isNotEmpty())
                            <div class="TextInvest2" id="value_btc" data-value="{{ $symbolCalc->last()->price }}">{{ $symbolCalc->last()->symbol }} <span>{{(numeric_fmt(50/$symbolCalc->last()->price))}}</span></div>
                        @endif
                    </div>
                </div>
            </div>
            <h2>{{ trans('site.page.home-example-calc') }}</h2>
            <div class="BoxTable">
                <div class="table-responsive">
                    <table class="table table-striped table-borderless">
                        <thead class="TitleNowrap">
                            <tr>
                                <th>#</th>
                                <th>{{ trans('site.page.home-example-calc-date') }}</th>
                                <th>{{ trans('site.page.home-example-calc-main-debt') }}</th>
                                <th>{{ trans('site.page.home-example-calc-balance-owed') }}</th>
                                <th>{{ trans('site.page.home-example-calc-per-month') }}</th>
                                <th>{{ trans('site.page.home-example-calc-amount') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>15.03.2021</td>
                                <td>83.33</td>
                                <td>916.67</td>
                                <td>9.37</td>
                                <td>92.70</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>15.04.2021</td>
                                <td>83.33</td>
                                <td>833.33</td>
                                <td>9.37</td>
                                <td>92.70</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>15.05.2021</td>
                                <td>83.33</td>
                                <td>750.00</td>
                                <td>9.37</td>
                                <td>92.70</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>15.06.2021</td>
                                <td>83.33</td>
                                <td>666.67</td>
                                <td>9.37</td>
                                <td>92.70</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>15.07.2021</td>
                                <td>83.33</td>
                                <td>583.33</td>
                                <td>9.37</td>
                                <td>92.70</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>15.08.2021</td>
                                <td>83.33</td>
                                <td>500.00</td>
                                <td>9.37</td>
                                <td>92.70</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>15.09.2021</td>
                                <td>83.33</td>
                                <td>416.67</td>
                                <td>9.37</td>
                                <td>92.70</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>15.10.2021</td>
                                <td>83.33</td>
                                <td>333.33</td>
                                <td>9.37</td>
                                <td>92.70</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>15.11.2021</td>
                                <td>83.33</td>
                                <td>250.00</td>
                                <td>9.37</td>
                                <td>92.70</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>15.12.2021</td>
                                <td>83.33</td>
                                <td>166.67</td>
                                <td>9.37</td>
                                <td>92.70</td>
                            </tr>
                            <tr>
                                <td>11</td>
                                <td>15.01.2022</td>
                                <td>83.33</td>
                                <td>83.33</td>
                                <td>9.37</td>
                                <td>92.70</td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>15.02.2022</td>
                                <td>83.33</td>
                                <td>0</td>
                                <td>9.37</td>
                                <td>92.70</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="text-center MarginTop30">
                <a href="{{ route('register') }}" class="btns-orange">@lang('site.page.register-now')</a>
            </div>
        </div>
    </div>

    <div class="wrapp">
        @if($blockPackages->isNotEmpty())
        <div class="SectionPakets">
            <div class="container">
                <h2>@lang('site.page.our-packages')</h2>
                <div class="row">
                    @foreach($blockPackages as $blockPackage)
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="PaketsList Paket-1" style="background-image: url({{ $blockPackage->getPhotoUrl() }});">
                            <h3>{{ trans_field($blockPackage, 'name') }}</h3>
                            <div class="Descrip">{{ trans_field($blockPackage, 'desc') }}</div>
                            <div class="Descrip2">{{ trans_field($blockPackage, 'price') }}</div>
                            <div class="MarginTop30">
                                <a href="{{ route('register') }}" class="btns-orange">@lang('site.page.start')</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif

        @if($faqItems->isNotEmpty())
        <div class="container">
            <div class="SectionFAQ MaxWidth890 PaddingBot100">
                <h2>FAQ</h2>
                <div class="Descrip">@lang('site.page.text-experienced-team')</div>
                <div class="BoxFAQ" id="accordionFAQ">
                    @foreach($faqItems as $faqItem)
                        <div class="FAQList">
                            <div class="FAQHeader">
                                <a href="#" class="ShowMoreDetalis collapsed" data-toggle="collapse" data-target="#FAQ{{ $faqItem->id }}">
                                    {{ trans_field($faqItem, 'title') }}
                                </a>
                            </div>
                            <div id="FAQ{{ $faqItem->id }}" class="collapse" data-parent="#accordionFAQ">
                                <div class="card-body">
                                    {!! trans_field($faqItem, 'description') !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="text-center MarginTop30">
                    <a href="{{ route('front.static-page.faq') }}" class="btns-orange">@lang('site.page.all-questions')</a>
                </div>
            </div>
        </div>
        @endif
    </div>
@endsection

@section('script')
    <script src="/js/Chart.min.js"></script>

    @foreach($symbols as $key => $symbol)
        @if($symbol->coinValue->ohlcv_x && $symbol->coinValue->ohlcv_y)
            <script>
                setGraphic("GraphHomeCab{{$key+1}}", {!! $symbol->coinValue->ohlcv_y !!}, {!! $symbol->coinValue->ohlcv_x !!}, 'rgba(247,147,26,.5)', 'rgba(247,147,26, 1)', false);
            </script>
        @endif
    @endforeach

    <script type="text/javascript">
        function setValueCalc() {
            $('#RangeSliderMoney11').val($('#RangeSliderMoney1').val()*1);
            $('#RangeSliderMonth11').val($('#RangeSliderMonth1').val()*1);
            $.ajax({
                type: "POST",
                url: '{{ route('front.home.calcTraderAnnuity') }}',
                data: {amount: $('#RangeSliderMoney1').val(), period: $('#RangeSliderMonth11').val(), type: $("input.type-transaction-value:checked").val()},
                success: function (result) {
                    if(result.hasOwnProperty('name')){
                        let value = result.amount;
                        $('#credit_value_1').text(value);
                        $('#credit_value_2').text(value);
                        $('#percent_1').text(result.value);
                        $('#percent_2').text(result.value);
                        let value_gold = value / $('#value_gold').data('value') * 1;
                        $('#value_gold span').text(value_gold.toFixed(8));
                        let value_btc = value / $('#value_btc').data('value') * 1;
                        $('#value_btc span').text(value_btc.toFixed(8));
                    }
                }
            });
        }
        $(document).ready(function(){
            $('#RangeSliderMoney1').data("ionRangeSlider").update({
                onFinish: function (data) {
                    setValueCalc();
                },
            });
            $('#RangeSliderMonth1').data("ionRangeSlider").update({
                onFinish: function (data) {
                    setValueCalc();
                },
            });

            setValueCalc();
            $('.ListTypeTransaction').on('click', function (e) {
                var row = $(this).data('row');
                if(row == 1){
                    $('#RangeSliderMoney1').data("ionRangeSlider").update({
                        min: 17,
                        max: 240,
                        from: 17,
                        step: 10,
                    });
                }else if(row == 2){
                    $('#RangeSliderMoney1').data("ionRangeSlider").update({
                        min: 241,
                        max: 1180,
                        from: 241,
                        step: 10,
                    });
                }else{
                    $('#RangeSliderMoney1').data("ionRangeSlider").update({
                        min: 1181,
                        max: 2330,
                        from: 1181,
                        step: 10,
                    });
                }
                setValueCalc();
            });
            $('#RangeSliderMoney11').on('change', function () {
                $('#RangeSliderMoney1').data("ionRangeSlider").update({
                    from: $(this).val()
                });
                setValueCalc();
            });
            $('#RangeSliderMonth11').on('change', function () {
                $('#RangeSliderMonth1').data("ionRangeSlider").update({
                    from: $(this).val()
                });
                setValueCalc();
            });
            $('.ListType .jq-radio').on('click', function (e) {
                e.preventDefault();
                if ($(this).hasClass("checked")) {
                    $(".ListType").removeClass('active');
                    $(this).parent().addClass("active");
                    $(this).next().parent().addClass("active");
                } else {
                    $(this).next().parent().removeClass("active");
                    $(this).parent().removeClass("active");
                }
                return false;
            });
        });
    </script>
@endsection
