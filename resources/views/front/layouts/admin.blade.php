<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.standalone.min.css">
        @include("front.layouts.parts._head")
        @yield('title')
        <title>{{ env('APP_NAME') }}</title>
        @yield('style')
        @yield('style2')
    </head>
    <body>

        @include("front.layouts.parts._header")
        @section('classHeader'){{'White Fixed'}}@endsection

        <div class="wrapp Admin Cabinet">
            <div class="Content">
                <div class="container">
                    <div class="AdminContent CabinetContent">
                        @include("front.layouts.parts._flash")
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

        @include("front.layouts.parts._scripts")
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.ru.min.js"></script>
        <script src="/js/cabinet.js?v=1.0.0"></script>
        @yield('script')
        @yield('script2')
        @yield('modals')
        @yield('script-modals')

    </body>
</html>
