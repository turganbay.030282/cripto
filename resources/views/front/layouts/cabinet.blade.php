<!DOCTYPE html>
<html>
    <head>
        @include("front.layouts.parts._head")
        @yield('title')
        <title>{{ env('APP_NAME') }}</title>
        <link rel="stylesheet" href="/css/cabinet.css?v=1.0.4">
        @yield('style')
        @yield('style2')
    </head>
    <body>

        @include("front.layouts.parts._header", ['class' => 'White Fixed'])

        <div class="wrapp Cabinet">
            <div class="Content">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                            @include("front.layouts.parts._flash")
                            @yield('content')
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                            <div class="SideBar">

                                {!! Form::open(['url' => route('front.cabinet.profile.changeType')]) !!}
                                    <div class="ChooseCab">
                                        <div class="row  no-gutters">
                                            <div class="col-4 d-flex align-items-center justify-content-center">
                                                <div class="ChooseText">@lang('site.page.cabinet.Traders-Cabinet') <div class="Notifications {{ !auth()->user()->is_investor ? 'Active' : '' }}">{{ $userCreditLevel }}</div></div>
                                            </div>
                                            <div class="col-4">
                                                <label class="switch" id="checked_is_investor">
                                                    <input  name="is_investor" type="hidden" value="0">
                                                    <input type="checkbox" name="is_investor" value="1" {{ auth()->user()->is_investor ? 'checked' : '' }}>
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                            <div class="col-4 d-flex align-items-center justify-content-center">
                                                <div class="ChooseText">@lang('site.page.cabinet.Investors-Cabinet') <div class="Notifications Right {{ auth()->user()->is_investor ? 'Active' : '' }}">{{ $userInvestorRank }}</div></div>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}

                                @if(auth()->user()->is_investor)
                                    @include("front.layouts.parts._sidebar_investor")
                                @else
                                    @include("front.layouts.parts._sidebar_trader")
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="holder">
            <div class="preloader"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        </div>
        @include("front.layouts.parts._scripts")
        <script src="/js/cabinet.js?v=1.0.1"></script>
        @yield('modals')
        @yield('modals2')
        @yield('modals3')
        @yield('modals-s-bar')
        @yield('script-modals')
        @yield('script-modals2')
        @yield('script-modals3')
        @yield('script-modals4')
        @yield('script')
        @yield('script2')
        @yield('script-sidebar')
        <script>
            $(document).ready(function(){
                $('#btn_new_notifications.NewMassage').on('click', function (e) {
                    $.ajax({
                        type: "GET",
                        url: '{{ route('front.cabinet.client.journal.read') }}',
                        success: function (result) {
                            console.log(result);
                        }
                    });
                });
            });
            function CopyToBuffer() {
                var copyText = document.getElementById("copy-buffer");
                copyText.select();
                document.execCommand("copy");
                alert("Copied the text: " + copyText.value);
            }
            function CopyToBuffer2() {
                var copyText = document.getElementById("copy-buffer2");
                copyText.select();
                document.execCommand("copy");
                alert("Copied the text: " + copyText.value);
            }
            function CopyToBuffer3() {
                var copyText = document.getElementById("copy-buffer3");
                copyText.select();
                document.execCommand("copy");
                alert("Copied the text: " + copyText.value);
            }
            function CopyToBuffer4() {
                var copyText = document.getElementById("copy-buffer4");
                copyText.select();
                document.execCommand("copy");
                alert("Copied the text: " + copyText.value);
            }
        </script>
    </body>
</html>
