<!DOCTYPE html>
<html>
    <head>
        @include("front.layouts.parts._head")
        @yield('title')
        @yield('style')
        @yield('style2')
    </head>
    <body>

        @include("front.layouts.parts._header")

        @include("front.layouts.parts._flash")
        @yield('content')

        @include("front.layouts.parts._footer", ['setting' => $setting] )
        @include("front.layouts.parts._scripts")

        @yield('script')
        @yield('script2')

    </body>
</html>
