<style>
    .x_content .alert{
        position: fixed;
        z-index: 99999;
        right: 100px;
        top: 100px;
    }
</style>
@if (session('success') || session('status') || session('info') || session('warning') || session('danger') || session('error'))
<div class="x_content bs-example-popovers">

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>{{ session('success') }}</strong>
        </div>
    @endif

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>{{ session('status') }}</strong>
        </div>
    @endif

    @if (session('info'))
        <div class="alert alert-info" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>{{ session('info') }}</strong>
        </div>
    @endif

    @if (session('warning'))
        <div class="alert alert-warning" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>{{ session('warning') }}</strong>
        </div>
    @endif

    @if (session('danger'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>{{ session('danger') }}</strong>
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>{{ session('error') }}</strong>
        </div>
    @endif

</div>
@endif

@if ($errors->any())
    <div class="x_content bs-example-popovers">

        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <ul>
                @foreach($errors->all() as $error)
                    <li><strong>{{ $error }}</strong></li>
                @endforeach
            </ul>
        </div>

    </div>
@endif
