<footer>
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                <a href="/" class="logo"></a>
                <p>{{ $setting->footer_text_1 }}</p>
                <div class="social">
                    @if($setting->soc_fb)
                        <a href="{{ $setting->soc_fb }}" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    @endif
                    @if($setting->soc_tw)
                        <a href="{{ $setting->soc_tw }}" target="_blank"><i class="fab fa-twitter"></i></a>
                    @endif
                    @if($setting->soc_in)
                        <a href="{{ $setting->soc_in }}" target="_blank"><i class="fab fa-instagram"></i></a>
                    @endif
                    @if($setting->soc_yt)
                        <a href="{{ $setting->soc_yt }}" target="_blank"><i class="fab fa-youtube"></i></a>
                    @endif
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-3 col-sm-12 col-12">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-5 col-sm-6 col-12">
                        <h3>@lang('site.footer.Links')</h3>
                        <nav class="FootMenu">
                            <ul>
                                <li class="{{ url()->current() == route('front.home') ? 'active' : '' }}"><a href="{{ route('front.home') }}">@lang('site.menu.main')</a></li>
                                <li class="{{ url()->current() == route('front.static-page.about') ? 'active' : '' }}"><a href="{{ route('front.static-page.about') }}">@lang('site.menu.about')</a></li>
                                <li class="{{ url()->current() == route('front.static-page.faq') ? 'active' : '' }}"><a href="{{ route('front.static-page.faq') }}">@lang('site.menu.faq')</a></li>
{{--                                <li class="{{ request()->is('news*') ? 'active' : '' }}"><a href="{{ route('front.news') }}">@lang('site.menu.news')</a></li>--}}
                                <li class="{{ url()->current() == route('front.static-page.investors') ? 'active' : '' }}"><a href="{{ route('front.static-page.investors') }}">@lang('site.menu.investors')</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-7 col-sm-6 col-12">
                        <h3>@lang('site.footer.Company')</h3>
                        <nav class="FootMenu">
                            <ul>
                                <li><a href="{{ route('front.static-page.cert') }}">@lang('site.footer.Certificates-licenses')</a></li>
                                @if($footerPages->isNotEmpty())
                                    @foreach($footerPages as $page)
                                        <li><a href="{{ route('front.pages.show', $page->slug) }}">{{ trans_field($page, 'title') }}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
{{--                <h3>@lang('site.footer.Requisites')</h3>--}}
{{--                <nav class="FootMenu">--}}
{{--                    <ul>--}}
{{--                        @if($setting->req_sia)--}}
{{--                            <li>{{ $setting->req_sia }}</li>--}}
{{--                        @endif--}}
{{--                        @if($setting->req_reg)--}}
{{--                        <li>{{ $setting->req_reg }}</li>--}}
{{--                        @endif--}}
{{--                        @if($setting->req_seb)--}}
{{--                            <li>{{ $setting->req_seb }}</li>--}}
{{--                        @endif--}}
{{--                        @if($setting->req_swift)--}}
{{--                            <li>{{ $setting->req_swift }}</li>--}}
{{--                        @endif--}}
{{--                    </ul>--}}
{{--                </nav>--}}
            </div>
            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-6 col-12">
                <h3>@lang('site.footer.Contacts')</h3>
                <nav class="FootMenu">
                    <ul>
                        @if($setting->contact_address)
                            <li>{{ $setting->contact_address }}</li>
                        @endif
                        @if($setting->contact_email)
                            <li><a href="mailto:{{ $setting->contact_email }}">{{ $setting->contact_email }}</a></li>
                        @endif
{{--                        @if($setting->contact_phone)--}}
{{--                            <li><a href="tel:+{{ str_replace(['+', ' ', '(', ')'], '', $setting->contact_phone) }}">{{ $setting->contact_phone }}</a></li>--}}
{{--                        @endif--}}
                    </ul>
                </nav>
            </div>
        </div>
        <div class="copyBox">
            <div class="row">
                <div class="col-xl-4 col-lg-3 col-md-12 col-sm-12 col-12">
                    <div class="copy">&copy; {{ date('Y') }} @lang('site.footer.rights')</div>
                </div>
                {{--
                <div class="col-xl-8 col-lg-9 col-md-12 col-sm-12 col-12">
                    <div class="Subscribe">
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                <div class="SubText">@lang('site.footer.subscriber'):</div>
                            </div>
                            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                                <div class="FormBox">
                                    {!! Form::open(['url' => route('front.home.subscribers')]) !!}
                                        <input type="text" placeholder="@lang('site.footer.Enter-e-mail')" name="email" required>
                                        {!! Form::error('email') !!}
                                        <button type="submit">@lang('site.footer.Subscribe')</button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                --}}
            </div>
        </div>
    </div>
</footer>
<div class="holder">
  <div class="preloader"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
</div>

