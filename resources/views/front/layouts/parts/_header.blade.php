<header class="@yield('classHeader')@isset($class){{$class}}@endisset">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-xl-3 col-lg-3 col-md-12 col-12">
                @if (auth()->guest())
                    <a href="{{ route('front.home') }}" class="logo"></a>
                @elseif(auth()->user()->is_investor)
                    <a href="{{ route('front.cabinet.investor.home') }}" class="logo"></a>
                @else
                    <a href="{{ route('front.cabinet.trader.home') }}" class="logo"></a>
                @endif
            </div>
            <div class="col-xl-6 col-lg-6 col-12">
                <nav class="TopMenu d-lg-block d-xl-block">
                    @include('front.layouts.parts._menu')
                </nav>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-12 col-12">
                <div class="LangBox">
                    <select id="select-lang">
                        <option class="Lang lang3" value="{{ route('setlocale', ['lang' => 'ru']) }}" {{ app()->getLocale() == 'ru' ? 'selected' : ''}}>RU</option>
                        <option class="Lang lang2" value="{{ route('setlocale', ['lang' => 'en']) }}" {{ app()->getLocale() == 'en' ? 'selected' : ''}}>EN</option>
                    </select>
                </div>
                @if(auth()->guest())
                    <div class="text-right headerBTN d-none d-lg-block">
                        <a href="{{ route('login') }}">@lang('site.header.login')</a>
                        <a href="{{ route('register') }}" class="btns-white">@lang('site.page.header.registration')</a>
                    </div>
                @else
                    <div class="text-right BoxUserHeadLink d-none d-lg-block">
                        <a href="#" id="btn_new_notifications" class="LinkBellBox {{ $headerUserNotifications->isNotEmpty() ? 'NewMassage' : '' }}" title="Bell"></a>
                        <a href="#" class="LinkUserBox">{{ auth()->user()->full_name }}</a>
                    </div>
                @endif
            </div>
        </div>

        @yield('header_box')

        @if(!auth()->guest())
            <div class="UserBox">
                <div class="ListUserBox">
                    <h5>{{ auth()->user()->full_name }}</h5>
                    <h5>{{ auth()->user()->email }}</h5>
                    <a href="{{ route('front.cabinet.profile.show') }}" class="SettingsBTN"></a>
                </div>
                <div class="ListUserBox">
                    <ul>
                        <li><a href="{{ route('front.cabinet.profile.show') }}">@lang('site.header.profile')</a></li>
                        <li class="JornalBTN"><a href="{{ route('front.cabinet.client.journal') }}">@lang('site.header.magazine')</a></li>
                        <li class="JornalBTN"><a href="{{ route('front.cabinet.client.bonuses') }}">@lang('site.menu.detail-bonuses')</a></li>
                        <li class="JornalBTN"><a href="{{ route('front.cabinet.client.transactions') }}">@lang('site.header.transactions')</a></li>
                        <li class="JornalBTN"><a href="{{ route('front.cabinet.client.conversions') }}">@lang('site.header.conversions')</a></li>
                        <li class="HelpBTN"><a href="{{ route('front.cabinet.contacts') }}">@lang('site.header.help')</a></li>
                    </ul>
                </div>
                <div class="ListUserBox">
                    <ul>
                        <li class="ExitBTN">
                            <label>
                                {!! Form::open(['url' => route('logout')]) !!}
                                    {!! Form::submit(trans('site.header.logout')) !!}
                                {!! Form::close() !!}
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="BellBox">
                <h5>@lang('site.page.your-notifications')</h5>
                @if($headerUserNotifications->isNotEmpty())
                    <div class="ListBellBox">
                        <ul>
                            @foreach($headerUserNotifications as $headerUserNotification)
                                <li class="NotRead">
                                    <a href="{{ route('front.cabinet.client.journal') }}">
                                        {{ env('APP_NAME') }}
                                        <span>{{ $headerUserNotification->title }}</span>
                                    </a>
                                </li>
                            @endforeach
                            <li class="More">
                                <a href="{{ route('front.cabinet.client.journal') }}">@lang('site.page.show-more')</a>
                            </li>
                        </ul>
                    </div>
                @endif
            </div>
        @endif
    </div>

    @yield('title_2')

    @if(auth()->guest())
        <div class="FixedHeaderDevice">
            <a href="#" id="RespBurg"></a>
            <div class="LoginDevice">
                <a class="LogIN" href="{{ route('login') }}" title="@lang('site.header.login')"></a>
                <a class="RegIN" href="{{ route('register') }}" title="@lang('site.page.header.registration')"></a>
            </div>
        </div>
    @else
        <div class="FixedHeaderDevice d-block d-lg-none d-xl-none">
            <a href="#" id="RespBurg"></a>
            <div class="LoginDevice">
                <a href="#" class="LinkBellBox NewMassage" title="Bell"></a>
                <a href="#" class="LinkUserBox" title="User"></a>
            </div>
        </div>
    @endif
</header>
