<header class="Green">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-12 col-12">
                <a href="{{ route('front.home') }}" class="logo"></a>
            </div>
            <div class="col-xl-6 col-lg-6 col-12">
                <nav class="TopMenu d-lg-block d-xl-block">
                    @include('front.layouts.parts._menu')
                </nav>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-12 col-12">
                <div class="LangBox">
                    <select id="select-lang">
                        <option class="Lang lang3" value="{{ route('setlocale', ['lang' => 'ru']) }}" {{ app()->getLocale() == 'ru' ? 'selected' : ''}}>RU</option>
                        <option class="Lang lang2" value="{{ route('setlocale', ['lang' => 'en']) }}" {{ app()->getLocale() == 'en' ? 'selected' : ''}}>EN</option>
                    </select>
                </div>
                <div class="text-right headerBTN d-none d-lg-block">
                    <a href="{{ route('login') }}">@lang('site.header.login')</a>
                    <a href="{{ route('register') }}" class="btns-white">@lang('site.page.header.registration')</a>
                </div>
            </div>
        </div>
    </div>
    <div class="FixedHeaderDevice">
        <a href="#" id="RespBurg"></a>
        <div class="LoginDevice">
            <a class="LogIN" href="{{ route('login') }}" title="@lang('site.header.login')"></a>
            <a class="RegIN" href="{{ route('register') }}" title="@lang('site.page.header.registration')"></a>
        </div>
    </div>
</header>
