<ul class="d-xl-flex d-lg-flex justify-content-xl-start justify-content-lg-start">
    @if(auth()->guest())
        <li class="{{ url()->current() == route('front.home') ? 'active' : '' }}"><a
                href="{{ route('front.home') }}">@lang('site.menu.main')</a></li>
        {{--            <li class="{{ url()->current() == route('front.static-page.about') ? 'active' : '' }}"><a href="{{ route('front.static-page.about') }}">@lang('site.menu.about')</a></li>--}}
        {{--            <li class="{{ url()->current() == route('front.static-page.faq') ? 'active' : '' }}"><a href="{{ route('front.static-page.faq') }}">@lang('site.menu.faq')</a></li>--}}
        {{--            <li class="{{ request()->is('news*') ? 'active' : '' }}"><a href="{{ route('front.news') }}">@lang('site.menu.news')</a></li>--}}
        <li class="{{ url()->current() == route('front.static-page.investors') ? 'active' : '' }}"><a
                href="{{ route('front.static-page.investors') }}">@lang('site.menu.investors')</a></li>
        <li class="{{ url()->current() == route('front.static-page.contact') ? 'active' : '' }}"><a
                href="{{ route('front.static-page.contact') }}">@lang('site.menu.contacts')</a></li>
    @endif
    @if(auth()->user())
        @if(auth()->user()->isAdmin())
            {{--            <li class="{{ url()->current() == route('front.cabinet.admin.home') ? 'active' : '' }}">--}}
            {{--                <a href="{{ route('front.cabinet.admin.home') }}">@lang('site.menu.statistics')</a>--}}
            {{--            </li>--}}
            {{--            <li class="{{ url()->current() == route('front.cabinet.admin.payments.index') ? 'active' : '' }}">--}}
            {{--                <a href="{{ route('front.cabinet.admin.payments.index') }}">@lang('site.menu.Payments')</a>--}}
            {{--            </li>--}}
            {{--            <li class="{{ url()->current() == route('front.cabinet.admin.benefits.index') ? 'active' : '' }}">--}}
            {{--                <a href="{{ route('front.cabinet.admin.benefits.index') }}">@lang('site.menu.Assets')</a>--}}
            {{--            </li>--}}
            {{--            <li class="{{ url()->current() == route('front.cabinet.admin.users.index') ? 'active' : '' }}">--}}
            {{--                <a href="{{ route('front.cabinet.admin.users.index') }}">@lang('site.menu.Users')</a>--}}
            {{--            </li>--}}
            {{--            <li class="{{ url()->current() == route('front.cabinet.admin.configs.index') ? 'active' : '' }}">--}}
            {{--                <a href="{{ route('front.cabinet.admin.configs.index') }}">@lang('site.menu.Configuration')</a>--}}
            {{--            </li>--}}
        @else
            @if(auth()->user()->is_investor)
                <li class="{{ url()->current() == route('front.cabinet.investor.home') ? 'active' : '' }}">
                    <a href="{{ route('front.cabinet.investor.home') }}">@lang('site.menu.main')</a>
                </li>
                <li class="{{ url()->current() == route('front.cabinet.investor.contracts.index') ? 'active' : '' }}">
                    <a href="{{ route('front.cabinet.investor.contracts.index') }}">@lang('site.menu.Agreements')</a>
                </li>
                <li class="{{ url()->current() == route('front.cabinet.contacts') ? 'active' : '' }}">
                    <a href="{{ route('front.cabinet.contacts') }}">@lang('site.menu.contacts')</a>
                </li>
            @else
                <li class="{{ url()->current() == route('front.cabinet.trader.home') ? 'active' : '' }}">
                    <a href="{{ route('front.cabinet.trader.home') }}">@lang('site.menu.main')</a>
                </li>
                <li class="{{ url()->current() == route('front.cabinet.trader.portfolios.index') ? 'active' : '' }}">
                    <a href="{{ route('front.cabinet.trader.portfolios.index') }}">@lang('site.menu.Portfolio')</a>
                </li>
                <li class="{{ url()->current() == route('front.cabinet.trader.contracts.index') ? 'active' : '' }}">
                    <a href="{{ route('front.cabinet.trader.contracts.index') }}">@lang('site.menu.Agreements')</a>
                </li>
                <li class="{{ url()->current() == route('front.cabinet.contacts') ? 'active' : '' }}">
                    <a href="{{ route('front.cabinet.contacts') }}">@lang('site.menu.contacts')</a>
                </li>
            @endif
        @endif
    @endif
    <li class="{{ url()->current() == route('front.static-pages.bonuses') ? 'active' : '' }}">
        <a href="{{ route('front.static-pages.bonuses') }}">@lang('site.menu.bonuses')</a>
    </li>

    <li class="{{ url()->current() == route('front.convert.index') ? 'active' : '' }}">
        <a href="{{ route('front.convert.index') }}">@lang('site.menu.conversion')</a>
    </li>
</ul>
