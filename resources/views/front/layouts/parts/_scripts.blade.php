<script src="/js/jquery-3.5.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.formstyler.min.js"></script>
<script src="/js/fontawesome.min.js"></script>
<script src="/js/ion.rangeSlider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.ru.min.js"></script>
<script src="/js/js.js?v1.0.13"></script>
<script src="/js/site.js?v=1.0.13"></script>


<script>
    $(document).ready(function () {
        $('.get-sms-code').on('click', function (e) {
            e.preventDefault();
            var el = $(this);
            el.attr('disabled', true);
            $.ajax({
                method: 'POST',
                url: el.data('url'),
                success: function (result) {
                    if (result.hasOwnProperty('error')) {
                        el.closest('div').append('<span class="invalid-feedback" style="color: red;display: block;"><strong>' + result.error + '</strong></span>');
                        setTimeout(function () {
                            el.closest('div').find('.invalid-feedback').remove();
                        }, 10000);
                        return;
                    }
                    el.closest('div').hide();
                },
                complete: function (){
                    el.attr('disabled', false);
                }
            });
        });
    })
</script>

@if(production() && auth()->check())
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script>
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('{{config('broadcasting.connections.pusher.key')}}', {
            cluster: '{{config('broadcasting.connections.pusher.options.cluster')}}',
            encrypted: true,
            disableStats: true,
            authEndpoint: '/broadcasting/auth',
            auth: {
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
                }
            },
            enabledTransports: ['ws', 'xhr_streaming', 'xhr_polling', 'sockjs'],
        });

        var channel = pusher.subscribe('user.transaction.' + {{auth()->id()}});

        channel.bind('transaction-approve', function (data) {
            location.reload();
        });
    </script>
@endif

<script>
    $(window).on('load', function () {
        setTimeout(function () {
            $(".holder").delay(100).fadeOut().remove();
        });
    });
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-WLQDC8MMG7"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'G-WLQDC8MMG7');
</script>

