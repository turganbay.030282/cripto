@section('modals3')
    @if(auth()->user()->is_verification && auth()->user()->investor_rank)
        @include('front.cabinet.investor.parts._modal_deposit_line')
        @include('front.cabinet.investor.parts._modal_deposit_in_euro')
        @include('front.cabinet.investor.parts._modal_payout_euro')
        @include('front.cabinet.investor.parts._modal_payout_dai')

        @foreach($sInvestorAssets as $investorAsset)
            @include('front.cabinet.investor.parts._modal_payin_dai', compact('investorAsset'))
        @endforeach
    @endif
@endsection

<div class="InfoListCab">
    <div class="ListCab">
        <div class="row no-gutters">
            <div class="col-6">
                <div class="Height100pc d-flex align-items-center">
                    <div class="Text1 ">@lang('site.page.cabinet.sidebar-inv-Deposit'):</div>
                </div>
            </div>
            <div class="col-6">
                <div class="Text2">€ {{ $user->balance_investor }}</div>
            </div>
        </div>
    </div>
    {{--    <div class="ListCab">--}}
    {{--        <div class="row no-gutters">--}}
    {{--            <div class="col-6">--}}
    {{--                <div class="Height100pc d-flex align-items-center">--}}
    {{--                    <div class="Text1 ">@lang('site.page.cabinet.sidebar-inv-courses'):</div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="col-6">--}}
    {{--                <div class="Text2"> 1 DAI = €{{ $setting->course_investor }}</div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--    <div class="ListCab">--}}
    {{--        <div class="row no-gutters">--}}
    {{--            <div class="col-6">--}}
    {{--                <div class="Height100pc d-flex align-items-center">--}}
    {{--                    <div class="Text1 ">@lang('site.page.cabinet.sidebar-inv-Profit'):</div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="col-6">--}}
    {{--                <div class="Text2">DAI {{ $user->free_active }}</div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--    <div class="ListCab">--}}
    {{--        <div class="row no-gutters">--}}
    {{--            <div class="col-6">--}}
    {{--                <div class="Height100pc d-flex align-items-center">--}}
    {{--                    <div class="Text1">@lang('site.page.cabinet.sidebar-inv-Available-balance'):</div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="col-6">--}}
    {{--                <div class="Text2">DAI {{ $user->getBalanceInvestorActivities() }}</div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}

    @if($user->is_verification && $user->investor_rank)
        <div class="ListCab">
            <div class="text-center MarginTop15 BTNWidth100">
                <a href="#" data-toggle="modal" data-target="#DepositInEuro"
                   class="btns-green">@lang('site.page.cabinet.Make-Deposit')</a>
            </div>
            {{--            <div class="text-center MarginTop15 BTNWidth100">--}}
            {{--                <a href="#" data-toggle="modal" data-target="#PayInDai" class="btns-green">@lang('site.page.cabinet.pay-to-dai-funds')</a>--}}
            {{--            </div>--}}
        </div>
    @endif
</div>

@if($user->is_verification)
    <div class="InfoListCab">
        <h3>@lang('site.page.cabinet.my-balance-money')</h3>
        <div class="ListCab">
            @foreach($sInvestorAssets as $investorAsset)
                <div class="row no-gutters MarginTop15">
                    <div class="col-2">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text1">{{ $investorAsset->coinValue->symbol ?? '' }}</div>
                        </div>
                    </div>
                    <div class="col-5">
                        @php
                            $balance = $sBalances->where('asset_id', $investorAsset->id)->first()
                        @endphp
                        <input type="hidden" value="{{ $balance->amount ?? 0 }}"
                               name="coin_{{ $investorAsset->coinValue->symbol ?? '' }}">
                        <div class="Height100pc d-flex align-items-center justify-content-end">
                            <div
                                class="Text2">{{ $balance && $balance->amount ? numeric_fmt($balance->amount) : 0 }}</div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="Text2">
                            <a href="#" data-toggle="modal"
                               data-target="#PayIn{{ $investorAsset->coinValue->symbol ?? '' }}"
                               class="btns-green">
                                @lang('site.page.cabinet.btn-pay-to')
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif

@if($user->is_verification && $user->investor_rank)
    <div class="InfoListCab">
        <h3>@lang('site.page.cabinet.inv-sidebar-calc-title')</h3>
        <div class="ListCab">
            <div class="row no-gutters">
                <div class="col-4">
                    <div class="Height100pc d-flex align-items-center">
                        <div class="Text1">@lang('site.page.cabinet.inv-sidebar-calc-money-title')</div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="form-group Height100pc d-flex align-items-center">
                        <select id="s-amount_dai" name="amount_dai" required>
                            <option value=""></option>
                            @foreach($sInvestorAssets as $active)
                                <option value="{{ $active->id }}"
                                        data-price="{{ $active->coinValue ? $active->coinValue->price : 0 }}"
                                        data-symbol="{{ $active->coinValue ? $active->coinValue->symbol : '' }}">
                                    {{ $active->name }}
                                </option>
                            @endforeach
                        </select>
                        {!! Form::error('amount_dai') !!}
                    </div>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-4">
                    <div class="Height100pc d-flex align-items-center">
                        <div class="Text1">@lang('site.page.cabinet.sidebar-conversion_fee')</div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="form-group Height100pc d-flex align-items-center">
                        {!! Form::text('commission_investor', null, ['id' => 's-commission_investor', 'readonly' => true, 'data-commission_investor' => $setting->commission_investor]) !!}
                        {!! Form::error('commission_investor') !!}
                    </div>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-4">
                    <div class="Height100pc d-flex align-items-center">
                        <div class="Text1">@lang('site.page.cabinet.sidebar-to-conclusion')</div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="form-group Height100pc d-flex align-items-center">
                        {!! Form::text('conclusion', null, ['id' => 's-conclusion', 'readonly' => true]) !!}
                        {!! Form::error('conclusion') !!}
                    </div>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-4">
                    <div class="Height100pc d-flex align-items-center">
                        <div class="Text1">€</div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="form-group Height100pc d-flex align-items-center">
                        {!! Form::text('amount_euro', null, ['id' => 's-amount_euro', 'readonly' => true, 'data-course' => $setting->course_investor]) !!}
                        {!! Form::error('amount_euro') !!}
                    </div>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-4">
                    <div class="Height100pc d-flex align-items-center">
                        <div class="Text1" id="s-symbol_in_money"></div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="form-group Height100pc d-flex align-items-center">
                        {!! Form::text('s-amount_in_money', null, ['id' => 's-amount_in_money']) !!}
                    </div>
                </div>
            </div>

            <div class="text-center MarginTop15 BTNWidth100">
                <a href="#" class="btns-orange" data-toggle="modal"
                   data-target="#PayOutEuro">@lang('site.page.cabinet.Withdraw-funds')</a>
            </div>
        </div>
    </div>

    <div class="InfoListCab">
        <h3>@lang('site.page.cabinet.inv-sidebar-withdraw-dai-title')</h3>
        <div class="ListCab">
            {{--        <div class="row no-gutters">--}}
            {{--            <div class="col-4">--}}
            {{--                <div class="Height100pc d-flex align-items-center">--}}
            {{--                    <div class="Text1">DAI:</div>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="col-8">--}}
            {{--                <div class="form-group Height100pc d-flex align-items-center">--}}
            {{--                    {!! Form::text('amount_dai', null, ['id' => 's-amount_dai2', 'required' => true]) !!}--}}
            {{--                    {!! Form::error('amount_dai') !!}--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--        </div>--}}

            <div class="text-center MarginTop15 BTNWidth100">
                <a href="#" class="btns-orange" data-toggle="modal"
                   data-target="#PayOutDai">@lang('site.page.cabinet.Withdraw-funds')</a>
            </div>
        </div>
    </div>
@endif

@if($investorContracts->isNotEmpty())
    <div class="InfoListCab PaddingBot0" id="accordionCab">
        <h3>@lang('site.page.cabinet.My-Deposit')</h3>
        @foreach($investorContracts as $investorContract)
            <a href="#" class="ListCab Links">
                <div class="row no-gutters">
                    <div class="col-7">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text3 ">@lang('site.page.cabinet.Contract')</div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="Text4">{{ $investorContract->created_at->format('d.m.Y') }}</div>
                    </div>
                    <div class="col-7">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text3 ">{{ $investorContract->number }}</div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="Text4">€ {{ $investorContract->amount }} /
                            DAI {{ format_number($investorContract->amount / $setting->course_investor) }}</div>
                    </div>
                </div>
            </a>
        @endforeach
    </div>
@endif

<div class="InfoListCab">
    <h3 class="NoBorder">
        <a href="{{ route('front.cabinet.investor.contracts.archive') }}">@lang('site.page.cabinet.sidebar-close-investor-contracts')</a>
    </h3>
</div>



@section('script-sidebar')
    <script>
        $('#s-amount_dai').on('change', function (e) {
            let course = $(this).find(':selected').data('price') * 1;
            let symbolName = $(this).find(':selected').data('symbol');
            let symbolCount = $("input[name=coin_" + symbolName + "]").val() * 1;
            let amount = course * symbolCount;
            let percent = symbolCount * {{ $setting->conversion_fee }} / 100;
            let conclusion = symbolCount - percent;

            $('#s-amount_euro').val(amount.toFixed(2));
            $('#s-symbol_in_money').text(symbolName);
            $('#s-amount_in_money').val(symbolCount);
            $('#s-commission_investor').val(percent.toFixed(2));
            $('#s-conclusion').val(conclusion.toFixed(2));

            $('#m-amount_tmp-euro').val(conclusion.toFixed(2));
            $('#m-amount-euro').val(conclusion.toFixed(2));
            $('#m-amount-dai').val(symbolCount);
            $('#m-type_asset_id').val($(this).val());
        });

        $('#s-amount_in_money').on('keyup', function (e){
            let amountDai = $('#s-amount_dai').find(':selected');
            let course = amountDai.data('price') * 1;
            let symbolCount = e.target.value;
            let amount = course * symbolCount;
            let percent = symbolCount * {{ $setting->conversion_fee }} / 100;
            let conclusion = symbolCount - percent;

            $('#s-amount_euro').val(amount.toFixed(2));
            $('#s-amount_in_money').val(symbolCount);
            $('#s-commission_investor').val(percent.toFixed(2));
            $('#s-conclusion').val(conclusion.toFixed(2));
            $('#m-amount_tmp-euro').val(conclusion.toFixed(2));
            $('#m-amount-euro').val(conclusion.toFixed(2));
            $('#m-amount-dai').val(symbolCount);
            $('#m-type_asset_id').val(amountDai.val());
        })
    </script>
@endsection

