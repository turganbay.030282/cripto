@section('modals3')
    @if($user->is_verification && $user->credit_level_id)
        @include('front.cabinet.trader.parts._modal_credit_line')
        @include('front.cabinet.trader.parts._modal_payout_coin')
        @include('front.cabinet.trader.parts._modal_transfer_investor')
        @include('front.cabinet.trader.parts._modal_payout_euro')
    @endif
@endsection

<div class="InfoListCab">
    {{--
    <div class="ListCab">
        <div class="row no-gutters">
            <div class="col-7">
                <div class="Height100pc d-flex align-items-center">
                    <div class="Text1">@lang('site.page.cabinet.sidebar-Credit-line'):</div>
                    @if($user->creditLineVerifications()->wait()->exists())
                        <a href="#" class="btn-reload green">
                            &nbsp;<i class="fas fa-sync"></i> <span class="timer-reload">30</span>
                        </a>
                    @endif
                </div>
            </div>
            <div class="col-5">
                <div class="Text2">€ {{ $user->credit_line }}</div>
            </div>
        </div>
    </div>
    --}}
    <div class="ListCab">
        <div class="row no-gutters">
            <div class="col-7">
                <div class="Height100pc d-flex align-items-center">
                    <div class="Text2 text-left">@lang('site.page.cabinet.sidebar-available-payout'):</div>
                </div>
            </div>
            <div class="col-5">
                <div class="Text2">€ <span id="s-user_payout_balance">{{ $user->payout_balance }}</span></div>
            </div>
        </div>
    </div>
    <div class="ListCab">
        <div class="row no-gutters">
            <div class="col-7">
                <div class="Height100pc d-flex align-items-center">
                    <div class="Text1">@lang('site.page.cabinet.sidebar-Profit'):</div>
                </div>
            </div>
            <div class="col-5">
                <div class="Text2">€ {{ format_number($sAmountPortfolioValue - $sAmountContracts) }}</div>
            </div>
        </div>
    </div>
    <div class="ListCab">
        <div class="row no-gutters">
            <div class="col-7">
                <div class="Height100pc d-flex align-items-center">
                    <div class="Text1">@lang('site.page.cabinet.sidebar-Available-balance'):</div>
                </div>
            </div>
            <div class="col-5">
                <div class="Text2">€ {{ $user->balance_trader }}</div>
            </div>
        </div>
    </div>
    @if($user->is_verification && $user->credit_level_id)
        <div class="ListCab">
            <div class="text-center MarginTop15 BTNWidth100">
                <a href="#" class="btns-green" data-toggle="modal"
                   data-target="#CreditLine">@lang('site.page.cabinet.Credit-line')</a>
            </div>
            <div class="text-center MarginTop15 BTNWidth100">
                <a href="{{ route('front.cabinet.trader.contracts.index') }}" data-toggle="modal"
                   data-target="#MTransferInvestor"
                   class="btns-green">@lang('site.page.cabinet.btn-transfer-to-investor')</a>
            </div>
            <div class="text-center MarginTop15 BTNWidth100">
                <a href="{{ route('front.cabinet.trader.contracts.index') }}" data-toggle="modal"
                   data-target="#TraderOut" class="btns-orange">@lang('site.page.cabinet.Withdraw-funds')</a>
            </div>
        </div>
    @endif
</div>

@if($assetContracts->isNotEmpty())
    <div class="InfoListCab PaddingBot0" id="accordionCab">
        <h3>@lang('site.page.cabinet.Portfolio-my-active')</h3>
        <div class="InfoListCabAccord">
            @foreach($assetContracts as $assetContract)
                <div class="CabHeader">
                    <a href="#" class="ShowMoreDetalis collapsed" data-toggle="collapse"
                       data-target="#ListCab-ACC-{{ $assetContract->id }}">
                        <div class="ListCab">
                            <div class="row no-gutters">
                                <div class="col-7">
                                    <div class="Height100pc d-flex align-items-center">
                                        <div class="Text1 ">{{ $assetContract->name }}:</div>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="Text5">€ {{ $assetContract->contracts->sum('amount') }}</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div id="ListCab-ACC-{{ $assetContract->id }}" class="collapse" data-parent="#accordionCab">
                    <div class="ListCab-body">
                        @foreach($assetContract->contracts as $contract)
                            <a href="#" class="BoxDogovor">
                                <div class="ListDogovor">
                                    <div class="row no-gutters">
                                        <div class="col-7">
                                            <div class="Height100pc d-flex align-items-center">
                                                <div
                                                    class="Text1 ">@lang('site.page.cabinet.Contract') {{ $contract->number }}</div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="Text2">1 @lang('site.page.cabinet.Asset')</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ListDogovor">
                                    <div class="row no-gutters">
                                        <div class="col-7">
                                            <div class="Height100pc d-flex align-items-center">
                                                <div class="Text1 ">@lang('site.page.cabinet.Asset-price-purchase'):
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="Text2">€ {{ $contract->amount }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ListDogovor">
                                    <div class="row no-gutters">
                                        <div class="col-7">
                                            <div class="Height100pc d-flex align-items-center">
                                                <div class="Text1 ">@lang('site.page.cabinet.Current-asset-price'):
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="Text2">€ {{ $contract->amount }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ListDogovor">
                                    <div class="row no-gutters">
                                        <div class="col-7">
                                            <div class="Height100pc d-flex align-items-center">
                                                <div class="Text1 ">@lang('site.page.cabinet.Interest-rate'):</div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="Text2">{{ $contract->rate }}%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ListDogovor">
                                    <div class="row no-gutters">
                                        <div class="col-7">
                                            <div class="Height100pc d-flex align-items-center">
                                                <div class="Text1 ">@lang('site.page.cabinet.Term'):</div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div
                                                class="Text2">{{ $contract->period }} @lang('site.page.cabinet.months')</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ListDogovor">
                                    <div class="row no-gutters">
                                        <div class="col-7">
                                            <div class="Height100pc d-flex align-items-center">
                                                <div class="Text1 ">@lang('site.page.cabinet.amount-asset'):</div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="Text2">{{ numeric_fmt($contract->amount_asset) }}</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif

@if($contractFreeAssets)
    <div id="contractFreeAssets" class="InfoListCab TileOtst">
        <h3>@lang('site.page.cabinet.trader-free-active')</h3>
        @foreach($contractFreeAssets as $key => $contractFreeAsset)
            <div class="ListCab">
                <div class="row no-gutters">
                    <div class="col-7">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text1 ">{{ $contractFreeAsset['symbol'] }}:</div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="Text2 fee-asset-{{ $contractFreeAsset['id'] }}"
                             data-amount-asset="{{ $contractFreeAsset['amount_asset'] }}"> {{ $contractFreeAsset['amount_asset'] }}</div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div id="MoneyWithdrawSidebar" class="InfoListCab">
        <h3>@lang('site.page.cabinet.sidebar-title-calc-payot')</h3>
        <div class="ListCab">
            <div class="row">
                <div class="col-6">
                    <div class="form-group Height100pc d-flex align-items-center">
                        {!! Form::select('coin', $contractFreeAssets->pluck('symbol', 'id')->all(), null, ['class' => 'select-coin', 'required' => true, 'placeholder' => '']) !!}
                        {!! Form::error('coin') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group Height100pc d-flex align-items-center">
                        {!! Form::text('coin_value', null, ['class' => 'coin-value text-right']) !!}
                        {!! Form::error('coin_value') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="Height100pc d-flex align-items-center">
                        <div class="Text1">@lang('site.page.cabinet.sidebar-cost-active')</div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group text-right Height100pc d-flex align-items-center">
                        <span class="cost-active"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="Height100pc d-flex align-items-center">
                        <div class="Text1">@lang('site.page.cabinet.sidebar-conversion_fee')</div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group text-right Height100pc d-flex align-items-center">
                        <span class="conversion_fee"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="Height100pc d-flex align-items-center">
                        <div class="Text1">@lang('site.page.cabinet.sidebar-to-conclusion')</div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group text-right Height100pc d-flex align-items-center">
                        <span class="conclusion"></span>
                    </div>
                </div>
            </div>
            <div class="text-center MarginTop15 BTNWidth100">
                <a href="#" class="btns-orange" data-toggle="modal"
                   data-target="#PayOutEuro">@lang('site.page.cabinet.Withdraw-funds')</a>
            </div>
        </div>
    </div>

    <div id="CoinTransferSidebar" class="InfoListCab">
        <h3>@lang('site.page.cabinet.sidebar-title-transfer-active')</h3>
        <div class="ListCab">
            <div class="row">
                <div class="col-6">
                    <div class="form-group Height100pc d-flex align-items-center">
                        {!! Form::select('coin', $contractFreeAssets->pluck('symbol', 'id')->all(), null, ['class' => 'select-coin', 'required' => true, 'placeholder' => '']) !!}
                        {!! Form::error('coin') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group Height100pc d-flex align-items-center">
                        {!! Form::text('coin_value', null, ['class' => 'coin-value text-right']) !!}
                        {!! Form::error('coin_value') !!}
                        {!! Form::hidden('wallet', null, ['id' => 'st-coin-wallet']) !!}
                        {!! Form::error('wallet') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="Height100pc d-flex align-items-center">
                        <div class="Text1">@lang('site.page.cabinet.sidebar-cost-active')</div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group text-right Height100pc d-flex align-items-center">
                        <span class="cost-active"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="Height100pc d-flex align-items-center">
                        <div class="Text1">@lang('site.page.cabinet.sidebar-conversion_fee')</div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group text-right Height100pc d-flex align-items-center">
                        <span class="conversion_fee"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="Height100pc d-flex align-items-center">
                        <div class="Text1">@lang('site.page.cabinet.sidebar-to-conclusion')</div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group text-right Height100pc d-flex align-items-center">
                        <span class="conclusion"></span>
                    </div>
                </div>
            </div>
            <div class="text-center MarginTop15 BTNWidth100">
                <a href="#" class="btns-orange" data-toggle="modal"
                   data-target="#PayOutCoin">@lang('site.page.cabinet.sidebar-btn-transfer-active')</a>
            </div>
        </div>
    </div>
@endif

<div class="InfoListCab">
    <h3>@lang('site.page.cabinet.sidebar-title-buy-active')</h3>
    <div class="ListCab">
        {!! Form::open(['url' => route('front.cabinet.trader.payout.buyAsset'), 'method' => 'POST']) !!}
        <div class="row">
            <div class="col-6">
                <div class="Height100pc d-flex align-items-center">
                    <div class="Text1">@lang('site.page.form-payment-amount'):</div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group Height100pc d-flex align-items-center">
                    {!! Form::text('amount', null, ['id' => 'st-buy-amount', 'required' => true]) !!}
                    {!! Form::error('amount') !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group Height100pc d-flex align-items-center">
                    {!! Form::select('coin', $sAssets, null, ['id' => 'st-buy-coin', 'required' => true, 'placeholder' => '']) !!}
                    {!! Form::error('coin') !!}
                </div>
            </div>
            <div class="col-6">
                <div class="form-group Height100pc d-flex align-items-center">
                    <span id="st-amount-buy-active"></span>
                </div>
            </div>
        </div>
        <div class="text-center MarginTop15 BTNWidth100">
            <button type="submit" class="btns-orange">@lang('site.page.cabinet.sidebar-btn-buy-active')</button>
        </div>

        {!! Form::close() !!}
    </div>
</div>

<div class="InfoListCab TileOtst">
    <h3>@lang('site.page.cabinet.Portfolio-liquidity')</h3>
    <div class="ListCab">
        <div class="row no-gutters">
            <div class="col-7">
                <div class="Height100pc d-flex align-items-center">
                    <div class="Text1 ">@lang('site.page.cabinet.Invested'):</div>
                </div>
            </div>
            <div class="col-5">
                <div class="Text2">€ {{ $sAmountContracts }}</div>
            </div>
        </div>
    </div>
    <div class="ListCab">
        <div class="row no-gutters">
            <div class="col-7">
                <div class="Height100pc d-flex align-items-center">
                    <div class="Text1">@lang('site.page.cabinet.Portfolio-cost'):</div>
                </div>
            </div>
            <div class="col-5">
                <div class="Text2">€ {{ $sAmountPortfolioValue }}</div>
            </div>
        </div>
    </div>
    <div class="ListCab">
        <div class="row no-gutters">
            <div class="col-7">
                <div class="Height100pc d-flex align-items-center">
                    <div class="Text1">@lang('site.page.cabinet.Profitability'):</div>
                </div>
            </div>
            <div class="col-5">
                <div class="Text2">€ {{ format_number($sAmountPortfolioValue - $sAmountContracts) }}</div>
            </div>
        </div>
    </div>
</div>

@if($sContractPays->isNotEmpty())
    <div class="InfoListCab">
        <h3>@lang('site.page.cabinet.Portfolio-to-pay')</h3>
        @foreach($sContractPays as $sContractPay)
            <a href="#"
               class="ListCab Links {{ $sContractPay->getNextDatePaymentCarbon() && $sContractPay->getNextDatePaymentCarbon()->lt(\Carbon\Carbon::now()->endOfDay()) ? 'Backlog' : ''}}"
               data-toggle="modal" data-target="#DogovorPaymentTraderSBar{{ $sContractPay->id }}">
                <div class="row no-gutters">
                    <div class="col-7">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text3 ">@lang('site.page.cabinet.Contract')</div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="Text4">{{ $sContractPay->getNextDatePayment() }}</div>
                    </div>
                    <div class="col-7">
                        <div class="Height100pc d-flex align-items-center">
                            <div class="Text3 ">{{ $sContractPay->number }}</div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="Text4">€ {{ format_number($sContractPay->calcTraderAmount()) }}</div>
                    </div>
                </div>
            </a>
        @endforeach
        <div class="MarginTop15 text-center">
            <a href="#" data-toggle="modal" data-target="#DogovorPaymentTraderSBarAll"
               class="btns-green">@lang('site.page.cabinet.PayAll')</a>
        </div>
    </div>
@endif

<div class="InfoListCab">
    <h3 class="NoBorder"><a
            href="{{ route('front.cabinet.trader.contracts.archive') }}">@lang('site.page.cabinet.sidebar-close-trader-contracts')</a>
    </h3>
</div>

@section('modals-s-bar')
    @foreach($sContractPays as $sContractPay)
        @include('front.cabinet.trader.parts._modal_payment_send', ['idModal' => 'DogovorPaymentTraderSBar'.$sContractPay->id, 'contract' => $sContractPay])
    @endforeach

    @include('front.cabinet.trader.parts._modal_payment_send_all', ['idModal' => 'DogovorPaymentTraderSBarAll', 'contracts' => $sContractPays])

    @include('front.cabinet.trader.parts._modal_withdraw')
@endsection

@section('script-sidebar')
    <script>
        // calculate withdraw
        $('#MoneyWithdrawSidebar select.select-coin').on('change', function (e) {
            let box = $(this).closest('#MoneyWithdrawSidebar');
            let coinId = $(this).val();
            let course = $('#contractFreeAssets .fee-asset-' + coinId).data('amount-asset') * 1;
            box.find('.coin-value').val(course);
            $.ajax({
                url: "{{ route('front.cabinet.ajax.coinPrice') }}",
                method: 'POST',
                data: {coin: coinId}
            }).done(function (response) {
                if (response.hasOwnProperty('price')) {
                    box.find('.cost-active').text((course * response.price).toFixed(2));
                    box.find('.conversion_fee').text((course * response.price * {{ $setting->conversion_fee }} / 100).toFixed(2));
                    let conclusion = course * response.price - course * response.price * {{ $setting->conversion_fee }} / 100;
                    box.find('.conclusion').text(conclusion.toFixed(2));
                    $('#m-amount_tmp-euro').val(conclusion.toFixed(2));

                    let form = $('#PayOutEuro form');
                    form.find('input[name="coin"]').val(coinId);
                    form.find('input[name="coin_value"]').val(course);
                }
            });
        });

        // calculate withdraw
        $('#MoneyWithdrawSidebar .coin-value').on('keyup', function (e) {
            let box = $(this).closest('#MoneyWithdrawSidebar');
            let coinAmount = e.target.value * 1;
            let coinId = box.find('select.select-coin').val();
            let counValue = $(this).val();
            let course = $('#contractFreeAssets .fee-asset-' + coinId).data('amount-asset') * 1;
            if (coinAmount > course) {
                sendMessage('{{trans('site.page.cabinet.sidebar-payot-error-amount')}}')
                $(this).val(course);
                coinAmount = course;
            }
            $.ajax({
                url: "{{ route('front.cabinet.ajax.coinPrice') }}",
                method: 'POST',
                data: {coin: coinId}
            }).done(function (response) {
                if (response.hasOwnProperty('price')) {
                    box.find('.cost-active').text((coinAmount * response.price).toFixed(2));
                    box.find('.conversion_fee').text((coinAmount * response.price * {{ $setting->conversion_fee }} / 100).toFixed(2));
                    let conclusion = coinAmount * response.price - coinAmount * response.price * {{ $setting->conversion_fee }} / 100;
                    box.find('.conclusion').text(conclusion.toFixed(2));
                    $('#m-amount_tmp-euro').val(conclusion.toFixed(2));

                    let form = $('#PayOutEuro form');
                    form.find('input[name="coin"]').val(coinId);
                    form.find('input[name="coin_value"]').val(counValue);
                }
            });
        });

        //calculate transfer
        $('#CoinTransferSidebar select.select-coin').on('change', function (e) {
            let box = $(this).closest('#CoinTransferSidebar');
            let coinId = $(this).val();
            let course = $('#contractFreeAssets .fee-asset-' + coinId).data('amount-asset') * 1;
            box.find('.coin-value').val(course);
            let percent = course * {{ $setting->conversion_fee }} / 100;
            let conclusion = course - percent;
            box.find('.cost-active').text(course.toFixed(8));
            box.find('.conversion_fee').text(percent.toFixed(8));
            box.find('.conclusion').text(conclusion.toFixed(8));
            $('#stf-coin-value-transfer').val((conclusion).toFixed(8));

            let form = $('form#transferAssetForm');
            form.find('input[name="coin"]').val(coinId);
            form.find('input[name="coin_value"]').val(course);
        });

        //calculate transfer
        $('#CoinTransferSidebar .coin-value').on('keyup', function (e) {
            let box = $(this).closest('#CoinTransferSidebar');
            let coinAmount = e.target.value * 1;
            let coinId = box.find('select.select-coin').val();
            let course = $('#contractFreeAssets .fee-asset-' + coinId).data('amount-asset') * 1;
            if (coinAmount > course) {
                sendMessage('{{trans('site.page.cabinet.sidebar-payot-error-amount')}}')
                coinAmount = course;
                $(this).val(course);
            }
            let percent = coinAmount * {{ $setting->conversion_fee }} / 100;
            let conclusion = coinAmount - percent;
            box.find('.cost-active').text(coinAmount.toFixed(8));
            box.find('.conversion_fee').text(percent.toFixed(8));
            box.find('.conclusion').text(conclusion.toFixed(8));
            $('#stf-coin-value-transfer').val(conclusion.toFixed(8));

            let form = $('form#transferAssetForm');
            form.find('input[name="coin"]').val(coinId);
            form.find('input[name="coin_value"]').val(coinAmount);
        });

        $('#TraderOut .btn-withdraw').on('click', function (e) {
            e.preventDefault();
            var balancePayout = $('#s-user_payout_balance').text() * 1;
            if ($('#TraderOut input[name="amount"]').val() * 1 > balancePayout) {
                sendMessage('{{trans('site.page.cabinet.sidebar-payot-error-amount')}}');
                $('#TraderOut input[name="amount"]').val(balancePayout);
                return;
            }
            $(this).closest('form').submit();
        });

        $('#st-coin-value').closest('form').find('button').on('click', function (e) {
            e.preventDefault();
            let course = $('.fee-asset-' + $('#st-coin').val()).data('amount-asset') * 1;
            if ($('#st-coin-value').val() * 1 > course) {
                sendMessage('{{trans('site.page.cabinet.sidebar-payot-error-amount')}}')
                $('#st-coin-value').val(course);
                return;
            }
            $(this).closest('form').submit();
        });

        $('#btn-coin-transfer').on('click', function () {
            $('#st-coin-wallet').val($('#stf-coin-wallet').val()).closest('form').submit();
            $('#PayOutCoin').modal('hide');
        });

        $('#st-buy-amount').on('change', function (e) {
            let amount = $(this).val() * 1;
            if (amount) {
                var avalaibleAmount = $('#s-user_payout_balance').text() * 1;
                if (amount > avalaibleAmount) {
                    sendMessage('{{trans('site.page.cabinet.sidebar-payot-error-amount')}}')
                    $('#st-buy-amount').val(avalaibleAmount);
                    return;
                }
                $.ajax({
                    url: "{{ route('front.cabinet.ajax.coinPrice') }}",
                    method: 'POST',
                    data: {coin: $('#st-buy-coin').val()}
                }).done(function (response) {
                    if (response.hasOwnProperty('price')) {
                        $('#st-amount-buy-active').text((amount / response.price).toFixed(8));
                    }
                });
            }
        });
        $('#st-buy-coin').on('change', function (e) {
            let amount = $('#st-buy-amount').val() * 1;
            if (amount) {
                var avalaibleAmount = $('#s-user_payout_balance').text() * 1;
                if (amount > avalaibleAmount) {
                    sendMessage('{{trans('site.page.cabinet.sidebar-payot-error-amount')}}')
                    $('#st-buy-amount').val(avalaibleAmount);
                    return;
                }
                $.ajax({
                    url: "{{ route('front.cabinet.ajax.coinPrice') }}",
                    method: 'POST',
                    data: {coin: $('#st-buy-coin').val()}
                }).done(function (response) {
                    if (response.hasOwnProperty('price')) {
                        $('#st-amount-buy-active').text((amount / response.price).toFixed(8));
                    }
                });
            }
        });
    </script>
@endsection
