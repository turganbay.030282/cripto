<!DOCTYPE html>
<html>
<head>
    @include("front.layouts.parts._head")
    @yield('title')
    <title>{{ env('APP_NAME') }}</title>
    <link rel="stylesheet" href="/css/cabinet.css?v=1.0.4">
    @yield('style')
    @yield('style2')
</head>
<body>

@include("front.layouts.parts._header", ['class' => 'White Fixed'])

<div class="wrapp Cabinet Width1260">
    <div class="Content">
        @include("front.layouts.parts._flash")
        @yield('content')
    </div>
</div>

<div class="holder">
    <div class="preloader"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
</div>

@include("front.layouts.parts._scripts")
<script src="/js/cabinet.js?v=1.0.1"></script>
@yield('modals')
@yield('modals2')
@yield('modals3')
@yield('modals-s-bar')
@yield('script-modals')
@yield('script-modals2')
@yield('script-modals3')
@yield('script-modals4')
@yield('script')
@yield('script2')
@yield('script-sidebar')
</body>
</html>
