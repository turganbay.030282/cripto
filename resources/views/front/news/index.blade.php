@extends('front.layouts.layout')

@section('meta')
    <title>News</title>
    <meta name="description" content="News">
    <meta name="keywords" content="News">
    <meta property="og:title" content="News">
    <meta property="og:description" content="News">
@endsection
@section('classHeader'){{'Green'}}@endsection
@section('content')

    <div class="wrapp">
        <div class="Content">
            <div class="container">
                <div class="InfoPage">@lang('site.news.most-interesting')</div>
                <h1 class="Title">@lang('site.news.title')</h1>
                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                        @if($news->isNotEmpty())
                            <div class="row">
                                @foreach($news as $new)
                                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div class="ListNews">
                                            <a href="{{ route('front.news.show', $new->slug) }}">
                                                <img class="imageRespon" src="{{ $new->getCropPhotoUrl() }}" alt="{{ trans_field($new, 'title') }}">
                                            </a>
                                            <div class="PaddBox">
                                                <div class="Date">{{ $new->created_at->format('d/m') }}</div>
                                                <h2><a href="{{ route('front.news.show', $new->slug) }}">{{ trans_field($new, 'title') }}</a></h2>
                                                <a href="{{ route('front.news.show', $new->slug) }}" class="ReadMore">@lang('site.news.Read-more')</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        {{$news->links()}}
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="SideBar">
                            @if($categories->isNotEmpty())
                                <h3>@lang('site.news.Categories')</h3>
                                <nav class="SedeBarMenu Tag">
                                    <ul>
                                        @foreach($categories as $category)
                                            <li><a href="{{ route('front.news.category', $category->slug) }}">{{ trans_field($category, 'title') }}</a></li>
                                        @endforeach
                                    </ul>
                                </nav>
                            @endif
                            @if($newPopulars->isNotEmpty())
                                <h3>@lang('site.news.Most-Popular')</h3>
                                <nav class="SedeBarMenu">
                                    <ul>
                                        @foreach($newPopulars as $new)
                                            <li><a href="{{ route('front.news.show', $new->slug) }}">{{ trans_field($new, 'title') }}</a></li>
                                        @endforeach
                                    </ul>
                                </nav>
                            @endif
{{--                            <h3>Архив</h3>--}}
{{--                            <nav class="SedeBarMenu Arhive">--}}
{{--                                <ul>--}}
{{--                                    <li><a href="#">July 2017</a><span>(15)</span></li>--}}
{{--                                    <li><a href="#">July 2017</a><span>(15)</span></li>--}}
{{--                                    <li><a href="#">July 2017</a><span>(15)</span></li>--}}
{{--                                    <li><a href="#">July 2017</a><span>(15)</span></li>--}}
{{--                                    <li><a href="#">July 2017</a><span>(15)</span></li>--}}
{{--                                    <li><a href="#">July 2017</a><span>(15)</span></li>--}}
{{--                                    <li><a href="#">July 2017</a><span>(15)</span></li>--}}
{{--                                </ul>--}}
{{--                            </nav>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
