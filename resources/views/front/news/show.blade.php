@extends('front.layouts.layout')

@section('meta')
    <title>{{$new->getSeoTitle()}}</title>
    <meta name="description" content="{{$new->meta_desc}}">
    <meta name="keywords" content="{{$new->meta_key}}">
    <meta property="og:title" content="{{$new->getSeoTitle()}}">
    <meta property="og:description" content="{{$new->meta_desc}}">
@endsection
@section('classHeader'){{'White'}}@endsection
@section('content')

    <div class="wrapp">
        <div class="Content">
            <div class="container">
                <h1 class="Title TopLine">{{ trans_field($new, 'title') }}</h1>
                <div class="EntryNews Tipography">
                    <div class="ImageNews">
                        <img class="imageRespon" src="{{ $new->getPhotoUrl() }}" alt="{{ trans_field($new, 'title') }}">
                    </div>
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                            @if($author)
                                <div class="Autor">
                                    <img src="{{ $author->getCropPhotoUrl() }}" alt="image">
                                    <div class="AutorTitle">{{ $author->full_name }}</div>
                                    <div class="Descrip">{{ $author->position }}</div>
                                </div>
                            @endif
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                            {!! trans_field($new, 'content') !!}
                        </div>
                    </div>

                    @if($lastNews->isNotEmpty())
                    <div class="LastNews">
                        <h2>@lang('site.news.Last news')</h2>
                        <div class="row">
                            @foreach($lastNews as $item)
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <div class="ListNews">
                                    <a href="{{ route('front.news.show', $item->slug) }}"><img class="imageRespon" src="{{ $item->getCropPhotoUrl() }}" alt=""></a>
                                    <div class="PaddBox">
                                        <div class="Date">{{ $new->created_at->format('d/m') }}</div>
                                        <h2><a href="{{ route('front.news.show', $item->slug) }}">{{ trans_field($item, 'title') }}</a></h2>
                                        <a href="{{ route('front.news.show', $item->slug) }}" class="ReadMore">@lang('site.news.Read-more')</a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection
