@extends('front.layouts.layout')

@section('meta')
    <title>{{$page->getSeoTitle()}}</title>
    <meta name="description" content="{{trans_field($page, 'meta_desc')}}">
    <meta name="keywords" content="{{trans_field($page, 'meta_key')}}">
    <meta property="og:title" content="{{$page->getSeoTitle()}}">
    <meta property="og:description" content="{{trans_field($page, 'meta_desc')}}">
@endsection

@section('classHeader'){{'White'}}@endsection

@section('content')

    <div class="wrapp">
        <div class="Content Page Tipography">
            <div class="container">
                <h1 class="Title text-left">{{ trans_field($page, 'title') }}</h1>
                {!! trans_field($page, 'content') !!}
            </div>
        </div>
    </div>

@endsection
