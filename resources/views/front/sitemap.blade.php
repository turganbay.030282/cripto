<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{ route('front.home') }}</loc>
        <lastmod>{{ gmdate(DateTime::W3C, \Carbon\Carbon::yesterday()->timestamp) }}</lastmod>
        <changefreq>monthly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{ route('front.static-page.about') }}</loc>
        <lastmod>{{ gmdate(DateTime::W3C, \Carbon\Carbon::now()->subMonth()->timestamp) }}</lastmod>
        <changefreq>monthly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{ route('front.static-page.faq') }}</loc>
        <lastmod>{{ gmdate(DateTime::W3C, \Carbon\Carbon::now()->subMonth()->timestamp) }}</lastmod>
        <changefreq>monthly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{ route('front.static-page.investors') }}</loc>
        <lastmod>{{ gmdate(DateTime::W3C, \Carbon\Carbon::now()->subMonth()->timestamp) }}</lastmod>
        <changefreq>monthly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{ route('front.static-page.contact') }}</loc>
        <lastmod>{{ gmdate(DateTime::W3C, \Carbon\Carbon::now()->subMonth()->timestamp) }}</lastmod>
        <changefreq>monthly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{ route('front.static-page.cert') }}</loc>
        <lastmod>{{ gmdate(DateTime::W3C, \Carbon\Carbon::now()->subMonth()->timestamp) }}</lastmod>
        <changefreq>monthly</changefreq>
        <priority>1.0</priority>
    </url>
    @if($pages->isNotEmpty())
        @foreach($pages as $page)
            <url>
                <loc>{{ route('front.pages.show', $page->slug) }}</loc>
                <lastmod>{{ gmdate(DateTime::W3C, $page->updated_at->timestamp) }}</lastmod>
                <changefreq>daily</changefreq>
                <priority>1.0</priority>
            </url>
        @endforeach
    @endif
</urlset>
