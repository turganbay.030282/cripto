@extends('front.layouts.layout')

@section('title')
    <title>О нас - Assets Core</title>
@endsection

@section('classHeader'){{'Big About'}}@endsection

@section('header_box')
    <div class="BoxStart">
        <h1>{{ trans_field($pageAbout, 'title') }}</h1>
        <h2>{{ trans_field($pageAbout, 'desc') }}</h2>
        <div class="MarginTop30">
            <a href="{{ route('register') }}" class="btns-orange">{{ trans_field($pageAbout, 'btn_text') }}</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="wrapp">
        <div class="container">
            <div class="Content About">
                <div class="InfoPage">{{ trans_field($pageAbout, 'benefit_title') }}</div>
                <h2 class="Title">{{ trans_field($pageAbout, 'benefit_sub_title') }}</h2>
                <div class="BoxListAbout">
                    <div class="row">
                        @for($i=1;$i<=6;$i++)
                        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="ListAbout ListAbout-{{ $i }}" style="background-image: url('{{ $pageAbout->getPhotoUrl($i==1 ?'benefit_b_icon' : 'benefit_b_icon'.$i) }}');">
                                <h3>{{ trans_field($pageAbout, $i==1 ?'benefit_b_title' : 'benefit_b_title'.$i) }}</h3>
                                <p>{{ trans_field($pageAbout, $i==1 ?'benefit_b_desc' : 'benefit_b_desc'.$i) }}</p>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="Priority">
        <div class="container">
            <div class="MaxWidth890">
                <div class="TitlePrior">{{ trans_field($pageAbout, 'priority_title') }}</div>
                <h2>{{ trans_field($pageAbout, 'priority_sub_title') }}</h2>
                @for($i=1;$i<=4;$i++)
                <div class="ListPriority">
                    <h3>{{ trans_field($pageAbout, $i==1 ?'priority_b_title' : 'priority_b_title'.$i) }}</h3>
                    {!! trans_field($pageAbout, $i==1 ?'priority_b_desc' : 'priority_b_desc'.$i) !!}
                </div>
                @endfor
            </div>
        </div>
    </div>
@endsection
