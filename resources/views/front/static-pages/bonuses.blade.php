@extends('front.layouts.temp')

@section('title')
    <title>Bonuses - Assets Core</title>
@endsection

@section('content')
    <div class="SectionPartner">
        <div class="container">
            <h1>{{ $pageBonus->{'title:'.$locale} }}</h1>
            <div class="Sec1">
                <h2>{{ $pageBonus->{'subtitle:'.$locale} }}</h2>
                <div class="BoxSend">
                    @if($user)
                        {!! Form::open(['url' => route('front.cabinet.profile.sendPromo')]) !!}
                            <div class="form-group">
                                <h6>@lang('site.page.cabinet.Enter-friend-email')</h6>
                                {!! Form::text('email', null, ['placeholder' => 'Email', 'required' => true]) !!}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btns-orange">@lang('site.page.form-btn-send')</button>
                            </div>
                        {!! Form::close() !!}
                    @else
                        <div class="form-group">
                            <h6>@lang('site.page.cabinet.Enter-friend-email')</h6>
                            {!! Form::text('email', null, ['placeholder' => 'Email', 'required' => true]) !!}
                        </div>
                        <div class="form-group">
                            <a href="{{ route('register') }}" class="btns-orange">@lang('site.page.form-btn-send')</a>
                        </div>
                    @endif
                </div>
                <div class="BoxSend">
                    <div class="form-group">
                        <h6>@lang('site.page.cabinet.Referral-link')</h6>
                        <div class="CopyReffBox">
                            <input id="CopyReff" type="text" placeholder="Email" value="{{ env('APP_URL') }}?ref={{ $user ? $user->promo_code : '' }}">
                        </div>
                    </div>
                    @if($user)
                        <div class="form-group">
                            <button class="btns-orange" onclick="CopyReff()">{{ trans('site.page.cabinet.bonuses-copy-url') }}</button>
                        </div>
                    @else
                        <div class="form-group">
                            <a href="{{ route('register') }}" class="btns-orange">{{ trans('site.page.cabinet.bonuses-copy-url') }}</a>
                        </div>
                    @endif
                </div>
            </div>
            <h6 class="LinkPar">
                <a href="{{ $user ? route('front.cabinet.client.bonuses') : route('register') }}">
                    {{ trans('site.page.cabinet.bonuses-details') }} <i class="fal fa-long-arrow-right"></i>
                </a>
            </h6>
        </div>
    </div>

    <div class="SectionPartner GreenBG">
        <div class="container">
            <div class="Sec2">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                        <div class="IconBox">
                            <h2>{{ $pageBonus->{'title1:'.$locale} }}</h2>
                            <div class="Left">
                                <i class="fas fa-user-tie"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-12 col-sm-12 col-12">
                        <div class="IconArrow">
                            <i class="fal fa-long-arrow-right d-none d-lg-block"></i>
                            <i class="fal fa-long-arrow-down d-block d-lg-none"></i>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-5 col-md-12 col-sm-12 col-12">
                        <div class="MarginTop45 MarginRespTopNone">
                            <div class="Text">
                                {!! $pageBonus->{'content1:'.$locale} !!}
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="SectionPartner">
        <div class="container">
            <div class="Sec3">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 col-md-12 col-sm-12 col-12 oreder-xl-1 order-lg-1 order-md-3 order-3">
                        <div class="Text">
                            {!! $pageBonus->{'content2:'.$locale} !!}
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-12 col-sm-12 col-12 oreder-xl-2 order-lg-2 order-md-2 order-2">
                        <div class="IconArrow">
                            <i class="fal fa-long-arrow-left d-none d-lg-block"></i>
                            <i class="fal fa-long-arrow-down d-block d-lg-none"></i>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 oreder-xl-3 order-lg-3 order-md-1 order-1">
                        <div class="IconBox">
                            <h2>{{ $pageBonus->{'title2:'.$locale} }}</h2>
                            <div class="Left">
                                <i class="fas fa-user-tie"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="SectionPartner GreenBG">
        <div class="container">
            <div class="Sec4">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                        <div class="IconBox">
                            <i class="fal fa-coins"></i>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <h2>{{ $pageBonus->{'title3:'.$locale} }}</h2>
                        <div class="Text">
                            {!! $pageBonus->{'content3:'.$locale} !!}
                        </div>
                    </div>
                </div>
                @if(!$user)
                    <div class="text-center">
                        <a href="{{ route('register') }}" class="btns-orange">{{ trans('site.page.cabinet.bonuses-start') }}</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
