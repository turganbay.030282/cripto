@extends('front.layouts.layout')

@section('title')
    <title>Сертификаты и лицензии - Assets Core</title>
@endsection

@section('classHeader'){{'White'}}@endsection

@section('content')
    <div class="wrapp">
        <div class="Content">
            <div class="container">
                <div class="InfoPage">@lang('site.page.info-about-us')</div>
                <h1 class="Title">@lang('site.page.cert-title')</h1>
                @if($certificates->isNotEmpty())
                    <div class="row">
                        @foreach($certificates as $certificate)
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="listCert">
                                <a href="{{ $certificate->getFileUrl() }}" target="_blank">
                                    <img class="imageRespon" src="{{ $certificate->getCropPhotoUrl() }}" alt="{{ trans_field($certificate, 'title') }}">
                                </a>
                                <h2>{{ trans_field($certificate, 'title') }}</h2>
                                <p>{{ trans_field($certificate, 'description') }}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
