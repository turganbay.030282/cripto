@extends('front.layouts.layout')

@section('title')
    <title>Контакты - Assets Core</title>
@endsection

@section('title_2')
    <h1 class="Title">@lang('site.footer.Contacts')</h1>
@endsection

@section('classHeader'){{'Big Contacts'}}@endsection

@section('content')
    <div class="ContactBox">
        <div class="container">
            <div class="ContactInfo">
                <div class="row no-gutters">
                    <div class="col-xl-8 col-lg-8 col-md-12 col-12">
                        <div class="ContactLeft">
                            <h2>@lang('site.page.contact-Send-message')</h2>
                            {!! Form::open(['url' => route('front.static-page.submitContact')]) !!}
                                <div class="row">
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                        <div class="form-group">
                                            <div class="FormText">@lang('site.page.contact-form-Your-name')</div>
                                            {!! Form::text('name', null, ['placeholder' => trans('site.page.contact-form-Your-name')]) !!}
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                        <div class="form-group">
                                            <div class="FormText">@lang('site.page.contact-form-Your-E-mail')</div>
                                            {!! Form::text('email', null, ['placeholder' => trans('site.page.contact-form-Your-E-mail')]) !!}
                                            {!! Form::error('email') !!}
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                        <div class="form-group">
                                            <div class="FormText">@lang('site.page.contact-phone')</div>
                                            {!! Form::text('phone', null, ['placeholder' => trans('site.page.contact-enter-phone')]) !!}
                                            {!! Form::error('phone') !!}
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                                        <div class="form-group">
                                            <div class="FormText">@lang('site.footer.Company')</div>
                                            {!! Form::text('company', null, ['placeholder' => trans('site.page.contact-enter-company')]) !!}
                                            {!! Form::error('company') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="FormText MarginBot15">@lang('site.page.contact-write-us')</div>
                                    {!! Form::textarea('message', null, ['placeholder' => trans('site.page.contact-Enter-your-message')]) !!}
                                    {!! Form::error('message') !!}
                                </div>
                                <div class="form-group">
                                    {!! NoCaptcha::display() !!}
                                    {{ Form::error('g-recaptcha-response') }}
                                </div>
                                <div class="form-group text-right">
                                    <button class="btns-orange" type="submit">@lang('site.page.form-btn-send')</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 col-12">
                        <div class="ContactRight">
                            <h2>@lang('site.page.contact-info')</h2>
                            @if(trans_field($setting, 'contact_address'))
                                <div class="Adress">{{ trans_field($setting, 'contact_address') }}</div>
                            @endif
{{--                            @if($setting->contact_phone)--}}
{{--                                <div class="Phone"><a href="tel:+{{ str_replace(['+', ' ', '(', ')'], '', $setting->contact_phone) }}">{{ $setting->contact_phone }}</a></div>--}}
{{--                            @endif--}}
                            @if($setting->contact_email)
                            <div class="Email"><a href="mailto:{{ $setting->contact_email }}">{{ $setting->contact_email }}</a></div>
                            @endif
                            <div class="social">
                                @if($setting->soc_fb)
                                    <a href="{{ $setting->soc_fb }}" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                @endif
                                @if($setting->soc_tw)
                                    <a href="{{ $setting->soc_tw }}" target="_blank"><i class="fab fa-twitter"></i></a>
                                @endif
                                @if($setting->soc_in)
                                    <a href="{{ $setting->soc_in }}" target="_blank"><i class="fab fa-instagram"></i></a>
                                @endif
                                @if($setting->soc_yt)
                                    <a href="{{ $setting->soc_yt }}" target="_blank"><i class="fab fa-youtube"></i></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapp">
        <div class="container">
            <div class="Content">
                <h2 class="Title">@lang('site.footer.Requisites')</h2>
                <div class="SilverBG text-center">
                    <h3 class="RecvisitText">
                        {{ trans_field($setting, 'req_sia') }}<br>
                        {{ trans_field($setting, 'req_reg') }}<br>
                        {{ trans_field($setting, 'req_seb') }}<br>
                        {{ trans_field($setting, 'req_swift') }}
                    </h3>
                </div>
            </div>
        </div>
    </div>
    {!! NoCaptcha::renderJs() !!}
@endsection
