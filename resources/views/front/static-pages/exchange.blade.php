@extends('front.layouts.temp')

@section('title')
    <title>Exchange - Assets Core</title>
@endsection

@section('content')
    <div class="CabHeaderSec GreenBG">
        <div class="container">
            <h2 class="text-center">Внебиржевые сделки и конвертация</h2>
            <p class="text-center">Торгуйте биткоинами, BNB и другими криптовалютами на единой платформе.</p>
        </div>
    </div>
    <div class="ExchangeContent">
        <div class="ExchangeSec">
            <div class="container">
                Graph
            </div>
        </div>
    </div>        
@endsection
