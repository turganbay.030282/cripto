@extends('front.layouts.layout')

@section('title')
    <title>Faq - Assets Core</title>
@endsection

@section('classHeader'){{'Green'}}@endsection
@section('content')
    <div class="wrapp">
        <div class="Content">
            <div class="container">
                <div class="SectionFAQ MaxWidth890">
                    <div class="InfoPage">@lang('site.page.info-about-us')</div>
                    <h1 class="Title">@lang('site.menu.faq')</h1>
                    @if($category)
                    <div class="CategoryFAQ">
                        <h3>{{ trans_field($category, 'title') }}</h3>
                        <div class="Descrip">{{ trans_field($category, 'description') }}</div>
                        @if($category->items->isNotEmpty())
                            <div class="BoxFAQ" id="accordionFAQ{{ $category->id }}">
                                @foreach($category->items as $item)
                                <div class="FAQList">
                                    <div class="FAQHeader">
                                        <a href="#" class="ShowMoreDetalis collapsed" data-toggle="collapse" data-target="#FAQ{{ $category->id }}{{ $item->id }}">
                                            {{ trans_field($item, 'title') }}
                                        </a>
                                    </div>
                                    <div id="FAQ{{$category->id}}{{$item->id}}" class="collapse" data-parent="#accordionFAQ{{ $category->id }}">
                                        <div class="card-body">
                                            {!! trans_field($item, 'description') !!}
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    @endif
                </div>
            </div>

            @if($categories->isNotEmpty())
                @foreach($categories as $key => $cat)
                    <div class="{{ $key%2 == 0 ? 'SilverBG' : 'WhiteBG' }}">
                        <div class="container">
                            <div class="SectionFAQ MaxWidth890">
                                <div class="CategoryFAQ">
                                    <h3>{{ trans_field($cat, 'title') }}</h3>
                                    <div class="Descrip">{{ trans_field($cat, 'description') }}</div>
                                    @if($cat->items->isNotEmpty())
                                        <div class="BoxFAQ" id="accordionFAQ{{ $cat->id }}">
                                            @foreach($cat->items as $item)
                                                <div class="FAQList">
                                                    <div class="FAQHeader">
                                                        <a href="#" class="ShowMoreDetalis collapsed" data-toggle="collapse" data-target="#FAQ{{ $cat->id }}{{ $item->id }}">
                                                            {{ trans_field($item, 'title') }}
                                                        </a>
                                                    </div>
                                                    <div id="FAQ{{ $cat->id }}{{ $item->id }}" class="collapse" data-parent="#accordionFAQ{{ $cat->id }}">
                                                        <div class="card-body">
                                                            {!! trans_field($item, 'description') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            @endif
        </div>
    </div>
@endsection
