@extends('front.layouts.layout')

@section('title')
    <title>Инвесторам - Assets Core</title>
@endsection

@section('classHeader'){{'Big Investors'}}@endsection
@section('header_box')
    <div class="BoxStart">
        <h1>{{ trans_field($pageInvestor, 'title') }}</h1>
        <h2>{{ trans_field($pageInvestor, 'desc') }}</h2>
        <div class="MarginTop30">
            <a href="#top" class="btns-orange">{{ trans_field($pageInvestor, 'btn_text') }}</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="wrapp">
        <div class="Content Investors">
            <div class="container">
                <h2 class="Title">{{ trans('site.page.investor-title-reward-rates') }}</h2>
                <div class="CalculatorRadio">
                    <div class="MaxWidth890">

                        <div class="row">
                            {{--
                            <div class="col-12 col-md-2">
                                <h5>{{ trans('site.page.investor-title-term') }}</h5>
                            </div>
                            --}}
                            <div class="col-12 col-md-12 col-lg-8 offset-lg-2">
                                <div class="CalcRadioBox">
                                    <div class="d-xl-flex d-lg-flex d-md-flex d-sm-flex justify-content-xl-between justify-content-lg-between justify-content-md-between justify-content-sm-between">
                                        <div class="CalcRadioList">
                                            <input id="Time-1" type="radio" name="Time" value="1" checked>
                                            <label for="Time-1">{{ trans('site.page.investor-term1') }}</label>
                                        </div>
                                        <div class="CalcRadioList">
                                            <input id="Time-2" type="radio" name="Time" value="2">
                                            <label for="Time-2">{{ trans('site.page.investor-term2') }}</label>
                                        </div>
                                        <div class="CalcRadioList">
                                            <input id="Time-3" type="radio" name="Time" value="3">
                                            <label for="Time-3">{{ trans('site.page.investor-term3') }}</label>
                                        </div>
                                        <div class="CalcRadioList">
                                            <input id="Time-4" type="radio" name="Time" value="4">
                                            <label for="Time-4">{{ trans('site.page.investor-term4') }}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="CalcRadioBox">
                                    <div class="d-xl-flex d-lg-flex d-md-flex d-sm-flex justify-content-xl-between justify-content-lg-between justify-content-md-between justify-content-sm-between">
                                        <div class="CalcRadioList">
                                            <input id="Time-5" type="radio" name="Time" value="5">
                                            <label for="Time-5">{{ trans('site.page.investor-term5') }}</label>
                                        </div>
                                        <div class="CalcRadioList">
                                            <input id="Time-6" type="radio" name="Time" value="6">
                                            <label for="Time-6">{{ trans('site.page.investor-term6') }}</label>
                                        </div>
                                        <div class="CalcRadioList">
                                            <input id="Time-7" type="radio" name="Time" value="7">
                                            <label for="Time-7">{{ trans('site.page.investor-term7') }}</label>
                                        </div>
                                        <div class="CalcRadioList">
                                            <input id="Time-8" type="radio" name="Time" value="8">
                                            <label for="Time-8">{{ trans('site.page.investor-term8') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--
                        <div class="row">

                            <div class="col-12 col-md-2">
                                <h5>{{ trans('site.page.investor-title-stake') }}</h5>
                            </div>

                            <div class="col-12 col-md-12 col-lg-8 offset-lg-2">
                                <div class="CalcRadioBox">
                                    <div class="d-xl-flex d-lg-flex d-md-flex d-sm-flex justify-content-xl-between justify-content-lg-between justify-content-md-between justify-content-sm-between">
                                        <div class="CalcRadioList">
                                            <input id="Money-1" type="radio" name="Money" value="1" checked>
                                            <label for="Money-1">{{ trans('site.page.investor-stake1') }}</label>
                                        </div>
                                        <div class="CalcRadioList">
                                            <input id="Money-2" type="radio" name="Money" value="2">
                                            <label for="Money-2">{{ trans('site.page.investor-stake2') }}</label>
                                        </div>
                                        <div class="CalcRadioList">
                                            <input id="Money-3" type="radio" name="Money" value="3">
                                            <label for="Money-3">{{ trans('site.page.investor-stake3') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        --}}
                        <div class="TabBorder">
                            <div class="table-responsive">
                                <table class="table table-striped table-borderless">
                                    <tbody>
                                    @foreach($assets->chunk(4) as $chunk)
                                        <tr>
                                            @foreach($chunk as $asset)
                                                <td>
                                                    @if($asset->coinValue->logo)
                                                        <img src="{{ $asset->coinValue->logo }}" alt="">
                                                    @endif
                                                    {{ $asset->{'name:'.app()->getLocale()} }}
                                                </td>
                                                <td
                                                    @for ($j = 1; $j <= 3; $j++)
                                                        @for ($i = 1; $i <= 8; $i++)
                                                            data-td{{$j}}{{$i}}="{{$asset->calc["td{$j}{$i}"] ?? null}}"
                                                    @endfor
                                                    @endfor
                                                    class="stake-percent">
                                                    <span>{{ $asset->calc['td11'] }}</span>%
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="{{ route('register') }}"
                            class="btns-orange">{{ trans('site.page.investor-btn-text') }}</a>
                        </div>
                    </div>
                </div>

                <div class="InfoPage">@lang('site.page.info-about-us')</div>
                <h2 class="Title">@lang('site.page.investor-making-profit')</h2>
                <div class="ProfitBox">
                    <div class="row">
                        @for($i=1;$i<=3;$i++)
                            <style>
                                .ProfitBox .ProfitList.Profit-{{$i}}:before {
                                    background-image: url('{{$pageInvestor->getPhotoUrl($i==1 ?'about_b_icon' : 'about_b_icon'.$i)}}');
                                }
                            </style>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="ProfitList Profit-{{$i}}">
                                    <h3>{{ trans_field($pageInvestor, $i==1 ?'about_b_title' : 'about_b_title'.$i) }}</h3>
                                    <div
                                        class="TextProfit">{{ trans_field($pageInvestor, $i==1 ?'about_b_desc' : 'about_b_desc'.$i) }}</div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
            {{--
            @if($blockPackages->isNotEmpty())
            <div class="SectionPakets">
                <div class="container">
                    <h2 id="top">@lang('site.page.our-packages-investor')</h2>
                    <div class="row">
                        @foreach($blockPackages as $blockPackage)
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="PaketsList Paket-1" style="background-image: url({{ $blockPackage->getPhotoUrl() }});">
                                    <h3>{{ trans_field($blockPackage, 'name') }}</h3>
                                    <div class="Descrip">{{ trans_field($blockPackage, 'desc') }}</div>
                                    <div class="Descrip2">{{ trans_field($blockPackage, 'price') }}</div>
                                    <div class="MarginTop30">
                                        <a href="{{ route('register') }}" class="btns-orange">@lang('site.page.start')</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
            --}}
            {{--
            <div class="Calculator">
                <div class="container">
                    <div class="Calculator MaxWidth700">
                        <div class="container">
                            <h2 id="top">@lang('site.page.calc-investor-Interactive-form-calculating')</h2>
                            <div class="BoxCalculator">
                                <h3>@lang('site.page.calc-investor-Investment-term')</h3>
                                <input type="text" class="RangeSliderMonth" name="RangeSliderMonth" value="" id="RangeSliderMonth">
                                <h3 class="MarginTop15">@lang('site.page.calc-investor-Investment-amount')</h3>
                                <input type="text" class="RangeSliderMoneyDai" name="RangeSliderMoney" value="" id="RangeSliderMoney">
                                <input type="text" name="RangeSliderMoney11" id="RangeSliderMoney11" value="50">
                            </div>
                            <div class="BoxInvesMaterial">
                                <div class="HeadInvest">@lang('site.page.calc-investor-Profit-calculation-form')</div>
                                <div class="TextInvest1">@lang('site.page.calc-investor-Deposit-rank')</div>
                                <div class="TextInvest2 LineBottom"><span id="depositLevelText">Бронза</span></div>
                                <div class="TextInvest1">@lang('site.page.calc-investor-annual-interest-rate')</div>
                                <div class="TextInvest2 LineBottom"><span id="percent">5</span>%</div>
                                <div class="TextInvest1">@lang('site.page.calc-investor-Total-income')</div>
                                <div class="TextInvest2 LineBottom"><span id="deposit_value">700</span> DAI</div>
                                <div class="TextInvest1">@lang('site.page.calc-investor-Your-profit')</div>
                                <div class="TextInvest2"><span id="amoun_pay">1000</span> DAI</div>
                            </div>
                            <div class="text-center MarginTop15">
                                <a href="{{ route('register') }}" class="btns-orange">@lang('site.page.register-now')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            --}}
            <div class="Benefits">
                <div class="container">
                    <h2 class="Title">{{ trans_field($pageInvestor, 'benefit_title') }}</h2>
                    <div class="row">
                        @for($i=1;$i<=6;$i++)
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <div class="BenefitList Benefit-{{$i}}">
                                    <h3>{{ trans_field($pageInvestor, $i==1 ?'benefit_b_title' : 'benefit_b_title'.$i) }}</h3>
                                    <div
                                        class="BenefitText">{{ trans_field($pageInvestor, $i==1 ?'benefit_b_desc' : 'benefit_b_desc'.$i) }}</div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $('input[name="Time"], input[name="Money"]').on('change', function (e) {
            e.preventDefault()
            let time = $('input[name="Time"]:checked'),
                money = $('input[name="Money"]:checked'),
                td = 'td' + 1 + time.val();
                //td = 'td' + money.val() + time.val();

            $('.stake-percent').each(function (index, element) {
                $(element).find('span').text(
                    $(element).data(td)
                );
            });
        });

        //Calculator
        function calc(amount, month, percent) {
            //let value = amount*1 * (1 + ((percent*1 + month*1 * 30) / 365));
            let value = amount + amount * ((percent / 100) * (month / 12));
            return value.toFixed(2);
        }

        function setCalcValue() {
            $('#RangeSliderMoney11').val($('#RangeSliderMoney').val() * 1);
            $.ajax({
                type: "POST",
                url: '{{ route('front.home.calcInvest') }}',
                data: {amount: $('#RangeSliderMoney').val()},
                success: function (result) {
                    if (result.hasOwnProperty('name')) {
                        let value = calc($('#RangeSliderMoney').val() * 1, $('#RangeSliderMonth').val() * 1, result.value * 1);
                        $('#percent').text(result.value);
                        $('#deposit_value').text(value);
                        $('#amoun_pay').text((value - $('#RangeSliderMoney').val() * 1).toFixed(2));
                        $('#depositLevelText').text(result.name);
                    }
                }
            });
        }

        $(document).ready(function () {
            //setCalcValue();
            $('#RangeSliderMoney11').on('change', function () {
                $('#RangeSliderMoney').data("ionRangeSlider").update({
                    from: $(this).val()
                });
            });
            $('#RangeSliderMonth, #RangeSliderMoney').on('change', function () {
                setCalcValue();
            });
        });
    </script>
@endsection
