<table border="1" cellpadding="1" cellspacing="1" width="100%">
    <thead>
    <tr>
        <th>#</th>
        <th>{{ trans('site.cabinet.contract-investor-table-date',[],app()->getLocale()) }}</th>
        @if($visibleColumn3)
        <th>{{ trans('site.cabinet.contract-investor-table-percent',[],app()->getLocale()) }}</th>
        @endif
        @if($visibleColumn2)
        <th>{{ $symbol }}</th>
        @endif
        @if($visibleColumn4)
        <th>{{ trans('site.cabinet.contract-investor-table-amount',[],app()->getLocale()) }}</th>
        @endif
        @if($visibleColumn)
        <th>{{ trans('site.cabinet.contract-investor-table-amount-dai',[],app()->getLocale()) }} {{ $symbol }}</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @if($payments->isNotEmpty())
        @foreach($payments as $key => $payment)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $payment->date }}</td>
                @if($visibleColumn3)
                <td>{{ $payment->percent }}</td>
                @endif
                @if($visibleColumn2)
                <td>{{ $payment->percent_dai }}</td>
                @endif
                @if($visibleColumn4)
                <td>{{ $payment->amount }}</td>
                @endif
                @if ($visibleColumn)
                    <td>{{ $payment->amount_dai }}</td>
                @endif
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
