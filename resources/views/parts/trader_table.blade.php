<table border="1" cellpadding="1" cellspacing="1" width="100%">
    <thead>
    <tr>
        <th>#</th>
        <th>{{ trans('site.cabinet.contract-trader-table-date',[],app()->getLocale()) }}</th>
        <th>{{ trans('site.cabinet.contract-trader-table-main-debt',[],app()->getLocale()) }}</th>
        <th>{{ trans('site.cabinet.contract-trader-table-other-debt',[],app()->getLocale()) }}</th>
        <th>{{ trans('site.cabinet.contract-trader-table-percent',[],app()->getLocale()) }}</th>
        <th>{{ trans('site.cabinet.contract-trader-table-amount',[],app()->getLocale()) }}</th>
    </tr>
    </thead>
    <tbody>
    @if($payments->isNotEmpty())
        @foreach($payments as $key => $payment)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $payment->date->format("d.m.Y") }}</td>
                <td>{{ $payment->main_amount }}</td>
                <td>{{ $payment->balance_owed }}</td>
                <td>{{ $payment->percent }}</td>
                <td>{{ $payment->amount }}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
