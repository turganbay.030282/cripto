<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Переключение языков
Route::get('setlocale/{lang}', function ($lang) {

    $referer = Redirect::back()->getTargetUrl(); //URL предыдущей страницы
    $parse_url = parse_url($referer, PHP_URL_PATH); //URI предыдущей страницы

    //разбиваем на массив по разделителю
    $segments = explode('/', $parse_url);

    //Если URL (где нажали на переключение языка) содержал корректную метку языка
    if (in_array($segments[1], App\Http\Middleware\LocaleMiddleware::languages())) {
        unset($segments[1]); //удаляем метку
    }

    //Добавляем метку языка в URL (если выбран не язык по-умолчанию)
    if ($lang != App\Http\Middleware\LocaleMiddleware::mainLanguage()) {
        array_splice($segments, 1, 0, $lang);
    }

    //формируем полный URL
    $url = Request::root() . implode("/", $segments);

    //если были еще GET-параметры - добавляем их
    if (parse_url($referer, PHP_URL_QUERY)) {
        $url = $url . '?' . parse_url($referer, PHP_URL_QUERY);
    }
    return redirect($url); //Перенаправляем назад на ту же страницу

})->name('setlocale');

Route::group(
    [
        'prefix' => 'admin',
        'as' => 'admin.',
        'namespace' => 'Admin',
        'middleware' => ['auth', 'can:admin-panel'],
    ],
    function () {
        Route::get('/filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
        Route::post('/filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');

        Route::get('/', 'HomeController@index')->name('home');

        Route::resource('settings', 'SettingsController')->only(["index", "update"]);

        Route::group(['namespace' => 'User', 'as' => 'user.', 'prefix' => 'user'], function () {
            Route::post('users/add-wallet', 'UsersController@addWallet')->name('users.addWallet');
            Route::post('users/remove-wallet/{wallet}', 'UsersController@removeWallet')->name('users.removeWallet');
            Route::post('users/delete', 'UsersController@delete')->name('users.delete');
            Route::post('users/restore', 'UsersController@restore')->name('users.restore');
            Route::get('users/archive', 'UsersController@archive')->name('users.archive');
            Route::resource('users', 'UsersController');

            Route::get('/profile/change-password', 'ProfileController@changePassword')->name('profile.changePassword');
            Route::post('/profile/set-password', 'ProfileController@setPassword')->name('profile.setPassword');

            Route::get('user-verifications/archive', 'UserVerificationsController@archive')->name('user-verifications.archive');
            Route::resource('user-verifications', 'UserVerificationsController')->only('index', 'show', 'update', 'edit');

            Route::get('credit-level-verifications/archive-ur', 'CreditLevelVerificationsController@archiveUr')->name('credit-level-verifications.archiveUr');
            Route::get('credit-level-verifications/archive', 'CreditLevelVerificationsController@archive')->name('credit-level-verifications.archive');
            Route::get('credit-level-verifications/show-ur/{creditLevelVerification}', 'CreditLevelVerificationsController@showUr')->name('credit-level-verifications.showUr');
            Route::get('credit-level-verifications/edit-ur/{creditLevelUrVerification}', 'CreditLevelVerificationsController@editUr')->name('credit-level-verifications.editUr');
            Route::put('credit-level-verifications/update-ur/{creditLevelUrVerification}', 'CreditLevelVerificationsController@updateUr')->name('credit-level-verifications.updateUr');
            Route::resource('credit-level-verifications', 'CreditLevelVerificationsController')->only('index', 'update', 'edit', 'show');

            Route::get('credit-line-verifications/archive', 'CreditLineVerificationsController@archive')->name('credit-line-verifications.archive');
            Route::resource('credit-line-verifications', 'CreditLineVerificationsController')->only('index', 'update', 'edit');

            Route::get('invest-rank-verifications/archive', 'InvestRankVerificationsController@archive')->name('invest-rank-verifications.archive');
            Route::resource('invest-rank-verifications', 'InvestRankVerificationsController')->only('index', 'show', 'update', 'edit');

            Route::get('deposit-line-verifications/archive', 'DepositLineVerificationsController@archive')->name('deposit-line-verifications.archive');
            Route::resource('deposit-line-verifications', 'DepositLineVerificationsController')->only('index', 'update', 'edit');

            Route::get('user-changes/archive', 'UserChangesController@archive')->name('user-changes.archive');
            Route::resource('user-changes', 'UserChangesController')->only('index', 'update', 'edit');
        });

        Route::group(['namespace' => 'Content', 'prefix' => 'content', 'as' => 'content.'], function () {
            Route::get('pages/archive', 'PagesController@archive')->name('pages.archive');
            Route::post('pages/restore', 'PagesController@restore')->name('pages.restore');
            Route::resource('pages', 'PagesController');

            Route::get('blog-categories/archive', 'BlogCategoriesController@archive')->name('blog-categories.archive');
            Route::post('blog-categories/restore', 'BlogCategoriesController@restore')->name('blog-categories.restore');
            Route::resource('blog-categories', 'BlogCategoriesController');

            Route::get('blogs/archive', 'BlogsController@archive')->name('blogs.archive');
            Route::post('blogs/restore', 'BlogsController@restore')->name('blogs.restore');
            Route::resource('blogs', 'BlogsController');

            Route::get('certificates/archive', 'CertificatesController@archive')->name('certificates.archive');
            Route::post('certificates/restore', 'CertificatesController@restore')->name('certificates.restore');
            Route::resource('certificates', 'CertificatesController');

            Route::get('faq-categories/archive', 'FaqCategoriesController@archive')->name('faq-categories.archive');
            Route::post('faq-categories/restore', 'FaqCategoriesController@restore')->name('faq-categories.restore');
            Route::resource('faq-categories', 'FaqCategoriesController');

            Route::get('faqs/archive', 'FaqsController@archive')->name('faqs.archive');
            Route::post('faqs/restore', 'FaqsController@restore')->name('faqs.restore');
            Route::resource('faqs', 'FaqsController');

            Route::get('block-packages/archive', 'BlockPackagesController@archive')->name('block-packages.archive');
            Route::post('block-packages/restore', 'BlockPackagesController@restore')->name('block-packages.restore');
            Route::resource('block-packages', 'BlockPackagesController');

            Route::get('page-about', 'PageAboutController@edit')->name('page-about.edit');
            Route::resource('page-about', 'PageAboutController')->only('update');

            Route::get('page-bonus', 'PageBonusController@edit')->name('page-bonus.edit');
            Route::resource('page-bonus', 'PageBonusController')->only('update');

            Route::get('page-main', 'PageMainController@edit')->name('page-main.edit');
            Route::resource('page-main', 'PageMainController')->only('update');

            Route::get('page-investor', 'PageInvestorController@edit')->name('page-investor.edit');
            Route::resource('page-investor', 'PageInvestorController')->only('update');
        });

        Route::group(['namespace' => 'Handbook', 'prefix' => 'handbook', 'as' => 'handbook.'], function () {
            Route::get('credit-levels/archive', 'CreditLevelsController@archive')->name('credit-levels.archive');
            Route::post('credit-levels/restore', 'CreditLevelsController@restore')->name('credit-levels.restore');
            Route::resource('credit-levels', 'CreditLevelsController');

            Route::get('investor-ranks/archive', 'InvestorRanksController@archive')->name('investor-ranks.archive');
            Route::post('investor-ranks/restore', 'InvestorRanksController@restore')->name('investor-ranks.restore');
            Route::resource('investor-ranks', 'InvestorRanksController');

            Route::get('type-transactions/archive', 'TypeTransactionsController@archive')->name('type-transactions.archive');
            Route::post('type-transactions/restore', 'TypeTransactionsController@restore')->name('type-transactions.restore');
            Route::resource('type-transactions', 'TypeTransactionsController');

            Route::get('assets/archive', 'AssetsController@archive')->name('assets.archive');
            Route::get('assets/coins', 'AssetsController@coins')->name('assets.coins');
            Route::post('assets/restore', 'AssetsController@restore')->name('assets.restore');
            Route::resource('assets', 'AssetsController');

            Route::resource('type-statuses', 'TypeStatusesController');
            Route::resource('type-two-statuses', 'TypeTwoStatusesController');
        });

        Route::post('forms/restore', 'FormsController@restore')->name('forms.restore');
        Route::get('forms/archive', 'FormsController@archive')->name('forms.archive');
        Route::resource('forms', 'FormsController')->only('index', 'destroy');

        Route::post('subscribes/restore', 'SubscribesController@restore')->name('subscribes.restore');
        Route::get('subscribes/archive', 'SubscribesController@archive')->name('subscribes.archive');
        Route::resource('subscribes', 'SubscribesController')->only('index', 'destroy');

        Route::resource('admin-mailings', 'AdminMailingsController')->only('index', 'show', 'create', 'store');

        Route::post('transactions/approve/{transaction}', 'TransactionsController@approve')->name('transactions.approve');
        Route::put('transactions/reject/{transaction}', 'TransactionsController@reject')->name('transactions.reject');
        Route::post('transactions/approve-bonus/{transaction}', 'TransactionsController@approveBonus')->name('transactions.approveBonus');
        Route::get('transactions/approve-trader-out-edit/{transaction}', 'TransactionsController@approveTraderOutEdit')->name('transactions.approve-trader-out-edit');
        Route::post('transactions/approve-trader-out/{transaction}', 'TransactionsController@approveTraderOut')->name('transactions.approve-trader-out');
        Route::post('transactions/approve-trader-transaction/{transaction}', 'TransactionsController@approveTraderTransaction')->name('transactions.approveTraderTransaction');
        Route::post('transactions/approve-trader-sell-transaction/{transaction}', 'TransactionsController@approveTraderIsSell')->name('transactions.approveTraderIsSell');
        Route::post('transactions/approve-deposit-out/{transaction}', 'TransactionsController@approveDepositOut')->name('transactions.approve-deposit-out');
        Route::get('transactions/approve-inv-out-edit/{transaction}', 'TransactionsController@approveInvOutEdit')->name('transactions.approve-inv-out-edit');
        Route::post('transactions/approve-staking/{transaction}', 'TransactionsController@approveStaking')->name('transactions.approve-staking');
        Route::post('transactions/approve-inv-out/{transaction}', 'TransactionsController@approveInvOut')->name('transactions.approve-inv-out');
        Route::post('transactions/approve-inv-out-pay/{transaction}', 'TransactionsController@approveInvOutPay')->name('transactions.approve-inv-out-pay');
        Route::post('transactions/approve-inv-pay-in-dai/{transaction}', 'TransactionsController@approveInvPayInDai')->name('transactions.approveInvPayInDai');
        Route::resource('transactions', 'TransactionsController')->only('index');

        Route::post('converts/close/{convert}', 'ConvertsController@close')->name('converts.close');
        Route::put('converts/cancel/{convert}', 'ConvertsController@cancel')->name('converts.cancel');
        Route::resource('converts', 'ConvertsController')->only('index');

        Route::get('contracts/{contract}/pdf', 'ContractsController@pdf')->name('contracts.pdf');
        Route::resource('contracts', 'ContractsController')->only('index', 'show', 'destroy');

        Route::resource('translations', 'TranslationsController')->except("show", "destroy");

        Route::post('tpl-documents/restore', 'TplDocumentsController@restore')->name('tpl-documents.restore');
        Route::get('tpl-documents/archive', 'TplDocumentsController@archive')->name('tpl-documents.archive');
        Route::resource('tpl-documents', 'TplDocumentsController');

        Route::post('notify-texts/restore', 'NotifyTextsController@restore')->name('notify-texts.restore');
        Route::get('notify-texts/archive', 'NotifyTextsController@archive')->name('notify-texts.archive');
        Route::resource('notify-texts', 'NotifyTextsController');

        Route::group(['namespace' => 'Referrals', 'prefix' => 'referrals', 'as' => 'referrals.'], function () {
            Route::resource('statistic-promo-codes', 'StatisticPromoCodesController')->only('index', 'destroy');
            Route::get('statistics/invites', 'StatisticsController@invites')->name('statistics.invites');
            Route::get('statistics/ref-partners', 'StatisticsController@partners')->name('statistics.partners');
            Route::get('statistics/analytics', 'StatisticsController@analytics')->name('statistics.analytics');
        });


        //todo
        Route::get('/2fa', function (\Illuminate\Http\Request $request){
            if ($request->get('email')){
                $user = \App\Entity\User::query()->where('email', $request->get('email'))->first();
                if ($user){
                    $user->update([
                        'google2fa_enable' => false
                    ]);
                    dd($user->email);
                }
                dd('user not found');
            }
            dd('email required');
        });
    }
);

Route::post('/2faVerify', function () {
    return redirect(URL()->previous());
})->name('2faVerify')->middleware('2fa');

Route::group(['prefix' => App\Http\Middleware\LocaleMiddleware::getLocale()], function () {
    Auth::routes();

    Route::get('/reset/success', 'Auth\ResetPasswordController@success')->name('reset.success');
    Route::get('/register/resend/{email}', 'Auth\RegisterController@showResendForm')->name('register.showResendForm');
    Route::post('/register/resend', 'Auth\RegisterController@resend')->name('register.resend');
    Route::post('/register/send-verify-code', 'Auth\RegisterController@sendVerifyCode')->name('register.sendVerifyCode');
    Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('register.verify');
    Route::get('/verify-email/{email}', 'Auth\RegisterController@verifyEmail')->name('register.verifyEmail');

    Route::group(
        [
            'as' => 'front.',
            'namespace' => 'Front',
        ],
        function () {
            Route::get('/', 'HomeController@index')->name('home');

            Route::post('/calc-invest', 'HomeController@calcInvest')->name('home.calcInvest');
            Route::post('/calc-trader', 'HomeController@calcTrader')->name('home.calcTrader');
            Route::post('/calc-trader-annuity', 'HomeController@calcTraderAnnuity')->name('home.calcTraderAnnuity');
            Route::post('/subscribers', 'HomeController@subscribers')->name('home.subscribers');
            Route::get('/convert', 'ConvertController@index')->name('convert.index');

            Route::get('/sitemap', static function () {
                $pages = \App\Entity\Content\Page::publish()->get();

                $content = View::make('front.sitemap', compact('pages'));

                return response()->make($content)->header(
                    'Content-Type', 'text/xml;charset=utf-8'
                );
            });

            Route::post('/verifications/confirm', 'VerificationsController@confirm')->name('verifications.confirm');

            Route::group(['middleware' => ['auth', '2fa'], 'as' => 'cabinet.', 'namespace' => 'Cabinet', 'prefix' => 'cabinet'],
                function () {
                    Route::get('/profile', 'ProfileController@show')->name('profile.show');
                    Route::get('/g2fa', 'ProfileController@g2fa')->name('profile.g2fa');
                    Route::post('/g2fa', 'ProfileController@g2faEnable')->name('profile.g2faEnable');
                    Route::post('/g2fa-disable', 'ProfileController@g2faDisable')->name('profile.g2faDisable');
                    Route::get('/profile/send-phone-verification', 'ProfileController@sendPhoneVerification')->name('profile.sendPhoneVerification');
                    Route::post('/profile/verify-phone', 'ProfileController@verifyPhone')->name('profile.verifyPhone');
                    Route::post('/profile/change-password', 'ProfileController@changePassword')->name('profile.changePassword');
                    Route::post('/profile/verify', 'ProfileController@verifySend')->name('profile.verifySend');
                    Route::post('/profile/change', 'ProfileController@changeSend')->name('profile.changeSend');
                    Route::post('/profile/change-type', 'ProfileController@changeType')->name('profile.changeType');
                    Route::post('/profile/change-notify', 'ProfileController@changeNotify')->name('profile.changeNotify');
                    Route::post('/profile/send-promo', 'ProfileController@sendPromo')->name('profile.sendPromo');
                    Route::post('/sms-get-code', 'ProfileController@getSmsCode')->name('sms.get.code');

                    Route::get('/contacts', 'ProfileController@contacts')->name('contacts');
                    Route::post('/contacts', 'ProfileController@submitContact')->name('profile.submitContact');

                    Route::get('/bonuses', 'BonusesController@index')->name('client.bonuses');
                    Route::post('/bonuses/payout', 'BonusesController@payout')->name('client.bonuses.payout');
                    Route::post('/bonuses/payout-dai', 'BonusesController@payoutDai')->name('client.bonuses.payoutDai');

                    Route::get('/journal', 'JournalController@index')->name('client.journal');
                    Route::get('/journal/read', 'JournalController@read')->name('client.journal.read');
                    Route::get('/transactions', 'TransactionsController@index')->name('client.transactions');
                    Route::put('/transactions/reject/{transaction}', 'TransactionsController@reject')->name('client.transactions.reject');

                    Route::post('/ajax/coin-price', 'AjaxController@coinPrice')->name('ajax.coinPrice');

                    Route::get('/conversions', 'ConvertController@conversions')->name('client.conversions');
                    Route::put('/conversions/cancel/{convert}', 'ConvertController@cancel')->name('client.conversions.cancel');

                    Route::post('/convert/request', 'ConvertController@request')->name('convert.request');

                    Route::group(['as' => 'admin.', 'namespace' => 'Admin', 'prefix' => 'admin'],
                        function () {
                            Route::get('/', 'HomeController@show')->name('home');
                            Route::get('/users', 'UsersController@index')->name('users.index');
                            Route::get('/configs', 'ConfigsController@index')->name('configs.index');
                            Route::get('/payments', 'PaymentsController@index')->name('payments.index');
                            Route::get('/benefits', 'BenefitsController@index')->name('benefits.index');
                        });

                    Route::group(['as' => 'trader.', 'namespace' => 'Trader', 'prefix' => 'trader'],
                        function () {
                            Route::get('/', 'HomeController@show')->name('home');
                            Route::post('/credit-line', 'HomeController@creditLine')->name('home.creditLine');
                            Route::post('/credit-level', 'HomeController@creditLevel')->name('home.creditLevel');
                            Route::post('/contract', 'HomeController@contract')->name('home.contract');
                            Route::get('/contract/request-phone', 'HomeController@requestPhoneVerification')->name('home.requestPhoneVerification');
                            Route::post('/graph-trader', 'HomeController@graphTrader')->name('home.graphTrader');
                            Route::post('/trader-document', 'HomeController@traderDocument')->name('home.traderDocument');
                            Route::post('/trader-document-pdf', 'HomeController@traderDocumentPdf')->name('home.traderDocumentPdf');
                            Route::post('/calc-first-payment', 'HomeController@calcFirstPayment')->name('home.calcFirstPayment');

                            Route::post('/credit-level-first', 'CreditLevelController@creditLevelFirst')->name('creditLevel.creditLevelFirst');
                            Route::post('/credit-level-second', 'CreditLevelController@creditLevelSecond')->name('creditLevel.creditLevelSecond');
                            Route::post('/credit-level-finish', 'CreditLevelController@creditLevelFinish')->name('creditLevel.creditLevelFinish');

                            Route::post('/credit-level-ur-first', 'CreditLevelController@creditLevelFirstUr')->name('creditLevel.creditLevelFirstUr');
                            Route::post('/credit-level-ur-second', 'CreditLevelController@creditLevelSecondUr')->name('creditLevel.creditLevelSecondUr');
                            Route::post('/credit-level-ur-third', 'CreditLevelController@creditLevelThirdUr')->name('creditLevel.creditLevelThirdUr');
                            Route::post('/credit-level-ur-finish', 'CreditLevelController@creditLevelFinishUr')->name('creditLevel.creditLevelFinishUr');
                            Route::post('/credit-level-ur-row', 'CreditLevelController@creditLevelFinishUrRow')->name('creditLevel.creditLevelFinishUrRow');

                            Route::get('/contracts', 'ContractsController@index')->name('contracts.index');
                            Route::get('/contracts/archive', 'ContractsController@archive')->name('contracts.archive');
                            Route::post('/contracts/{contract}/payment', 'ContractsController@payContract')->name('contracts.payContract');
                            Route::post('/contracts/payment-all', 'ContractsController@payAllContract')->name('contracts.payAllContract');
                            Route::post('/contracts/{contract}/download', 'ContractsController@download')->name('contracts.download');
                            Route::post('/contracts/{contract}/print', 'ContractsController@print')->name('contracts.print');
                            Route::post('/contracts/{contract}/sell', 'ContractsController@sell')->name('contracts.sell');
                            Route::post('/contracts/{contract}/redeem', 'ContractsController@redeem')->name('contracts.redeem');

                            Route::get('/portfolios', 'PortfoliosController@index')->name('portfolios.index');

                            Route::post('/payout', 'PayoutsController@index')->name('payout');
                            Route::post('/transfer-to-investor', 'PayoutsController@transferToInvestor')->name('payout.transferToInvestor');
                            Route::post('/payout-asset', 'PayoutsController@payoutAsset')->name('payout.payoutAsset');
                            Route::post('/transfer-asset', 'PayoutsController@transferAsset')->name('payout.transferAsset');
                            Route::post('/buy-asset', 'PayoutsController@buyAsset')->name('payout.buyAsset');
                        });

                    Route::group(['as' => 'investor.', 'namespace' => 'Investor', 'prefix' => 'investor'],
                        function () {
                            Route::get('/', 'HomeController@show')->name('home');
                            Route::post('/investor-document', 'HomeController@investorDocument')->name('home.investorDocument');
                            Route::post('/investor-document-dai', 'HomeController@investorDocumentDai')->name('home.investorDocumentDai');
                            Route::post('/investor-document-pdf', 'HomeController@investorDocumentPdf')->name('home.investorDocumentPdf');
                            Route::post('/deposit-line', 'HomeController@depositLine')->name('home.depositLine');
                            Route::post('/investor-rank', 'HomeController@investorRank')->name('home.investorRank');
                            Route::get('/contract/request-phone', 'HomeController@requestPhoneVerification')->name('home.requestPhoneVerification');
                            Route::post('/contract/month-percent', 'HomeController@getMonthPercent')->name('home.getMonthPercent');
                            Route::post('/graph-investor', 'HomeController@graphInvestor')->name('home.graphInvestor');

                            Route::post('/contract', 'HomeController@contract')->name('home.contract');
                            Route::post('/contract-dai', 'HomeController@contractDai')->name('home.contractDai');
                            Route::get('/contracts', 'ContractsController@index')->name('contracts.index');
                            Route::get('/contracts/archive', 'ContractsController@archive')->name('contracts.archive');
                            Route::post('/contracts/{contract}/download', 'ContractsController@download')->name('contracts.download');
                            Route::post('/contracts/{contract}/print', 'ContractsController@print')->name('contracts.print');
                            Route::post('/contracts/{contract}/terminate', 'ContractsController@terminate')->name('contracts.terminate');

                            Route::post('/investor-rank-first', 'InvestRanksController@investorRankFirst')->name('investRanks.investorRankFirst');
                            Route::post('/investor-rank-second', 'InvestRanksController@investorRankSecond')->name('investRanks.investorRankSecond');
                            Route::post('/investor-rank-finish', 'InvestRanksController@investorRankFinish')->name('investRanks.investorRankFinish');

                            Route::post('/investor-rank-ur-first', 'InvestRanksController@investorRankFirstUr')->name('investRanks.investorRankFirstUr');
                            Route::post('/investor-rank-ur-second', 'InvestRanksController@investorRankSecondUr')->name('investRanks.investorRankSecondUr');
                            Route::post('/investor-rank-ur-third', 'InvestRanksController@investorRankThirdUr')->name('investRanks.investorRankThirdUr');
                            Route::post('/investor-rank-ur-finish', 'InvestRanksController@investorRankFinishUr')->name('investRanks.investorRankFinishUr');

                            Route::get('/deposit/request-phone-deposit', 'DepositController@requestPhoneDeposit')->name('deposit.requestPhoneDeposit');
                            Route::get('/deposit/request-phone-payout-dai', 'DepositController@requestPhonePayoutDai')->name('deposit.requestPhonePayoutDai');
                            Route::post('/deposit/pay-in', 'DepositController@payIn')->name('deposit.payIn');
                            Route::post('/deposit/pay-in-dai', 'DepositController@payInDai')->name('deposit.payInDai');
                            Route::post('/deposit/pay-out-euro', 'DepositController@payOutEuro')->name('deposit.payOutEuro');
                            Route::post('/deposit/pay-out-dai', 'DepositController@payOutDai')->name('deposit.payOutDai');
                        });
                });

            Route::get('/news/category/{slug}', 'NewsController@category')->name('news.category');
            Route::get('/news/{slug}', 'NewsController@show')->name('news.show');
            Route::get('/news', 'NewsController@index')->name('news');

            Route::get('/exchange', 'StaticPagesController@exchange')->name('static-page.exchange');
            Route::get('/about', 'StaticPagesController@about')->name('static-page.about');
            Route::get('/investors', 'StaticPagesController@investors')->name('static-page.investors');
            Route::get('/contact', 'StaticPagesController@contact')->name('static-page.contact');
            Route::post('/contacts', 'StaticPagesController@submitContact')->name('static-page.submitContact');
            Route::get('/faq', 'StaticPagesController@faq')->name('static-page.faq');
            Route::get('/cert', 'StaticPagesController@cert')->name('static-page.cert');
            Route::get('/bonuses', 'StaticPagesController@bonuses')->name('static-pages.bonuses');

            Route::get('/{slug}', 'PagesController@show')->name('pages.show');

        }
    );
});

